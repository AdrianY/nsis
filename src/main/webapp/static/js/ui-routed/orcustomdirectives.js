/**
 * 
 */
nsisApp.directive('nsisCdOrRecipient', ["NsisDirectiveScopeWrapper", "FacilityPersonnelService",function(NsisDirectiveScopeWrapper, FacilityPersonnelService) {
    var directive = {};
    directive.restrict = 'E';
    
    directive.template = '<div ng-class="xstate" class="form-group no-big-gap">' +
						 '<label for="{{label}}" class="control-label">{{textLabel}}</label>' +
						 '<div class="{{containerClassIds}}">' +
						 '<input type="text" placeholder="Search Personnel" ' +
						 'name="{{label}}" ' +
						 'ng-disabled="isDisabled"' +
						 'uib-tooltip="{{problemMsg}}" ' +
						 'tooltip-enable="{{hasProblem}}"' +
						 'uib-typeahead="facilityPersonnel as facilityPersonnel.fullName for facilityPersonnel in searchFacilityPersonnels($viewValue)"' +
						 'ng-model="ngModel" ' +
						 'typeahead-no-results="noResults" typeahead-editable="false" '+
						 'class="{{classIds}}" ' +
						 'typeahead-on-select="onFacilityPersonnelSelect($item, $model, $label)" ' +
						 'typeahead-template-url="/NSIS/static/view/ui-routed/typeahead/facility-personnel.html">' +
						 '<i ng-show="loadingLocations" class="glyphicon glyphicon-refresh"></i>' +
						 '<div ng-show="noResults">' +
						 '<i class="glyphicon glyphicon-remove"></i> No Results Found' +
						 '</div>' +
						 '</div>' +
						 '</div>';
    
    
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'input-sm form-control'; }
        if (!attrs.containerClassIds) { attrs.containerClassIds ='col-sm-9 pull-right'; }
        if (!attrs.augment) {attrs.augment = true;}

        var linkFunction = function($scope, element, atttributes) {
        	if($scope.augment)
        		NsisDirectiveScopeWrapper.augmentScope($scope);
        	
        	$scope.searchFacilityPersonnels = function(val) {
				var parameters = {
						facilityId: $scope.facilityIdNgModel,
						fullName: val,
						username: val, 
						searchType: "TYPEHEAD"
				};
				return FacilityPersonnelService.get(parameters).$promise.then(function (data) {
					return data.rows;
				});
			};
			
			$scope.onFacilityPersonnelSelect = function($item, $model, $label){
				$scope.targetNgModel.fullName = $model.fullName;
				$scope.targetNgModel.emailAddress = $model.emailAddress;
				$scope.targetNgModel.contactNumber = $model.contactNumber;
				$scope.targetNgModel.userName = $model.username;
			}
			
			$scope.$watch(
				'targetNgModel.fullName',
				function(newValue, oldValue) {
					if(!newValue) $scope.targetNgModel.userName = null;
				}
			);
        }

        return linkFunction;
    }
    
    directive.scope = {
        ngModel: '=',
        targetNgModel: '=target',
        facilityIdNgModel: '=filter',
        classIds: '@',
        containerClassIds: '@',
        label: '@',
        textLabel: '@',
        coId: '@',
        qroId: '@',
        augment: '@'
    }

    return directive;
}])
nsisApp.directive('nsisCdTaWarehouseLocations', ["NsisDirectiveScopeWrapper", "WarehouseLocationService",function(NsisDirectiveScopeWrapper, WarehouseLocationService) {
    var directive = {};
    directive.restrict = 'E';
    
    directive.templateUrl = '/NSIS/static/view/ui-routed/typeahead/warehouse-location-search-field.html';
    
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'input-sm form-control'; }
        if (!attrs.containerClassIds) { attrs.containerClassIds ='col-sm-9 pull-right'; }
        if (!attrs.augment) {attrs.augment = true;}

        var linkFunction = function($scope, element, atttributes) {
        	if($scope.augment)
        		NsisDirectiveScopeWrapper.augmentScope($scope);
        	
        	$scope.searchWarehouseLocation = function(val) {
				var parameters = {
						facilityCode: $scope.facilityIdNgModel,
						rackCode: val,
						searchType: "TYPEHEAD"
				};
				return WarehouseLocationService.get(parameters).$promise.then(function (data) {
					return data.rows;
				});
			};
			
			$scope.onSelectWarehouseLocation = function($item, $model, $label){
				$scope.targetNgModel = $model.rackCode;
			}
			
        }

        return linkFunction;
    }
    
    directive.scope = {
        ngModel: '=',
        disabled: '=',
        targetNgModel: '=target',
        facilityIdNgModel: '=filter',
        classIds: '@',
        containerClassIds: '@',
        label: '@',
        textLabel: '@',
        coId: '@',
        qroId: '@',
        augment: '@'
    }

    return directive;
}])
.directive('nsisGridCdOrItems', ["$uibModal", "NsisDirectiveScopeWrapper", "OrderRequestItem",function($uibModal, NsisDirectiveScopeWrapper, OrderRequestItem) {
	var recalcGridData = [];
	var linkFunction = function($scope, element, atttributes, ctrl, transclude){
		if($scope.augment)
    		NsisDirectiveScopeWrapper.augmentScope($scope);
		
		$scope.$watch(
			'orderRequest.body.status', function(){
				recalculateItems();
			}
		)
		
		$scope.remove = function(){
			var itemsForRemoval = $scope.gridOptions.api.getSelectedRows();
			if(itemsForRemoval && 0 < itemsForRemoval.length){
				var itemsRequested = $scope.orderRequest.body.itemsRequested;
				var remainingItems = itemsRequested.filter(function(item){
					var itemForRemoval = itemsForRemoval.find(function(itemToBeRemoved){
						return itemToBeRemoved.codeName === item.itemCode;
					});
					
					return !itemForRemoval;
				})
				$scope.orderRequest.body.itemsRequested = remainingItems;
				recalculateItems();
			}
		}
		
		$scope.open = function (size) {
			var orderRequest = $scope.orderRequest.body;
			var data = {
				orderType: orderRequest.orderType,
				currency: orderRequest.currency,
				facilityId: orderRequest.facilityId,
				orderNumber: orderRequest.orderNumber
			}
			var orderRequestItem = new OrderRequestItem(data);
			openAddItemModal(orderRequestItem, 'ADD');
		};
		
		/********************************Grid related*******************************/
		var columnDefs = [
		   {headerName: "Item code", field: "codeName", suppressMenu:true},
		   {headerName: "Type", field: "type", suppressMenu:true},
		   {headerName: "Description", field: "description", suppressMenu:true},
		   {headerName: "UOM", field: "uom",cellStyle: {'text-align': 'center'}, suppressMenu:true},
		   {headerName: "Qty", field: "orderdQtyByUom", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}},
		   {headerName: "Pcs", field: "orderedQtyByPcs", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}},
		   {headerName: "L.Stk", field: "logicalStock", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}},
		   {headerName: "P.Stk", field: "physicalStock", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}},
		   {headerName: "Max", field: "max", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}},
		   {headerName: "Min", field: "min", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}},
		   {headerName: "Amount", field: "requestedItemsCost", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}},
		   {headerName: "Remarks", field: "remarks", suppressMenu:true, hide: true},
		   {headerName: "itemCost", field: "itemCost", suppressMenu:true, hide: true}
		];
		
		var dataSource = {
				pageSize: 10,
				getRows: function (params) {
					var rowsThisPage = recalcGridData.slice(params.startRow, params.endRow);
					var lastRow = -1;
	                if (recalcGridData.length <= params.endRow) {
	                    lastRow = recalcGridData.length;
	                }
	                params.successCallback(rowsThisPage, lastRow);
				}
		};
		
		$scope.gridOptions = {
				enableServerSideSorting: true,
			    enableFilter: true,
			    enableColResize: true,
			    rowSelection: 'multiple',
				columnDefs: columnDefs,
				onGridReady: function (api) {
					recalculateItems();
	            },
				onRowDoubleClicked: function(params){
					
					if(!$scope.isDisabled){
						var data =  params['data'];
						var orderRequestRaw = {};
						var orderRequest = $scope.orderRequest.body;
						orderRequestRaw.orderNumber = orderRequest.orderNumber;
						orderRequestRaw.facilityId = orderRequest.facilityId;
						orderRequestRaw.itemType = data.type;
						orderRequestRaw.description = data.description;
						orderRequestRaw.unitOfMeasurement = data.uom;
						orderRequestRaw.maximum = data.max;
						orderRequestRaw.minimum = data.min;
						orderRequestRaw.quantity = data.orderdQtyByUom;
						orderRequestRaw.remarks = data.remarks;
						orderRequestRaw.orderType = orderRequest.orderType;
						orderRequestRaw.currency = orderRequest.currency;
						orderRequestRaw.itemCost = data.itemCost;
						orderRequestRaw.itemCode = data.codeName;
						var orderRequestItem = new OrderRequestItem(orderRequestRaw);
						openAddItemModal(orderRequestItem, 'EDIT');
					}
				}
		};
		
		transclude($scope, function(clone, scope) {
			element.find('.insertion-point').append(clone);
		});
		
		function recalculateItems(){
			$scope.$parent.notifyLoading();
			$scope.$parent.orderRequest.showRecalculatedItems(displayRecalculatedItems);
		}
		function displayRecalculatedItems(result){
			recalcGridData = result;
			$scope.gridOptions.api.setDatasource(dataSource);
			$scope.gridOptions.api.sizeColumnsToFit();
			$scope.$parent.notifyDoneLoading();
		}
		
		
		function openAddItemModal(orderRequestItem, mode){
			var modalInstance = $uibModal.open({
				animation: true,
				backdrop: 'static',
				templateUrl: parentPopUri + 'order-form-add-item.html',
				controller: 'ModalInstanceCtrl',
				resolve: {
					orderRequestItem: function () {return orderRequestItem;},
					mode: function () {return mode;}
				}
			});
			
			modalInstance.result.then(function (requestedItem) {
				$scope.orderRequest.saveRequestedItem(requestedItem,mode,function(thereIsChange){
					if(thereIsChange) recalculateItems();
				});
			}, function () {
				console.log('Modal dismissed at: ' + new Date());
			});
		}
	}
    var directive = {};
    
    directive.restrict = 'E';
    directive.transclude = true;
    directive.templateUrl = '/NSIS/static/view/ui-routed/grids/order-requests-form-items.html';
    
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'default-nsis-grid'; }
        if (!attrs.augment) {attrs.augment = true;}

        return linkFunction;
    }
    
    directive.scope = {
        orderRequest: '=orderRequest',
        classIds: '@',
        label: '@',
        textLabel: '@',
        coId: '@',
        qroId: '@',
        augment: '@'
    }

    return directive;
}])
.directive('nsisGridCdOrderrequests', function() {
	var recalcGridData = [];
	var linkFunction = function($scope, element, atttributes, ctrl, transclude){
		
		/********************************Grid related*******************************/
		var columnDefs = [
		                  {headerName: "Order Number", field: "orderNumber", suppressMenu:true},
		                  {headerName: "Status", field: "status", suppressMenu:true},
		                  {headerName: "Order Date", field: "orderDate", suppressMenu:true},
		                  {headerName: "Type", field: "orderType", suppressMenu:true},
		                  {headerName: "Order By", field: "orderBy",cellStyle: {'text-align': 'center'}, suppressMenu:true},
		                  {headerName: "Order Cycle", field: "orderCycle",cellStyle: {'text-align': 'center'}, suppressMenu:true},
		                  {headerName: "Amount", field: "totalRequestCost", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}}
		                  ];

		var dataSource = {
				pageSize: 10,
				getRows: function (params) {
					var parameters = {/*type: 'Labels', */rows: 10, rowOffset: params.startRow};
					if(params.sortModel && params.sortModel.length > 0){
						var sortModel = params.sortModel[0];
						parameters.sidx = sortModel.colId;
						parameters.sord = sortModel.sort;
					}
					$scope.list(parameters, function (data) {
						params.successCallback(data.rows, data.records);
						if ($scope.gridOptions) {
							$scope.gridOptions.api.refreshView();
							$scope.gridOptions.api.sizeColumnsToFit();
						}
					});
				}
		};

		$scope.gridOptions = {
				enableServerSideSorting: true,
				enableFilter: true,
				enableColResize: true,
				columnDefs: columnDefs,
				rowSelection: 'single',
				onGridReady: function (api) {
					$scope.gridOptions.api.setDatasource(dataSource);
					$scope.gridOptions.api.sizeColumnsToFit();
				},
				onRowDoubleClicked: function(params){
					var data =  params['data'];
					$scope.view(data.orderNumber);
				}
		};
		
		$scope.remove = function(){
			var orderRequestOverview = $scope.gridOptions.api.getSelectedRows()[0];
			if(orderRequestOverview){
				$scope.removeOr(orderRequestOverview.orderNumber, function(successful){
					if(successful){
						$scope.gridOptions.api.setDatasource(dataSource);
						$scope.gridOptions.api.sizeColumnsToFit();
					}
				});
			}
			
		}

		transclude($scope, function(clone, scope) {
			element.find('.insertion-point').append(clone);
		});
		
	}
    var directive = {};
    
    directive.restrict = 'E';
    directive.transclude = true;
    directive.templateUrl = '/NSIS/static/view/ui-routed/grids/order-requests.html';
    
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'default-nsis-grid'; }
        if (!attrs.augment) {attrs.augment = true;}

        return linkFunction;
    }
    
    directive.scope = {
    	view: '=viewAction',
    	add: '=addAction',
    	removeOr: '=removeAction',
    	list: '=listAction',
        classIds: '@'
    }

    return directive;
})
.directive('nsisGridCdSubmittedOrderrequests', function() {
	var recalcGridData = [];
	var linkFunction = function($scope, element, atttributes, ctrl, transclude){
		
		/********************************Grid related*******************************/
		var columnDefs = [
		                  {headerName: "Order Number", field: "orderNumber", suppressMenu:true},
		                  {headerName: "Status", field: "status", suppressMenu:true},
		                  {headerName: "Order Date", field: "orderDate", suppressMenu:true},
		                  {headerName: "Type", field: "orderType", suppressMenu:true},
		                  {headerName: "Order By", field: "orderBy",cellStyle: {'text-align': 'center'}, suppressMenu:true},
		                  {headerName: "Order Cycle", field: "orderCycle",cellStyle: {'text-align': 'center'}, suppressMenu:true},
		                  {headerName: "Amount", field: "totalRequestCost", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}}
		                  ];

		var dataSource = {
				pageSize: 10,
				getRows: function (params) {
					var parameters = {/*type: 'Labels', */rows: 10, rowOffset: params.startRow};
					if(params.sortModel && params.sortModel.length > 0){
						var sortModel = params.sortModel[0];
						parameters.sidx = sortModel.colId;
						parameters.sord = sortModel.sort;
					}
					$scope.list(parameters, function (data) {
						params.successCallback(data.rows, data.records);
						if ($scope.gridOptions) {
							$scope.gridOptions.api.refreshView();
							$scope.gridOptions.api.sizeColumnsToFit();
						}
					});
				}
		};
		
		$scope.$watch(
	    		'triggerReload',
	    		function(newValue, oldValue) {
	    			if(newValue) {
	    				$scope.gridOptions.api.setDatasource(dataSource);
						$scope.gridOptions.api.sizeColumnsToFit();
	    			}
	    		}
	    );

		$scope.gridOptions = {
				enableServerSideSorting: true,
				enableFilter: true,
				enableColResize: true,
				columnDefs: columnDefs,
				rowSelection: 'single',
				onGridReady: function (api) {
					$scope.gridOptions.api.setDatasource(dataSource);
				},
				onRowDoubleClicked: function(params){
					var data =  params['data'];
					console.log('data.ordeNumber: '+data.orderNumber)
					$scope.view(data.orderNumber);
				}
		};

		transclude($scope, function(clone, scope) {
			element.find('.insertion-point').append(clone);
		});
		
	}
    var directive = {};
    
    directive.restrict = 'E';
    directive.transclude = true;
    directive.templateUrl = '/NSIS/static/view/ui-routed/grids/submitted-order-requests.html';
    
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'default-nsis-grid'; }
        if (!attrs.augment) {attrs.augment = true;}

        return linkFunction;
    }
    
    directive.scope = {
    	view: '=viewAction',
    	list: '=listAction',
    	triggerReload: '=trigger',
        classIds: '@'
    }

    return directive;
})
.directive('nsisGridCdDeliveryReceipts', function() {
	var recalcGridData = [];
	var linkFunction = function($scope, element, atttributes, ctrl, transclude){
		
		/********************************Grid related*******************************/
		var columnDefs = [
		                  {headerName: "Order Number", field: "orderNumber", suppressMenu:true},
		                  {headerName: "Status", field: "status", suppressMenu:true},
		                  {headerName: "Order Date", field: "orderDate", suppressMenu:true},
		                  {headerName: "Type", field: "orderType", suppressMenu:true},
		                  {headerName: "Order By", field: "orderBy",cellStyle: {'text-align': 'center'}, suppressMenu:true},
		                  {headerName: "Order Cycle", field: "orderCycle",cellStyle: {'text-align': 'center'}, suppressMenu:true},
		                  {headerName: "Amount", field: "totalRequestCost", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}}
		                  ];

		var dataSource = {
				pageSize: 10,
				getRows: function (params) {
					var parameters = {/*type: 'Labels', */rows: 10, rowOffset: params.startRow};
					if(params.sortModel && params.sortModel.length > 0){
						var sortModel = params.sortModel[0];
						parameters.sidx = sortModel.colId;
						parameters.sord = sortModel.sort;
					}
					$scope.list(parameters, function (data) {
						params.successCallback(data.rows, data.records);
						if ($scope.gridOptions) {
							$scope.gridOptions.api.refreshView();
							$scope.gridOptions.api.sizeColumnsToFit();
						}
					});
				}
		};

		$scope.gridOptions = {
				enableServerSideSorting: true,
				enableFilter: true,
				enableColResize: true,
				columnDefs: columnDefs,
				rowSelection: 'single',
				onGridReady: function (api) {
					$scope.gridOptions.api.setDatasource(dataSource);
					$scope.gridOptions.api.sizeColumnsToFit();
				},
				onRowDoubleClicked: function(params){
					var data =  params['data'];
					console.log('data.ordeNumber: '+data.orderNumber)
					$scope.view(data.orderNumber);
				}
		};
		
		$scope.remove = function(){
			var orderRequestOverview = $scope.gridOptions.api.getSelectedRows()[0];
			if(orderRequestOverview){
				$scope.removeOr(orderRequestOverview.orderNumber, function(successful){
					if(successful){
						$scope.gridOptions.api.setDatasource(dataSource);
						$scope.gridOptions.api.sizeColumnsToFit();
					}
				});
			}
			
		}

		transclude($scope, function(clone, scope) {
			element.find('.insertion-point').append(clone);
		});
		
	}
    var directive = {};
    
    directive.restrict = 'E';
    directive.transclude = true;
    directive.templateUrl = '/NSIS/static/view/ui-routed/grids/delivery-receipts.html';
    
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'default-nsis-grid'; }
        if (!attrs.augment) {attrs.augment = true;}

        return linkFunction;
    }
    
    directive.scope = {
    	view: '=viewAction',
    	list: '=listAction',
        classIds: '@'
    }

    return directive;
})
.directive('nsisGridCdOrItemReceivables', ["$uibModal", "NsisDirectiveScopeWrapper", "NHelper", "DeliveredItemBatchGroup",function($uibModal, NsisDirectiveScopeWrapper, NHelper, DeliveredItemBatchGroup) {
	var recalcGridData = [];
	
	var linkFunction = function($scope, element, atttributes, ctrl, transclude){
		
		if($scope.augment)
    		NsisDirectiveScopeWrapper.augmentScope($scope);
		
		/********************************Grid related*******************************/
		var columnDefs = [
		   {headerName: "Item code", field: "codeName", suppressMenu:true},
		   {headerName: "Type", field: "type", suppressMenu:true},
		   {headerName: "Description", field: "description", suppressMenu:true},
		   {headerName: "UOM", field: "uom",cellStyle: {'text-align': 'center'}, suppressMenu:true},
		   {headerName: "Qty", field: "orderdQtyByUom", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}},
		   {headerName: "Pcs", field: "orderedQtyByPcs", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}},
		   {headerName: "L.Stk", field: "logicalStock", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}},
		   {headerName: "P.Stk", field: "physicalStock", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}},
		   {headerName: "Max", field: "max", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}},
		   {headerName: "Min", field: "min", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}},
		   {headerName: "Amount", field: "requestedItemsCost", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}},
		   {headerName: "Remarks", field: "remarks", suppressMenu:true, hide: true},
		   {headerName: "itemCost", field: "itemCost", suppressMenu:true, hide: true}
		];
		
		var dataSource = {
				pageSize: 10,
				getRows: function (params) {
					var rowsThisPage = recalcGridData.slice(params.startRow, params.endRow);
					var lastRow = -1;
	                if (recalcGridData.length <= params.endRow) {
	                    lastRow = recalcGridData.length;
	                }
	                params.successCallback(rowsThisPage, lastRow);
				}
		};
		
		$scope.gridOptions = {
				enableServerSideSorting: true,
			    enableFilter: true,
			    enableColResize: true,
			    rowSelection: 'single',
				columnDefs: columnDefs,
				onGridReady: function (api) {
					recalculateItems();
	            },
				onRowDoubleClicked: function(params){
					
					if(true){
						var data =  params['data'];
						var deliveryReceiptDetails = $scope.deliveryReceipt.body;
						var orderRequest = deliveryReceiptDetails.orderRequest;
						
						var itemBatchGroup = null;
						NHelper.ifPresent(deliveryReceiptDetails.itemBatches,function(itemBatchGroups){
							itemBatchGroup = itemBatchGroups[data.codeName];
						})
						var itemBatches = itemBatchGroup ? angular.copy(itemBatchGroup) : [];
						var itemBatchGroupData = {
							orderDetails: {
								orderNumber: orderRequest.orderNumber,
								facilityCode: orderRequest.facilityId,
								currency: orderRequest.currency,
							},
							receivableDetails: {
								itemCode: data.codeName,
								itemType: data.type,
								description: data.description,
								uom: data.uom,
								quantity: data.orderdQtyByUom
							},
							itemBatches: itemBatches,
							editedItemBatch: {}
						};

						openAddItemModal(new DeliveredItemBatchGroup(itemBatchGroupData));
					}
				}
		};
		
		transclude($scope, function(clone, scope) {
			element.find('.insertion-point').append(clone);
		});
		
		function recalculateItems(){
			$scope.$parent.notifyLoading();
			$scope.orderRequest.showRecalculatedItems(displayRecalculatedItems);
		}
		
		function displayRecalculatedItems(result){
			recalcGridData = result;
			$scope.gridOptions.api.setDatasource(dataSource);
			$scope.gridOptions.api.sizeColumnsToFit();
			$scope.$parent.notifyDoneLoading();
		}
		
		function openAddItemModal(deliveredItemBatchGroup){
			var modalInstance = $uibModal.open({
				animation: true,
				backdrop: 'static',
				templateUrl: parentPopUri + 'delivery-receipt-form-item-batch.html',
				controller: 'DeliveryItemBatchGroupModalCtrl',
				resolve: {
					deliveredItemBatchGroup: function () {return deliveredItemBatchGroup;},
					editMeta: function(){return $scope.$parent.editMeta;}
				}
			});
			
			modalInstance.result.then(function (itemBatchGroup) {
				$scope.deliveryReceipt.updateItemBatchGroupList(itemBatchGroup,null);
			}, function () {
			});
		}
	}
	
    var directive = {};
    
    directive.restrict = 'E';
    directive.transclude = true;
    directive.templateUrl = '/NSIS/static/view/ui-routed/grids/delivery-receipts-form-items.html';
    
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'default-nsis-grid'; }
        if (!attrs.augment) {attrs.augment = true;}

        return linkFunction;
    }
    
    directive.scope = {
        orderRequest: '=or',
        deliveryReceipt: '=dr',
        classIds: '@',
        label: '@',
        textLabel: '@',
        coId: '@',
        qroId: '@',
        augment: '@'
    }

    return directive;
}])
.directive('nsisGridCdDeliveryItemBatches', ["DateUtil",function(DateUtil) {
	var recalcGridData = [];
	var linkFunction = function($scope, element, atttributes, ctrl, transclude){
		
		/********************************Grid related*******************************/
		var columnDefs = [
		                  {headerName: "Batch Number", field: "batchNumber", suppressMenu:true, width: 500},
		                  {headerName: "Rack", field: "rackingCode", suppressMenu:true, width: 170},
		                  {headerName: "Dte Stored", field: "dateStored", suppressMenu:true, cellStyle: {'text-align': 'center'}, width: 250},
		                  {headerName: "Qty", field: "quantity", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}, width: 250},
		                  {headerName: "Amount", field: "amount", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}, width: 250}
		                  ];

		var dataSource = {
				pageSize: 10,
				getRows: function (params) {
					var rowsThisPage = recalcGridData.slice(params.startRow, params.endRow);
					var lastRow = -1;
	                if (recalcGridData.length <= params.endRow) {
	                    lastRow = recalcGridData.length;
	                }
	                params.successCallback(rowsThisPage, lastRow);
				}
		};

		$scope.gridOptions = {
				enableServerSideSorting: true,
				enableFilter: true,
				enableColResize: true,
				columnDefs: columnDefs,
				rowSelection: 'single',
				onGridReady: function (api) {
					$scope.gridOptions.api.setDatasource(dataSource);
					$scope.gridOptions.api.sizeColumnsToFit();
				},
				onRowDoubleClicked: function(params){
					var data =  params['data'];
					var itemBatch = {
						dateStored: DateUtil.parseShortDate(data.dateStored),
						rackingCode: {
							rackCode: data.rackingCode
						},
						quantity: data.quantity
					}
					$scope.view(itemBatch);
				}
		};
		
		$scope.remove = function(){
			var itemBatch = $scope.gridOptions.api.getSelectedRows()[0];
			if(itemBatch){
				$scope.removeOr(itemBatch.batchNumber, function(successful){
					if(successful){
						$scope.list(function (data) {
							recalcGridData = data;
							$scope.gridOptions.api.setDatasource(dataSource);
							$scope.gridOptions.api.sizeColumnsToFit();
						});
					}
				});
			}
			
		}
		
		$scope.$watch('refresh', function(nv,ov){
			if(nv){
				$scope.list(function (data) {
					recalcGridData = data;
					$scope.gridOptions.api.setDatasource(dataSource);
					$scope.gridOptions.api.sizeColumnsToFit();
				});
				$scope.refresh = false;
			}
		});

		transclude($scope, function(clone, scope) {
			element.find('.insertion-point').append(clone);
		});
		
	}
    var directive = {};
    
    directive.restrict = 'E';
    directive.transclude = true;
    directive.templateUrl = '/NSIS/static/view/ui-routed/grids/delivered-item-batches.html';
    
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'default-nsis-grid'; }
        if (!attrs.augment) {attrs.augment = true;}

        return linkFunction;
    }
    
    directive.scope = {
    		view: '=viewAction',
    		removeOr: '=removeAction',
    		list: '=listAction',
    		refresh:'=refreshTrigger',
    		classIds: '@',
    		label: '@',
    		textLabel: '@'
    }

    return directive;
}])
.directive('nsisGridCdWhRequestsIssuance', function() {
	var recalcGridData = [];
	var linkFunction = function($scope, element, atttributes, ctrl, transclude){
		
		/********************************Grid related*******************************/
		var columnDefs = [
		                  {headerName: "Request Number", field: "requestNumber", suppressMenu:true},
		                  {headerName: "Request Date", field: "requestDate", suppressMenu:true},
		                  {headerName: "Facility", field: "facilityCodeName", suppressMenu:true},
		                  {headerName: "Request By", field: "requestBy",cellStyle: {'text-align': 'center'}, suppressMenu:true},
		                  {headerName: "Status", field: "status", suppressMenu:true},
		                  ];

		var dataSource = {
				pageSize: 10,
				getRows: function (params) {
					var parameters = {/*type: 'Labels', */rows: 10, rowOffset: params.startRow};
					if(params.sortModel && params.sortModel.length > 0){
						var sortModel = params.sortModel[0];
						parameters.sidx = sortModel.colId;
						parameters.sord = sortModel.sort;
					}
					$scope.list(parameters, function (data) {
						params.successCallback(data.rows, data.records);
						if ($scope.gridOptions) {
							$scope.gridOptions.api.refreshView();
							$scope.gridOptions.api.sizeColumnsToFit();
						}
					});
				}
		};

		$scope.gridOptions = {
				enableServerSideSorting: true,
				enableFilter: true,
				enableColResize: true,
				columnDefs: columnDefs,
				rowSelection: 'single',
				onGridReady: function (api) {
					$scope.gridOptions.api.setDatasource(dataSource);
					$scope.gridOptions.api.sizeColumnsToFit();
				},
				onRowDoubleClicked: function(params){
					var data =  params['data'];
					$scope.view(data.requestNumber,data.facilityCodeName);
				}
		};
		
		transclude($scope, function(clone, scope) {
			element.find('.insertion-point').append(clone);
		});
		
	}
    var directive = {};
    
    directive.restrict = 'E';
    directive.transclude = true;
    directive.templateUrl = '/NSIS/static/view/ui-routed/grids/wh-requests-issuances.html';
    
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'default-nsis-grid'; }
        if (!attrs.augment) {attrs.augment = true;}

        return linkFunction;
    }
    
    directive.scope = {
    	view: '=viewAction',
    	list: '=listAction',
        classIds: '@'
    }

    return directive;
})
.directive('nsisGridCdReqIssuanceSupplyGrps', ["$uibModal", "NsisDirectiveScopeWrapper", "RequestedItemIssuance", "NHelper",function($uibModal, NsisDirectiveScopeWrapper, RequestedItemIssuance, NHelper) {
	var recalcGridData = [];
	var linkFunction = function($scope, element, atttributes, ctrl, transclude){
		if($scope.augment)
    		NsisDirectiveScopeWrapper.augmentScope($scope);
		
		$scope.$watch(
			'supplyRequest.body.status', function(){
				recalculateItems();
			}
		)
		
		/********************************Grid related*******************************/
		var columnDefs = [
		   {headerName: "Item code", field: "codeName", suppressMenu:true},
		   {headerName: "Type", field: "type", suppressMenu:true},
		   {headerName: "Description", field: "description", suppressMenu:true},
		   {headerName: "UOM", field: "uom",cellStyle: {'text-align': 'center'}, suppressMenu:true},
		   {headerName: "Qty", field: "orderdQtyByUom", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}},
		   {headerName: "Pcs", field: "orderedQtyByPcs", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}},
		   {headerName: "Amount", field: "requestedItemsCost", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}},
		   {headerName: "itemCost", field: "itemCost", suppressMenu:true, hide: true}
		];
		
		var dataSource = {
				pageSize: 10,
				getRows: function (params) {
					var rowsThisPage = recalcGridData.slice(params.startRow, params.endRow);
					var lastRow = -1;
	                if (recalcGridData.length <= params.endRow) {
	                    lastRow = recalcGridData.length;
	                }
	                params.successCallback(rowsThisPage, lastRow);
				}
		};
		
		$scope.gridOptions = {
				enableServerSideSorting: true,
			    enableFilter: true,
			    enableColResize: true,
			    rowSelection: 'single',
				columnDefs: columnDefs,
				onGridReady: function (api) {
					recalculateItems();
	            },
				onRowDoubleClicked: function(params){
					
					if(!$scope.isDisabled){
						var data =  params['data'];
						var supplyRequest = $scope.supplyRequest.body;
						var existingRequestItemGroupDetails = 
							$scope.requestIssuance.findSuppliedItemsGrp(data.codeName);
						
						var suppliedItems = [];
						if(existingRequestItemGroupDetails){
							NHelper.ifPresent(existingRequestItemGroupDetails.suppliedItems, function(siArr){
								suppliedItems = siArr.map(function(si){
									si.currentlyEdited = false;
									return si;
								})
							})
						}
						
						console.log("existing suppliedItems for item: "+data.codeName)
						suppliedItems.forEach(function(si){
							var propStr = "";
							for(var key in si){
								propStr += key+": "+si[key]+", "
							}
							console.log(propStr)
						})
						
						var requestItemGroupRaw = {
								requestDetails: {
									requestNumber: supplyRequest.requestNumber,
									itemCode: data.codeName,
									itemType: data.type,
									description: data.description,
									uom: data.uom,
									piecesPerUOM: data.orderedQtyByPcs,
									requestedQty: data.orderdQtyByUom
								},
								supplyDetails: {},
								suppliedItems: suppliedItems
						};
						var requestItemGroup = new RequestedItemIssuance(requestItemGroupRaw);
						openAddItemModal(requestItemGroup, 'EDIT');
					}
				}
		};
		
		transclude($scope, function(clone, scope) {
			element.find('.insertion-point').append(clone);
		});
		
		function recalculateItems(){
			$scope.$parent.notifyLoading();
			$scope.$parent.supplyRequest.showRecalculatedItems(displayRecalculatedItems);
		}
		function displayRecalculatedItems(result){
			recalcGridData = result;
			$scope.gridOptions.api.setDatasource(dataSource);
			$scope.gridOptions.api.sizeColumnsToFit();
			$scope.$parent.notifyDoneLoading();
		}
		
		
		function openAddItemModal(requestItemGroup, mode){
			var modalInstance = $uibModal.open({
				animation: true,
				backdrop: 'static',
				size: 'lg',
				templateUrl: parentPopUri + 'wh-request-issuance-issue-supply-by-fifo-batch.html',
				controller: 'RequestIssuanceModalCtrl',
				resolve: {
					requestItemGroup: function () {return requestItemGroup;},
					mode: function () {return mode;}
				}
			});
			
			modalInstance.result.then(function (requestItemGroup) {
				var itemIssuanceGroup = {
					itemCode: requestItemGroup.requestDetails.itemCode, 
					suppliedItems: requestItemGroup.suppliedItems.reduce(function(prev, si){
						prev.push({
							sourceBatchNumber: si.sourceBatchNumber,
							suppliedQuantity: si.suppliedQuantity,
							remarks: si.remarks
						})
						return prev
					},[])
				}
				
				$scope.requestIssuance.saveSuppliedItemsGrp(itemIssuanceGroup);
			}, function () {
				console.log('Modal dismissed at: ' + new Date());
			});
		}
	}
    var directive = {};
    
    directive.restrict = 'E';
    directive.transclude = true;
    directive.templateUrl = '/NSIS/static/view/ui-routed/grids/wh-requests-issuances-item-sup-grps.html';
    
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'default-nsis-grid'; }
        if (!attrs.augment) {attrs.augment = true;}

        return linkFunction;
    }
    
    directive.scope = {
        supplyRequest: '=sr',
        requestIssuance: '=requestIssuance',
        classIds: '@',
        label: '@',
        textLabel: '@',
        coId: '@',
        qroId: '@',
        augment: '@'
    }

    return directive;
}])
.directive('nsisGridCdReqIssuanceSupplyGrpsItems', ["DateUtil",function(DateUtil) {
	
	var linkFunction = function($scope, element, atttributes, ctrl, transclude){
		
		/********************************Grid related*******************************/
		var editable = true;
		
		var columnDefs = [
		                  {headerName: "Batch Number", field: "batchNumber", suppressMenu:true, width: 510},
		                  {headerName: "Rack", field: "rackNumber", suppressMenu:true, width: 170},
		                  {headerName: "Dte Stored", field: "dateDelivered", suppressMenu:true, cellStyle: {'text-align': 'center'}, width: 240},
		                  {headerName: "Item Cost", field: "itemCost", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}, width: 180},
		                  {headerName: "Total Cost", field: "totalCost", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}, width: 200},
		                  {headerName: "Rem Phy Stk", field: "physicalStock", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}, width: 200},
		                  {headerName: "Sup Qty", field: "suppliedQuantity",editable:editable,suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}, width: 200},
		                  {headerName: "Remarks", field: "remarks",editable:editable, suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}, width: 410},
		                  {headerName: "Prev Sup Qty", field: "previousSuppliedQuantity", hide:true}
		                  ];

		var recalcGridData = [];
		var dataSource = {
				pageSize: 10,
				getRows: function (params) {
					var rowsThisPage = recalcGridData.slice(params.startRow, params.endRow);
					var lastRow = -1;
	                if (recalcGridData.length <= params.endRow) {
	                    lastRow = recalcGridData.length;
	                }
	                params.successCallback(rowsThisPage, lastRow);
				}
		};

		var currSelectedRow = null;
		var thereWasChange = false;
		var thereIsError = false;
		var validate = false;
		var redrawing = false;
		$scope.gridOptions = {
				enableFilter: true,
				enableColResize: true,
				columnDefs: columnDefs,
				rowSelection: 'single',
				onGridReady: function (api) {
					$scope.gridOptions.api.setDatasource(dataSource);
					$scope.gridOptions.api.sizeColumnsToFit();
				},
				onSelectionChanged: function(event){
					var selectedIndex = Object.keys(event.selectedNodesById)[0];
					var selectedRow = event.selectedRows[0];
					validateCurrentRow(selectedIndex, selectedRow, currSelectedRow, false, null);
				},
				onRowSelected: function(row){
					if(1 === recalcGridData.length) currSelectedRow = row;
				},
				onRowDeselected: function(row){
					if(1 < recalcGridData.length) currSelectedRow = row;
					if(!redrawing) validate = thereWasChange || thereIsError;
				},
				onCellValueChanged: function(params){
					thereWasChange = params.oldValue !== params.newValue;
					if(thereWasChange && (
							params['colDef'] && params['colDef']['field'] === 'suppliedQuantity')){
						updateBatchData(params['rowIndex'],params.oldValue,params.newValue);
					}
				}
		};
		
		$scope.$parent.validateHighlighted = function(cb){
			if(currSelectedRow){
				var selectNode = currSelectedRow['node'];
				var selectedIndex = selectNode['id'];
				var selectedRow = selectNode['data'];
				validateCurrentRow(selectedIndex, selectedRow, currSelectedRow, true, cb);
			}else{
				var selectedNode = $scope.gridOptions.api.getSelectedNodesById()[0];
				if(selectedNode){
					var selectedIndex = selectedNode['id'];
					var selectedRow = selectedNode['data'];
					var currentSelectedRow = {
						node: selectedNode
					}
					validateCurrentRow(selectedIndex, selectedRow, currentSelectedRow, true, cb);
				}else{
					cb(false,"Select batch to ensure validation");
				}
			}
		}
		
		function validateCurrentRow(selectedIndex, selectedRow, currentRow, forceValidate, cb){
			var previouslySelectedRow = currentRow ? currentRow['node']['data'] : null;
			var prevBatchNumber = previouslySelectedRow  ? previouslySelectedRow['batchNumber'] : null;
			
			if(((prevBatchNumber && selectedRow['batchNumber'] !== prevBatchNumber) && validate) || forceValidate){
				$scope.$parent.notifyLoading();
				
				thereWasChange = false;
				validate = false;
				thereIsError = false;
				var nodeData = previouslySelectedRow;
				
				disableRequiredComponents();
				console.log('nodeData.originalPhysicalStock: '+nodeData.originalPhysicalStock)
				$scope.availableSupplyGroup.saveSuppliedItem(
					parseInt(nodeData.previousSuppliedQuantity),
					{
						sourceBatchNumber: nodeData.batchNumber,
						suppliedQuantity: nodeData.suppliedQuantity,
						remarks: nodeData.remarks
					}, function(err){
						var msg = null;
						if(err){
							var batchNumber = Object.keys(err.data)[0];
							var erroneousSuppliedItem = batchNumber;
							$scope.gridOptions.api.selectIndex(currentRow['node']['id'], true);
							thereIsError = true;
							msg = err.data;
						}else{
							$scope.gridOptions.api.selectIndex(selectedIndex, true);
							thereIsError = false;
						}
						enableRequiredComponents(thereIsError,msg);
						$scope.$parent.notifyDoneLoading();
						if(cb) cb(!thereIsError,msg);
				})
			}
		}
		
		function disableRequiredComponents(){
			editable = false;
			$scope.gridOptions.api.setColumnDefs(columnDefs);
			$scope.gridOptions.api.sizeColumnsToFit();
			$scope.disablePersist(true);
		}
		
		function enableRequiredComponents(hasError, msg){
			editable = true;
			redrawing = true;
			$scope.gridOptions.api.setColumnDefs(columnDefs);
			$scope.gridOptions.api.sizeColumnsToFit();
			$scope.disablePersist(hasError, msg);
			redrawing = false;
		}
		
		$scope.$watch('refresh', function(nv,ov){
			if(nv){
				$scope.list(function (data) {
					recalcGridData = data;
					$scope.gridOptions.api.setDatasource(dataSource);
					$scope.gridOptions.api.sizeColumnsToFit();
				});
				$scope.refresh = false;
			}
		});
		
		function updateBatchData(rowId, oldValue, newValue){
			if(recalcGridData && recalcGridData.length > 0){
				var gridData = recalcGridData[rowId]
				
				var trimmedPhysicalStockStr = String(gridData.physicalStock).replace (/,/g, "");
				var newPhysicalStock = parseInt(trimmedPhysicalStockStr) + 
					(parseInt(oldValue) - parseInt(newValue))
				gridData.physicalStock = newPhysicalStock;
				
				$scope.gridOptions.api.setColumnDefs(columnDefs);
				$scope.gridOptions.api.sizeColumnsToFit();
			}
		}
		
		$scope.disablePersist(false);

		transclude($scope, function(clone, scope) {
			element.find('.insertion-point').append(clone);
		});
		
	}
    var directive = {};
    
    directive.restrict = 'E';
    directive.transclude = true;
    directive.templateUrl = '/NSIS/static/view/ui-routed/grids/wh-request-issuance-issue-supply-by-fifo-batch.html';
    
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'default-nsis-grid'; }
        if (!attrs.augment) {attrs.augment = true;}

        return linkFunction;
    }
    
    directive.scope = {
    		list: '=listAction',
    		disablePersist: '=disablePersistFx',
    		refresh:'=refreshTrigger',
    		availableSupplyGroup: '=supplyGroup',
    		classIds: '@',
    		label: '@',
    		textLabel: '@'
    }

    return directive;
}])
.directive('nsisGridCdWhImOnhandByItem', function() {
	var recalcGridData = [];
	var linkFunction = function($scope, element, atttributes, ctrl, transclude){
		/********************************Grid related*******************************/
		var columnDefs = [
		                  {headerName: "Item Code", field: "itemCode", suppressMenu:true},
		                  {headerName: "Description", field: "description", suppressMenu:true},
		                  {headerName: "UOM", field: "uom", suppressMenu:true},
		                  {headerName: "Phy. Stock", field: "physicalStock",cellStyle: {'text-align': 'center'}, suppressMenu:true},
		                  {headerName: "Amount in PHP", field: "totalAmountInPHP",cellStyle: {'text-align': 'right'}, suppressMenu:true},
		                  {headerName: "Amount in SGD", field: "totalAmountInSGD",cellStyle: {'text-align': 'right'}, suppressMenu:true}
		                  ];

		var dataSource = {
				pageSize: 10,
				getRows: function (params) {
					var parameters = {/*type: 'Labels', */rows: 10, rowOffset: params.startRow};
					if(params.sortModel && params.sortModel.length > 0){
						var sortModel = params.sortModel[0];
						parameters.sidx = sortModel.colId;
						parameters.sord = sortModel.sort;
					}
					$scope.list(parameters, function (data) {
						params.successCallback(data.rows, data.records);
						if ($scope.gridOptions) {
							$scope.gridOptions.api.refreshView();
							$scope.gridOptions.api.sizeColumnsToFit();
						}
					});
				}
		};

		$scope.gridOptions = {
				enableServerSideSorting: true,
				enableFilter: true,
				enableColResize: true,
				columnDefs: columnDefs,
				rowSelection: 'single',
				onGridReady: function (api) {
					$scope.gridOptions.api.setDatasource(dataSource);
					$scope.gridOptions.api.sizeColumnsToFit();
				}
		};
		
		$scope.$watch(
	    		'triggerReload',
	    		function(newValue, oldValue) {
	    			if(newValue) {
	    				$scope.gridOptions.api.setDatasource(dataSource);
						$scope.gridOptions.api.sizeColumnsToFit();
	    			}
	    		}
	    );
		
		transclude($scope, function(clone, scope) {
			element.find('.insertion-point').append(clone);
		});
		
	}
    var directive = {};
    
    directive.restrict = 'E';
    directive.transclude = true;
    directive.templateUrl = '/NSIS/static/view/ui-routed/grids/wh-im-onhand-by-item.html';
    
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'default-nsis-grid'; }
        return linkFunction;
    }
    
    directive.scope = {
    	list: '=listAction',
    	triggerReload: '=trigger',
        classIds: '@'
    }

    return directive;
})
.directive('nsisGridCdWhImOnhandByBatch', function() {
	var recalcGridData = [];
	var linkFunction = function($scope, element, atttributes, ctrl, transclude){
		/********************************Grid related*******************************/
		var columnDefs = [
		                  {headerName: "Batch Number", field: "batchNumber", suppressMenu:true, width: 340},
		                  {headerName: "Item Code", field: "itemCode", suppressMenu:true, width: 170},
		                  {headerName: "Description", field: "description", suppressMenu:true, width: 370},
		                  {headerName: "Rack/Shelf", field: "rackLocation",cellStyle: {'text-align': 'center'}, suppressMenu:true, width: 170},
		                  {headerName: "UOM", field: "uom", suppressMenu:true, width: 100},
		                  {headerName: "Phy. Stock", field: "physicalStock",cellStyle: {'text-align': 'right'}, suppressMenu:true, width: 160},
		                  {headerName: "Amount in PHP", field: "totalAmountInPHP",cellStyle: {'text-align': 'right'}, suppressMenu:true, width: 180},
		                  {headerName: "Amount in SGD", field: "totalAmountInSGD",cellStyle: {'text-align': 'right'}, suppressMenu:true, width: 180}
		                  ];

		var dataSource = {
				pageSize: 10,
				getRows: function (params) {
					var parameters = {/*type: 'Labels', */rows: 10, rowOffset: params.startRow};
					if(params.sortModel && params.sortModel.length > 0){
						var sortModel = params.sortModel[0];
						parameters.sidx = sortModel.colId;
						parameters.sord = sortModel.sort;
					}
					$scope.list(parameters, function (data) {
						params.successCallback(data.rows, data.records);
						if ($scope.gridOptions) {
							$scope.gridOptions.api.refreshView();
							$scope.gridOptions.api.sizeColumnsToFit();
						}
					});
				}
		};

		$scope.gridOptions = {
				enableServerSideSorting: true,
				enableFilter: true,
				enableColResize: true,
				columnDefs: columnDefs,
				rowSelection: 'single',
				onGridReady: function (api) {
					$scope.gridOptions.api.setDatasource(dataSource);
					$scope.gridOptions.api.sizeColumnsToFit();
				}
		};
		
		$scope.$watch(
	    		'triggerReload',
	    		function(newValue, oldValue) {
	    			if(newValue) {
	    				$scope.gridOptions.api.setDatasource(dataSource);
						$scope.gridOptions.api.sizeColumnsToFit();
	    			}
	    		}
	    );
		
		transclude($scope, function(clone, scope) {
			element.find('.insertion-point').append(clone);
		});
		
	}
    var directive = {};
    
    directive.restrict = 'E';
    directive.transclude = true;
    directive.templateUrl = '/NSIS/static/view/ui-routed/grids/wh-im-onhand-by-batch.html';
    
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'default-nsis-grid'; }
        return linkFunction;
    }
    
    directive.scope = {
    	list: '=listAction',
    	triggerReload: '=trigger',
        classIds: '@'
    }

    return directive;
})