/**
 * 
 */
angular.module('nsisSvcDomainServices', ['ngResource','ngCookies','nsisSvcServices'])
.factory('SvcSupplyRequest', ["$resource","SvcSupplyRequestService",function($resource,SvcSupplyRequestService) {

	var SvcSupplyRequest = $resource(mainUrl+'facilities/:facilityCodeName/supplyrequests/:requestNumber', {requestNumber:'@_reqNumber'},
			{
				recalculateItems: {
					method: 'POST',
					params: {facilityCodeName:'@facilityCodeName'},
					url: mainUrl+'facilities/:facilityCodeName/supplyrequests/supplyrequests/',
					headers: {'Content-Type':'application/json','xAction':'CALCULATE'}, 
					isArray:true
				},
				get: {
		            method: 'GET',
		            transformResponse: transFxForOR
		        },
				update:{method:'PUT'},
				getTemplate: {
					method: 'POST',
					params: {facilityCodeName:'@facilityCodeName'},
					url: mainUrl+'facilities/:facilityCodeName/supplyrequests/supplyrequests/',
					headers: {'Content-Type':'application/json','xAction':'TEMPLATE'}
				},
				create:{
					method:'POST',
					params: {facilityCodeName:'@facilityCodeName'},
					url: mainUrl+'facilities/:facilityCodeName/supplyrequests/supplyrequests/',
					headers: {'Content-Type':'application/json','xAction':'CREATE'}
				},
		        submit: {
		        	method: 'POST',
		        	params: {facilityCodeName:'@facilityCodeName'},
					url: mainUrl+'facilities/:facilityCodeName/supplyrequests/supplyrequests/',
					headers: {'Content-Type':'application/json','xAction':'SUBMIT'}
		        },
		        approve: {
		        	method: 'POST',
		        	params: {facilityCodeName:'@facilityCodeName'},
					url: mainUrl+'facilities/:facilityCodeName/supplyrequests/supplyrequests/',
					headers: {'Content-Type':'application/json','xAction':'APPROVE'}
		        },
		        reject: {
		        	method: 'POST',
		        	params: {facilityCodeName:'@facilityCodeName'},
					url: mainUrl+'facilities/:facilityCodeName/supplyrequests/supplyrequests/',
					headers: {'Content-Type':'application/json','xAction':'REJECT'}
		        }
			}
	);
	
	function transFxForOR(data, headers){
		var dataFromJSON = angular.fromJson(data);
		
    	if(!!(dataFromJSON && !(dataFromJSON.rows))){
    		dataFromJSON = SvcSupplyRequestService.sanitizesFRQRO(dataFromJSON)
    	}
    		
        return dataFromJSON;
	}
	
	SvcSupplyRequest.prototype ={
		showRecalculatedItems: function(cb){
			var currentOR = this.body;
			if(!currentOR) currentOR = this;
			var supplyRequestCO = SvcSupplyRequestService.generateSupplyRequestCO(currentOR);
			SvcSupplyRequest.recalculateItems({},supplyRequestCO,function(result){
				if(result)cb(result);
			});
		},
		saveRequestedItem: function(requestedItem,mode,cb){
			var thisRef = this;
			var existingRequestedItem = thisRef.body.itemsRequested.find(function(item){
				return item.itemCode === requestedItem.itemCode;
			});
			
			var getQuantity = function(prevQuantity, quantity){
				return 'ADD' === mode ? 
						parseInt(prevQuantity) + parseInt(quantity) : 
						parseInt(quantity);
			}
			
			if(existingRequestedItem){
				var thereIsChange = existingRequestedItem.quantity !== requestedItem.quantity || 
									existingRequestedItem.remarks !== requestedItem.remarks;
				
				existingRequestedItem.quantity = getQuantity(existingRequestedItem.quantity, requestedItem.quantity);
				existingRequestedItem.remarks = requestedItem.remarks;
				
				cb(thereIsChange);
			}else{
				var newRequestedItem = angular.copy(requestedItem);
				newRequestedItem.quantity = parseInt(newRequestedItem.quantity);
				thisRef.body.itemsRequested.push(newRequestedItem);
				cb(true);
			}
		},
		save: function(cb){
			var currentOR = this.body;
			if(!currentOR) currentOR = this;
			var supplyRequestCO = SvcSupplyRequestService.generateSupplyRequestCO(currentOR);
			if('NEW'===currentOR.status){
				SvcSupplyRequest.create({},supplyRequestCO,function(result){
					if(cb && result){
						cb(new SvcSupplyRequest(SvcSupplyRequestService.sanitizesFRQRO(result)));
					}
				},function(err){
					cb(null,err)
				});
			}else if('DRAFT'===currentOR.status){
				SvcSupplyRequest.update(
					{
						requestNumber:this.body.requestNumber,
						facilityCodeName:supplyRequestCO.facilityCodeName
					},
					supplyRequestCO,function(result){
					if(cb && result){
						cb(new SvcSupplyRequest(SvcSupplyRequestService.sanitizesFRQRO(result)));
					}
				},function(err){
					cb(null,err)
				});
			}
		},
		submit: function(cb){
			var supplyRequestCO = SvcSupplyRequestService.generateSupplyRequestCO(this.body);
			console.log('supplyRequestCO: '+supplyRequestCO)
			SvcSupplyRequest.submit({},supplyRequestCO,function(result){
				if(cb && result){
					cb(new SvcSupplyRequest(SvcSupplyRequestService.sanitizesFRQRO(result)));
				}
			},function(err){
				cb(null,err)
			});
		},
		approve: function(cb){
			var supplyRequestCO = SvcSupplyRequestService.generateSupplyRequestCO(this.body);
			console.log('supplyRequestCO: '+supplyRequestCO)
			SvcSupplyRequest.approve({},supplyRequestCO,function(result){
				if(cb && result){
					cb(new SvcSupplyRequest(SvcSupplyRequestService.sanitizesFRQRO(result)));
				}
			},function(err){
				cb(null,err)
			});
		},
		reject: function(cb){
			var supplyRequestCO = SvcSupplyRequestService.generateSupplyRequestCO(this.body);
			console.log('supplyRequestCO: '+supplyRequestCO)
			SvcSupplyRequest.reject({},supplyRequestCO,function(result){
				if(cb && result){
					cb(new SvcSupplyRequest(SvcSupplyRequestService.sanitizesFRQRO(result)));
				}
			},function(err){
				cb(null,err)
			});
		}
	}
	
	return SvcSupplyRequest;
}])
.factory('SupplyRequestItem', ["$resource","SupplyRequestItemService",function($resource,SupplyRequestItemService){
	var SupplyRequestItem = $resource(mainUrl + 'supplyrequests/:requestNumber/items:/itemCode', {requestNumber:'@requestNumber'}, {
		validate: {
			method: 'POST',
			params: {requestNumber:'@requestNumber'},
			url: mainUrl+'supplyrequests/:requestNumber/items/items',
			headers: {'Content-Type':'application/json','xAction':'VALIDATE'}
		}
	})
	
	SupplyRequestItem.prototype = {
		validate: function(cb){
			console.log('validating sri for or: '+this.requestNumber)
			var supplyRequestItemCO = SupplyRequestItemService.generateSupplyRequestItemCO(this);
			SupplyRequestItem.validate({requestNumber:this.requestNumber},supplyRequestItemCO, function(result){
				if(cb) cb(null);
			},function(err){
				cb(null,err);
			})
		}
	}
	
	return SupplyRequestItem;
}])
.factory('SvcDeliveryReceipt', ["$resource","SvcDeliveryReceiptService","NHelper",function($resource,SvcDeliveryReceiptService,NHelper) {

	var SvcDeliveryReceipt = $resource(mainUrl+'facilities/:facilityCodeName/deliveryreceipts/:requestNumber', {requestNumber:'@_reqNumber'},
			{
				get: {
		            method: 'GET',
		            transformResponse: transFxForOR
		        },
				update:{method:'PUT'},
		        finalize: {
		        	method: 'POST',
					url: mainUrl+'facilities/:facilityCodeName/deliveryreceipts/deliveryreceipts',
					headers: {'Content-Type':'application/json','xAction':'FINALIZE'}
		        }
			}
	);
	
	function transFxForOR(data, headers){
		var dataFromJSON = angular.fromJson(data);
		
    	if(!!(dataFromJSON && !(dataFromJSON.rows))){
    		dataFromJSON = SvcDeliveryReceiptService.sanitizeOrQRO(dataFromJSON)
    	}
    		
        return dataFromJSON;
	}
	
	SvcDeliveryReceipt.prototype ={
		save: function(cb){
			var currentDR = this.body;
			var deliveryReceiptCO = SvcDeliveryReceiptService.generateCO(currentDR);
			
			SvcDeliveryReceipt.update(
				{
					requestNumber:deliveryReceiptCO.requestNumber,
					facilityCodeName: currentDR.supplyRequest.facilityCodeName
				},
				deliveryReceiptCO,
				function(result){
					if(cb && result){
						cb(new SvcDeliveryReceipt(SvcDeliveryReceiptService.sanitizeOrQRO(result)));
					}
				},
				function(err){
					cb(null,err)
				});
		},
		finalize: function(cb){
			var currentDR = this.body;
			var deliveryReceiptCO = SvcDeliveryReceiptService.generateCO(currentDR);
			console.log('deliveryReceiptCO: '+deliveryReceiptCO.itemBatchGroups)
			SvcDeliveryReceipt.finalize(
					{facilityCodeName: currentDR.supplyRequest.facilityCodeName},
					deliveryReceiptCO,
					function(result){
						if(cb && result){
							cb(new SvcDeliveryReceipt(SvcDeliveryReceiptService.sanitizeOrQRO(result)));
						}
					},
					function(err){
						cb(null,err)
					});
		},
		updateItemBatchGroupList: function(deliveredItemBatchGroup, cb){
			var itemCode = deliveredItemBatchGroup.receivableDetails.itemCode;
			var thisRef = this.body;
			var batchGroup = thisRef.itemBatches[itemCode];
			NHelper.ifPresent(batchGroup, function(group){
				var itemBatch = SvcDeliveryReceiptService.sanitizeEditedItemBatchGroup(deliveredItemBatchGroup);
				thisRef.itemBatches[itemCode] = itemBatch;
			})
			
			if(cb)cb();
		}
	}
	
	return SvcDeliveryReceipt;
}])
.factory('SvcDeliveredItemBatchGroup', ["$resource","NHelper","SvcDeliveredItemBatchGroupService",function($resource,NHelper,SvcDeliveredItemBatchGroupService){
	var SvcDeliveredItemBatchGroup = $resource(mainUrl + 'facilities/:facilityCodeName/deliveryreceipts/:requestNumber/itembatchgroups/:itemCode', 
			{requestNumber:'@_reqNumber'}, 
			{
				validate: {
					method: 'POST',
					params: {facilityCodeName:'@facilityCodeName', requestNumber: '@requestNumber'},
					url: mainUrl+'facilities/:facilityCodeName/deliveryreceipts/:requestNumber/itembatchgroups/itembatchgroups/',
					headers: {'Content-Type':'application/json','xAction':'VALIDATE'}
				},
				recalculateBatches: {
					method: 'POST',
					params: {facilityCodeName:'@facilityCodeName', requestNumber: '@requestNumber'},
					url: mainUrl+'facilities/:facilityCodeName/deliveryreceipts/:requestNumber/itembatchgroups/itembatchgroups/',
					headers: {'Content-Type':'application/json','xAction':'CALCULATE'}, 
					isArray:true
				}
			})
	
	SvcDeliveredItemBatchGroup.prototype = {
		validate: function(isNew,cb){
			var deliveredItemBatchGroupCO = SvcDeliveredItemBatchGroupService.generateValidationCO(this,isNew);
			var myReference = this;
			SvcDeliveredItemBatchGroup.validate(
					{
						requestNumber:this.requestDetails.requestNumber,
						facilityCodeName: this.requestDetails.facilityCodeName
					},
					deliveredItemBatchGroupCO, 
					function(result){
						if(cb) cb(myReference,null);
					},function(err){
						cb(null,err);
					})
		},
		recalculateItemBatches: function(cb){
			var deliveredItemBatchCO = SvcDeliveredItemBatchGroupService.generateCO(this);
			SvcDeliveredItemBatchGroup.recalculateBatches(
					{
						requestNumber:this.requestDetails.requestNumber,
						facilityCodeName: this.requestDetails.facilityCodeName
					},
					deliveredItemBatchCO,
					function(result){
						if(result)cb(result);
					});
		},
		removeItemBatch: function(batchNumber){
			this.itemBatches = this.itemBatches.filter(function(batch){
				return batchNumber !== batch.batchNumber;
			});
		},
		saveItemBatch: function(isNew, successCb, errCb){
			var thisRef = this;
			thisRef.validate(isNew,function(result, err){
				if(err) errCb(err);
				else {
					var itemBatch = thisRef.itemBatches.find(function(batch){
						return thisRef.editedItemBatch.batchNumber === batch.batchNumber;
					});
					var thereIsChange = false;
					if(itemBatch){
						var updatedItemBatch = angular.copy(result.editedItemBatch);
						thereIsChange = parseInt(itemBatch.quantity) !== parseInt(updatedItemBatch.quantity);
						var editQuantity = parseInt(updatedItemBatch.quantity);
						if(thereIsChange){
							itemBatch.quantity = editQuantity;
						}else if(isNew){
							thereIsChange = true;
							itemBatch.quantity = parseInt(itemBatch.quantity) + editQuantity;
						}
					}else{
						var newItemBatch = angular.copy(result.editedItemBatch);
						newItemBatch = SvcDeliveredItemBatchGroupService.sanitizeItemBatch(newItemBatch);
						result.itemBatches.push(newItemBatch);
						thereIsChange = true;
					}

					thisRef.editedItemBatch = {};
					successCb(thereIsChange);
				}
			})
		}
	}
	
	return SvcDeliveredItemBatchGroup;
}])
.factory('SvcRequestIssuance', ["$resource","SvcRequestIssuanceService",function($resource,SvcRequestIssuanceService) {

	var SvcRequestIssuance = $resource(mainUrl+'facilities/:facilityCodeName/requestissuances/:requestNumber', {requestNumber:'@_reqNumber'},
			{
				get: {
		            method: 'GET',
		            transformResponse: transFxForOR
		        },
				update:{method:'PUT'},
				process:{
					method: 'POST',
					params: {facilityCodeName:'@facilityCodeName'},
					url: mainUrl+'facilities/:facilityCodeName/requestissuances/requestissuances/',
					headers: {'Content-Type':'application/json','xAction':'PROCESS'}
				},
		        finalize: {
		        	method: 'POST',
		        	params: {facilityCodeName:'@facilityCodeName'},
					url: mainUrl+'facilities/:facilityCodeName/requestissuances/requestissuances/',
					headers: {'Content-Type':'application/json','xAction':'FINALIZE'}
		        },
		        reject: {
		        	method: 'POST',
		        	params: {facilityCodeName:'@facilityCodeName'},
					url: mainUrl+'facilities/:facilityCodeName/requestissuances/requestissuances/',
					headers: {'Content-Type':'application/json','xAction':'REJECT'}
		        }
			}
	);
	
	function transFxForOR(data, headers){
		var dataFromJSON = angular.fromJson(data);
		
    	if(!!(dataFromJSON && !(dataFromJSON.rows))){
    		dataFromJSON = SvcRequestIssuanceService.sanitizeOrQRO(dataFromJSON)
    	}
    		
        return dataFromJSON;
	}
	
	SvcRequestIssuance.prototype ={
		findSuppliedItemsGrp: function(itemCode){
			var thisRef = this.body;
			var specfiedItemIssuanceGrp = thisRef.requestedItems.find(function(ri){
				return ri.itemCode === itemCode;
			})
			return {
				itemCode: specfiedItemIssuanceGrp.itemCode,
				suppliedItems: angular.copy(specfiedItemIssuanceGrp.suppliedItems)
			};
		},
		saveSuppliedItemsGrp: function(itemIssuanceGroup){
			var thisRef = this.body;
			var specfiedItemIssuanceGrp = thisRef.requestedItems.find(function(ri){
				return ri.itemCode === itemIssuanceGroup.itemCode;
			})
			
			var suppliedItemsGroup = 
				SvcRequestIssuanceService.sanitizeSuppliedItemsGroup(itemIssuanceGroup);
			var clonedCopy = {
					itemCode: suppliedItemsGroup.itemCode,
					suppliedItems: angular.copy(suppliedItemsGroup.suppliedItems)
			}
			
			if(specfiedItemIssuanceGrp){
				specfiedItemIssuanceGrp.itemCode = clonedCopy.itemCode;
				specfiedItemIssuanceGrp.suppliedItems = clonedCopy.suppliedItems;
			}else{
				thisRef.requestedItems.push(clonedCopy)
			}
		},
		save: function(successHandler, errorHandler){
			var currentOR = this.body;
			if(!currentOR) currentOR = this;
			var requestIssuanceCO = SvcRequestIssuanceService.generateCO(currentOR);
			
			SvcRequestIssuance.update(
				{
					requestNumber:this.body.requestNumber,
					facilityCodeName: this.body.facilityCodeName
				},requestIssuanceCO,
				function(result){
					if(successHandler && result){
						var rerouted = requestIssuanceCO.facilityCode !== requestIssuanceCO.previousFacilityCode;
						successHandler(new SvcRequestIssuance(SvcRequestIssuanceService.sanitizeOrQRO(result)),rerouted);
					}
				},
				errorHandler
			);
		},
		print: function(successHandler, errorHandler){
			var requestIssuanceCO = SvcRequestIssuanceService.generateCO(this.body);
			console.log('requestIssuanceCO: '+requestIssuanceCO)
			SvcRequestIssuance.process({
				facilityCodeName: this.body.facilityCodeName
			},requestIssuanceCO,function(result){
				if(successHandler && result){
					successHandler(new SvcRequestIssuance(SvcRequestIssuanceService.sanitizeOrQRO(result)));
				}
			},errorHandler);
			
			//TODO add print action after process post request
		},
		finalize: function(successHandler, errorHandler){
			var requestIssuanceCO = SvcRequestIssuanceService.generateCO(this.body);
			SvcRequestIssuance.finalize({
				facilityCodeName: this.body.facilityCodeName
			},requestIssuanceCO,function(result){
				if(successHandler && result){
					successHandler(new SvcRequestIssuance(SvcRequestIssuanceService.sanitizeOrQRO(result)));
				}
			},errorHandler);
		},
		reject: function(successHandler, errorHandler){
			var requestIssuanceCO = SvcRequestIssuanceService.generateCO(this.body);
			SvcRequestIssuance.reject({
				facilityCodeName: this.body.facilityCodeName
			},requestIssuanceCO,function(result){
				if(successHandler && result){
					successHandler(new SvcRequestIssuance(SvcRequestIssuanceService.sanitizeOrQRO(result)));
				}
			},errorHandler);
		}
	}
	
	return SvcRequestIssuance;
}])
.factory('SvcRequestedItemIssuance', ["$resource","SvcRequestedItemIssuanceService",function($resource,SvcRequestedItemIssuanceService) {

	var SvcRequestedItemIssuance = $resource(
			mainUrl+'facilities/:facilityCodeName/requestissuances/:requestNumber/supplieditemsgroup/:itemCode', 
			{requestNumber:'@_reqNumber', itemCode:'@_itemCd', facilityCodeName: '@_facilityCodeName'},
			{
				validate:{
					method: 'POST',
					url: mainUrl+'facilities/:facilityCodeName/requestissuances/:requestNumber/supplieditemsgroup/supplieditemsgroup',
					headers: {'Content-Type':'application/json','xAction':'VALIDATE'}
				},
		        listSupplies: {
		        	method: 'POST',
					url: mainUrl+'facilities/:facilityCodeName/requestissuances/:requestNumber/supplieditemsgroup/supplieditemsgroup',
					headers: {'Content-Type':'application/json','xAction':'LISTSUPPLIES'}
		        }
			}
	);
	
	SvcRequestedItemIssuance.prototype ={
		saveSuppliedItem: function(previousSuppliedQuantity, suppliedItem, cb){
			var thisRef = this;
			thisRef.suppliedItems = SvcRequestedItemIssuanceService.resetEditedList(thisRef.suppliedItems);
			
			var indexOfMatch;
			var suppliedItemRec = thisRef.suppliedItems.find(function(si, index){
				indexOfMatch = index;
				return si.sourceBatchNumber === suppliedItem.sourceBatchNumber
			});
			if(0 < suppliedItem.suppliedQuantity){
				if(suppliedItemRec){
					setSuppliedItemRec(suppliedItem.suppliedQuantity, suppliedItem.remarks);
				}else{
					var newSuppliedItemRec = angular.copy(suppliedItem);
					newSuppliedItemRec.currentlyEdited = true;
					thisRef.suppliedItems.push(newSuppliedItemRec);
				}
				thisRef.validate(previousSuppliedQuantity, cb)
			}else if(suppliedItemRec){
				if(-1 < indexOfMatch && !suppliedItem.remarks){
					thisRef.suppliedItems.splice(indexOfMatch, 1);
					cb(null);
				}else if(suppliedItem.remarks){
					setSuppliedItemRec(0, suppliedItem.remarks);
					thisRef.validate(previousSuppliedQuantity, cb);
				}
			}else{
				cb(null)
			}
			
			function setSuppliedItemRec(quantity, remarks){
				suppliedItemRec.suppliedQuantity = quantity;
				suppliedItemRec.remarks = remarks;
				suppliedItemRec.currentlyEdited = true;
			}
		},
		validate: function(previousSuppliedQuantity, cb){
			var body = this.body;
			if(!body) body = this;
			var commandObject = 
				SvcRequestedItemIssuanceService.generateValidationCO(previousSuppliedQuantity, body);
			
			SvcRequestedItemIssuance.validate(
				{
					requestNumber:body.requestDetails.requestNumber,
					facilityCodeName: body.requestDetails.facilityCodeName
				},commandObject,function(result){
					if(cb && result){
						cb(null);
					}
				},function(err){
					cb(err)
				}
			);
		},
		listAvailableSupplies: function(cb){
			var body = this.body;
			if(!body) body = this;
			var commandObject = SvcRequestedItemIssuanceService.generateCO(body);
			SvcRequestedItemIssuance.listSupplies(
					{
						requestNumber:body.requestDetails.requestNumber,
						facilityCodeName: body.requestDetails.facilityCodeName
					},
					commandObject,
					function(result){
						if(cb && result){
							cb(result);
						}
					},function(err){
						cb(null,err)
					}
			);
		}
	}
	
	return SvcRequestedItemIssuance;
}])
.factory('SvcOnHandItemBatchesPhysicalCount', ["$resource",function($resource) {

	var OnHandSupplyItems = $resource(
			mainUrl+'facilities/:facilityCodeName/onhandsupplyitems', 
			{facilityCodeName:'@facilityCodeName'},
			{}
	);
	
	return OnHandSupplyItems;
}])
.factory('SvcPhysicalStockCountRequest', ["$resource","SvcPhysicalStockCountRequestService",function($resource,SvcPhysicalStockCountRequestService) {

	var SvcPhysicalStockCountRequest = $resource(mainUrl+'facilities/:facilityCodeName/physicalstockcountrequest/:requestNumber', {requestNumber:'@_reqNumber'},
			{
				getBatchDetails: {
					method: 'POST',
					params: {facilityCodeName:'@facilityCodeName'},
					url: mainUrl+'facilities/:facilityCodeName/physicalstockcountrequest/physicalstockcountrequest/',
					headers: {'Content-Type':'application/json','xAction':'CALCULATE'}, 
					isArray:true
				},
				get: {
		            method: 'GET',
		            transformResponse: transFxForOR
		        },
				update:{method:'PUT'},
				getTemplate: {
					method: 'POST',
					params: {facilityCodeName:'@facilityCodeName'},
					url: mainUrl+'facilities/:facilityCodeName/physicalstockcountrequest/physicalstockcountrequest/',
					headers: {'Content-Type':'application/json','xAction':'TEMPLATE'}
				},
				create:{
					method:'POST',
					params: {facilityCodeName:'@facilityCodeName'},
					url: mainUrl+'facilities/:facilityCodeName/physicalstockcountrequest/physicalstockcountrequest/',
					headers: {'Content-Type':'application/json','xAction':'CREATE'}
				},
		        submit: {
		        	method: 'POST',
		        	params: {facilityCodeName:'@facilityCodeName'},
					url: mainUrl+'facilities/:facilityCodeName/physicalstockcountrequest/physicalstockcountrequest/',
					headers: {'Content-Type':'application/json','xAction':'SUBMIT'}
		        },
		        approve: {
		        	method: 'POST',
		        	params: {facilityCodeName:'@facilityCodeName'},
					url: mainUrl+'facilities/:facilityCodeName/physicalstockcountrequest/physicalstockcountrequest/',
					headers: {'Content-Type':'application/json','xAction':'APPROVE'}
		        },
		        reject: {
		        	method: 'POST',
		        	params: {facilityCodeName:'@facilityCodeName'},
					url: mainUrl+'facilities/:facilityCodeName/physicalstockcountrequest/physicalstockcountrequest/',
					headers: {'Content-Type':'application/json','xAction':'REJECT'}
		        }
			}
	);
	
	function transFxForOR(data, headers){
		var dataFromJSON = angular.fromJson(data);
		
    	if(!!(dataFromJSON && !(dataFromJSON.rows))){
    		dataFromJSON = SvcPhysicalStockCountRequestService.sanitizePSCRQRO(dataFromJSON)
    	}
    		
        return dataFromJSON;
	}
	
	SvcPhysicalStockCountRequest.prototype ={
		showRecalculatedItems: function(cb){
			var currentOR = this.body;
			if(!currentOR) currentOR = this;
			var physicalStockCountReqCO = SvcPhysicalStockCountRequestService.generateSupplyRequestCO(currentOR);
			SvcPhysicalStockCountRequest.recalculateItems({},physicalStockCountReqCO,function(result){
				if(result)cb(result);
			});
		},
		saveRequestedItem: function(requestedItem,mode,cb){
			var thisRef = this;
			var existingRequestedItem = thisRef.body.itemsRequested.find(function(item){
				return item.itemCode === requestedItem.itemCode;
			});
			
			var getQuantity = function(prevQuantity, quantity){
				return 'ADD' === mode ? 
						parseInt(prevQuantity) + parseInt(quantity) : 
						parseInt(quantity);
			}
			
			if(existingRequestedItem){
				var thereIsChange = existingRequestedItem.quantity !== requestedItem.quantity || 
									existingRequestedItem.remarks !== requestedItem.remarks;
				
				existingRequestedItem.quantity = getQuantity(existingRequestedItem.quantity, requestedItem.quantity);
				existingRequestedItem.remarks = requestedItem.remarks;
				
				cb(thereIsChange);
			}else{
				var newRequestedItem = angular.copy(requestedItem);
				newRequestedItem.quantity = parseInt(newRequestedItem.quantity);
				thisRef.body.itemsRequested.push(newRequestedItem);
				cb(true);
			}
		},
		save: function(cb){
			var currentOR = this.body;
			if(!currentOR) currentOR = this;
			var physicalStockCountReqCO = SvcPhysicalStockCountRequestService.generateSupplyRequestCO(currentOR);
			if('NEW'===currentOR.status){
				SvcPhysicalStockCountRequest.create({},physicalStockCountReqCO,function(result){
					if(cb && result){
						cb(new SvcPhysicalStockCountRequest(SvcPhysicalStockCountRequestService.sanitizePSCRQRO(result)));
					}
				},function(err){
					cb(null,err)
				});
			}else if('DRAFT'===currentOR.status){
				SvcPhysicalStockCountRequest.update(
					{
						requestNumber:this.body.requestNumber,
						facilityCodeName:physicalStockCountReqCO.facilityCodeName
					},
					physicalStockCountReqCO,function(result){
					if(cb && result){
						cb(new SvcPhysicalStockCountRequest(SvcPhysicalStockCountRequestService.sanitizePSCRQRO(result)));
					}
				},function(err){
					cb(null,err)
				});
			}
		},
		submit: function(cb){
			var physicalStockCountReqCO = SvcPhysicalStockCountRequestService.generateSupplyRequestCO(this.body);
			SvcPhysicalStockCountRequest.submit({},physicalStockCountReqCO,function(result){
				if(cb && result){
					cb(new SvcPhysicalStockCountRequest(SvcPhysicalStockCountRequestService.sanitizePSCRQRO(result)));
				}
			},function(err){
				cb(null,err)
			});
		},
		approve: function(cb){
			var physicalStockCountReqCO = SvcPhysicalStockCountRequestService.generateSupplyRequestCO(this.body);
			SvcPhysicalStockCountRequest.approve({},physicalStockCountReqCO,function(result){
				if(cb && result){
					cb(new SvcPhysicalStockCountRequest(SvcPhysicalStockCountRequestService.sanitizePSCRQRO(result)));
				}
			},function(err){
				cb(null,err)
			});
		},
		reject: function(cb){
			var physicalStockCountReqCO = SvcPhysicalStockCountRequestService.generateSupplyRequestCO(this.body);
			SvcPhysicalStockCountRequest.reject({},physicalStockCountReqCO,function(result){
				if(cb && result){
					cb(new SvcPhysicalStockCountRequest(SvcPhysicalStockCountRequestService.sanitizePSCRQRO(result)));
				}
			},function(err){
				cb(null,err)
			});
		}
	}
	
	return SvcPhysicalStockCountRequest;
}]);