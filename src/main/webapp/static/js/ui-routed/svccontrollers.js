/**
 * 
 */
nsisApp.controller('SvcSupplyRequestsCtrl',
		['$scope','$rootScope','$state','spinnerService','Prompter','DateUtil','SvcSupplyRequest','svcAssignments','message',
		 function($scope,$rootScope,$state,spinnerService,Prompter,DateUtil,SvcSupplyRequest,svcAssignments, message){
			
			function doClearFilter(){
				$scope.filter.status = null;
				$scope.filter.toDate = null;
				$scope.filter.fromDate = null;
				$scope.filter.requestNumber = null;
			}
			
			var initDone = false;
			function init(cb, filter, actualCB){
				$scope.filter = {};
				doClearFilter()
				initDone = true;
				cb(filter, actualCB);
			}
			
			function actualReloadList(filter, cb){
				var scopeFilter = $scope.filter;
				spinnerService.show("nsisSpinner");
				filter.facilityCodeName = svcAssignments[0].facilityCodeName
				if(scopeFilter.toDate)
					filter.toDate = DateUtil.toStringIgnoreTZ(scopeFilter.toDate);
				if(scopeFilter.fromDate){
					filter.fromDate = DateUtil.toStringIgnoreTZ(scopeFilter.fromDate);
				} 
				filter.status = scopeFilter.status;
				filter.reqNumber = scopeFilter.requestNumber;
				SvcSupplyRequest.get(filter,function(data){
					spinnerService.hide("nsisSpinner");
					cb(data)
				});
				$scope.triggerReload = false;
			}
			
			$scope.reloadList = function(filter, cb){
				if(!initDone){
					init(actualReloadList, filter, cb)
				}else
					actualReloadList(filter, cb);
			}
			
			$scope.clearFilter = function(){
				doClearFilter();
				$scope.triggerReload = true;
				$scope.$digest();
			}
			
			$scope.remove = function(requestNumber, cb){
				if(requestNumber){
					spinnerService.show("nsisSpinner");
					SvcSupplyRequest.remove(
							{
								requestNumber:requestNumber,
								facilityCodeName:svcAssignments[0].facilityCodeName
							},
							function(){
								finalizeAction('Successfully removed request: '+requestNumber,'success');
								cb(true);
							},
							function(err){
								console.log(err)
								var errMsg = 'Failed to remove request: '+requestNumber + '. Reason is: ' + err.data;
								finalizeAction(errMsg,'danger');
								cb(false);
							}
					)
				}else{
					var errMsg = 'Cannot do action. Select first a request.';
					Prompter.promptActionState(errMsg,'danger');
					cb(false);
				}
			}
			
			$scope.view = function(requestNumber){
				spinnerService.show("nsisSpinner");
				SvcSupplyRequest.get(
						{
							requestNumber:requestNumber,
							facilityCodeName:svcAssignments[0].facilityCodeName
						},
						{}, 
						function(supplyRequest){
							spinnerService.hide("nsisSpinner");
							$state.go('home.svcsupplyrequestsform',{supplyRequest: supplyRequest});
						});
			}
			
			$scope.add = function(){
				spinnerService.show("nsisSpinner");
				SvcSupplyRequest.getTemplate({facilityCodeName:svcAssignments[0].facilityCodeName},function(supplyRequest){
					spinnerService.hide("nsisSpinner");
					$state.go('home.svcsupplyrequestsform',{supplyRequest: supplyRequest});
				});
			}
			
			if(message){
				Prompter.promptActionState(message, 'success');
			}
			
			function finalizeAction(msg, mode){
				spinnerService.hide("nsisSpinner");
				Prompter.promptActionState(msg, mode);
			}
			
			$scope.doTriggerReload = function(){
				$scope.triggerReload = true;
			}
		}]
)
.controller('SvcSupplyRequestsFormCtrl',
		['$scope','$rootScope', '$state','spinnerService','Prompter','supplyRequest',
		 function($scope,$rootScope, $state,spinnerService,Prompter,supplyRequest){
			$scope.supplyRequest = supplyRequest;
			$scope.editMeta = supplyRequest.editMeta;
			$scope.notifyLoading = function(){
				spinnerService.show("nsisSpinner");
			}
			$scope.notifyDoneLoading = function(){
				spinnerService.hide("nsisSpinner");
			}
			$scope.cancel = function(){
				//TODO place prompt here specially when its NEW or there are changes
				$state.go('home.svcsupplyrequests');
			}
			
			$scope.save = function(){
				$scope.notifyLoading();
				$scope.supplyRequest.save(function(result,err){
					var msg = '', mode = '';
					if(err){
						$scope.errorMeta = err.data;
						msg = 'Failed to update Request: '+$scope.supplyRequest.body.requestNumber + " due to errors"
						mode = 'danger'
					}else{
						delete $scope['errorMeta'];
						$scope.supplyRequest = result;
						msg = 'Successfully updated Request: '+$scope.supplyRequest.body.requestNumber
						mode = 'success'
					}
					finalizeAction(msg, mode);
				});
			}
			
			$scope.submit = function(){
				$scope.notifyLoading();
				$scope.supplyRequest.submit(function(result,err){
					if(err){
						$scope.errorMeta = err.data;
						var msg = 'Failed to submit Request: '+$scope.supplyRequest.body.requestNumber + " due to errors"
						finalizeAction(msg, 'danger');
					}else{
						delete $scope['errorMeta'];
						$scope.supplyRequest = result;
						$scope.editMeta = result.editMeta;
						var msg = 'Successfully submitted Request: '+$scope.supplyRequest.body.requestNumber
						finalizeUnEditableAction(msg);
					}
				});
			}
			
			function finalizeUnEditableAction(msg){
				$state.go('home.svcsupplyrequests',{message: msg})
			}
			
			function finalizeAction(msg, mode){
				$scope.notifyDoneLoading();
				Prompter.promptActionState(msg, mode);
			}
			
			$scope.submitConfirmMsg = "Are you sure you want to submit this supply request? "+ 
			"You will not be able to edit this request if no errors are found after this action."
		}]
)
.controller('SubmittedSvcSupplyRequestsCtrl',
		['$scope','$rootScope', '$state','spinnerService','Prompter','DateUtil','SvcSupplyRequest','svcAssignments','message',
		 function($scope,$rootScope, $state,spinnerService,Prompter,DateUtil,SvcSupplyRequest,svcAssignments,message){
			
			function doClearFilter(){
				$scope.filter.status = null;
				$scope.filter.toDate = null;
				$scope.filter.fromDate = null;
				$scope.filter.requestNumber = null;
				$scope.filter.facilityCodeName = null;
			}
			
			var initDone = false;
			function init(cb, filter, actualCB){
				$scope.filter = {};
				doClearFilter()
				$scope.filter.status = 'SUBMITTED';
				initDone = true;
				cb(filter, actualCB);
			}
			
			function actualReloadList(filter, cb){
				var scopeFilter = $scope.filter;
				spinnerService.show("nsisSpinner");
				filter.facilityCodeName = svcAssignments[0].facilityCodeName
				filter["statuses"] = ['SUBMITTED','APPROVED','REJECTED']
				
				if(scopeFilter.facilityCodeName)
					filter.facilityCodes = [scopeFilter.facilityCodeName]
				if(scopeFilter.toDate)
					filter.toDate = DateUtil.toStringIgnoreTZ(scopeFilter.toDate);
				if(scopeFilter.fromDate){
					filter.fromDate = DateUtil.toStringIgnoreTZ(scopeFilter.fromDate);
				} 
				filter.status = scopeFilter.status;
				filter.reqNumber = scopeFilter.requestNumber;
				filter.submittedList = true;
				SvcSupplyRequest.get(filter,function(data){
					spinnerService.hide("nsisSpinner");
					cb(data)
				});
				$scope.triggerReload = false;
			}
			
			$scope.reloadList = function(filter, cb){
				if(!initDone){
					init(actualReloadList, filter, cb)
				}else
					actualReloadList(filter, cb);
			}
			
			$scope.clearFilter = function(){
				doClearFilter();
				$scope.triggerReload = true;
				$scope.$digest();
			}
			
			$scope.view = function(requestNumber){
				spinnerService.show("nsisSpinner");
				SvcSupplyRequest.get(
						{
							requestNumber:requestNumber,
							facilityCodeName:svcAssignments[0].facilityCodeName
						},
						{}, 
						function(supplyRequest){
							spinnerService.hide("nsisSpinner");
							$state.go('home.submittedsupplyrequestsform',{supplyRequest: supplyRequest});
						});
			}
			
			if(message){
				Prompter.promptActionState(message, 'success');
			}
			
			$scope.doTriggerReload = function(){
				$scope.triggerReload = true;
			}
		}]
)
.controller('SubmittedSupplyRequestsFormCtrl',
		['$scope','$rootScope', '$state','spinnerService','Prompter','supplyRequest',
		 function($scope,$rootScope, $state,spinnerService,Prompter,supplyRequest){
			$scope.supplyRequest = supplyRequest;
			$scope.editMeta = supplyRequest.editMeta;
			
			$scope.notifyLoading = function(){
				spinnerService.show("nsisSpinner");
			}
			$scope.notifyDoneLoading = function(){
				spinnerService.hide("nsisSpinner");
			}
			$scope.cancel = function(){
				//TODO place prompt here specially when its NEW or there are changes
				$state.go('home.svcsupplyrequests');
			}
			
			$scope.cancel = function(){
				//TODO place prompt here specially when there are changes
				$state.go('home.submittedsvcsupplyrequests');
			}
			
			$scope.approve = function(){
				spinnerService.hide("nsisSpinner");
				$scope.supplyRequest.approve(function(result,err){
					if(err){
						console.log(err)
						$scope.errorMeta = err.data;
						var msg = 'Failed to approve Request: '+$scope.supplyRequest.body.requestNumber + " due to errors"
						finalizeAction(msg, 'danger');
					}else{
						delete $scope['errorMeta'];
						$scope.supplyRequest = result;
						$scope.editMeta = result.editMeta;
						var msg = 'Successfully approved Request: '+$scope.supplyRequest.body.requestNumber
						finalizeUnEditableAction(msg);
					}
				});
			}
			
			$scope.reject = function(){
				spinnerService.hide("nsisSpinner");
				$scope.supplyRequest.reject(function(result,err){
					if(err){
						$scope.errorMeta = err.data;
						var msg = 'Failed to reject Request: '+$scope.supplyRequest.body.requestNumber + " due to errors"
						finalizeAction(msg, 'danger');
					}else{
						delete $scope['errorMeta'];
						$scope.supplyRequest = result;
						$scope.editMeta = result.editMeta;
						var msg = 'Successfully rejected Request: '+$scope.supplyRequest.body.requestNumber+
							  '. Ignoring changes.'
						finalizeUnEditableAction(msg);
					}
				});
			}
			
			function finalizeUnEditableAction(msg){
				$state.go('home.submittedsvcsupplyrequests',{message: msg})
			}
			
			function finalizeAction(msg, mode){
				$scope.notifyDoneLoading();
				Prompter.promptActionState(msg, mode);
			}
			
			$scope.approvalConfirmMsg = "Are you sure you want to approve this supply request? "+ 
			"You will not be able to edit this request if no errors are found after this action."
			
			$scope.rejectionConfirmMsg = "Are you sure you want to reject this supply request? "+ 
			"You will not be able to edit this request if no errors are found after this action."
		}]
)
.controller('SvcInvMaintenanceCtrl',
		['$scope','$rootScope', '$state',
		 function($scope,$rootScope, $state){
			$scope.tabs = [
			               { heading: "On-hand", route:".inventoryonhand", index:0},
			               { heading: "Delivery Receipts", route:".deliveryreceipts", index:1},
			               { heading: "Disposal", route:".inventorydisposal", index:2},
			               /*{ heading: "Physical Stock Count Requests", route:".physicalstockcount", index:3}*/
			               ];

			$scope.go = function(route){
				$state.go(route);
			};

			$scope.setActive = function(route){
				switch(route){
					case 'home.svcinventorymaintenance.inventoryonhand': $scope.active = 0;break;
					case 'home.svcinventorymaintenance.deliveryreceipts': $scope.active = 1;break;
					case 'home.svcinventorymaintenance.inventorydisposal': $scope.active = 2;break;
					/*case 'home.svcinventorymaintenance.physicalstockcount': $scope.active = 3;break;*/
				}
			};
			
			$scope.$on("$stateChangeSuccess", function() {
				$scope.setActive($state.current.name);
			});
		}]
)
.controller('SvcDeliveryReceiptsCtrl',
		['$scope','$rootScope', '$state','Prompter','DateUtil','spinnerService','SvcDeliveryReceipt','svcAssignments','message',
		 function($scope,$rootScope, $state,Prompter,DateUtil,spinnerService, SvcDeliveryReceipt,svcAssignments,message){
			
			function doClearFilter(){
				$scope.filter.status = null;
				$scope.filter.toDate = null;
				$scope.filter.fromDate = null;
				$scope.filter.requestNumber = null;
				$scope.filter.cycle = null;
			}
			
			var initDone = false;
			function init(cb, filter, actualCB){
				$scope.filter = {};
				doClearFilter()
				initDone = true;
				cb(filter, actualCB);
			}
			
			function actualReloadList(filter, cb){
				var scopeFilter = $scope.filter;
				spinnerService.show("nsisSpinner");
				filter.facilityCodeName = svcAssignments[0].facilityCodeName
				if(scopeFilter.toDate)
					filter.toDate = DateUtil.toStringIgnoreTZ(scopeFilter.toDate);
				if(scopeFilter.fromDate){
					filter.fromDate = DateUtil.toStringIgnoreTZ(scopeFilter.fromDate);
				} 
				filter.cycle = scopeFilter.cycle;
				filter.status = scopeFilter.status;
				filter.reqNumber = scopeFilter.requestNumber;
				SvcDeliveryReceipt.get(filter,function(data){
					spinnerService.hide("nsisSpinner");
					cb(data)
				});
				$scope.triggerReload = false;
			}
			
			$scope.reloadList = function(filter, cb){
				if(!initDone){
					init(actualReloadList, filter, cb)
				}else
					actualReloadList(filter, cb);
			}
			
			$scope.clearFilter = function(){
				doClearFilter();
				$scope.triggerReload = true;
				$scope.$digest();
			}
			
			$scope.view = function(requestNumber){
				spinnerService.show("nsisSpinner");
				SvcDeliveryReceipt.get(
						{
							requestNumber:requestNumber,
							facilityCodeName:svcAssignments[0].facilityCodeName
						},
						{}, 
						function(svcDeliveryReceipt){
							spinnerService.hide("nsisSpinner");
							$state.go('home.svcdeliveryreceiptsform',{svcDeliveryReceipt: svcDeliveryReceipt});
						}
				);
			}
			
			if(message){
				Prompter.promptActionState(message, 'success');
			}
			
			$scope.doTriggerReload = function(){
				$scope.triggerReload = true;
			}
		}]
)
.controller('SvcDeliveryReceiptsFormCtrl',
		['$scope','$rootScope', '$state','spinnerService','Prompter','SvcSupplyRequest','SvcSupplyRequestService','svcDeliveryReceipt',
		 function($scope,$rootScope, $state,spinnerService,Prompter, SvcSupplyRequest,SvcSupplyRequestService,svcDeliveryReceipt){
			$scope.deliveryReceipt = svcDeliveryReceipt;
			$scope.editMeta = svcDeliveryReceipt.editMeta;
			$scope.supplyRequest = new SvcSupplyRequest(SvcSupplyRequestService.sanitizesFRQRO(svcDeliveryReceipt.body.supplyRequest));
			
			$scope.notifyLoading = function(){
				spinnerService.show("nsisSpinner");
			}
			$scope.notifyDoneLoading = function(){
				spinnerService.hide("nsisSpinner");
			}
			
			$scope.cancel = function(){
				//TODO place prompt here specially when there are changes
				$state.go('home.svcinventorymaintenance.deliveryreceipts');
			}
			
			$scope.save = function(){
				$scope.notifyLoading();
				$scope.deliveryReceipt.save(function(result,err){
					var msg = '', mode = '';
					if(err){
						$scope.errorMeta = err.data;
						msg = 'Failed to update Delivery Receipt for Request: '+getORNumber() + " due to errors"
						mode = 'danger'
					}else{
						delete $scope['errorMeta'];
						$scope.deliveryReceipt = result;
						msg = 'Successfully updated Delivery Receipt for Request: '+getORNumber()
						mode = 'success'
					}
					finalizeAction(msg, mode);
				});
			}
			
			$scope.finalize = function(){
				$scope.notifyLoading();
				$scope.deliveryReceipt.finalize(function(result,err){
					if(err){
						$scope.errorMeta = err.data;
						var msg = 'Failed to finalize Delivery Receipt for Request: '+getORNumber() + " due to errors"
						finalizeAction(msg, 'danger');
					}else{
						delete $scope['errorMeta'];
						$scope.deliveryReceipt = result;
						$scope.editMeta = result.editMeta;
						var msg = 'Successfully inalize Delivery Receipt for Request: '+getORNumber()
						finalizeUnEditableAction(msg);
					}
				});
			}
			
			function finalizeUnEditableAction(msg){
				$state.go('home.svcinventorymaintenance.deliveryreceipts',{message: msg})
			}
			
			function getORNumber(){
				return $scope.deliveryReceipt.body.supplyRequest.requestNumber;
			}
			
			function finalizeAction(msg, mode){
				$scope.notifyDoneLoading();
				Prompter.promptActionState(msg, mode);
			}
			
			$scope.finalizeConfirmMsg = "Are you sure you want to finalize this delivery receipt? "+ 
			"You will not be able to edit this receipt if no errors are found after this action."
		}]
)
.controller('SvcInventoryOnHandCtrl',
		['$scope','$rootScope', '$state',
		 function($scope,$rootScope, $state){
			 $scope.radioModel = 'Item';
			 $state.go("home.svcinventorymaintenance.inventoryonhand.byitem");
		}]
)
.controller('SvcInventoryOnHandByBatchCtrl',
		['$scope','$rootScope', '$state','spinnerService','OnHandSupplyItems','svcAssignments',
		 function($scope,$rootScope, $state, spinnerService,OnHandSupplyItems,svcAssignments){
			$scope.filter = {itemCode:null}
			$scope.reloadList = function(filter, cb){
				spinnerService.show("nsisSpinner");
				if($scope.filter.itemCode)
					filter.itemCode = $scope.filter.itemCode;
				filter.facilityCodeName = svcAssignments[0].facilityCodeName;
				filter.byItemCode = false;
				OnHandSupplyItems.get(filter, function(data){
					spinnerService.hide("nsisSpinner");
					cb(data)
				})
				$scope.triggerReload = false;
			}
			
			$scope.doTriggerReload = function(){
				$scope.triggerReload = true;
			}
		}]
)
.controller('SvcInventoryOnHandByItemCtrl',
		['$scope','$rootScope', '$state','spinnerService','OnHandSupplyItems','svcAssignments',
		 function($scope,$rootScope, $state, spinnerService,OnHandSupplyItems,svcAssignments){
			$scope.filter = {itemCode:null}
			$scope.reloadList = function(filter, cb){
				spinnerService.show("nsisSpinner");
				if($scope.filter.itemCode)
					filter.itemCode = $scope.filter.itemCode;
				filter.facilityCodeName = svcAssignments[0].facilityCodeName;
				filter.byItemCode = true;
				OnHandSupplyItems.get(filter, function(data){
					spinnerService.hide("nsisSpinner");
					cb(data)
				})
				$scope.triggerReload = false;
			}
			
			$scope.doTriggerReload = function(){
				$scope.triggerReload = true;
			}
		}]
)
.controller('SvcPhysicalStockCountCtrl',
		['$scope','$rootScope', '$state','spinnerService','Prompter','DateUtil','SvcPhysicalStockCountRequest','svcAssignments','message',
		 function($scope,$rootScope, $state, spinnerService,Prompter,DateUtil,SvcPhysicalStockCountRequest,svcAssignments,message){
			
			function doClearFilter(){
				$scope.filter = {
						status: null, requestNumber: null,
						toDate: null, fromDate: null
				}
			}
			
			var initDone = false;
			function init(cb, filter, actualCB){
				$scope.filter = {}
				doClearFilter();
				$scope.filter.toDate = new Date();
				$scope.filter.fromDate = new Date();
				initDone = true
				cb(filter, actualCB);
			}
			
			function actualReloadList(filter, cb){
				var scopeFilter = $scope.filter;
				spinnerService.show("nsisSpinner");
				filter.status = scopeFilter.status;
				filter.reqNumber = scopeFilter.requestNumber;
				filter.facilityCodeName = svcAssignments[0].facilityCodeName;
				if(scopeFilter.toDate)
					filter.toDate = DateUtil.toStringIgnoreTZ(scopeFilter.toDate);
				if(scopeFilter.fromDate){
					filter.fromDate = DateUtil.toStringIgnoreTZ(scopeFilter.fromDate);
				}
				filter.showFacilityCode = false;
				SvcPhysicalStockCountRequest.get(filter, function(data){
					spinnerService.hide("nsisSpinner");
					cb(data)
				})
				$scope.triggerReload = false;
			}
			
			$scope.reloadList = function(filter, cb){
				if(!initDone){
					init(actualReloadList, filter, cb)
				}else
					actualReloadList(filter, cb);
			}
			
			$scope.clearFilter = function(){
				doClearFilter();
				$scope.triggerReload = true;
				$scope.$digest();
			}
			
			$scope.remove = function(requestNumber, cb){
				if(requestNumber){
					spinnerService.show("nsisSpinner");
					SvcPhysicalStockCountRequest.remove(
							{
								requestNumber:requestNumber,
								facilityCodeName:svcAssignments[0].facilityCodeName
							},
							function(){
								finalizeAction('Successfully removed request: '+requestNumber,'success');
								cb(true);
							},
							function(err){
								console.log(err)
								var errMsg = 'Failed to remove request: '+requestNumber + '. Reason is: ' + err.data;
								finalizeAction(errMsg,'danger');
								cb(false);
							}
					)
				}else{
					var errMsg = 'Cannot do action. Select first a request.';
					Prompter.promptActionState(errMsg,'danger');
					cb(false);
				}
			}
			
			$scope.view = function(requestNumber){
				spinnerService.show("nsisSpinner");
				SvcPhysicalStockCountRequest.get(
						{
							requestNumber:requestNumber,
							facilityCodeName:svcAssignments[0].facilityCodeName
						},
						{}, 
						function(svcPhysicalStockCountRequest){
							spinnerService.hide("nsisSpinner");
							$state.go('home.svcphysicalstockcountrequestsform',
									{svcPhysicalStockCountRequest: svcPhysicalStockCountRequest});
						});
			}
			
			$scope.add = function(){
				spinnerService.show("nsisSpinner");
				SvcPhysicalStockCountRequest.getTemplate(
						{facilityCodeName:svcAssignments[0].facilityCodeName},
						function(svcPhysicalStockCountRequest){
							spinnerService.hide("nsisSpinner");
							$state.go('home.svcphysicalstockcountrequestsform',
									{svcPhysicalStockCountRequest: svcPhysicalStockCountRequest});
						});
			}
			
			if(message){
				Prompter.promptActionState(message, 'success');
			}
			
			function finalizeAction(msg, mode){
				spinnerService.hide("nsisSpinner");
				Prompter.promptActionState(msg, mode);
			}
			
			$scope.doTriggerReload = function(){
				$scope.triggerReload = true;
			}
		}]
)
.controller('SvcPhysicalStockCountFormCtrl',
		['$scope','$rootScope', '$state','spinnerService','Prompter','SvcPhysicalStockCountRequestService','svcPhysicalStockCountRequest',
		 function($scope,$rootScope, $state,spinnerService,Prompter,SvcPhysicalStockCountRequestService,svcPhysicalStockCountRequest){
			$scope.deliveryReceipt = svcDeliveryReceipt;
			$scope.editMeta = svcDeliveryReceipt.editMeta;
			$scope.supplyRequest = new SvcSupplyRequest(SvcSupplyRequestService.sanitizesFRQRO(svcDeliveryReceipt.body.supplyRequest));
			
			$scope.notifyLoading = function(){
				spinnerService.show("nsisSpinner");
			}
			$scope.notifyDoneLoading = function(){
				spinnerService.hide("nsisSpinner");
			}
			
			$scope.cancel = function(){
				//TODO place prompt here specially when there are changes
				$state.go('home.svcinventorymaintenance.deliveryreceipts');
			}
			
			$scope.save = function(){
				$scope.notifyLoading();
				$scope.deliveryReceipt.save(function(result,err){
					var msg = '', mode = '';
					if(err){
						$scope.errorMeta = err.data;
						msg = 'Failed to update Delivery Receipt for Request: '+getORNumber() + " due to errors"
						mode = 'danger'
					}else{
						delete $scope['errorMeta'];
						$scope.deliveryReceipt = result;
						msg = 'Successfully updated Delivery Receipt for Request: '+getORNumber()
						mode = 'success'
					}
					finalizeAction(msg, mode);
				});
			}
			
			$scope.finalize = function(){
				$scope.notifyLoading();
				$scope.deliveryReceipt.finalize(function(result,err){
					if(err){
						$scope.errorMeta = err.data;
						var msg = 'Failed to finalize Delivery Receipt for Request: '+getORNumber() + " due to errors"
						finalizeAction(msg, 'danger');
					}else{
						delete $scope['errorMeta'];
						$scope.deliveryReceipt = result;
						$scope.editMeta = result.editMeta;
						var msg = 'Successfully inalize Delivery Receipt for Request: '+getORNumber()
						finalizeUnEditableAction(msg);
					}
				});
			}
			
			function finalizeUnEditableAction(msg){
				$state.go('home.svcinventorymaintenance.deliveryreceipts',{message: msg})
			}
			
			function getORNumber(){
				return $scope.deliveryReceipt.body.supplyRequest.requestNumber;
			}
			
			function finalizeAction(msg, mode){
				$scope.notifyDoneLoading();
				Prompter.promptActionState(msg, mode);
			}
			
			$scope.finalizeConfirmMsg = "Are you sure you want to finalize this delivery receipt? "+ 
			"You will not be able to edit this receipt if no errors are found after this action."
		}]
)
.controller('SvcDeliveryItemBatchGroupModalCtrl',["$scope", "$uibModalInstance","spinnerService","DateUtil",
                                               "NsisDirectiveScopeWrapper","editMeta","deliveredItemBatchGroup",
		function ($scope, $uibModalInstance,spinnerService,DateUtil,NsisDirectiveScopeWrapper,editMeta,deliveredItemBatchGroup) {
			$scope.disableIdFields = false;
			$scope.itemBatchActionLabel = 'Add';
			$scope.editMeta = editMeta;
			
			if($scope.augment)
	    		NsisDirectiveScopeWrapper.augmentScope($scope);
			
			$scope.deliveredItemBatchGroup = deliveredItemBatchGroup;
			
			$scope.$watch('deliveredItemBatchGroup.editedItemBatch.rackingCode', function(nv, ov){
				$scope.deliveredItemBatchGroup.editedItemBatch.batchNumber = constructBatchNumber($scope.deliveredItemBatchGroup);
			});
			
			$scope.$watch('deliveredItemBatchGroup.editedItemBatch.dateStored', function(nv, ov){
				$scope.deliveredItemBatchGroup.editedItemBatch.batchNumber = constructBatchNumber($scope.deliveredItemBatchGroup);
			});
			
			$scope.$watch('deliveredItemBatchGroup.editedItemBatch.quantityByUOM', function(nv, ov){
				var quantity = parseInt(nv);
				var piecesPerUOM = parseInt(deliveredItemBatchGroup.receivableDetails.piecesPerUOM);
				$scope.deliveredItemBatchGroup.editedItemBatch.quantity = quantity * piecesPerUOM;
			});
			
			function constructBatchNumber(itemBatchGroup){
				var returnValue = null;
				if(itemBatchGroup.editedItemBatch.rackingCode && itemBatchGroup.editedItemBatch.dateStored){
					var itemCode = itemBatchGroup.receivableDetails.itemCode.replace(/-/g,"");
					var rackCode = itemBatchGroup.editedItemBatch.rackingCode.rackCode.replace(/-/g,"");
					var dateStored = DateUtil.parseAsNoSlashShortDate(itemBatchGroup.editedItemBatch.dateStored);
					returnValue = itemCode + "-" + rackCode + "-" + dateStored;
				}
				
				return returnValue;
			}
			
			$scope.ok = function () {
				spinnerService.show("nsisSpinner");
				$scope.deliveredItemBatchGroup.saveItemBatch(
					$scope.itemBatchActionLabel === 'Add',
					function(thereIsChange){
						delete $scope['errorMeta']
						if(thereIsChange)$scope.triggerGridReload = true;
						$scope.itemBatchActionLabel = 'Add';
						$scope.disableIdFields = false;
						spinnerService.hide("nsisSpinner");
					},function(err){
						$scope.errorMeta = err.data;
						spinnerService.hide("nsisSpinner");
					}
				);
			};
			
			$scope.view = function (selectedItemBatch) {
				$scope.deliveredItemBatchGroup.editedItemBatch = angular.copy(selectedItemBatch);
				$scope.itemBatchActionLabel = 'Update';
				$scope.disableIdFields = true;
			};
			
			$scope.remove = function (batchNumber, cb) {
				$scope.deliveredItemBatchGroup.removeItemBatch(batchNumber);
				cb(true);
			};
			
			$scope.list = function(cb){
				spinnerService.show("nsisSpinner");
				$scope.deliveredItemBatchGroup.recalculateItemBatches(function(data){
					spinnerService.hide("nsisSpinner");
					cb(data)
				});
			}
		
			$scope.clear = function () {
				delete $scope['errorMeta'];
				$scope.disableIdFields = false;
				$scope.itemBatchActionLabel = 'Add';
				$scope.deliveredItemBatchGroup.editedItemBatch = {};
			};
			
			$scope.apply = function (){
				$uibModalInstance.close($scope.deliveredItemBatchGroup);
			}
			
			$scope.close = function (){
				$uibModalInstance.dismiss('close');
			}

			$scope.triggerGridReload = true;
		}]
)
.controller('RequestsToServiceCenterIssuancesCtrl',
		['$scope','$rootScope', '$state','spinnerService','Prompter','DateUtil','SvcRequestIssuance','CsCustomerRequest','message','svcAssignments',
		 function($scope,$rootScope, $state,spinnerService,Prompter,DateUtil,SvcRequestIssuance,CsCustomerRequest,message,svcAssignments){
			
			function doClearFilter(){
				$scope.filter.status = null;
				$scope.filter.requestNumber = null;
				$scope.filter.toDate = null;
				$scope.filter.fromDate = null;
				$scope.filter.accountNumber = null;
				$scope.filter.accountName = null;
				$scope.filter.contactName = null;
				$scope.filter.requestType = null;
			}
			
			var initDone = false;
			function init(cb, filter, actualCB){
				$scope.filter = {};
				doClearFilter()
				$scope.filter.status = 'FOR PROCESSING';
				initDone = true;
				cb(filter, actualCB);
			}
			
			function actualReloadList(filter, cb){
				spinnerService.show('nsisSpinner')
				filter.facilityCodeName = svcAssignments[0].facilityCodeName
				
				var scopeFilter = $scope.filter;
				
				filter.requestType = scopeFilter.requestType;
				filter.status = scopeFilter.status;
				filter.reqNumber = scopeFilter.requestNumber;
				filter.accountNumber = scopeFilter.accountNumber;
				filter.customerName = scopeFilter.customerName;
				
				if(scopeFilter.toDate)
					filter.toDate = DateUtil.toStringIgnoreTZ(scopeFilter.toDate);
				if(scopeFilter.fromDate){
					filter.fromDate = DateUtil.toStringIgnoreTZ(scopeFilter.fromDate);
				}
				
				SvcRequestIssuance.get(filter,function(data){
					spinnerService.hide('nsisSpinner')
					cb(data)
				});
				$scope.triggerReload = false;
			}
			
			$scope.reloadList = function(filter, cb){
				if(!initDone){
					init(actualReloadList, filter, cb)
				}else
					actualReloadList(filter, cb);
			}
			
			$scope.clearFilter = function(){
				doClearFilter();
				$scope.triggerReload = true;
				$scope.$digest();
			}
			
			$scope.view = function(requestNumber){
				spinnerService.show("nsisSpinner");
				var facilityCode = svcAssignments[0].facilityCodeName;
				SvcRequestIssuance.get(
					{
						requestNumber:requestNumber,
						facilityCodeName:facilityCode
					},{}, 
					function(requestIssuance){
						CsCustomerRequest.get(
								{
									requestNumber:requestNumber,
									facilityCodeName:facilityCode
								},
								{}, 
								function(customerRequest){
									spinnerService.hide("nsisSpinner");
									$state.go(
											'home.requesttosvcissuanceform',
											{requestIssuance: requestIssuance, customerRequest: customerRequest}
										);
								});
					});
			}
			
			if(message){
				Prompter.promptActionState(message, 'success');
			}
			
			function finalizeAction(msg, mode){
				spinnerService.hide("nsisSpinner");
				Prompter.promptActionState(msg, mode);
			}
			
			$scope.doTriggerReload = function(){
				$scope.triggerReload = true;
			}
		}]
)
.controller('RequestsToServiceCenterIssuancesFormCtrl',
		['$scope','$rootScope', '$state','spinnerService','Prompter','customerRequest','requestIssuance',
		 function($scope,$rootScope, $state,spinnerService,Prompter, customerRequest,requestIssuance){
			$scope.requestIssuance = requestIssuance;
			$scope.editMeta = requestIssuance.editMeta;
			$scope.customerRequest = customerRequest;
			
			$scope.reroutedMessage = null;
			$scope.disabled = $scope.editMeta.type === 'NONE';
			$scope.disableOtherButtons = $scope.disabled;

			$scope.notifyLoading = function(){
				spinnerService.show("nsisSpinner");
			}
			$scope.notifyDoneLoading = function(){
				spinnerService.hide("nsisSpinner");
			}
			
			$scope.cancel = function(){
				//TODO place prompt here specially when its NEW or there are changes
				$state.go('home.requesttosvcissuance');
			}
			
			$scope.$watch('requestIssuance.body.facilityCodeName',function(nv, ov){
				var toBeRerouted = nv !== requestIssuance.body.previousFacilityCodeName;
				if(toBeRerouted){
					$scope.reroutedMessage = "Changing the facility code will make " +
							"this request to be rerouted. Do you still want to to continue?"
				}else{
					$scope.reroutedMessage = null;
				}
				$scope.disableOtherButtons = toBeRerouted || $scope.disabled;
			})
			
			var msg ="";
			$scope.save = function(){
				$scope.notifyLoading();
				$scope.requestIssuance.save(function(result, rerouted){
					if(rerouted){
						msg = 'Successfully rerouted Request: '+
							getRequestNumber(result)+
							" from "+requestIssuance.body.previousFacilityCodeName+
							" to "+result.body.facilityCodeName;
						finalizeUnEditableAction(msg);
					}else{
						msg = 'Successfully updated Request Issuance for request: '+getRequestNumber(result);
						defaultSuccessHandler(msg, result);
					}
					
				}, function(err){
					msg = 'Failed to update Request Issuance for request: '+
						getRequestNumber($scope.requestIssuance) + 
						" due to errors"
					defaultErrorHandler(msg, err);
				})
			}
			
			$scope.finalize = function(){
				$scope.notifyLoading();
				$scope.requestIssuance.finalize(function(result){
					msg = 'Successfully finalized Request Issuance for request: '+getRequestNumber(result);
					window.open("/NSIS/rest/1.0/facilities/"+
							getRequestFacilityCode(result)+
							"/requestissuances/"+getRequestNumber(result)+"/deliveryreceiptform");
					finalizeUnEditableAction(msg);
				}, function(err){
					msg = 'Failed to finalize Request Issuance for request: '+
						getRequestNumber($scope.requestIssuance) + 
						" due to errors"
					defaultErrorHandler(msg, err);
				})
			}
			
			$scope.print = function(){
				$scope.notifyLoading();
				$scope.requestIssuance.print(function(result){
					msg = 'Successfully printed Request Issuance form for request: '+getRequestNumber(result);
					defaultSuccessHandler(msg, result);
					window.open("/NSIS/rest/1.0/facilities/"+
							getRequestFacilityCode(result)+
							"/requestissuances/"+getRequestNumber(result)+"/picklistform");
				}, function(err){
					msg = 'Failed to print Request Issuance form for request: '+
					getRequestNumber($scope.requestIssuance) + 
						" due to errors"
					defaultErrorHandler(msg, err);
				})
			}
			
			$scope.reject = function(){
				$scope.notifyLoading();
				$scope.requestIssuance.reject(function(result){
					msg = 'Successfully rejected Request Issuance for request: '+getRequestNumber(result);
					finalizeUnEditableAction(msg);
				}, function(err){
					msg = 'Failed to reject Request Issuance for request: '+
						getRequestNumber($scope.requestIssuance) + 
						" due to errors"
					defaultErrorHandler(msg, err);
				})
			}
			
			function defaultErrorHandler(msg, err){
				$scope.errorMeta = err.data;
				finalizeAction(msg, 'danger');
			}
			
			function defaultSuccessHandler(msg, updatedRequestIssuance){
				delete $scope['errorMeta'];
				$scope.requestIssuance = updatedRequestIssuance;
				$scope.editMeta = updatedRequestIssuance.editMeta;
				finalizeAction(msg, 'success');
			}
			
			function getRequestNumber(requestIssuance){
				return requestIssuance.body.requestNumber;
			}
			
			function getRequestFacilityCode(requestIssuance){
				return requestIssuance.body.facilityCodeName;
			}
			
			function finalizeUnEditableAction(msg){
				$state.go('home.requesttosvcissuance',{message: msg})
			}
			
			function finalizeAction(msg, mode){
				$scope.notifyDoneLoading();
				Prompter.promptActionState(msg, mode);
			}
			
			$scope.finalizeConfirmMsg = "Are you sure you want to finalize this supply issuance? "+ 
			"You will not be able to edit this record if no errors are found after this action."
			
			$scope.rejectionConfirmMsg = "Are you sure you want to reject this supply request? "+ 
			"You will not be able to edit this record if no errors are found after this action."
			
		}]
)
.controller('SvcRequestIssuanceModalCtrl', ["$scope", "$uibModalInstance","spinnerService", "requestItemGroup","mode","disabled",
                                            function ($scope, $uibModalInstance,spinnerService, requestItemGroup,mode,disabled) {
	
	$scope.notifyLoading = function(){
		spinnerService.show("nsisSpinner");
	}
	$scope.notifyDoneLoading = function(){
		spinnerService.hide("nsisSpinner");
	}
	$scope.disabled = disabled;
	$scope.requestItemGroup = requestItemGroup;
	
	$scope.listAvailableSupplies = function(cb){
		$scope.notifyLoading();
		$scope.requestItemGroup.listAvailableSupplies(function(suppliesGroup){
			
			var availableSupplies = suppliesGroup.availableSupplies;
			var suppliesIssued = $scope.requestItemGroup.suppliedItems;
			var totalSuppliedQuantity = parseInt(0);
			availableSupplies.forEach(function(availableSupply){
				var suppliedQuantity = parseInt(0);
				if(suppliesIssued){
					var supplyMatch = suppliesIssued.find(function(si){
						return si.sourceBatchNumber === availableSupply.batchNumber
					});
					if(supplyMatch) {
						suppliedQuantity = parseInt(supplyMatch.suppliedQuantity);
						availableSupply.remarks = supplyMatch.remarks;
					}
				}
				availableSupply.suppliedQuantity = suppliedQuantity;
				availableSupply.previousSuppliedQuantity = suppliedQuantity;
				totalSuppliedQuantity+= suppliedQuantity;
			});
			
			$scope.requestItemGroup.supplyDetails  = {
					physicalStock: suppliesGroup.physicalStock,
					logicalStock: suppliesGroup.logicalStock,
					totalSuppliedQty: totalSuppliedQuantity
				}
			$scope.notifyDoneLoading();
			cb(suppliesGroup.availableSupplies)
		});
	}
	
	function finalizeErrMsg(errMsg){
		var finalMsg = errMsg;
		if(errMsg !== null && typeof errMsg === 'object'){
			var batchNumber = Object.keys(errMsg)[0];
			finalMsg = batchNumber+": "+errMsg[batchNumber];
		}
		
		return finalMsg;
	}
	
	$scope.updateRequestItemIssuanceView = function(hasError, msg){
		$scope.disableApplyButton = hasError || $scope.disabled;
		if(!hasError){
			var sum = $scope.requestItemGroup.suppliedItems.reduce(function(previousSum, suppliedItem){
				return previousSum + parseInt(suppliedItem.suppliedQuantity);
			},parseInt(0));
			
			$scope.requestItemGroup.supplyDetails.totalSuppliedQty = sum;
			$scope.modalError = null;
		}else if(msg){
			$scope.modalError = finalizeErrMsg(msg);
		}
	}
	
	$scope.apply = function () {
		$scope.validateHighlighted(function(successful, msg){
			$scope.disableApplyButton = false;
			$scope.modalError = finalizeErrMsg(msg);
			if(successful) {
				$uibModalInstance.close($scope.requestItemGroup);
			}
		})
	};

	$scope.close = function () {
		$uibModalInstance.dismiss('cancel');
	};
	
	$scope.triggerGridReload = true;
	
}])