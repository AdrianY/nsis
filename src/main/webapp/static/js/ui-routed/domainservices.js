/**
 * 
 */

var mainUrl = '/NSIS/rest/1.0/';
angular.module('nsisDomainServices', ['ngResource','ngCookies','nsisServices'])
.factory('AuthWebService', ["$resource",function($resource) {

	return $resource(mainUrl+'auth/:action', {},
			{
		authenticate: {
			method: 'POST',
			params: {'action' : 'authenticate'},
			headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		}
			}
	);
}])
.factory('InventoryItemService', ["$resource",function($resource) {

	return $resource(mainUrl+'items/:codename', {codename:'@_codeName'},
			{
			}
	);
}])
.factory('Route', ["$resource",function($resource) {

	return $resource(mainUrl+'facilities/:facilityCodeName/routes/:routecode', 
			{facilityCodeName:'@facilityCodeName',routecode:'@routecode'},
			{
			}
	);
}])
.factory('FacilityInventoryItemService', ["$resource",function($resource) {

	return $resource(
			mainUrl+'facilities/:facilityId/items/:codeName', 
			{facilityId:'@facilityId',codeName:'@codeName'},
			{}
	);
}])
.factory('FacilityPersonnelService', ["$resource",function($resource) {

	return $resource(
			mainUrl+'facilities/:facilityId/personnels/:userName', 
			{facilityId:'@facilityId',userName:'@userName'},
			{}
	);
}])
.factory('WarehouseLocationService', ["$resource",function($resource) {

	return $resource(
			mainUrl+'facilities/:facilityCode/warehouselocations/:rackcode', 
			{facilityId:'@facilityCode',rackcode:'@_rackCode'},
			{}
	);
}])
.factory('OrderRequest', ["$resource","OrderRequestService",function($resource,OrderRequestService) {

	var OrderRequest = $resource(mainUrl+'orderrequests/:orderNumber', {orderNumber:'@_orNumber'},
			{
				recalculateItems: {
					method: 'POST',
					url: mainUrl+'orderrequests/orderrequests/',
					headers: {'Content-Type':'application/json','xAction':'CALCULATE'}, 
					isArray:true
				},
				get: {
		            method: 'GET',
		            transformResponse: transFxForOR
		        },
				update:{method:'PUT'},
				getTemplate: {
					method: 'POST',
					url: mainUrl+'orderrequests/orderrequests/',
					headers: {'Content-Type':'application/json','xAction':'TEMPLATE'}
				},
				create:{
					method:'POST',
					url:mainUrl+'orderrequests/orderrequests/',
					headers: {'Content-Type':'application/json','xAction':'CREATE'}
				},
		        submit: {
		        	method: 'POST',
					url: mainUrl+'orderrequests/orderrequests/',
					headers: {'Content-Type':'application/json','xAction':'SUBMIT'}
		        },
		        approve: {
		        	method: 'POST',
					url: mainUrl+'orderrequests/orderrequests/',
					headers: {'Content-Type':'application/json','xAction':'APPROVE'}
		        },
		        reject: {
		        	method: 'POST',
					url: mainUrl+'orderrequests/orderrequests/',
					headers: {'Content-Type':'application/json','xAction':'REJECT'}
		        }
			}
	);
	
	function transFxForOR(data, headers){
		var dataFromJSON = angular.fromJson(data);
		
    	if(!!(dataFromJSON && !(dataFromJSON.rows))){
    		dataFromJSON = OrderRequestService.sanitizeOrQRO(dataFromJSON)
    	}
    		
        return dataFromJSON;
	}
	
	OrderRequest.prototype ={
		showRecalculatedItems: function(cb){
			var currentOR = this.body;
			if(!currentOR) currentOR = this;
			var orderRequestCO = OrderRequestService.generateOrderRequestCO(currentOR);
			OrderRequest.recalculateItems({},orderRequestCO,function(result){
				if(result)cb(result);
			});
		},
		saveRequestedItem: function(requestedItem,mode,cb){
			var thisRef = this;
			var existingRequestedItem = thisRef.body.itemsRequested.find(function(item){
				return item.itemCode === requestedItem.itemCode;
			});
			
			if(existingRequestedItem){
				
				var getQuantity = function(prevQuantity, quantity){
					return 'ADD' === mode ? 
							parseInt(prevQuantity) + parseInt(quantity) : 
							parseInt(quantity);
				}
				
				var thereIsChange = existingRequestedItem.quantity !== requestedItem.quantity || 
									existingRequestedItem.remarks !== requestedItem.remarks;
				
				existingRequestedItem.quantity = 
					getQuantity(existingRequestedItem.quantity, requestedItem.quantity);
				existingRequestedItem.remarks = angular.copy(requestedItem.remarks);
				
				cb(thereIsChange);
			}else{
				var newRequestedItem = angular.copy(requestedItem);
				newRequestedItem.quantity = parseInt(newRequestedItem.quantity);
				thisRef.body.itemsRequested.push(newRequestedItem);
				cb(true);
			}
		},
		save: function(cb){
			var currentOR = this.body;
			if(!currentOR) currentOR = this;
			var orderRequestCO = OrderRequestService.generateOrderRequestCO(currentOR);
			
			if('NEW'===currentOR.status){
				OrderRequest.create({},orderRequestCO,function(result){
					if(cb && result){
						cb(new OrderRequest(OrderRequestService.sanitizeOrQRO(result)));
					}
				},function(err){
					cb(null,err)
				});
			}else if('DRAFT'===currentOR.status){
				OrderRequest.update({orderNumber:this.body.orderNumber},orderRequestCO,function(result){
					if(cb && result){
						cb(new OrderRequest(OrderRequestService.sanitizeOrQRO(result)));
					}
				},function(err){
					cb(null,err)
				});
			}
		},
		submit: function(cb){
			var orderRequestCO = OrderRequestService.generateOrderRequestCO(this.body);
			console.log('orderRequestCO: '+orderRequestCO)
			OrderRequest.submit({},orderRequestCO,function(result){
				if(cb && result){
					cb(new OrderRequest(OrderRequestService.sanitizeOrQRO(result)));
				}
			},function(err){
				cb(null,err)
			});
		},
		approve: function(cb){
			var orderRequestCO = OrderRequestService.generateOrderRequestCO(this.body);
			console.log('orderRequestCO: '+orderRequestCO)
			OrderRequest.approve({},orderRequestCO,function(result){
				if(cb && result){
					cb(new OrderRequest(OrderRequestService.sanitizeOrQRO(result)));
				}
			},function(err){
				cb(null,err)
			});
		},
		reject: function(cb){
			var orderRequestCO = OrderRequestService.generateOrderRequestCO(this.body);
			console.log('orderRequestCO: '+orderRequestCO)
			OrderRequest.reject({},orderRequestCO,function(result){
				if(cb && result){
					cb(new OrderRequest(OrderRequestService.sanitizeOrQRO(result)));
				}
			},function(err){
				cb(null,err)
			});
		}
	}
	
	return OrderRequest;
}])
.factory('OrderRequestItem', ["$resource","OrderRequestItemService",function($resource,OrderRequestItemService){
	var OrderRequestItem = $resource(mainUrl + 'orderrequests/:orderNumber/items:/itemCode', {orderNumber:'@orderNumber'}, {
		validate: {
			method: 'POST',
			params: {orderNumber:'@orderNumber'},
			url: mainUrl+'orderrequests/:orderNumber/items/items',
			headers: {'Content-Type':'application/json','xAction':'VALIDATE'}
		}
	})
	
	OrderRequestItem.prototype = {
		validate: function(cb){
			console.log('validating ori for or: '+this.orderNumber)
			var orderRequestItemCO = OrderRequestItemService.generateOrderRequestItemCO(this);
			OrderRequestItem.validate({orderNumber:this.orderNumber},orderRequestItemCO, function(result){
				if(cb) cb(null);
			},function(err){
				cb(null,err);
			})
		}
	}
	
	return OrderRequestItem;
}])
.factory('DeliveryReceipt', ["$resource","DeliveryReceiptService","NHelper",function($resource,DeliveryReceiptService,NHelper) {

	var DeliveryReceipt = $resource(mainUrl+'deliveryreceipts/:orderNumber', {orderNumber:'@_orNumber'},
			{
				get: {
		            method: 'GET',
		            transformResponse: transFxForOR
		        },
				update:{method:'PUT'},
		        finalize: {
		        	method: 'POST',
					url: mainUrl+'deliveryreceipts/deliveryreceipts',
					headers: {'Content-Type':'application/json','xAction':'FINALIZE'}
		        }
			}
	);
	
	function transFxForOR(data, headers){
		var dataFromJSON = angular.fromJson(data);
		
    	if(!!(dataFromJSON && !(dataFromJSON.rows))){
    		dataFromJSON = DeliveryReceiptService.sanitizeOrQRO(dataFromJSON)
    	}
    		
        return dataFromJSON;
	}
	
	DeliveryReceipt.prototype ={
		save: function(cb){
			var currentDR = this.body;
			var deliveryReceiptCO = DeliveryReceiptService.generateCO(currentDR);
			
			DeliveryReceipt.update({orderNumber:deliveryReceiptCO.orderNumber},deliveryReceiptCO,function(result){
				if(cb && result){
					cb(new DeliveryReceipt(DeliveryReceiptService.sanitizeOrQRO(result)));
				}
			},function(err){
				cb(null,err)
			});
		},
		finalize: function(cb){
			var deliveryReceiptCO = DeliveryReceiptService.generateCO(this.body);
			console.log('deliveryReceiptCO: '+deliveryReceiptCO.itemBatchGroups)
			DeliveryReceipt.finalize({},deliveryReceiptCO,function(result){
				if(cb && result){
					cb(new DeliveryReceipt(DeliveryReceiptService.sanitizeOrQRO(result)));
				}
			},function(err){
				cb(null,err)
			});
		},
		updateItemBatchGroupList: function(deliveredItemBatchGroup, cb){
			var itemCode = deliveredItemBatchGroup.receivableDetails.itemCode;
			var thisRef = this.body;
			var batchGroup = thisRef.itemBatches[itemCode];
			NHelper.ifPresent(batchGroup, function(group){
				var itemBatch = DeliveryReceiptService.sanitizeEditedItemBatchGroup(deliveredItemBatchGroup);
				thisRef.itemBatches[itemCode] = itemBatch;
			})
			
			if(cb)cb();
		}
	}
	
	return DeliveryReceipt;
}])
.factory('DeliveredItemBatchGroup', ["$resource","NHelper","DeliveredItemBatchGroupService",function($resource,NHelper,DeliveredItemBatchGroupService){
	var DeliveredItemBatchGroup = $resource(mainUrl + 'deliveryreceipts/:orderNumber/itembatches/:itemCode', {orderNumber:'@orderNumber'}, {
		validate: {
			method: 'POST',
			params: {orderNumber:'@orderNumber'},
			url: mainUrl+'deliveryreceipts/:orderNumber/itembatchgroups/itembatchgroups',
			headers: {'Content-Type':'application/json','xAction':'VALIDATE'}
		},
		recalculateBatches: {
			method: 'POST',
			params: {orderNumber:'@orderNumber'},
			url: mainUrl+'deliveryreceipts/:orderNumber/itembatchgroups/itembatchgroups',
			headers: {'Content-Type':'application/json','xAction':'CALCULATE'}, 
			isArray:true
		}
	})
	
	DeliveredItemBatchGroup.prototype = {
		validate: function(isNew,cb){
			var deliveredItemBatchGroupCO = DeliveredItemBatchGroupService.generateValidationCO(this,isNew);
			var myReference = this;
			DeliveredItemBatchGroup.validate({orderNumber:this.orderDetails.orderNumber},deliveredItemBatchGroupCO, function(result){
				if(cb) cb(myReference,null);
			},function(err){
				cb(null,err);
			})
		},
		recalculateItemBatches: function(cb){
			var deliveredItemBatchCO = DeliveredItemBatchGroupService.generateCO(this);
			DeliveredItemBatchGroup.recalculateBatches({orderNumber:this.orderDetails.orderNumber},deliveredItemBatchCO,function(result){
				if(result)cb(result);
			});
		},
		removeItemBatch: function(batchNumber){
			this.itemBatches = this.itemBatches.filter(function(batch){
				return batchNumber !== batch.batchNumber;
			});
		},
		saveItemBatch: function(isNew, successCb, errCb){
			var thisRef = this;
			thisRef.validate(isNew,function(result, err){
				if(err) errCb(err);
				else {
					var itemBatch = thisRef.itemBatches.find(function(batch){
						return thisRef.editedItemBatch.batchNumber === batch.batchNumber;
					});
					var thereIsChange = false;
					if(itemBatch){
						var updatedItemBatch = angular.copy(result.editedItemBatch);
						thereIsChange = parseInt(itemBatch.quantity) !== parseInt(updatedItemBatch.quantity);
						var editQuantity = parseInt(updatedItemBatch.quantity);
						if(thereIsChange){
							itemBatch.quantity = editQuantity;
						}else if(isNew){
							thereIsChange = true;
							itemBatch.quantity = itemBatch.quantity + editQuantity;
						}
					}else{
						var newItemBatch = angular.copy(result.editedItemBatch);
						newItemBatch = DeliveredItemBatchGroupService.sanitizeItemBatch(newItemBatch);
						result.itemBatches.push(newItemBatch);
						thereIsChange = true;
					}

					thisRef.editedItemBatch = {};
					successCb(thereIsChange);
				}
			})
		}
	}
	
	return DeliveredItemBatchGroup;
}])
.factory('PrincipalService',['$cookies',
		 function($cookies) {
			var _identity = undefined,
			_authenticated = false;

			return {
				cacheIdentity: function(identity) {
					_identity = identity;
					_authenticated = identity != null;

					if (identity) {
						if(identity.authToken){
							console.log('save authToken to cookieStore: '+identity.authToken)
							$cookies.put('authToken', identity.authToken);
							delete identity['authToken'];
						}
						$cookies.put("identity", angular.toJson(identity))
					}
					else delete $cookies["identity"];
				}
			};
		}
])
.factory('RequestIssuance', ["$resource","RequestIssuanceService",function($resource,RequestIssuanceService) {

	var RequestIssuance = $resource(mainUrl+'requestissuances/:requestNumber', {requestNumber:'@_reqNumber'},
			{
				get: {
		            method: 'GET',
		            transformResponse: transFxForOR
		        },
				update:{method:'PUT'},
				process:{
					method: 'POST',
					url: mainUrl+'requestissuances/requestissuances/',
					headers: {'Content-Type':'application/json','xAction':'PROCESS'}
				},
		        finalize: {
		        	method: 'POST',
					url: mainUrl+'requestissuances/requestissuances/',
					headers: {'Content-Type':'application/json','xAction':'FINALIZE'}
		        },
		        reject: {
		        	method: 'POST',
					url: mainUrl+'requestissuances/requestissuances/',
					headers: {'Content-Type':'application/json','xAction':'REJECT'}
		        }
			}
	);
	
	function transFxForOR(data, headers){
		var dataFromJSON = angular.fromJson(data);
		
    	if(!!(dataFromJSON && !(dataFromJSON.rows))){
    		dataFromJSON = RequestIssuanceService.sanitizeOrQRO(dataFromJSON)
    	}
    		
        return dataFromJSON;
	}
	
	RequestIssuance.prototype ={
		findSuppliedItemsGrp: function(itemCode){
			var thisRef = this.body;
			var specfiedItemIssuanceGrp = thisRef.requestedItems.find(function(ri){
				return ri.itemCode === itemCode;
			})
			return {
				itemCode: specfiedItemIssuanceGrp.itemCode,
				suppliedItems: angular.copy(specfiedItemIssuanceGrp.suppliedItems)
			};
		},
		saveSuppliedItemsGrp: function(itemIssuanceGroup){
			var thisRef = this.body;
			var specfiedItemIssuanceGrp = thisRef.requestedItems.find(function(ri){
				return ri.itemCode === itemIssuanceGroup.itemCode;
			})
			
			var suppliedItemsGroup = 
				RequestIssuanceService.sanitizeSuppliedItemsGroup(itemIssuanceGroup);
			var clonedCopy = {
					itemCode: suppliedItemsGroup.itemCode,
					suppliedItems: angular.copy(suppliedItemsGroup.suppliedItems)
			}
			
			if(specfiedItemIssuanceGrp){
				specfiedItemIssuanceGrp.itemCode = clonedCopy.itemCode;
				specfiedItemIssuanceGrp.suppliedItems = clonedCopy.suppliedItems;
			}else{
				thisRef.requestedItems.push(clonedCopy)
			}
		},
		save: function(successHandler, errorHandler){
			var currentOR = this.body;
			if(!currentOR) currentOR = this;
			var requestIssuanceCO = RequestIssuanceService.generateCO(currentOR);
			
			RequestIssuance.update({requestNumber:this.body.requestNumber},requestIssuanceCO,function(result){
				if(successHandler && result){
					successHandler(new RequestIssuance(RequestIssuanceService.sanitizeOrQRO(result)));
				}
			},errorHandler);
		},
		print: function(successHandler, errorHandler){
			var requestIssuanceCO = RequestIssuanceService.generateCO(this.body);
			console.log('requestIssuanceCO: '+requestIssuanceCO)
			RequestIssuance.process({},requestIssuanceCO,function(result){
				if(successHandler && result){
					successHandler(new RequestIssuance(RequestIssuanceService.sanitizeOrQRO(result)));
				}
			},errorHandler);
			
			//TODO add print action after process post request
		},
		finalize: function(successHandler, errorHandler){
			var requestIssuanceCO = RequestIssuanceService.generateCO(this.body);
			RequestIssuance.finalize({},requestIssuanceCO,function(result){
				if(successHandler && result){
					successHandler(new RequestIssuance(RequestIssuanceService.sanitizeOrQRO(result)));
				}
			},errorHandler);
		},
		reject: function(successHandler, errorHandler){
			var requestIssuanceCO = RequestIssuanceService.generateCO(this.body);
			RequestIssuance.reject({},requestIssuanceCO,function(result){
				if(successHandler && result){
					successHandler(new RequestIssuance(RequestIssuanceService.sanitizeOrQRO(result)));
				}
			},errorHandler);
		}
	}
	
	return RequestIssuance;
}])
.factory('RequestedItemIssuance', ["$resource","RequestedItemIssuanceService",function($resource,RequestedItemIssuanceService) {

	var RequestedItemIssuance = $resource(
			mainUrl+'requestissuances/:requestNumber/supplieditemsgroup/:itemCode', 
			{requestNumber:'@_reqNumber', itemCode:'@_itemCd'},
			{
				validate:{
					method: 'POST',
					url: mainUrl+'requestissuances/:requestNumber/supplieditemsgroup/supplieditemsgroup',
					headers: {'Content-Type':'application/json','xAction':'VALIDATE'}
				},
		        listSupplies: {
		        	method: 'POST',
					url: mainUrl+'requestissuances/:requestNumber/supplieditemsgroup/supplieditemsgroup',
					headers: {'Content-Type':'application/json','xAction':'LISTSUPPLIES'}
		        }
			}
	);
	
	RequestedItemIssuance.prototype ={
		saveSuppliedItem: function(previousSuppliedQuantity, suppliedItem, cb){
			var thisRef = this;
			thisRef.suppliedItems = RequestedItemIssuanceService.resetEditedList(thisRef.suppliedItems);
			
			var indexOfMatch;
			var suppliedItemRec = thisRef.suppliedItems.find(function(si, index){
				indexOfMatch = index;
				return si.sourceBatchNumber === suppliedItem.sourceBatchNumber
			});
			
			if(0 < suppliedItem.suppliedQuantity){
				if(suppliedItemRec){
					setSuppliedItemRec(suppliedItem.suppliedQuantity, suppliedItem.remarks);
				}else{
					var newSuppliedItemRec = angular.copy(suppliedItem);
					newSuppliedItemRec.currentlyEdited = true;
					thisRef.suppliedItems.push(newSuppliedItemRec);
				}
				thisRef.validate(previousSuppliedQuantity, cb)
			}else if(suppliedItemRec){
				if(-1 < indexOfMatch && !suppliedItem.remarks){
					thisRef.suppliedItems.splice(indexOfMatch, 1);
					cb(null);
				}else if(suppliedItem.remarks){
					setSuppliedItemRec(0, suppliedItem.remarks);
					thisRef.validate(previousSuppliedQuantity, cb);
				}
			}else{
				cb(null)
			}
			
			function setSuppliedItemRec(quantity, remarks){
				suppliedItemRec.suppliedQuantity = quantity;
				suppliedItemRec.remarks = remarks;
				suppliedItemRec.currentlyEdited = true;
			}
		},
		validate: function(previousSuppliedQuantity, cb){
			var body = this.body;
			if(!body) body = this;
			var commandObject = 
				RequestedItemIssuanceService.generateValidationCO(previousSuppliedQuantity, body);
			
			RequestedItemIssuance.validate(
				{requestNumber:body.requestDetails.requestNumber},commandObject,function(result){
					if(cb && result){
						cb(null);
					}
				},function(err){
					cb(err)
				}
			);
		},
		listAvailableSupplies: function(cb){
			var body = this.body;
			if(!body) body = this;
			var commandObject = RequestedItemIssuanceService.generateCO(body);
			RequestedItemIssuance.listSupplies(
				{requestNumber:body.requestDetails.requestNumber},commandObject,function(result){
				if(cb && result){
					cb(result);
				}
			},function(err){
				cb(null,err)
			});
		}
	}
	
	return RequestedItemIssuance;
}])
.factory('OnHandSupplyItems', ["$resource",function($resource) {

	var OnHandSupplyItems = $resource(
			mainUrl+'facilities/:facilityCodeName/onhandsupplyitems', 
			{facilityCodeName:'@facilityCodeName'},
			{}
	);
	
	return OnHandSupplyItems;
}]);
