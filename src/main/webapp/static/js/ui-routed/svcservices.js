/**
 * 
 */
angular.module('nsisSvcServices', [])
.service('SvcSupplyRequestService',["CloneHelper","NHelper","DateUtil",function(CloneHelper,NHelper,DateUtil){
	var SvcSupplyRequestCO = function(supplyRequest){
		console.log('SupplyRequest: ' +supplyRequest)
		var thisRef = this;
		
		CloneHelper.copyEquivalentAttributes(
				thisRef, supplyRequest, 
				['requestNumber','facilityCodeName','requestCycle','currency','status']);
		
		NHelper.ifPresent(supplyRequest.requestDate,function(requestDate){
			thisRef.requestDate = DateUtil.toStringIgnoreTZ(requestDate);
		})
		
		NHelper.ifPresent(supplyRequest.issuanceDate,function(issuanceDate){
			thisRef.issuanceDate = DateUtil.toStringIgnoreTZ(issuanceDate);
		})
				
		NHelper.ifPresent(supplyRequest.requestBy,function(requestBy){
			thisRef.requestByUsername = requestBy.userName;
		});
		
		NHelper.ifPresent(supplyRequest.approver, function(approver){
			thisRef.approver = {
				remarks: approver.remarks	
			};
			NHelper.ifPresent(approver.approverDetails, function(details){
				thisRef.approver.username = approver.approverDetails.userName
			});
		});
		
		NHelper.ifPresent(supplyRequest.recipient, function(recipient){
			thisRef.recipient = {
				deliveryAddress: recipient.deliveryAddress
			};
			NHelper.ifPresent(recipient.recipientDetails, function(recipientDetails){
				thisRef.recipient.username = recipientDetails.userName,
				thisRef.recipient.contactNumber = recipientDetails.contactNumber,
				thisRef.recipient.emailAddress = recipientDetails.emailAddress
			});
		});
		thisRef.itemsRequested = supplyRequest.itemsRequested;
	}
	
	this.generateSupplyRequestCO = function(supplyRequest){
		return new SvcSupplyRequestCO(supplyRequest);
	}
	
	this.sanitizesFRQRO = function(sFRQRO){
		var body = sFRQRO.body;
		if(!body) body = sFRQRO;
		NHelper.ifPresent(body.requestDate,function(rDte){
			body.requestDate = DateUtil.parseShortDate(rDte);
		});
		
		NHelper.ifPresent(body.issuanceDate,function(iDte){
			body.issuanceDate = DateUtil.parseShortDate(iDte);
		});
		
		return sFRQRO;
	}
}])
.service('SupplyRequestItemService',function(){
	var SupplyRequestItemCO = function(supplyRequestItem){
		var thisRef = this;
		thisRef.facilityCodeName = supplyRequestItem.facilityId;
		thisRef.details = {
			itemCode: supplyRequestItem.itemCode,
			quantity: supplyRequestItem.quantity,
			remarks: supplyRequestItem.remarks
		}
	}
	
	this.generateSupplyRequestItemCO = function(supplyRequestItem){
		return new SupplyRequestItemCO(supplyRequestItem);
	}
	
	this.sanitizeOriQRO = function(oriQRO){
		
	}
})
.service('SvcDeliveryReceiptService',["NHelper","DateUtil",function(NHelper,DateUtil){
	var DeliveryRequestCO = function(deliveryReceipt){
		var thisRef = this;
		thisRef.requestNumber = deliveryReceipt.supplyRequest.requestNumber;
		NHelper.ifPresent(deliveryReceipt.dateDelivered,function(dateDelivered){
			thisRef.dateDelivered = DateUtil.toStringIgnoreTZ(dateDelivered);
		})
		thisRef.itemBatchGroups = getItemBatchesCO(deliveryReceipt.itemBatches);
	}
	
	function getItemBatchesCO (dRItemBatches){
		var itemBatchGroups = [];
		NHelper.ifPresent(dRItemBatches,function(itemBatchesMap){
			for(var itemCode in itemBatchesMap){
				var itemBatches = [];
				itemBatchesMap[itemCode].forEach(function(itemBatch){
					itemBatches.push(angular.copy(itemBatch));
				})
				
				var batchGroup = {itemCode: itemCode, itemBatches: itemBatches};
				itemBatchGroups.push(batchGroup);
			}
		})
		return itemBatchGroups;
	}
	
	this.generateCO = function(deliveryReceipt){
		return new DeliveryRequestCO(deliveryReceipt);
	}
	
	this.sanitizeOrQRO = function(oRQRO){
		var body = oRQRO.body;
		NHelper.ifPresent(body.dateDelivered,function(oDte){
			body.dateDelivered = DateUtil.parseShortDate(oDte);
		});
		
		return oRQRO;
	}
	
	this.sanitizeEditedItemBatchGroup = function(itemBatchGroup){
		return itemBatchGroup.itemBatches;
	}
}])
.service('SvcDeliveredItemBatchGroupService',["NHelper","DateUtil",function(NHelper,DateUtil){
	var DeliveredItemBatchGroupCO = function(deliveredItemBatchGroup,isNew){
		var thisRef = this;
		thisRef.itemCode = deliveredItemBatchGroup.receivableDetails.itemCode;
		thisRef.otherItemBatches = deliveredItemBatchGroup.itemBatches;
	}
	
	function sanitizeItemBatch(itemBatch){
		var sanitizedItemBatch = {
			batchNumber: itemBatch.batchNumber,
			quantity: parseInt(itemBatch.quantity)
		}
		
		NHelper.ifPresent(itemBatch.rackingCode,function(warehouseLocation){
			sanitizedItemBatch.rackingCode = warehouseLocation.rackCode;
		})
		
		NHelper.ifPresent(itemBatch.dateStored,function(dateStored){
			sanitizedItemBatch.dateStored = DateUtil.toStringIgnoreTZ(dateStored);
		})
		
		return sanitizedItemBatch;
	}
	
	var DeliveredItemBatchGroupValCO = function(deliveredItemBatchGroup,isNew){
		var thisRef = this;
		thisRef.itemCode = deliveredItemBatchGroup.receivableDetails.itemCode;
		
		var editedBatchNumber = deliveredItemBatchGroup.editedItemBatch.batchNumber;
		NHelper.ifPresent(deliveredItemBatchGroup.itemBatches,function(itemBatches){
			thisRef.otherItemBatches = itemBatches.filter(function(iB){
				console.log('iB.batchNumber: '+iB.batchNumber)
				console.log('editedBatchNumber: '+editedBatchNumber)
				console.log('iB.batchNumber !== editedBatchNumber: '+iB.batchNumber !== editedBatchNumber)
				return iB.batchNumber !== editedBatchNumber;
			});
			
		})
		
		thisRef.editedItemBatch = sanitizeItemBatch(deliveredItemBatchGroup.editedItemBatch);
	}
	
	this.sanitizeItemBatch = sanitizeItemBatch;
	
	this.generateValidationCO = function(deliveredItemBatchGroup){
		return new DeliveredItemBatchGroupValCO(deliveredItemBatchGroup);
	}
	
	this.generateCO = function(deliveredItemBatchGroup){
		return new DeliveredItemBatchGroupCO(deliveredItemBatchGroup);
	}
	
	this.sanitizeOriQRO = function(oriQRO){
		var body = oRQRO.body;
		NHelper.ifPresent(body.dateStored,function(dateStored){
			body.dateStored = DateUtil.parseShortDate(dateStored);
		});
		
		return oRQRO;
	}
}])
.service('SvcRequestIssuanceService',["NHelper","DateUtil",function(NHelper,DateUtil){
	var SvcRequestIssuanceCO = function(requestIssuance){
		var thisRef = this;
		thisRef.requestNumber = requestIssuance.requestNumber;
		NHelper.ifPresent(requestIssuance.issuanceDate,function(issuanceDate){
			thisRef.issuanceDate = DateUtil.toStringIgnoreTZ(issuanceDate);
		})
		var handlingCustodian = requestIssuance.handlingCustodian;
		thisRef.custodianUsername = handlingCustodian.custodianDetails.userName;
		thisRef.custodianRemarks = handlingCustodian.remarks;
		thisRef.facilityCode = requestIssuance.facilityCodeName;
		thisRef.routeCode = requestIssuance.routeCode;
		thisRef.previousFacilityCode = requestIssuance.previousFacilityCodeName;
		
		thisRef.suppliedItemsGroups = getSuppliedItemsGroupsCO(requestIssuance.requestedItems);
	}
	
	function getSuppliedItemsGroupsCO (requestedItems){
		var suppliedItemsGroups = [];
		requestedItems.forEach(function(requestedItemsGrp){
			console.log("xxx: ")
			for(var key in requestedItemsGrp){
				console.log("key: "+key+", value: "+requestedItemsGrp[key])
			}
			var suppliedItemsGroup = {
				itemCode: requestedItemsGrp.itemCode,
				suppliedItems: requestedItemsGrp.suppliedItems
			}
			suppliedItemsGroups.push(suppliedItemsGroup)
		})
		return suppliedItemsGroups;
	}
	
	this.generateCO = function(requestIssuance){
		return new SvcRequestIssuanceCO(requestIssuance);
	}
	
	this.sanitizeOrQRO = function(oRQRO){
		var body = oRQRO.body;
		NHelper.ifPresent(body.requestDate,function(oDte){
			body.requestDate = DateUtil.parseShortDate(oDte);
		});
		
		NHelper.ifPresent(body.issuanceDate,function(oDte){
			body.issuanceDate = DateUtil.parseShortDate(oDte);
		});
		
		return oRQRO;
	}
	
	this.sanitizeSuppliedItemsGroup = function(suppliedItemsGroup){
		suppliedItemsGroup.suppliedItems.forEach(function(si){
			delete si['currentlyEdited']
		})
		
		return suppliedItemsGroup;
	}
}])
.service('SvcRequestedItemIssuanceService',function(){
	var SvcRequestedItemIssuanceCO = function(requestedItemIssuance){
		var thisRef = this;
		thisRef.itemCode = requestedItemIssuance.requestDetails.itemCode;
		thisRef.suppliedItems = getSuppliedItemsCO(requestedItemIssuance.suppliedItems);
	}
	
	var SvcRequestedItemIssuanceValidateCO = function(previousSuppliedQuantity, requestedItemIssuance){
		var thisRef = this;
		thisRef.itemCode = requestedItemIssuance.requestDetails.itemCode;
		thisRef.toBeValidatedSupplyItem = 
			getToBeValidatedCO(previousSuppliedQuantity, requestedItemIssuance.suppliedItems);
		thisRef.suppliedItems = getSuppliedItemsCO(requestedItemIssuance.suppliedItems);
		
	}
	
	function getToBeValidatedCO(previousSuppliedQuantity, suppliedItems){
		var toBeValidatedRaw = suppliedItems.find(function(suppliedItemsInstance){
			return suppliedItemsInstance.currentlyEdited;
		});
		var toBeValidated = angular.copy(toBeValidatedRaw);
		delete toBeValidated['currentlyEdited'];
		toBeValidated.previousSuppliedQuantity = previousSuppliedQuantity;
		return toBeValidated;
	}
	
	function getSuppliedItemsCO (suppliedItems){
		return suppliedItems.filter(function(suppliedItemsInstance){
			return true!==suppliedItemsInstance.currentlyEdited;
		}).map(function(suppliedItemsInstance){
			return {
				sourceBatchNumber: suppliedItemsInstance.sourceBatchNumber,
				suppliedQuantity: suppliedItemsInstance.suppliedQuantity,
				remarks: suppliedItemsInstance.remarks
			}
		});
	}
	
	this.generateCO = function(requestIssuance){
		return new SvcRequestedItemIssuanceCO(requestIssuance);
	}
	
	this.generateValidationCO = function(previousSuppliedQuantity, requestIssuance){
		return new SvcRequestedItemIssuanceValidateCO(previousSuppliedQuantity, requestIssuance);
	}
	
	this.resetEditedList = function(suppliedItems){
		return suppliedItems.map(function(si){
			si.currentlyEdited = false;
			return si;
		});
	}
})
.service('SvcPhysicalStockCountRequestService',["NHelper", function(NHelper){
	var SvcPhysicalStockCountRequestServiceCO = function(physicalStockCountRequest){
		var thisRef = this;
		thisRef.facilityCodeName = physicalStockCountRequest.facilityCodeName;
		thisRef.requestNumber = physicalStockCountRequest.requestNumber;
		
		NHelper.ifPresent(physicalStockCountRequest.physicalCountList, function(physicalCountList){
			thisRef.physicalCounts = physicalCountList.map(function(physicalCount){
				return {
					batchNumber: physicalCount.batchNumber,
					count: parseInt(physicalCount.count)
				}
			});
		});
	}
	
	this.generatePhysicalStockCountRequestCO = function(physicalStockCountRequest){
		return new SvcPhysicalStockCountRequestServiceCO(physicalStockCountRequest);
	}
	
	this.sanitizePSCRQRO = function(sFRQRO){
		var body = sFRQRO.body;
		if(!body) body = sFRQRO;
		NHelper.ifPresent(body.dateRequested,function(rDte){
			body.dateRequested = DateUtil.parseShortDate(rDte);
		});
		
		return sFRQRO;
	}
}]);