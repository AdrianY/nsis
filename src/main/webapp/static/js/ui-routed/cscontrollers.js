/**
 * 
 */
nsisApp.controller('CsCustomerRequestsCtrl',
		['$scope','$rootScope','$state','spinnerService','Prompter','DateUtil','CsCustomerRequest','requestType','message',
		 function($scope,$rootScope,$state,spinnerService,Prompter,DateUtil,CsCustomerRequest,requestType,message){
			
			function doClearFilter(){
				$scope.filter.status = null;
				$scope.filter.requestNumber = null;
				$scope.filter.toDate = null;
				$scope.filter.fromDate = null;
				$scope.filter.accountNumber = null;
				$scope.filter.accountName = null;
				$scope.filter.contactName = null;
				$scope.filter.facilityCodeName= null;
			}
			
			var initDone = false;
			function init(cb, filter, actualCB){
				$scope.filter = {};
				doClearFilter()
				$scope.filter.toDate = new Date();
				$scope.filter.fromDate = new Date();
				initDone = true;
				cb(filter, actualCB);
			}
			
			function actualReloadList(filter, cb){
				var scopeFilter = $scope.filter;
				spinnerService.show("nsisSpinner");
				filter.facilityCodeName = scopeFilter.facilityCodeName
				filter.requestType = requestType;
				filter.status = scopeFilter.status;
				filter.reqNumber = scopeFilter.requestNumber;
				filter.accountNumber = scopeFilter.accountNumber;
				filter.accountName = scopeFilter.accountName;
				filter.contactName = scopeFilter.contactName;
				if(scopeFilter.toDate)
					filter.toDate = DateUtil.toStringIgnoreTZ(scopeFilter.toDate);
				if(scopeFilter.fromDate){
					filter.fromDate = DateUtil.toStringIgnoreTZ(scopeFilter.fromDate);
				}
				CsCustomerRequest.get(filter,function(data){
					spinnerService.hide("nsisSpinner");
					cb(data);
				});
				$scope.triggerReload = false;
			}
			
			$scope.reloadList = function(filter, cb){
				if(!initDone){
					init(actualReloadList, filter, cb)
				}else
					actualReloadList(filter, cb);
			}
			
			$scope.requestType = requestType;
			
			$scope.clearFilter = function(){
				doClearFilter();
				$scope.triggerReload = true;
				$scope.$digest();
			}
			
			$scope.remove = function(requestNumber, cb){
				if(requestNumber){
					spinnerService.show("nsisSpinner");
					CsCustomerRequest.remove(
							{
								requestNumber:requestNumber,
								facilityCodeName:svcAssignments[0].facilityCodeName
							},
							function(){
								finalizeAction('Successfully removed request: '+requestNumber,'success');
								cb(true);
							},
							function(err){
								console.log(err)
								var errMsg = 'Failed to remove request: '+requestNumber + '. Reason is: ' + err.data;
								finalizeAction(errMsg,'danger');
								cb(false);
							}
					)
				}else{
					var errMsg = 'Cannot do action. Select first a request.';
					Prompter.promptActionState(errMsg,'danger');
					cb(false);
				}
			}
			
			$scope.view = function(requestNumber){
				spinnerService.show("nsisSpinner");
				CsCustomerRequest.get(
						{requestNumber:requestNumber},
						{}, 
						function(customerRequest){
							spinnerService.hide("nsisSpinner");
							$state.go('home.cscustomerrequestsform',{customerRequest: customerRequest});
						}
				);
			}
			
			$scope.add = function(){
				spinnerService.show("nsisSpinner");
				CsCustomerRequest.getTemplate({},function(customerRequest){
					spinnerService.hide("nsisSpinner");
					$state.go('home.cscustomerrequestsform',{customerRequest: customerRequest});
				});
			}
			
			if(message){
				Prompter.promptActionState(message, 'success');
			}
			
			function finalizeAction(msg, mode){
				spinnerService.hide("nsisSpinner");
				Prompter.promptActionState(msg, mode);
			}
			
			$scope.doTriggerReload = function(){
				$scope.triggerReload = true;
			}
		}]
)
.controller('CsCustomerRequestsFormCtrl',
		['$scope','$rootScope', '$state','spinnerService','Prompter','customerRequest',
		 function($scope,$rootScope, $state,spinnerService,Prompter,customerRequest){
			$scope.customerRequest = customerRequest;
			$scope.editMeta = customerRequest.editMeta;
			$scope.notifyLoading = function(){
				spinnerService.show("nsisSpinner");
			}
			$scope.notifyDoneLoading = function(){
				spinnerService.hide("nsisSpinner");
			}
			$scope.cancel = function(){
				//TODO place prompt here specially when its NEW or there are changes
				$state.go('home.cscustomerrequests');
			}
			$scope.save = function(){
				$scope.notifyLoading();
				$scope.customerRequest.save(function(result,err){
					var msg = '', mode = '';
					if(err){
						$scope.errorMeta = err.data;
						msg = 'Failed to update Request: '+$scope.customerRequest.body.requestNumber + " due to errors"
						mode = 'danger'
					}else{
						delete $scope['errorMeta'];
						$scope.customerRequest = result;
						msg = 'Successfully updated Request: '+$scope.customerRequest.body.requestNumber
						mode = 'success'
					}
					finalizeAction(msg, mode);
				});
			}
			
			$scope.submit = function(){
				$scope.notifyLoading();
				$scope.customerRequest.submit(function(result,err){
					if(err){
						$scope.errorMeta = err.data;
						var msg = 'Failed to submit Request: '+$scope.customerRequest.body.requestNumber + " due to errors"
						finalizeAction(msg, 'danger');
					}else{
						delete $scope['errorMeta'];
						$scope.customerRequest = result;
						$scope.editMeta = result.editMeta;
						var msg = 'Successfully submitted Request: '+$scope.customerRequest.body.requestNumber
						finalizeUnEditableAction(msg);
					}
				});
			}
			
			function finalizeUnEditableAction(msg){
				$state.go('home.cscustomerrequests',{message: msg})
			}
			
			function finalizeAction(msg, mode){
				$scope.notifyDoneLoading();
				Prompter.promptActionState(msg, mode);
			}
			
			$scope.submitConfirmMsg = "Are you sure you want to submit this customer request? "+ 
			"You will not be able to edit this request if no errors are found after this action."
		}]
)
.controller('CustomerAccountsListMCtrl', ["$scope", "$uibModalInstance","spinnerService", "customer", "CustomerAccountService",
    function ($scope, $uibModalInstance,spinnerService, customer, CustomerAccountService) {
	
	
	if(customer){
		$scope.filter = {
			accountNumber: customer.accountNumber,
			name: customer.name
		}
	}else{
		$scope.filter = {
				accountNumber: null,
				name: null
		}
	}
	
	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
	
	$scope.reloadList = function(params, cb){
		spinnerService.show("nsisSpinner");
		var filter = $scope.filter;
		params.accountNumber = filter.accountNumber;
		params.name = filter.name;
		CustomerAccountService.get(params,{},function(data){
			spinnerService.hide("nsisSpinner");
			cb(data)
		});
		$scope.triggerReload = false;
	}
	
	$scope.doTriggerReload = function(){
		$scope.triggerReload = true;
	}
	
	$scope.ok = function (customerData) {
		$uibModalInstance.close(customerData);
	};
	
}])
.controller('RequestItemMCtrl', ["$scope", "$uibModalInstance","NsisDirectiveScopeWrapper",
                                 "spinnerService", "requestedItem","mode","disabled", "InventoryItemService",
    function ($scope, $uibModalInstance,NsisDirectiveScopeWrapper,spinnerService, requestedItem,mode,disabled, InventoryItemService) {
	
	$scope.requestedItem = requestedItem;
	$scope.actionName = mode === 'ADD' ? 'ADD' : 'UPDATE';
	$scope.disabled = disabled;
	
	NsisDirectiveScopeWrapper.augmentScope($scope);	
	
	$scope.ok = function () {
		spinnerService.show("nsisSpinner");
		$scope.requestedItem.validate(function(result,err){
			if(err){
				$scope.meta = err.data;
				spinnerService.hide("nsisSpinner");
			}else{
				delete $scope['meta'];
				var data = {
					itemCode: $scope.requestedItem.details.codeName,
					quantity: $scope.requestedItem.requestDetails.quantity,
					remarks: $scope.requestedItem.requestDetails.remarks,
					type: $scope.requestedItem.details.type,
					description: $scope.requestedItem.details.description
				}
				spinnerService.hide("nsisSpinner");	
				$uibModalInstance.close(data);
			}
		})
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
	
}])