/**
 * 
 */
angular.module('nsisCsDomainServices', ['ngResource','ngCookies','nsisCsServices'])
.factory('CustomerAccountService', ["$resource",function($resource) {
	return $resource(
			mainUrl+'customeraccounts/:accountnumber', 
			{accountnumber:'@_accountNumber'},
			{}
	);
}])
.factory('CsCustomerRequest', ["$resource","NHelper","CsCustomerRequestService",function($resource,NHelper,CsCustomerRequestService) {

	var CsCustomerRequest = $resource(mainUrl+'customerrequests/:requestNumber', {requestNumber:'@_reqNumber'},
			{
				recalculateItems: {
					method: 'POST',
					url: mainUrl+'customerrequests/customerrequests/',
					headers: {'Content-Type':'application/json','xAction':'CALCULATE'}, 
					isArray:true
				},
				get: {
		            method: 'GET',
		            transformResponse: transFxForOR
		        },
				update:{method:'PUT'},
				getTemplate: {
					method: 'POST',
					url: mainUrl+'customerrequests/customerrequests/',
					headers: {'Content-Type':'application/json','xAction':'TEMPLATE'}
				},
				create:{
					method:'POST',
					url: mainUrl+'customerrequests/customerrequests/',
					headers: {'Content-Type':'application/json','xAction':'CREATE'}
				},
		        submit: {
		        	method: 'POST',
					url: mainUrl+'customerrequests/customerrequests/',
					headers: {'Content-Type':'application/json','xAction':'SUBMIT'}
		        }
			}
	);
	
	function transFxForOR(data, headers){
		var dataFromJSON = angular.fromJson(data);
		
    	if(!!(dataFromJSON && !(dataFromJSON.rows))){
    		dataFromJSON = CsCustomerRequestService.sanitizesFRQRO(dataFromJSON)
    	}
    		
        return dataFromJSON;
	}
	
	CsCustomerRequest.prototype ={
		showRecalculatedItems: function(cb){
			var currentOR = this.body;
			if(!currentOR) currentOR = this;
			var customerRequestCO = CsCustomerRequestService.generateCustomerRequestCO(currentOR);
			console.log('customerRequestCO: '+customerRequestCO)
			console.log('customerRequestCO.requestedItems: '+customerRequestCO.requestedItems)
			NHelper.ifPresent(customerRequestCO.requestedItems, function(res){
				console.log('res.length: '+res.length)
				if(res.length > 0){
					CsCustomerRequest.recalculateItems({},
							customerRequestCO,function(result){
						if(result)cb(result);
					});
				}else{
					console.log("HERE")
					cb([]);
				}
			})
		},
		removeRequestedItems: function(requestedItemsCodesArr, cb){
			var thisRef = this;
			var remainingRequestedItem = thisRef.body.requestedItems.filter(function(item){
				var match = requestedItemsCodesArr.find(function(itemCode){
					return itemCode === item.itemCode;
				})
				return !match;
			});
			
			thisRef.body.requestedItems = remainingRequestedItem;
			cb();
		},
		saveRequestedItem: function(requestedItem,mode,cb){
			var thisRef = this;
			var existingRequestedItem = thisRef.body.requestedItems.find(function(item){
				return item.itemCode === requestedItem.itemCode;
			});
			
			if(existingRequestedItem){
				var getQuantity = function(prevQuantity, quantity){
					return 'ADD' === mode ? 
							parseInt(prevQuantity) + parseInt(quantity) : 
							parseInt(quantity);
				}
				
				var thereIsChange = existingRequestedItem.quantity !== requestedItem.quantity || 
				existingRequestedItem.remarks !== requestedItem.remarks;
				
				existingRequestedItem.quantity = getQuantity(existingRequestedItem.quantity, requestedItem.quantity);
				
				existingRequestedItem.remarks = angular.copy(requestedItem.remarks);
				
				cb(thereIsChange, existingRequestedItem);
			}else{
				var newRequestedItem = {
						itemCode: requestedItem.itemCode,
						quantity: parseInt(requestedItem.quantity),
						remarks: requestedItem.remarks
				}
				thisRef.body.requestedItems.push(newRequestedItem);
				cb(true, newRequestedItem);
			}
		},
		save: function(cb){
			var currentOR = this.body;
			if(!currentOR) currentOR = this;
			var customerRequestCO = CsCustomerRequestService.generateCustomerRequestCO(currentOR);
			if('NEW'===currentOR.status){
				CsCustomerRequest.create(
						{},customerRequestCO,
						function(result){
							if(cb && result){
								cb(new CsCustomerRequest(CsCustomerRequestService.sanitizesFRQRO(result)));
							}
						},function(err){
							cb(null,err)
				});
			}else if('DRAFT'===currentOR.status){
				CsCustomerRequest.update(
					{requestNumber:this.body.requestNumber},
					customerRequestCO,function(result){
					if(cb && result){
						cb(new CsCustomerRequest(CsCustomerRequestService.sanitizesFRQRO(result)));
					}
				},function(err){
					cb(null,err)
				});
			}
		},
		submit: function(cb){
			var customerRequestCO = CsCustomerRequestService.generateCustomerRequestCO(this.body);
			console.log('customerRequestCO: '+customerRequestCO)
			CsCustomerRequest.submit(
					{},customerRequestCO,function(result){
						if(cb && result){
							cb(new CsCustomerRequest(CsCustomerRequestService.sanitizesFRQRO(result)));
						}
					},function(err){
						cb(null,err)
					});
		}
	}
	
	return CsCustomerRequest;
}])
.factory('CustomerRequestItem', ["$resource","CustomerRequestItemService",function($resource,CustomerRequestItemService){
	var CustomerRequestItem = $resource(mainUrl + 'customerrequests/:requestNumber/items:/itemCode', {requestNumber:'@requestNumber'}, {
		validate: {
			method: 'POST',
			params: {requestNumber:'@requestNumber'},
			url: mainUrl+'customerrequests/:requestNumber/items/items',
			headers: {'Content-Type':'application/json','xAction':'VALIDATE'}
		}
	})
	
	CustomerRequestItem.prototype = {
		validate: function(cb){
			var thisRef = this;
			console.log('validating sri for or: '+this.requestNumber)
			if(0 >= parseInt(thisRef.quantity) &&  cb){
				var err = {
					data: {
						'requestedItem.quantity': "Must be at least 1"
					}
				}
				cb(null,err);
			}else if(cb){
				 cb(null);
			}
		}
	}
	
	return CustomerRequestItem;
}])