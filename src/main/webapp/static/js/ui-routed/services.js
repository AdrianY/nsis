/**
 * 
 */
angular.module('nsisServices', [])
.service('CookieStore',["$cookies",function($cookies){
	this.extractSvcAssignments = function(cb){
		var cOSvcAssignments = $cookies.getObject("facilityAssignments");
		var result = cOSvcAssignments.filter(function(facAssign){return 'SVC' === facAssign.type;})
		if(cb)
			cb(result);
		else
			return result;
	}
}])
.service('Prompter',["$uibModal",function($uibModal){
	this.promptActionState = function(message, mode){
		$uibModal.open({
			animation: true,
			templateUrl: parentPopUri + 'action-notif.html',
			controller: 'AlertModalCtrl',
			resolve: {
				message: function () {return message;},
				mode: function() {return mode;}
			}
		});
	}
}])
.service('NsisDirectiveScopeWrapper',function(){

	this.augmentScope = function($scope){
		$scope.$watch(
				'$parent.editMeta',
				function(newValue, oldValue) {
					$scope.isDisabled = $scope.disabled || setDisable(newValue, $scope.qroId);
				}
		);
		
		$scope.$watch(
				'disabled',
				function(newValue, oldValue) {
					var alwaysEnabled = $scope.alwaysEnabled;
					if(!alwaysEnabled) alwaysEnabled = false;
					$scope.isDisabled = ($scope.disabled || setDisable(newValue, $scope.qroId)) && !alwaysEnabled;
				}
		);

		$scope.$watch(
				'$parent.errorMeta',
				function(newValue, oldValue) {
					var currentState = afterCommandState($scope.coId, newValue);
					if('has-error' === currentState.state){
						$scope.xstate = 'has-error';
						$scope.hasProblem = true;
						$scope.problemMsg = currentState.message;
					}else{
						$scope.xstate = '';
						$scope.hasProblem = false;
						$scope.problemMsg = null;
					}
				}
		);
	}
	
	this.augmentBtnScope = function($scope){
		$scope.$watch(
				'$parent.editMeta',
				function(newValue, oldValue) {
					var alwaysEnabled = $scope.alwaysEnabled;
					if(!alwaysEnabled) alwaysEnabled = false;
					$scope.isDisabled = ($scope.disabled || setDisable(newValue)) && !alwaysEnabled;
				}
		);
		
		$scope.$watch(
				'disabled',
				function(newValue, oldValue) {
					var alwaysEnabled = $scope.alwaysEnabled;
					if(!alwaysEnabled) alwaysEnabled = false;
					$scope.isDisabled = ($scope.disabled || setDisable(newValue, $scope.qroId)) && !alwaysEnabled;
				}
		);

		$scope.$watch(
				'$parent.errorMeta',
				function(newValue, oldValue) {
					var currentState = afterCommandState($scope.coId, newValue);
					$scope.xstate = currentState.state;
				}
		);
	}
	
	this.isDisabled = setDisable;
	
	this.getAfterCommandState = afterCommandState;
	
	function setDisable(editMeta, qroId){
		var type = editMeta.type;
		var fieldList = editMeta.fieldList;
		
		var returnValue = false;
		if("NONE" === type) returnValue = true;
		if("SPECIFIC" === type && (qroId && !isInFieldList(qroId))) returnValue = true;
		else if("EXCEPT"  === type && isInFieldList(qroId)) returnValue = true;
		
		function isInFieldList(id){
			return -1 != fieldList.indexOf(id)
		}
		
		return returnValue;
	}
	
	function afterCommandState(coId, errorMeta){
		var exists = !!errorMeta && !!(errorMeta[coId])
		var returnValue = {};
		if(exists){
			returnValue = {message: errorMeta[coId], state:'has-error'};
		}else{
			returnValue = {state:'ok'};
		}
		return returnValue;
	}
})
.service('CloneHelper',function(){
	this.copyEquivalentAttributes = function(dest, src, attributeArr){
		attributeArr.forEach(function(attr){
			dest[attr] = src[attr];
		});	
	}
	
	this.generateWithSpecifiedAttr = function(src, attributeArr){
		var data = {};
		this.copyEquivalentAttributes(data, src, attributeArr);
		return data;
	}
})
.service('DateUtil',function(){
	this.toStringIgnoreTZ = function(origDate){
		var month = origDate.getMonth() + 1;
		month = month < 10 ? "0" + month : month;
		var day = origDate.getDate();
		day = day < 10 ? "0" + day : day;
		return month+"/"+day+"/"+origDate.getFullYear();
	}

	this.parseShortDate = function(shortDate){
		var parts = shortDate.split('/');
		var year = parseInt(parts[2]);
		var monthRaw = parseInt(parts[0]);
		var day = parseInt(parts[1]);
		return new Date(year, monthRaw-1, day);
	}
	
	this.parseAsNoSlashShortDate = function(date){
		var yyyy = date.getFullYear().toString();
		var mm = (date.getMonth()+1).toString(); // getMonth() is zero-based
		var dd  = date.getDate().toString();
		return (mm[1]?mm:"0"+mm[0]) + (dd[1]?dd:"0"+dd[0]) + yyyy; // padding
	}
})
.service('OrderRequestService',["CloneHelper","NHelper","DateUtil",function(CloneHelper,NHelper,DateUtil){
	var OrderRequestCO = function(orderRequest){
		console.log('orderRequest: ' +orderRequest)
		var thisRef = this;
		
		CloneHelper.copyEquivalentAttributes(
				thisRef, orderRequest, 
				['orderNumber','facilityId','orderType','orderCycle',
				 'currency','status']);

		NHelper.ifPresent(orderRequest.deliveryDate,function(deliveryDate){
			thisRef.deliveryDate = DateUtil.toStringIgnoreTZ(deliveryDate);
		})
		
		NHelper.ifPresent(orderRequest.orderDate,function(orderDate){
			thisRef.orderDate = DateUtil.toStringIgnoreTZ(orderDate);
		})
				
		NHelper.ifPresent(orderRequest.orderBy,function(orderBy){
			thisRef.orderByUsername = orderBy.userName;
		});
		
		NHelper.ifPresent(orderRequest.approver, function(approver){
			thisRef.approver = {
				remarks: approver.remarks	
			};
			NHelper.ifPresent(approver.approverDetails, function(details){
				thisRef.approver.username = approver.approverDetails.userName
			});
		});
		
		NHelper.ifPresent(orderRequest.recipient, function(recipient){
			thisRef.recipient = {
				deliveryAddress: recipient.deliveryAddress
			};
			NHelper.ifPresent(recipient.recipientDetails, function(recipientDetails){
				thisRef.recipient.username = recipientDetails.userName,
				thisRef.recipient.contactNumber = recipientDetails.contactNumber,
				thisRef.recipient.emailAddress = recipientDetails.emailAddress
			});
		});
		thisRef.itemsRequested = orderRequest.itemsRequested;
	}
	
	this.generateOrderRequestCO = function(orderRequest){
		return new OrderRequestCO(orderRequest);
	}
	
	this.sanitizeOrQRO = function(oRQRO){
		var body = oRQRO.body;
		if(!body) body = oRQRO;
		NHelper.ifPresent(body.orderDate,function(oDte){
			body.orderDate = DateUtil.parseShortDate(oDte);
		});
		
		NHelper.ifPresent(body.deliveryDate,function(dDte){
			body.deliveryDate = DateUtil.parseShortDate(dDte);
		});
		
		return oRQRO;
	}
}])
.service('OrderRequestItemService',function(){
	var OrderRequestItemCO = function(orderRequestItem){
		var thisRef = this;
		thisRef.facilityId = orderRequestItem.facilityId;
		thisRef.details = {
			itemCode: orderRequestItem.itemCode,
			quantity: orderRequestItem.quantity,
			remarks: orderRequestItem.remarks
		}
	}
	
	this.generateOrderRequestItemCO = function(orderRequestItem){
		return new OrderRequestItemCO(orderRequestItem);
	}
	
	this.sanitizeOriQRO = function(oriQRO){
		
	}
})
.service('DeliveryReceiptService',["NHelper","DateUtil",function(NHelper,DateUtil){
	var DeliveryRequestCO = function(deliveryReceipt){
		var thisRef = this;
		thisRef.orderNumber = deliveryReceipt.orderRequest.orderNumber;
		NHelper.ifPresent(deliveryReceipt.dateDelivered,function(dateDelivered){
			thisRef.dateDelivered = DateUtil.toStringIgnoreTZ(dateDelivered);
		})
		thisRef.itemBatchGroups = getItemBatchesCO(deliveryReceipt.itemBatches);
	}
	
	function getItemBatchesCO (dRItemBatches){
		var itemBatchGroups = [];
		NHelper.ifPresent(dRItemBatches,function(itemBatchesMap){
			for(var itemCode in itemBatchesMap){
				var itemBatches = [];
				itemBatchesMap[itemCode].forEach(function(itemBatch){
					itemBatches.push(angular.copy(itemBatch));
				})
				
				var batchGroup = {itemCode: itemCode, itemBatches: itemBatches};
				itemBatchGroups.push(batchGroup);
			}
		})
		return itemBatchGroups;
	}
	
	this.generateCO = function(deliveryReceipt){
		return new DeliveryRequestCO(deliveryReceipt);
	}
	
	this.sanitizeOrQRO = function(oRQRO){
		var body = oRQRO.body;
		NHelper.ifPresent(body.dateDelivered,function(oDte){
			body.dateDelivered = DateUtil.parseShortDate(oDte);
		});
		
		return oRQRO;
	}
	
	this.sanitizeEditedItemBatchGroup = function(itemBatchGroup){
		return itemBatchGroup.itemBatches;
	}
}])
.service('DeliveredItemBatchGroupService',["NHelper","DateUtil",function(NHelper,DateUtil){
	var DeliveredItemBatchGroupCO = function(deliveredItemBatchGroup,isNew){
		var thisRef = this;
		thisRef.orderNumber = deliveredItemBatchGroup.orderDetails.orderNumber;
		thisRef.itemCode = deliveredItemBatchGroup.receivableDetails.itemCode;
		thisRef.otherItemBatches = deliveredItemBatchGroup.itemBatches;
		
		thisRef.editedItemBatch = sanitizeItemBatch(deliveredItemBatchGroup.editedItemBatch);
	}
	
	function sanitizeItemBatch(itemBatch){
		var sanitizedItemBatch = {
			batchNumber: itemBatch.batchNumber,
			quantity: parseInt(itemBatch.quantity)
		}
		
		NHelper.ifPresent(itemBatch.rackingCode,function(warehouseLocation){
			sanitizedItemBatch.rackingCode = warehouseLocation.rackCode;
		})
		
		NHelper.ifPresent(itemBatch.dateStored,function(dateStored){
			sanitizedItemBatch.dateStored = DateUtil.toStringIgnoreTZ(dateStored);
		})
		
		return sanitizedItemBatch;
	}
	
	var DeliveredItemBatchGroupValCO = function(deliveredItemBatchGroup,isNew){
		var thisRef = this;
		thisRef.orderNumber = deliveredItemBatchGroup.orderDetails.orderNumber;
		thisRef.itemCode = deliveredItemBatchGroup.receivableDetails.itemCode;
		
		var editedBatchNumber = deliveredItemBatchGroup.editedItemBatch.batchNumber;
		NHelper.ifPresent(deliveredItemBatchGroup.itemBatches,function(itemBatches){
			thisRef.otherItemBatches = itemBatches.filter(function(iB){
				return iB.batchNumber !== editedBatchNumber;
			});
		})
		
		thisRef.editedItemBatch = sanitizeItemBatch(deliveredItemBatchGroup.editedItemBatch);
	}
	
	this.sanitizeItemBatch = sanitizeItemBatch;
	
	this.generateValidationCO = function(deliveredItemBatchGroup){
		return new DeliveredItemBatchGroupValCO(deliveredItemBatchGroup);
	}
	
	this.generateCO = function(deliveredItemBatchGroup){
		return new DeliveredItemBatchGroupCO(deliveredItemBatchGroup);
	}
	
	this.sanitizeOriQRO = function(oriQRO){
		var body = oRQRO.body;
		NHelper.ifPresent(body.dateStored,function(dateStored){
			body.dateStored = DateUtil.parseShortDate(dateStored);
		});
		
		return oRQRO;
	}
}])
.service('RequestIssuanceService',["NHelper","DateUtil",function(NHelper,DateUtil){
	var RequestIssuanceCO = function(requestIssuance){
		var thisRef = this;
		thisRef.requestNumber = requestIssuance.requestNumber;
		NHelper.ifPresent(requestIssuance.issuanceDate,function(issuanceDate){
			thisRef.issuanceDate = DateUtil.toStringIgnoreTZ(issuanceDate);
		})
		var handlingCustodian = requestIssuance.handlingCustodian;
		thisRef.custodianUsername = handlingCustodian.custodianDetails.userName;
		thisRef.custodianRemarks = handlingCustodian.remarks;
		
		thisRef.suppliedItemsGroups = getSuppliedItemsGroupsCO(requestIssuance.requestedItems);
	}
	
	function getSuppliedItemsGroupsCO (requestedItems){
		var suppliedItemsGroups = [];
		requestedItems.forEach(function(requestedItemsGrp){
			var suppliedItemsGroup = {
				itemCode: requestedItemsGrp.itemCode,
				suppliedItems: requestedItemsGrp.suppliedItems
			}
			suppliedItemsGroups.push(suppliedItemsGroup)
		})
		return suppliedItemsGroups;
	}
	
	this.generateCO = function(requestIssuance){
		return new RequestIssuanceCO(requestIssuance);
	}
	
	this.sanitizeOrQRO = function(oRQRO){
		var body = oRQRO.body;
		NHelper.ifPresent(body.requestDate,function(oDte){
			body.requestDate = DateUtil.parseShortDate(oDte);
		});
		
		NHelper.ifPresent(body.issuanceDate,function(oDte){
			body.issuanceDate = DateUtil.parseShortDate(oDte);
		});
		
		return oRQRO;
	}
	
	this.sanitizeSuppliedItemsGroup = function(suppliedItemsGroup){
		suppliedItemsGroup.suppliedItems.forEach(function(si){
			delete si['currentlyEdited']
		})
		
		return suppliedItemsGroup;
	}
}])
.service('RequestedItemIssuanceService',function(){
	var RequestedItemIssuanceCO = function(requestedItemIssuance){
		var thisRef = this;
		thisRef.itemCode = requestedItemIssuance.requestDetails.itemCode;
		thisRef.suppliedItems = getSuppliedItemsCO(requestedItemIssuance.suppliedItems);
	}
	
	var RequestedItemIssuanceValidateCO = function(previousSuppliedQuantity, requestedItemIssuance){
		var thisRef = this;
		thisRef.itemCode = requestedItemIssuance.requestDetails.itemCode;
		thisRef.toBeValidatedSupplyItem = 
			getToBeValidatedCO(previousSuppliedQuantity, requestedItemIssuance.suppliedItems);
		thisRef.suppliedItems = getSuppliedItemsCO(requestedItemIssuance.suppliedItems);
	}
	
	function getToBeValidatedCO(previousSuppliedQuantity, suppliedItems){
		var toBeValidatedRaw = suppliedItems.find(function(suppliedItemsInstance){
			return suppliedItemsInstance.currentlyEdited;
		});
		var toBeValidated = angular.copy(toBeValidatedRaw);
		delete toBeValidated['currentlyEdited'];
		toBeValidated.previousSuppliedQuantity = previousSuppliedQuantity;
		return toBeValidated;
	}
	
	function getSuppliedItemsCO (suppliedItems){
		return suppliedItems.filter(function(suppliedItemsInstance){
			return !suppliedItemsInstance.currentlyEdited;
		}).map(function(suppliedItemsInstance){
			return {
				sourceBatchNumber: suppliedItemsInstance.sourceBatchNumber,
				suppliedQuantity: suppliedItemsInstance.suppliedQuantity,
				remarks: suppliedItemsInstance.remarks
			}
		});
	}
	
	this.generateCO = function(requestIssuance){
		return new RequestedItemIssuanceCO(requestIssuance);
	}
	
	this.generateValidationCO = function(previousSuppliedQuantity, requestIssuance){
		return new RequestedItemIssuanceValidateCO(previousSuppliedQuantity, requestIssuance);
	}
	
	this.resetEditedList = function(suppliedItems){
		return suppliedItems.map(function(si){
			si.currentlyEdited = false;
			return si;
		});
	}
})
.service('NHelper',function(){
	this.ifPresent = function(data,fx){
		if(data) fx(data);
	}
})
.service('RerouteDetector',[
    "OrderRequest","DeliveryReceipt","RequestIssuance",
    "SvcSupplyRequest","SvcDeliveryReceipt","SvcRequestIssuance",
    "CsCustomerRequest"
    ,function(
    		OrderRequest,DeliveryReceipt,RequestIssuance,
    	    SvcSupplyRequest,SvcDeliveryReceipt,SvcRequestIssuance,
    	    CsCustomerRequest
    ){
    	var resourceHandlerMap = {
    		"home.orderrequestsform":{
    			clazz: [OrderRequest],
    			params: ['orderRequest'],
    			redirectUrl: 'home.orderrequests'
    		},
    		"home.submittedorderrequestsform":{
    			clazz: [OrderRequest],
    			params: ['orderRequest'],
    			redirectUrl: 'home.submittedorderrequests'
    		},
    		"home.deliveryreceiptsform":{
    			clazz: [DeliveryReceipt],
    			params: ['deliveryReceipt'],
    			redirectUrl: 'home.inventorymaintenance.deliveryreceipts'
    		},
    		"home.requesttowhsissuanceform":{
    			clazz: [RequestIssuance,SvcSupplyRequest],
    			params: ['supplyRequest','requestIssuance'],
    			redirectUrl: 'home.requesttowhsissuance'
    		},
    		"home.svcsupplyrequestsform":{
    			clazz: [SvcSupplyRequest],
    			params: ['supplyRequest'],
    			redirectUrl: 'home.svcsupplyrequests'
    		},
    		"home.submittedsupplyrequestsform":{
    			clazz: [SvcSupplyRequest],
    			params: ['supplyRequest'],
    			redirectUrl: 'home.submittedsvcsupplyrequests'
    		},
    		"home.svcdeliveryreceiptsform":{
    			clazz: [SvcDeliveryReceipt],
    			params: ['svcDeliveryReceipt'],
    			redirectUrl: 'home.svcinventorymaintenance.deliveryreceipts'
    		},
    		"home.requesttosvcissuanceform":{
    			clazz: [SvcRequestIssuance,CsCustomerRequest],
    			params: ['customerRequest','requestIssuance'],
    			redirectUrl: 'home.requesttosvcissuance'
    		},
    		"home.cscustomerrequestsform":{
    			clazz: [CsCustomerRequest],
    			params: ['customerRequest'],
    			redirectUrl: 'home.cscustomerrequests'
    		}
    	}
    	
    	this.toBeRerouted = function(stateUrl, stateParam){
    		var resourceHandler = resourceHandlerMap[stateUrl];
    		
    		var hasViolation = false
    		if(null != resourceHandler){
    			var paramList = resourceHandler.params;
    			var classList = resourceHandler.clazz;
    			
    			var invalidParam = paramList.find(function(param){
    				var arr = classList.filter(function(clazz){
    					return (stateParam[param] instanceof clazz)
    				})
    				return (null == arr || 0 >= arr.length);
    			});
    			
    			hasViolation = null != invalidParam;
    		}
    		
    		return hasViolation;
    	}
    	
    	this.getRerouteUrl = function(stateUrl){
    		var resourceHandler = resourceHandlerMap[stateUrl];
    		return (null != resourceHandler) ? resourceHandler['redirectUrl'] : null;
    	}
}]);