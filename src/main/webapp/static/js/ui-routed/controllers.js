/**
 * 
 */
var parentPopUri = '/NSIS/static/view/ui-routed/modals/'
nsisApp.controller('SigninCtrl', ['$scope','$rootScope','$cookies', '$state', 'PrincipalService','AuthWebService', function($scope,$rootScope,$cookies, $state, PrincipalService, AuthWebService) {
	console.log('SigninCtrl called');

	//$scope.rememberme = true;
	$scope.unauthorized = false;
	$scope.signin = function() {
		var credentials = $scope.credentials;
		AuthWebService.authenticate(
				$.param({username: credentials.username, password: credentials.password}), 
				function(authenticationResult) {
					var authToken = authenticationResult.token;
					$rootScope.authToken = authToken;
					$scope.unauthorized = false;
					AuthWebService.get(function(user) {
						var identity = {user: user, authToken: authToken};
						$cookies.putObject("roles",user.roles);
						PrincipalService.cacheIdentity(identity);

						if ($scope.returnToState) {
							var returnStateName = $scope.returnToState.name;
							if(returnStateName == 'signin') returnStateName = 'home'
							$state.go(returnStateName, $scope.returnToStateParams);
						}
						else $state.go('home');
					});
				},
				function(errorResult) {
					if(errorResult.status === 401) {  
						$scope.unauthorized = true;

						if(!$scope.$$phase) {
							$scope.$apply()
						}
					}
				}
		);
	};
}
])
.controller('HomeCtrl', ['$scope','$rootScope', '$state','$cookies','AuthWebService',
                         function($scope,$rootScope,$state,$cookies,AuthWebService) {
	
	$scope.signout = function() {
		delete $rootScope.user;
		delete $rootScope.authToken;
		$cookies.remove('authToken');
		$cookies.remove('identity');
		$cookies.remove('toState');
		$cookies.remove('toStateParams');
		$cookies.remove('roles');
		$cookies.remove('username');
		$cookies.remove('facilityAssignments');
		$state.go('signin');
	};
	
	var roles = $cookies.getObject("roles");
	function rolesContains(roleCode){
		return roles.find(function(role){
			return role.codeName === roleCode
		})
	}
	
	var possiblePanelItemsMap = {
		"WHPERSONNEL": {
        	name: "Warehouse module",
        	desc: "Warehouse",
        	subitems: [
        	           {
        	        	   state: "home.inventorymaintenance.inventoryonhand",
        	        	   desc: "Inv. Maintenance"
        	           },
        	           {
        	        	   state: ".orderrequests",
        	        	   desc: "Order Requests"
        	           },
        	           {
        	        	   state: ".requesttowhsissuance",
        	        	   desc: "Requests Issuance"
        	           },
        	           {
        	        	   state: ".maintenance",
        	        	   desc: "Admin"
        	           }
        	]
        },
        "APPROVER": {
        	name: "Requests Submitted",
        	desc: "Requests",
        	subitems: [
					{
						state: ".submittedorderrequests",
						desc: "Order Requests"
					},
					{
						state: ".submittedsvcsupplyrequests",
						desc: "Supply Requests"
					}
        	]
        },
        "SVCPERSONNEL":  {
        	name: "Service Center module",
        	desc: "Service Center",
        	subitems: [
        	           {
        	        	   state: "home.svcinventorymaintenance.deliveryreceipts",
        	        	   desc: "Inv. Maintenance"
        	           },
        	           {
        	        	   state: ".svcsupplyrequests",
        	        	   desc: "Supply Requests"
        	           },
        	           {
        	        	   state: ".requesttosvcissuance",
        	        	   desc: "Requests Issuance"
        	           },
        	           {
        	        	   state: ".svcmaintenance",
        	        	   desc: "Admin"
        	           }
        	]
        },
        "CSPERSONNEL":  {
        	name: "Customer Service module",
        	desc: "Customer Service",
        	subitems: [
        	           {
        	        	   state: ".cscustomerrequests",
        	        	   desc: "Customer Requests"
        	           }
        	]
        },
        "SPPERSONNEL":  {
        	name: "Service Point module",
        	desc: "Service Point",
        	subitems: [
        	           {
        	        	   state: ".cscustomerrequests",
        	        	   desc: "Supply Requests"
        	           }
        	]
        },
        "CRPERSONNEL":  {
        	name: "Courier module",
        	desc: "Courier",
        	subitems: [
        	           {
        	        	   state: ".cscustomerrequests",
        	        	   desc: "Supply Requests"
        	           }
        	]
        }
	}
	
	var panelItems = [];
	function startPanel(){
		if(rolesContains("WHPERSONNEL")) panelItems.push(possiblePanelItemsMap["WHPERSONNEL"]);
		if(rolesContains("SVCPERSONNEL")) panelItems.push(possiblePanelItemsMap["SVCPERSONNEL"]);
		if(rolesContains("APPROVER")) panelItems.push(possiblePanelItemsMap["APPROVER"]);
		if(rolesContains("CSPERSONNEL")) {
			panelItems.push(possiblePanelItemsMap["CSPERSONNEL"]);
		}
		if(rolesContains("SPPERSONNEL")) {
			panelItems.push(possiblePanelItemsMap["SPPERSONNEL"]);
		}
		if(rolesContains("CRPERSONNEL")) {
			panelItems.push(possiblePanelItemsMap["CRPERSONNEL"]);
		}
	};
	
	startPanel();
	
	$scope.items = panelItems;

	$scope['default'] = $scope.items[0];
	
	$scope.setCurrent = function(currentState){
    	var theState = 'home'+currentState;
    	var myclass = 'not-selected';
    	if($state.is(theState))
    		myclass = 'selected';

    	return myclass;
    };
    
    $scope.status = {
    		isFirstOpen: true,
    		isFirstDisabled: false
    };
}
])
.controller('ItemCtrl',
		['$scope', function (scope) {
            scope.$parent.isopen = (scope.$parent['default'] === scope.item);

            scope.$watch('isopen', function (newvalue, oldvalue, scope) {
                scope.$parent.isopen = newvalue;
            });
        }]
)
.controller('OrderRequestsCtrl',
		['$scope','$rootScope', '$state','spinnerService','Prompter','OrderRequest',"message",
		 function($scope,$rootScope, $state,spinnerService,Prompter,OrderRequest,message){
			$scope.reloadList = function(filter, cb){
				spinnerService.show("nsisSpinner")
				OrderRequest.get(filter,function(data){
					spinnerService.hide("nsisSpinner")
					cb(data)
				});
			}
			
			$scope.remove = function(orderNumber, cb){
				if(orderNumber){
					spinnerService.show("nsisSpinner")
					OrderRequest.remove({orderNumber:orderNumber},
							function(){
								finalizeAction('Successfully removed OR: '+orderNumber,'success');
								cb(true);
							},
							function(err){
								var errMsg = 'Failed to remove OR: '+orderNumber + '. Reason is: ' + err.data;
								finalizeAction(errMsg,'danger');
								cb(false);
							}
					)
				}else{
					var errMsg = 'Cannot do action. Select first an OR.';
					Prompter.promptActionState(errMsg,'danger');
					cb(false);
				}
			}
			
			$scope.view = function(orderNumber){
				spinnerService.show("nsisSpinner");
				OrderRequest.get({orderNumber:orderNumber},{}, function(orderRequest){
					spinnerService.hide("nsisSpinner");
					$state.go('home.orderrequestsform',{orderRequest: orderRequest});
				});
			}
			
			$scope.add = function(){
				spinnerService.show("nsisSpinner");
				OrderRequest.getTemplate(function(orderRequest){
					spinnerService.hide("nsisSpinner");
					$state.go('home.orderrequestsform',{orderRequest: orderRequest});
				});
			}
			
			if(message){
				Prompter.promptActionState(message, 'success');
			}
			
			function finalizeAction(msg, mode){
				spinnerService.hide("nsisSpinner");
				Prompter.promptActionState(msg, mode);
			}
		}]
)
.controller('OrderRequestsFormCtrl',
		['$scope','$rootScope', '$state','spinnerService','Prompter','orderRequest',
		 function($scope,$rootScope, $state,spinnerService,Prompter,orderRequest){
			$scope.orderRequest = orderRequest;
			$scope.editMeta = orderRequest.editMeta;
			$scope.notifyLoading = function(){
				spinnerService.show("nsisSpinner");
			}
			$scope.notifyDoneLoading = function(){
				spinnerService.hide("nsisSpinner");
			}
			$scope.cancel = function(){
				//TODO place prompt here specially when its NEW or there are changes
				$state.go('home.orderrequests');
			}
			
			$scope.$watch('orderRequest.body.orderType',function(nv,ov){
				if('Local' === nv){
					$scope.orderRequest.body.currency = 'PHP';
				}
			})
			
			$scope.save = function(){
				$scope.notifyLoading();
				$scope.orderRequest.save(function(result,err){
					var msg = '', mode = '';
					if(err){
						$scope.errorMeta = err.data;
						msg = 'Failed to update OR: '+$scope.orderRequest.body.orderNumber + " due to errors"
						mode = 'danger'
					}else{
						delete $scope['errorMeta'];
						$scope.orderRequest = result;
						msg = 'Successfully updated OR: '+$scope.orderRequest.body.orderNumber
						mode = 'success'
					}
					finalizeAction(msg, mode);
				});
			}
			
			$scope.submit = function(){
				$scope.notifyLoading();
				$scope.orderRequest.submit(function(result,err){
					if(err){
						$scope.errorMeta = err.data;
						var msg = 'Failed to submit OR: '+$scope.orderRequest.body.orderNumber + " due to errors"
						finalizeAction(msg, 'danger');	
					}else{
						delete $scope['errorMeta'];
						$scope.orderRequest = result;
						$scope.editMeta = result.editMeta;
						var msg = 'Successfully submitted OR: '+$scope.orderRequest.body.orderNumber
						finalizeUnEditableAction(msg);
					}
					
				});
			}
			
			function finalizeUnEditableAction(msg){
				$state.go('home.orderrequests',{message: msg})
			}
			
			function finalizeAction(msg, mode){
				$scope.notifyDoneLoading();
				Prompter.promptActionState(msg, mode);
			}
			
			$scope.submitConfirmMsg = "Are you sure you want to submit this order request for approval? "+ 
			"You will not be able to edit this request if no errors are found after this action."
		}]
)
.controller('SubmittedOrderRequestsCtrl',
		['$scope','$rootScope', '$state','spinnerService','Prompter','DateUtil','OrderRequest','message',
		 function($scope,$rootScope, $state,spinnerService,Prompter,DateUtil,OrderRequest,message){
			
			function doClearFilter(){
				$scope.filter.status = null;
				$scope.filter.toDate = null;
				$scope.filter.fromDate = null;
				$scope.filter.orderNumber = null;
			}
			
			var initDone = false;
			function init(cb, filter, actualCB){
				$scope.filter = {};
				doClearFilter()
				$scope.filter.status = 'SUBMITTED'
				initDone = true;
				cb(filter, actualCB);
			}
			
			function actualReloadList(filter, cb){
				var scopeFilter = $scope.filter;
				spinnerService.show("nsisSpinner");
				filter.statuses = ['SUBMITTED','APPROVED','REJECTED']
				if(scopeFilter.toDate){
					filter.toDate = DateUtil.toStringIgnoreTZ(scopeFilter.toDate);
				}
				if(scopeFilter.fromDate){
					filter.fromDate = DateUtil.toStringIgnoreTZ(scopeFilter.fromDate);
				} 
				filter.status = scopeFilter.status;
				filter.ordNumber = scopeFilter.orderNumber;
				OrderRequest.get(filter,function(data){
					spinnerService.hide("nsisSpinner")
					cb(data)
				});
				$scope.triggerReload = false;
			}
			
			$scope.reloadList = function(filter, cb){
				if(!initDone){
					init(actualReloadList, filter, cb)
				}else
					actualReloadList(filter, cb);
			}
			
			$scope.clearFilter = function(){
				doClearFilter();
				$scope.triggerReload = true;
				$scope.$digest();
			}
			
			$scope.view = function(orderNumber){
				spinnerService.show("nsisSpinner");
				OrderRequest.get({orderNumber:orderNumber},{}, function(orderRequest){
					spinnerService.hide("nsisSpinner");
					$state.go('home.submittedorderrequestsform',{orderRequest: orderRequest});
				});
			}
			
			if(message){
				Prompter.promptActionState(message, 'success');
			}
			
			function finalizeAction(msg, mode){
				spinnerService.hide("nsisSpinner");
				Prompter.promptActionState(msg, mode);
			}
			
			$scope.doTriggerReload = function(){
				$scope.triggerReload = true;
			}
		}]
)
.controller('SubmittedOrderRequestsFormCtrl',
		['$scope','$rootScope', '$state','spinnerService','Prompter','orderRequest',
		 function($scope,$rootScope, $state,spinnerService,Prompter,orderRequest){
			$scope.orderRequest = orderRequest;
			$scope.editMeta = orderRequest.editMeta;
			$scope.notifyLoading = function(){
				spinnerService.show("nsisSpinner");
			}
			$scope.notifyDoneLoading = function(){
				spinnerService.hide("nsisSpinner");
			}
			$scope.cancel = function(){
				//TODO place prompt here specially when there are changes
				$state.go('home.submittedorderrequests');
			}
			
			$scope.approve = function(){
				$scope.notifyLoading();
				$scope.orderRequest.approve(function(result,err){
					if(err){
						console.log(err)
						$scope.errorMeta = err.data;
						var msg = 'Failed to approve OR: '+$scope.orderRequest.body.orderNumber + " due to errors"
						finalizeAction(msg, 'danger');
					}else{
						delete $scope['errorMeta'];
						$scope.orderRequest = result;
						$scope.editMeta = result.editMeta;
						var msg = 'Successfully approved OR: '+$scope.orderRequest.body.orderNumber
						finalizeUnEditableAction(msg);
					}
				});
			}
			
			$scope.reject = function(){
				$scope.notifyLoading();
				$scope.orderRequest.reject(function(result,err){
					if(err){
						$scope.errorMeta = err.data;
						var msg = 'Failed to reject OR: '+$scope.orderRequest.body.orderNumber + " due to errors"
						finalizeAction(msg, 'danger');
					}else{
						delete $scope['errorMeta'];
						$scope.orderRequest = result;
						$scope.editMeta = result.editMeta;
						var msg = 'Successfully rejected OR: '+$scope.orderRequest.body.orderNumber+
							  '. Ignoring changes.'
						finalizeUnEditableAction(msg);
					}
					
				});
			}
			
			function finalizeUnEditableAction(msg){
				$state.go('home.submittedorderrequests',{message: msg})
			}
			
			function finalizeAction(msg, mode){
				$scope.notifyDoneLoading();
				Prompter.promptActionState(msg, mode);
			}
			
			$scope.approvalConfirmMsg = "Are you sure you want to approve this order request? "+ 
			"You will not be able to edit this request if no errors are found after this action."
			
			$scope.rejectionConfirmMsg = "Are you sure you want to reject this order request? "+ 
			"You will not be able to edit this request if no errors are found after this action."
		}]
)
.controller('InventoryOnHandCtrl',
		['$scope','$rootScope', '$state',
		 function($scope,$rootScope, $state){
			 $scope.radioModel = 'Item';
			 $state.go("home.inventorymaintenance.inventoryonhand.byitem");
		}]
)
.controller('InventoryOnHandByBatchCtrl',
		['$scope','$rootScope', '$state','spinnerService','OnHandSupplyItems',
		 function($scope,$rootScope, $state, spinnerService,OnHandSupplyItems){
			$scope.filter = {itemCode:null}
			$scope.reloadList = function(filter, cb){
				spinnerService.show("nsisSpinner");
				if($scope.filter.itemCode)
					filter.itemCode = $scope.filter.itemCode;
				filter.facilityCodeName = 'GTW';
				filter.byItemCode = false;
				OnHandSupplyItems.get(filter, function(data){
					spinnerService.hide("nsisSpinner");
					cb(data)
				})
				$scope.triggerReload = false;
			}
			
			$scope.doTriggerReload = function(){
				$scope.triggerReload = true;
			}
		}]
)
.controller('InventoryOnHandByItemCtrl',
		['$scope','$rootScope', '$state','spinnerService','OnHandSupplyItems',
		 function($scope,$rootScope, $state, spinnerService,OnHandSupplyItems){
			$scope.filter = {itemCode:null}
			$scope.reloadList = function(filter, cb){
				spinnerService.show("nsisSpinner");
				if($scope.filter.itemCode)
					filter.itemCode = $scope.filter.itemCode;
				filter.facilityCodeName = 'GTW';
				filter.byItemCode = true;
				OnHandSupplyItems.get(filter, function(data){
					spinnerService.hide("nsisSpinner");
					cb(data)
				})
				$scope.triggerReload = false;
			}
			
			$scope.doTriggerReload = function(){
				$scope.triggerReload = true;
			}
		}]
)
.controller('DeliveryReceiptsCtrl',
		['$scope','$rootScope', '$state','Prompter','spinnerService','DeliveryReceipt','message',
		 function($scope,$rootScope, $state,Prompter,spinnerService, DeliveryReceipt,message){
			$scope.reloadList = function(filter, cb){
				spinnerService.show("nsisSpinner");
				DeliveryReceipt.get(filter,function(data){
					spinnerService.hide("nsisSpinner");
					cb(data)
				});
			}
			
			$scope.view = function(orderNumber){
				spinnerService.show("nsisSpinner");
				DeliveryReceipt.get({orderNumber:orderNumber},{}, function(deliveryReceipt){
					spinnerService.hide("nsisSpinner");
					$state.go('home.deliveryreceiptsform',{deliveryReceipt: deliveryReceipt});
				});
			}
			
			if(message){
				Prompter.promptActionState(message, 'success');
			}
			
			function finalizeAction(msg, mode){
				spinnerService.hide("nsisSpinner");
				Prompter.promptActionState(msg, mode);
			}
		}]
)
.controller('InventoryDisposalCtrl',
		['$scope','$rootScope', '$state',
		 function($scope,$rootScope, $state){
			console.log('InventoryMaintenanceCtrl')
			$scope.tabs = [
			               {title: 'On-hand', state: '.inventoryonhand'},
			               {title: 'Delivery Receipts', state: '.deliveryreceipts'},
			               {title: 'Disposal', state: '.inventorydisposal'},
			]
		}]
)
.controller('InvMaintenanceCtrl',
		['$scope','$rootScope', '$state',
		 function($scope,$rootScope, $state){
			$scope.tabs = [
			               { heading: "On-hand", route:".inventoryonhand", index:0},
			               { heading: "Delivery Receipts", route:".deliveryreceipts", index:1},
			               { heading: "Disposal", route:".inventorydisposal", index:2},
			               ];

			$scope.go = function(route){
				$state.go('home.inventorymaintenance'+route);
			};

			$scope.setActive = function(route){
				switch(route){
					case 'home.inventorymaintenance.inventoryonhand': $scope.active = 0;break;
					case 'home.inventorymaintenance.deliveryreceipts': $scope.active = 1;break;
					case 'home.inventorymaintenance.inventorydisposal': $scope.active = 2;break;
				}
			};
			
			$scope.$on("$stateChangeSuccess", function() {
				$scope.setActive($state.current.name);
			});
			
		}]
)
.controller('FacilityIssuanceCtrl',
		['$scope','$rootScope', '$state','ItemMaintenanceService',
		 function($scope,$rootScope, $state,ItemMaintenanceService){
		}]
)
.controller('DeliveryReceiptsFormCtrl',
		['$scope','$rootScope', '$state','spinnerService','Prompter','OrderRequest','OrderRequestService','deliveryReceipt',
		 function($scope,$rootScope, $state,spinnerService,Prompter, OrderRequest,OrderRequestService,deliveryReceipt){
			$scope.deliveryReceipt = deliveryReceipt;
			$scope.editMeta = deliveryReceipt.editMeta;
			$scope.orderRequest = new OrderRequest(OrderRequestService.sanitizeOrQRO(deliveryReceipt.body.orderRequest));
			
			$scope.notifyLoading = function(){
				spinnerService.show("nsisSpinner");
			}
			$scope.notifyDoneLoading = function(){
				spinnerService.hide("nsisSpinner");
			}
			
			$scope.cancel = function(){
				//TODO place prompt here specially when its NEW or there are changes
				$state.go('home.inventorymaintenance.deliveryreceipts');
			}
			
			$scope.save = function(){
				$scope.notifyLoading();
				$scope.deliveryReceipt.save(function(result,err){
					var msg = '', mode = '';
					if(err){
						$scope.errorMeta = err.data;
						msg = 'Failed to update Delivery Receipt for OR: '+getORNumber() + " due to errors"
						mode = 'danger'
					}else{
						delete $scope['errorMeta'];
						$scope.deliveryReceipt = result;
						msg = 'Successfully updated Delivery Receipt for OR: '+getORNumber()
						mode = 'success'
					}
					finalizeAction(msg, mode);
				});
			}
			
			$scope.finalize = function(){
				$scope.notifyLoading();
				$scope.deliveryReceipt.finalize(function(result,err){
					if(err){
						$scope.errorMeta = err.data;
						var msg = 'Failed to finalize Delivery Receipt for OR: '+getORNumber() + " due to errors"
						finalizeAction(msg, 'danger');
					}else{
						delete $scope['errorMeta'];
						$scope.deliveryReceipt = result;
						$scope.editMeta = result.editMeta;
						var msg = 'Successfully finalized Delivery Receipt for OR: '+getORNumber()
						finalizeUnEditableAction(msg);
					}
				});
			}
			
			function getORNumber(){
				return $scope.deliveryReceipt.body.orderRequest.orderNumber;
			}
			
			function finalizeUnEditableAction(msg){
				$state.go('home.inventorymaintenance.deliveryreceipts',{message: msg})
			}
			
			function finalizeAction(msg, mode){
				$scope.notifyDoneLoading();
				Prompter.promptActionState(msg, mode);
			}
			
			$scope.finalizeConfirmMsg = "Are you sure you want to finalize this delivery receipt? "+ 
			"You will not be able to edit this receipt if no errors are found after this action."
		}]
)
.controller('RequestsToWarehouseIssuancesCtrl',
		['$scope','$rootScope', '$state','spinnerService','Prompter','RequestIssuance','SvcSupplyRequest','message',
		 function($scope,$rootScope, $state,spinnerService,Prompter,RequestIssuance,SvcSupplyRequest,message){
			$scope.reloadList = function(filter, cb){
				spinnerService.show('nsisSpinner')
				RequestIssuance.get(filter,function(data){
					spinnerService.hide('nsisSpinner')
					cb(data)
				});
			}
			
			$scope.view = function(requestNumber, facilityCodeName){
				spinnerService.show("nsisSpinner");
				RequestIssuance.get({requestNumber:requestNumber},{}, function(requestIssuance){
					SvcSupplyRequest.get(
							{
								requestNumber:requestNumber,
								facilityCodeName:facilityCodeName
							},
							{}, 
							function(supplyRequest){
								spinnerService.hide("nsisSpinner");
								$state.go(
										'home.requesttowhsissuanceform',
										{requestIssuance: requestIssuance, supplyRequest: supplyRequest}
									);
							});
				});
			}
			
			if(message){
				Prompter.promptActionState(message, 'success');
			}
			
			function finalizeAction(msg, mode){
				spinnerService.hide("nsisSpinner");
				Prompter.promptActionState(msg, mode);
			}
			
		}]
)
.controller('RequestsToWarehouseIssuancesFormCtrl',
		['$scope','$rootScope', '$state','spinnerService','Prompter','supplyRequest','requestIssuance',
		 function($scope,$rootScope, $state,spinnerService,Prompter, supplyRequest,requestIssuance){
			$scope.requestIssuance = requestIssuance;
			$scope.editMeta = requestIssuance.editMeta;
			$scope.supplyRequest = supplyRequest;
			
			$scope.notifyLoading = function(){
				spinnerService.show("nsisSpinner");
			}
			$scope.notifyDoneLoading = function(){
				spinnerService.hide("nsisSpinner");
			}
			
			$scope.cancel = function(){
				//TODO place prompt here specially when its NEW or there are changes
				$state.go('home.requesttowhsissuance');
			}
			
			var msg ="";
			$scope.save = function(){
				$scope.notifyLoading();
				$scope.requestIssuance.save(function(result){
					msg = 'Successfully updated Request Issuance for request: '+getRequestNumber(result);
					defaultSuccessHandler(msg, result);
				}, function(err){
					msg = 'Failed to update Request Issuance for request: '+
						getRequestNumber($scope.requestIssuance) + 
						" due to errors"
					defaultErrorHandler(msg, err);
				})
			}
			
			$scope.finalize = function(){
				$scope.notifyLoading();
				$scope.requestIssuance.finalize(function(result){
					msg = 'Successfully finalized Request Issuance for request: '+getRequestNumber(result);
					window.open("/NSIS/rest/1.0/requestissuances/"+getRequestNumber(result)+"/deliveryreceiptform");
					finalizeUnEditableAction(msg);
				}, function(err){
					msg = 'Failed to finalize Request Issuance for request: '+
						getRequestNumber($scope.requestIssuance) + 
						" due to errors"
					defaultErrorHandler(msg, err);
				})
			}
			
			$scope.print = function(){
				$scope.notifyLoading();
				$scope.requestIssuance.print(function(result){
					msg = 'Successfully printed Request Issuance form for request: '+getRequestNumber(result);
					defaultSuccessHandler(msg, result);
					window.open("/NSIS/rest/1.0/requestissuances/"+getRequestNumber(result)+"/picklistform");
				}, function(err){
					msg = 'Failed to print Request Issuance form for request: '+
					getRequestNumber($scope.requestIssuance) + 
						" due to errors"
					defaultErrorHandler(msg, err);
				})
			}
			
			$scope.reject = function(){
				$scope.notifyLoading();
				$scope.requestIssuance.reject(function(result){
					msg = 'Successfully rejected Request Issuance for request: '+getRequestNumber(result);
					finalizeUnEditableAction(msg);
				}, function(err){
					msg = 'Failed to reject Request Issuance for request: '+
						getRequestNumber($scope.requestIssuance) + 
						" due to errors"
					defaultErrorHandler(msg, err);
				})
			}
			
			function defaultErrorHandler(msg, err){
				$scope.errorMeta = err.data;
				finalizeAction(msg, 'danger');
			}
			
			function defaultSuccessHandler(msg, updatedRequestIssuance){
				delete $scope['errorMeta'];
				$scope.requestIssuance = updatedRequestIssuance;
				$scope.editMeta = updatedRequestIssuance.editMeta;
				finalizeAction(msg, 'success');
			}
			
			function getRequestNumber(requestIssuance){
				return requestIssuance.body.requestNumber;
			}
			
			function finalizeUnEditableAction(msg){
				$state.go('home.requesttowhsissuance',{message: msg})
			}
			
			function finalizeAction(msg, mode){
				$scope.notifyDoneLoading();
				Prompter.promptActionState(msg, mode);
			}
			
			$scope.finalizeConfirmMsg = "Are you sure you want to finalize this supply issuance? "+ 
			"You will not be able to edit this record if no errors are found after this action."
			
			$scope.rejectionConfirmMsg = "Are you sure you want to reject this supply request? "+ 
			"You will not be able to edit this record if no errors are found after this action."
		}]
)
.controller('ModalInstanceCtrl', ["$scope", "$uibModalInstance","spinnerService", "orderRequestItem","mode", "FacilityInventoryItemService",
    function ($scope, $uibModalInstance,spinnerService, orderRequestItem,mode, FacilityInventoryItemService) {
	
	$scope.orderRequestItem = orderRequestItem;
	$scope.actionName = mode === 'ADD' ? 'ADD' : 'UPDATE';
	
	$scope.ok = function () {
		spinnerService.show("nsisSpinner");
		$scope.orderRequestItem.validate(function(result,err){
			if(err){
				$scope.meta = err.data;
				spinnerService.hide("nsisSpinner");
			}else{
				delete $scope['meta'];
				var data = {
					itemCode: $scope.orderRequestItem.itemCode,
					quantity: $scope.orderRequestItem.quantity,
					remarks: $scope.orderRequestItem.remarks
				}
				spinnerService.hide("nsisSpinner");	
				$uibModalInstance.close(data);
			}
		})
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
	
	$scope.searchFacilityItems = function(val) {
		var parameters = {
				facilityId: orderRequestItem.facilityId,
				itemCode: val,
				description: val, 
				type: val,
				unitOfMeasurement: val,
				searchType: "TYPEHEAD",
				currency: orderRequestItem.currency
		};
		return FacilityInventoryItemService.get(parameters).$promise.then(function (data) {
			return data.rows;
		});
	};

	$scope.onFacilityItemSelect = function($item, $model, $label){
		$scope.orderRequestItem.itemType = $model.type;
		$scope.orderRequestItem.description = $model.description;
		$scope.orderRequestItem.unitOfMeasurement = $model.uom;
		$scope.orderRequestItem.maximum = $model.max;
		$scope.orderRequestItem.minimum = $model.min;
		$scope.orderRequestItem.minimum = $model.min;
		$scope.orderRequestItem.itemCost = $model.itemCost;
		$scope.orderRequestItem.itemCode = $model.itemCode;
	}
	
	$scope.setState = function(id){
		return $scope.meta && $scope.meta[id] ? 'has-error' : '';
	}
	
	$scope.hasError = function(id){
		return !!($scope.meta && $scope.meta[id]);
	}
}])
.controller('DeliveryItemBatchGroupModalCtrl',["$scope", "$uibModalInstance","spinnerService","DateUtil",
                                               "NsisDirectiveScopeWrapper","editMeta","deliveredItemBatchGroup",
		function ($scope, $uibModalInstance,spinnerService,DateUtil,NsisDirectiveScopeWrapper,editMeta,deliveredItemBatchGroup) {
			$scope.disableIdFields = false;
			$scope.itemBatchActionLabel = 'Add';
			$scope.editMeta = editMeta;
			
			if($scope.augment)
	    		NsisDirectiveScopeWrapper.augmentScope($scope);
			
			$scope.deliveredItemBatchGroup = deliveredItemBatchGroup;
			
			$scope.$watch('deliveredItemBatchGroup.editedItemBatch.rackingCode', function(nv, ov){
				$scope.deliveredItemBatchGroup.editedItemBatch.batchNumber = constructBatchNumber($scope.deliveredItemBatchGroup);
			});
			
			$scope.$watch('deliveredItemBatchGroup.editedItemBatch.dateStored', function(nv, ov){
				$scope.deliveredItemBatchGroup.editedItemBatch.batchNumber = constructBatchNumber($scope.deliveredItemBatchGroup);
			});
			
			function constructBatchNumber(itemBatchGroup){
				var returnValue = null;
				if(itemBatchGroup.editedItemBatch.rackingCode && itemBatchGroup.editedItemBatch.dateStored){
					var itemCode = itemBatchGroup.receivableDetails.itemCode.replace(/-/g,"");
					var rackCode = itemBatchGroup.editedItemBatch.rackingCode.rackCode.replace(/-/g,"");
					var dateStored = DateUtil.parseAsNoSlashShortDate(itemBatchGroup.editedItemBatch.dateStored);
					returnValue = itemCode + "-" + rackCode + "-" + dateStored;
				}
				
				return returnValue;
			}
			
			$scope.ok = function () {
				spinnerService.show("nsisSpinner");
				$scope.deliveredItemBatchGroup.saveItemBatch(
					$scope.itemBatchActionLabel === 'Add',
					function(thereIsChange){
						delete $scope['errorMeta']
						if(thereIsChange)$scope.triggerGridReload = true;
						$scope.itemBatchActionLabel = 'Add';
						$scope.disableIdFields = false;
						spinnerService.hide("nsisSpinner");
					},function(err){
						$scope.errorMeta = err.data;
						spinnerService.hide("nsisSpinner");
					}
				);
			};
			
			$scope.view = function (selectedItemBatch) {
				$scope.deliveredItemBatchGroup.editedItemBatch = angular.copy(selectedItemBatch);
				$scope.itemBatchActionLabel = 'Update';
				$scope.disableIdFields = true;
			};
			
			$scope.remove = function (batchNumber, cb) {
				$scope.deliveredItemBatchGroup.removeItemBatch(batchNumber);
				cb(true);
			};
			
			$scope.list = function(cb){
				spinnerService.show("nsisSpinner");
				$scope.deliveredItemBatchGroup.recalculateItemBatches(function(data){
					spinnerService.hide("nsisSpinner");
					cb(data)
				});
			}
		
			$scope.clear = function () {
				delete $scope['errorMeta'];
				$scope.disableIdFields = false;
				$scope.itemBatchActionLabel = 'Add';
				$scope.deliveredItemBatchGroup.editedItemBatch = {};
			};
			
			$scope.apply = function (){
				$uibModalInstance.close($scope.deliveredItemBatchGroup);
			}
			
			$scope.close = function (){
				$uibModalInstance.dismiss('close');
			}

			$scope.triggerGridReload = true;
		}]
)
.controller('RequestIssuanceModalCtrl', ["$scope", "$uibModalInstance","spinnerService", "requestItemGroup","mode",function ($scope, $uibModalInstance,spinnerService, requestItemGroup,mode) {
	
	$scope.notifyLoading = function(){
		spinnerService.show("nsisSpinner");
	}
	$scope.notifyDoneLoading = function(){
		spinnerService.hide("nsisSpinner");
	}
	
	$scope.requestItemGroup = requestItemGroup;
	
	$scope.listAvailableSupplies = function(cb){
		$scope.notifyLoading();
		$scope.requestItemGroup.listAvailableSupplies(function(suppliesGroup){
			
			var availableSupplies = suppliesGroup.availableSupplies;
			var suppliesIssued = $scope.requestItemGroup.suppliedItems;
			var totalSuppliedQuantity = parseInt(0);
			availableSupplies.forEach(function(availableSupply){
				var suppliedQuantity = parseInt(0);
				if(suppliesIssued){
					var supplyMatch = suppliesIssued.find(function(si){
						return si.sourceBatchNumber === availableSupply.batchNumber
					});
					if(supplyMatch) {
						suppliedQuantity = parseInt(supplyMatch.suppliedQuantity);
						availableSupply.remarks = supplyMatch.remarks;
					}
				}
				availableSupply.suppliedQuantity = suppliedQuantity;
				availableSupply.previousSuppliedQuantity = suppliedQuantity;
				totalSuppliedQuantity+= suppliedQuantity;
			});
			
			$scope.requestItemGroup.supplyDetails  = {
					physicalStock: suppliesGroup.physicalStock,
					logicalStock: suppliesGroup.logicalStock,
					totalSuppliedQty: totalSuppliedQuantity
				}
			$scope.notifyDoneLoading();
			cb(suppliesGroup.availableSupplies)
		});
	}
	
	function finalizeErrMsg(errMsg){
		var finalMsg = errMsg;
		if(errMsg !== null && typeof errMsg === 'object'){
			var batchNumber = Object.keys(errMsg)[0];
			finalMsg = batchNumber+": "+errMsg[batchNumber];
		}
		
		return finalMsg;
	}
	
	$scope.updateRequestItemIssuanceView = function(hasError, msg){
		$scope.disableApplyButton = hasError;
		if(!hasError){
			var sum = $scope.requestItemGroup.suppliedItems.reduce(function(previousSum, suppliedItem){
				return previousSum + parseInt(suppliedItem.suppliedQuantity);
			},parseInt(0));
			
			$scope.requestItemGroup.supplyDetails.totalSuppliedQty = sum;
			$scope.modalError = null;
		}else if(msg){
			$scope.modalError = finalizeErrMsg(msg);
		}
	}
	
	$scope.apply = function () {
		$scope.validateHighlighted(function(successful, msg){
			$scope.disableApplyButton = false;
			$scope.modalError = finalizeErrMsg(msg);
			if(successful) {
				$uibModalInstance.close($scope.requestItemGroup);
			}
		})
	};

	$scope.close = function () {
		$uibModalInstance.dismiss('cancel');
	};
	
	$scope.triggerGridReload = true;
	
}])
.controller('AlertModalCtrl', ["$scope", "$uibModalInstance", "message", "mode",function ($scope, $uibModalInstance, message, mode) {

	$scope.message = message;
	$scope.mode = mode;
	$scope.close = function(){
		$uibModalInstance.dismiss('close');
	};
	 
}]);
