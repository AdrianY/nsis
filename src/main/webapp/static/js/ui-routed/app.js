agGrid.initialiseAgGridWithAngular1(angular);
var nsisApp = angular.module('nsisUiRoutedApp', ['ui.router','ngAnimate','agGrid','angularSpinners', 
                                                 'ui.bootstrap','ngCookies','ngTouch',
                                                 'nsisDomainServices','nsisServices',
                                                 'nsisSvcDomainServices','nsisSvcServices',
                                                 'nsisCsDomainServices','nsisCsServices'])
.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
})
.config(['$stateProvider', '$urlRouterProvider','$httpProvider',
         function($stateProvider, $urlRouterProvider, $httpProvider) {
	
	var parentTemplateUri = '/NSIS/static/view/ui-routed/';
	$urlRouterProvider.otherwise('/home');

	$stateProvider.state('site', {
		'abstract': true
	}).state('home', {
		parent: 'site',
		url: '/home',
		data: {
			roles: ['User']
		},
		views: {
			'content@': {
				templateUrl: parentTemplateUri+'home.html',
				controller: 'HomeCtrl'
			}
		}
	}).state('signin', {
		parent: 'site',
		url: '/signin',
		views: {
			'content@': {
				templateUrl: parentTemplateUri+'signin.html',
				controller: 'SigninCtrl'
			}
		}
	}).state('restricted', {
		parent: 'site',
		templateUrl: '/restricted',
		data: {
			roles: ['Admin']
		},
		views: {
			'content@': {
				templateUrl: parentTemplateUri+'restricted.html'
			}
		}
	}).state('accessdenied', {
		parent: 'site',
		url: '/denied',
		data: {
			roles: []
		},
		views: {
			'content@': {
				templateUrl: parentTemplateUri+'denied.html'
			}
		}
	}).state('home.maintenance', {
		url: '/maintenance',
		templateUrl: parentTemplateUri+'maintenance.html',
		controller: 'MaintenanceCtrl'
	}).state('home.orderrequests', {
		url: '/order-requests',
		resolve: {
			message: ["$stateParams",function($stateParams){return $stateParams.message}]
    	},
		params: {message: null},
		templateUrl: parentTemplateUri+'order-requests.html',
		controller: 'OrderRequestsCtrl'
	}).state('home.orderrequestsform', {
		url: '/order-requests/form',
		resolve: {
    		orderRequest: ["$stateParams",function($stateParams){return $stateParams.orderRequest}]
    	},
    	params: {orderRequest: {}},
		views: {
            '': { templateUrl:  parentTemplateUri+'order-requests-form.html'},
            'orderDetails@home.orderrequestsform': { 
            	templateUrl: parentTemplateUri+'order-requests-form-details.html',
            	controller: 'OrderRequestsFormCtrl'
            }
        }
	}).state('home.submittedorderrequests', {
		url: '/submitted-order-requests',
		resolve: {
			message: ["$stateParams",function($stateParams){return $stateParams.message}]
    	},
		params: {message: null},
		templateUrl: parentTemplateUri+'submitted-order-requests.html',
		controller: 'SubmittedOrderRequestsCtrl'
	}).state('home.submittedorderrequestsform', {
		url: '/submitted-order-requests/form',
		resolve: {
    		orderRequest: ['$stateParams',function($stateParams){return $stateParams.orderRequest}]
    	},
    	params: {orderRequest: {}},
    	controller: 'SubmittedOrderRequestsFormCtrl',
    	templateUrl: parentTemplateUri+'submitted-order-requests-form.html'
	}).state('home.facilityissuance', {
		url: '/facility-issuance',
		templateUrl: parentTemplateUri+'facility-issuance.html',
		controller: 'FacilityIssuanceCtrl'
	}).state('home.inventorymaintenance', {
		parent: 'home',
		url: '/inventory-maintenance',
		templateUrl: parentTemplateUri+'inventory-maintenance.html',
		controller: 'InvMaintenanceCtrl'
	}).state('home.inventorymaintenance.inventoryonhand', {
		url: '/inventory-onhand',
		templateUrl: parentTemplateUri+'im-on-hand.html',
		controller: 'InventoryOnHandCtrl'
	}).state('home.inventorymaintenance.inventoryonhand.byitem', {
		url: '/by-item',
		templateUrl: parentTemplateUri+'im-on-hand-by-item.html',
		controller: 'InventoryOnHandByItemCtrl'
	}).state('home.inventorymaintenance.inventoryonhand.bybatch', {
		url: '/by-batch',
		templateUrl: parentTemplateUri+'im-on-hand-by-batch.html',
		controller: 'InventoryOnHandByBatchCtrl'
	}).state('home.inventorymaintenance.deliveryreceipts', {
		url: '/delivery-receipts',
		resolve: {
			message: ["$stateParams",function($stateParams){return $stateParams.message}]
    	},
		params: {message: null},
		templateUrl: parentTemplateUri+'im-delivery-receipts.html',
		controller: 'DeliveryReceiptsCtrl'
	}).state('home.inventorymaintenance.inventorydisposal', {
		url: '/inventory-disposal',
		templateUrl: parentTemplateUri+'im-disposal.html',
		controller: 'InventoryDisposalCtrl'
	}).state('home.deliveryreceiptsform', {
		url: '/delivery-receipts/form',
		resolve: {
    		deliveryReceipt: ["$stateParams",function($stateParams){return $stateParams.deliveryReceipt}]
    	},
    	params: {deliveryReceipt: {}},
    	controller: 'DeliveryReceiptsFormCtrl',
    	templateUrl: parentTemplateUri+'delivery-receipts-form.html'
	}).state('home.svcsupplyrequests', {
		url: '/svc-supply-requests',
		resolve: {
			svcAssignments: ["CookieStore",function(CookieStore){return CookieStore.extractSvcAssignments(null);}],
			message: ["$stateParams",function($stateParams){return $stateParams.message}]
    	},
    	params: {svcAssignments: [],message:null},
		templateUrl: parentTemplateUri+'svc-supply-requests.html',
		controller: 'SvcSupplyRequestsCtrl'
	}).state('home.svcsupplyrequestsform', {
		url: '/svc-supply-requests/form',
		resolve: {
			supplyRequest: ["$stateParams",function($stateParams){return $stateParams.supplyRequest}]
    	},
    	params: {supplyRequest: {}},
    	controller: 'SvcSupplyRequestsFormCtrl',
    	templateUrl: parentTemplateUri+'svc-supply-requests-form.html'
	}).state('home.submittedsvcsupplyrequests', {
		url: '/submitted-supply-requests',
		resolve: {
			svcAssignments: ["CookieStore",function(CookieStore){return CookieStore.extractSvcAssignments(null);}],
			message: ["$stateParams",function($stateParams){return $stateParams.message}]
    	},
    	params: {svcAssignments: [],message:null},
		templateUrl: parentTemplateUri+'submitted-supply-requests.html',
		controller: 'SubmittedSvcSupplyRequestsCtrl'
	}).state('home.submittedsupplyrequestsform', {
		url: '/submitted-supply-requests/form',
		resolve: {
    		supplyRequest: ["$stateParams",function($stateParams){return $stateParams.supplyRequest}]
    	},
    	params: {supplyRequest: {}},
    	controller: 'SubmittedSupplyRequestsFormCtrl',
    	templateUrl: parentTemplateUri+'submitted-supply-requests-form.html'
	}).state('home.requesttowhsissuance', {
		url: '/supply-request-issuances',
		resolve: {
			message: ["$stateParams",function($stateParams){return $stateParams.message}]
    	},
		params: {message: null},
		templateUrl: parentTemplateUri+'request-to-wh-issuances.html',
		controller: 'RequestsToWarehouseIssuancesCtrl'
	}).state('home.requesttowhsissuanceform', {
		url: '/supply-request-issuances/form',
		resolve: {
    		supplyRequest: ["$stateParams",function($stateParams){return $stateParams.supplyRequest}],
    		requestIssuance: ["$stateParams",function($stateParams){return $stateParams.requestIssuance}]
    	},
    	params: {supplyRequest: {}, requestIssuance: {}},
		templateUrl: parentTemplateUri+'request-to-wh-issuances-form.html',
		controller: 'RequestsToWarehouseIssuancesFormCtrl'
	}).state('home.svcinventorymaintenance', {
		parent: 'home',
		url: '/svc-inventory-maintenance',
		templateUrl: parentTemplateUri+'svc-inventory-maintenance.html',
		controller: 'SvcInvMaintenanceCtrl'
	}).state('home.svcinventorymaintenance.deliveryreceipts', {
		url: '/delivery-receipts',
		resolve: {
			svcAssignments: ["CookieStore",function(CookieStore){return CookieStore.extractSvcAssignments(null);}],
			message: ["$stateParams",function($stateParams){return $stateParams.message}]
    	},
    	params: {svcAssignments: [],message:null},
		templateUrl: parentTemplateUri+'svc-im-delivery-receipts.html',
		controller: 'SvcDeliveryReceiptsCtrl'
	}).state('home.svcdeliveryreceiptsform', {
		url: '/svc-delivery-receipts/form',
		resolve: {
    		svcDeliveryReceipt: ["$stateParams",function($stateParams){return $stateParams.svcDeliveryReceipt}]
    	},
    	params: {svcDeliveryReceipt: {}},
    	controller: 'SvcDeliveryReceiptsFormCtrl',
    	templateUrl: parentTemplateUri+'svc-delivery-receipts-form.html'
	}).state('home.svcinventorymaintenance.inventoryonhand', {
		url: '/inventory-onhand',
		templateUrl: parentTemplateUri+'svc-im-on-hand.html',
		controller: 'SvcInventoryOnHandCtrl'
	}).state('home.svcinventorymaintenance.inventoryonhand.byitem', {
		url: '/by-item',
		resolve: {
			svcAssignments: ["CookieStore",function(CookieStore){return CookieStore.extractSvcAssignments(null);}]
    	},
    	params: {svcAssignments: []},
		templateUrl: parentTemplateUri+'svc-im-on-hand-by-item.html',
		controller: 'SvcInventoryOnHandByItemCtrl'
	}).state('home.svcinventorymaintenance.inventoryonhand.bybatch', {
		url: '/by-batch',
		resolve: {
			svcAssignments: ["CookieStore",function(CookieStore){return CookieStore.extractSvcAssignments(null);}]
    	},
    	params: {svcAssignments: []},
		templateUrl: parentTemplateUri+'svc-im-on-hand-by-batch.html',
		controller: 'SvcInventoryOnHandByBatchCtrl'
	}).state('home.svcinventorymaintenance.physicalstockcount', {
		url: '/physical-stock-count-request',
		resolve: {
			svcAssignments: ["CookieStore",function(CookieStore){return CookieStore.extractSvcAssignments(null);}],
			message: ["$stateParams",function($stateParams){return $stateParams.message}]
    	},
    	params: {svcAssignments: [], message: null},
		templateUrl: parentTemplateUri+'svc-im-phy-stk-count.html',
		controller: 'SvcPhysicalStockCountCtrl'
	}).state('home.svcphysicalstockcountrequestsform', {
		url: '/svc-physical-stock-count-request/form',
		resolve: {
    		svcPhysicalStockCountRequest: ["$stateParams",function($stateParams){return $stateParams.svcDeliveryReceipt}]
    	},
    	params: {svcPhysicalStockCountRequest: {}},
    	controller: 'SvcPhysicalStockCountFormCtrl',
    	templateUrl: parentTemplateUri+'svc-im-phy-stk-count-form.html'
	}).state('home.requesttosvcissuance', {
		url: '/customer-request-issuances',
		resolve: {
			message: ["$stateParams",function($stateParams){return $stateParams.message}],
			svcAssignments: ["CookieStore",function(CookieStore){return CookieStore.extractSvcAssignments(null);}]
    	},
		params: {message: null,svcAssignments: []},
		templateUrl: parentTemplateUri+'request-to-svc-issuances.html',
		controller: 'RequestsToServiceCenterIssuancesCtrl'
	}).state('home.requesttosvcissuanceform', {
		url: '/customer-request-issuances/form',
		resolve: {
			customerRequest: ["$stateParams",function($stateParams){return $stateParams.customerRequest}],
    		requestIssuance: ["$stateParams",function($stateParams){return $stateParams.requestIssuance}]
    	},
    	params: {customerRequest: {}, requestIssuance: {}},
		templateUrl: parentTemplateUri+'request-to-svc-issuances-form.html',
		controller: 'RequestsToServiceCenterIssuancesFormCtrl'
	}).state('home.cscustomerrequests', {
		url: '/cs-customer-requests',
		resolve: {
			requestType: ["$cookies",function($cookies){return $cookies.getObject("ciType");}],
			message: ["$stateParams",function($stateParams){return $stateParams.message}]
    	},
    	params: {svcAssignments: [],message:null},
		templateUrl: parentTemplateUri+'cs-customer-requests.html',
		controller: 'CsCustomerRequestsCtrl'
	}).state('home.cscustomerrequestsform', {
		url: '/cs-customer-requests/form',
		resolve: {
			customerRequest: ["$stateParams",function($stateParams){return $stateParams.customerRequest}]
    	},
    	params: {customerRequest: {}},
    	controller: 'CsCustomerRequestsFormCtrl',
    	templateUrl: parentTemplateUri+'cs-customer-requests-form.html'
	});

	$httpProvider.interceptors.push(["$q", "$rootScope", "$location",function ($q, $rootScope, $location) {
		return {
			'responseError': function(rejection) {
				var status = rejection.status;
				var config = rejection.config;
				var method = config.method;
				var url = config.url;
				if(status == 422){
					console.log(rejection)
				}
				console.log('error: '+method + " on " + url + " failed with status " + status)

				if (status == 401) {
					$rootScope.returnToState = $rootScope.toState;
					$rootScope.returnToStateParams = $rootScope.toStateParams
					$location.path( "/signin" );
				} else if(status == 403){
					$rootScope.error = method + " on " + url + " failed with status " + status;
					$location.path( "/denied" );
				}

				return $q.reject(rejection);
			}
		};
	}]
	);
	
	$httpProvider.interceptors.push(["$q","$injector", "$rootScope", "$location",function ($q,$injector, $rootScope, $location) {
		var cookies = $injector.get('$cookies');
		return {
			'request': function(config) {
				var authToken = $rootScope.authToken;
				if(!angular.isDefined(authToken)) {
					authToken = cookies.get('authToken');
				}
				console.log('authToken: '+authToken);
				var isRestCall = config.url.indexOf('rest') == 6;
				if (isRestCall && angular.isDefined(authToken)) {
					if (nsisUiRoutedAppConfig.useAuthTokenHeader) {
						config.headers['X-Auth-Token'] = authToken;
					} else {
						config.url = config.url + "?token=" + authToken;
					}
				}
				return config || $q.when(config);
			}
		};
	}]
	);

}
])
.run(["$rootScope", "$state", "$stateParams", "$cookies", "AuthWebService","RerouteDetector",function($rootScope, $state, $stateParams, $cookies, AuthWebService, RerouteDetector) {
	
	$rootScope.$on('$stateChangeStart', function(event, toState, toStateParams) {
		
        if(toState.name === "signin"){
           return; // no need to redirect 
        }
		
		var rootScopeToState = $rootScope.toState ? $rootScope.toState.name : null;
		if(toState.name === rootScopeToState){
	        return; // no need to redirect 
	    }
		
		var authToken = $cookies.get('authToken');
		if (authToken) {
			if($rootScope.inSession){
				$cookies.putObject("toState",toState);
				$cookies.putObject("toStateParams", toStateParams);
			}
			
			delete $rootScope.error;
			
			$rootScope.authToken = authToken;
			
			var user = $rootScope.user;
			if(user) proceed(user)
			else AuthWebService.get(function(user){
				
				$cookies.putObject("username",user.username);
				$cookies.putObject("roles",user.roles);
				$cookies.putObject("facilityAssignments",user.facilityAssignments);
				$cookies.putObject("ciType",customerInterfaceType(user.roles))
				proceed(user);
			});
		}else{
			event.preventDefault();
			$state.go('signin');
		}
	});
	
	function proceed(user){
		
		$rootScope.inSession = true;
		$rootScope.user = user;
		
		var exToState = $cookies.getObject("toState");
		var exToStateParams = $cookies.getObject("toStateParams");
		
		$rootScope.toState = exToState;
		$rootScope.toStateParams = exToStateParams;
		
		if(!exToState || !exToState.name) {
			$state.go('home');
		}else if(!exToStateParams){
			$state.go(exToState.name);
		}else if(RerouteDetector.toBeRerouted(exToState.name, exToStateParams)){
			$state.go(RerouteDetector.getRerouteUrl(exToState.name));
		}else
			$state.go(exToState.name,exToStateParams);
	}
	
	function customerInterfaceType(rolesList){
		var returnValue = 'NA';
		if(rolesContains("CSPERSONNEL", rolesList)) {
			returnValue = 'CS';
		}
		if(rolesContains("SPPERSONNEL", rolesList)) {
			returnValue = 'SP';
		}
		if(rolesContains("CRPERSONNEL", rolesList)) {
			returnValue = 'CR';
		}
		return returnValue;
	}
	
	function rolesContains(roleCode, roles){
		return roles.find(function(role){
			return role.codeName === roleCode
		})
	}

	$rootScope.initialized = true;
	console.log('nsisUiRoutedApp is running')
}]);