/**
 * 
 */
nsisApp.directive('nsisCsPageTitle', function() {
    return {
        restrict: 'E',
        priority: 1,
        scope: {
            'type': '=',
            'isForm': '='
        },
        template: "<h3>{{title}}</h3>",
        link: function($scope, element, atttributes){
        	if($scope.isForm) $scope.title = coreTitle($scope.type, true) + " Form";
        	else $scope.title = coreTitle($scope.type, false) + "s";
        	
        	function coreTitle(type, whole){
        		var returnValue = 'Customer Request';
        		return returnValue;
        	}
        }
    };
}).directive('nsisCdTaInventoryItems', ["NsisDirectiveScopeWrapper", "InventoryItemService",function(NsisDirectiveScopeWrapper, InventoryItemService) {
    var directive = {};
    directive.restrict = 'E';
    
    directive.templateUrl = '/NSIS/static/view/ui-routed/typeahead/inventory-item-search-field.html';
    
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'input-sm form-control'; }
        if (!attrs.containerClassIds) { attrs.containerClassIds ='col-sm-9 pull-right'; }
        if (!attrs.augment) {attrs.augment = true;}

        var linkFunction = function($scope, element, atttributes) {
        	if($scope.augment)
        		NsisDirectiveScopeWrapper.augmentScope($scope);
        	
        	$scope.searchInventoryItems = function(val) {
        		var parameters = {
        				codeName: val,
        				description: val, 
        				type: val,
        				searchType: "TYPEHEAD"
        		};
        		
        		return InventoryItemService.get(parameters).$promise.then(function (data) {
        			return data.rows;
        		});
			};
			
			$scope.onSelectInventoryItem = function($item, $model, $label){
				$scope.targetNgModel = $model;
			}
			
        }

        return linkFunction;
    }
    
    directive.scope = {
        ngModel: '=',
        disabled: '=',
        targetNgModel: '=target',
        classIds: '@',
        containerClassIds: '@',
        label: '@',
        textLabel: '@',
        coId: '@',
        qroId: '@',
        augment: '@'
    }

    return directive;
}])
.directive('nsisCdCustomerList',["$uibModal", "NsisDirectiveScopeWrapper",function($uibModal, NsisDirectiveScopeWrapper){
	var directive = {};
    directive.restrict = 'E';
    directive.template = '<div ng-class="xstate" class="form-group no-big-gap">' +
						 '<label for="{{label}}" class="control-label">{{textLabel}}</label>' +
						 '<div class="{{containerClassIds}}">'+
						 '<div class="input-group">'+
						 '<input type="text" class="{{classIds}}" '+
						 'uib-tooltip="{{problemMsg}}" '+
						 'tooltip-enable="{{hasProblem}}" '+
						 'ng-model="ngModel" '+
						 'ng-required="true" close-text="Close" '+
						 'ng-disabled="isDisabled"/> ' + 
						 '<span class="input-group-btn">' +
						 '<button type="button" ng-disabled="isDisabled" class="btn btn-default input-sm" ng-click="open()">' +
						 '<i class="glyphicon glyphicon-folder-open"></i>' +
						 '</button>' +
						 '</span>' +
						 '</div>' +
						 '</div>' +
						 '</div>';
						 
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'input-sm form-control'; }
        if (!attrs.containerClassIds) { attrs.containerClassIds ='col-sm-9 pull-right'; }
        if (!attrs.augment) {attrs.augment = true;}
        if (!attrs.disabled) {attrs.disabled = false;}

        var linkFunction = function($scope, element, atttributes) {
        	if($scope.augment)
        		NsisDirectiveScopeWrapper.augmentScope($scope);

        	$scope.open = function(){
        		openCustomerAccountModal($scope.filter);
        	}
        	
        	function openCustomerAccountModal(customerDetails){
    			var modalInstance = $uibModal.open({
    				animation: true,
    				backdrop: 'static',
    				size: 'lg',
    				templateUrl: parentPopUri + 'customer-account-list.html',
    				controller: 'CustomerAccountsListMCtrl',
    				resolve: {
    					customer: function(){return customerDetails}
    				}
    			});
    			
    			modalInstance.result.then(function (customerAccount) {
    				$scope.targetCustomer = customerAccount;
    				$scope.targetFacility = customerAccount.servicingFacility;
    			}, function (){
    				console.log('Modal dismissed at: ' + new Date());
    			});
    		}
        }
        
        return linkFunction;
    }
    
    directive.scope = {
        ngModel: '=',
        filter: '=',
        targetCustomer: '=target',
        targetFacility: '=',
        disabled: '=',
        label: '@',
        textLabel: '@',
        coId: '@',
        qroId: '@',
        classIds: '@', 
        containerClassIds: '@',
        augment: '@'
    }

    return directive;
}])
.directive('nsisGridCdCustomerAccountList', function() {
	var recalcGridData = [];
	var linkFunction = function($scope, element, atttributes, ctrl, transclude){
		
		/********************************Grid related*******************************/
		var columnDefs = [
		                  {headerName: "Account Number", field: "accountNumber",cellStyle: {'text-align': 'center'}, suppressMenu:true},
		                  {headerName: "Name", field: "name", suppressMenu:true},
		                  {headerName: "Delivery Address", field: "deliveryAddress", suppressMenu:true},
		                  {headerName: "Contact Name", field: "contactName",cellStyle: {'text-align': 'center'}, suppressMenu:true},
		                  {headerName: "Contact No", field: "contactNumber", suppressMenu:true, cellStyle: {'text-align': 'center'}},
		                  {headerName: "Servicing Facility", field: "servicingFacility", suppressMenu:true, cellStyle: {'text-align': 'center'}},
		                  ];

		var dataSource = {
				pageSize: 10,
				getRows: function (params) {
					var parameters = {/*type: 'Labels', */rows: 10, rowOffset: params.startRow};
					if(params.sortModel && params.sortModel.length > 0){
						var sortModel = params.sortModel[0];
						parameters.sidx = sortModel.colId;
						parameters.sord = sortModel.sort;
					}
					$scope.list(parameters, function (data) {
						params.successCallback(data.rows, data.records);
						if ($scope.gridOptions) {
							$scope.gridOptions.api.refreshView();
							$scope.gridOptions.api.sizeColumnsToFit();
						}
					});
				}
		};

		$scope.gridOptions = {
				enableServerSideSorting: true,
				enableFilter: true,
				enableColResize: true,
				columnDefs: columnDefs,
				rowSelection: 'single',
				onGridReady: function (api) {
					$scope.gridOptions.api.setDatasource(dataSource);
					$scope.gridOptions.api.sizeColumnsToFit();
				},
				onRowDoubleClicked: function(params){
					var data =  params['data'];
					$scope.select(data);
				}
		};
		
		$scope.$watch(
	    		'triggerReload',
	    		function(newValue, oldValue) {
	    			if(newValue) {
	    				$scope.gridOptions.api.setDatasource(dataSource);
						$scope.gridOptions.api.sizeColumnsToFit();
	    			}
	    		}
	    );
		
		transclude($scope, function(clone, scope) {
			element.find('.insertion-point').append(clone);
		});
		
	}
    var directive = {};
    
    directive.restrict = 'E';
    directive.transclude = true;
    directive.templateUrl = '/NSIS/static/view/ui-routed/grids/customer-account-list.html';
    
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'default-nsis-grid'; }
        if (!attrs.augment) {attrs.augment = true;}

        return linkFunction;
    }
    
    directive.scope = {
    	select: '=selectAction',
    	list: '=listAction',
    	triggerReload: '=trigger',
        classIds: '@'
    }

    return directive;
})
.directive('nsisGridCdCsCustomerRequest', function() {
	var recalcGridData = [];
	var linkFunction = function($scope, element, atttributes, ctrl, transclude){
		
		/********************************Grid related*******************************/
		var columnDefs = [
		                  {headerName: "Request Number", field: "requestNumber",cellStyle: {'text-align': 'center'}, suppressMenu:true},
		                  {headerName: "Status", field: "status",cellStyle: {'text-align': 'center'}, suppressMenu:true},
		                  {headerName: "Request Date", field: "requestDate", suppressMenu:true},
		                  {headerName: "Request By", field: "requestBy",cellStyle: {'text-align': 'center'}, suppressMenu:true},
		                  {headerName: "Customer Acct No", field: "accountNumber", suppressMenu:true, cellStyle: {'text-align': 'center'}},
		                  {headerName: "Customer", field: "accountName", suppressMenu:true, cellStyle: {'text-align': 'left', 'padding-right':'15px'}},
		                  {headerName: "Facility", field: "facilityCode",cellStyle: {'text-align': 'center'}, suppressMenu:true}
		                  ];

		var dataSource = {
				pageSize: 10,
				getRows: function (params) {
					var parameters = {/*type: 'Labels', */rows: 10, rowOffset: params.startRow};
					if(params.sortModel && params.sortModel.length > 0){
						var sortModel = params.sortModel[0];
						parameters.sidx = sortModel.colId;
						parameters.sord = sortModel.sort;
					}
					$scope.list(parameters, function (data) {
						params.successCallback(data.rows, data.records);
						if ($scope.gridOptions) {
							$scope.gridOptions.api.refreshView();
							$scope.gridOptions.api.sizeColumnsToFit();
						}
					});
				}
		};

		$scope.gridOptions = {
				enableServerSideSorting: true,
				enableFilter: true,
				enableColResize: true,
				columnDefs: columnDefs,
				rowSelection: 'single',
				onGridReady: function (api) {
					$scope.gridOptions.api.setDatasource(dataSource);
					$scope.gridOptions.api.sizeColumnsToFit();
				},
				onRowDoubleClicked: function(params){
					var data =  params['data'];
					$scope.view(data.requestNumber);
				}
		};
		
		$scope.$watch(
	    		'triggerReload',
	    		function(newValue, oldValue) {
	    			if(newValue) {
	    				$scope.gridOptions.api.setDatasource(dataSource);
						$scope.gridOptions.api.sizeColumnsToFit();
	    			}
	    		}
	    );
		
		$scope.remove = function(){
			var requestRequestOverview = $scope.gridOptions.api.getSelectedRows()[0];
			if(requestRequestOverview){
				$scope.removeOr(requestRequestOverview.requestNumber, function(successful){
					if(successful){
						$scope.gridOptions.api.setDatasource(dataSource);
						$scope.gridOptions.api.sizeColumnsToFit();
					}
				});
			}
			
		}

		transclude($scope, function(clone, scope) {
			element.find('.insertion-point').append(clone);
		});
		
	}
    var directive = {};
    
    directive.restrict = 'E';
    directive.transclude = true;
    directive.templateUrl = '/NSIS/static/view/ui-routed/grids/cs-customer-requests.html';
    
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'default-nsis-grid'; }
        if (!attrs.augment) {attrs.augment = true;}

        return linkFunction;
    }
    
    directive.scope = {
    	view: '=viewAction',
    	add: '=addAction',
    	removeOr: '=removeAction',
    	list: '=listAction',
    	triggerReload: '=trigger',
        classIds: '@'
    }

    return directive;
})
.directive('nsisGridCdCrItems', ["$uibModal", "NsisDirectiveScopeWrapper", "CustomerRequestItem",
function($uibModal, NsisDirectiveScopeWrapper, CustomerRequestItem) {
	var recalcGridData = [];
	var linkFunction = function($scope, element, atttributes, ctrl, transclude){
		if($scope.augment)
    		NsisDirectiveScopeWrapper.augmentScope($scope);
		
		$scope.$watch(
			'customerRequest.body.status', function(){
				recalculateItems();
			}
		)
		
		$scope.remove = function(){
			var itemsForRemoval = $scope.gridOptions.api.getSelectedRows();
			console.log('itemsForRemoval.length: '+itemsForRemoval.length)
			if(itemsForRemoval && 0 < itemsForRemoval.length){
				var itemCodeArr = itemsForRemoval.map(function(rowData){return rowData.itemCode});
				$scope.customerRequest.removeRequestedItems(itemCodeArr, function(){
					recalculateItems();
				})
			}
		}
		
		$scope.open = function (size) {
			var customerRequest = $scope.customerRequest.body;
			var data = {
				requestNumber: customerRequest.requestNumber,
				details: {
					type: null,
					description: null,
					codeName: null
				},
				requestDetails: {
					quantity: null,
					remarks: null
				}
			}
			var customerRequestItem = new CustomerRequestItem(data);
			openAddItemModal(customerRequestItem, 'ADD');
		};
		
		/********************************Grid related*******************************/
		var init = true;
		var columnDefs = [
		   {headerName: "Item code", field: "itemCode", suppressMenu:true, cellStyle: {'text-align': 'center'}},
		   {headerName: "Type", field: "type", suppressMenu:true, cellStyle: {'text-align': 'center'}},
		   {headerName: "Description", field: "description", suppressMenu:true},
		   {headerName: "Pcs", field: "quantity", suppressMenu:true, cellStyle: {'text-align': 'right', 'padding-right':'15px'}},
		   {headerName: "Remarks", field: "remarks", suppressMenu:true}
		];
		
		var dataSource = {
				pageSize: 10,
				getRows: function (params) {
					var rowsThisPage = recalcGridData.slice(params.startRow, params.endRow);
					var lastRow = -1;
	                if (recalcGridData.length <= params.endRow) {
	                    lastRow = recalcGridData.length;
	                }
	                params.successCallback(rowsThisPage, lastRow);
				}
		};
		
		$scope.gridOptions = {
				enableServerSideSorting: true,
			    enableFilter: true,
			    enableColResize: true,
			    rowSelection: 'multiple',
				columnDefs: columnDefs,
				onGridReady: function (api) {
					if(init)recalculateItems();
	            },
				onRowDoubleClicked: function(params){
					var data =  params['data'];
					var customerRequestRaw = {};
					var customerRequest = $scope.customerRequest.body;
					customerRequestRaw.requestNumber = customerRequest.requestNumber;
					customerRequestRaw.details ={
							type: data.type,
							description: data.description,
							codeName: data.itemCode
					}
					customerRequestRaw.requestDetails = {
							remarks: data.remarks,
							quantity: data.quantity
					}
					var customerRequestItem = new CustomerRequestItem(customerRequestRaw);
					openAddItemModal(customerRequestItem, 'EDIT');
				}
		};
		
		transclude($scope, function(clone, scope) {
			element.find('.insertion-point').append(clone);
		});
		
		function recalculateItems(){
			$scope.$parent.notifyLoading();
			$scope.customerRequest.showRecalculatedItems(function(result){
				recalcGridData = result;
				$scope.gridOptions.api.setDatasource(dataSource);
				$scope.gridOptions.api.sizeColumnsToFit();
				$scope.$parent.notifyDoneLoading();
				init = false;
			});
		}
		
		function addNewRecordInGrid(requestedItemDetail){
			$scope.$parent.notifyLoading();
			doAdd(requestedItemDetail, function(thereIsChange){
				if(thereIsChange){
					$scope.gridOptions.api.setDatasource(dataSource);
					$scope.gridOptions.api.sizeColumnsToFit();
					$scope.$parent.notifyDoneLoading();
				}
			});
		}
		
		function doAdd(requestedItemDetail, cb){
			var existingRequestedItem = recalcGridData.find(function(item){
				return item.itemCode === requestedItemDetail.itemCode;
			});
			if(existingRequestedItem){
				var thereIsChange = existingRequestedItem.quantity !== requestedItemDetail.quantity || 
									existingRequestedItem.remarks !== requestedItemDetail.remarks;
				console.log('existingRequestedItem@thereIsChange: '+thereIsChange)
				existingRequestedItem.quantity = requestedItemDetail.quantity;
				existingRequestedItem.remarks = angular.copy(requestedItemDetail.remarks);
				
				cb(thereIsChange);
			}else{
				var newRequestedItem = angular.copy(requestedItemDetail);
				newRequestedItem.quantity = parseInt(requestedItemDetail.quantity);
				recalcGridData.push(newRequestedItem);
				cb(true);
			}
		}
		
		function openAddItemModal(customerRequestItem, mode){
			var modalInstance = $uibModal.open({
				animation: true,
				backdrop: 'static',
				templateUrl: parentPopUri + 'cs-request-form-add-item.html',
				controller: 'RequestItemMCtrl',
				resolve: {
					requestedItem: function () {return customerRequestItem;},
					mode: function () {return mode;},
					disabled: $scope.isDisabled
				}
			});
			
			modalInstance.result.then(function (requestedItem) {
				$scope.customerRequest.saveRequestedItem(requestedItem,mode,function(thereIsChange, savedRequestedItem){
					if(thereIsChange) {
						requestedItem.quantity = savedRequestedItem.quantity
						requestedItem.remarks = savedRequestedItem.remarks
						addNewRecordInGrid(requestedItem);
					}
				});
			}, function () {
				console.log('Modal dismissed at: ' + new Date());
			});
		}
	}
    var directive = {};
    
    directive.restrict = 'E';
    directive.transclude = true;
    directive.templateUrl = '/NSIS/static/view/ui-routed/grids/cs-requested-items.html';
    
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'default-nsis-grid'; }
        if (!attrs.augment) {attrs.augment = true;}

        return linkFunction;
    }
    
    directive.scope = {
        customerRequest: '=cr',
        classIds: '@',
        label: '@',
        textLabel: '@',
        coId: '@',
        qroId: '@',
        augment: '@'
    }

    return directive;
}])