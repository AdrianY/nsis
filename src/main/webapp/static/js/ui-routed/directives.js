/**
 * 
 */
nsisApp.directive('nsisUserIdentifier', function() {
    return {
        restrict: 'E',
        priority: 1,
        scope: {
            'ngModel': '='
        },
        template: "<span>{{ngModel}}</span>"
    };
})
.directive('nsisList', ["NsisDirectiveScopeWrapper",function(NsisDirectiveScopeWrapper) {
    var directive = {};
    directive.restrict = 'E';
    directive.transclude=true;
    directive.template = '<div ng-class="xstate" class="form-group no-big-gap">' +
						 '<label for="{{label}}" class="control-label">{{textLabel}}</label>' +
						 '<div class="{{containerClassIds}}">' +
    					 '<select name="{{label}}" class="{{classIds}}" uib-tooltip="{{problemMsg}}" ' +
    					 'ng-model="ngModel" ng-disabled="isDisabled" tooltip-enable="{{hasProblem}}" ng-change="ngChangeFx()">'+
					     '<option ng-repeat="option in options" '+
    					 'value="{{option}}">{{option}}</option>' +
						 '</select>' +
						 '</div>' +
						 '</div>';
    
    
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'input-sm form-control'; }
        if (!attrs.containerClassIds) { attrs.containerClassIds ='col-sm-9 pull-right'; }
        if (!attrs.augment) {attrs.augment = true;}

        var linkFunction = function($scope, element, atttributes) {
        	if($scope.augment)
        		NsisDirectiveScopeWrapper.augmentScope($scope);
        }

        return linkFunction;
    }
    
    directive.scope = {
        ngModel: '=',
        options: '=',
        ngChangeFx: '=',
        classIds: '@',
        containerClassIds: '@',
        label: '@',
        textLabel: '@',
        coId: '@',
        qroId: '@',
        augment: '@'
    }

    return directive;
}])
.directive('nsisTextField',["NsisDirectiveScopeWrapper",function(NsisDirectiveScopeWrapper){
	var directive = {};
    directive.restrict = 'E';
    directive.template = '<div ng-class="xstate" class="form-group no-big-gap">'+
						 '<label for="{{label}}" class="control-label">{{textLabel}}</label>' +
						 '<div class="{{containerClassIds}}"> ' +
						 '<input type="text" id="{{label}}" name="{{label}}" '+
						 'uib-tooltip="{{problemMsg}}" ' +
						 'tooltip-enable="{{hasProblem}}" ' +
						 'class="{{classIds}}" ng-model="ngModel" ' +
						 'ng-disabled="isDisabled"/>' +
						 '</div>' +
						 '</div>';
						 
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'input-sm form-control'; }
        if (!attrs.containerClassIds) { attrs.containerClassIds ='col-sm-9 pull-right'; }
        if (!attrs.augment) {attrs.augment = true;}
        if (!attrs.disabled) {attrs.disabled = false;}

        var linkFunction = function($scope, element, atttributes) {
        	if($scope.augment)
        		NsisDirectiveScopeWrapper.augmentScope($scope);
        }

        return linkFunction;
    }
    
    directive.scope = {
        ngModel: '=',
        disabled: '=',
        label: '@',
        textLabel: '@',
        coId: '@',
        qroId: '@',
        classIds: '@',
        containerClassIds: '@',
        augment: '@'
    }

    return directive;
}])
.directive('nsisTextArea',["NsisDirectiveScopeWrapper",function(NsisDirectiveScopeWrapper){
	var directive = {};
    directive.restrict = 'E';
    directive.template = '<div ng-class="xstate" class="form-group no-big-gap">'+
						 '<label for="{{label}}" class="control-label">{{textLabel}}</label>' +
						 '<div class="{{containerClassIds}}"> ' +
						 '<textarea type="text" name="{{label}}" rows="{{rows}}" '+
						 'uib-tooltip="{{problemMsg}}" ' +
						 'tooltip-enable="{{hasProblem}}" ' +
						 'class="{{classIds}}" ng-model="ngModel" ' +
						 'ng-disabled="isDisabled"/>' +
						 '</div>' +
						 '</div>';
						 
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'input-sm form-control'; }
        if (!attrs.containerClassIds) { attrs.containerClassIds ='col-sm-9 pull-right'; }
        if (!attrs.augment) {attrs.augment = true;}
        if (!attrs.rows) {attrs.rows = "1";}

        var linkFunction = function($scope, element, atttributes) {
        	if($scope.augment)
        		NsisDirectiveScopeWrapper.augmentScope($scope);
        }

        return linkFunction;
    }
    
    directive.scope = {
        ngModel: '=',
        /*ngDisabled: '=',*/
        disabled: '=',
        label: '@',
        textLabel: '@',
        coId: '@',
        qroId: '@',
        rows: '@',
        classIds: '@',
        containerClassIds: '@',
        augment: '@'
    }

    return directive;
}])
.directive('nsisActionButton',["NsisDirectiveScopeWrapper",function(NsisDirectiveScopeWrapper){
	var directive = {};
	directive.transclude = true;
    directive.restrict = 'E';
    directive.template = '<button ng-class="{\'disabled\': xstate === \'has-error\'}" ' +
    					 'class="btn {{classIds}}" ' +
	 					 'ng-disabled="isDisabled" ' +
	 					 'type="button" ng-transclude>{{textLabel}}</button>';
						 
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'btn-default'; }
        if (!attrs.augment) {attrs.augment = true;}
        if (!attrs.disabled) {attrs.disabled = false;}

        var linkFunction = function($scope, element, atttributes) {
        	if($scope.augment)
        		NsisDirectiveScopeWrapper.augmentBtnScope($scope);
        	
            element.bind('click',function (event) {
            	var msg = $scope.confirmMsg;
            	if(msg){
            		if ( window.confirm(msg) ) {
            			$scope.$eval($scope.fx())
                    }
            	}else{
            		$scope.$eval($scope.fx())
            	}
            });
        }

        return linkFunction;
    }
    
    directive.scope = {
        fx: '=action',
        disabled: '=',
        alwaysEnabled: '=enabledAlways',
        confirmMsg: '=confMsg',
        textLabel: '@',
        classIds: '@',
        augment: '@'
    }

    return directive;
}])
.directive('nsisDate',["NsisDirectiveScopeWrapper",function(NsisDirectiveScopeWrapper){
	var directive = {};
    directive.restrict = 'E';
    directive.template = '<div ng-class="xstate" class="form-group no-big-gap">' +
						 '<label for="{{label}}" class="control-label">{{textLabel}}</label>' +
						 '<div class="{{containerClassIds}}">'+
						 '<div class="input-group">'+
						 '<input type="text" class="{{classIds}}" '+
						 'uib-tooltip="{{problemMsg}}" '+
						 'tooltip-enable="{{hasProblem}}" '+
						 'uib-datepicker-popup="{{format}}" ng-model="ngModel" '+
						 'is-open="popup.opened" datepicker-options="dateOptions" ' +
						 'ng-required="true" close-text="Close" '+
						 'alt-input-formats="altInputFormats" ng-disabled="isDisabled"/> ' + 
						 '<span class="input-group-btn">' +
						 '<button type="button" class="btn btn-default input-sm" ng-click="open()">' +
						 '<i class="glyphicon glyphicon-calendar"></i>' +
						 '</button>' +
						 '</span>' +
						 '</div>' +
						 '</div>' +
						 '</div>';
						 
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'input-sm form-control'; }
        if (!attrs.containerClassIds) { attrs.containerClassIds ='col-sm-9 pull-right'; }
        if (!attrs.augment) {attrs.augment = true;}
        if (!attrs.disabled) {attrs.disabled = false;}

        var linkFunction = function($scope, element, atttributes) {
        	if($scope.augment)
        		NsisDirectiveScopeWrapper.augmentScope($scope);

        	if($scope.defaultValue){
        		$scope.ngModel = $scope.defaultValue;
        	}
        	$scope.today = function() {
        		$scope.ngModel = new Date();
        	};

        	$scope.clear = function() {
        		$scope.ngModel = null;
        	};

        	$scope.inlineOptions = {
        			customClass: getDayClass,
        			minDate: new Date(),
        			showWeeks: true
        	};

        	$scope.dateOptions = {
        			formatYear: 'yy',
        			maxDate: new Date(2100, 5, 22),
        			minDate: new Date(),
        			startingDay: 1
        	};

        	$scope.toggleMin = function() {
        		$scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        		$scope.dateOptions.minDate = $scope.inlineOptions.minDate;
        	};

        	$scope.toggleMin();

        	$scope.open = function() {
        		$scope.popup.opened = true;
        	};

        	$scope.setDate = function(year, month, day) {
        		$scope.ngModel = new Date(year, month, day);
        	};

        	$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy','MMMM dd, yyyy', 'shortDate'];
    		$scope.format = $scope.formats[3];
    		$scope.altInputFormats = ['M!/d!/yyyy'];

        	$scope.popup = {
        			opened: false
        	};

        	var tomorrow = new Date();
        	tomorrow.setDate(tomorrow.getDate() + 1);
        	var afterTomorrow = new Date();
        	afterTomorrow.setDate(tomorrow.getDate() + 1);
        	$scope.events = [
        	                 {
        	                	 date: tomorrow,
        	                	 status: 'full'
        	                 },
        	                 {
        	                	 date: afterTomorrow,
        	                	 status: 'partially'
        	                 }
        	                 ];

        	function getDayClass(data) {
        		var date = data.date,
        		mode = data.mode;
        		if (mode === 'day') {
        			var dayToCheck = new Date(date).setHours(0,0,0,0);

        			for (var i = 0; i < $scope.events.length; i++) {
        				var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

        				if (dayToCheck === currentDay) {
        					return $scope.events[i].status;
        				}
        			}
        		}

        		return '';
        	}
        }

        return linkFunction;
    }
    
    directive.scope = {
        ngModel: '=',
        disabled: '=',
        defaultValue: '=',
        label: '@',
        textLabel: '@',
        coId: '@',
        qroId: '@',
        classIds: '@',
        containerClassIds: '@',
        augment: '@'
    }

    return directive;
}])
.directive('nsisDateLite',["NsisDirectiveScopeWrapper",function(NsisDirectiveScopeWrapper){
	var directive = {};
    directive.restrict = 'E';
    directive.template = '<div class="{{containerClassIds}}">' +
    					 '<input type="text" class="{{classIds}}" '+
						 'uib-datepicker-popup="{{format}}" ng-model="ngModel" '+
						 'is-open="popup.opened" datepicker-options="dateOptions" '+
						 'ng-required="true" close-text="Close" '+
						 'alt-input-formats="altInputFormats" name="{{label}}"/> ' +
						 '<span class="input-group-btn"> '+
						 '<button type="button" class="btn btn-default input-sm" '+
						 'ng-click="open()"> '+
						 '<i class="glyphicon glyphicon-calendar"></i> '+
						 '</button>' +
						 '</span>' +
						 '</div>';
						 
    directive.compile = function(elem, attrs) {
    	if (!attrs.classIds) { attrs.classIds = 'input-sm form-control'; }
        if (!attrs.augment) {
        	console.log('overriding console')
        	attrs.augment = true;
        }

        var linkFunction = function($scope, element, atttributes) {
        	
        	if($scope.augment)
        		NsisDirectiveScopeWrapper.augmentScope($scope);
        	
        	$scope.today = function() {
        		$scope.ngModel = new Date();
        	};

        	$scope.clear = function() {
        		$scope.ngModel = null;
        	};

        	$scope.inlineOptions = {
        			customClass: getDayClass,
        			minDate: new Date(),
        			showWeeks: true
        	};

        	$scope.dateOptions = {
        			formatYear: 'yy',
        			maxDate: new Date(2100, 5, 22),
        			minDate: new Date(),
        			startingDay: 1
        	};

        	$scope.toggleMin = function() {
        		$scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        		$scope.dateOptions.minDate = $scope.inlineOptions.minDate;
        	};

        	$scope.toggleMin();

        	$scope.open = function() {
        		$scope.popup.opened = true;
        	};

        	$scope.setDate = function(year, month, day) {
        		$scope.ngModel = new Date(year, month, day);
        	};

        	$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy','MMMM dd, yyyy', 'shortDate'];
    		$scope.format = $scope.formats[3];
    		$scope.altInputFormats = ['M!/d!/yyyy'];

        	$scope.popup = {
        			opened: false
        	};

        	var tomorrow = new Date();
        	tomorrow.setDate(tomorrow.getDate() + 1);
        	var afterTomorrow = new Date();
        	afterTomorrow.setDate(tomorrow.getDate() + 1);
        	$scope.events = [
        	                 {
        	                	 date: tomorrow,
        	                	 status: 'full'
        	                 },
        	                 {
        	                	 date: afterTomorrow,
        	                	 status: 'partially'
        	                 }
        	                 ];

        	function getDayClass(data) {
        		var date = data.date,
        		mode = data.mode;
        		if (mode === 'day') {
        			var dayToCheck = new Date(date).setHours(0,0,0,0);

        			for (var i = 0; i < $scope.events.length; i++) {
        				var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

        				if (dayToCheck === currentDay) {
        					return $scope.events[i].status;
        				}
        			}
        		}

        		return '';
        	}
        }

        return linkFunction;
    }
    
    directive.scope = {
        ngModel: '=ngModel',
        classIds: '@',
        containerClassIds: '@',
        label: '@',
        coId: '@',
        qroId: '@',
        augment: '@'
    }

    return directive;
}])
.directive('ngConfirmClick', [
        function(){
            return {
                link: function (scope, element, attr) {
                    var msg = attr.ngConfirmClick;
                    var clickAction = attr.confirmedClick;
                    element.bind('click',function (event) {
                    	if(msg){
                    		if ( window.confirm(msg) ) {
                                scope.$eval(clickAction)
                            }
                    	}else{
                    		scope.$eval(clickAction)
                    	}
                    });
                },
                scope: {
                	ngConfirmClick: '='
                }
            };
}])
.directive('nsisEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.nsisEnter);
                });

                event.preventDefault();
            }
        });
    };
});