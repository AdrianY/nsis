/**
 * 
 */
angular.module('nsisCsServices', [])
.service('CsCustomerRequestService',["CloneHelper","NHelper","DateUtil",function(CloneHelper,NHelper,DateUtil){
	var CsCustomerRequestCO = function(customerRequest){
		var thisRef = this;
		
		CloneHelper.copyEquivalentAttributes(
				thisRef, customerRequest, 
				['requestNumber','status']);
		thisRef.facilityCode = customerRequest.facilityCodeName;
		NHelper.ifPresent(customerRequest.requiredDate,function(requiredDate){
			thisRef.requiredDate = DateUtil.toStringIgnoreTZ(requiredDate);
		})
		
		thisRef.customer = {
				accountNumber: '',
				deliveryAddress: '',
				customerName: '',
				contactNumber: '',
				contactName: ''
		}
		NHelper.ifPresent(customerRequest.customer,function(customer){
			thisRef.customer.accountNumber = customer.accountNumber
			thisRef.customer.deliveryAddress= customer.deliveryAddress
			thisRef.customer.customerName= customer.name
			thisRef.customer.contactNumber= customer.contactNumber
			thisRef.customer.contactName= customer.contactName
		});
		console.log('customerRequest.requestedItems: '+customerRequest.requestedItems)
		NHelper.ifPresent(customerRequest.requestedItems,function(requestedItems){
			thisRef.requestedItems = requestedItems;
		});
	}
	
	this.generateCustomerRequestCO = function(customerRequest){
		return new CsCustomerRequestCO(customerRequest);
	}
	
	this.sanitizesFRQRO = function(sFRQRO){
		var body = sFRQRO.body;
		if(!body) body = sFRQRO;
		NHelper.ifPresent(body.requestDate,function(rDte){
			body.requestDate = DateUtil.parseShortDate(rDte);
		});
		
		NHelper.ifPresent(body.requiredDate,function(iDte){
			body.requiredDate = DateUtil.parseShortDate(iDte);
		});
		
		return sFRQRO;
	}
}])
.service('CustomerRequestItemService',function(){
	var CustomerRequestItemCO = function(customerRequestItem){
		var thisRef = this;
		thisRef.facilityCode = customerRequestItem.facilityId;
		thisRef.details = {
			itemCode: customerRequestItem.itemCode,
			quantity: customerRequestItem.quantity,
			remarks: customerRequestItem.remarks
		}
	}
	
	this.generateCustomerRequestItemCO = function(customerRequestItem){
		return new CustomerRequestItemCO(customerRequestItem);
	}
	
	this.sanitizeOriQRO = function(oriQRO){
		
	}
})