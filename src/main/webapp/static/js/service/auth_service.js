/**
 * 
 */
var services = angular.module('nsisApp.services', ['ngResource']);

services.factory('AuthSrvc',function($resource){
	return {
		login: function(){
			return $resource('http://localhost:8083/NSIS/rest/:resource/:version/:action', {},{
					authenticate: {
						method: 'POST',
						params: {'resource':'auth','action' : 'authenticate', 'version':'1.0'},
						headers : {'Content-Type': 'application/x-www-form-urlencoded'}
					}
				}
			);
		}
	}
})

services.factory('MaintenanceSrvc',function($resource){
	return $resource('http://localhost:8083/NSIS/rest/:resource/:version/:action', {},{
			authenticate: {
				method: 'POST',
				params: {'resource':'auth','action' : 'authenticate', 'version':'1.0'},
				headers : {'Content-Type': 'application/x-www-form-urlencoded'}
			}
		}
	)
})