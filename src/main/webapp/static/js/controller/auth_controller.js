/**
 * 
 */
//App.controller('AuthCrtlr',['$scope','$location','AuthSrvc',function($scope,$location, AuthService){
//	var user = this;
//	
//	$scope.rememberMe = false;
//	$scope.login = function(){
//		var params = {username: $scope.username, password: $scope.password}
//		alert($scope.username)
//		AuthService.authenticate($.param(params), function(authResult){
//			var authToken = authenticationResult.token;
//			$rootScope.authToken = authToken;
//			if ($scope.rememberMe) {
//				$cookieStore.put('authToken', authToken);
//			}
//			
//			AuthService.get(function(user) {
//				$rootScope.user = user;
//				$location.path("/");
//			});
//		})
//	}
//}])

function LoginCrtlr($scope, $rootScope, $location, $cookieStore, AuthService) {
	
	$scope.rememberMe = false;
	
	$scope.login = function() {
		alert('yeah')
		AuthService.authenticate($.param({username: $scope.username, password: $scope.password}), function(authenticationResult) {
			var authToken = authenticationResult.token;
			$rootScope.authToken = authToken;
			if ($scope.rememberMe) {
				$cookieStore.put('authToken', authToken);
			}
			AuthService.get(function(user) {
				$rootScope.user = user;
				$location.path("/");
			});
		});
	};
};

function MaintenanceCrtlr($scope, $location, AuthSrvc) {
	
	$scope.newsEntry = new AuthSrvc();
	
	$scope.save = function() {
		$scope.newsEntry.$save(function() {
			$location.path('/');
		});
	};
};

