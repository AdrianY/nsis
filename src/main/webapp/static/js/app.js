angular.module('nsisApp', ['ngRoute','ngCookies','nsisApp.services'])
.config(['$routeProvider', '$locationProvider', '$httpProvider',function($routeProvider,$locationProvider,$httpProvider) {

	$routeProvider.when('/', {
		templateUrl : '/NSIS/static/view/home.html',
		controller : 'home'
	}).when('/login', {
		templateUrl : '/NSIS/static/view/login.html',
		controller : 'navigation'
	}).otherwise('/');

	$locationProvider.hashPrefix('!');

//	$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
	$httpProvider.interceptors.push(function ($q, $rootScope, $location) {
		return {
			'responseError': function(rejection) {
				var status = rejection.status;
				var config = rejection.config;
				var method = config.method;
				var url = config.url;

				if (status == 401) {
					$location.path( "/login" );
				} else {
					$rootScope.error = method + " on " + url + " failed with status " + status;
				}

				return $q.reject(rejection);
			}
		};
	}
	);

	/* Registers auth token interceptor, auth token is either passed by header or by query parameter
	 * as soon as there is an authenticated user */
	$httpProvider.interceptors.push(function ($q, $rootScope, $location) {
		return {
			'request': function(config) {
				console.log("config.url.indexOf('rest'): "+config.url.indexOf('rest')+", url: "+config.url)
				var isRestCall = config.url.indexOf('rest') == 6;
				if (isRestCall && angular.isDefined($rootScope.authToken)) {
					var authToken = $rootScope.authToken;
					if (nsisAppConfig.useAuthTokenHeader) {
						config.headers['X-Auth-Token'] = authToken;
					} else {
						config.url = config.url + "?token=" + authToken;
					}
				}
				return config || $q.when(config);
			}
		};
	})
}])
.controller('home', ['$scope','$rootScope',function($scope, $rootScope) {
	var user  = $rootScope.user;
	console.log('home user: '+user);
	$scope.greeting = {username: user.username, fullname: user.firstName + " " + user.lastName}
}])
.controller('navigation', function($rootScope, $scope, $http, $location, $cookieStore, AuthService) {
	//authenticate();
	$scope.credentials = {};
	$scope.login = function() {
		var credentials = $scope.credentials
		var loginData = {username:credentials.username, password:credentials.password}
		AuthService.authenticate($.param(loginData), function(authenticationResult) {
			$rootScope.authenticated = true;
			var authToken = authenticationResult.token;
			$rootScope.authToken = authToken;
			if ($scope.rememberMe) {
				$cookieStore.put('authToken', authToken);
			}
			console.log('init authToken: '+authToken)
			$cookieStore.put('authToken', authToken);
			AuthService.get(function(user) {
				$rootScope.user = user;
				$cookieStore.user = user
				$location.path("/");
			});
		});
	};

}
)
.run(function($rootScope, $location, $cookieStore, AuthService) {

	/* Reset error when a new view is loaded */
	$rootScope.$on('$viewContentLoaded', function() {
		delete $rootScope.error;
	});

//	$rootScope.hasRole = function(role) {
//
//		if ($rootScope.user === undefined) {
//			return false;
//		}
//
//		if ($rootScope.user.roles[role] === undefined) {
//			return false;
//		}
//
//		return $rootScope.user.roles[role];
//	};

	$rootScope.logout = function() {
		delete $rootScope.user;
		delete $rootScope.authToken;
		$rootScope.authenticated = false;
		$cookieStore.remove('authToken');
		$location.path("/login");
	};

	/* Try getting valid user from cookie or go to login page */
	var originalPath = $location.path();
	
	var authToken = $cookieStore.get('authToken');
	
	console.log('init authToken: '+authToken)
	if (authToken !== undefined) {
		$rootScope.authToken = authToken;
		AuthService.get(function(user) {
			$rootScope.user = user;
			console.log('init user: '+$rootScope.user.username+', original path: '+originalPath)
			$location.path(originalPath);
		});
	}else{
		$location.path("/login");
	}

	$rootScope.initialized = true;
})