/**
 * 
 */
var services = angular.module('nsisApp.services', ['ngResource']);

services.factory('AuthService', function($resource) {

	return $resource('/NSIS/rest/auth/1.0/:action', {},
			{
				authenticate: {
					method: 'POST',
					params: {'action' : 'authenticate'},
					headers : {'Content-Type': 'application/x-www-form-urlencoded'}
				}
			}
	);
});