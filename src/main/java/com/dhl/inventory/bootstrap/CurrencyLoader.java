package com.dhl.inventory.bootstrap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.dhl.inventory.dto.commandobject.maintenance.CurrencyCO;
import com.dhl.inventory.repository.maintenance.CurrencyRepository;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.services.maintenance.CurrencyService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class CurrencyLoader implements LoadExecutor {
	
	@Autowired
	private CurrencyRepository currencyRepo;
	
	@Autowired
	@Qualifier("currencyService")
	private CurrencyService currencyService;

	
	private void createCurrency(String codeName, String exchangeRateStr){
		if(canLoadCurrency(codeName)){
			CurrencyCO currencyCommand = new CurrencyCO();
			currencyCommand.setCode(codeName);
			currencyCommand.setExchangeRateToPhp(exchangeRateStr);
			try {
				currencyService.createNewCurrency(currencyCommand);
			} catch (ValidationException e) {
				e.printStackTrace();
			}
		}
	}

	private boolean canLoadCurrency(String codeName){
		return null == currencyRepo.findByCurrencyCode(codeName);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void execute() {
		createCurrency("SGD","33.89");
		createCurrency("PHP","1.00");
		createCurrency("EUR","51.83");
	}
}
