package com.dhl.inventory.bootstrap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.dto.commandobject.RecipientCO;
import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.DeliveredItemBatchCO;
import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.DeliveredItemBatchGroupCO;
import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.DeliveryReceiptCO;
import com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest.SupplyRequestApproverCO;
import com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest.SupplyRequestCO;
import com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest.SupplyRequestItemCO;
import com.dhl.inventory.services.servicecenter.inventorymaintenance.DeliveryReceiptLoaderService;
import com.dhl.inventory.services.servicecenter.supplyrequest.SupplyRequestLoaderService;
import com.dhl.inventory.util.Optional;

@Component
public class PhysicalStockLoader implements LoadExecutor {
	
	private static final Logger LOGGER = Logger.getLogger(PhysicalStockLoader.class);
	
	private static final String DATE_TODAY = "05/13/2016";
	
	@Autowired
	@Qualifier("supplyRequestLoaderService")
	private SupplyRequestLoaderService supplyRequestLoaderService;
	
	@Autowired
	@Qualifier("serviceCenterDeliveryReceiptLoaderService")
	private DeliveryReceiptLoaderService deliveryReceiptLoaderService;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void execute() {
		
		PhysicalStockActualLoader.getInstance(supplyRequestLoaderService, deliveryReceiptLoaderService).setFacilityCode("FSB")
		.registerPhysicalStock("AP-00-11-01016A","A-AA-3",60)
		.registerPhysicalStock("AP-00-11-01016A","A-AA-2",7)
		.registerPhysicalStock("AP-00-11-01016A","A-AA-1",100)
		.registerPhysicalStock("AP-00-11-01016A","A-AB-3",65)
		.registerPhysicalStock("AP-00-11-01016A","A-AB-1",13)
		.registerPhysicalStock("AP-00-11-01016A","A-AC-3",3)
		.registerPhysicalStock("AP-00-11-01016A","A-AC-2",2)
		.registerPhysicalStock("AP-00-11-01016A","A-AC-1",21)
		.registerPhysicalStock("AP-00-11-01016A","A-AD-3",20)
		.registerPhysicalStock("AP-00-11-01016A","A-AD-2",20)
		.registerPhysicalStock("AP-00-11-01016A","A-AD-1",1)
		.registerPhysicalStock("AP-00-11-01016A","A-AE-3",2)
		.registerPhysicalStock("AP-00-11-01016A","A-AE-2",1)
		.registerPhysicalStock("AP-00-11-01016A","A-AE-1",1)
		.registerPhysicalStock("AP-00-11-01016A","A-AF-3",3)
		.registerPhysicalStock("AP-00-11-01016A","A-AF-2",1)
		.registerPhysicalStock("AP-00-11-01016A","A-AF-1",1)
		.registerPhysicalStock("AP-00-11-01016A","A-AG-3",5)
		.registerPhysicalStock("AP-00-11-01016A","A-AG-2",3)
		.registerPhysicalStock("AP-00-11-01016A","A-AG-1",1)
		.registerPhysicalStock("AP-00-11-01016A","B-AA-1",1)
		.registerPhysicalStock("AP-00-11-01016A","B-AB-3",1)
		.registerPhysicalStock("AP-00-11-01016A","B-AB-2",65)
		.execute();
		
		/*PhysicalStockActualLoader.getInstance(supplyRequestLoaderService, deliveryReceiptLoaderService).setFacilityCode("DXO")
		.registerPhysicalStock("AP-00-14-02001","A-AA-3",1000)
		.registerPhysicalStock("AP-00-11-05001","A-AA-3",2000)
		.registerPhysicalStock("AP-00-11-03001","A-AA-1",15500)
		.registerPhysicalStock("AP-00-11-03002","A-AA-1",18750)
		.registerPhysicalStock("AP-00-11-02003","A-AA-3",4000)
		.registerPhysicalStock("AP-00-11-02004","A-AA-3",6000)
		.registerPhysicalStock("AP-00-11-02005","A-AA-3",4000)
		.registerPhysicalStock("AP-00-11-02018","A-AA-3",4000)
		.registerPhysicalStock("AP-00-11-02019","A-AA-3",22000)
		.registerPhysicalStock("AP-00-11-02022","A-AA-3",20000)
		.registerPhysicalStock("AP-00-11-02027","A-AA-3",10000)
		.registerPhysicalStock("AP-00-11-02028","A-AA-3",3000)
		.registerPhysicalStock("AP-00-11-02029","A-AA-3",6000)
		.registerPhysicalStock("AP-00-11-01016A","A-AA-1",8)
		.registerPhysicalStock("AP-00-11-11001","A-AA-1",200)
		.registerPhysicalStock("AP-00-11-11002","A-AA-1",250)
		.registerPhysicalStock("AP-00-11-07001B","A-AB-3",5200)
		.registerPhysicalStock("AP-00-11-08001","A-AB-3",79000)
		.registerPhysicalStock("AP-00-11-01022","A-AB-2",90)
		.registerPhysicalStock("AP-00-11-01023","A-AB-2",310)
		.registerPhysicalStock("AP-00-11-01024","A-AB-2",201)
		.registerPhysicalStock("AP-00-11-01025","A-AB-1",17)
		.registerPhysicalStock("AP-00-11-01027","A-AC-3",5)
		.registerPhysicalStock("AP-00-11-01028","A-AC-3",13)
		.registerPhysicalStock("AP-00-11-01029","A-AC-2",174)
		.registerPhysicalStock("AP-00-11-01030","A-AC-2",220)
		.registerPhysicalStock("AP-00-11-06004","A-AC-1",500)
		.registerPhysicalStock("AP-00-11-04006","A-AC-1",72)
		.registerPhysicalStock("AP-00-11-04010","A-AC-1",10)
		.execute();
		
		
		PhysicalStockActualLoader.getInstance(supplyRequestLoaderService, deliveryReceiptLoaderService).setFacilityCode("DXM")
		.registerPhysicalStock("AP-00-11-01022","A-AA-3",880)
		.registerPhysicalStock("AP-00-11-01022","A-AA-2",1000)
		.registerPhysicalStock("AP-00-11-01022","A-AA-1",860)
		.registerPhysicalStock("AP-00-11-01023","A-AB-3",650)
		.registerPhysicalStock("AP-00-11-01024","A-AB-2",1040)
		.registerPhysicalStock("AP-00-11-01030","A-AB-1",120)
		.registerPhysicalStock("AP-00-11-01029","A-AC-3",200)
		.registerPhysicalStock("PH-00-16-02004","A-AC-2",790)
		.registerPhysicalStock("AP-00-11-07001B","A-AC-1",7000)
		.registerPhysicalStock("AP-00-11-03001","A-AD-3",35250)
		.registerPhysicalStock("AP-00-11-05001","A-AD-2",9500)
		.registerPhysicalStock("AP-00-11-08001","A-AD-1",112000)
		.registerPhysicalStock("AP-00-11-03002","A-AE-3",21750)
		.registerPhysicalStock("AP-00-11-01016A","A-AE-2",10)
		.registerPhysicalStock("AP-00-11-01025","A-AE-1",65)
		.registerPhysicalStock("AP-00-11-01027","A-AF-3",35)
		.registerPhysicalStock("AP-00-11-01028","A-AF-2",15)
		.registerPhysicalStock("AP-00-11-02004","A-AF-1",7000)
		.registerPhysicalStock("AP-00-11-02027","A-AG-3",18000)
		.registerPhysicalStock("AP-00-11-02029","A-AG-2",11000)
		.registerPhysicalStock("AP-00-11-02028","A-AG-1",2000)
		.registerPhysicalStock("AP-00-11-02003","B-AA-3",26000)
		.registerPhysicalStock("AP-00-11-02018","B-AA-2",6000)
		.registerPhysicalStock("AP-00-11-04010","B-AA-1",600)
		.registerPhysicalStock("AP-00-14-02001","B-AB-3",5000)
		.registerPhysicalStock("PH-00-16-02001","B-AB-2",196)
		.registerPhysicalStock("AP-00-11-11001","B-AB-1",300)
		.registerPhysicalStock("AP-00-11-11002","B-AC-3",300)
		.registerPhysicalStock("AP-00-11-02022","B-AC-2",2000)
		.registerPhysicalStock("AP-00-11-02019","B-AC-1",36000)
		.registerPhysicalStock("PH-00-16-06001","B-AD-3",100)
		.execute();
		
		
		PhysicalStockActualLoader.getInstance(supplyRequestLoaderService, deliveryReceiptLoaderService).setFacilityCode("DXS")
		.registerPhysicalStock("AP-00-14-02001","A-AA-3",1000)
		.registerPhysicalStock("AP-00-11-05001","A-AA-3",2000)
		.registerPhysicalStock("AP-00-11-03001","A-AA-1",15500)
		.registerPhysicalStock("AP-00-11-03002","A-AA-1",18750)
		.registerPhysicalStock("AP-00-11-02003","A-AA-3",4000)
		.registerPhysicalStock("AP-00-11-02004","A-AA-3",6000)
		.registerPhysicalStock("AP-00-11-02005","A-AA-3",4000)
		.registerPhysicalStock("AP-00-11-02018","A-AA-3",4000)
		.registerPhysicalStock("AP-00-11-02019","A-AA-3",22000)
		.registerPhysicalStock("AP-00-11-02022","A-AA-3",20000)
		.registerPhysicalStock("AP-00-11-02027","A-AA-3",10000)
		.registerPhysicalStock("AP-00-11-02028","A-AA-3",3000)
		.registerPhysicalStock("AP-00-11-02029","A-AA-3",6000)
		.registerPhysicalStock("AP-00-11-01016A","A-AA-1",8)
		.registerPhysicalStock("AP-00-11-11001","A-AA-1",200)
		.registerPhysicalStock("AP-00-11-11002","A-AA-1",250)
		.registerPhysicalStock("AP-00-11-07001B","A-AB-3",5200)
		.registerPhysicalStock("AP-00-11-08001","A-AB-3",79000)
		.registerPhysicalStock("AP-00-11-01022","A-AB-2",90)
		.registerPhysicalStock("AP-00-11-01023","A-AB-2",310)
		.registerPhysicalStock("AP-00-11-01024","A-AB-2",201)
		.registerPhysicalStock("AP-00-11-01025","A-AB-1",17)
		.registerPhysicalStock("AP-00-11-01027","A-AC-3",5)
		.registerPhysicalStock("AP-00-11-01028","A-AC-3",13)
		.registerPhysicalStock("AP-00-11-01029","A-AC-2",174)
		.registerPhysicalStock("AP-00-11-01030","A-AC-2",220)
		.registerPhysicalStock("AP-00-11-06004","A-AC-1",500)
		.registerPhysicalStock("AP-00-11-04006","A-AC-1",72)
		.registerPhysicalStock("AP-00-11-04010","A-AC-1",10)
		.execute();
		
		PhysicalStockActualLoader.getInstance(supplyRequestLoaderService, deliveryReceiptLoaderService).setFacilityCode("DML")
		.registerPhysicalStock("AP-00-14-02001","A-AA-3",50)
		.registerPhysicalStock("AP-00-11-05001","A-AA-2",14000)
		.registerPhysicalStock("AP-00-11-03001","A-AA-1",3500)
		.registerPhysicalStock("AP-00-11-03002","A-AB-3",22500)
		.registerPhysicalStock("AP-00-11-02004","A-AB-1",32005)
		.registerPhysicalStock("AP-00-11-02018","A-AC-3",24002)
		.registerPhysicalStock("AP-00-11-02019","A-AC-2",6002)
		.registerPhysicalStock("AP-00-11-02027","A-AC-1",4000)
		.registerPhysicalStock("AP-00-11-02029","A-AD-2",4000)
		.registerPhysicalStock("AP-00-11-02029","A-AD-1",20)
		.registerPhysicalStock("AP-00-11-11001","A-AE-3",500)
		.registerPhysicalStock("AP-00-11-11002","A-AE-2",700)
		.registerPhysicalStock("AP-00-11-07001B","A-AE-1",12400)
		.registerPhysicalStock("AP-00-11-08001","A-AF-3",252000)
		.registerPhysicalStock("AP-00-11-01022","A-AF-2",1280)
		.registerPhysicalStock("AP-00-11-01023","A-AF-1",200)
		.registerPhysicalStock("AP-00-11-01024","A-AG-3",510)
		.registerPhysicalStock("AP-00-11-01029","B-AA-1",40)
		.registerPhysicalStock("AP-00-11-01030","B-AB-3",180)
		.registerPhysicalStock("AP-00-11-06004","B-AB-2",50)
		.registerPhysicalStock("AP-00-11-04006","B-AC-3",36)
		.registerPhysicalStock("AP-00-11-04010","B-AC-2",560)
		.registerPhysicalStock("PH-00-16-02001","B-AE-3",90000)
		.registerPhysicalStock("PH-00-16-02002","B-AE-2",2003)
		.registerPhysicalStock("PH-00-16-06001","B-AE-1",800)
		.execute();
		
		
		
		PhysicalStockActualLoader.getInstance(supplyRequestLoaderService, deliveryReceiptLoaderService).setFacilityCode("DXC")
		.registerPhysicalStock("PH-00-16-01008","A-AA-3",193)
		.registerPhysicalStock("PH-00-16-01005","A-AA-2",136)
		.registerPhysicalStock("PH-00-16-01006","A-AA-1",18)
		.registerPhysicalStock("AP-00-11-01022","A-AB-3",320)
		.registerPhysicalStock("AP-00-11-01023","A-AB-2",110)
		.registerPhysicalStock("AP-00-11-01024","A-AB-1",161)
		.registerPhysicalStock("AP-00-11-07001B","A-AC-2",148)
		.registerPhysicalStock("AP-00-11-01016A","A-AC-1",4)
		.registerPhysicalStock("AP-00-11-03002","A-AD-3",150)
		.registerPhysicalStock("AP-00-11-08001","A-AD-2",112)
		.registerPhysicalStock("AP-00-11-02019","A-AD-1",11)
		.registerPhysicalStock("AP-00-11-02027","A-AE-3",3)
		.registerPhysicalStock("AP-00-11-02029","A-AE-2",1)
		.registerPhysicalStock("AP-00-11-03001","A-AE-1",64)
		.registerPhysicalStock("AP-00-11-05001","A-AF-3",18)
		.registerPhysicalStock("PH-00-16-02003","A-AF-2",58)
		.registerPhysicalStock("PH-00-16-02004","A-AF-1",11)
		.registerPhysicalStock("AP-00-11-06005","A-AG-3",10)
		.registerPhysicalStock("PH-00-16-04001","A-AG-2",3)
		.registerPhysicalStock("PH-00-16-04002","A-AG-1",7)
		.registerPhysicalStock("PH-00-16-06002","B-AA-3",800)
		.registerPhysicalStock("AP-00-11-02027","B-AA-2",5)
		.registerPhysicalStock("AP-00-11-02005","B-AA-1",4)
		.registerPhysicalStock("AP-00-11-11002","B-AB-2",2)
		.registerPhysicalStock("AP-00-11-11001","B-AB-1",2)
		.execute();
		
		
		
		PhysicalStockActualLoader.getInstance(supplyRequestLoaderService, deliveryReceiptLoaderService).setFacilityCode("CKF")
		.registerPhysicalStock("AP-00-14-02001","A-AA-3",1000)
		.registerPhysicalStock("AP-00-11-05001","A-AA-2",1000)
		.registerPhysicalStock("AP-00-11-03001","A-AA-1",500)
		.registerPhysicalStock("AP-00-11-03002","A-AB-3",7000)
		.registerPhysicalStock("AP-00-11-02003","A-AB-2",2000)
		.registerPhysicalStock("AP-00-11-02004","A-AB-1",2000)
		.registerPhysicalStock("AP-00-11-02018","A-AC-3",4000)
		.registerPhysicalStock("AP-00-11-02019","A-AC-2",6000)
		.registerPhysicalStock("AP-00-11-02027","A-AC-1",1000)
		.registerPhysicalStock("AP-00-11-02028","A-AD-3",2000)
		.registerPhysicalStock("AP-00-11-02029","A-AD-2",1000)
		.registerPhysicalStock("AP-00-11-11001","A-AE-3",1000)
		.registerPhysicalStock("AP-00-11-11002","A-AE-2",400)
		.registerPhysicalStock("AP-00-11-07001B","A-AE-1",600)
		.registerPhysicalStock("AP-00-11-08001","A-AF-3",2000)
		.registerPhysicalStock("AP-00-11-01022","A-AF-2",140)
		.registerPhysicalStock("AP-00-11-01023","A-AF-1",90)
		.registerPhysicalStock("AP-00-11-01024","A-AG-3",378)
		.registerPhysicalStock("AP-00-11-01025","A-AG-2",57)
		.registerPhysicalStock("AP-00-11-04006","B-AC-3",36)
		.registerPhysicalStock("AP-00-11-04010","B-AC-2",144)
		.registerPhysicalStock("PH-00-16-01007","B-AD-2",90)
		.registerPhysicalStock("PH-00-16-01008","B-AD-1",76)
		.registerPhysicalStock("PH-00-16-02001","B-AE-3",280)
		.registerPhysicalStock("PH-00-16-06001","B-AE-1",200)
		.execute();
		
		PhysicalStockActualLoader.getInstance(supplyRequestLoaderService, deliveryReceiptLoaderService).setFacilityCode("FSB")
		.registerPhysicalStock("PH-00-16-01007","A-AA-3",60)
		.registerPhysicalStock("AP-00-11-01028","A-AA-2",7)
		.registerPhysicalStock("AP-00-11-01024","A-AA-1",100)
		.registerPhysicalStock("AP-00-11-08001","A-AB-3",65)
		.registerPhysicalStock("AP-00-11-01016A","A-AB-2",5)
		.registerPhysicalStock("AP-00-11-03002","A-AB-1",13)
		.registerPhysicalStock("AP-00-11-03001","A-AC-3",3)
		.registerPhysicalStock("PH-00-16-06001","A-AC-2",2)
		.registerPhysicalStock("AP-00-11-07001B","A-AC-1",21)
		.registerPhysicalStock("AP-00-11-11001","A-AD-3",20)
		.registerPhysicalStock("AP-00-11-11002","A-AD-2",20)
		.registerPhysicalStock("AP-00-11-02027","A-AD-1",1)
		.registerPhysicalStock("AP-00-11-02028","A-AE-3",2)
		.registerPhysicalStock("AP-00-11-02018","A-AE-2",1)
		.registerPhysicalStock("PH-00-16-02001","A-AE-1",1)
		.registerPhysicalStock("AP-00-11-02019","A-AF-3",3)
		.registerPhysicalStock("AP-00-11-02005","A-AF-2",1)
		.registerPhysicalStock("AP-00-11-02004","A-AF-1",1)
		.registerPhysicalStock("PH-00-16-02004","A-AG-3",5)
		.registerPhysicalStock("AP-00-11-04010","A-AG-2",3)
		.registerPhysicalStock("AP-00-11-04006","A-AG-1",1)
		.registerPhysicalStock("AP-00-11-02004","B-AA-1",1)
		.registerPhysicalStock("AP-00-11-02029","B-AB-3",1)
		.registerPhysicalStock("AP-00-11-08001","B-AB-2",65)
		.execute();
		
		PhysicalStockActualLoader.getInstance(supplyRequestLoaderService, deliveryReceiptLoaderService).setFacilityCode("CEB")
		.registerPhysicalStock("AP-00-14-02001","A-AA-3",2132)
		.registerPhysicalStock("AP-00-11-05001","A-AA-2",500)
		.registerPhysicalStock("AP-00-11-03002","A-AB-3",612)
		.registerPhysicalStock("AP-00-11-02003","A-AB-2",1)
		.registerPhysicalStock("AP-00-11-02004","A-AB-1",7)
		.registerPhysicalStock("AP-00-11-02018","A-AC-3",8)
		.registerPhysicalStock("AP-00-11-02027","A-AC-1",1)
		.registerPhysicalStock("AP-00-11-02028","A-AD-3",4)
		.registerPhysicalStock("AP-00-11-02029","A-AD-2",2)
		.registerPhysicalStock("AP-00-11-01016A","A-AD-1",8)
		.registerPhysicalStock("AP-00-11-11001","A-AE-3",18)
		.registerPhysicalStock("AP-00-11-11002","A-AE-2",12)
		.registerPhysicalStock("AP-00-11-07001B","A-AE-1",240)
		.registerPhysicalStock("AP-00-11-08001","A-AF-3",21)
		.registerPhysicalStock("AP-00-11-01022","A-AF-2",9)
		.registerPhysicalStock("AP-00-11-01023","A-AF-1",28)
		.registerPhysicalStock("AP-00-11-01024","A-AG-3",234)
		.registerPhysicalStock("AP-00-11-01025","A-AG-2",64)
		.registerPhysicalStock("AP-00-11-01029","B-AA-1",10)
		.registerPhysicalStock("PH-00-16-02001","B-AE-3",7)
		.registerPhysicalStock("PH-00-16-06001","B-AE-1",163)
		.registerPhysicalStock("AP-00-11-01016A","B-AF-2",32)
		.registerPhysicalStock("AP-00-11-11001","B-AF-1",800)
		.registerPhysicalStock("AP-00-11-11002","B-AG-3",100)
		.registerPhysicalStock("AP-00-11-07001B","B-AG-2",14800)
		.registerPhysicalStock("AP-00-11-08001","B-AG-1",69000)
		.registerPhysicalStock("AP-00-11-01022","C-AA-3",400)
		.execute();*/
	}
	
	private static class PhysicalStockActualLoader{
		
		private SupplyRequestLoaderService supplyRequestLoaderService;
		
		private DeliveryReceiptLoaderService deliveryReceiptLoaderService;
		
		private String facilityCode;
		
		private List<PhysicalStock> physicalStocks;
		
		static PhysicalStockActualLoader getInstance(
				SupplyRequestLoaderService supplyRequestService, 
				DeliveryReceiptLoaderService deliveryReceiptService
		){
			return new PhysicalStockActualLoader(supplyRequestService, deliveryReceiptService);
		}
		
		private PhysicalStockActualLoader(
				final SupplyRequestLoaderService supplyRequestService,
				final DeliveryReceiptLoaderService deliveryReceiptService
		){
			supplyRequestLoaderService = supplyRequestService;
			deliveryReceiptLoaderService = deliveryReceiptService;
			physicalStocks = new ArrayList<>();
		}
		
		public PhysicalStockActualLoader setFacilityCode(String facilityCode){
			this.facilityCode = facilityCode;
			return this;
		}
		
		public PhysicalStockActualLoader registerPhysicalStock(
				String itemCode, String rackCode, int pieces
		){
			physicalStocks.add(new PhysicalStock(itemCode, rackCode, pieces));
			return this;
		}
		
		public void execute(){
			LOGGER.info("STARTED loading initial physical stock for: "+facilityCode);
			
			SupplyRequestCO dummyRequestCO = generateSupplyRequestCO();
			String requestNumber = 
					supplyRequestLoaderService.generateDummySupplyRequest(dummyRequestCO);
			LOGGER.info("FINISHED Dummy request: "+requestNumber);
			DeliveryReceiptCO dummyDeliveryReceiptCO = generateDeliveryReceiptCO(requestNumber); 
			deliveryReceiptLoaderService.generateDummyDeliveryReceipt(dummyDeliveryReceiptCO);
			
			LOGGER.info("FINISHED loading initial physical stock for: "+facilityCode);
		}
		
		private SupplyRequestCO generateSupplyRequestCO(){
			SupplyRequestCO returnValue = new SupplyRequestCO();
			returnValue.setFacilityCodeName(facilityCode);
			returnValue.setCurrency("PHP");
			returnValue.setIssuanceDate(DATE_TODAY);
			returnValue.setRecipient(generateRecipient());
			returnValue.setRequestCycle("WK1");
			returnValue.setRequestDate(DATE_TODAY);
			returnValue.setApprover(generateSupplyRequestApprover());
			returnValue.setItemsRequested(generateSupplyRequestItemBasedOnRegisteredStock());
			returnValue.setRequestByUsername("SYSTEM");
			return returnValue;
		}
		
		private DeliveryReceiptCO generateDeliveryReceiptCO(final String requestNumber){
			DeliveryReceiptCO returnValue = new DeliveryReceiptCO();
			returnValue.setRequestNumber(requestNumber);
			returnValue.setDateDelivered(DATE_TODAY);
			returnValue.setItemBatchGroups(generateDummyDeliveredItemBatchGroupsCO());
			return returnValue;
		}
		
		private RecipientCO generateRecipient(){
			RecipientCO returnValue = new RecipientCO();
			returnValue.setContactNumber("SYSTEM");
			returnValue.setDeliveryAddress("SYSTEM");
			returnValue.setEmailAddress("SYSTEM");
			returnValue.setUsername("SYSTEM");
			return returnValue;
		}
		
		private SupplyRequestApproverCO generateSupplyRequestApprover(){
			SupplyRequestApproverCO returnValue = new SupplyRequestApproverCO();
			returnValue.setRemarks("For initial physical stock for facility: "+
					facilityCode+". SYSTEM created.");
			returnValue.setUsername("SYSTEM");
			return returnValue;
		}
		
		private ArrayList<SupplyRequestItemCO> generateSupplyRequestItemBasedOnRegisteredStock(){
			ArrayList<SupplyRequestItemCO> returnValue =new ArrayList<SupplyRequestItemCO>();
			List<String> uniqueItemCodes= new ArrayList<>();
			
			physicalStocks.stream().forEach(val -> {
				if(!uniqueItemCodes.contains(val.getItemCode())){
					uniqueItemCodes.add(val.getItemCode());
				}
			});
			
			uniqueItemCodes.stream().forEach(val -> {
				SupplyRequestItemCO dummyRequestItem = new SupplyRequestItemCO();
				dummyRequestItem.setItemCode(val);
				dummyRequestItem.setRemarks("SYSTEM");
				dummyRequestItem.setQuantity(1);
				returnValue.add(dummyRequestItem);
			});
			return returnValue;
		}
		
		private ArrayList<DeliveredItemBatchGroupCO> generateDummyDeliveredItemBatchGroupsCO(){
			
			Map<String, List<PhysicalStock>> physicalStockGroupByItems = new HashMap<>();
			physicalStocks.stream().forEach(val -> {
				List<PhysicalStock> physicalStockGroup = physicalStockGroupByItems.get(val.getItemCode());
				if(Optional.object(physicalStockGroup).hasContent()){
					physicalStockGroup.add(val);
				}else{
					physicalStockGroup = new ArrayList<>();
					physicalStockGroup.add(val);
					physicalStockGroupByItems.put(val.getItemCode(), physicalStockGroup);
				}
			});
			
			ArrayList<DeliveredItemBatchGroupCO> returnValue = new ArrayList<>();
			physicalStockGroupByItems.forEach((key, val) -> {
				DeliveredItemBatchGroupCO dummyGroupCO = new DeliveredItemBatchGroupCO();
				dummyGroupCO.setItemCode(key);
				dummyGroupCO.setItemBatches(generateDummyItemBatchesCO(val));
				returnValue.add(dummyGroupCO);
			});
			
			return returnValue;
		}
		
		private ArrayList<DeliveredItemBatchCO> generateDummyItemBatchesCO(final List<PhysicalStock> physicalStockGroup){
			ArrayList<DeliveredItemBatchCO> returnValue = new ArrayList<>();
			physicalStockGroup.stream().forEach(val -> {
				DeliveredItemBatchCO dummyItemBatchCO =  new DeliveredItemBatchCO();
				dummyItemBatchCO.setRackingCode(val.getRackCode());
				dummyItemBatchCO.setQuantity(val.getNumberOfPieces());
				dummyItemBatchCO.setDateStored(DATE_TODAY);
				returnValue.add(dummyItemBatchCO);
			});
			return returnValue;
		}
	}
	
	private static class PhysicalStock{
		private String itemCode;
		
		private String rackCode;
		
		private int numberOfPieces;

		public PhysicalStock(String itemCode, String rackCode, int numberOfPieces) {
			this.itemCode = itemCode;
			this.rackCode = rackCode;
			this.numberOfPieces = numberOfPieces;
		}

		/**
		 * @return the itemCode
		 */
		public String getItemCode() {
			return itemCode;
		}

		/**
		 * @return the rackCode
		 */
		public String getRackCode() {
			return rackCode;
		}

		/**
		 * @return the numberOfPieces
		 */
		public int getNumberOfPieces() {
			return numberOfPieces;
		}
	}
}
