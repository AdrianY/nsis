package com.dhl.inventory.bootstrap;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.dto.commandobject.maintenance.InventoryItemCO;
import com.dhl.inventory.repository.maintenance.InventoryItemRepository;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.services.maintenance.InventoryItemService;

import org.springframework.stereotype.Component;

@Component
public class InventoryItemLoader  implements LoadExecutor {

	private static final Logger LOGGER = Logger.getLogger(InventoryItemLoader.class);
	
	@Autowired
	@Qualifier("inventoryItemService")
	private InventoryItemService inventoryItemService;
	
	@Autowired
	private InventoryItemRepository inventoryItemRepo;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void execute(){
		LOGGER.info("ContextRefreshedEvent - bootstrapping inventory items");

		createNewItem("AP-00-14-02001","Airwaybill (forecast from Y2008)","Operational Print","CTN",1000,"SGD","27.3");
		createNewItem("AP-00-11-05001","Containerization Seal (HUID)","Bags, Purse","CTN",500,"SGD","35");
		createNewItem("AP-00-11-05002","Bag seal","Bags, Purse","CTN",6000,"SGD","175");
		createNewItem("AP-00-11-03001","Express Flyer - Standard","Flyers","CTN",250,"SGD","36");
		createNewItem("AP-00-11-03002","Express Flyer - Large","Flyers","CTN",250,"SGD","26");
		createNewItem("AP-00-11-02003","Heavy Shipment Label","Labels","CTN",2000,"SGD","10.3");
		createNewItem("AP-00-11-02004","Service Alert Label","Labels","CTN",1000,"SGD","20");
		createNewItem("AP-00-11-02005","Break Bulk Shipment Label ","Labels","CTN",2000,"SGD","20");
		createNewItem("AP-00-11-02018","Security Checked Label","Labels","CTN",2000,"SGD","26");
		createNewItem("AP-00-11-02019","Fragile Label","Labels","CTN",2000,"SGD","26");
		createNewItem("AP-00-11-02022","Diplomatic Bag Label","Labels","CTN",2000,"SGD","49");
		createNewItem("AP-00-11-02027","Service Alert 9:00 Label","Labels","CTN",1000,"SGD","10.3");
		createNewItem("AP-00-11-02028","Service Alert 12:00 Label","Labels","CTN",1000,"SGD","10.3");
		createNewItem("AP-00-11-02029","Service Alert 10:30 Label","Labels","CTN",1000,"SGD","10.3");
		createNewItem("AP-00-11-01016A","Express Box 800 (Express Pallet)","Express Pallet","CTN",1,"SGD","95");
		createNewItem("AP-00-11-11001","Green DOX Woven Bag","Sorting Bags","BUNDLE",1000,"SGD","184.3");
		createNewItem("AP-00-11-11002","Red WPX Woven Bag","Sorting Bags","BUNDLE",100,"SGD","179.3");
		createNewItem("AP-00-11-07001B","Global Packaging Range - Envelope 1","Carton Envelopes","CTN",200,"SGD","27.8");
		createNewItem("AP-00-11-08001","Packing List Envelope","Packing list Envelopes","CTN",1000,"SGD","44");
		createNewItem("AP-00-11-01022","Global Packaging Range - Box 2","Corrugated Boxes","BUNDLE",20,"SGD","8");
		createNewItem("AP-00-11-01023","Global Packaging Range - Box 3","Corrugated Boxes","BUNDLE",10,"SGD","6.5");
		createNewItem("AP-00-11-01024","Global Packaging Range - Box 4","Corrugated Boxes","BUNDLE",20,"SGD","17");
		createNewItem("AP-00-11-01025","Global Packaging Range - Box 5","Corrugated Boxes","BUNDLE",5,"SGD","9.5");
		createNewItem("AP-00-11-01026","Global Packaging Range - Box 6","Corrugated Boxes","BUNDLE",5,"SGD","12");
		createNewItem("AP-00-11-01027","Global Packaging Range - Box 7","Corrugated Boxes","BUNDLE",5,"SGD","14.5");
		createNewItem("AP-00-11-01028","Global Packaging Range - Box 8","Corrugated Boxes","BUNDLE",5,"SGD","17.5");
		createNewItem("AP-00-11-01029","Global Packaging Range - Tri Tube Large","Corrugated Boxes","BUNDLE",20,"SGD","16.5");
		createNewItem("AP-00-11-01030","Global Packaging Range - Tri Tube Small","Corrugated Boxes","BUNDLE",20,"SGD","10");
		createNewItem("AP-00-11-06004","TDW Bag","Bags, Purse","CTN",50,"SGD","30.5");
		createNewItem("AP-00-11-06005","TDX Bag","Bags, Purse","CTN",50,"SGD","30.5");
		createNewItem("AP-00-11-06006","AKE ULD Bag","Bags, Purse","CTN",10,"SGD","32");
		createNewItem("AP-00-11-06007","AAK/AAJ ULD Bag","Bags, Purse","CTN",10,"SGD","6.804");
		createNewItem("AP-00-11-06008","AAX ULD Bag","Bags, Purse","CTN",10,"SGD","58.2");
		createNewItem("AP-00-11-06009","AAC ULD Bag","Bags, Purse","CTN",10,"SGD","48.7");
		createNewItem("AP-00-11-06010","AMD/AMJ ULD Sheet","Bags, Purse","CTN",10,"SGD","34");
		createNewItem("AP-00-11-04002","DHL Custom Opened Resealed Tape","Packaging Tapes","ROLL",36,"SGD","63.5");
		createNewItem("AP-00-11-04006","DHL Repacked Tape","Packaging Tapes","ROLL",36,"SGD","63.5");
		createNewItem("AP-00-11-04010","DHL Tape","Packaging Tapes","ROLL",36,"SGD","63.5");
		createNewItem("PH-00-16-01001","Express Pallet local","Pallet","PC",1,"PHP","750");
		createNewItem("PH-00-16-01002","Storage Box local","Corrugated Boxes","BUNDLE",10,"PHP","46");
		createNewItem("PH-00-16-01005","Box 5 local Brown","Corrugated Boxes","BUNDLE",5,"PHP","44.3");
		createNewItem("PH-00-16-01006","Box 6 local Brown","Corrugated Boxes","BUNDLE",5,"PHP","64.98");
		createNewItem("PH-00-16-01007","Box 7 local Brown","Corrugated Boxes","BUNDLE",5,"PHP","85.4");
		createNewItem("PH-00-16-01008","Box 8 local Brown","Corrugated Boxes","BUNDLE",5,"PHP","92.72");
		createNewItem("PH-00-16-02001","Easyship Label (190x105xID40mm) local","Labels","BOX",14,"PHP","176.79");
		createNewItem("PH-00-16-02002","Easyship Label (190x200xID40mm) local","Labels","B",14,"PHP","176.79");
		createNewItem("PH-00-16-02003","Huid Label local","Labels","BOX",14,"PHP","258.93");
		createNewItem("PH-00-16-02004","R/W WDL Label (with DHL logo) local","Labels","BOX",10,"PHP","276.79");
		createNewItem("PH-00-16-06001","CLEAR PLASTIC BAG (17 1/2 x 12 x 45) local","Sorting Bags","BUNDLE",100,"PHP","19.48");
		createNewItem("PH-00-16-06002","CLEAR PLASTIC BAG (17 1/2 x 12 x 35) local","Sorting Bags","BUNDLE",100,"PHP","15");
		createNewItem("PH-00-16-03003","White Opaque Plastic Pouch local","Sorting Bags","BUNDLE",500,"PHP","1.86");
		createNewItem("PH-00-16-04001","Do Not Break Down Pallet Tape local","Tapes","BOX",120,"PHP","42.86");
		createNewItem("PH-00-16-04002","Dhl Tape Local","Tapes","BOX",120,"PHP","42.86");
	}

	private void createNewItem(String codeName, String description, String type, String uom, int piecesPerUom, String currency, String cost){
		
		if(canLoadItem(codeName)){
			InventoryItemCO itemForm = 
					new InventoryItemCO(codeName, type)
					.setDescription(description)
					.setUnitOfMeasurement(uom)
					.setPiecesPerUom(piecesPerUom)
					.setCost(cost)
					.setCurrency(currency);
			
			try {
				inventoryItemService.createNewInventoryItem(itemForm);
			} catch (ValidationException e) {
				LOGGER.error("Cannot load item " + codeName, e);
			}
		}
		
	}
	
	private boolean canLoadItem(String codeName){
		boolean returnValue = null == inventoryItemRepo.findByCodeName(codeName);
		return returnValue;
	}
}
