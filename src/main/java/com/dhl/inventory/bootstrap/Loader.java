package com.dhl.inventory.bootstrap;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@PropertySource(value = { "classpath:application.properties" })
public class Loader implements ApplicationListener<ContextRefreshedEvent> {
	
	private static final Logger LOGGER = Logger.getLogger(Loader.class);
	
	private List<LoadExecutor> executors;
	
	@Autowired
	private Environment environment;
	
	@Autowired
	private ApplicationContext appContext;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		loadExecutors();
		
		boolean runBootstrap = environment.getRequiredProperty("nsis.run_bootstrap", Boolean.class);
		
		if(runBootstrap){
			LOGGER.info("no. of load executors: "+executors.size());
			for(LoadExecutor executor: executors){
				executor.execute();
			}
		}else{
			LOGGER.info("bootstrap sequence is disabled");
		}
	}
	
	private void loadExecutors(){
		executors = new ArrayList<LoadExecutor>();
		/*executors.add(appContext.getBean(SecurityProfilesLoader.class));
		executors.add(appContext.getBean(CurrencyLoader.class));
		executors.add(appContext.getBean(InventoryItemLoader.class));
		executors.add(appContext.getBean(FacilityItemLoader.class));
		executors.add(appContext.getBean(FacilityPersonnelLoader.class));
		executors.add(appContext.getBean(WarehouseLocationLoader.class));
		executors.add(appContext.getBean(CustomerAccountLoader.class));
		executors.add(appContext.getBean(RouteLoader.class));*/
		executors.add(appContext.getBean(PhysicalStockLoader.class));
		/*executors.add(appContext.getBean(SecurityProfileRoleUpdateLoader.class));*/
	}

}
