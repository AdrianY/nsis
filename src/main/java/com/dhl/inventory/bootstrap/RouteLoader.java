package com.dhl.inventory.bootstrap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.maintenance.Route;
import com.dhl.inventory.repository.maintenance.FacilityRepository;
import com.dhl.inventory.repository.maintenance.RouteRepository;

@Component
public class RouteLoader implements LoadExecutor {

	@Autowired
	private RouteRepository routeRepo;
	
	@Autowired
	private FacilityRepository facilityRepo;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void execute() {
		Facility dxm = facilityRepo.findByCodeName("DXM");
		Facility dxo = facilityRepo.findByCodeName("DXO");
		Facility dml = facilityRepo.findByCodeName("DML");
		Facility dxs = facilityRepo.findByCodeName("DXS");
		Facility dxc = facilityRepo.findByCodeName("DXC");
		Facility ckf = facilityRepo.findByCodeName("CKF");
		Facility fsb = facilityRepo.findByCodeName("FSB");
		Facility ceb = facilityRepo.findByCodeName("CEB");

		createRoute("CE01",ceb);
		createRoute("CE02",ceb);
		createRoute("CE03",ceb);
		createRoute("CE04",ceb);
		createRoute("CE05",ceb);
		createRoute("CE06",ceb);
		createRoute("CE11",ceb);
		createRoute("CE16",ceb);
		createRoute("CE21",ceb);
		createRoute("CE26",ceb);
		createRoute("CE31",ceb);
		createRoute("CE36",ceb);
		createRoute("CEY1",ceb);
		createRoute("CEB1",ceb);
		createRoute("CEB2",ceb);

		createRoute("CL01",dxc);
		createRoute("CL11",dxc);
		createRoute("CL21",dxc);
		createRoute("CL31",dxc);
		createRoute("CL36",dxc);
		createRoute("CL41",dxc);
		createRoute("CLB2",dxc);
		createRoute("CLB3",dxc);
		createRoute("CLB5",dxc);
		createRoute("CLB9",dxc);
		createRoute("CL35",dxc);
		createRoute("CLB6",dxc);
		createRoute("CL26",dxc);
		
		createRoute("OR11",dxo);
		createRoute("OR16",dxo);
		createRoute("OR21",dxo);
		createRoute("OR26",dxo);
		createRoute("OR31",dxo);
		createRoute("OR36",dxo);
		createRoute("OR41",dxo);
		createRoute("OR46",dxo);
		createRoute("OR51",dxo);
		createRoute("OR56",dxo);
		createRoute("OR61",dxo);
		createRoute("OR66",dxo);
		createRoute("OR71",dxo);
		createRoute("OR76",dxo);
		createRoute("ORB1",dxo);
		createRoute("ORB2",dxo);
		createRoute("ORB3",dxo);
		createRoute("ORB4",dxo);
		createRoute("ORB5",dxo);
		createRoute("ORB6",dxo);
		createRoute("ORB8",dxo);
		createRoute("ORW1",dxo);
		
		createRoute("CK01",ckf);
		createRoute("CK06",ckf);
		createRoute("CK11",ckf);
		createRoute("CK16",ckf);
		createRoute("CK21",ckf);
		createRoute("CK02",ckf);

		createRoute("MO06",dml);
		createRoute("MO11",dml);
		createRoute("MO16",dml);
		createRoute("MO21",dml);
		createRoute("MO31",dml);
		createRoute("MO36",dml);
		createRoute("MO46",dml);
		createRoute("MO56",dml);
		createRoute("MO61",dml);
		createRoute("MO71",dml);
		createRoute("MOB1",dml);
		createRoute("MOB2",dml);
		createRoute("MOB3",dml);
		createRoute("MOB4",dml);
		createRoute("MOB5",dml);
		createRoute("MOB6",dml);
		createRoute("MOB7",dml);
		createRoute("MOB8",dml);
		createRoute("MOB9",dml);
		createRoute("MOW1",dml);
		createRoute("MO01",dml);
		createRoute("MO26",dml);
		createRoute("MO41",dml);
		createRoute("MO51",dml);

		createRoute("MK01",dxm);
		createRoute("MK08",dxm);
		createRoute("MK22",dxm);
		createRoute("MK28",dxm);
		createRoute("MK49",dxm);
		createRoute("MK75",dxm);
		createRoute("MK04",dxm);
		createRoute("MK07",dxm);
		createRoute("MK10",dxm);
		createRoute("MK05",dxm);
		createRoute("MK16",dxm);
		createRoute("MK23",dxm);
		createRoute("MK25",dxm);
		createRoute("MK26",dxm);
		createRoute("MK31",dxm);
		createRoute("MK41",dxm);
		createRoute("MK53",dxm);
		createRoute("MK57",dxm);
		createRoute("MK61",dxm);
		createRoute("MK65",dxm);
		createRoute("MK45",dxm);
		createRoute("MK46",dxm);
		createRoute("MK51",dxm);
		createRoute("MK55",dxm);
		createRoute("MK67",dxm);
		createRoute("MK69",dxm);
		createRoute("MK76",dxm);
		createRoute("MK13",dxm);
		createRoute("MK37",dxm);
		createRoute("MK32",dxm);
		createRoute("MK35",dxm);
		createRoute("MK36",dxm);
		createRoute("MK08",dxm);
		createRoute("MK49",dxm);
		createRoute("MK28",dxm);
		createRoute("MK13",dxm);
		createRoute("MK37",dxm);
		createRoute("MK32",dxm);
		createRoute("MK35",dxm);
		createRoute("MK36",dxm);
		createRoute("MK08",dxm);
		createRoute("MK49",dxm);
		createRoute("MK28",dxm);

		createRoute("SB01",fsb);
		createRoute("SB06",fsb);

		createRoute("ST00",dxs);
		createRoute("ST11",dxs);
		createRoute("ST21",dxs);
		createRoute("ST31",dxs);
		createRoute("ST41",dxs);
		createRoute("ST46",dxs);
		createRoute("ST61",dxs);
		createRoute("ST66",dxs);
		createRoute("ST71",dxs);
		createRoute("ST76",dxs);
		createRoute("ST81",dxs);
		createRoute("STB2",dxs);
		createRoute("STB3",dxs);
		createRoute("STB4",dxs);
		createRoute("STB7",dxs);
		createRoute("STB8",dxs);
		createRoute("STB9",dxs);
		createRoute("STW5",dxs);
		createRoute("STW6",dxs);
		createRoute("STW7",dxs);
		createRoute("STW8",dxs);
		createRoute("STY1",dxs);
	}

	private void createRoute(String routeCode, Facility facility){
		if(routeDoesntExistYet(routeCode, facility)){
			Route route = new Route();
			route.setRouteCode(routeCode);
			route.setAssignedFacility(facility);
			
			routeRepo.save(route);
		}
	}
	
	private boolean routeDoesntExistYet(String routeCode, Facility facility){
		return routeRepo.findByRouteCodeAndFacility(routeCode, facility) == null;
	}
}
