package com.dhl.inventory.bootstrap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.services.security.NSISUserService;

@Component
public class SecurityProfileRoleUpdateLoader implements LoadExecutor {

	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	@Qualifier("nsisUserService")
	NSISUserService nsisUserService;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void execute() {
		updateUser("marlonse", new String[]{"CRPERSONNEL"});
	}
	
	private void updateUser(String username, String [] roleIds){
		if(userExists(username)){
			nsisUserService.updateUser(username, roleIds);
		}
	}

	private boolean userExists(String username){
		return null != userRepo.findByUserName(username);
	}
}
