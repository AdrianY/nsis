package com.dhl.inventory.bootstrap;

public interface LoadExecutor {
	void execute();
}
