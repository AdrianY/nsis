package com.dhl.inventory.bootstrap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.domain.security.NsisRole;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.repository.security.RolesRepository;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.services.security.NSISUserRoleService;
import com.dhl.inventory.services.security.NSISUserService;
import org.springframework.stereotype.Component;

@Component
public class SecurityProfilesLoader implements LoadExecutor {

	private static final Logger LOGGER = Logger.getLogger(SecurityProfilesLoader.class);

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private RolesRepository roleRepo;

	@Autowired
	@Qualifier("nsisUserService")
	NSISUserService nsisUserService;
	
	@Autowired
	@Qualifier("nsisUserRoleService")
	NSISUserRoleService nsisUserRoleService;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void execute() {
		LOGGER.info("ContextRefreshedEvent - bootstrapping user profiles");

		createNewRole("WHPERSONNEL", "Warehouse Personnel");
		createNewRole("SVCPERSONNEL", "Service Center Personnel");
		createNewRole("APPROVER", "Requests Approver");
		createNewRole("CSPERSONNEL", "Customer Service Personnel");
		createNewRole("SPPERSONNEL", "Service Provider Personnel");
		createNewRole("CRPERSONNEL", "Courier Requestor Personnel");

		createNewUser("didelros","didelros","Diana Marie","Del Rosario","diana.delrosario@dhl.com","09173153235", new String[]{"APPROVER", "SVCPERSONNEL","WHPERSONNEL"});
		createNewUser("hdeleon","hdeleon","Hardy","de Leon","hardy.deleon@dhl.com","09173153235", new String[]{"APPROVER", "SVCPERSONNEL","WHPERSONNEL"});
		createNewUser("rocorpuz","rocorpuz","Ronald","Corpuz","Ronald.M.Corpuz@dhl.com","09152935254", new String[]{"WHPERSONNEL"});
		createNewUser("dannyl","dannyl","Danny","Loquere","Danny.Loquere@dhl.com","09178229844", new String[]{"WHPERSONNEL"});
		createNewUser("cobelen","cobelen","Conrad","Belen","Conrad.Belen@dhl.com","09178449547", new String[]{"APPROVER "});
		createNewUser("gcortez","gcortez","Gino","Cortez","Gino.Cortez@dhl.com","09178438252", new String[]{"APPROVER "});
		createNewUser("rbenebe","rbenebe","Ronaldo","Benebe","ronaldo.benebe@dhl.com","09175560799", new String[]{"APPROVER "});
		createNewUser("bmendoza","bmendoza","Benjamin","Mendoza","Benjamin.Mendoza@dhl.com","09276113235", new String[]{"SVCPERSONNEL"});
		createNewUser("jbaculo","jbaculo","Jun ","Baculo","jonardo.baculo@dhl.com","09178268791", new String[]{"APPROVER", "SVCPERSONNEL","WHPERSONNEL"});
		createNewUser("allsanto","allsanto","Allen","Santos","Allen.Santos@dhl.com","09155062215", new String[]{"APPROVER", "SVCPERSONNEL","WHPERSONNEL"});
		createNewUser("gcortez","gcortez","Gino","Cortez","Gino.Cortez@dhl.com","09178438252", new String[]{"APPROVER "});
		createNewUser("rbenebe","rbenebe","Ronaldo","Benebe","ronaldo.benebe@dhl.com","09175560799", new String[]{"APPROVER "});
		createNewUser("dalejand","dalejand","Dawn","Alejandro","Dawn.Alejandrino@dhl.com","07175986028 ", new String[]{"SVCPERSONNEL"});
		createNewUser("jesusb","jesusb","Jesus","Bulaun","Jesus.Bulaun@dhl.com","09175987670", new String[]{"SVCPERSONNEL"});
		createNewUser("bmendoza","bmendoza","Benjamin","Mendoza","Benjamin.Mendoza@dhl.com","09276113235", new String[]{"SVCPERSONNEL"});
		createNewUser("dandiga","dandiga","Danilo","Diga","danilo.diga@dhl.com","09226324729", new String[]{"SVCPERSONNEL"});
		createNewUser("rdizon","rdizon","Ralph ","Dizon","ralph.dizon@dhl.com","09276110478", new String[]{"SVCPERSONNEL"});
		createNewUser("gldiaz","gldiaz","Glenn ","Diaz","glenn.diaz@dhl.com","09178100783", new String[]{"SVCPERSONNEL"});
		createNewUser("bensonit","bensonit","Benjamin ","Sonit","benjamin.sonit.@dhl.com","09175560797", new String[]{"SVCPERSONNEL"});
		createNewUser("cbooc","cbooc","Christian","Booc","christian.booc@dhl.com","09434012804", new String[]{"SVCPERSONNEL"});
		createNewUser("karda","karda","Ken","Arda","ken.arda@dhl.com","09062048852", new String[]{"SVCPERSONNEL"});
		createNewUser("walegres","walegres","Wilardo","Alegres","wilardo.alegres@dhl.com","09175378407", new String[]{"SVCPERSONNEL"});
		createNewUser("adejesus","adejesus","Agapito  F Jr","De Jesus","aga.dejesus@dhl.com","9276110594", new String[]{"SVCPERSONNEL"});
		createNewUser("raydy","raydy","Raymond","Dy","Raymund.Dy@dhl.com","9176584669", new String[]{"SVCPERSONNEL"});
		createNewUser("allansan","allansan","Allan ","Santos","Allan.Santos@dhl.com","9176110666", new String[]{"SVCPERSONNEL"});
		createNewUser("hmoral","hmoral","Harris","Morales","Harris.Morales@dhl.com","9273801461", new String[]{"SVCPERSONNEL"});
		createNewUser("desciba","desciba","Danren","Escriba","danren.escriba@dhl.com","9276110543", new String[]{"SVCPERSONNEL"});
		createNewUser("judeguzm","judeguzm","Justine Ivan","De Guzman","JustineIvan.DeGuzman@dhl.com","9954358855", new String[]{"SVCPERSONNEL"});
		createNewUser("lestcal","lestcal","Lester","Calixtro","Lester.Calixtro@dhl.com","9428040484", new String[]{"SVCPERSONNEL"});
		createNewUser("rallura","rallura","Rex","Alura","Rex.Alura@dhl.com","9178467528", new String[]{"SVCPERSONNEL"});
		createNewUser("rastille","rastille","Renato ","Astillero","Renato.Astillero@dhl.com","9175987540", new String[]{"SVCPERSONNEL"});
		createNewUser("cespora","cespora","Charles ","Espora","Charles.Espora@dhl.com","9778012086", new String[]{"SVCPERSONNEL"});
		createNewUser("gdelacru","gdelacru","Glenn","Dela Cruz","Glenn.delaCruz@dhl.com","9178837062", new String[]{"SVCPERSONNEL"});
		createNewUser("ronncorp","ronncorp","ronnie","Corpuz","ronnie.corpuz@dhl.com","9276110481", new String[]{"SVCPERSONNEL"});
		createNewUser("mdeclaro","mdeclaro","Matias","Declaro","matias.declaro@dhl.com","9273275689", new String[]{"SVCPERSONNEL"});
		createNewUser("arielgon","arielgon","Ariel","Gonzales","ariel.gonzales@dhl.com","9175984997", new String[]{"SVCPERSONNEL"});
		createNewUser("rgarcia","rgarcia","Rael","Garcia","rael.garcia@dhl.com","9178467190", new String[]{"SVCPERSONNEL"});
		createNewUser("rambasto","rambasto","Ramir","Basto","ramir.basto@dhl.com","9167845652", new String[]{"SVCPERSONNEL"});
		createNewUser("marlonse","marlonse","Marlon ","Serrano","marlon.serrano@dhl.com","9173103810", new String[]{"SVCPERSONNEL"});
		createNewUser("rbancil","rbancil","Raniel","Bancil","raniel.bancil@dhl.com","9276110544", new String[]{"SVCPERSONNEL"});
		createNewUser("gerryvic","gerryvic","Gerry ","Victoria","gerry.victoria@dhl.com","9175413973", new String[]{"SVCPERSONNEL"});
		createNewUser("jerdomin","jerdomin","Jerico","Domingo","jerico.domingo@dhl.com","9176584882", new String[]{"SVCPERSONNEL"});
		createNewUser("jpunzala","jpunzala","Joel ","Punzalan","joel.punzalan@dhl.com","9274848356", new String[]{"SVCPERSONNEL"});
		createNewUser("wendelat","wendelat","Wendell","Atinaja","wendel.atinaja@dhl.com","9178278909", new String[]{"SVCPERSONNEL"});
		createNewUser("zaldyzar","zaldyzar","Zaldy","Zarsadias","zaldy.zarsadias@dhl.com","9175987320", new String[]{"SVCPERSONNEL"});
		createNewUser("cminas","cminas","Cris Adam","Mi�as","CrisAdam.Minas@dhl.com","9178467472", new String[]{"SVCPERSONNEL"});
		createNewUser("rgayta","rgayta","Rene ","Gaayta","rene.gayta@dhl.com","9276110516", new String[]{"SVCPERSONNEL"});
		createNewUser("jganotis","jganotis","Joel ","Ganotisi","joel.ganotisi@dhl.com","9175986087", new String[]{"SVCPERSONNEL"});
		createNewUser("cballest","cballest","Christian","Ballester","christian.ballester@dhl.com","9176584901", new String[]{"SVCPERSONNEL"});
		createNewUser("albertgo","albertgo","Albert Valen","Gonzales","albert.gonzales@dhl.com","9178562606", new String[]{"SVCPERSONNEL"});
		createNewUser("gervacio","gervacio","Gervacio","Banal, Jr","gervacio.banaljr@dhl.com","9178438057", new String[]{"SVCPERSONNEL"});
		createNewUser("mmanalas","mmanalas","Mario","Manalastas, Jr","mario.manalastas@dhl.com","9178562736", new String[]{"SVCPERSONNEL"});
		createNewUser("dpomasin","dpomasin","Dennis","Pomasin","dennis.pomasin@dhl.com","09276110432", new String[]{"SVCPERSONNEL"});
		createNewUser("rojacosa","rojacosa","Rodolfo","Jacosalem","rodolfo.jacosalem@dhl.com","09276110473", new String[]{"SVCPERSONNEL"});
		createNewUser("richcoll","richcoll","Richard","Collado","richard.collado@dhl.com","09276110580", new String[]{"SVCPERSONNEL"});
		createNewUser("arinfant","arinfant","Aron","Infante","aronjay.infante@dhl.com","09178465194", new String[]{"SVCPERSONNEL"});
		createNewUser("katabend","katabend","Ma. Katrina ","Abendanio","Katrina.Abendanio@dhl.com","0925 5289462", new String[]{"CSPERSONNEL"});
		createNewUser("billyjab","billyjab","Billy John","Abiad","BillyJohn.Abiad@dhl.com","0925 8663432", new String[]{"CSPERSONNEL"});
		createNewUser("jaabina","jaabina","Jave-Lin ","Abina","Javelin.abina@dhl.com","0916 3157396", new String[]{"CSPERSONNEL"});
		createNewUser("eantig","eantig","Evangeline ","Antig","evangeline.antig@dhl.com","0917 8438014", new String[]{"CSPERSONNEL"});
		createNewUser("aarda","aarda","Angelica ","Arda","angelica.arda@dhl.com","0917 8044011", new String[]{"CSPERSONNEL"});
		createNewUser("searreol","searreol","Sierra","Arreola","Seira.Arreola@dhl.com","0998 5567308", new String[]{"CSPERSONNEL"});
		createNewUser("jegbayog","jegbayog","Jegger","Bayog","jegger.bayo@dhl.com","0927 5755605", new String[]{"CSPERSONNEL"});
		createNewUser("gbenin","gbenin","George Fiel ","Benin","george.benin@dhl.com","0906 3306516", new String[]{"CSPERSONNEL"});
		createNewUser("anbernar","anbernar","Analyn ","Bernardo","analyn.bernardo@dhl.com","0917 5276098", new String[]{"CSPERSONNEL"});
		createNewUser("mborbon","mborbon","Mario ","Borbon","mario.borbon@dhl.com","�0949 1901114", new String[]{"CSPERSONNEL"});
		createNewUser("jborras","jborras","Jessamine ","Borras","jessamine.borras@dhl.com","0947 4749107", new String[]{"CSPERSONNEL"});
		createNewUser("jbracamo","jbracamo"," Jonard ","Bracamonte","jonard.bracamonte@dhl.com","0917 8355639", new String[]{"CSPERSONNEL"});
		createNewUser("maribuma","maribuma","Maricel","Bumanglag","maricel.bumanglag@dhl.com","0917 8078421", new String[]{"CSPERSONNEL"});
		createNewUser("jbunag","jbunag","Josel ","Bunag","josel.bunag@dhl.com","0917 8795636", new String[]{"CSPERSONNEL"});
		createNewUser("rcabus","rcabus"," Ranie","Cabus","ranie.cabus@dhl.com","0935 1498481", new String[]{"CSPERSONNEL"});
		createNewUser("lcaluag","lcaluag","Lorena ","Caluag","lorena.caluag@dhl.com","0917 8078419", new String[]{"CSPERSONNEL"});
		createNewUser("marlcarl","marlcarl"," Marlon","Carlos","marlon.carlos@dhl.com","0908 4957292", new String[]{"CSPERSONNEL"});
		createNewUser("ncolisao","ncolisao","Noel ","Colisao","noel.colisao@dhl.com","0917 8438059", new String[]{"CSPERSONNEL"});
		createNewUser("arrianea","arrianea"," Arriane","Cruz","arraine.alejandro@dhl.com","0917 8588935", new String[]{"CSPERSONNEL"});
		createNewUser("mdequito","mdequito","Maricel ","Deguito","maricel.deguito@dhl.com","0935 2800836", new String[]{"CSPERSONNEL"});
		createNewUser("cdelmundo","cdelmundo","Cherrie","Del Mundo","cherrie.delmundo@dhl.com","0917 6226470 ", new String[]{"CSPERSONNEL"});
		createNewUser("nebio","nebio","Ma. Neil","Ebio","neil.ebio@dhl.com","0917 8562724", new String[]{"CSPERSONNEL"});
		createNewUser("melegado","melegado","Maria Ayla","Elegado","marieayla.elegado@dhl.com","0917 8464543", new String[]{"CSPERSONNEL"});
		createNewUser("djardin","djardin","Dharen","Encanto","dharen.encanto@dhl.com","0917 7437429", new String[]{"CSPERSONNEL"});
		createNewUser("cescarez","cescarez","Celeste","Escarez","celeste.escarez@dhl.com","0929 7925395", new String[]{"CSPERSONNEL"});
		createNewUser("aniefern","aniefern","Anie","Fernando","anie.fernando@dhl.com","0919 8659469", new String[]{"CSPERSONNEL"});
		createNewUser("lourdego","lourdego","Ma. Lourdes","Gonzales","lourdes.gonzales@dhl.com","0998 5565680", new String[]{"CSPERSONNEL"});
		createNewUser("asuncion","asuncion","Ma. Asuncion","Gutierrez","asuncion.gutierrez@dhl.com","0917 8898798", new String[]{"CSPERSONNEL"});
		createNewUser("rbesira","rbesira","Trhia Ann","Hautea","rhia.besira@dhl.com","0917 5374154", new String[]{"CSPERSONNEL"});
		createNewUser("jhautea","jhautea","Rolando Luis","Hautea","rolando.hautea@dhl.com","0917 8938548", new String[]{"CSPERSONNEL"});
		createNewUser("sharhern","sharhern","Sharon","Hernandez","sharon.hernandez@dhl.com","0917 8588928", new String[]{"CSPERSONNEL"});
		createNewUser("beholmes","beholmes","Bernadette","Holmes","bernadette.holmes@dhl.com","0917 8437850", new String[]{"CSPERSONNEL"});
		createNewUser("melodyja","melodyja","Melody","Jayubo","melody.jayubo@dhl.com","0917 8588924", new String[]{"CSPERSONNEL"});
		createNewUser("jeffreyj","jeffreyj","Jeffrey","Jornales","jeffrey.jornales@dhl.com","0927 5360187 ", new String[]{"CSPERSONNEL"});
		createNewUser("djulian","djulian","Dennys","Julian","dennys.julian@dhl.com","0932 4310650", new String[]{"CSPERSONNEL"});
		createNewUser("mlinda","mlinda","Mario ","Linda","mario.linda@dhl.com","0915 9277424", new String[]{"CSPERSONNEL"});
		createNewUser("gelopez","gelopez","Gemarie","Lopez","gemarie.lopez@dhl.com","093 59728444", new String[]{"CSPERSONNEL"});
		createNewUser("victorma","victorma","Victorino","Manaligod","victorino.manaligod@dhl.com","0917 8044007", new String[]{"CSPERSONNEL"});
		createNewUser("jtayag","jtayag","Jenor","Mandigma","jenor.tayag@dhl.com","0917 8810452", new String[]{"CSPERSONNEL"});
		createNewUser("anabelle","anabelle","Anabelle","Marquinez","anabelle.marquinez@dhl.com","0927 3974888", new String[]{"CSPERSONNEL"});
		createNewUser("lplaza","lplaza","Laila","Mendez","laila.mendez@dhl.com","0917 5787673", new String[]{"CSPERSONNEL"});
		createNewUser("lezylmon","lezylmon","Lezyl","Monterona","lezyl.monterona@dhl.com","0942 4708483", new String[]{"CSPERSONNEL"});
		createNewUser("jnaguit","jnaguit","Jean","Naguit","jean.naguit@dhl.co","0917 8438257", new String[]{"CSPERSONNEL"});
		createNewUser("dimples","dimples","Anna Marie","Paas","annamarie.paas@dhl.com","0917 8450663", new String[]{"CSPERSONNEL"});
		createNewUser("riapagka","riapagka","Ria","Pagkalinawan","ria.pagkalinawan@dhl.com","0922 8259284", new String[]{"CSPERSONNEL"});
		createNewUser("cpanaligan","cpanaligan","Crizalyn ","Panaligan","crizalyn.panaligan@dhl.com","0943 5519558", new String[]{"CSPERSONNEL"});
		createNewUser("analypan","analypan","Analyn ","Panis","analyn.panis@dhl.com","0998 5347973", new String[]{"CSPERSONNEL"});
		createNewUser("gparajas","gparajas","Grace","Parajas","grace.parajas@dhl.com","0917 8621384", new String[]{"CSPERSONNEL"});
		createNewUser("maureyes","maureyes","Maureen Lou","Reyes","mauren.reyes@dhl.com","0917 5524423", new String[]{"CSPERSONNEL"});
		createNewUser("aroces","aroces","Anafe","Roces","anafe.roces@dhl.com","0922 8731189", new String[]{"CSPERSONNEL"});
		createNewUser("lromasan","lromasan","Larabel","Romasanta","larabel.romasanta@dhl.com","0927 7516321", new String[]{"CSPERSONNEL"});
		createNewUser("msabile","msabile","Mary Rose","Sabile","maryrose.sabile@dhl.com","0915 6512041", new String[]{"CSPERSONNEL"});
		createNewUser("mcsantos","mcsantos","Ma. Corazon","Santos","mariacorazon@dhl.com","0916 7745671", new String[]{"CSPERSONNEL"});
		createNewUser("msapon","msapon","Mark","Sapon","mark.sapo@dhl.com","0977 4317408", new String[]{"CSPERSONNEL"});
		createNewUser("jsillar","jsillar","Jennifer","Sillar","jennifer.sillar@dhl.com","0917 8562706", new String[]{"CSPERSONNEL"});
		createNewUser("csurio","csurio","Christopher","Surio","christopher.surio@dhl.com","0919 5713422", new String[]{"CSPERSONNEL"});
		createNewUser("gltorres","gltorres","Gloria","Torres","gloria.torres@dhl.com","0917 8117106", new String[]{"CSPERSONNEL"});
		createNewUser("joyceann","joyceann","Joyce Ann","Valdez","joyceanne.valdez@dhl.com","0917 8467333", new String[]{"CSPERSONNEL"});
		createNewUser("jventura","jventura","John Michael","Ventura","johnmichael.ventura@dhl.com","0916 2765255", new String[]{"CSPERSONNEL"});
		createNewUser("jevillar","jevillar","Jenny Belle","Villarin","jennybelle.villarin@dhl.com","0915 9910326", new String[]{"CSPERSONNEL"});
		createNewUser("michaelz","michaelz","Michael","Zenarosa","michael.zenarosa@dhl.com","0917 3664149", new String[]{"CSPERSONNEL"});
		createNewUser("maralejo","maralejo","Ma. Concordia","Alejo","macorcordia@dhl.com","0927 7308880", new String[]{"CSPERSONNEL"});
		createNewUser("dancheta","dancheta","Donato","Ancheta","donato.ancheta@dhl.com","0927 9419998", new String[]{"CSPERSONNEL"});
		createNewUser("ebatayca","ebatayca","Elcid","Bataycan","elcid.bataycan@dhl.com","0977 4988177", new String[]{"CSPERSONNEL"});
		createNewUser("mbuado","mbuado","Mariz","Buado","mariz.buado@dhl.com","0949 4882588", new String[]{"CSPERSONNEL"});
		createNewUser("rclasara","rclasara","Rochelle","Clasara","rochelle.clasara@dhl.com","090 51041312", new String[]{"CSPERSONNEL"});
		createNewUser("cconcono","cconcono","Charles King","Conocono","charlesking.conocono@dhl.com","0919 3136777", new String[]{"CSPERSONNEL"});
		createNewUser("lfaustin","lfaustin","Lean","Faustino","lean.faustino@dhl.com","0929 7440984", new String[]{"CSPERSONNEL"});
		createNewUser("alygutie","alygutie","Alyssa Kariz","Gutierrez","alyssa.gutierrez@dhl.com","0926 4872887", new String[]{"SPPERSONNEL"});
		createNewUser("zhaber","zhaber","Zoraida","Haber","zoraida.haber@dhl.com","093 92075525 ", new String[]{"SPPERSONNEL"});
		createNewUser("jmalapit","jmalapit","Jennelyn","Malapit","jennelyn.malapit@dhl.com","0920 5652777", new String[]{"SPPERSONNEL"});
		createNewUser("pabdilla","pabdilla","PINKY","ABDILLA","ph.greenhillssp@dhl.com","(0932)639-8524", new String[]{"SPPERSONNEL"});
		createNewUser("maacero","maacero","MARK ROSEL","ACERO","ph.metrolanesp@dhl.com","(0923) 402-1084", new String[]{"SPPERSONNEL"});
		createNewUser("aalcantara","aalcantara","ANGELIE","ALCANTARA","PHPacifica.MallSP@dhl.com","(0933)551-3840", new String[]{"SPPERSONNEL"});
		createNewUser("juagusti","juagusti","JUNNEL","AGUSTIN","agustin__junnel@yahoo.com","(0948)242-1903", new String[]{"SPPERSONNEL"});
		createNewUser("naligado","naligado","NIEVA","ALIGADO","PHGateway.Reception@dhl.com","(0930)906-3982", new String[]{"SPPERSONNEL"});
		createNewUser("halumpong","halumpong","HANIELAINE","ALUMPONG","PH.SMBicutanSP@dhl.com","(0917)735-3990", new String[]{"SPPERSONNEL"});
		createNewUser("maribela","maribela","MARIBEL","ATIENZA","PH.LipaSP@dhl.com","(0927) 546-4998", new String[]{"SPPERSONNEL"});
		createNewUser("jbranzuela","jbranzuela","JONEL","BRANZUELA","ph.smmolinosp@dhl.com","(0921) 540-9102", new String[]{"SPPERSONNEL"});
		createNewUser("rcabugay","rcabugay","RICHILDA","CABUGAYAN","PH.LipaSP@dhl.com","(0915)118-8960", new String[]{"SPPERSONNEL"});
		createNewUser("achua","achua","ANA ISABELLE","CHUA","PH.SMSeasideSP@dhl.com","(0932)2452-068", new String[]{"SPPERSONNEL"});
		createNewUser("genriquez","genriquez","GENALYN","ENRIQUEZ","ghen_janirish@yahoo.com","(0927)661-7554", new String[]{"SPPERSONNEL"});
		createNewUser("mespera","mespera","MONETTE","ESPERA","PH.Festival@dhl.com","(0977)342-2669", new String[]{"SPPERSONNEL"});
		createNewUser("afernandez","afernandez","ALDRICO","FERNANDEZ","ph.cybermallsp@dhl.com","(0908) 292-7274", new String[]{"SPPERSONNEL"});
		createNewUser("fherrera","fherrera","FERDIE","HERRERA","PHGateway.Reception@dhl.com","(0918) 632-1320", new String[]{"SPPERSONNEL"});
		createNewUser("alelim","alelim","ALEX","LIM","phsmcebu@yahoo.com","(0916)585-4339", new String[]{"SPPERSONNEL"});
		createNewUser("mlongalo","mlongalo","MARICAR","LONGALONG","PH.RockwellSP@dhl.com","(0916)469-9320", new String[]{"SPPERSONNEL"});
		createNewUser("amorales","amorales","AMADEO","MORALES","PHDXS.Reception@dhl.com","(0915)448-6788", new String[]{"SPPERSONNEL"});
		createNewUser("cmuller","cmuller","CHARLEMAGNE","MULLER","ph.updilimansp@dhl.com ","(0922)379-9895", new String[]{"SPPERSONNEL"});
		createNewUser("jnazario","jnazario","JESSA","NAZARIO","PH.smaurasp@dhl.com","(0918) 200-0955", new String[]{"SPPERSONNEL"});
		createNewUser("rodina","rodina","ROXANE","ODINA","roxaneodina@yahoo.com","(0948)000-5427", new String[]{"SPPERSONNEL"});
		createNewUser("bpelicia","bpelicia","BRYAN","PELICIA","PH.SMMegaSP@dhl.com","(0927)359-3087", new String[]{"SPPERSONNEL"});
		createNewUser("rpeligro","rpeligro","RHEANN","PELIGRO","phcebu.reception@dhl.com","(0922) 552-2975", new String[]{"SPPERSONNEL"});
		createNewUser("dquierrez","dquierrez","DANILO","QUIERREZ","ph.sucatsp@dhl.com","(0949) 339-1340", new String[]{"SPPERSONNEL"});
		createNewUser("aramos","aramos","ARVIN JAY","RAMOS","phsmcebu@yahoo.com","(0932)605-5123", new String[]{"SPPERSONNEL"});
		createNewUser("wrullan","wrullan","WILMA","RULLAN","PH.SMMegaSP@dhl.com","(0917)594-3894", new String[]{"SPPERSONNEL"});
		createNewUser("esanchez","esanchez","EDUARDO","SANCHEZ","PH.HarrisonSP@dhl.com","(0915)125-8000", new String[]{"SPPERSONNEL"});
		createNewUser("madelias","madelias","MADEL","SANCHEZ","madelia.sanchez@dhl.com","(0926)128-2393", new String[]{"SPPERSONNEL"});
		createNewUser("jsevalla","jsevalla","JULLIE ANN","SEVALLA","PH.NorthSP@dhl.com","(0927)747-7587", new String[]{"SPPERSONNEL"});
		createNewUser("msolis","msolis","MA. KRISTINA","SOLIS","phretail.reception@dhl.com","(0915)117-8758", new String[]{"SPPERSONNEL"});
		createNewUser("asubang","asubang","ANA MARIE","SUBANG","mab_alegre@yahoo.com","(0905)921-8283", new String[]{"SPPERSONNEL"});
		createNewUser("btecson","btecson","BLESSILA","TECSON","phretail.reception@dhl.com","(0927) 618-5485", new String[]{"SPPERSONNEL"});
		createNewUser("jenuy","jenuy","JENNIFER","UY","jeniftw@gmail.com","(0927)810-4000", new String[]{"SPPERSONNEL"});
		createNewUser("anvillar","anvillar","ANDREA CLARICE","VILLAREAL","DXO.Reception@dhl.com","(0905) 747-7069", new String[]{"SPPERSONNEL"});
		createNewUser("vwalican","vwalican","VERONICA","WALICAN","PH.SMStaRosaSP@dhl.com","(0916) 572-0848", new String[]{"SPPERSONNEL"});
		createNewUser("paomendo","paomendo","PAOLO","MENDOZA","paolo.mendoza@dhl.com","(0927) 769-3754", new String[]{"SPPERSONNEL"});
		createNewUser("ssison","ssison","SHERLY","SISON","sherly.sison@dhl.com","(0916)670-0602", new String[]{"SPPERSONNEL"});
		createNewUser("marietta","marietta","MARIETTA","MANALO","marietta.manalo@dhl.com","(0917) 843-7870", new String[]{"SPPERSONNEL"});
		createNewUser("flocerpa","flocerpa","FLOCER","CARILLO","flocer.parreno@dhl.com","(0917) 578-7685", new String[]{"SPPERSONNEL"});
		createNewUser("elaineta","elaineta","ELAINE","TAN","elaine.tan@dhl.com","(0917) 843-7825", new String[]{"SPPERSONNEL"});
	}

	private void createNewRole(String codeName, String description){
		NsisRole role = roleRepo.findByCodeName(codeName);
		if(null == role){
			nsisUserRoleService.addNewRole(codeName, description);
		}
	}

	private void createNewUser(String username, String passwordString, String firstName, String lastName, String emailAddress,String contactNumber, String [] roles){

		NsisUser user = userRepo.findByUserName(username);
		if(null == user){
			nsisUserService.addNewUser(username,passwordString,firstName,lastName,emailAddress,contactNumber,roles);
		}
	}
}
