package com.dhl.inventory.bootstrap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.dto.commandobject.maintenance.CustomerAccountCO;
import com.dhl.inventory.repository.maintenance.CustomerAccountRepository;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.services.maintenance.CustomerAccountService;

@Component
public class CustomerAccountLoader implements LoadExecutor {

	@Autowired
	private CustomerAccountRepository customerAccountRepo;
	
	@Autowired
	@Qualifier("customerAccountService")
	private CustomerAccountService customerAccountService;
	
	private void createCustomerAccount(
			String name, 
			String accountNumber, 
			String deliveryAddress, 
			String contactName, 
			String contactNumber, 
			String facilityCode
	){
		if(canLoadCustomerAccount(accountNumber)){
			CustomerAccountCO currencyCommandCommand = new CustomerAccountCO();
			currencyCommandCommand.setAccountNumber(accountNumber);
			currencyCommandCommand.setName(name);
			currencyCommandCommand.setContactNumber(contactNumber);
			currencyCommandCommand.setEmailAddress(contactName);
			currencyCommandCommand.setDeliveryAddress(deliveryAddress);
			currencyCommandCommand.setFacilityCodeName(facilityCode);
			
			try {
				customerAccountService.createCustomerAccount(currencyCommandCommand);
			} catch (ValidationException e) {
				e.printStackTrace();
			}
		}
	}
	
	private boolean canLoadCustomerAccount(String accountNumber){
		return null ==
				customerAccountRepo.findByAccountNumber(accountNumber);
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void execute() {
		createCustomerAccount("LENCORE FAR EAST PHILS AG","642285959","UNIT 2202 JOLLIBEE PLAZA BLDG F ORTIGAS JR ROAD ORTIGAS CENTER","","6368021","DXO");
		createCustomerAccount("128 DREAM FOUNTAIN CORP","966516593","7 AGNO ST BRGY DONA JOSEFA","MR EDWIN NGO/MS SELIA SUAGAO","9994150333","DML");
		createCustomerAccount("128 DREAM FOUNTAIN CORP","642230115","7 AGNO ST BRGY DONA JOSEFA","MR EDWIN NGO/MS SELIA SUAGAO","9994150333","DML");
		createCustomerAccount("1749 DIGITAL GRAPHICS INC","967814755","291-A MAYON CORNER MARIA CLARA ST BRGY SAN ISIDRO LABRADOR","MR JAY PUNZALAN","02-8164534","DML");
		createCustomerAccount("2 WORLD TRADERS","642307576","NO 285 EL GRANDE AVE BF HOMES","MARY GRACE PEREZ","8203394","DXS");
		createCustomerAccount("2 WORLD TRADERS","965533289","NO 285 EL GRANDE AVE BF HOMES","MARY GRACE PEREZ","8203394","DXS");
		createCustomerAccount("2000 MILES PLACEMENT AGENCY","967813608","UNIT 14 DON GRACIANO BLDG 1180 E RODRIGUEZ SR AVENUE","MS ANNIE MELCHOR COLLANTE","639129196","DXO");
		createCustomerAccount("21 CENTURY CORPORATION","642311762","LOT C3-3A CARMELRAY INDUSTRIAL PARK II BRGY. PUNTA, CALAMBA","ELOISA MASICAT","049-502-7767","DXC");
		createCustomerAccount("21 CENTURY CORPORATION","649124583","LOT C3-3A CARMELRAY INDUSTRIAL PARK 11 BRGY PUNTA CALAMBA","ELOISA MASICAT","049-502-7767","DXC");
		createCustomerAccount("21 CENTURY CORPORATION","968014431","LOT C3-3A BLDG.6 CARMELRAY INDUSTRIAL PARK 2,BRGY PUNTA","ELOISA MASICAT","049-502-7767","DXC");
		createCustomerAccount("24 7 CUSTOMER PHILS INVC","968011320","8/F INSULAR LIFE BLDG 6781 AYALA AVE","M JEAN-MARC HAUDCOEUR","02-8262517","DXM");
	}

}
