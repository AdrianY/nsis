package com.dhl.inventory.validationgroups;

public final class OrderRequestValidationGroup {
	private OrderRequestValidationGroup(){}
	
	public static interface Create{}
	
	public static interface Update{}
	
	public static interface Submit{}
	
	public static interface Approve{}
	
	public static interface Reject{}
}
