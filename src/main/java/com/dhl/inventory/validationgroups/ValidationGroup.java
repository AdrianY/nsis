package com.dhl.inventory.validationgroups;

public final class ValidationGroup {
	private ValidationGroup(){}
	
	public static interface Create{}
	
	public static interface Update{}
	
	public static interface Submit{}
	
	public static interface Finalize{}
	
	public static interface Approve{}
	
	public static interface Reject{}
	
	public static interface Validate{}
}
