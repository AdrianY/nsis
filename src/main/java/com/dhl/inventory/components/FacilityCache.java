package com.dhl.inventory.components;

import com.dhl.inventory.domain.maintenance.Facility;

public interface FacilityCache {
	Facility getFacility(String facilityCode);
	
	void add(Facility user);
}
