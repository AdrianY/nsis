package com.dhl.inventory.components;

import com.dhl.inventory.domain.security.NsisUser;

public interface UserCache {
	NsisUser getUser(String userName);
	
	void addUser(NsisUser user);
}
