package com.dhl.inventory.components;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;

@Component
class BatchNumberUtilityBeanImpl implements BatchNumberUtilityBean {

	@Override
	public Map<String, String> analyzeBatchNumber(String batchNumber) throws IllegalArgumentException{
		String[] batchNumberComponents = validateBatchNumber(batchNumber);

		String itemCode = constructItemCode(batchNumberComponents[0]);
		String rackCode = constructRackCode(batchNumberComponents[1]);
		String dateStoredString = constructDateStoredInShortFormat(batchNumberComponents[2]);

		Map<String, String> returnValue = new HashMap<>();
		returnValue.put(BNC_ITEM_CODE, itemCode);
		returnValue.put(BNC_RACK_CODE, rackCode);
		returnValue.put(BNC_DATE_STORED, dateStoredString);
		return returnValue;
	}
	
	/**
	 * TODO malformed analysis should use regular expression
	 * @param batchNumber
	 * @return
	 */
	private String[] validateBatchNumber(String batchNumber) throws IllegalArgumentException{
		if(!Optional.object(batchNumber).hasContent())
			throw new IllegalArgumentException("Batch Number is empty");
		
		String[] batchNumberComponents = batchNumber.split("-");
		if(3 > batchNumberComponents.length)
			throw new IllegalArgumentException("Batch Number is malformed");
		else if(!Optional.object(batchNumberComponents[0]).hasContent())
			throw new IllegalArgumentException("Item code component is malformed");
		else if(!Optional.object(batchNumberComponents[1]).hasContent())
			throw new IllegalArgumentException("Rack number component is malformed");
		else if(!Optional.object(batchNumberComponents[2]).hasContent())
			throw new IllegalArgumentException("Date stored string component is malformed");
		
		return batchNumberComponents;
	}
	
	private String constructItemCode(String itemCodeCoponent){
		StringBuilder itemCodeBldr = new StringBuilder(itemCodeCoponent);
		itemCodeBldr.insert(2, "-");
		itemCodeBldr.insert(5, "-");
		itemCodeBldr.insert(8, "-");
		return itemCodeBldr.toString();
	}

	/**
	 * TODO apply commented code once we go online
	 * @param rackCodeCoponent
	 * @return
	 */
	private String constructRackCode(String rackCodeCoponent){
		StringBuilder rackCodeBldr = new StringBuilder(rackCodeCoponent);
		rackCodeBldr.insert(1, "-");
		rackCodeBldr.insert(4, "-");
		return rackCodeBldr.toString();
	}
	
	private String constructDateStoredInShortFormat(String dateStoredCoponent){
		StringBuilder dateStoredStrBldr = new StringBuilder(dateStoredCoponent);
		dateStoredStrBldr.insert(2, "/");
		dateStoredStrBldr.insert(5, "/");
		return dateStoredStrBldr.toString();
	}

	@Override
	public String constructBatchNumber(String itemCode, String rackCode, Date dateStored) {
		StringBuilder batchNumberBldr = new StringBuilder();
		batchNumberBldr.append(itemCode.replaceAll("-", ""));
		batchNumberBldr.append("-");
		batchNumberBldr.append(rackCode.replaceAll("-", ""));
		batchNumberBldr.append("-");
		batchNumberBldr.append(FormatterUtil.shortDateFormat(dateStored).replaceAll("/", ""));
		return batchNumberBldr.toString();
	}
}
