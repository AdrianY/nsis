package com.dhl.inventory.components;

import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

import org.springframework.stereotype.Component;

import com.dhl.inventory.domain.security.NsisUser;

@Component("userCache")
class UserCacheImpl implements UserCache {
	
	private Map<String, NsisUser> cache;
	
	public UserCacheImpl(){
		cache = new ConcurrentSkipListMap<String, NsisUser>(new Comparator<String>(){
			@Override
			public int compare(String o1, String o2){
				return o1.compareTo(o2);
			}
		});
	}

	@Override
	public NsisUser getUser(String userName) {
		return cache.get(userName);
	}

	@Override
	public void addUser(NsisUser user) {
		cache.put(user.getUsername(), user);
	}

}
