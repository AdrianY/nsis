package com.dhl.inventory.components;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
class JasperReportRegistryImpl implements JasperReportRegistry {
	
	private Map<String, ReportDetails> reportRegistry;
	
	public JasperReportRegistryImpl(){
		reportRegistry = new HashMap<>();
		reportRegistry.put("REQUEST_ISSUANCE_PICK_LIST", generateRequestIssuancePickListForm());
		reportRegistry.put("SVC_REQUEST_ISSUANCE_PICK_LIST", generateServiceCenterRequestIssuancePickListForm());
		reportRegistry.put("REQUEST_ISSUANCE_DELIVERY_RECEIPT", generateRequestIssuanceDeliveryReceiptForm());
		reportRegistry.put("SVC_REQUEST_ISSUANCE_DELIVERY_RECEIPT", generateServiceCenterRequestIssuanceDeliveryReceiptForm());
	}

	@Override
	public ReportDetails getReportDetails(String reportRegistryKey) {
		return reportRegistry.get(reportRegistryKey);
	}

	private ReportDetails generateRequestIssuancePickListForm(){
		ReportDetails returnValue = new ReportDetails("delivery_receipt_pick_list");
		Map<String, ReportDetails> subReports = new HashMap<>();
		subReports.put("PickListSubReport", new ReportDetails("pick_list_items"));
		
		returnValue.setSubReports(subReports);
		return returnValue;
	}
	
	private ReportDetails generateServiceCenterRequestIssuancePickListForm(){
		ReportDetails returnValue = new ReportDetails("svc_delivery_receipt_pick_list");
		Map<String, ReportDetails> subReports = new HashMap<>();
		subReports.put("PickListSubReport", new ReportDetails("pick_list_items"));
		
		returnValue.setSubReports(subReports);
		return returnValue;
	}
	
	private ReportDetails generateRequestIssuanceDeliveryReceiptForm(){
		ReportDetails returnValue = new ReportDetails("delivery_receipt_form");
		Map<String, ReportDetails> subReports = new HashMap<>();
		subReports.put("DeliveryReceiptItemSubReport", new ReportDetails("delivery_receipt_items"));
		
		returnValue.setSubReports(subReports);
		return returnValue;
	}
	
	private ReportDetails generateServiceCenterRequestIssuanceDeliveryReceiptForm(){
		ReportDetails returnValue = new ReportDetails("svc_delivery_receipt_form");
		Map<String, ReportDetails> subReports = new HashMap<>();
		subReports.put("DeliveryReceiptItemSubReport", new ReportDetails("delivery_receipt_items"));
		
		returnValue.setSubReports(subReports);
		return returnValue;
	}
}
