package com.dhl.inventory.components;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.ws.rs.core.StreamingOutput;

import org.springframework.stereotype.Component;

@Component
class FileStreamingOutputFactoryImpl implements FileStreamingOutputFactory {

	@Override
	public StreamingOutput generateFileStreamingOutput(final File file) {
		return new StreamingOutput() {

		    @Override
		    public void write(OutputStream output) throws IOException {
		        FileInputStream input = new FileInputStream(file);
		        try {
		            int bytes;
		            while ((bytes = input.read()) != -1) {
		                output.write(bytes);
		            }
		        } catch (IOException e) {
		            throw new IOException(e);
		        } finally {
		            if (output != null) output.close();
		            if (input != null) input.close();
		        }
		    }

		};
	}

}
