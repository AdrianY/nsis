package com.dhl.inventory.components;

import java.io.File;

import javax.ws.rs.core.StreamingOutput;

public interface FileStreamingOutputFactory {
	StreamingOutput generateFileStreamingOutput(File file);
	
}
