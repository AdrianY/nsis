package com.dhl.inventory.components;

import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

import org.springframework.stereotype.Component;

import com.dhl.inventory.domain.maintenance.Facility;

@Component
class FacilityCacheImpl implements FacilityCache {
	
	private Map<String, Facility> cache;
	
	public FacilityCacheImpl(){
		cache = new ConcurrentSkipListMap<String, Facility>(new Comparator<String>(){
			@Override
			public int compare(String o1, String o2){
				return o1.compareTo(o2);
			}
		});
	}

	@Override
	public Facility getFacility(String facilityCode) {
		return cache.get(facilityCode);
	}

	@Override
	public void add(Facility facility) {
		cache.put(facility.getCodeName(), facility);
	}

}
