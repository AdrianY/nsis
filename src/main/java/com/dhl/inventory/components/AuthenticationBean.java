package com.dhl.inventory.components;

import org.springframework.security.core.Authentication;

public interface AuthenticationBean {
	Authentication getAuthentication();
	
	String getPrincipalName();
	
	String getAuthenticatedUserId();
}
