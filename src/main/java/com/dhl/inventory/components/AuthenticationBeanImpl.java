package com.dhl.inventory.components;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.repository.security.UserRepository;

@Component
public class AuthenticationBeanImpl implements AuthenticationBean{
	
	private static final Logger LOGGER = Logger.getLogger(AuthenticationBeanImpl.class);
	
	@Autowired
	private UserRepository userRepo;

	private Map<String, String> principalToUserIdMap;
	
	@PostConstruct   
	public void init() {
		principalToUserIdMap = new HashMap<String, String>();
	}

	@Override
	public Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	@Override
	public String getAuthenticatedUserId() {
		String principalName = getPrincipalName();
		
		LOGGER.info("getAuthenticatedUserId: principal is - " +principalName);
		
		String userId = principalToUserIdMap.get(principalName);
		if(null == userId || userId.isEmpty()){
			NsisUser user = userRepo.findByUserName(principalName);
			if(null != user){
				userId = user.getId();
				principalToUserIdMap.put(principalName, userId);
			}
		}
		
		return userId;
	}

	@Override
	public String getPrincipalName() {
		return null != getAuthentication() ? getAuthentication().getName() : "SYSTEM";
	}

}
