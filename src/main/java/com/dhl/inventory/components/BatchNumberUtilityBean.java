package com.dhl.inventory.components;

import java.util.Date;
import java.util.Map;

public interface BatchNumberUtilityBean {
	public static final String BNC_ITEM_CODE = "itemCode";
	
	public static final String BNC_RACK_CODE = "rackNumber";
	
	public static final String BNC_DATE_STORED = "dateStored";
	
	Map<String, String> analyzeBatchNumber(String batchNumber) throws IllegalArgumentException;
	
	String constructBatchNumber(String itemCode, String rackCode, Date dateStored);
}
