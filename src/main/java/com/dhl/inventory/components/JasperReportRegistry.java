package com.dhl.inventory.components;

import java.util.Map;

public interface JasperReportRegistry {
	
	ReportDetails getReportDetails(String reportRegistryKey);
	
	public static class ReportDetails{
		private String reportName;
		
		private Map<String, ReportDetails> subReports;
		
		public ReportDetails(String reportName){
			this.reportName = reportName;
		}

		public String getReportName() {
			return reportName;
		}

		public void setReportName(String reportName) {
			this.reportName = reportName;
		}

		public Map<String, ReportDetails> getSubReports() {
			return subReports;
		}

		public void setSubReports(Map<String, ReportDetails> subReports) {
			this.subReports = subReports;
		}
		
		
	}
}
