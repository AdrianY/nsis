package com.dhl.inventory.validator;

import com.dhl.inventory.services.exceptions.ValidationException;

public interface ImplementableValidator<T> {
	void validate(String commandType, T commandObject) throws ValidationException;
}
