package com.dhl.inventory.validator.warehouse.inventorymaintenance;

import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.commandobject.warehouse.inventorymaintenance.DeliveryReceiptCO;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.util.ActionType;
import static com.dhl.inventory.validationgroups.ValidationGroup.*;


import com.dhl.inventory.validator.AbstractCommandHibernateValidator;

@Component("warehouseDeliveryReceiptCOValidator")
public class DeliveryReceiptCOValidator extends AbstractCommandHibernateValidator<DeliveryReceiptCO> {

	@Override
	public void validate(String commandType, DeliveryReceiptCO commandObject) throws ValidationException {
		switch(commandType){
			case ActionType.UPDATE_COMMAND: 
				performGroupValidation(commandObject, Update.class);
				break;
			case ActionType.FINALIZE_COMMAND: 
				performGroupValidation(commandObject, Finalize.class);
				break;
		}
	}
}
