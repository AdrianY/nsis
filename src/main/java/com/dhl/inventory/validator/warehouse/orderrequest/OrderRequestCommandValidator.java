package com.dhl.inventory.validator.warehouse.orderrequest;

import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.commandobject.warehouse.orderrequest.OrderRequestCO;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.util.ActionType;
import static com.dhl.inventory.validationgroups.OrderRequestValidationGroup.*;
import com.dhl.inventory.validator.AbstractCommandHibernateValidator;

@Component
public class OrderRequestCommandValidator extends AbstractCommandHibernateValidator<OrderRequestCO> {

	@Override
	public void validate(String commandType, OrderRequestCO commandObject) throws ValidationException {
		switch(commandType){
			case ActionType.CREATE_COMMAND: 
				performGroupValidation(commandObject, Create.class);
				break;
			case ActionType.UPDATE_COMMAND:
				performGroupValidation(commandObject, Update.class);
				break;
			case ActionType.SUBMIT_COMMAND: 
				performGroupValidation(commandObject, Submit.class);
				break;
			case ActionType.APPROVE_COMMAND:
				performGroupValidation(commandObject, Approve.class);
				break;
			case ActionType.REJECT_COMMAND: 
				performGroupValidation(commandObject, Reject.class);
				break;
		}
	}
	
}
