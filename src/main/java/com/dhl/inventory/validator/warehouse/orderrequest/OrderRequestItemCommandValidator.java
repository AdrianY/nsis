package com.dhl.inventory.validator.warehouse.orderrequest;

import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.commandobject.warehouse.orderrequest.OrderRequestItemResourceCO;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.util.ActionType;
import com.dhl.inventory.validationgroups.OrderRequestItemValidationGroup.Validate;
import com.dhl.inventory.validator.AbstractCommandHibernateValidator;

@Component
public class OrderRequestItemCommandValidator extends AbstractCommandHibernateValidator<OrderRequestItemResourceCO> {

	@Override
	public void validate(String commandType, OrderRequestItemResourceCO commandObject) throws ValidationException {
		switch(commandType){
			case ActionType.VALIDATE_COMMAND: 
				performGroupValidation(commandObject, Validate.class);
				break;
		}
	}

}
