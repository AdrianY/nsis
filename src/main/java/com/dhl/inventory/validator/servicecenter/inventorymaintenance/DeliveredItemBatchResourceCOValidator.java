package com.dhl.inventory.validator.servicecenter.inventorymaintenance;

import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.DeliveredItemBatchGroupResourceCO;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.util.ActionType;
import com.dhl.inventory.validationgroups.ValidationGroup.Validate;
import com.dhl.inventory.validator.AbstractCommandHibernateValidator;

@Component("serviceCenterDeliveredItemBatchResourceCOValidator")
public class DeliveredItemBatchResourceCOValidator extends AbstractCommandHibernateValidator<DeliveredItemBatchGroupResourceCO> {

	@Override
	public void validate(String commandType, DeliveredItemBatchGroupResourceCO commandObject)
			throws ValidationException {
		switch(commandType){
			case ActionType.VALIDATE_COMMAND: 
				performGroupValidation(commandObject, Validate.class);
				break;
		}
	}

}
