package com.dhl.inventory.validator.servicecenter.supplyrequest;

import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest.SupplyRequestItemResourceCO;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.util.ActionType;
import com.dhl.inventory.validationgroups.ValidationGroup.Validate;
import com.dhl.inventory.validator.AbstractCommandHibernateValidator;

@Component
public class SupplyRequestItemResourceCommandValidator extends AbstractCommandHibernateValidator<SupplyRequestItemResourceCO> {

	@Override
	public void validate(String commandType, SupplyRequestItemResourceCO commandObject) throws ValidationException {
		switch(commandType){
			case ActionType.VALIDATE_COMMAND: 
				performGroupValidation(commandObject, Validate.class);
				break;
		}
	}

}
