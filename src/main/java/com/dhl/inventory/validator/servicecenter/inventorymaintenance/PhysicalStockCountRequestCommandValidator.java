package com.dhl.inventory.validator.servicecenter.inventorymaintenance;

import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.PhysicalStockCountRequestCO;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.util.ActionType;
import com.dhl.inventory.validationgroups.ValidationGroup.Approve;
import com.dhl.inventory.validationgroups.ValidationGroup.Create;
import com.dhl.inventory.validationgroups.ValidationGroup.Reject;
import com.dhl.inventory.validationgroups.ValidationGroup.Submit;
import com.dhl.inventory.validationgroups.ValidationGroup.Update;
import com.dhl.inventory.validator.AbstractCommandHibernateValidator;

@Component("serviceCenterPhysicalStockCountRequestCommandValidator")
public class PhysicalStockCountRequestCommandValidator extends AbstractCommandHibernateValidator<PhysicalStockCountRequestCO> {

	@Override
	public void validate(String commandType, PhysicalStockCountRequestCO commandObject) throws ValidationException {
		switch(commandType){
			case ActionType.CREATE_COMMAND: 
				performGroupValidation(commandObject, Create.class);
				break;
			case ActionType.UPDATE_COMMAND:
				performGroupValidation(commandObject, Update.class);
				break;
			case ActionType.SUBMIT_COMMAND: 
				performGroupValidation(commandObject, Submit.class);
				break;
			case ActionType.APPROVE_COMMAND:
				performGroupValidation(commandObject, Approve.class);
				break;
			case ActionType.REJECT_COMMAND: 
				performGroupValidation(commandObject, Reject.class);
				break;
		}
	}

}
