package com.dhl.inventory.validator.servicecenter.requestissuance;

import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.commandobject.servicecenter.requestissuance.RequestIssuanceCO;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.util.ActionType;
import static com.dhl.inventory.validationgroups.ValidationGroup.*;
import com.dhl.inventory.validator.AbstractCommandHibernateValidator;

@Component("serviceCenterRequestIssuanceCommandValidator")
public class RequestIssuanceCommandValidator extends AbstractCommandHibernateValidator<RequestIssuanceCO> {

	@Override
	public void validate(String commandType, RequestIssuanceCO commandObject) throws ValidationException {
		switch(commandType){
			case ActionType.CREATE_COMMAND: 
				performGroupValidation(commandObject, Create.class);
				break;
			case ActionType.UPDATE_COMMAND:
				performGroupValidation(commandObject, Update.class);
				break;
			case ActionType.SUBMIT_COMMAND: 
				performGroupValidation(commandObject, Submit.class);
				break;
			case ActionType.FINALIZE_COMMAND: 
				performGroupValidation(commandObject, Finalize.class);
				break;
			case ActionType.REJECT_COMMAND: 
				performGroupValidation(commandObject, Reject.class);
				break;
		}
	}

}
