package com.dhl.inventory.validator;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dhl.inventory.services.exceptions.ValidationException;

public abstract class AbstractCommandHibernateValidator<T> implements ImplementableValidator<T> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractCommandHibernateValidator.class);
	
	protected ValidatorFactory getDefaultValidatorFactory(){
		return Validation.buildDefaultValidatorFactory();
	}
	
	protected void wrapConstraintViolations(Set<ConstraintViolation<T>> constraintViolations) throws ValidationException{
		if(!constraintViolations.isEmpty()){
			Map<String, String> errMessages = new HashMap<String, String>();
			constraintViolations.forEach(s -> {
				LOGGER.error(s.getPropertyPath().toString() + ": "+s.getMessage());
				errMessages.put(s.getPropertyPath().toString(), s.getMessage());
			});
			throw new ValidationException(errMessages);
		}
	}
	
	protected void performGroupValidation(T commandObject, Class<?> groupInterfaceClass) throws ValidationException{
		Validator validator = getDefaultValidatorFactory().getValidator();
		Set<ConstraintViolation<T>> constraintViolations =
		         validator.validate(commandObject, groupInterfaceClass);
		wrapConstraintViolations(constraintViolations);
	}
}
