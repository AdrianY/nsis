package com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance;

import java.util.ArrayList;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;

import com.dhl.inventory.validationgroups.ValidationGroup.Validate;

public class CalculateDeliveredItemBatchGroupResourceCO {
	@NotBlank(groups = Validate.class)
	private String itemCode;
	
	@Valid
	private ArrayList<DeliveredItemBatchCO> otherItemBatches;
	
	public CalculateDeliveredItemBatchGroupResourceCO(){}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public ArrayList<DeliveredItemBatchCO> getOtherItemBatches() {
		return otherItemBatches;
	}

	public void setOtherItemBatches(ArrayList<DeliveredItemBatchCO> otherItemBatches) {
		this.otherItemBatches = otherItemBatches;
	}
}
