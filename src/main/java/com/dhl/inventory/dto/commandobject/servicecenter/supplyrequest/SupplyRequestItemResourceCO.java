package com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import static com.dhl.inventory.validationgroups.ValidationGroup.*;

public class SupplyRequestItemResourceCO {
	@NotBlank(groups = Validate.class)
	private String facilityCodeName;
	
	@NotNull(groups = Validate.class)
	@Valid
	private SupplyRequestItemCO details;
	
	public SupplyRequestItemResourceCO(){}

	public String getFacilityCodeName() {
		return facilityCodeName;
	}

	public void setFacilityCodeName(String facilityCodeName) {
		this.facilityCodeName = facilityCodeName;
	}

	public SupplyRequestItemCO getDetails() {
		return details;
	}

	public void setDetails(SupplyRequestItemCO details) {
		this.details = details;
	}
}
