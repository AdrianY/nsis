package com.dhl.inventory.dto.commandobject;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import static com.dhl.inventory.validationgroups.ValidationGroup.*;

public class RecipientCO {
	@NotBlank(groups = {Submit.class, Approve.class})
	@Length(max=500,groups = {Create.class, Update.class, Submit.class, Approve.class})
	private String deliveryAddress;
	
	@NotBlank(groups = {Submit.class, Approve.class})
	@Length(max=150,groups = {Create.class, Update.class, Submit.class, Approve.class})
	private String username;
	
	@NotBlank(groups = {Submit.class, Approve.class})
	@Length(max=20,groups = {Create.class, Update.class, Submit.class, Approve.class})
	private String contactNumber;
	
	@NotBlank(groups = {Submit.class, Approve.class})
	@Email(groups = {Create.class, Update.class, Submit.class, Approve.class})
	@Length(max=100,groups = {Create.class, Update.class, Submit.class, Approve.class})
	private String emailAddress;
	
	public RecipientCO(){}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
}
