package com.dhl.inventory.dto.commandobject.customerinterfaces;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.dhl.inventory.validationgroups.ValidationGroup.Submit;
import com.dhl.inventory.validationgroups.ValidationGroup.Update;

public class CustomerCO {
	
	@Length(max=32,groups = {Update.class, Submit.class})
	@NotBlank(groups = {Submit.class})
	private String accountNumber;
	
	@Length(max=500,groups = {Update.class, Submit.class})
	@NotBlank(groups = {Submit.class})
	private String deliveryAddress;
	
	@Length(max=100,groups = {Update.class, Submit.class})
	private String contactName;
	
	@Length(max=32,groups = {Update.class, Submit.class})
	private String contactNumber;
	
	@Length(max=200,groups = {Update.class, Submit.class})
	@NotBlank(groups = {Submit.class})
	private String customerName;
	
	public CustomerCO(){}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the deliveryAddress
	 */
	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	/**
	 * @param deliveryAddress the deliveryAddress to set
	 */
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	/**
	 * @return the emailAddress
	 */
	public String getContactName() {
		return contactName;
	}

	/**
	 * @param contactName the emailAddress to set
	 */
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	/**
	 * @return the contactNumber
	 */
	public String getContactNumber() {
		return contactNumber;
	}

	/**
	 * @param contactNumber the contactNumber to set
	 */
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	
}
