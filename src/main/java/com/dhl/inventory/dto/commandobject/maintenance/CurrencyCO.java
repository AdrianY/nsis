package com.dhl.inventory.dto.commandobject.maintenance;

public class CurrencyCO {
	private String code;
	
	private String exchangeRateToPhp;
	
	public CurrencyCO(){
		
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getExchangeRateToPhp() {
		return exchangeRateToPhp;
	}

	public void setExchangeRateToPhp(String exchangeRateToPhp) {
		this.exchangeRateToPhp = exchangeRateToPhp;
	}
	
	
}
