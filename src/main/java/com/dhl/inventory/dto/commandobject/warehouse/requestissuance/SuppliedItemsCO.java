package com.dhl.inventory.dto.commandobject.warehouse.requestissuance;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import static com.dhl.inventory.validationgroups.ValidationGroup.*;

import javax.validation.constraints.Min;

public class SuppliedItemsCO {
	
	@NotBlank(groups = {Validate.class, Update.class, Submit.class, Finalize.class})
	@Length(max=32,groups = {Validate.class,Update.class, Submit.class, Finalize.class})
	private String sourceBatchNumber;
	
	@Min(value=0,groups = {Validate.class,Update.class, Submit.class, Validate.class, Finalize.class})
	private int previousSuppliedQuantity;
	
	@Min(value=0,groups = {Validate.class,Update.class, Submit.class, Validate.class, Finalize.class})
	private int suppliedQuantity;
	
	@Length(max=200,groups = {Validate.class,Update.class, Submit.class, Validate.class, Finalize.class})
	private String remarks;
	
	public SuppliedItemsCO(){}

	public String getSourceBatchNumber() {
		return sourceBatchNumber;
	}

	public void setSourceBatchNumber(String sourceBatchNumber) {
		this.sourceBatchNumber = sourceBatchNumber;
	}

	public int getSuppliedQuantity() {
		return suppliedQuantity;
	}

	public void setSuppliedQuantity(int suppliedQuantity) {
		this.suppliedQuantity = suppliedQuantity;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the previousSuppliedQuantity
	 */
	public int getPreviousSuppliedQuantity() {
		return previousSuppliedQuantity;
	}

	/**
	 * @param previousSuppliedQuantity the previousSuppliedQuantity to set
	 */
	public void setPreviousSuppliedQuantity(int previousSuppliedQuantity) {
		this.previousSuppliedQuantity = previousSuppliedQuantity;
	}
	
	
}
