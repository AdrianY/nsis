package com.dhl.inventory.dto.commandobject.servicecenter.requestissuance;

import java.util.ArrayList;

import javax.validation.Valid;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.dhl.inventory.validationgroups.ValidationGroup.Finalize;
import com.dhl.inventory.validationgroups.ValidationGroup.Submit;
import com.dhl.inventory.validationgroups.ValidationGroup.Update;

public class SuppliedItemsGroupCO {
	@NotBlank(groups = {Update.class, Submit.class, Finalize.class})
	@Length(max=32,groups = {Update.class, Submit.class, Finalize.class})
	private String itemCode;
	
	@Valid
	private ArrayList<SuppliedItemsCO> suppliedItems;
	
	public SuppliedItemsGroupCO(){}

	/**
	 * @return the itemCode
	 */
	public String getItemCode() {
		return itemCode;
	}

	/**
	 * @param itemCode the itemCode to set
	 */
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	/**
	 * @return the suppliedItems
	 */
	public ArrayList<SuppliedItemsCO> getSuppliedItems() {
		return suppliedItems;
	}

	/**
	 * @param suppliedItems the suppliedItems to set
	 */
	public void setSuppliedItems(ArrayList<SuppliedItemsCO> suppliedItems) {
		this.suppliedItems = suppliedItems;
	}
}
