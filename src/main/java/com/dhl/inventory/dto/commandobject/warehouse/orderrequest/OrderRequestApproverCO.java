package com.dhl.inventory.dto.commandobject.warehouse.orderrequest;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.dhl.inventory.validationgroups.OrderRequestValidationGroup.Approve;
import com.dhl.inventory.validationgroups.OrderRequestValidationGroup.Reject;

public class OrderRequestApproverCO {

	@Length(max=150,groups = {Approve.class, Reject.class})
	private String username;
	
	@NotBlank(groups = {Reject.class}, message="Provide explanation for rejecting this OR")
	@Length(max=500,groups = {Approve.class, Reject.class})
	private String remarks;
	
	public OrderRequestApproverCO(){}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}
