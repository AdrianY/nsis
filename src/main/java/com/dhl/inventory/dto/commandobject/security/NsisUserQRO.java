package com.dhl.inventory.dto.commandobject.security;

import java.util.List;
import java.util.Map;

public class NsisUserQRO {
	private String lastName;
	private String firstName;
	private String username;
	private String passwordRawText;
	private List<NsisUserRoleQRO> roles;
	List<Map<String, String>> facilityAssignments;
	
	public NsisUserQRO(){}
	
	public NsisUserQRO setLastName(String lastName){
		this.lastName = lastName;
		return this;
	}
	public NsisUserQRO setFirstName(String firstName){
		this.firstName = firstName;
		return this;
	}
	public NsisUserQRO setUsername(String username){
		this.username = username;
		return this;
	}
	public NsisUserQRO setRoles(List<NsisUserRoleQRO> roles){
		this.roles = roles;
		return this;
	}
	public NsisUserQRO setPassword(String passwordRawText){
		this.passwordRawText = passwordRawText;
		return this;
	}
	
	public List<Map<String, String>> getFacilityAssignments() {
		return facilityAssignments;
	}

	public NsisUserQRO setFacilityAssignments(List<Map<String, String>> facilityAssignments) {
		this.facilityAssignments = facilityAssignments;
		return this;
	}

	public String getLastName() {
		return lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getUsername() {
		return username;
	}
	
	public String getPassword() {
		return passwordRawText;
	}

	public List<NsisUserRoleQRO> getRoles() {
		return roles;
	}
	
}
