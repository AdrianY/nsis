package com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance;

import java.util.ArrayList;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.dhl.inventory.validationgroups.ValidationGroup.Approve;
import com.dhl.inventory.validationgroups.ValidationGroup.Reject;
import com.dhl.inventory.validationgroups.ValidationGroup.Submit;

public class PhysicalStockCountRequestApprovalCO {
	@Length(max=150,groups = {Approve.class, Reject.class})
	private String username;
	
	@NotBlank(groups = {Reject.class}, message="Provide explanation for rejecting this request")
	@Length(max=500,groups = {Approve.class, Reject.class})
	private String remarks;
	
	@NotNull(groups = {Submit.class, Approve.class})
	@NotEmpty(groups = {Submit.class, Approve.class})
	@Valid
	private ArrayList<String> approvedBatchNumberCountList;
	
	public PhysicalStockCountRequestApprovalCO(){}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the approvedBatchNumberCountList
	 */
	public ArrayList<String> getApprovedBatchNumberCountList() {
		return approvedBatchNumberCountList;
	}

	/**
	 * @param approvedBatchNumberCountList the approvedBatchNumberCountList to set
	 */
	public void setApprovedBatchNumberCountList(ArrayList<String> approvedBatchNumberCountList) {
		this.approvedBatchNumberCountList = approvedBatchNumberCountList;
	}
	
	
}
