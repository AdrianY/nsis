package com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance;

import java.util.ArrayList;

import javax.validation.Valid;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.dhl.inventory.validationgroups.ValidationGroup.Finalize;
import com.dhl.inventory.validationgroups.ValidationGroup.Update;

public class DeliveryReceiptCO {
	@NotBlank(groups = {Update.class, Finalize.class})
	@Length(max=32,groups = {Update.class, Finalize.class})
	private String requestNumber;
	
	@NotBlank(groups = Finalize.class)
	private String dateDelivered;
	
	@NotEmpty(groups = Finalize.class)
	@Valid
	private ArrayList<DeliveredItemBatchGroupCO> itemBatchGroups;
	
	public DeliveryReceiptCO(){}
	
	public DeliveryReceiptCO(String requestNumber){this.requestNumber = requestNumber;};

	public String getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	public String getDateDelivered() {
		return dateDelivered;
	}

	public void setDateDelivered(String dateDelivered) {
		this.dateDelivered = dateDelivered;
	}

	public ArrayList<DeliveredItemBatchGroupCO> getItemBatchGroups() {
		return itemBatchGroups;
	}

	public void setItemBatchGroups(ArrayList<DeliveredItemBatchGroupCO> itemBatchGroups) {
		this.itemBatchGroups = itemBatchGroups;
	}

}
