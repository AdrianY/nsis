package com.dhl.inventory.dto.commandobject.warehouse.inventorymaintenance;

import java.util.ArrayList;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.dhl.inventory.validationgroups.ValidationGroup.Validate;

public class DeliveredItemBatchGroupResourceCO {
	@NotBlank(groups = Validate.class)
	private String itemCode;
	
	@NotBlank(groups = Validate.class)
	private String orderNumber;
	
	@NotNull(groups = Validate.class)
	@Valid
	private DeliveredItemBatchCO editedItemBatch;
	
	@Valid
	private ArrayList<DeliveredItemBatchCO> otherItemBatches;
	
	public DeliveredItemBatchGroupResourceCO(){}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public DeliveredItemBatchCO getEditedItemBatch() {
		return editedItemBatch;
	}

	public void setEditedItemBatch(DeliveredItemBatchCO editedItemBatch) {
		this.editedItemBatch = editedItemBatch;
	}

	public ArrayList<DeliveredItemBatchCO> getOtherItemBatches() {
		return otherItemBatches;
	}

	public void setOtherItemBatches(ArrayList<DeliveredItemBatchCO> otherItemBatches) {
		this.otherItemBatches = otherItemBatches;
	}
}
