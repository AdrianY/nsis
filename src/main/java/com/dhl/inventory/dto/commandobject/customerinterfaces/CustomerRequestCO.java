package com.dhl.inventory.dto.commandobject.customerinterfaces;

import java.util.ArrayList;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.dhl.inventory.validationgroups.ValidationGroup.Create;

import static com.dhl.inventory.validationgroups.ValidationGroup.*;

public class CustomerRequestCO {
	
	@NotBlank(groups = {Create.class, Update.class, Submit.class})
	@Length(max=32,groups = {Create.class, Update.class, Submit.class})
	private String requestNumber;
	
	@NotBlank(groups = {Create.class, Update.class, Submit.class})
	@Length(max=3,groups = {Create.class, Update.class, Submit.class})
	private String facilityCode;
	
	@NotNull(groups = {Submit.class})
	@Valid
	private CustomerCO customer;
	
	@NotBlank(groups = {Submit.class})
	private String requiredDate;
	
	@NotBlank(groups = {Create.class})
	private String status;
	
	@NotNull(groups = {Submit.class})
	@NotEmpty(groups = {Submit.class})
	@Valid
	private ArrayList<RequestedItemCO> requestedItems;

	/**
	 * @return the requestNumber
	 */
	public String getRequestNumber() {
		return requestNumber;
	}

	/**
	 * @param requestNumber the requestNumber to set
	 */
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * @return the facilityCode
	 */
	public String getFacilityCode() {
		return facilityCode;
	}

	/**
	 * @param facilityCode the facilityCode to set
	 */
	public void setFacilityCode(String facilityCode) {
		this.facilityCode = facilityCode;
	}

	

	/**
	 * @return the customer
	 */
	public CustomerCO getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(CustomerCO customer) {
		this.customer = customer;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the requiredDate
	 */
	public String getRequiredDate() {
		return requiredDate;
	}

	/**
	 * @param requiredDate the requiredDate to set
	 */
	public void setRequiredDate(String requiredDate) {
		this.requiredDate = requiredDate;
	}

	/**
	 * @return the requestedItems
	 */
	public ArrayList<RequestedItemCO> getRequestedItems() {
		return requestedItems;
	}

	/**
	 * @param requestedItems the requestedItems to set
	 */
	public void setRequestedItems(ArrayList<RequestedItemCO> requestedItems) {
		this.requestedItems = requestedItems;
	}
	
}
