package com.dhl.inventory.dto.commandobject.warehouse.inventorymaintenance;

import java.util.ArrayList;

import javax.validation.Valid;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import static com.dhl.inventory.validationgroups.ValidationGroup.*;

public class DeliveryReceiptCO {
	@NotBlank(groups = {Update.class, Finalize.class})
	@Length(max=32,groups = {Update.class, Finalize.class})
	private String orderNumber;
	
	@NotBlank(groups = Finalize.class)
	private String dateDelivered;
	
	@NotEmpty(groups = Finalize.class)
	@Valid
	private ArrayList<DeliveredItemBatchGroupCO> itemBatchGroups;
	
	public DeliveryReceiptCO(){}
	
	public DeliveryReceiptCO(String orderNumber){this.orderNumber = orderNumber;};

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getDateDelivered() {
		return dateDelivered;
	}

	public void setDateDelivered(String dateDelivered) {
		this.dateDelivered = dateDelivered;
	}

	public ArrayList<DeliveredItemBatchGroupCO> getItemBatchGroups() {
		return itemBatchGroups;
	}

	public void setItemBatchGroups(ArrayList<DeliveredItemBatchGroupCO> itemBatchGroups) {
		this.itemBatchGroups = itemBatchGroups;
	}
}
