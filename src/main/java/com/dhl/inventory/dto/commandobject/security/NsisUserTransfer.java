package com.dhl.inventory.dto.commandobject.security;

import java.util.List;

public class NsisUserTransfer {
	private String lastName;
	private String firstName;
	private String username;
	private String passwordRawText;
	private List<NsisUserRoleTransfer> roles;
	
	public NsisUserTransfer(){}
	
	public NsisUserTransfer setLastName(String lastName){
		this.lastName = lastName;
		return this;
	}
	public NsisUserTransfer setFirstName(String firstName){
		this.firstName = firstName;
		return this;
	}
	public NsisUserTransfer setUsername(String username){
		this.username = username;
		return this;
	}
	public NsisUserTransfer setRoles(List<NsisUserRoleTransfer> roles){
		this.roles = roles;
		return this;
	}
	public NsisUserTransfer setPassword(String passwordRawText){
		this.passwordRawText = passwordRawText;
		return this;
	}

	public String getLastName() {
		return lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getUsername() {
		return username;
	}
	
	public String getPassword() {
		return passwordRawText;
	}

	public List<NsisUserRoleTransfer> getRoles() {
		return roles;
	}
	
}
