package com.dhl.inventory.dto.commandobject.customerinterfaces;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.dhl.inventory.validationgroups.ValidationGroup.Validate;

public class CustomerRequestedItemResourceCO {
	
	@NotBlank(groups = Validate.class)
	private String facilityCode;
	
	@Valid
	@NotNull(groups = Validate.class)
	private RequestedItemCO details;
	
	public CustomerRequestedItemResourceCO(){}

	/**
	 * @return the facilityCode
	 */
	public String getFacilityCode() {
		return facilityCode;
	}

	/**
	 * @param facilityCode the facilityCode to set
	 */
	public void setFacilityCode(String facilityCode) {
		this.facilityCode = facilityCode;
	}

	/**
	 * @return the details
	 */
	public RequestedItemCO getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(RequestedItemCO details) {
		this.details = details;
	}
}
