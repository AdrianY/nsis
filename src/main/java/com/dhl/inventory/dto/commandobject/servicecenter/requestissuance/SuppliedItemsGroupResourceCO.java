package com.dhl.inventory.dto.commandobject.servicecenter.requestissuance;

import java.util.ArrayList;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;

import com.dhl.inventory.dto.commandobject.servicecenter.requestissuance.SuppliedItemsCO;
import com.dhl.inventory.validationgroups.ValidationGroup.Validate;

public class SuppliedItemsGroupResourceCO {
	@NotBlank(groups = Validate.class)
	private String itemCode;
	
	@Valid
	@NotBlank(groups = Validate.class)
	private SuppliedItemsCO toBeValidatedSupplyItem;
	
	@Valid
	private ArrayList<SuppliedItemsCO> suppliedItems;
	
	public SuppliedItemsGroupResourceCO(){}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public ArrayList<SuppliedItemsCO> getSuppliedItems() {
		return suppliedItems;
	}

	public void setSuppliedItems(ArrayList<SuppliedItemsCO> suppliedItems) {
		this.suppliedItems = suppliedItems;
	}

	public SuppliedItemsCO getToBeValidatedSupplyItem() {
		return toBeValidatedSupplyItem;
	}

	public void setToBeValidatedSupplyItem(SuppliedItemsCO toBeValidatedSupplyItem) {
		this.toBeValidatedSupplyItem = toBeValidatedSupplyItem;
	}
}
