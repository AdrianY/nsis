package com.dhl.inventory.dto.commandobject.warehouse.orderrequest;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.dhl.inventory.validationgroups.OrderRequestItemValidationGroup.Validate;
import com.dhl.inventory.validationgroups.OrderRequestValidationGroup.Approve;
import com.dhl.inventory.validationgroups.OrderRequestValidationGroup.Create;
import com.dhl.inventory.validationgroups.OrderRequestValidationGroup.Submit;
import com.dhl.inventory.validationgroups.OrderRequestValidationGroup.Update;

public class OrderRequestItemCO {
	
	@NotBlank(groups = {Create.class, Update.class, Submit.class, Validate.class, Approve.class})
	@Length(max=50,groups = {Create.class, Update.class, Submit.class, Validate.class, Approve.class})
	private String itemCode;
	
	@Min(value=1,groups = {Create.class, Update.class, Submit.class, Validate.class, Approve.class})
	private int quantity;
	
	@Length(max=200,groups = {Create.class, Update.class, Submit.class, Validate.class, Approve.class})
	private String remarks;
	
	public OrderRequestItemCO(){}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}
