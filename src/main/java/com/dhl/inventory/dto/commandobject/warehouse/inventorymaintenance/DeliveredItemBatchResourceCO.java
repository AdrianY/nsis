package com.dhl.inventory.dto.commandobject.warehouse.inventorymaintenance;

import javax.validation.Valid;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.dhl.inventory.validationgroups.ValidationGroup.Validate;

public class DeliveredItemBatchResourceCO {
	
	@NotBlank(groups = Validate.class)
	private String orderNumber;
	
	@NotBlank(groups = Validate.class)
	@Length(max=50, groups = Validate.class)
	private String itemCode;
	
	@NotBlank(groups = Validate.class)
	@Valid
	private DeliveredItemBatchCO details;
	
	public DeliveredItemBatchResourceCO(){}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public DeliveredItemBatchCO getDetails() {
		return details;
	}

	public void setDetails(DeliveredItemBatchCO details) {
		this.details = details;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
}
