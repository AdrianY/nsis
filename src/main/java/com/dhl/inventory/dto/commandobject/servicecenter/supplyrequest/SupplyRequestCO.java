package com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest;

import java.util.ArrayList;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.dhl.inventory.dto.commandobject.RecipientCO;

import static com.dhl.inventory.validationgroups.ValidationGroup.*;

public class SupplyRequestCO {
	
	@NotBlank(groups = {Create.class, Update.class, Submit.class, Approve.class, Reject.class})
	@Length(max=32,groups = {Create.class, Update.class, Submit.class, Approve.class, Reject.class})
	private String requestNumber;
	
	@NotBlank(groups = {Create.class, Update.class, Submit.class, Approve.class})
	private String facilityCodeName;

	@NotBlank(groups = {Submit.class})
	@Length(max=150,groups = {Create.class, Update.class, Submit.class, Approve.class})
	private String requestByUsername;
	
	@NotBlank(groups = {Create.class, Update.class, Submit.class, Approve.class})
	@Length(max=3,groups = {Create.class, Update.class, Submit.class, Approve.class})
	private String currency;
	
	private String requestDate;
	
	@NotBlank(groups = {Submit.class})
	@Length(max=5,groups = {Create.class, Update.class, Submit.class, Approve.class})
	private String requestCycle;
	
	@NotBlank(groups = {Submit.class, Approve.class})
	private String issuanceDate;
	
	@NotBlank(groups = {Create.class, Update.class, Submit.class, Approve.class})
	private String status;
	
	@NotNull(groups = {Submit.class, Approve.class})
	@Valid
	private RecipientCO recipient;
	
	@NotNull(groups = {Approve.class, Reject.class})
	@Valid
	private SupplyRequestApproverCO approver;
	
	@NotNull(groups = {Submit.class, Approve.class})
	@NotEmpty(groups = {Submit.class, Approve.class})
	@Valid
	private ArrayList<SupplyRequestItemCO> itemsRequested;
	
	public SupplyRequestCO(){}

	public String getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	public String getFacilityCodeName() {
		return facilityCodeName;
	}

	public void setFacilityCodeName(String facilityCodeName) {
		this.facilityCodeName = facilityCodeName;
	}

	public String getRequestByUsername() {
		return requestByUsername;
	}

	public void setRequestByUsername(String requestByUsername) {
		this.requestByUsername = requestByUsername;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getIssuanceDate() {
		return issuanceDate;
	}

	public void setIssuanceDate(String issuanceDate) {
		this.issuanceDate = issuanceDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public RecipientCO getRecipient() {
		return recipient;
	}

	public void setRecipient(RecipientCO recipient) {
		this.recipient = recipient;
	}

	public SupplyRequestApproverCO getApprover() {
		return approver;
	}

	public void setApprover(SupplyRequestApproverCO approver) {
		this.approver = approver;
	}

	public ArrayList<SupplyRequestItemCO> getItemsRequested() {
		return itemsRequested;
	}

	public void setItemsRequested(ArrayList<SupplyRequestItemCO> itemsRequested) {
		this.itemsRequested = itemsRequested;
	}

	public String getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}

	/**
	 * @return the requestCycle
	 */
	public String getRequestCycle() {
		return requestCycle;
	}

	/**
	 * @param requestCycle the requestCycle to set
	 */
	public void setRequestCycle(String requestCycle) {
		this.requestCycle = requestCycle;
	}
}
