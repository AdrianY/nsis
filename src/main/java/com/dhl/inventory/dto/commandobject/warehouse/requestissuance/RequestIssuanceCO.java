 package com.dhl.inventory.dto.commandobject.warehouse.requestissuance;

import java.util.ArrayList;

import javax.validation.Valid;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import static com.dhl.inventory.validationgroups.ValidationGroup.*;

public class RequestIssuanceCO {
	
	@NotBlank(groups = {Create.class, Update.class, Submit.class, Finalize.class, Reject.class})
	@Length(max=32,groups = {Update.class, Submit.class, Finalize.class, Reject.class})
	private String requestNumber;
	
	@NotBlank(groups = {Finalize.class})
	private String issuanceDate;
	
	@NotBlank(groups = {Update.class, Submit.class, Finalize.class, Reject.class})
	@Length(max=32,groups = {Update.class, Submit.class, Finalize.class, Reject.class})
	private String custodianUsername;
	
	@NotBlank(groups = {Reject.class})
	@Length(max=500,groups = {Update.class, Submit.class,Finalize.class, Reject.class})
	private String custodianRemarks;
	
	@NotEmpty(groups = {Update.class, Submit.class, Finalize.class})
	@Valid
	private ArrayList<SuppliedItemsGroupCO> suppliedItemsGroups;
	
	public RequestIssuanceCO(){}
	
	public RequestIssuanceCO(String requestNumber){this.requestNumber = requestNumber;}

	public String getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	public String getCustodianUsername() {
		return custodianUsername;
	}

	public void setCustodianUsername(String custodianUsername) {
		this.custodianUsername = custodianUsername;
	}

	public String getCustodianRemarks() {
		return custodianRemarks;
	}

	public void setCustodianRemarks(String custodianRemarks) {
		this.custodianRemarks = custodianRemarks;
	}

	public ArrayList<SuppliedItemsGroupCO> getSuppliedItemsGroups() {
		return suppliedItemsGroups;
	}

	public void setSuppliedItemsGroups(ArrayList<SuppliedItemsGroupCO> suppliedItemsGroups) {
		this.suppliedItemsGroups = suppliedItemsGroups;
	}

	public String getIssuanceDate() {
		return issuanceDate;
	}

	public void setIssuanceDate(String issuanceDate) {
		this.issuanceDate = issuanceDate;
	}
}
