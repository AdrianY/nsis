package com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import static com.dhl.inventory.validationgroups.ValidationGroup.*;

public class SupplyRequestApproverCO {
	@Length(max=150,groups = {Approve.class, Reject.class})
	private String username;
	
	@NotBlank(groups = {Reject.class}, message="Provide explanation for rejecting this request")
	@Length(max=500,groups = {Approve.class, Reject.class})
	private String remarks;
	
	public SupplyRequestApproverCO(){}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}
