package com.dhl.inventory.dto.commandobject.servicecenter.requestissuance;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.dhl.inventory.validationgroups.ValidationGroup.Finalize;
import com.dhl.inventory.validationgroups.ValidationGroup.Submit;
import com.dhl.inventory.validationgroups.ValidationGroup.Update;
import com.dhl.inventory.validationgroups.ValidationGroup.Validate;

public class SuppliedItemsCO {
	@NotBlank(groups = {Validate.class, Update.class, Submit.class, Finalize.class})
	@Length(max=32,groups = {Validate.class,Update.class, Submit.class, Finalize.class})
	private String sourceBatchNumber;
	
	@Min(value=0,groups = {Validate.class,Update.class, Submit.class, Validate.class, Finalize.class})
	private int previousSuppliedQuantity;
	
	@Min(value=0,groups = {Validate.class,Update.class, Submit.class, Validate.class, Finalize.class})
	private int suppliedQuantity;
	
	@Length(max=200,groups = {Validate.class,Update.class, Submit.class, Validate.class, Finalize.class})
	private String remarks;
	
	public SuppliedItemsCO(){}

	/**
	 * @return the sourceBatchNumber
	 */
	public String getSourceBatchNumber() {
		return sourceBatchNumber;
	}

	/**
	 * @param sourceBatchNumber the sourceBatchNumber to set
	 */
	public void setSourceBatchNumber(String sourceBatchNumber) {
		this.sourceBatchNumber = sourceBatchNumber;
	}

	/**
	 * @return the previousSuppliedQuantity
	 */
	public int getPreviousSuppliedQuantity() {
		return previousSuppliedQuantity;
	}

	/**
	 * @param previousSuppliedQuantity the previousSuppliedQuantity to set
	 */
	public void setPreviousSuppliedQuantity(int previousSuppliedQuantity) {
		this.previousSuppliedQuantity = previousSuppliedQuantity;
	}

	/**
	 * @return the suppliedQuantity
	 */
	public int getSuppliedQuantity() {
		return suppliedQuantity;
	}

	/**
	 * @param suppliedQuantity the suppliedQuantity to set
	 */
	public void setSuppliedQuantity(int suppliedQuantity) {
		this.suppliedQuantity = suppliedQuantity;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}
