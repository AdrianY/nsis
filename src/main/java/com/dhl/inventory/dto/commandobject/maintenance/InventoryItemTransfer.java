package com.dhl.inventory.dto.commandobject.maintenance;

import javax.ws.rs.FormParam;

public class InventoryItemTransfer {
	
	@FormParam("codeName")
	private String codeName;
	
	@FormParam("description")
	private String description;
	
	@FormParam("unitOfMeasurement")
	private String unitOfMeasurement; 
	
	@FormParam("piecesPerUnitOfMeasurement")
	private int piecesPerUnitOfMeasurement;
	
	@FormParam("type")
	private String type;
	
	public InventoryItemTransfer(){}
	
	public InventoryItemTransfer(String codeName, String type){
		this.codeName = codeName;
		this.type = type;
	}
	
	public String getDescription() {
		return description;
	}

	public InventoryItemTransfer setDescription(String description) {
		this.description = description;
		return this;
	}

	public String getUnitOfMeasurement() {
		return unitOfMeasurement;
	}

	public InventoryItemTransfer setUnitOfMeasurement(String uom) {
		this.unitOfMeasurement = uom;
		return this;
	}

	public String getCodeName() {
		return codeName;
	}

	public int getPiecesPerUnitOfMeasurement() {
		return piecesPerUnitOfMeasurement;
	}

	public String getType() {
		return type;
	}

	public InventoryItemTransfer setPiecesPerUom(int piecesPerUom) {
		this.piecesPerUnitOfMeasurement = piecesPerUom;
		return this;
	}

}
