package com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance;


import java.util.ArrayList;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.dhl.inventory.validationgroups.ValidationGroup.Approve;
import com.dhl.inventory.validationgroups.ValidationGroup.Create;
import com.dhl.inventory.validationgroups.ValidationGroup.Reject;
import com.dhl.inventory.validationgroups.ValidationGroup.Submit;
import com.dhl.inventory.validationgroups.ValidationGroup.Update;

public class PhysicalStockCountRequestCO {
	
	@NotBlank(groups = {Create.class, Update.class, Submit.class, Approve.class, Reject.class})
	@Length(max=32,groups = {Create.class, Update.class, Submit.class, Approve.class, Reject.class})
	private String requestNumber;
	
	@NotNull(groups = {Submit.class, Approve.class})
	@NotEmpty(groups = {Submit.class, Approve.class})
	@Valid
	private ArrayList<PhysicalStockCountCO> physicalCounts;
	
	public PhysicalStockCountRequestCO(){}

	/**
	 * @return the requestNumber
	 */
	public String getRequestNumber() {
		return requestNumber;
	}

	/**
	 * @param requestNumber the requestNumber to set
	 */
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * @return the physicalCounts
	 */
	public ArrayList<PhysicalStockCountCO> getPhysicalCounts() {
		return physicalCounts;
	}

	/**
	 * @param physicalCounts the physicalCounts to set
	 */
	public void setPhysicalCounts(ArrayList<PhysicalStockCountCO> physicalCounts) {
		this.physicalCounts = physicalCounts;
	}
	
	
}
