package com.dhl.inventory.dto.commandobject.warehouse.inventorymaintenance;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.dhl.inventory.validationgroups.ValidationGroup.Finalize;

import static com.dhl.inventory.validationgroups.ValidationGroup.*;

import javax.validation.constraints.Min;

public class DeliveredItemBatchCO {
	
	@NotBlank(groups = {Update.class, Validate.class, Finalize.class})
	@Length(max=150, groups = {Update.class, Validate.class, Finalize.class})
	private String batchNumber;
	
	@NotBlank(groups = {Update.class, Validate.class, Finalize.class})
	@Length(max=50, groups = {Update.class, Validate.class, Finalize.class})
	private String rackingCode;
	
	@NotBlank(groups = {Update.class, Validate.class, Finalize.class})
	private String dateStored;
	
	@Min(value=1, groups = {Update.class, Validate.class, Finalize.class})
	private int quantity;
	
	public DeliveredItemBatchCO(){}

	public String getRackingCode() {
		return rackingCode;
	}

	public void setRackingCode(String rackingCode) {
		this.rackingCode = rackingCode;
	}

	public String getDateStored() {
		return dateStored;
	}

	public void setDateStored(String dateStored) {
		this.dateStored = dateStored;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
}
