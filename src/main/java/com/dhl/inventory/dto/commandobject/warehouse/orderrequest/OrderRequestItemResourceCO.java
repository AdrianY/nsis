package com.dhl.inventory.dto.commandobject.warehouse.orderrequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.dhl.inventory.validationgroups.OrderRequestItemValidationGroup.Validate;

public class OrderRequestItemResourceCO {

	@NotBlank(groups = Validate.class)
	private String facilityId;
	
	@NotNull(groups = Validate.class)
	@Valid
	private OrderRequestItemCO details;
	
	public OrderRequestItemResourceCO(){}

	public String getFacilityId() {
		return facilityId;
	}

	public void setFacilityId(String facilityId) {
		this.facilityId = facilityId;
	}

	public OrderRequestItemCO getDetails() {
		return details;
	}

	public void setDetails(OrderRequestItemCO orderRequestItemDetails) {
		this.details = orderRequestItemDetails;
	}
}
