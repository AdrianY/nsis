package com.dhl.inventory.dto.commandobject.maintenance;

public class CustomerAccountCO {
	
	private String name;
	
	private String accountNumber;
	
	private String emailAddress;
	
	private String contactNumber;
	
	private String deliveryAddress;
	
	private String facilityCodeName;
	
	public CustomerAccountCO(){}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	/**
	 * @return the deliveryAddress
	 */
	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	/**
	 * @param deliveryAddress the deliveryAddress to set
	 */
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the facilityCodeName
	 */
	public String getFacilityCodeName() {
		return facilityCodeName;
	}

	/**
	 * @param facilityCodeName the facilityCodeName to set
	 */
	public void setFacilityCodeName(String facilityCodeName) {
		this.facilityCodeName = facilityCodeName;
	}
}
