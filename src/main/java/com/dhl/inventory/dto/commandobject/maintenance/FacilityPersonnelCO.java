package com.dhl.inventory.dto.commandobject.maintenance;

public class FacilityPersonnelCO {
	
	private String facilityId;
	
	private String username;
	
	public FacilityPersonnelCO(){}
	
	public String getFacilityId() {
		return facilityId;
	}

	public String getUsername() {
		return username;
	}

	public void setFacilityId(String facilityId) {
		this.facilityId = facilityId;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
