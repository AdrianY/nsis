package com.dhl.inventory.dto.commandobject.security;

public class NsisUserRoleQRO {
	
	private String codeName;
	
	public NsisUserRoleQRO(){}

	public String getCodeName() {
		return codeName;
	}

	public NsisUserRoleQRO setCodeName(String codeName) {
		this.codeName = codeName;
		return this;
	}
	
	
}
