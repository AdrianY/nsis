package com.dhl.inventory.dto.commandobject.servicecenter.requestissuance;

import java.util.ArrayList;

import javax.validation.Valid;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.dhl.inventory.validationgroups.ValidationGroup.Create;
import com.dhl.inventory.validationgroups.ValidationGroup.Finalize;
import com.dhl.inventory.validationgroups.ValidationGroup.Reject;
import com.dhl.inventory.validationgroups.ValidationGroup.Submit;
import com.dhl.inventory.validationgroups.ValidationGroup.Update;

public class RequestIssuanceCO {
	
	@NotBlank(groups = {Update.class, Submit.class, Finalize.class, Reject.class})
	@Length(max=5,groups = {Update.class, Submit.class, Finalize.class, Reject.class})
	private String previousFacilityCode;
	
	@NotBlank(groups = {Update.class, Submit.class, Finalize.class, Reject.class})
	@Length(max=5,groups = {Update.class, Submit.class, Finalize.class, Reject.class})
	private String facilityCode;

	@NotBlank(groups = {Create.class, Update.class, Submit.class, Finalize.class, Reject.class})
	@Length(max=32,groups = {Update.class, Submit.class, Finalize.class, Reject.class})
	private String requestNumber;
	
	@NotBlank(groups = {Reject.class})
	@Length(max=500,groups = {Update.class, Submit.class,Finalize.class, Reject.class})
	private String custodianRemarks;
	
	@NotEmpty(groups = {Update.class, Submit.class, Finalize.class})
	@Valid
	private String custodianUsername;
	
	@NotBlank(groups = {Finalize.class})
	private String issuanceDate;
	
	@NotBlank(groups = {Submit.class,Finalize.class})
	@Length(max=10,groups = {Update.class, Submit.class,Finalize.class})
	private String routeCode;
	
	@NotEmpty(groups = {Update.class, Submit.class, Finalize.class})
	@Valid
	private ArrayList<SuppliedItemsGroupCO> suppliedItemsGroups;
	
	public RequestIssuanceCO(){}
	
	public RequestIssuanceCO(final String requestNumber){
		this.requestNumber = requestNumber;
	}
	
	public String getRequestNumber() {
		return requestNumber;
	}

	public String getCustodianRemarks() {
		return custodianRemarks;
	}

	public String getCustodianUsername() {
		return custodianUsername;
	}

	public String getIssuanceDate() {
		return issuanceDate;
	}
	

	/**
	 * @param requestNumber the requestNumber to set
	 */
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * @param custodianRemarks the custodianRemarks to set
	 */
	public void setCustodianRemarks(String custodianRemarks) {
		this.custodianRemarks = custodianRemarks;
	}

	/**
	 * @param custodianUsername the custodianUsername to set
	 */
	public void setCustodianUsername(String custodianUsername) {
		this.custodianUsername = custodianUsername;
	}

	/**
	 * @param issuanceDate the issuanceDate to set
	 */
	public void setIssuanceDate(String issuanceDate) {
		this.issuanceDate = issuanceDate;
	}

	/**
	 * @return the suppliedItemsGroups
	 */
	public ArrayList<SuppliedItemsGroupCO> getSuppliedItemsGroups() {
		return suppliedItemsGroups;
	}

	/**
	 * @param suppliedItemsGroups the suppliedItemsGroups to set
	 */
	public void setSuppliedItemsGroups(ArrayList<SuppliedItemsGroupCO> suppliedItemsGroups) {
		this.suppliedItemsGroups = suppliedItemsGroups;
	}

	/**
	 * @return the facilityCode
	 */
	public String getFacilityCode() {
		return facilityCode;
	}

	/**
	 * @param facilityCode the facilityCode to set
	 */
	public void setFacilityCode(String facilityCode) {
		this.facilityCode = facilityCode;
	}

	/**
	 * @return the routeCode
	 */
	public String getRouteCode() {
		return routeCode;
	}

	/**
	 * @param routeCode the routeCode to set
	 */
	public void setRouteCode(String routeCode) {
		this.routeCode = routeCode;
	}

	/**
	 * @return the previousFacilityCode
	 */
	public String getPreviousFacilityCode() {
		return previousFacilityCode;
	}

	/**
	 * @param previousFacilityCode the previousFacilityCode to set
	 */
	public void setPreviousFacilityCode(String previousFacilityCode) {
		this.previousFacilityCode = previousFacilityCode;
	}
}
