package com.dhl.inventory.dto.commandobject.warehouse.orderrequest;

import java.util.List;

public class CalculateOrderRequestItemsCO {
	private String facilityId;
	
	private String orderCurrency;
	
	private List<OrderRequestItemCO> toBeCalculatedItems;
	
	public CalculateOrderRequestItemsCO(){}


	public String getFacilityId() {
		return facilityId;
	}

	public void setFacilityId(String facilityId) {
		this.facilityId = facilityId;
	}

	public String getOrderCurrency() {
		return orderCurrency;
	}

	public void setOrderCurrency(String orderCurrency) {
		this.orderCurrency = orderCurrency;
	}

	public List<OrderRequestItemCO> getToBeCalculatedItems() {
		return toBeCalculatedItems;
	}

	public void setToBeCalculatedItems(List<OrderRequestItemCO> toBeCalculatedItems) {
		this.toBeCalculatedItems = toBeCalculatedItems;
	}
}
