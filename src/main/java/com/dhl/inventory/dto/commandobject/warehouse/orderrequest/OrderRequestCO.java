package com.dhl.inventory.dto.commandobject.warehouse.orderrequest;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.dhl.inventory.validationgroups.OrderRequestValidationGroup.Approve;
import com.dhl.inventory.validationgroups.OrderRequestValidationGroup.Create;
import com.dhl.inventory.validationgroups.OrderRequestValidationGroup.Reject;
import com.dhl.inventory.validationgroups.OrderRequestValidationGroup.Submit;
import com.dhl.inventory.validationgroups.OrderRequestValidationGroup.Update;

public class OrderRequestCO {
	
	@NotBlank(groups = {Create.class, Update.class, Submit.class, Approve.class, Reject.class})
	@Length(max=32,groups = {Create.class, Update.class, Submit.class, Approve.class, Reject.class})
	private String orderNumber;
	
	@NotBlank(groups = {Create.class, Update.class, Submit.class, Approve.class})
	private String facilityId;

	@NotBlank(groups = {Submit.class, Approve.class})
	private String orderType;
	
	private String orderDate;
	
	@NotBlank(groups = {Submit.class})
	@Length(max=150,groups = {Create.class, Update.class, Submit.class, Approve.class})
	private String orderByUsername;
	
	@NotBlank(groups = {Submit.class, Approve.class})
	@Length(max=5,min=2,groups = {Create.class, Update.class, Submit.class, Approve.class})
	private String orderCycle;
	
	@NotBlank(groups = {Create.class, Update.class, Submit.class, Approve.class})
	@Length(max=3,groups = {Create.class, Update.class, Submit.class, Approve.class})
	private String currency;
	
	@NotBlank(groups = {Submit.class, Approve.class})
	private String deliveryDate;
	
	@NotBlank(groups = {Create.class, Update.class, Submit.class, Approve.class})
	private String status;
	
	@NotNull(groups = {Submit.class, Approve.class})
	@Valid
	private OrderRequestRecipientCO recipient;
	
	@NotNull(groups = {Approve.class, Reject.class})
	@Valid
	private OrderRequestApproverCO approver;
	
	@NotNull(groups = {Submit.class, Approve.class})
	@NotEmpty(groups = {Submit.class, Approve.class})
	@Valid
	private ArrayList<OrderRequestItemCO> itemsRequested;

	public OrderRequestCO(){}
	
	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	
	public String getOrderByUsername() {
		return orderByUsername;
	}

	public void setOrderByUsername(String orderByUsername) {
		this.orderByUsername = orderByUsername;
	}

	public String getOrderCycle() {
		return orderCycle;
	}

	public void setOrderCycle(String orderCycle) {
		this.orderCycle = orderCycle;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public OrderRequestRecipientCO getRecipient() {
		return recipient;
	}

	public void setRecipient(OrderRequestRecipientCO recipient) {
		this.recipient = recipient;
	}

	public List<OrderRequestItemCO> getItemsRequested() {
		return itemsRequested;
	}

	public void setItemsRequested(ArrayList<OrderRequestItemCO> itemsRequested) {
		this.itemsRequested = itemsRequested;
	}

	public String getFacilityId() {
		return facilityId;
	}

	public void setFacilityId(String facilityId) {
		this.facilityId = facilityId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public OrderRequestApproverCO getApprover() {
		return approver;
	}

	public void setApprover(OrderRequestApproverCO approver) {
		this.approver = approver;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	
	
}
