package com.dhl.inventory.dto.commandobject.customerinterfaces;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import static com.dhl.inventory.validationgroups.ValidationGroup.*;

import javax.validation.constraints.Min;

public class RequestedItemCO {
	
	@NotBlank(groups = {Update.class, Submit.class, Validate.class})
	@Length(max=50,groups = {Create.class, Update.class, Submit.class, Validate.class})
	private String itemCode;
	
	@Min(value=1,groups = {Update.class, Submit.class, Validate.class})
	private int quantity;
	
	@Length(max=200,groups = {Update.class, Submit.class, Validate.class})
	private String remarks;
	
	public RequestedItemCO(){}

	/**
	 * @return the itemCode
	 */
	public String getItemCode() {
		return itemCode;
	}

	/**
	 * @param itemCode the itemCode to set
	 */
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
}
