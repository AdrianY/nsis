package com.dhl.inventory.dto.commandobject.maintenance;

public class InventoryItemCO {
	
	private String codeName;
	
	private String description;
	
	private String unitOfMeasurement; 
	
	private int piecesPerUnitOfMeasurement;
	
	private String type;
	
	private String cost;
	
	private String currency;
	
	public InventoryItemCO(){}
	
	public InventoryItemCO(String codeName, String type){
		this.codeName = codeName;
		this.type = type;
	}
	
	public String getDescription() {
		return description;
	}

	public InventoryItemCO setDescription(String description) {
		this.description = description;
		return this;
	}

	public String getUnitOfMeasurement() {
		return unitOfMeasurement;
	}

	public InventoryItemCO setUnitOfMeasurement(String uom) {
		this.unitOfMeasurement = uom;
		return this;
	}

	public String getCodeName() {
		return codeName;
	}

	public int getPiecesPerUnitOfMeasurement() {
		return piecesPerUnitOfMeasurement;
	}

	public String getType() {
		return type;
	}

	public InventoryItemCO setPiecesPerUom(int piecesPerUom) {
		this.piecesPerUnitOfMeasurement = piecesPerUom;
		return this;
	}

	public String getCost() {
		return cost;
	}

	public InventoryItemCO setCost(String cost) {
		this.cost = cost;
		return this;
	}

	public String getCurrency() {
		return currency;
	}

	public InventoryItemCO setCurrency(String currency) {
		this.currency = currency;
		return this;
	}
}
