package com.dhl.inventory.dto.commandobject.maintenance;

public class WarehouseLocationCO {
	private String rackingCode;
	
	private String facilityCode;
	
	public WarehouseLocationCO(){}

	public String getRackingCode() {
		return rackingCode;
	}

	public void setRackingCode(String rackingCode) {
		this.rackingCode = rackingCode;
	}

	public String getFacilityCode() {
		return facilityCode;
	}

	public void setFacilityCode(String facilityId) {
		this.facilityCode = facilityId;
	}
}
