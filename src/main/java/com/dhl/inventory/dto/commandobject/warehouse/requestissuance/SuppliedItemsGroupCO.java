package com.dhl.inventory.dto.commandobject.warehouse.requestissuance;

import java.util.ArrayList;

import javax.validation.Valid;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import static com.dhl.inventory.validationgroups.ValidationGroup.*;

public class SuppliedItemsGroupCO {
	
	@NotBlank(groups = {Update.class, Submit.class, Finalize.class})
	@Length(max=32,groups = {Update.class, Submit.class, Finalize.class})
	private String itemCode;
	
	@Valid
	private ArrayList<SuppliedItemsCO> suppliedItems;
	
	public SuppliedItemsGroupCO(){}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public ArrayList<SuppliedItemsCO> getSuppliedItems() {
		return suppliedItems;
	}

	public void setSuppliedItems(ArrayList<SuppliedItemsCO> suppliedItems) {
		this.suppliedItems = suppliedItems;
	}
}
