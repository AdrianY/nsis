package com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance;

import java.util.ArrayList;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.dhl.inventory.validationgroups.ValidationGroup.Finalize;
import com.dhl.inventory.validationgroups.ValidationGroup.Update;

public class DeliveredItemBatchGroupCO {
	
	@NotBlank(groups = {Update.class, Finalize.class})
	@Length(max=50,groups = {Update.class, Finalize.class})
	private String itemCode;
	
	//Should be validated if we want the API to stand alone but for now we validate only inside the service
	private ArrayList<DeliveredItemBatchCO> itemBatches;
	
	public DeliveredItemBatchGroupCO(){}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public ArrayList<DeliveredItemBatchCO> getItemBatches() {
		return itemBatches;
	}

	public void setItemBatches(ArrayList<DeliveredItemBatchCO> itemBatches) {
		this.itemBatches = itemBatches;
	}
}
