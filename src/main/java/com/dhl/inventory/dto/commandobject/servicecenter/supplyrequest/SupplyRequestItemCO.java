package com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import static com.dhl.inventory.validationgroups.ValidationGroup.*;

public class SupplyRequestItemCO {
	@NotBlank(groups = {Create.class, Update.class, Submit.class, Validate.class, Approve.class})
	@Length(max=50,groups = {Create.class, Update.class, Submit.class, Validate.class, Approve.class})
	private String itemCode;
	
	@Min(value=1,groups = {Create.class, Update.class, Submit.class, Validate.class, Approve.class})
	private int quantity;
	
	@Length(max=200,groups = {Create.class, Update.class, Submit.class, Validate.class, Approve.class})
	private String remarks;
	
	public SupplyRequestItemCO(){}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}
