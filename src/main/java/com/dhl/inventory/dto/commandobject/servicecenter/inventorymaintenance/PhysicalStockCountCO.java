package com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.dhl.inventory.validationgroups.ValidationGroup.Approve;
import com.dhl.inventory.validationgroups.ValidationGroup.Create;
import com.dhl.inventory.validationgroups.ValidationGroup.Finalize;
import com.dhl.inventory.validationgroups.ValidationGroup.Submit;
import com.dhl.inventory.validationgroups.ValidationGroup.Update;
import com.dhl.inventory.validationgroups.ValidationGroup.Validate;

public class PhysicalStockCountCO {
	
	@Min(value=0,groups = {Create.class, Update.class, Submit.class, Validate.class, Approve.class})
	private int count;
	
	@NotBlank(groups = {Update.class, Validate.class, Finalize.class})
	@Length(max=150, groups = {Update.class, Validate.class, Finalize.class})
	private String batchNumber;
	
	public PhysicalStockCountCO(){}

	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}

	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * @return the batchNumber
	 */
	public String getBatchNumber() {
		return batchNumber;
	}

	/**
	 * @param batchNumber the batchNumber to set
	 */
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
	
	
}
