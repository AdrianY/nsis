package com.dhl.inventory.dto.queryobject.warehouse.requestissuance;

import java.util.ArrayList;

public class RequestedItemQRO {
	
	private String itemCode;
	
	private ArrayList<SuppliedItemsQRO> suppliedItems;
	
	public RequestedItemQRO(){}

	public String getItemCode() {
		return itemCode;
	}

	public RequestedItemQRO setItemCode(String itemCode) {
		this.itemCode = itemCode;
		return this;
	}

	public ArrayList<SuppliedItemsQRO> getSuppliedItems() {
		return suppliedItems;
	}

	public RequestedItemQRO setSuppliedItems(ArrayList<SuppliedItemsQRO> suppliedItems) {
		this.suppliedItems = suppliedItems;
		return this;
	}
}
