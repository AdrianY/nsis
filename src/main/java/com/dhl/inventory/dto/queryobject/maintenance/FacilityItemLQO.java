package com.dhl.inventory.dto.queryobject.maintenance;

import java.util.HashMap;

import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;

public class FacilityItemLQO extends AbstractSearchListQueryObject {
	@PathParam("facilityId")
	private String facilityId;
	
	@QueryParam("itemCode")
	private String itemCode;
	
	@QueryParam("type")
	private String type;
	
	@QueryParam("description")
	private String description;
	
	@QueryParam("piecesPerUnitOfMeaurement")
	private int piecesPerUnitOfMeaurement;
	
	@QueryParam("unitOfMeasurement")
	private String unitOfMeasurement;
	
	@QueryParam("currency")
	private String orderCurrency;
	
	public FacilityItemLQO(){
		orderByMap = new HashMap<String, String>();
		orderByMap.put("type", "type");
		orderByMap.put("description", "description");
		orderByMap.put("itemCode", "codeName");
		orderByMap.put("piecesPerUom", "piecesPerUnitOfMeasurement");
		orderByMap.put("uom", "unitOfMeasurement");
		
		defaultOrderField = "codeName";

	}

	public String getFacilityId() {
		return facilityId;
	}

	public void setFacilityId(String facilityId) {
		this.facilityId = facilityId;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPiecesPerUnitOfMeaurement() {
		return piecesPerUnitOfMeaurement;
	}

	public void setPiecesPerUnitOfMeaurement(int piecesPerUnitOfMeaurement) {
		this.piecesPerUnitOfMeaurement = piecesPerUnitOfMeaurement;
	}

	public String getUnitOfMeasurement() {
		return unitOfMeasurement;
	}

	public void setUnitOfMeasurement(String unitOfMeasurement) {
		this.unitOfMeasurement = unitOfMeasurement;
	}

	public String getOrderCurrency() {
		return orderCurrency;
	}

	public void setOrderCurrency(String orderCurrency) {
		this.orderCurrency = orderCurrency;
	}
}
