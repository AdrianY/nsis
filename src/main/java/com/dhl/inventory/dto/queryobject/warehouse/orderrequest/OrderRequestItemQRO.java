package com.dhl.inventory.dto.queryobject.warehouse.orderrequest;

public class OrderRequestItemQRO {
	private String itemCode;
	
	private int quantity;
	
	private String remarks;
	
	public OrderRequestItemQRO(){}

	public String getItemCode() {
		return itemCode;
	}

	public OrderRequestItemQRO setItemCode(String itemCode) {
		this.itemCode = itemCode;
		return this;
	}

	public int getQuantity() {
		return quantity;
	}

	public OrderRequestItemQRO setQuantity(int quantity) {
		this.quantity = quantity;
		return this;
	}

	public String getRemarks() {
		return remarks;
	}

	public OrderRequestItemQRO setRemarks(String remarks) {
		this.remarks = remarks;
		return this;
	}
}
