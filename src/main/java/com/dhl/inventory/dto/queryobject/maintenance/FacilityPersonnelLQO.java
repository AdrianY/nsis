package com.dhl.inventory.dto.queryobject.maintenance;

import java.util.HashMap;

import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;

public class FacilityPersonnelLQO extends AbstractSearchListQueryObject{
	
	@QueryParam("username")
	private String username;
	
	@QueryParam("fullName")
	private String fullName;
	
	@PathParam("facilityId")
	private String facilityId;
	
	public FacilityPersonnelLQO(){
		orderByMap = new HashMap<String, String>();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFacilityId() {
		return facilityId;
	}

	public void setFacilityId(String facilityId) {
		this.facilityId = facilityId;
	}
}
