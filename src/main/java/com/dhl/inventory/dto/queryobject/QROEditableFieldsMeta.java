package com.dhl.inventory.dto.queryobject;

import java.util.ArrayList;

public final class QROEditableFieldsMeta {
	
	private String type;
	
	private ArrayList<String> fieldList;
	
	public String getType() {
		return type;
	}

	public ArrayList<String> getFieldList() {
		return fieldList;
	}

	private QROEditableFieldsMeta(){}
	
	public static enum Types{
		ALL, SPECIFIC, EXCEPT, NONE
	}
	
	public static QROEditableFieldsMeta editableAllInstance(){
		QROEditableFieldsMeta returnValue = new QROEditableFieldsMeta();
		returnValue.type = Types.ALL.toString();
		return returnValue;
	}
	
	public static QROEditableFieldsMeta editableSpecificFieldsInstance(final ArrayList<String> fieldsList){
		QROEditableFieldsMeta returnValue = new QROEditableFieldsMeta();
		returnValue.type = Types.SPECIFIC.toString();
		returnValue.fieldList = fieldsList;
		return returnValue;
	}
	
	public static QROEditableFieldsMeta editableExceptCertainFieldsInstance(final ArrayList<String> fieldsList){
		QROEditableFieldsMeta returnValue = new QROEditableFieldsMeta();
		returnValue.type = Types.EXCEPT.toString();
		returnValue.fieldList = fieldsList;
		return returnValue;
	}
	
	public static QROEditableFieldsMeta noEditableInstance(){
		QROEditableFieldsMeta returnValue = new QROEditableFieldsMeta();
		returnValue.type = Types.NONE.toString();
		return returnValue;
	}
}
