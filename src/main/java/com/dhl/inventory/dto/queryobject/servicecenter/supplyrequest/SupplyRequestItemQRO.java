package com.dhl.inventory.dto.queryobject.servicecenter.supplyrequest;

public class SupplyRequestItemQRO {
	private String itemCode;
	
	private int quantity;
	
	private String remarks;
	
	public SupplyRequestItemQRO(){}

	public String getItemCode() {
		return itemCode;
	}

	public SupplyRequestItemQRO setItemCode(String itemCode) {
		this.itemCode = itemCode;
		return this;
	}

	public int getQuantity() {
		return quantity;
	}

	public SupplyRequestItemQRO setQuantity(int quantity) {
		this.quantity = quantity;
		return this;
	}

	public String getRemarks() {
		return remarks;
	}

	public SupplyRequestItemQRO setRemarks(String remarks) {
		this.remarks = remarks;
		return this;
	}
}
