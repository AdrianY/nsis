package com.dhl.inventory.dto.queryobject.servicecenter.supplyrequest;

import com.dhl.inventory.dto.queryobject.PersonnelQRO;

public class SupplyRequestApproverQRO {
	private PersonnelQRO approverDetails;
	
	private String remarks;
	
	public SupplyRequestApproverQRO(){}

	public PersonnelQRO getApproverDetails() {
		return approverDetails;
	}

	public SupplyRequestApproverQRO setApproverDetails(PersonnelQRO approver) {
		this.approverDetails = approver;
		return this;
	}

	public String getRemarks() {
		return remarks;
	}

	public SupplyRequestApproverQRO setRemarks(String remarks) {
		this.remarks = remarks;
		return this;
	}
}
