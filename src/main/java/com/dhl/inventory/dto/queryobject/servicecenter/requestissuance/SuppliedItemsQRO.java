package com.dhl.inventory.dto.queryobject.servicecenter.requestissuance;

public class SuppliedItemsQRO {
	private String sourceBatchNumber;
	
	private int suppliedQuantity;
	
	private String remarks;
	
	public SuppliedItemsQRO(){}

	public String getSourceBatchNumber() {
		return sourceBatchNumber;
	}

	public SuppliedItemsQRO setSourceBatchNumber(String sourceBatchNumber) {
		this.sourceBatchNumber = sourceBatchNumber;
		return this;
	}

	public int getSuppliedQuantity() {
		return suppliedQuantity;
	}

	public SuppliedItemsQRO setSuppliedQuantity(int suppliedQuantity) {
		this.suppliedQuantity = suppliedQuantity;
		return this;
	}

	public String getRemarks() {
		return remarks;
	}

	public SuppliedItemsQRO setRemarks(String remarks) {
		this.remarks = remarks;
		return this;
	}
}
