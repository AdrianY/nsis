package com.dhl.inventory.dto.queryobject.servicecenter.requestissuance;

import java.util.List;


public class PickListItemGroupQRO {
	
	String itemCode;

	String description;

	String qtyRequested;

	String qtyIssued;

	List<PickListItemQRO> pickListItems;
	
	public PickListItemGroupQRO(){}

	/**
	 * @return the itemCode
	 */
	public String getItemCode() {
		return itemCode;
	}

	/**
	 * @param itemCode the itemCode to set
	 */
	public PickListItemGroupQRO setItemCode(String itemCode) {
		this.itemCode = itemCode;
		return this;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public PickListItemGroupQRO setDescription(String description) {
		this.description = description;
		return this;
	}

	/**
	 * @return the qtyRequested
	 */
	public String getQtyRequested() {
		return qtyRequested;
	}

	/**
	 * @param qtyRequested the qtyRequested to set
	 */
	public PickListItemGroupQRO setQtyRequested(String qtyRequested) {
		this.qtyRequested = qtyRequested;
		return this;
	}

	/**
	 * @return the qtyIssued
	 */
	public String getQtyIssued() {
		return qtyIssued;
	}

	/**
	 * @param qtyIssued the qtyIssued to set
	 */
	public PickListItemGroupQRO setQtyIssued(String qtyIssued) {
		this.qtyIssued = qtyIssued;
		return this;
	}

	/**
	 * @return the pickListItems
	 */
	public List<PickListItemQRO> getPickListItems() {
		return pickListItems;
	}

	/**
	 * @param pickListItems the pickListItems to set
	 */
	public PickListItemGroupQRO setPickListItems(List<PickListItemQRO> pickListItems) {
		this.pickListItems = pickListItems;
		return this;
	}
}
