package com.dhl.inventory.dto.queryobject.warehouse.requestissuance;

public class PickListItemQRO {
	
	String batchNumber;
	
	String qtyIssuedPerBatch;
	
	public PickListItemQRO(){}

	/**
	 * @return the batchNumber
	 */
	public String getBatchNumber() {
		return batchNumber;
	}

	/**
	 * @param batchNumber the batchNumber to set
	 */
	public PickListItemQRO setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
		return this;
	}

	/**
	 * @return the qtyIssuedPerBatch
	 */
	public String getQtyIssuedPerBatch() {
		return qtyIssuedPerBatch;
	}

	/**
	 * @param qtyIssuedPerBatch the qtyIssuedPerBatch to set
	 */
	public PickListItemQRO setQtyIssuedPerBatch(String qtyIssuedPerBatch) {
		this.qtyIssuedPerBatch = qtyIssuedPerBatch;
		return this;
	}

	
	
}
