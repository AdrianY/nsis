package com.dhl.inventory.dto.queryobject.warehouse.orderrequest;

import com.dhl.inventory.dto.queryobject.PersonnelQRO;

public class OrderRequestRecipientQRO {
	private String deliveryAddress;

	private PersonnelQRO recipientDetails;
	
	public OrderRequestRecipientQRO(){}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public OrderRequestRecipientQRO setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
		return this;
	}

	public PersonnelQRO getRecipientDetails() {
		return recipientDetails;
	}

	public OrderRequestRecipientQRO setRecipientDetails(PersonnelQRO recipient) {
		this.recipientDetails = recipient;
		return this;
	}
}
