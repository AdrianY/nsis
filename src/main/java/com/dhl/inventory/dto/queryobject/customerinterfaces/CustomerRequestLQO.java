package com.dhl.inventory.dto.queryobject.customerinterfaces;

import java.util.HashMap;

import javax.ws.rs.QueryParam;

import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;

public class CustomerRequestLQO extends AbstractSearchListQueryObject {
	
	@QueryParam("accountNumber")
	private String accountNumber;
	
	@QueryParam("reqNumber")
	private String reqNumber;
	
	@QueryParam("accountName")
	private String accountName;
	
	@QueryParam("requestType")
	private String requestType;
	
	@QueryParam("status")
	private String status;
	
	@QueryParam("toDate")
	private String toDate;
	
	@QueryParam("fromDate")
	private String fromDate;
	
	@QueryParam("facilityCodeName")
	private String facilityCodeName;
	
	public CustomerRequestLQO(){
		orderByMap = new HashMap<String, String>();
		orderByMap.put("requestNumber", "aa_.request_number");
		orderByMap.put("accountNumber", "aa_.account_number");
		orderByMap.put("accountName", "aa_.account_name");
		orderByMap.put("facilityCode", "aa_.facility_code");
		orderByMap.put("requestDate", "aa_.request_date");
		orderByMap.put("status", "aa_.status");
		defaultOrderField = "aa_.request_number";
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the accountName
	 */
	public String getAccountName() {
		return accountName;
	}

	/**
	 * @param accountName the accountName to set
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 * @return the requestType
	 */
	public String getRequestType() {
		String returnValue = "NA";
		switch(requestType){
			case "CS": returnValue = "Customer Service"; break;
			case "SP": returnValue = "Service Provider"; break;
			case "CR": returnValue = "Courier Request"; break;
		}
		return returnValue;
	}

	/**
	 * @param requestType the requestType to set
	 */
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the facilityCodeName
	 */
	public String getFacilityCodeName() {
		return facilityCodeName;
	}

	/**
	 * @param facilityCodeName the facilityCodeName to set
	 */
	public void setFacilityCodeName(String facilityCodeName) {
		this.facilityCodeName = facilityCodeName;
	}

	/**
	 * @return the reqNumber
	 */
	public String getReqNumber() {
		return reqNumber;
	}

	/**
	 * @param reqNumber the reqNumber to set
	 */
	public void setReqNumber(String reqNumber) {
		this.reqNumber = reqNumber;
	}
}
