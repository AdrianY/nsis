package com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance;

public class PhysicalStockCountQRO {
	
	private String batchNumber;
	
	private String count;
	
	public PhysicalStockCountQRO(){}

	/**
	 * @return the batchNumber
	 */
	public String getBatchNumber() {
		return batchNumber;
	}

	/**
	 * @param batchNumber the batchNumber to set
	 */
	public PhysicalStockCountQRO setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
		return this;
	}

	/**
	 * @return the count
	 */
	public String getCount() {
		return count;
	}

	/**
	 * @param count the count to set
	 */
	public PhysicalStockCountQRO setCount(String count) {
		this.count = count;
		return this;
	}
}
