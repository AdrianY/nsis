package com.dhl.inventory.dto.queryobject.customerinterfaces;

public class CustomerAccountQRO {
	
	private String accountNumber;
	
	private String deliveryAddress;
	
	private String name;
	
	private String contactName;
	
	private String contactNumber;
	
	public CustomerAccountQRO(){}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public CustomerAccountQRO setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
		return this;
	}

	/**
	 * @return the deliveryAddress
	 */
	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	/**
	 * @param deliveryAddress the deliveryAddress to set
	 */
	public CustomerAccountQRO setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
		return this;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public CustomerAccountQRO setName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * @return the emailAddress
	 */
	public String getContactName() {
		return contactName;
	}

	/**
	 * @param contactName the emailAddress to set
	 */
	public CustomerAccountQRO setContactName(String contactName) {
		this.contactName = contactName;
		return this;
	}

	/**
	 * @return the contactNumber
	 */
	public String getContactNumber() {
		return contactNumber;
	}

	/**
	 * @param contactNumber the contactNumber to set
	 */
	public CustomerAccountQRO setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
		return this;
	}
	
	
}
