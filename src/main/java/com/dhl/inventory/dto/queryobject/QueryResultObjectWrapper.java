package com.dhl.inventory.dto.queryobject;

public final class QueryResultObjectWrapper<QRO> {
	
	public static <U> QueryResultObjectWrapper<U> generateInstance(
		final U queryResultObject, 
		final QROEditableFieldsMeta editMeta
	){
		QueryResultObjectWrapper<U> newInstance = new QueryResultObjectWrapper<U>();
		newInstance.body = queryResultObject;
		newInstance.editMeta = editMeta;
		return newInstance;
	}
	
	private QRO body;
	
	private QROEditableFieldsMeta editMeta;
	
	private QueryResultObjectWrapper(){}

	public QRO getBody() {
		return body;
	}

	public QROEditableFieldsMeta getEditMeta() {
		return editMeta;
	}

}
