package com.dhl.inventory.dto.queryobject.customerinterfaces;

public class RequestedItemQRO {
	private String itemCode;
	
	private int quantity;
	
	private String remarks;
	
	public RequestedItemQRO(){}

	/**
	 * @return the itemCode
	 */
	public String getItemCode() {
		return itemCode;
	}

	/**
	 * @param itemCode the itemCode to set
	 */
	public RequestedItemQRO setItemCode(String itemCode) {
		this.itemCode = itemCode;
		return this;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public RequestedItemQRO setQuantity(int quantity) {
		this.quantity = quantity;
		return this;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public RequestedItemQRO setRemarks(String remarks) {
		this.remarks = remarks;
		return this;
	}
	
	
	
}
