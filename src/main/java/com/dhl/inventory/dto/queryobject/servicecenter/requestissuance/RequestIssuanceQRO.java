package com.dhl.inventory.dto.queryobject.servicecenter.requestissuance;

import java.util.ArrayList;

import com.dhl.inventory.dto.queryobject.PersonnelQRO;

public class RequestIssuanceQRO {
private String requestNumber;
	
	private String status;
	
	private String requestDate;
	
	private String requiredDate;

	private String issuanceDate;
	
	private String requestType;
	
	private PersonnelQRO requestorDetails;
	
	private String previousFacilityCodeName;
	
	private String facilityCodeName;
	
	private String routeCode;
	
	private CustomerQRO customer;
	
	private HandlingCustodianQRO handlingCustodian;
	
	private ArrayList<RequestedItemQRO> requestedItems;
	
	public RequestIssuanceQRO(){}

	public String getRequestNumber() {
		return requestNumber;
	}

	public RequestIssuanceQRO setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
		return this;
	}

	public String getStatus() {
		return status;
	}

	public RequestIssuanceQRO setStatus(String status) {
		this.status = status;
		return this;
	}

	public String getRequestDate() {
		return requestDate;
	}

	public RequestIssuanceQRO setRequestDate(String requestDate) {
		this.requestDate = requestDate;
		return this;
	}

	public String getIssuanceDate() {
		return issuanceDate;
	}

	public RequestIssuanceQRO setIssuanceDate(String issuanceDate) {
		this.issuanceDate = issuanceDate;
		return this;
	}

	public PersonnelQRO getRequestorDetails() {
		return requestorDetails;
	}

	public RequestIssuanceQRO setRequestorDetails(PersonnelQRO requestorDetails) {
		this.requestorDetails = requestorDetails;
		return this;
	}

	public String getFacilityCodeName() {
		return facilityCodeName;
	}

	public RequestIssuanceQRO setFacilityCodeName(String facilityCodeName) {
		this.facilityCodeName = facilityCodeName;
		return this;
	}

	public HandlingCustodianQRO getHandlingCustodian() {
		return handlingCustodian;
	}

	public RequestIssuanceQRO setHandlingCustodian(HandlingCustodianQRO handlingCustodian) {
		this.handlingCustodian = handlingCustodian;
		return this;
	}

	public ArrayList<RequestedItemQRO> getRequestedItems() {
		return requestedItems;
	}

	public RequestIssuanceQRO setRequestedItems(ArrayList<RequestedItemQRO> requestedItems) {
		this.requestedItems = requestedItems;
		return this;
	}

	/**
	 * @return the customer
	 */
	public CustomerQRO getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public RequestIssuanceQRO setCustomer(CustomerQRO customer) {
		this.customer = customer;
		return this;
	}

	/**
	 * @return the routeCode
	 */
	public String getRouteCode() {
		return routeCode;
	}

	/**
	 * @param routeCode the routeCode to set
	 */
	public RequestIssuanceQRO setRouteCode(String routeCode) {
		this.routeCode = routeCode;
		return this;
	}

	/**
	 * @return the requiredDate
	 */
	public String getRequiredDate() {
		return requiredDate;
	}

	/**
	 * @param requiredDate the requiredDate to set
	 */
	public RequestIssuanceQRO setRequiredDate(String requiredDate) {
		this.requiredDate = requiredDate;
		return this;
	}

	/**
	 * @return the requestType
	 */
	public String getRequestType() {
		return requestType;
	}

	/**
	 * @param requestType the requestType to set
	 */
	public RequestIssuanceQRO setRequestType(String requestType) {
		this.requestType = requestType;
		return this;
	}

	/**
	 * @return the previousFacilityCodeName
	 */
	public String getPreviousFacilityCodeName() {
		return previousFacilityCodeName;
	}

	/**
	 * @param previousFacilityCodeName the previousFacilityCodeName to set
	 */
	public RequestIssuanceQRO setPreviousFacilityCodeName(String previousFacilityCodeName) {
		this.previousFacilityCodeName = previousFacilityCodeName;
		return this;
	}
}
