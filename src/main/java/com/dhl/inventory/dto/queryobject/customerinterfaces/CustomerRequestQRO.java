package com.dhl.inventory.dto.queryobject.customerinterfaces;

import java.util.List;

import com.dhl.inventory.dto.queryobject.PersonnelQRO;

public class CustomerRequestQRO {
	
	private String requestNumber;
	
	private String facilityCodeName;

	private String requestDate;
	
	private PersonnelQRO requestBy;
	
	private String requiredDate;
	
	private String status;
	
	private String requestType;
	
	private CustomerAccountQRO customer;
	
	private CustomerRequestCustodianQRO custodian;
	
	private String routeCode;
	
	private List<RequestedItemQRO> requestedItems;
	
	public CustomerRequestQRO(){}

	/**
	 * @return the requestNumber
	 */
	public String getRequestNumber() {
		return requestNumber;
	}

	/**
	 * @param requestNumber the requestNumber to set
	 */
	public CustomerRequestQRO setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
		return this;
	}

	/**
	 * @return the facilityCodeName
	 */
	public String getFacilityCodeName() {
		return facilityCodeName;
	}

	/**
	 * @param facilityCodeName the facilityCodeName to set
	 */
	public CustomerRequestQRO setFacilityCodeName(String facilityCodeName) {
		this.facilityCodeName = facilityCodeName;
		return this;
	}

	/**
	 * @return the requestDate
	 */
	public String getRequestDate() {
		return requestDate;
	}

	/**
	 * @param requestDate the requestDate to set
	 */
	public CustomerRequestQRO setRequestDate(String requestDate) {
		this.requestDate = requestDate;
		return this;
	}

	/**
	 * @return the requestBy
	 */
	public PersonnelQRO getRequestBy() {
		return requestBy;
	}

	/**
	 * @param requestBy the requestBy to set
	 */
	public CustomerRequestQRO setRequestBy(PersonnelQRO requestBy) {
		this.requestBy = requestBy;
		return this;
	}

	/**
	 * @return the requiredDate
	 */
	public String getRequiredDate() {
		return requiredDate;
	}

	/**
	 * @param requiredDate the requiredDate to set
	 */
	public CustomerRequestQRO setRequiredDate(String requiredDate) {
		this.requiredDate = requiredDate;
		return this;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public CustomerRequestQRO setStatus(String status) {
		this.status = status;
		return this;
	}

	/**
	 * @return the requestedItems
	 */
	public List<RequestedItemQRO> getRequestedItems() {
		return requestedItems;
	}

	/**
	 * @param requestedItems the requestedItems to set
	 */
	public CustomerRequestQRO setRequestedItems(List<RequestedItemQRO> requestedItems) {
		this.requestedItems = requestedItems;
		return this;
	}

	/**
	 * @return the custodian
	 */
	public CustomerRequestCustodianQRO getCustodian() {
		return custodian;
	}

	/**
	 * @param custodian the custodian to set
	 */
	public CustomerRequestQRO setCustodian(CustomerRequestCustodianQRO custodian) {
		this.custodian = custodian;
		return this;
	}

	/**
	 * @return the routeCode
	 */
	public String getRouteCode() {
		return routeCode;
	}

	/**
	 * @param routeCode the routeCode to set
	 */
	public CustomerRequestQRO setRouteCode(String routeCode) {
		this.routeCode = routeCode;
		return this;
	}

	/**
	 * @return the customer
	 */
	public CustomerAccountQRO getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public CustomerRequestQRO setCustomer(CustomerAccountQRO customer) {
		this.customer = customer;
		return this;
	}

	/**
	 * @return the requestType
	 */
	public String getRequestType() {
		return requestType;
	}

	/**
	 * @param requestType the requestType to set
	 */
	public CustomerRequestQRO setRequestType(String requestType) {
		this.requestType = requestType;
		return this;
	}
	
	
	
}
