package com.dhl.inventory.dto.queryobject.maintenance;

public class CourierQRO {
	
	private String plateNumber;
	
	private String type;
	
	private String facilityCode;
	
	public CourierQRO(){}

	/**
	 * @return the plateNumber
	 */
	public String getPlateNumber() {
		return plateNumber;
	}

	/**
	 * @param plateNumber the plateNumber to set
	 */
	public CourierQRO setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
		return this;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public CourierQRO setType(String type) {
		this.type = type;
		return this;
	}

	/**
	 * @return the facilityCode
	 */
	public String getFacilityCode() {
		return facilityCode;
	}

	/**
	 * @param facilityCode the facilityCode to set
	 */
	public CourierQRO setFacilityCode(String facilityCode) {
		this.facilityCode = facilityCode;
		return this;
	}
	
	
}
