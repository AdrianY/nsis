package com.dhl.inventory.dto.queryobject.warehouse.requestissuance;

import java.util.HashMap;

import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;

public class SuppliedItemsGroupResourceLQO extends AbstractSearchListQueryObject {
	
	@PathParam("requestNumber")
	private String requestNumber;
	
	@PathParam("itemCode")
	private String itemCode;
	
	@QueryParam("suggest")
	private boolean suggestAllocation;
	
	public SuppliedItemsGroupResourceLQO(){
		orderByMap = new HashMap<String, String>();
		defaultOrderField = "dateUpdated";
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public boolean isSuggestAllocation() {
		return suggestAllocation;
	}

	public void setSuggestAllocation(boolean suggestAllocation) {
		this.suggestAllocation = suggestAllocation;
	}

	public String getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}
}
