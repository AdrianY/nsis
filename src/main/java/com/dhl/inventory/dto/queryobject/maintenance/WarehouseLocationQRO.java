package com.dhl.inventory.dto.queryobject.maintenance;

public class WarehouseLocationQRO {
	private String rackingCode;
	
	private String facilityId;
	
	public WarehouseLocationQRO(){}

	public String getRackingCode() {
		return rackingCode;
	}

	public WarehouseLocationQRO setRackingCode(String rackingCode) {
		this.rackingCode = rackingCode;
		return this;
	}

	public String getFacilityId() {
		return facilityId;
	}

	public WarehouseLocationQRO setFacilityId(String facilityId) {
		this.facilityId = facilityId;
		return this;
	}
}
