package com.dhl.inventory.dto.queryobject.maintenance;


public class FacilityItemQRO {
	private String facilityId;
	
	private String codeName;
	
	private String description;
	
	private String unitOfMeasurement; 
	
	private int piecesPerUnitOfMeasurement;
	
	private String type;
	
	private int minimum;
	
	private int maximum;
	
	public FacilityItemQRO(String facilityId, String codeName){
		this.facilityId = facilityId;
		this.codeName = codeName;
	}

	public String getFacilityId() {
		return facilityId;
	}

	public void setFacilityId(String facilityId) {
		this.facilityId = facilityId;
	}

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUnitOfMeasurement() {
		return unitOfMeasurement;
	}

	public void setUnitOfMeasurement(String unitOfMeasurement) {
		this.unitOfMeasurement = unitOfMeasurement;
	}

	public int getPiecesPerUnitOfMeasurement() {
		return piecesPerUnitOfMeasurement;
	}

	public void setPiecesPerUnitOfMeasurement(int piecesPerUnitOfMeasurement) {
		this.piecesPerUnitOfMeasurement = piecesPerUnitOfMeasurement;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getMinimum() {
		return minimum;
	}

	public void setMinimum(int minimum) {
		this.minimum = minimum;
	}

	public int getMaximum() {
		return maximum;
	}

	public void setMaximum(int maximum) {
		this.maximum = maximum;
	}
}
