package com.dhl.inventory.dto.queryobject.maintenance;

public class CustomerAccountQRO {
	
	private String accountNumber;
	
	private String name;
	
	private String emailAddress;
	
	private String contactNumber;
	
	public CustomerAccountQRO(){}

	public String getName() {
		return name;
	}

	public CustomerAccountQRO setName(String name) {
		this.name = name;
		return this;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public CustomerAccountQRO setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
		return this;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public CustomerAccountQRO setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
		return this;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public CustomerAccountQRO setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
		return this;
	}
}
