package com.dhl.inventory.dto.queryobject.servicecenter.requestissuance;

public class AvailableSupplyItemsResourceQRO {
	
	private String batchNumber;
	
	private String rackNumber;
	
	private String dateDelivered;
	
	private String physicalStock;
	
	private String itemCost;
	
	private String totalCost;
	
	public AvailableSupplyItemsResourceQRO(){}

	public String getBatchNumber() {
		return batchNumber;
	}

	public AvailableSupplyItemsResourceQRO setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
		return this;
	}

	public String getRackNumber() {
		return rackNumber;
	}

	public AvailableSupplyItemsResourceQRO setRackNumber(String rackNumber) {
		this.rackNumber = rackNumber;
		return this;
	}

	public String getDateDelivered() {
		return dateDelivered;
	}

	public AvailableSupplyItemsResourceQRO setDateDelivered(String dateDelivered) {
		this.dateDelivered = dateDelivered;
		return this;
	}

	public String getPhysicalStock() {
		return physicalStock;
	}

	public AvailableSupplyItemsResourceQRO setPhysicalStock(String physicalStock) {
		this.physicalStock = physicalStock;
		return this;
	}

	public String getItemCost() {
		return itemCost;
	}

	public AvailableSupplyItemsResourceQRO setItemCost(String itemCost) {
		this.itemCost = itemCost;
		return this;
	}

	public String getTotalCost() {
		return totalCost;
	}

	public AvailableSupplyItemsResourceQRO setTotalCost(String totalCost) {
		this.totalCost = totalCost;
		return this;
	}
}
