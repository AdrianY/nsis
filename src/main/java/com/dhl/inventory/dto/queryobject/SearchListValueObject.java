package com.dhl.inventory.dto.queryobject;

import java.util.List;
import java.util.Map;

public class SearchListValueObject {
	private List<Map<String,String>> searchedInstances;
    private long totalCount;
    
    public SearchListValueObject(List<Map<String, String>> searchedInstances, long totalCount){
    	this.searchedInstances = searchedInstances;
    	this.totalCount = totalCount;
    }

	public List<Map<String, String>> getSearchedInstances() {
		return searchedInstances;
	}

	public long getTotalCount() {
		return totalCount;
	}
    
    
}
