package com.dhl.inventory.dto.queryobject.warehouse.requestissuance;

public class RequestIssuanceDeliveryReceiptItemQRO {
	
	String itemCode;

	String description;

	String qtyRequested;

	String qtyIssued;
	
	public RequestIssuanceDeliveryReceiptItemQRO(){}

	/**
	 * @return the itemCode
	 */
	public String getItemCode() {
		return itemCode;
	}

	/**
	 * @param itemCode the itemCode to set
	 */
	public RequestIssuanceDeliveryReceiptItemQRO setItemCode(String itemCode) {
		this.itemCode = itemCode;
		return this;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public RequestIssuanceDeliveryReceiptItemQRO setDescription(String description) {
		this.description = description;
		return this;
	}

	/**
	 * @return the qtyRequested
	 */
	public String getQtyRequested() {
		return qtyRequested;
	}

	/**
	 * @param qtyRequested the qtyRequested to set
	 */
	public RequestIssuanceDeliveryReceiptItemQRO setQtyRequested(String qtyRequested) {
		this.qtyRequested = qtyRequested;
		return this;
	}

	/**
	 * @return the qtyIssued
	 */
	public String getQtyIssued() {
		return qtyIssued;
	}

	/**
	 * @param qtyIssued the qtyIssued to set
	 */
	public RequestIssuanceDeliveryReceiptItemQRO setQtyIssued(String qtyIssued) {
		this.qtyIssued = qtyIssued;
		return this;
	}
}
