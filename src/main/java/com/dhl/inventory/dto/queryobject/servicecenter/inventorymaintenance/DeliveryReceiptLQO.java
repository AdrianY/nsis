package com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance;

import java.util.HashMap;

import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;

public class DeliveryReceiptLQO extends AbstractSearchListQueryObject{
	
	@PathParam("facilityCodeName")
	private String facilityCodeName;
	
	@QueryParam("status")
	private String status;
	
	@QueryParam("reqNumber")
	private String requestNumber;
	
	@QueryParam("toDate")
	private String toDate;
	
	@QueryParam("fromDate")
	private String fromDate;
	
	@QueryParam("cycle")
	private String cycle;
	
	public DeliveryReceiptLQO(){
		orderByMap = new HashMap<String, String>();
		orderByMap.put("status", "status");
		defaultOrderField = "dateUpdated";
	}

	public String getStatus() {
		return status;
	}

	public String getRequestNumber() {
		return requestNumber;
	}

	public String getFacilityCodeName() {
		return facilityCodeName;
	}

	public void setFacilityCodeName(String facilityCodeName) {
		this.facilityCodeName = facilityCodeName;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the cycle
	 */
	public String getCycle() {
		return cycle;
	}

	/**
	 * @param cycle the cycle to set
	 */
	public void setCycle(String cycle) {
		this.cycle = cycle;
	}
	
	
}
