package com.dhl.inventory.dto.queryobject;

public class PersonnelQRO {
	private String fullName;
	
	private String userName;
	
	private String contactNumber;
	
	private String emailAddress;
	
	public PersonnelQRO(){}

	public String getFullName() {
		return fullName;
	}

	public PersonnelQRO setFullName(String fullName) {
		this.fullName = fullName;
		return this;
	}

	public String getUserName() {
		return userName;
	}

	public PersonnelQRO setUserName(String userName) {
		this.userName = userName;
		return this;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public PersonnelQRO setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
		return this;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public PersonnelQRO setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
		return this;
	}
}
