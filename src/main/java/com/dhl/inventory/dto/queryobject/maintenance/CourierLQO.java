package com.dhl.inventory.dto.queryobject.maintenance;

import java.util.HashMap;

import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;

public class CourierLQO extends AbstractSearchListQueryObject {
	
	@PathParam("facilityCode")
	private String facilityCode;
	
	@QueryParam("plateNumber")
	private String plateNumber;
	
	@QueryParam("type")
	private String type;
	
	public CourierLQO(){
		orderByMap = new HashMap<>();
		orderByMap.put("plateNumber", "plateNumber");
		orderByMap.put("type", "type");
		defaultOrderField = "plateNumber";
	}

	/**
	 * @return the plateNumber
	 */
	public String getPlateNumber() {
		return plateNumber;
	}

	/**
	 * @param plateNumber the plateNumber to set
	 */
	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the facilityCode
	 */
	public String getFacilityCode() {
		return facilityCode;
	}

	/**
	 * @param facilityCode the facilityCode to set
	 */
	public void setFacilityCode(String facilityCode) {
		this.facilityCode = facilityCode;
	}
	
	
}
