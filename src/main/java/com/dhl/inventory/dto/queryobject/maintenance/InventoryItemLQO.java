package com.dhl.inventory.dto.queryobject.maintenance;

import java.util.HashMap;

import javax.ws.rs.QueryParam;

import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;

public class InventoryItemLQO extends AbstractSearchListQueryObject{
	
	@QueryParam("codeName")
	private String codeName;
	
	@QueryParam("type")
	private String type;
	
	@QueryParam("description")
	private String description;
	
	@QueryParam("piecesPerUnitOfMeaurement")
	private int piecesPerUnitOfMeaurement;
	
	@QueryParam("unitOfMeasurement")
	private String unitOfMeasurement;

	public InventoryItemLQO(){
		orderByMap = new HashMap<String, String>();
		orderByMap.put("type", "type");
		orderByMap.put("description", "description");
		orderByMap.put("codeName", "codeName");
		orderByMap.put("piecesPerUom", "piecesPerUnitOfMeasurement");
		orderByMap.put("uom", "unitOfMeasurement");
		
		defaultOrderField = "codeName";
	}

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPiecesPerUnitOfMeaurement() {
		return piecesPerUnitOfMeaurement;
	}

	public void setPiecesPerUnitOfMeaurement(int piecesPerUnitOfMeaurement) {
		this.piecesPerUnitOfMeaurement = piecesPerUnitOfMeaurement;
	}

	public String getUnitOfMeasurement() {
		return unitOfMeasurement;
	}

	public void setUnitOfMeasurement(String unitOfMeasurement) {
		this.unitOfMeasurement = unitOfMeasurement;
	}
}
