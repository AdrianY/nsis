package com.dhl.inventory.dto.queryobject;

import java.util.List;
import java.util.Map;

public class SearchList {
	private List<Map<String,String>> rows;
    private int page;
    private long records;
    private Double total;
    
    private SearchList(){}

    public static SearchList generateInstance(
            final SearchListValueObject valueObject,
            final AbstractSearchListQueryObject searchListCommandObject
    ){
    	SearchList instance =  new SearchList();
    	
    	instance.records = valueObject.getTotalCount();
    	instance.rows = valueObject.getSearchedInstances();
    	instance.total = Math.ceil(valueObject.getTotalCount()/searchListCommandObject.getMaxRows());
    	return instance;
    }

	public List<Map<String, String>> getRows() {
		return rows;
	}

	public int getPage() {
		return page;
	}

	public long getRecords() {
		return records;
	}

	public Double getTotal() {
		return total;
	}
    
    
}
