package com.dhl.inventory.dto.queryobject.customerinterfaces;

public class CustomerRequestCustodianQRO {
	
	private String remarks;
	
	private String name;
	
	public CustomerRequestCustodianQRO(){}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public CustomerRequestCustodianQRO setRemarks(String remarks) {
		this.remarks = remarks;
		return this;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public CustomerRequestCustodianQRO setName(String name) {
		this.name = name;
		return this;
	}

	
}
