package com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance;

import java.util.List;

import com.dhl.inventory.dto.queryobject.PersonnelQRO;

public class PhysicalStockCountRequestApprovalQRO {
	
	private PersonnelQRO approverDetails;
	
	private String remarks;
	
	private List<String> approvedBatchNumberCounts;
	
	public PhysicalStockCountRequestApprovalQRO(){}

	/**
	 * @return the approverDetails
	 */
	public PersonnelQRO getApproverDetails() {
		return approverDetails;
	}

	/**
	 * @param approverDetails the approverDetails to set
	 */
	public PhysicalStockCountRequestApprovalQRO setApproverDetails(PersonnelQRO approverDetails) {
		this.approverDetails = approverDetails;
		return this;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public PhysicalStockCountRequestApprovalQRO setRemarks(String remarks) {
		this.remarks = remarks;
		return this;
	}

	/**
	 * @return the approvedBatchNumberCounts
	 */
	public List<String> getApprovedBatchNumberCounts() {
		return approvedBatchNumberCounts;
	}

	/**
	 * @param approvedBatchNumberCounts the approvedBatchNumberCounts to set
	 */
	public PhysicalStockCountRequestApprovalQRO setApprovedBatchNumberCounts(List<String> approvedBatchNumberCounts) {
		this.approvedBatchNumberCounts = approvedBatchNumberCounts;
		return this;
	}
}
