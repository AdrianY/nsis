package com.dhl.inventory.dto.queryobject.warehouse.inventorymaintenance;

import java.util.HashMap;

import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;

public class OnHandByDeliveredItemBatchLQO extends AbstractSearchListQueryObject {
	
	@PathParam("facilityCode")
	private String facilityCode;
	
	@QueryParam("itemCode")
	private String itemCode;
	
	public OnHandByDeliveredItemBatchLQO(){
		orderByMap = new HashMap<>();
		defaultOrderField = "dateStored";
	}

	public String getFacilityCode() {
		return facilityCode;
	}

	public void setFacilityCode(String facilityCode) {
		this.facilityCode = facilityCode;
	}

	public String getItemCode() {
		return itemCode;
	}
	
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
}
