package com.dhl.inventory.dto.queryobject.maintenance;

import java.util.HashMap;

import javax.ws.rs.QueryParam;

import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;

public class CustomerAccountLQO extends AbstractSearchListQueryObject {
	@QueryParam("accountNumber")
	private String accountNumber;
	
	@QueryParam("name")
	private String name;
	
	public CustomerAccountLQO(){
		orderByMap = new HashMap<>();
		orderByMap.put("name", "name");
		orderByMap.put("accountNumber", "accountNumber");
		defaultOrderField = "name";
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
