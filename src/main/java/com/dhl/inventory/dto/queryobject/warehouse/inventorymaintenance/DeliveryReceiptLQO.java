package com.dhl.inventory.dto.queryobject.warehouse.inventorymaintenance;

import java.util.HashMap;

import javax.ws.rs.QueryParam;

import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;

public class DeliveryReceiptLQO extends AbstractSearchListQueryObject {
	
	@QueryParam("orderNumber")
	private String orderNumber;
	
	@QueryParam("status")
	private String status;
	
	public DeliveryReceiptLQO(){
		orderByMap = new HashMap<String, String>();
		orderByMap.put("orderRequest_.orderNumber", "orderNumber");
		orderByMap.put("status", "status");
		defaultOrderField = "dateUpdated";
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
