package com.dhl.inventory.dto.queryobject.maintenance;

import com.dhl.inventory.dto.queryobject.PersonnelQRO;

public class FacilityPersonnelQRO {
	private PersonnelQRO personnel;
	
	private String assignedFacilityAddress;
	
	private String facilityId;
	
	public FacilityPersonnelQRO(){}

	public PersonnelQRO getPersonnel() {
		return personnel;
	}

	public FacilityPersonnelQRO setPersonnel(PersonnelQRO personnel) {
		this.personnel = personnel;
		return this;
	}

	public String getAssignedFacilityAddress() {
		return assignedFacilityAddress;
	}

	public FacilityPersonnelQRO setAssignedFacilityAddress(String assignedFacilityAddress) {
		this.assignedFacilityAddress = assignedFacilityAddress;
		return this;
	}

	public String getFacilityId() {
		return facilityId;
	}

	public FacilityPersonnelQRO setFacilityId(String facilityId) {
		this.facilityId = facilityId;
		return this;
	}
	
}
