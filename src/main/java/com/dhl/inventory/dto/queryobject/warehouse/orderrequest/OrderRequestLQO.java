package com.dhl.inventory.dto.queryobject.warehouse.orderrequest;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.QueryParam;

import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;

public class OrderRequestLQO extends AbstractSearchListQueryObject {
	@QueryParam("ordNumber")
	private String orderNumber;
	
	@QueryParam("status")
	private String status;
	
	@QueryParam("statuses")
	private List<String> statuses;
	
	@QueryParam("toDate")
	private String toDate;
	
	@QueryParam("fromDate")
	private String fromDate;
	
	public OrderRequestLQO(){
		orderByMap = new HashMap<String, String>();
		orderByMap.put("orderNumber", "orderNumber");
		orderByMap.put("status", "status");
		defaultOrderField = "dateUpdated";
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getStatuses() {
		return statuses;
	}

	public void setStatuses(List<String> statuses) {
		this.statuses = statuses;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	
	
}
