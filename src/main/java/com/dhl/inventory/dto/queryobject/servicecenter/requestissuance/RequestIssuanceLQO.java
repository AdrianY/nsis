package com.dhl.inventory.dto.queryobject.servicecenter.requestissuance;

import java.util.HashMap;

import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;

public class RequestIssuanceLQO extends AbstractSearchListQueryObject {
	@QueryParam("requestType")
	private String requestType;
	
	@QueryParam("reqNumber")
	private String reqNumber;
	
	@QueryParam("accountNumber")
	private String accountNumber;
	
	@QueryParam("customerName")
	private String customerName;
	
	@PathParam("facilityCodeName")
	private String facilityCodeName;
	
	@QueryParam("status")
	private String status;
	
	@QueryParam("toDate")
	private String toDate;
	
	@QueryParam("fromDate")
	private String fromDate;
	
	public RequestIssuanceLQO(){
		orderByMap = new HashMap<String, String>();
		orderByMap.put("requestNumber", "aa_.request_number");
		orderByMap.put("accountNumber", "aa_.account_number");
		orderByMap.put("customerName", "aa_.customer_name");
		orderByMap.put("requestType", "aa_.request_type");
		orderByMap.put("requestDate", "aa_.request_date");
		orderByMap.put("status", "aa_.issuance_status");
		defaultOrderField = "aa_.request_number";
	}

	public String getReqNumber() {
		return reqNumber;
	}

	public void setReqNumber(String reqNumber) {
		this.reqNumber = reqNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFacilityCodeName() {
		return facilityCodeName;
	}

	public void setFacilityCodeName(String facilityCodeName) {
		this.facilityCodeName = facilityCodeName;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the requestType
	 */
	public String getRequestType() {
		return requestType;
	}

	/**
	 * @param requestType the requestType to set
	 */
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	
	
}
