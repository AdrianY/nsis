package com.dhl.inventory.dto.queryobject.warehouse.orderrequest;

import com.dhl.inventory.dto.queryobject.PersonnelQRO;

public class OrderRequestApproverQRO {
	
	private PersonnelQRO approverDetails;
	
	private String remarks;
	
	public OrderRequestApproverQRO(){}

	public PersonnelQRO getApproverDetails() {
		return approverDetails;
	}

	public OrderRequestApproverQRO setApproverDetails(PersonnelQRO approver) {
		this.approverDetails = approver;
		return this;
	}

	public String getRemarks() {
		return remarks;
	}

	public OrderRequestApproverQRO setRemarks(String remarks) {
		this.remarks = remarks;
		return this;
	}
}
