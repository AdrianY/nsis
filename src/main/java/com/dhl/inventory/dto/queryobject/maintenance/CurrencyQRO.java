package com.dhl.inventory.dto.queryobject.maintenance;

public class CurrencyQRO {
	private String code;
	
	private String exchangeRateToPhp;
	
	public CurrencyQRO(String code, String exchangeRateToPhp){
		this.code = code;
		this.exchangeRateToPhp = exchangeRateToPhp;
	}

	public String getCode() {
		return code;
	}

	public String getExchangeRateToPhp() {
		return exchangeRateToPhp;
	}
}
