package com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance;

import java.util.List;

import com.dhl.inventory.dto.queryobject.PersonnelQRO;

public class PhysicalStockCountRequestQRO {
	
	private String status;
	
	private String requestNumber;
	
	private PersonnelQRO requestBy;
	
	private String dateRequested;
	
	private String facilityCodeName;
	
	private PhysicalStockCountRequestApprovalQRO approvalDetails;
	
	private List<PhysicalStockCountQRO> physicalCountList;
	
	public PhysicalStockCountRequestQRO(){}

	/**
	 * @return the requestNumber
	 */
	public String getRequestNumber() {
		return requestNumber;
	}

	/**
	 * @param requestNumber the requestNumber to set
	 */
	public PhysicalStockCountRequestQRO setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
		return this;
	}

	/**
	 * @return the requestBy
	 */
	public PersonnelQRO getRequestBy() {
		return requestBy;
	}

	/**
	 * @param requestBy the requestBy to set
	 */
	public PhysicalStockCountRequestQRO setRequestBy(PersonnelQRO requestBy) {
		this.requestBy = requestBy;
		return this;
	}

	/**
	 * @return the dateRequested
	 */
	public String getDateRequested() {
		return dateRequested;
	}

	/**
	 * @param dateRequested the dateRequested to set
	 */
	public PhysicalStockCountRequestQRO setDateRequested(String dateRequested) {
		this.dateRequested = dateRequested;
		return this;
	}

	/**
	 * @return the physicalCountList
	 */
	public List<PhysicalStockCountQRO> getPhysicalCountList() {
		return physicalCountList;
	}

	/**
	 * @param physicalCountList the physicalCountList to set
	 */
	public PhysicalStockCountRequestQRO setPhysicalCountList(List<PhysicalStockCountQRO> physicalCountList) {
		this.physicalCountList = physicalCountList;
		return this;
	}

	/**
	 * @return the approvalDetails
	 */
	public PhysicalStockCountRequestApprovalQRO getApprovalDetails() {
		return approvalDetails;
	}

	/**
	 * @param approvalDetails the approvalDetails to set
	 */
	public PhysicalStockCountRequestQRO setApprovalDetails(PhysicalStockCountRequestApprovalQRO approvalDetails) {
		this.approvalDetails = approvalDetails;
		return this;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public PhysicalStockCountRequestQRO setStatus(String status) {
		this.status = status;
		return this;
	}

	/**
	 * @return the facilityCodeName
	 */
	public String getFacilityCodeName() {
		return facilityCodeName;
	}

	/**
	 * @param facilityCodeName the facilityCodeName to set
	 */
	public PhysicalStockCountRequestQRO setFacilityCodeName(String facilityCodeName) {
		this.facilityCodeName = facilityCodeName;
		return this;
	}
	
	
}
