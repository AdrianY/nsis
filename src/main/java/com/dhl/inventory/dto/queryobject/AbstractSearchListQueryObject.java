package com.dhl.inventory.dto.queryobject;

import java.util.Map;

import javax.ws.rs.QueryParam;

public abstract class AbstractSearchListQueryObject {
	
	private static final int DEFAULT_MAX_ROWS = 10;
	private static final int DEFAULT_ROW_OFFSET = 0;
	public static final String SORT_ORDER_ASC = "asc";
	public static final String SORT_ORDER_DESC = "desc";
	
	protected Map<String, String> orderByMap;
    protected String defaultOrderField;
    
    @QueryParam("sord")
    protected String sord;
    
	@QueryParam("sidx")
    protected String sidx;
    
	@QueryParam("rows")
    protected int rows;
	
	@QueryParam("rowOffset")
    protected int rowOffset;
	
	@QueryParam("searchType")
	protected String searchType;
    
    public AbstractSearchListQueryObject(){}
    
    public String getSortOrder(){
        return (null !=  sord && !sord.isEmpty()) && (!SORT_ORDER_ASC.equals(sord) || !SORT_ORDER_DESC.equals(sord)) ? sord : SORT_ORDER_ASC;
    }

    public String getSortField(){
    	String sortField = orderByMap.containsKey(sidx) ? orderByMap.get(sidx) : defaultOrderField;
        return sortField;
    }

    public int getMaxRows(){
    	int maxRows = 0 == rows ? DEFAULT_MAX_ROWS : rows;	
        return maxRows;
    }

    public int getRowOffset(){
        return rowOffset <= DEFAULT_ROW_OFFSET ? DEFAULT_ROW_OFFSET : rowOffset;
    }

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public void setRowOffset(int rowOffset) {
		this.rowOffset = rowOffset;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getSearchType() {
		return searchType;
	}
	
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}
}
