package com.dhl.inventory.dto.queryobject;

public class RecipientQRO {
	private String deliveryAddress;

	private PersonnelQRO recipientDetails;
	
	public RecipientQRO(){}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public RecipientQRO setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
		return this;
	}

	public PersonnelQRO getRecipientDetails() {
		return recipientDetails;
	}

	public RecipientQRO setRecipientDetails(PersonnelQRO recipient) {
		this.recipientDetails = recipient;
		return this;
	}
}
