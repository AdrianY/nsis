package com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance;


public class DeliveredItemBatchQRO {
	private String batchNumber;
	
	private String rackingCode;
	
	private String dateStored;
	
	private int quantity;
	
	public DeliveredItemBatchQRO(){}

	public String getRackingCode() {
		return rackingCode;
	}

	public DeliveredItemBatchQRO setRackingCode(String rackingCode) {
		this.rackingCode = rackingCode;
		return this;
	}

	public String getDateStored() {
		return dateStored;
	}

	public DeliveredItemBatchQRO setDateStored(String dateStored) {
		this.dateStored = dateStored;
		return this;
	}

	public int getQuantity() {
		return quantity;
	}

	public DeliveredItemBatchQRO setQuantity(int quantity) {
		this.quantity = quantity;
		return this;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public DeliveredItemBatchQRO setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
		return this;
	}
}
