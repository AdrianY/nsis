package com.dhl.inventory.dto.queryobject.servicecenter.supplyrequest;

import java.util.ArrayList;
import java.util.List;

import com.dhl.inventory.dto.queryobject.PersonnelQRO;
import com.dhl.inventory.dto.queryobject.RecipientQRO;

public class SupplyRequestQRO {
	private String requestNumber;
	
	private String facilityCodeName;

	private String requestDate;
	
	private PersonnelQRO requestBy;
	
	private String currency;
	
	private String issuanceDate;
	
	private String status;
	
	private String requestCycle;
	
	private SupplyRequestApproverQRO approver;
	
	private RecipientQRO recipient;
	
	private ArrayList<SupplyRequestItemQRO> itemsRequested;

	public SupplyRequestQRO(){
		itemsRequested = new ArrayList<SupplyRequestItemQRO>();
	}

	public String getRequestNumber() {
		return requestNumber;
	}

	public SupplyRequestQRO setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
		return this;
	}
	
	public String getRequestDate() {
		return requestDate;
	}

	public SupplyRequestQRO setRequestDate(String requestDate) {
		this.requestDate = requestDate;
		return this;
	}

	public PersonnelQRO getRequestBy() {
		return requestBy;
	}

	public SupplyRequestQRO setRequestBy(PersonnelQRO requestBy) {
		this.requestBy = requestBy;
		return this;
	}

	public String getCurrency() {
		return currency;
	}

	public SupplyRequestQRO setCurrency(String currency) {
		this.currency = currency;
		return this;
	}
	
	public String getIssuanceDate() {
		return issuanceDate;
	}

	public SupplyRequestQRO setIssuanceDate(String issuanceDate) {
		this.issuanceDate = issuanceDate;
		return this;
	}

	public SupplyRequestApproverQRO getApprover() {
		return approver;
	}

	public SupplyRequestQRO setApprover(SupplyRequestApproverQRO approver) {
		this.approver = approver;
		return this;
	}

	public RecipientQRO getRecipient() {
		return recipient;
	}

	public SupplyRequestQRO setRecipient(RecipientQRO recipient) {
		this.recipient = recipient;
		return this;
	}

	public List<SupplyRequestItemQRO> getItemsRequested() {
		return itemsRequested;
	}

	public SupplyRequestQRO setItemsRequested(ArrayList<SupplyRequestItemQRO> itemsRequested) {
		this.itemsRequested = itemsRequested;
		return this;
	}

	public String getFacilityCodeName() {
		return facilityCodeName;
	}

	public SupplyRequestQRO setFacilityCodeName(String facilityCodeName) {
		this.facilityCodeName = facilityCodeName;
		return this;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the requestCycle
	 */
	public String getRequestCycle() {
		return requestCycle;
	}

	/**
	 * @param requestCycle the requestCycle to set
	 */
	public SupplyRequestQRO setRequestCycle(String requestCycle) {
		this.requestCycle = requestCycle;
		return this;
	}
}
