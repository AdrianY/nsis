package com.dhl.inventory.dto.queryobject.maintenance;

import java.util.HashMap;

import javax.ws.rs.PathParam;

import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;

public class RouteLQO extends AbstractSearchListQueryObject {
	
	@PathParam("facilityCodeName")
	private String facilityCode;
	
	public RouteLQO(){
		orderByMap = new HashMap<String, String>();
		defaultOrderField = "routeCode";
	}

	/**
	 * @return the facilityCode
	 */
	public String getFacilityCode() {
		return facilityCode;
	}

	/**
	 * @param facilityCode the facilityCode to set
	 */
	public void setFacilityCode(String facilityCode) {
		this.facilityCode = facilityCode;
	}
	
	
}
