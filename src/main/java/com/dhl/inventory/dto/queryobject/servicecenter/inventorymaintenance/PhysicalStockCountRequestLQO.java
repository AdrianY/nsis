package com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.QueryParam;

import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;

public class PhysicalStockCountRequestLQO extends AbstractSearchListQueryObject{
	
	@QueryParam("requestNumber")
	private String requestNumber;
	
	@QueryParam("status")
	private String status;
	
	@QueryParam("facilityCode")
	private String facilityCode;
	
	@QueryParam("statuses")
	private List<String> statuses;
	
	@QueryParam("showFacilityCode")
	private boolean showFacilityCode;
	
	public PhysicalStockCountRequestLQO(){
		orderByMap = new HashMap<>();
		defaultOrderField = "dateUpdated";
	}

	/**
	 * @return the requestNumber
	 */
	public String getRequestNumber() {
		return requestNumber;
	}

	/**
	 * @param requestNumber the requestNumber to set
	 */
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the facilityCode
	 */
	public String getFacilityCode() {
		return facilityCode;
	}

	/**
	 * @param facilityCode the facilityCode to set
	 */
	public void setFacilityCode(String facilityCode) {
		this.facilityCode = facilityCode;
	}

	/**
	 * @return the statuses
	 */
	public List<String> getStatuses() {
		return statuses;
	}

	/**
	 * @param statuses the statuses to set
	 */
	public void setStatuses(List<String> statuses) {
		this.statuses = statuses;
	}

	/**
	 * @return the showFacilityCode
	 */
	public boolean isShowFacilityCode() {
		return showFacilityCode;
	}

	/**
	 * @param showFacilityCode the showFacilityCode to set
	 */
	public void setShowFacilityCode(boolean showFacilityCode) {
		this.showFacilityCode = showFacilityCode;
	}
	
	
}
