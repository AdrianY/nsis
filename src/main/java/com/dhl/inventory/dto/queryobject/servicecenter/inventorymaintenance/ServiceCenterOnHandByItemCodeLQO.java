package com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance;

import java.util.HashMap;

import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;

public class ServiceCenterOnHandByItemCodeLQO extends AbstractSearchListQueryObject {
	
	@PathParam("facilityCodeName")
	private String facilityCode;
	
	@QueryParam("itemCode")
	private String itemCode;
	
	@QueryParam("byItemCode")
	private boolean byItemCode;
	
	public ServiceCenterOnHandByItemCodeLQO(){
		orderByMap = new HashMap<>();
		defaultOrderField = "codeName";
	}
	
	public String getFacilityCode() {
		return facilityCode;
	}

	public void setFacilityCode(String facilityCode) {
		this.facilityCode = facilityCode;
	}

	public String getItemCode() {
		return itemCode;
	}
	
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public boolean isByItemCode() {
		return byItemCode;
	}

	public void setByItemCode(boolean byItemCode) {
		this.byItemCode = byItemCode;
	}
}
