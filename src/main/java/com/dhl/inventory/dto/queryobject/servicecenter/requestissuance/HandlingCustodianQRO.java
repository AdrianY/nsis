package com.dhl.inventory.dto.queryobject.servicecenter.requestissuance;

import com.dhl.inventory.dto.queryobject.PersonnelQRO;

public class HandlingCustodianQRO {
private String remarks;
	
	private PersonnelQRO custodianDetails;
	
	public HandlingCustodianQRO(){}

	public String getRemarks() {
		return remarks;
	}

	public HandlingCustodianQRO setRemarks(String remarks) {
		this.remarks = remarks;
		return this;
	}

	public PersonnelQRO getCustodianDetails() {
		return custodianDetails;
	}

	public HandlingCustodianQRO setCustodianDetails(PersonnelQRO custodianDetails) {
		this.custodianDetails = custodianDetails;
		return this;
	}
}
