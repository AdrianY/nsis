package com.dhl.inventory.dto.queryobject.servicecenter.requestissuance;

import java.util.List;

public class AvailableSupplyItemsGroupResourceQRO {
	private String physicalStock;
	
	private String logicalStock;
	
	private List<AvailableSupplyItemsResourceQRO> availableSupplies;
	
	public AvailableSupplyItemsGroupResourceQRO(){}

	public String getPhysicalStock() {
		return physicalStock;
	}

	public AvailableSupplyItemsGroupResourceQRO setPhysicalStock(String physicalStock) {
		this.physicalStock = physicalStock;
		return this;
	}

	public String getLogicalStock() {
		return logicalStock;
	}

	public AvailableSupplyItemsGroupResourceQRO setLogicalStock(String logicalStock) {
		this.logicalStock = logicalStock;
		return this;
	}

	public List<AvailableSupplyItemsResourceQRO> getAvailableSupplies() {
		return availableSupplies;
	}

	public AvailableSupplyItemsGroupResourceQRO setAvailableSupplies(List<AvailableSupplyItemsResourceQRO> availableSupplies) {
		this.availableSupplies = availableSupplies;
		return this;
	}
}
