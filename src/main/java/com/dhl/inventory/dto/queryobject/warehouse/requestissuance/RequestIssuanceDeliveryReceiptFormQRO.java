package com.dhl.inventory.dto.queryobject.warehouse.requestissuance;

import java.util.List;

public class RequestIssuanceDeliveryReceiptFormQRO {
	
	String facilityCode;

	String requestNumber;

	String requestorName;

	String requestedDate;

	String requiredDeliveryDate;

	String dateIssued;
	
	List<RequestIssuanceDeliveryReceiptItemQRO> requestedItems;
	
	public RequestIssuanceDeliveryReceiptFormQRO(){}

	/**
	 * @return the facilityCode
	 */
	public String getFacilityCode() {
		return facilityCode;
	}

	/**
	 * @param facilityCode the facilityCode to set
	 */
	public RequestIssuanceDeliveryReceiptFormQRO setFacilityCode(String facilityCode) {
		this.facilityCode = facilityCode;
		return this;
	}

	/**
	 * @return the requestNumber
	 */
	public String getRequestNumber() {
		return requestNumber;
	}

	/**
	 * @param requestNumber the requestNumber to set
	 */
	public RequestIssuanceDeliveryReceiptFormQRO setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
		return this;
	}

	/**
	 * @return the requestorName
	 */
	public String getRequestorName() {
		return requestorName;
	}

	/**
	 * @param requestorName the requestorName to set
	 */
	public RequestIssuanceDeliveryReceiptFormQRO setRequestorName(String requestorName) {
		this.requestorName = requestorName;
		return this;
	}

	/**
	 * @return the requestedDate
	 */
	public String getRequestedDate() {
		return requestedDate;
	}

	/**
	 * @param requestedDate the requestedDate to set
	 */
	public RequestIssuanceDeliveryReceiptFormQRO setRequestedDate(String requestedDate) {
		this.requestedDate = requestedDate;
		return this;
	}

	/**
	 * @return the requiredDeliveryDate
	 */
	public String getRequiredDeliveryDate() {
		return requiredDeliveryDate;
	}

	/**
	 * @param requiredDeliveryDate the requiredDeliveryDate to set
	 */
	public RequestIssuanceDeliveryReceiptFormQRO setRequiredDeliveryDate(String requiredDeliveryDate) {
		this.requiredDeliveryDate = requiredDeliveryDate;
		return this;
	}

	/**
	 * @return the dateIssued
	 */
	public String getDateIssued() {
		return dateIssued;
	}

	/**
	 * @param dateIssued the dateIssued to set
	 */
	public RequestIssuanceDeliveryReceiptFormQRO setDateIssued(String dateIssued) {
		this.dateIssued = dateIssued;
		return this;
	}

	/**
	 * @return the requestedItems
	 */
	public List<RequestIssuanceDeliveryReceiptItemQRO> getRequestedItems() {
		return requestedItems;
	}

	/**
	 * @param requestedItems the requestedItems to set
	 */
	public RequestIssuanceDeliveryReceiptFormQRO setRequestedItems(List<RequestIssuanceDeliveryReceiptItemQRO> requestedItems) {
		this.requestedItems = requestedItems;
		return this;
	}

}
