package com.dhl.inventory.dto.queryobject.servicecenter.requestissuance;

import java.util.List;

public class RequestIssuanceDeliveryReceiptFormQRO {

	String requestNumber;

	String requestedDate;

	String requiredDeliveryDate;

	String dateIssued;
	
	String customerName;
	
	String deliveryAddress;
	
	String contactNumber;
	
	String accountNumber;
	
	List<RequestIssuanceDeliveryReceiptItemQRO> requestedItems;
	
	public RequestIssuanceDeliveryReceiptFormQRO(){}

	/**
	 * @return the requestNumber
	 */
	public String getRequestNumber() {
		return requestNumber;
	}

	/**
	 * @param requestNumber the requestNumber to set
	 */
	public RequestIssuanceDeliveryReceiptFormQRO setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
		return this;
	}

	/**
	 * @return the requestedDate
	 */
	public String getRequestedDate() {
		return requestedDate;
	}

	/**
	 * @param requestedDate the requestedDate to set
	 */
	public RequestIssuanceDeliveryReceiptFormQRO setRequestedDate(String requestedDate) {
		this.requestedDate = requestedDate;
		return this;
	}

	/**
	 * @return the requiredDeliveryDate
	 */
	public String getRequiredDeliveryDate() {
		return requiredDeliveryDate;
	}

	/**
	 * @param requiredDeliveryDate the requiredDeliveryDate to set
	 */
	public RequestIssuanceDeliveryReceiptFormQRO setRequiredDeliveryDate(String requiredDeliveryDate) {
		this.requiredDeliveryDate = requiredDeliveryDate;
		return this;
	}

	/**
	 * @return the dateIssued
	 */
	public String getDateIssued() {
		return dateIssued;
	}

	/**
	 * @param dateIssued the dateIssued to set
	 */
	public RequestIssuanceDeliveryReceiptFormQRO setDateIssued(String dateIssued) {
		this.dateIssued = dateIssued;
		return this;
	}

	/**
	 * @return the requestedItems
	 */
	public List<RequestIssuanceDeliveryReceiptItemQRO> getRequestedItems() {
		return requestedItems;
	}

	/**
	 * @param requestedItems the requestedItems to set
	 */
	public RequestIssuanceDeliveryReceiptFormQRO setRequestedItems(List<RequestIssuanceDeliveryReceiptItemQRO> requestedItems) {
		this.requestedItems = requestedItems;
		return this;
	}
	
	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public RequestIssuanceDeliveryReceiptFormQRO setCustomerName(String customerName) {
		this.customerName = customerName;
		return this;
	}

	/**
	 * @return the deliveryAddress
	 */
	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	/**
	 * @param deliveryAddress the deliveryAddress to set
	 */
	public RequestIssuanceDeliveryReceiptFormQRO setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
		return this;
	}

	/**
	 * @return the contactNumber
	 */
	public String getContactNumber() {
		return contactNumber;
	}

	/**
	 * @param contactNumber the contactNumber to set
	 */
	public RequestIssuanceDeliveryReceiptFormQRO setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
		return this;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public RequestIssuanceDeliveryReceiptFormQRO setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
		return this;
	}
}
