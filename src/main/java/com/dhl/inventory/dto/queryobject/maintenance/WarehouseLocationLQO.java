package com.dhl.inventory.dto.queryobject.maintenance;

import java.util.HashMap;

import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;

public class WarehouseLocationLQO extends AbstractSearchListQueryObject {
	
	@PathParam("facilityCode")
	private String facilityCode;
	
	@QueryParam("rackCode")
	private String rackCode;
	
	public WarehouseLocationLQO(){orderByMap = new HashMap<String, String>();}

	public String getFacilityCode() {
		return facilityCode;
	}

	public void setFacilityCode(String facilityCode) {
		this.facilityCode = facilityCode;
	}

	public String getRackCode() {
		return rackCode;
	}

	public void setRackCode(String rackCode) {
		this.rackCode = rackCode;
	}
}
