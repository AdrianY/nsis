package com.dhl.inventory.dto.queryobject.customerinterfaces;

public class CustomerRequestCourierQRO {
	private String type;
	
	private String plateNumber;
	
	public CustomerRequestCourierQRO(){}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public CustomerRequestCourierQRO setType(String type) {
		this.type = type;
		return this;
	}

	/**
	 * @return the plateNumber
	 */
	public String getPlateNumber() {
		return plateNumber;
	}

	/**
	 * @param plateNumber the plateNumber to set
	 */
	public CustomerRequestCourierQRO setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
		return this;
	}
	
	
}
