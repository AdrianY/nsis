package com.dhl.inventory.dto.queryobject.warehouse.orderrequest;

import java.util.ArrayList;
import java.util.List;

import com.dhl.inventory.dto.queryobject.PersonnelQRO;

public class OrderRequestQRO {
	
	private String orderNumber;
	
	private String facilityId;

	private String orderType;
	
	private String orderDate;
	
	private PersonnelQRO orderBy;
	
	private String orderCycle;
	
	private String currency;
	
	private String deliveryDate;
	
	private String status;
	
	private OrderRequestApproverQRO approver;
	
	private OrderRequestRecipientQRO recipient;
	
	private ArrayList<OrderRequestItemQRO> itemsRequested;

	public OrderRequestQRO(){
		itemsRequested = new ArrayList<OrderRequestItemQRO>();
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public OrderRequestQRO setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
		return this;
	}
	
	public String getOrderDate() {
		return orderDate;
	}

	public OrderRequestQRO setOrderDate(String orderDate) {
		this.orderDate = orderDate;
		return this;
	}

	public String getOrderType() {
		return orderType;
	}

	public OrderRequestQRO setOrderType(String orderType) {
		this.orderType = orderType;
		return this;
	}

	public PersonnelQRO getOrderBy() {
		return orderBy;
	}

	public OrderRequestQRO setOrderBy(PersonnelQRO orderBy) {
		this.orderBy = orderBy;
		return this;
	}

	public String getOrderCycle() {
		return orderCycle;
	}

	public OrderRequestQRO setOrderCycle(String orderCycle) {
		this.orderCycle = orderCycle;
		return this;
	}

	public String getCurrency() {
		return currency;
	}

	public OrderRequestQRO setCurrency(String currency) {
		this.currency = currency;
		return this;
	}
	
	public String getDeliveryDate() {
		return deliveryDate;
	}

	public OrderRequestQRO setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
		return this;
	}

	public OrderRequestApproverQRO getApprover() {
		return approver;
	}

	public OrderRequestQRO setApprover(OrderRequestApproverQRO approver) {
		this.approver = approver;
		return this;
	}

	public OrderRequestRecipientQRO getRecipient() {
		return recipient;
	}

	public OrderRequestQRO setRecipient(OrderRequestRecipientQRO recipient) {
		this.recipient = recipient;
		return this;
	}

	public List<OrderRequestItemQRO> getItemsRequested() {
		return itemsRequested;
	}

	public OrderRequestQRO setItemsRequested(ArrayList<OrderRequestItemQRO> itemsRequested) {
		this.itemsRequested = itemsRequested;
		return this;
	}

	public String getFacilityId() {
		return facilityId;
	}

	public OrderRequestQRO setFacilityId(String facilityId) {
		this.facilityId = facilityId;
		return this;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
