package com.dhl.inventory.dto.queryobject.servicecenter.supplyrequest;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;

public class SupplyRequestLQO extends AbstractSearchListQueryObject {

	@PathParam("facilityCodeName")
	private String facilityCodeName;
	
	@QueryParam("reqNumber")
	private String requestNumber;
	
	@QueryParam("status")
	private String status;
	
	@QueryParam("facilityCodes")
	private List<String> facilityCodes;
	
	@QueryParam("statuses")
	private List<String> statuses;
	
	@QueryParam("toDate")
	private String toDate;
	
	@QueryParam("fromDate")
	private String fromDate;
	
	@QueryParam("submittedList")
	private boolean submittedList;
	
	public SupplyRequestLQO(){
		orderByMap = new HashMap<String, String>();
		orderByMap.put("requestNumber", "requestNumber");
		orderByMap.put("status", "status");
		orderByMap.put("requestDate", "requestDate");
		defaultOrderField = "dateUpdated";
	}

	public String getFacilityCodeName() {
		return facilityCodeName;
	}

	public void setFacilityCodeName(String facilityCodeName) {
		this.facilityCodeName = facilityCodeName;
	}

	public String getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getStatuses() {
		return statuses;
	}

	public void setStatuses(List<String> statuses) {
		this.statuses = statuses;
	}

	public List<String> getFacilityCodes() {
		return facilityCodes;
	}

	public void setFacilityCodes(List<String> facilityCodes) {
		this.facilityCodes = facilityCodes;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the submittedList
	 */
	public boolean isSubmittedList() {
		return submittedList;
	}

	/**
	 * @param submittedList the submittedList to set
	 */
	public void setSubmittedList(boolean submittedList) {
		this.submittedList = submittedList;
	}
	
	
}
