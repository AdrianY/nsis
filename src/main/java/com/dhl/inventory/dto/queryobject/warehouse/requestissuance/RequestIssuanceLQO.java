package com.dhl.inventory.dto.queryobject.warehouse.requestissuance;

import java.util.HashMap;

import javax.ws.rs.QueryParam;

import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;

public class RequestIssuanceLQO extends AbstractSearchListQueryObject {
	
	@QueryParam("requestNumber")
	private String requestNumber;
	
	@QueryParam("facilityCodeName")
	private String facilityCodeName;
	
	@QueryParam("status")
	private String status;
	
	public RequestIssuanceLQO(){
		orderByMap = new HashMap<String, String>();
		orderByMap.put("requestNumber", "referenceNumber");
		orderByMap.put("facilityCodeName", "facility_.codeName");
		orderByMap.put("status", "status");
		defaultOrderField = "dateUpdated";
	}

	public String getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFacilityCodeName() {
		return facilityCodeName;
	}

	public void setFacilityCodeName(String facilityCodeName) {
		this.facilityCodeName = facilityCodeName;
	}
}
