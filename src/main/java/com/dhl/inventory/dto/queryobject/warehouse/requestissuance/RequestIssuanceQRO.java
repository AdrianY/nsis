package com.dhl.inventory.dto.queryobject.warehouse.requestissuance;

import java.util.ArrayList;

import com.dhl.inventory.dto.queryobject.PersonnelQRO;
import com.dhl.inventory.dto.queryobject.RecipientQRO;

public class RequestIssuanceQRO {
	private String requestNumber;
	
	private String status;
	
	private String requestDate;
	
	/**
	 * required delivery date
	 */
	private String issuanceDate;
	
	private PersonnelQRO requestorDetails;
	
	private String currency;
	
	private RecipientQRO recipient;
	
	private String facilityCodeName;
	
	private HandlingCustodianQRO handlingCustodian;
	
	private ArrayList<RequestedItemQRO> requestedItems;
	
	public RequestIssuanceQRO(){}

	public String getRequestNumber() {
		return requestNumber;
	}

	public RequestIssuanceQRO setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
		return this;
	}

	public String getStatus() {
		return status;
	}

	public RequestIssuanceQRO setStatus(String status) {
		this.status = status;
		return this;
	}

	public String getRequestDate() {
		return requestDate;
	}

	public RequestIssuanceQRO setRequestDate(String requestDate) {
		this.requestDate = requestDate;
		return this;
	}

	public String getIssuanceDate() {
		return issuanceDate;
	}

	public RequestIssuanceQRO setIssuanceDate(String issuanceDate) {
		this.issuanceDate = issuanceDate;
		return this;
	}

	public PersonnelQRO getRequestorDetails() {
		return requestorDetails;
	}

	public RequestIssuanceQRO setRequestorDetails(PersonnelQRO requestorDetails) {
		this.requestorDetails = requestorDetails;
		return this;
	}

	public String getCurrency() {
		return currency;
	}

	public RequestIssuanceQRO setCurrency(String currency) {
		this.currency = currency;
		return this;
	}

	public RecipientQRO getRecipient() {
		return recipient;
	}

	public RequestIssuanceQRO setRecipient(RecipientQRO recipient) {
		this.recipient = recipient;
		return this;
	}

	public String getFacilityCodeName() {
		return facilityCodeName;
	}

	public RequestIssuanceQRO setFacilityCodeName(String facilityCodeName) {
		this.facilityCodeName = facilityCodeName;
		return this;
	}

	public HandlingCustodianQRO getHandlingCustodian() {
		return handlingCustodian;
	}

	public RequestIssuanceQRO setHandlingCustodian(HandlingCustodianQRO handlingCustodian) {
		this.handlingCustodian = handlingCustodian;
		return this;
	}

	public ArrayList<RequestedItemQRO> getRequestedItems() {
		return requestedItems;
	}

	public RequestIssuanceQRO setRequestedItems(ArrayList<RequestedItemQRO> requestedItems) {
		this.requestedItems = requestedItems;
		return this;
	}
}
