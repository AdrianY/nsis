package com.dhl.inventory.dto.queryobject.warehouse.inventorymaintenance;

import java.util.List;
import java.util.Map;

import com.dhl.inventory.dto.queryobject.warehouse.orderrequest.OrderRequestQRO;

public class DeliveryReceiptQRO {
	private OrderRequestQRO orderRequest;
	
	private String status;
	
	private String dateDelivered;
	
	private Map<String, List<DeliveredItemBatchQRO>> itemBatches;
	
	public DeliveryReceiptQRO(){}

	public OrderRequestQRO getOrderRequest() {
		return orderRequest;
	}

	public DeliveryReceiptQRO setOrderRequest(OrderRequestQRO orderRequest) {
		this.orderRequest = orderRequest;
		return this;
	}

	public String getStatus() {
		return status;
	}

	public DeliveryReceiptQRO setStatus(String status) {
		this.status = status;
		return this;
	}

	public String getDateDelivered() {
		return dateDelivered;
	}

	public DeliveryReceiptQRO setDateDelivered(String dateDelivered) {
		this.dateDelivered = dateDelivered;
		return this;
	}

	public Map<String, List<DeliveredItemBatchQRO>> getItemBatches() {
		return itemBatches;
	}

	public void setItemBatches(Map<String, List<DeliveredItemBatchQRO>> itemBatches) {
		this.itemBatches = itemBatches;
	}
}
