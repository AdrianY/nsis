package com.dhl.inventory.dto.queryobject.servicecenter.requestissuance;

import java.util.List;

import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.PickListItemGroupQRO;

public class RequestIssuancePickListQRO {
	
	String facilityCode;
	
	String routeCode;
	
	String customerName;

	String requestNumber;

	String requestorName;

	String requestedDate;

	String requiredDeliveryDate;

	String dateIssued;

	List<PickListItemGroupQRO> requestedItems;
	
	public RequestIssuancePickListQRO(){}

	/**
	 * @return the routeCode
	 */
	public String getRouteCode() {
		return routeCode;
	}


	/**
	 * @param routeCode the routeCode to set
	 */
	public RequestIssuancePickListQRO setRouteCode(String routeCode) {
		this.routeCode = routeCode;
		return this;
	}

	


	/**
	 * @return the facilityCode
	 */
	public String getFacilityCode() {
		return facilityCode;
	}

	/**
	 * @param facilityCode the facilityCode to set
	 */
	public RequestIssuancePickListQRO setFacilityCode(String facilityCode) {
		this.facilityCode = facilityCode;
		return this;
	}

	/**
	 * @return the requestNumber
	 */
	public String getRequestNumber() {
		return requestNumber;
	}
	
	

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public RequestIssuancePickListQRO setCustomerName(String customerName) {
		this.customerName = customerName;
		return this;
	}

	/**
	 * @param requestNumber the requestNumber to set
	 */
	public RequestIssuancePickListQRO setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
		return this;
	}

	/**
	 * @return the requestorName
	 */
	public String getRequestorName() {
		return requestorName;
	}

	/**
	 * @param requestorName the requestorName to set
	 */
	public RequestIssuancePickListQRO setRequestorName(String requestorName) {
		this.requestorName = requestorName;
		return this;
	}

	/**
	 * @return the requestedDate
	 */
	public String getRequestedDate() {
		return requestedDate;
	}

	/**
	 * @param requestedDate the requestedDate to set
	 */
	public RequestIssuancePickListQRO setRequestedDate(String requestedDate) {
		this.requestedDate = requestedDate;
		return this;
	}

	/**
	 * @return the requiredDeliveryDate
	 */
	public String getRequiredDeliveryDate() {
		return requiredDeliveryDate;
	}

	/**
	 * @param requiredDeliveryDate the requiredDeliveryDate to set
	 */
	public RequestIssuancePickListQRO setRequiredDeliveryDate(String requiredDeliveryDate) {
		this.requiredDeliveryDate = requiredDeliveryDate;
		return this;
	}

	/**
	 * @return the dateIssued
	 */
	public String getDateIssued() {
		return dateIssued;
	}

	/**
	 * @param dateIssued the dateIssued to set
	 */
	public RequestIssuancePickListQRO setDateIssued(String dateIssued) {
		this.dateIssued = dateIssued;
		return this;
	}

	/**
	 * @return the requestedItems
	 */
	public List<PickListItemGroupQRO> getRequestedItems() {
		return requestedItems;
	}

	/**
	 * @param requestedItems the requestedItems to set
	 */
	public RequestIssuancePickListQRO setRequestedItems(List<PickListItemGroupQRO> requestedItems) {
		this.requestedItems = requestedItems;
		return this;
	}
}
