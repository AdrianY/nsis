package com.dhl.inventory.dto.security;

public class TokenTransfer {
	
	private final String token;

	public TokenTransfer(String createdToken) {
		token = createdToken;
	}

	public String getToken(){
		return this.token;
	}
}
