package com.dhl.inventory.dto.maintenance;

import javax.ws.rs.QueryParam;

import com.dhl.inventory.dto.AbstractSearchListCommandObject;

public class InventoryItemListTransfer extends AbstractSearchListCommandObject{
	
	@QueryParam("codeName")
	private String codeName;
	
	@QueryParam("type")
	private String type;
	
	@QueryParam("description")
	private String description;
	
	@QueryParam("piecesPerUnitOfMeaurement")
	private int piecesPerUnitOfMeaurement;
	
	@QueryParam("unitOfMeasurement")
	private String unitOfMeasurement;

	public InventoryItemListTransfer(){
		System.out.println("should create InventoryItemListTransfer");
	}

	public String getCodeName() {
		return codeName;
	}

//	@QueryParam("codeName")
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getType() {
		return type;
	}

//	@QueryParam("type")
	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

//	@QueryParam("description")
	public void setDescription(String description) {
		this.description = description;
	}

	public int getPiecesPerUnitOfMeaurement() {
		return piecesPerUnitOfMeaurement;
	}

//	@QueryParam("piecesPerUnitOfMeaurement")
	public void setPiecesPerUnitOfMeaurement(int piecesPerUnitOfMeaurement) {
		this.piecesPerUnitOfMeaurement = piecesPerUnitOfMeaurement;
	}

	public String getUnitOfMeasurement() {
		return unitOfMeasurement;
	}

//	@QueryParam("unitOfMeasurement")
	public void setUnitOfMeasurement(String unitOfMeasurement) {
		this.unitOfMeasurement = unitOfMeasurement;
	}
    
}
