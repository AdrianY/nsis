package com.dhl.inventory.dto;

import java.util.List;
import java.util.Map;

import com.dhl.inventory.dto.maintenance.InventoryItemListTransfer;

public class SearchList {
	private List<Map<String,String>> rows;
    private int page;
    private long records;
    private Double total;
    
    private SearchList(){}

    public static SearchList generateInstance(
            final SearchListValueObject valueObject,
            final InventoryItemListTransfer searchListCommandObject
    ){
    	SearchList instance =  new SearchList();
    	
    	instance.page = searchListCommandObject.getCurrentPage();
    	instance.records = valueObject.getTotalCount();
    	instance.rows = valueObject.getSearchedInstances();
    	instance.total = Math.ceil(valueObject.getTotalCount()/searchListCommandObject.getMaxRows());
    	return instance;
    }

	public List<Map<String, String>> getRows() {
		return rows;
	}

	public int getPage() {
		return page;
	}

	public long getRecords() {
		return records;
	}

	public Double getTotal() {
		return total;
	}
    
    
}
