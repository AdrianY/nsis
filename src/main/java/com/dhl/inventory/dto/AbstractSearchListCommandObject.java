package com.dhl.inventory.dto;

import java.util.Map;

import javax.ws.rs.QueryParam;

public abstract class AbstractSearchListCommandObject {
	
	private static final int DEFAULT_MAX_ROWS = 10;
	private static final int DEFAULT_PAGE = 1;
	private static final int DEFAULT_ROW_OFFSET = 0;
	
	protected Map<String, String> orderByMap;
    protected String defaultOrderField;
    
	@QueryParam("sidx")
    protected String sidx;
    
	@QueryParam("rows")
    protected int rows;
    
	@QueryParam("page")
    protected int page;
    
    public AbstractSearchListCommandObject(){}

    public String getSortField(){
    	String sortField = orderByMap.containsKey(sidx) ? orderByMap.get(sidx) : defaultOrderField;
        return sortField;
    }

    public int getMaxRows(){
    	int maxRows = 0 == rows ? DEFAULT_MAX_ROWS : rows;	
        return maxRows;
    }

    public int getCurrentPage(){
    	int currentPage = 0 == page ? DEFAULT_PAGE : page;
        return currentPage;
    }

    public int getRowOffset(){
    	int currentPage = getCurrentPage();
        return currentPage == DEFAULT_PAGE ? DEFAULT_ROW_OFFSET : ((currentPage - 1) * getMaxRows());
    }

	public String getSidx() {
		return sidx;
	}

//	@QueryParam("sidx")
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public int getRows() {
		return rows;
	}

//	@QueryParam("rows")
	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

//	@QueryParam("page")
	public void setPage(int page) {
		this.page = page;
	}
    
    
}
