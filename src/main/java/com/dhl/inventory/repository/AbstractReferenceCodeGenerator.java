package com.dhl.inventory.repository;

import java.lang.reflect.ParameterizedType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.dhl.inventory.domain.NsisDomainEntity;

public abstract class AbstractReferenceCodeGenerator<T extends NsisDomainEntity> implements ReferenceCodeGenerator {

	@PersistenceContext
    private EntityManager em;
	
    private String currentReferenceCode;
    
    private final Class<T> persistentClass;
	
	@SuppressWarnings("unchecked")
	public AbstractReferenceCodeGenerator(){
		this.persistentClass =(Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

    /**
     * Called upon a the first time this generator receives to provide a reference getCode
     * Using the concept of Virtual Proxy, this should provide better processing time as
     * we just have to call this once to reference getCode from the DB. Not executed in the
     * constructor to prevent conflicts during bean instantiation
     */
    public abstract String loadCurrentReferenceCodeFromDB();
    public abstract String constructReferenceCode(String baseReferenceCode);
    
    protected String getTableName(){
		return EntityTableNameExtractorUtil.getTableName(em,persistentClass);
	}
    
    protected EntityManager getEntityManager(){
		return em;
	}

	@Override
    public final String generateReferenceCode(){
        if (null!=currentReferenceCode&&!currentReferenceCode.isEmpty())
        	this.currentReferenceCode = constructReferenceCode(currentReferenceCode);
        else
        	this.currentReferenceCode = loadCurrentReferenceCodeFromDB();

        return this.currentReferenceCode;
    }
}
