package com.dhl.inventory.repository;


import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.util.SearchType;


public interface ImplementableRepository<T> {
	void save(T entity);
	
	void delete(T entity);
	
	T getByKey(String key);
	
	SearchListValueObject findAll(AbstractSearchListQueryObject commandObject, SearchType mode);
}
