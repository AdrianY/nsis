package com.dhl.inventory.repository.security;

import java.util.List;

import com.dhl.inventory.domain.security.NsisRole;

public interface RolesRepository{
	NsisRole findByCodeName(final String codeName);
	
	NsisRole getRoleById(String id);
	
	void deleteRoleWithCodeName(String codeName);
	
	List<NsisRole> findAllNsisRoles();
	
	void saveRole(NsisRole role);
}
