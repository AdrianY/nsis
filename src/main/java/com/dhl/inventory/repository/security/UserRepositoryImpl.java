package com.dhl.inventory.repository.security;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.components.UserCache;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.repository.AbstractNSISRepository;
import com.dhl.inventory.util.SearchType;

@Repository("userRepository")
class UserRepositoryImpl extends AbstractNSISRepository<String ,NsisUser> implements UserRepository{

	private static final Logger LOGGER = Logger.getLogger(UserRepositoryImpl.class);
	
	private UserCache userCache;
	
	@Autowired
	public UserRepositoryImpl(@Qualifier("userCache") final UserCache userCache){
		this.userCache = userCache;
	}

	@Override
	@SuppressWarnings("unchecked")
	public NsisUser findByUserName(String username) {
		
		NsisUser nsisUser = userCache.getUser(username);
		if(null == nsisUser){
			List<NsisUser> users = new ArrayList<NsisUser>();

			users = getEntityManager()
				.createQuery("from NsisUser where username=:username")
				.setParameter("username", username)
				.getResultList();

			if (users.size() > 0) {
				nsisUser = users.get(0);
				userCache.addUser(nsisUser);
			} 
		}
		
		return nsisUser;
	}

	@Override
	public void delete(NsisUser object) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public SearchListValueObject findAll(AbstractSearchListQueryObject commandObject, SearchType mode) {
		// TODO Auto-generated method stub
		return null;
	}

	public Logger getLogger(){
		return LOGGER;
	}

	@Override
	public void save(NsisUser user) {
		persist(user);	
	}
}
