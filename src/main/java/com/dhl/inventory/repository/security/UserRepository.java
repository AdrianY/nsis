package com.dhl.inventory.repository.security;

import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.repository.ImplementableRepository;

public interface UserRepository extends ImplementableRepository<NsisUser> {
	NsisUser findByUserName(String username);
}
