package com.dhl.inventory.repository.security;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.domain.security.NsisRole;
import com.dhl.inventory.repository.AbstractNSISRepository;

import static com.dhl.inventory.services.security.NSISUserRoleService.ROLE_PREFIX;

@Repository("rolesRepository")
class RolesRepositoryImpl extends AbstractNSISRepository<String, NsisRole> implements RolesRepository {

	private static final Logger LOGGER = Logger.getLogger(RolesRepositoryImpl.class);
	
	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public NsisRole findByCodeName(String roleCodeName) {
		List<NsisRole> users = new ArrayList<NsisRole>();

		
		users = getEntityManager()
			.createQuery("from NsisRole where codeName=:codeName")
			.setParameter("codeName", wrappedCodeName(roleCodeName))
			.getResultList();
		
		NsisRole returnValue = null;
		if (users.size() > 0) {
			returnValue = users.get(0);
		}
		
		return returnValue;
	}

	@Override
	public Logger getLogger() {
		return LOGGER;
	}

	@Override
	public NsisRole getRoleById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<NsisRole> findAllNsisRoles() {
		CriteriaBuilder cb = createEntityCriteria();
		CriteriaQuery<NsisRole> cq = cb.createQuery(NsisRole.class);
		Root<NsisRole> role = cq.from(NsisRole.class);
		cq.select(role);
		TypedQuery<NsisRole> q = getEntityManager().createQuery(cq);
		
        return (List<NsisRole>) q.getResultList();
	}

	@Override
	public void deleteRoleWithCodeName(String codeName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveRole(NsisRole role) {
		persist(role);		
	}
	
	private String wrappedCodeName(String roleCodeName){
		if(null!=roleCodeName&&0!=roleCodeName.indexOf(ROLE_PREFIX)){
			roleCodeName = ROLE_PREFIX+roleCodeName;
		}
		return roleCodeName;
	}

}
