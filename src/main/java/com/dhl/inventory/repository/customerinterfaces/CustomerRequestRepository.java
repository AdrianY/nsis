package com.dhl.inventory.repository.customerinterfaces;

import com.dhl.inventory.domain.customerinterfaces.CustomerRequest;
import com.dhl.inventory.repository.ImplementableRepository;

public interface CustomerRequestRepository extends ImplementableRepository<CustomerRequest> {
	CustomerRequest findByRequestNumber(String requestNumber);
	
	int clearAllRequestedItemRecords(CustomerRequest customerRequest);
}
