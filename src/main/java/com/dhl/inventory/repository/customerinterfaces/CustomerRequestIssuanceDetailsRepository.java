package com.dhl.inventory.repository.customerinterfaces;

import java.util.List;

import com.dhl.inventory.domain.customerinterfaces.CustomerRequestIssuanceDetails;

public interface CustomerRequestIssuanceDetailsRepository {
	CustomerRequestIssuanceDetails findIssuanceDetailsByRequestNumber(final String requestNumber);
	
	List<CustomerRequestIssuanceDetails> findIssuanceDetailsForEveryRequest(final List<String> requestNumbers);
}
