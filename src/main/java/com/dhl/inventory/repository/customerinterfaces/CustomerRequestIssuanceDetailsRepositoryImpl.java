package com.dhl.inventory.repository.customerinterfaces;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.dhl.inventory.domain.customerinterfaces.CustomerRequestIssuanceDetails;
import com.dhl.inventory.repository.AbstractNSISQueryOnlyRepository;

@Repository
class CustomerRequestIssuanceDetailsRepositoryImpl extends AbstractNSISQueryOnlyRepository
		implements CustomerRequestIssuanceDetailsRepository {

	@SuppressWarnings("unchecked")
	@Override
	public CustomerRequestIssuanceDetails findIssuanceDetailsByRequestNumber(String requestNumber) {
		List<CustomerRequestIssuanceDetails> items = new ArrayList<>();
		
		items = getEntityManager()
			.createQuery("from CustomerRequestIssuanceDetails where requestNumber=:requestNumber")
			.setParameter("requestNumber", requestNumber)
			.getResultList();
		
		CustomerRequestIssuanceDetails returnValue = null;
		if (items.size() > 0) {
			returnValue = items.get(0);
		}
		
		return returnValue;
	}

	@Override
	public List<CustomerRequestIssuanceDetails> findIssuanceDetailsForEveryRequest(
			List<String> requestNumbers) {
		CriteriaBuilder cB = createEntityCriteria();
		CriteriaQuery<CustomerRequestIssuanceDetails> criteriaQuery = 
				cB.createQuery(CustomerRequestIssuanceDetails.class);
		Root<CustomerRequestIssuanceDetails> root = 
				criteriaQuery.from(CustomerRequestIssuanceDetails.class);
		CriteriaQuery<CustomerRequestIssuanceDetails> selectClause = 
				criteriaQuery.select(root);
		
		Expression<String> requestNumber = root.get("requestNumber");
		Predicate condition = requestNumber.in(requestNumbers);
		selectClause.where(condition);
		
		TypedQuery<CustomerRequestIssuanceDetails> typedQuery = 
				getEntityManager().createQuery(selectClause);
		List<CustomerRequestIssuanceDetails> issuances = typedQuery.getResultList();
		
		return issuances;
	}

}
