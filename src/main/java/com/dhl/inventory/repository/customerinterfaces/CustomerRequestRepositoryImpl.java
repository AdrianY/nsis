package com.dhl.inventory.repository.customerinterfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.domain.customerinterfaces.CustomerRequest;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.customerinterfaces.CustomerRequestLQO;
import com.dhl.inventory.repository.AbstractNSISRepository;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.util.SearchType;

@Repository
class CustomerRequestRepositoryImpl extends AbstractNSISRepository<String, CustomerRequest> implements CustomerRequestRepository {

	private static final Logger LOGGER = Logger.getLogger(CustomerRequestRepositoryImpl.class);
	
	@Autowired
	private UserRepository userRepo;
	
	@Override
	public void save(CustomerRequest entity) {
		persist(entity);
	}

	@Override
	public void delete(CustomerRequest object) {
		remove(object);
	}

	@Override
	public CustomerRequest getByKey(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Logger getLogger() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CustomerRequest findByRequestNumber(String requestNumber) {
		List<CustomerRequest> items = new ArrayList<>();
		
		items = getEntityManager()
			.createQuery("from CustomerRequest where requestNumber=:requestNumber")
			.setParameter("requestNumber", requestNumber)
			.getResultList();
		
		CustomerRequest returnValue = null;
		if (items.size() > 0) {
			returnValue = items.get(0);
		}
		
		return returnValue;
	}

	@Override
	public int clearAllRequestedItemRecords(CustomerRequest customerRequest) {
		Query query = getEntityManager().createQuery(
				"DELETE FROM RequestedItem WHERE fromRequest=:customerRequest");
		return query.setParameter("customerRequest", customerRequest).executeUpdate();
	}
	
	private Long getCustomerRequestCount(CustomerRequestLQO listQueryObject){
		String queryString = generateCustomerRequestCountQueryString(listQueryObject);
		Query query = getEntityManager().createNativeQuery(queryString);
		
		if(isNotEmpty(listQueryObject.getAccountName())){
			query.setParameter("accountName", likeString(listQueryObject.getAccountName()));
		}
		if(isNotEmpty(listQueryObject.getAccountNumber())){
			query.setParameter("accountNumber", likeString(listQueryObject.getAccountNumber()));
		}
		if(isNotEmpty(listQueryObject.getStatus())){
			query.setParameter("status", listQueryObject.getStatus());
		}
		if(isNotEmpty(listQueryObject.getToDate())){
			query.setParameter("toDate", listQueryObject.getToDate());
		}
		if(isNotEmpty(listQueryObject.getFromDate())){
			query.setParameter("fromDate", listQueryObject.getFromDate());
		}
		LOGGER.info("listQueryObject.getFromDate(): )"+listQueryObject.getFromDate());
		if(isNotEmpty(listQueryObject.getFacilityCodeName())){
			query.setParameter("facilityCodeName", listQueryObject.getFacilityCodeName());
		}
		if(isNotEmpty(listQueryObject.getRequestType())){
			query.setParameter("requestType", listQueryObject.getRequestType());
		}
		if(isNotEmpty(listQueryObject.getReqNumber())){
			query.setParameter("requestNumber", likeString(listQueryObject.getReqNumber()));
		}
		
		Long count = Long.valueOf(String.valueOf(query.getSingleResult()));
		
		return count;
	}
	
	@Override
	public SearchListValueObject findAll(AbstractSearchListQueryObject commandObject, SearchType mode) {
		CustomerRequestLQO listQueryObject = (CustomerRequestLQO) commandObject;
		List<Object[]> onHandSuppliesByItem = getCustomerRequestList(listQueryObject);
		List<Map<String, String>> returnValue = new ArrayList<>();
		Optional.object(onHandSuppliesByItem).ifPresent(supplyList -> {
			supplyList.stream().forEach(supplyRecord -> {
				Map<String, String> record = new HashMap<>();
				record.put("requestNumber", (String)supplyRecord[0]);
				record.put("requestDate", (String)supplyRecord[1]);
				record.put("facilityCode", (String)supplyRecord[2]);
				
				NsisUser user = userRepo.getByKey((String)supplyRecord[3]);
				record.put("requestBy", user.getFullName());
				record.put("status", (String)supplyRecord[4]);
				record.put("accountName", (String)supplyRecord[5]);
				record.put("accountNumber", (String)supplyRecord[6]);
				
				returnValue.add(record);
			});
		});
		
		Long totalCount = 0L;
		if(!SearchType.TYPE_HEAD.equals(mode)){
			totalCount = getCustomerRequestCount(listQueryObject);
		}
		
		return new SearchListValueObject(returnValue, totalCount);
	}
	
	@SuppressWarnings("unchecked")
	private List<Object[]> getCustomerRequestList(final CustomerRequestLQO listQueryObject){
		String queryString = generateCustomerRequestQueryString(listQueryObject);
		Query query = getEntityManager().createNativeQuery(queryString);
		query.setParameter("maxRow", listQueryObject.getMaxRows());
		query.setParameter("offset", listQueryObject.getRowOffset());
		if(isNotEmpty(listQueryObject.getAccountName())){
			query.setParameter("accountName", likeString(listQueryObject.getAccountName()));
		}
		if(isNotEmpty(listQueryObject.getAccountNumber())){
			query.setParameter("accountNumber", likeString(listQueryObject.getAccountNumber()));
		}
		if(isNotEmpty(listQueryObject.getStatus())){
			query.setParameter("status", listQueryObject.getStatus());
		}
		if(isNotEmpty(listQueryObject.getToDate())){
			query.setParameter("toDate", listQueryObject.getToDate());
		}
		if(isNotEmpty(listQueryObject.getFromDate())){
			query.setParameter("fromDate", listQueryObject.getFromDate());
		}
		if(isNotEmpty(listQueryObject.getFacilityCodeName())){
			query.setParameter("facilityCodeName", listQueryObject.getFacilityCodeName());
		}
		if(isNotEmpty(listQueryObject.getRequestType())){
			query.setParameter("requestType", listQueryObject.getRequestType());
		}
		if(isNotEmpty(listQueryObject.getReqNumber())){
			query.setParameter("requestNumber", likeString(listQueryObject.getReqNumber()));
		}
		
		return query.getResultList();
	}
	
	private String generateCustomerRequestQueryString(final CustomerRequestLQO listQueryObject){
		StringBuilder queryStringBldr = new StringBuilder();
		queryStringBldr.append("SELECT TOP (:maxRow) aa_.request_number, "
				+ "aa_.request_date, aa_.facility_code, aa_.request_by_id, "
				+ "aa_.status, aa_.account_name, aa_.account_number, aa_.request_type ");
		queryStringBldr.append("FROM ( ");
		queryStringBldr.append(
			"SELECT zz_.*, "+
				   "ROW_NUMBER() OVER (ORDER BY zz_.request_number) AS row_num "+
			"FROM( "+
				"SELECT x_.recepient_id AS account_number, "+
					   "x_.cust_name AS account_name, "+
					   "CASE WHEN y_.status IS NOT NULL THEN y_.status ELSE x_.status END AS status, "+
					   "x_.request_by AS request_by_id, "+
					   "mf_.code_name AS facility_code, "+
					   "replace(convert(NVARCHAR, x_.request_date, 101), ' ', '/') AS request_date, "+
					   "x_.request_number AS request_number, "+
					   "x_.type AS request_type "+
				"FROM cr_req x_ "+
				"LEFT OUTER JOIN cr_req_iss_details y_ "+
					"ON x_.request_number = y_.request_number "+
				"LEFT OUTER JOIN mn_fac mf_ "+
					"ON x_.to_facility_id = mf_.id "+ 	
			") zz_ "+
			"WHERE zz_.status <> 'SUBMITTED' "
		);
		queryStringBldr.append(generatePaginationWhereClause(listQueryObject));
		queryStringBldr.append(") aa_ ");
		queryStringBldr.append("WHERE aa_.row_num >= :offset ");
		queryStringBldr.append(generateOrderByClause(listQueryObject));
		return queryStringBldr.toString();
	}
	
	private String generateCustomerRequestCountQueryString(final CustomerRequestLQO listQueryObject){
		StringBuilder queryStringBldr = new StringBuilder();
		queryStringBldr.append(
				"SELECT COUNT(*) "+
				"FROM( "+
					"SELECT x_.recepient_id AS account_number, "+
						   "x_.cust_name AS account_name, "+
						   "CASE WHEN y_.status IS NOT NULL THEN y_.status ELSE x_.status END AS status, "+
						   "x_.request_by AS request_by_id, "+
						   "mf_.code_name AS facility_code, "+
						   "replace(convert(NVARCHAR, x_.request_date, 101), ' ', '/') AS request_date, "+
						   "x_.request_number AS request_number, "+
						   "x_.type AS request_type "+
					"FROM cr_req x_ "+
					"LEFT OUTER JOIN cr_req_iss_details y_ "+
						"ON x_.request_number = y_.request_number "+
					"LEFT OUTER JOIN mn_fac mf_ "+
						"ON x_.to_facility_id = mf_.id "+ 	
				") zz_ "+
				"WHERE zz_.status <> 'SUBMITTED' "
			);
		queryStringBldr.append(generatePaginationWhereClause(listQueryObject));
		return queryStringBldr.toString();
	}
	
	private String generatePaginationWhereClause(final CustomerRequestLQO listQueryObject){
		StringBuilder queryStringBldr = new StringBuilder();
		if(isNotEmpty(listQueryObject.getAccountNumber())){
			queryStringBldr.append("AND zz_.account_number LIKE :accountNumber ");
		}
		if(isNotEmpty(listQueryObject.getReqNumber())){
			queryStringBldr.append("AND zz_.request_number LIKE :requestNumber ");
		}
		if(isNotEmpty(listQueryObject.getAccountName())){
			queryStringBldr.append("AND zz_.account_name LIKE :accountName ");
		}
		if(isNotEmpty(listQueryObject.getStatus())){
			queryStringBldr.append("AND zz_.status = :status ");
		}
		if(isNotEmpty(listQueryObject.getFacilityCodeName())){
			queryStringBldr.append("AND zz_.facility_code = :facilityCodeName ");
		}
		if(isNotEmpty(listQueryObject.getRequestType())){
			queryStringBldr.append("AND zz_.request_type = :requestType ");
		}
		
		boolean toDateIsNotEmpty = isNotEmpty(listQueryObject.getToDate());
		boolean fromDateIsNotEmpty = isNotEmpty(listQueryObject.getFromDate());
		if(toDateIsNotEmpty && fromDateIsNotEmpty){
			queryStringBldr.append("AND zz_.request_date BETWEEN :fromDate AND :toDate ");
		}else if(toDateIsNotEmpty){
			queryStringBldr.append("AND zz_.request_date <= :toDate ");
		}else if(fromDateIsNotEmpty){
			queryStringBldr.append("AND zz_.request_date >= :fromDate ");
		}
		return queryStringBldr.toString();
	}
	
	private String generateOrderByClause(final CustomerRequestLQO listQueryObject){
		return "ORDER BY "+listQueryObject.getSortField()+" "+listQueryObject.getSortOrder()+" ";
	}
}
