package com.dhl.inventory.repository.customerinterfaces;

import com.dhl.inventory.repository.ReferenceCodeGenerator;

public interface CustomerRequestNumberGenerator extends ReferenceCodeGenerator {

}
