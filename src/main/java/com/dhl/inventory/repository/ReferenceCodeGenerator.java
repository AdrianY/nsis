package com.dhl.inventory.repository;

public interface ReferenceCodeGenerator {
	String generateReferenceCode();
}
