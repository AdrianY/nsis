package com.dhl.inventory.repository.maintenance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.maintenance.FacilityPersonnel;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.maintenance.FacilityPersonnelLQO;
import com.dhl.inventory.repository.AbstractNSISRepository;
import com.dhl.inventory.util.SearchType;

@Repository("facilityPersonnelRepository")
class FacilityPersonnelRepositoryImpl extends AbstractNSISRepository<String, FacilityPersonnel> implements FacilityPersonnelRepository {

	private static final Logger LOGGER = Logger.getLogger(FacilityPersonnelRepositoryImpl.class);
	
	@Override
	public void save(FacilityPersonnel entity) {
		persist(entity);
	}

	@Override
	public void delete(FacilityPersonnel object) {
		// TODO Auto-generated method stub

	}

	@Override
	public SearchListValueObject findAll(AbstractSearchListQueryObject commandObject, SearchType mode) {
		CriteriaBuilder cB = createEntityCriteria();
		CriteriaQuery<FacilityPersonnel> criteriaQuery = cB.createQuery(FacilityPersonnel.class);
		Root<FacilityPersonnel> facilityInventoryItemRoot = criteriaQuery.from(FacilityPersonnel.class);
		
		FacilityPersonnelLQO facilityPersonnlLQO = (FacilityPersonnelLQO) commandObject;
		Predicate conditions = getFacilityPersonnelSearchConditions(cB,facilityPersonnlLQO,facilityInventoryItemRoot, mode);
		
		Long totalCount = 0L;
		if(!SearchType.TYPE_HEAD.equals(mode)){
			totalCount = getFacilityPersonnelCount(cB, conditions);
		}
		List<Map<String, String>> valueObjects = 
				getFacilityPersonnelList(
						cB,
						facilityPersonnlLQO,
						criteriaQuery,
						facilityInventoryItemRoot, 
						conditions,
						mode
				);
		
		return new SearchListValueObject(valueObjects, totalCount);
	}

	private List<Map<String, String>> getFacilityPersonnelList(
			CriteriaBuilder cB,
			FacilityPersonnelLQO listQueryObject, 
			CriteriaQuery<FacilityPersonnel> cQ,
			Root<FacilityPersonnel> root, 
			Predicate conditions, 
			SearchType mode
	) {
		CriteriaQuery<FacilityPersonnel> selectClause = cQ.select(root);
		
		//TODO Refactor this should not be obtained from root. What?
		if(!SearchType.TYPE_HEAD.equals(mode)){
			switch(listQueryObject.getSortOrder()){
				case AbstractSearchListQueryObject.SORT_ORDER_ASC:
					selectClause.orderBy(cB.asc(root.get(listQueryObject.getSortField())));
					break;
				case AbstractSearchListQueryObject.SORT_ORDER_DESC:
					selectClause.orderBy(cB.desc(root.get(listQueryObject.getSortField())));
					break;
			}
		}
			
		if(null != conditions)
			selectClause.where(conditions);
		
		TypedQuery<FacilityPersonnel> typedQuery = getEntityManager().createQuery(selectClause);
		typedQuery.setFirstResult(listQueryObject.getRowOffset());
		typedQuery.setMaxResults(listQueryObject.getMaxRows());
		List<FacilityPersonnel> personnels = typedQuery.getResultList();
		
		LOGGER.info("FacilityPersonnel size(): "+personnels.size());
		List<Map<String, String>> valueObjects = new ArrayList<Map<String, String>>();
		for(FacilityPersonnel personnel: personnels){
			Map<String, String> data = new HashMap<String, String>();
			
			NsisUser userAccessDetails = personnel.getUserAccessDetails();
			data.put("username", userAccessDetails.getUsername());
			data.put("emailAddress", userAccessDetails.getEmailAddress());
			data.put("contactNumber", userAccessDetails.getContactNumber());
			data.put("fullName", userAccessDetails.getFullName());
			
			LOGGER.info("data: "+data.toString());
			valueObjects.add(data);
		}
		
		return valueObjects;
	}

	private Long getFacilityPersonnelCount(CriteriaBuilder cB, Predicate conditions) {
		CriteriaQuery<Long> countCriteriaQuery = cB.createQuery(Long.class);
		countCriteriaQuery.select(cB.count(countCriteriaQuery.from(FacilityPersonnel.class)));
		getEntityManager().createQuery(countCriteriaQuery);
		if(null != conditions)
			countCriteriaQuery.where(conditions);
		
		return getEntityManager().createQuery(countCriteriaQuery).getSingleResult();
	}

	private Predicate getFacilityPersonnelSearchConditions(CriteriaBuilder cB, FacilityPersonnelLQO listQueryObject,
			Root<FacilityPersonnel> root, SearchType mode) {
		Join<FacilityPersonnel, Facility> facilityPath = root.join("assignedFacility");
		Join<FacilityPersonnel, NsisUser> userAccessPath = root.join("userAccessDetails");
		
		List<Predicate> logicallyGroupedPredicates = new ArrayList<Predicate>();
		if(isNotEmpty(listQueryObject.getFullName())){
			logicallyGroupedPredicates.add(cB.like(userAccessPath.get("firstName"), likeString(listQueryObject.getFullName())));
			logicallyGroupedPredicates.add(cB.like(userAccessPath.get("lastName"), likeString(listQueryObject.getFullName())));
		}
		if(isNotEmpty(listQueryObject.getUsername())){
			logicallyGroupedPredicates.add(cB.like(userAccessPath.get("username"), likeString(listQueryObject.getUsername())));
		}
		
		Predicate mandatoryPredicate = cB.equal(facilityPath.get("codeName"), listQueryObject.getFacilityId());
		Predicate returnValue = mandatoryPredicate;
		if(!logicallyGroupedPredicates.isEmpty()){
			Predicate[] logicallyGroupedPredicateArr = convertToPredicateArray(logicallyGroupedPredicates);
			Predicate logicalGroupPredicate = wrappedClause(cB, mode, logicallyGroupedPredicateArr);
			returnValue = cB.and(mandatoryPredicate, logicalGroupPredicate);
		}
		
		return returnValue;
	}

	@SuppressWarnings("unchecked")
	@Override
	public FacilityPersonnel findByUsernameAndFacilityCode(String userName, String facilityId) {
		List<FacilityPersonnel> personnels = new ArrayList<FacilityPersonnel>();
		
		StringBuilder queryStrBldr = 
				new StringBuilder("select p from FacilityPersonnel p ");
		queryStrBldr.append("inner join p.assignedFacility a ");
		queryStrBldr.append("inner join p.userAccessDetails u ");
		queryStrBldr.append("where a.codeName=:facilityId and u.username=:userName");
		
		personnels = getEntityManager()
			.createQuery(queryStrBldr.toString())
			.setParameter("userName", userName)
			.setParameter("facilityId", facilityId)
			.getResultList();
		
		FacilityPersonnel returnValue = null;
		if (personnels.size() > 0) {
			returnValue = personnels.get(0);
		}
		
		return returnValue;
	}

	@Override
	public Logger getLogger() {
		return LOGGER;
	}

	@SuppressWarnings("unchecked")
	@Override
	public FacilityPersonnel findByUsername(String userName) {
		List<FacilityPersonnel> personnels = new ArrayList<FacilityPersonnel>();
		
		StringBuilder queryStrBldr = 
				new StringBuilder("select p from FacilityPersonnel p ");
		queryStrBldr.append("inner join p.userAccessDetails u ");
		queryStrBldr.append("where u.username=:userName");
		
		personnels = getEntityManager()
			.createQuery(queryStrBldr.toString())
			.setParameter("userName", userName)
			.getResultList();
		
		FacilityPersonnel returnValue = null;
		if (personnels.size() > 0) {
			returnValue = personnels.get(0);
		}
		
		return returnValue;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FacilityPersonnel> findAllPersonnelPerFacilityAssignment(String userName) {
		List<FacilityPersonnel> personnels = new ArrayList<FacilityPersonnel>();
		
		StringBuilder queryStrBldr = 
				new StringBuilder("select p from FacilityPersonnel p ");
		queryStrBldr.append("inner join p.userAccessDetails u ");
		queryStrBldr.append("where u.username=:userName");
		
		personnels = getEntityManager()
			.createQuery(queryStrBldr.toString())
			.setParameter("userName", userName)
			.getResultList();
		
		return personnels;
	}

}
