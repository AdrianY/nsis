package com.dhl.inventory.repository.maintenance;

import com.dhl.inventory.repository.ReferenceCodeGenerator;

public interface CustomerAccountNumberGenerator extends ReferenceCodeGenerator {

}
