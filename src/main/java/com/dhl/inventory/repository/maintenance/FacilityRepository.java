package com.dhl.inventory.repository.maintenance;

import com.dhl.inventory.domain.maintenance.Facility;

public interface FacilityRepository {
	Facility findByCodeName(String facilityCode);
	
	void save(Facility object);
}
