package com.dhl.inventory.repository.maintenance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.domain.maintenance.CustomerAccount;
import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.maintenance.CustomerAccountLQO;
import com.dhl.inventory.repository.AbstractNSISRepository;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.util.SearchType;
import com.dhl.inventory.util.Using;

@Repository
class CustomerAccountRepositoryImpl extends AbstractNSISRepository<String, CustomerAccount> implements CustomerAccountRepository {

	private static final Logger LOGGER = Logger.getLogger(CustomerAccountRepositoryImpl.class);
	
	@Override
	public void save(CustomerAccount entity) {
		persist(entity);
	}

	@Override
	public void delete(CustomerAccount object) {
		// TODO Auto-generated method stub

	}

	@Override
	public CustomerAccount getByKey(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchListValueObject findAll(AbstractSearchListQueryObject commandObject, SearchType mode) {
		CriteriaBuilder cB = createEntityCriteria();
		CriteriaQuery<Tuple> criteriaQuery = cB.createTupleQuery();
		Root<CustomerAccount> customerAccountEntityRoot = criteriaQuery.from(CustomerAccount.class);
		
		CustomerAccountLQO supplyRequestLQO = (CustomerAccountLQO) commandObject;
		Predicate conditions = getCustomerAccountSearchConditions(cB,supplyRequestLQO,customerAccountEntityRoot, mode);
		
		Long totalCount = 0L;
		if(!SearchType.TYPE_HEAD.equals(mode)){
			totalCount = getCustomerAccountCount(customerAccountEntityRoot,criteriaQuery, cB, conditions);
		}
		List<Map<String, String>> valueObjects = 
				getCustomerAccountList(
						cB,
						supplyRequestLQO,
						criteriaQuery,
						customerAccountEntityRoot, 
						conditions,
						mode
				);
		
		return new SearchListValueObject(valueObjects, totalCount);
	}

	private List<Map<String, String>> getCustomerAccountList(
			CriteriaBuilder cB, 
			CustomerAccountLQO listQueryObject,
			CriteriaQuery<Tuple> multiSelectClause, 
			Root<CustomerAccount> root, 
			Predicate conditions,
			SearchType mode
	) {
		Join<CustomerAccount, Facility> servicingFacilityPath = root.join("servicingFacility");
		servicingFacilityPath.alias("servicingFacility_");
		
		multiSelectClause.select(
				cB.tuple(
					root.get("accountNumber").alias("accountNumber"),
					root.get("name").alias("name"),
					root.get("deliveryAddress").alias("deliveryAddress"),
					root.get("contactName").alias("contactName"),
					root.get("contactNumber").alias("contactNumber"),
					servicingFacilityPath.get("codeName").alias("facilityCode")
				)
			);
			
			if(!SearchType.TYPE_HEAD.equals(mode)){
				switch(listQueryObject.getSortOrder()){
					case AbstractSearchListQueryObject.SORT_ORDER_ASC:
						multiSelectClause.orderBy(cB.asc(root.get(listQueryObject.getSortField())));
						break;
					case AbstractSearchListQueryObject.SORT_ORDER_DESC:
						multiSelectClause.orderBy(cB.desc(root.get(listQueryObject.getSortField())));
						break;
				}
			}
				
			if(null != conditions)
				multiSelectClause.where(conditions);
			
			TypedQuery<Tuple> typedQuery = getEntityManager().createQuery(multiSelectClause);
			typedQuery.setFirstResult(listQueryObject.getRowOffset());
			typedQuery.setMaxResults(listQueryObject.getMaxRows());
			List<Tuple> customerAccountsAggregate = typedQuery.getResultList();
			
			LOGGER.info("CustomerAccount size(): "+customerAccountsAggregate.size());
			List<Map<String, String>> valueObjects = new ArrayList<Map<String, String>>();
			for(Tuple or: customerAccountsAggregate){
				Map<String, String> data = new HashMap<String, String>();
				
				data.put("accountNumber", (String)or.get("accountNumber"));
				data.put("name", (String)or.get("name"));
				data.put("deliveryAddress", (String)or.get("deliveryAddress"));
				data.put("contactName", (String)or.get("contactName"));
				data.put("contactNumber", (String)or.get("contactNumber"));
				data.put("servicingFacility", (String)or.get("facilityCode"));
				valueObjects.add(data);
			}
			
			return valueObjects;
	}

	private Long getCustomerAccountCount(
			Root<CustomerAccount> root,
			CriteriaQuery<Tuple> criteriaQuery, 
			CriteriaBuilder cB, 
			Predicate conditions
	) {
		criteriaQuery.select(cB.tuple(cB.count(root).alias("count")));
		if(null != conditions)
			criteriaQuery.where(conditions);
		
		Tuple singleResult = getEntityManager().createQuery(criteriaQuery).getSingleResult();
		
		return singleResult.get("count", Long.class);
	}

	private Predicate getCustomerAccountSearchConditions(
			CriteriaBuilder cB, 
			CustomerAccountLQO listQueryObject,
			Root<CustomerAccount> root, 
			SearchType mode
	) {
		List<Predicate> predicates = new ArrayList<>();
		
		Optional.object(listQueryObject.getAccountNumber()).ifPresent(val -> {
			predicates.add(cB.like(root.get("accountNumber"), likeString(val)));
		});
		
		Optional.object(listQueryObject.getName()).ifPresent(val -> {
			predicates.add(cB.like(root.get("name"), likeString(val)));
		});
		
		Predicate logicalGroupPredicate = 
				Using
				.thisObject(predicates)
				.deriveIfPresent(p -> {
					Predicate[] logicallyGroupedPredicateArr = convertToPredicateArray(p);
					return wrappedClause(cB, mode, logicallyGroupedPredicateArr);
				});
		
		return logicalGroupPredicate;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CustomerAccount findByAccountNumber(String accountNumber) {
		List<CustomerAccount> customerAccounts = new ArrayList<CustomerAccount>();
		
		customerAccounts = getEntityManager()
			.createQuery("from CustomerAccount where accountNumber=:accountNumber")
			.setParameter("accountNumber", accountNumber)
			.getResultList();
		
		CustomerAccount returnValue = null;
		if (customerAccounts.size() > 0) {
			returnValue = customerAccounts.get(0);
		}
		
		return returnValue;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public CustomerAccount findByNameAndDeliveryAddress(String name, String deliveryAddress) {
		List<CustomerAccount> customerAccounts = new ArrayList<CustomerAccount>();
		
		customerAccounts = getEntityManager()
			.createQuery("from CustomerAccount where name=:name and deliveryAddress=:deliveryAddress ")
			.setParameter("name", name)
			.setParameter("deliveryAddress", deliveryAddress)
			.getResultList();
		
		CustomerAccount returnValue = null;
		if (customerAccounts.size() > 0) {
			returnValue = customerAccounts.get(0);
		}
		
		return returnValue;
	}

	@Override
	public Logger getLogger() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CustomerAccount findByName(String name) {
		List<CustomerAccount> customerAccounts = new ArrayList<CustomerAccount>();
		
		customerAccounts = getEntityManager()
			.createQuery("from CustomerAccount where name=:name")
			.setParameter("name", name)
			.getResultList();
		
		CustomerAccount returnValue = null;
		if (customerAccounts.size() > 0) {
			returnValue = customerAccounts.get(0);
		}
		
		return returnValue;
	}

}
