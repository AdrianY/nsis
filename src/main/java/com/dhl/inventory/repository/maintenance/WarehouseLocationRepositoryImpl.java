package com.dhl.inventory.repository.maintenance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.maintenance.WarehouseLocation;
import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.maintenance.WarehouseLocationLQO;
import com.dhl.inventory.repository.AbstractNSISRepository;
import com.dhl.inventory.util.SearchType;

@Repository
class WarehouseLocationRepositoryImpl extends AbstractNSISRepository<String, WarehouseLocation>
		implements WarehouseLocationRepository {
	
	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(WarehouseLocationRepositoryImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public WarehouseLocation findByFacilityAndRackingCode(Facility facility, String rackingCode) {
		List<WarehouseLocation> whLocations = new ArrayList<WarehouseLocation>();
		
		StringBuilder queryStrBldr = 
				new StringBuilder("select p from WarehouseLocation p ");
		queryStrBldr.append("where p.facility=:facility and p.rackCode=:rackCode");
		
		whLocations = getEntityManager()
			.createQuery(queryStrBldr.toString())
			.setParameter("facility", facility)
			.setParameter("rackCode", rackingCode)
			.getResultList();
		
		WarehouseLocation returnValue = null;
		if (whLocations.size() > 0) {
			returnValue = whLocations.get(0);
		}
		
		return returnValue;
	}

	@Override
	public Logger getLogger() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(WarehouseLocation entity) {
		persist(entity);
	}

	@Override
	public void delete(WarehouseLocation object) {
		// TODO Auto-generated method stub
	}

	@Override
	public SearchListValueObject findAll(AbstractSearchListQueryObject commandObject, SearchType mode) {
		CriteriaBuilder cB = createEntityCriteria();
		CriteriaQuery<WarehouseLocation> criteriaQuery = cB.createQuery(WarehouseLocation.class);
		Root<WarehouseLocation> warehouseLocationRoot = criteriaQuery.from(WarehouseLocation.class);
		
		WarehouseLocationLQO warehouseLocationLQO = (WarehouseLocationLQO) commandObject;
		Predicate conditions = getWarehouseLocationSearchConditions(cB,warehouseLocationLQO,warehouseLocationRoot, mode);
		
		Long totalCount = 0L;
		if(!SearchType.TYPE_HEAD.equals(mode)){
			totalCount = getWarehouseLocationCount(cB, conditions);
		}
		List<Map<String, String>> valueObjects = 
				getWarehouseLocationList(
						cB,
						warehouseLocationLQO,
						criteriaQuery,
						warehouseLocationRoot, 
						conditions,
						mode
				);
		
		return new SearchListValueObject(valueObjects, totalCount);
	}

	private List<Map<String, String>> getWarehouseLocationList(
			CriteriaBuilder cB,
			WarehouseLocationLQO listQueryObject, 
			CriteriaQuery<WarehouseLocation> criteriaQuery,
			Root<WarehouseLocation> root, 
			Predicate conditions, SearchType mode
	) {
		CriteriaQuery<WarehouseLocation> selectClause = criteriaQuery.select(root);
		
		//TODO Refactor this should not be obtained from root. What?
		if(!SearchType.TYPE_HEAD.equals(mode)){
			switch(listQueryObject.getSortOrder()){
				case AbstractSearchListQueryObject.SORT_ORDER_ASC:
					selectClause.orderBy(cB.asc(root.get(listQueryObject.getSortField())));
					break;
				case AbstractSearchListQueryObject.SORT_ORDER_DESC:
					selectClause.orderBy(cB.desc(root.get(listQueryObject.getSortField())));
					break;
			}
		}
			
		if(null != conditions)
			selectClause.where(conditions);
		
		TypedQuery<WarehouseLocation> typedQuery = getEntityManager().createQuery(selectClause);
		typedQuery.setFirstResult(listQueryObject.getRowOffset());
		typedQuery.setMaxResults(listQueryObject.getMaxRows());
		List<WarehouseLocation> warehouseLocations = typedQuery.getResultList();
		
		List<Map<String, String>> valueObjects = new ArrayList<Map<String, String>>();
		for(WarehouseLocation warehouseLocation: warehouseLocations){
			Map<String, String> data = new HashMap<String, String>();
			
			data.put("rackCode", warehouseLocation.getRackCode());
			
			valueObjects.add(data);
		}
		
		return valueObjects;
	}

	private Long getWarehouseLocationCount(CriteriaBuilder cB, Predicate conditions) {
		CriteriaQuery<Long> countCriteriaQuery = cB.createQuery(Long.class);
		countCriteriaQuery.select(cB.count(countCriteriaQuery.from(WarehouseLocation.class)));
		getEntityManager().createQuery(countCriteriaQuery);
		if(null != conditions)
			countCriteriaQuery.where(conditions);
		
		return getEntityManager().createQuery(countCriteriaQuery).getSingleResult();
	}

	private Predicate getWarehouseLocationSearchConditions(
			CriteriaBuilder cB,
			WarehouseLocationLQO listQueryObject,
			Root<WarehouseLocation> root, 
			SearchType mode
	) {
		Join<WarehouseLocation, Facility> facilityPath = root.join("facility");
		
		List<Predicate> logicallyGroupedPredicates = new ArrayList<Predicate>();
		if(isNotEmpty(listQueryObject.getRackCode())){
			logicallyGroupedPredicates.add(cB.like(root.get("rackCode"), likeString(listQueryObject.getRackCode())));
		}
		Predicate mandatoryPredicate = cB.equal(facilityPath.get("codeName"), listQueryObject.getFacilityCode());
		Predicate returnValue = mandatoryPredicate;
		if(!logicallyGroupedPredicates.isEmpty()){
			Predicate[] logicallyGroupedPredicateArr = convertToPredicateArray(logicallyGroupedPredicates);
			Predicate logicalGroupPredicate = wrappedClause(cB, mode, logicallyGroupedPredicateArr);
			returnValue = cB.and(mandatoryPredicate, logicalGroupPredicate);
		}
		
		return returnValue;
	}

}
