package com.dhl.inventory.repository.maintenance;

import java.util.List;

import com.dhl.inventory.domain.maintenance.InventoryItem;
import com.dhl.inventory.repository.ImplementableRepository;

public interface InventoryItemRepository extends ImplementableRepository<InventoryItem>{

	InventoryItem findByCodeName(String codeName);

	List<InventoryItem> findAllItemDetailsInList(List<String> itemCodes);
}
