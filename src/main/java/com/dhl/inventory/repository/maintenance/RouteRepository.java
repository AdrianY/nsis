package com.dhl.inventory.repository.maintenance;

import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.maintenance.Route;
import com.dhl.inventory.repository.ImplementableRepository;

public interface RouteRepository extends ImplementableRepository<Route>{
	Route findByRouteCodeAndFacility(String routeCode, Facility facilityCode);
}
