package com.dhl.inventory.repository.maintenance;

import java.util.List;

import com.dhl.inventory.domain.maintenance.FacilityPersonnel;
import com.dhl.inventory.repository.ImplementableRepository;

public interface FacilityPersonnelRepository extends ImplementableRepository<FacilityPersonnel> {

	FacilityPersonnel findByUsernameAndFacilityCode(String userName, String facilityId);

	/**
	 * TODO: This is just a workaround. Remove this after solution
	 * to the one calling this is provided.
	 * @param userName
	 * @return
	 * @deprecated
	 */
	FacilityPersonnel findByUsername(String userName);
	
	List<FacilityPersonnel> findAllPersonnelPerFacilityAssignment(String userName);
}
