package com.dhl.inventory.repository.maintenance;

import com.dhl.inventory.domain.maintenance.Currency;
import com.dhl.inventory.repository.ImplementableRepository;

public interface CurrencyRepository extends ImplementableRepository<Currency> {
	Currency findByCurrencyCode(String currencyCode);
}
