package com.dhl.inventory.repository.maintenance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.domain.maintenance.InventoryItem;
import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.maintenance.InventoryItemLQO;
import com.dhl.inventory.repository.AbstractNSISRepository;
import com.dhl.inventory.util.SearchType;

@Repository
class InventoryItemRepositoryImpl extends AbstractNSISRepository<String, InventoryItem>
		implements InventoryItemRepository {
	
	private static final Logger LOGGER = Logger.getLogger(InventoryItemRepositoryImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public InventoryItem findByCodeName(String codeName) {
		List<InventoryItem> items = new ArrayList<InventoryItem>();
		
		items = getEntityManager()
			.createQuery("from InventoryItem where codeName=:codeName")
			.setParameter("codeName", codeName)
			.getResultList();
		
		InventoryItem returnValue = null;
		if (items.size() > 0) {
			returnValue = items.get(0);
		}
		
		return returnValue;
	}

	@Override
	public Logger getLogger() {
		return LOGGER;
	}

	@Override
	public void save(InventoryItem object) {
		persist(object);
		
	}
	
	public SearchListValueObject findAll(AbstractSearchListQueryObject commandObject, SearchType mode) {
		CriteriaBuilder criteriaBuilder = createEntityCriteria();
		CriteriaQuery<InventoryItem> criteriaQuery = criteriaBuilder.createQuery(InventoryItem.class);
		Root<InventoryItem> inventoryItemRoot = criteriaQuery.from(InventoryItem.class);
		CriteriaQuery<InventoryItem> selectClause = criteriaQuery.select(inventoryItemRoot);
		
		InventoryItemLQO itemListCommandObject = (InventoryItemLQO) commandObject;
		List<Predicate> conditions = new ArrayList<Predicate>();		
		if(isNotEmpty(itemListCommandObject.getType())){
			conditions.add(criteriaBuilder.equal(inventoryItemRoot.get("type"), likeString(itemListCommandObject.getType())));
		}
		if(isNotEmpty(itemListCommandObject.getDescription())){
			conditions.add(criteriaBuilder.like(inventoryItemRoot.get("description"), likeString(itemListCommandObject.getDescription())));
		}
		if(isNotEmpty(itemListCommandObject.getCodeName())){
			conditions.add(criteriaBuilder.like(inventoryItemRoot.get("codeName"), likeString(itemListCommandObject.getCodeName())));
		}
		if(!SearchType.TYPE_HEAD.equals(mode)){
			if(isNotEmpty(itemListCommandObject.getUnitOfMeasurement())){
				conditions.add(criteriaBuilder.like(inventoryItemRoot.get("unitOfMeasurement"), likeString(itemListCommandObject.getUnitOfMeasurement())));
			}
			if(isNotEmpty(itemListCommandObject.getPiecesPerUnitOfMeaurement())){
				conditions.add(criteriaBuilder.greaterThanOrEqualTo(inventoryItemRoot.get("piecesPerUnitOfMeasurement"), itemListCommandObject.getPiecesPerUnitOfMeaurement()));
			}
		}
		Predicate whereClauseConditons = null;
		if(!conditions.isEmpty()){
			Predicate[] logicallyGroupedPredicateArr = convertToPredicateArray(conditions);
			whereClauseConditons = wrappedClause(criteriaBuilder, mode, logicallyGroupedPredicateArr);
		}
		
		
		Long totalCount = 0L;
		if(!SearchType.TYPE_HEAD.equals(mode)){
			CriteriaQuery<Long> countCriteriaQuery = criteriaBuilder.createQuery(Long.class);
			countCriteriaQuery.select(criteriaBuilder.count(countCriteriaQuery.from(InventoryItem.class)));
			getEntityManager().createQuery(countCriteriaQuery);
			if(null != whereClauseConditons)
				countCriteriaQuery.where(whereClauseConditons);
			
			totalCount = getEntityManager().createQuery(countCriteriaQuery).getSingleResult();
			
			switch(itemListCommandObject.getSortOrder()){
				case AbstractSearchListQueryObject.SORT_ORDER_ASC:
					selectClause.orderBy(criteriaBuilder.asc(inventoryItemRoot.get(itemListCommandObject.getSortField())));
					break;
				case AbstractSearchListQueryObject.SORT_ORDER_DESC:
					selectClause.orderBy(criteriaBuilder.desc(inventoryItemRoot.get(itemListCommandObject.getSortField())));
					break;
			}
		}
		
			
		if(null != whereClauseConditons)
			selectClause.where(whereClauseConditons);
				
		TypedQuery<InventoryItem> typedQuery = getEntityManager().createQuery(selectClause);
		typedQuery.setFirstResult(commandObject.getRowOffset());
		typedQuery.setMaxResults(commandObject.getMaxRows());
		List<InventoryItem> items = typedQuery.getResultList();
		
		List<Map<String, String>> valueObjects = new ArrayList<Map<String, String>>();
		for(InventoryItem item: items){
			Map<String, String> data = new HashMap<String, String>();
			data.put("codeName", item.getCodeName());
			data.put("type", item.getType());
			data.put("description", item.getDescription());
			data.put("uom", item.getUnitOfMeasurement());
			valueObjects.add(data);
		}
		return new SearchListValueObject(valueObjects, totalCount);
	}

	@Override
	public void delete(InventoryItem object) {
		remove(object);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InventoryItem> findAllItemDetailsInList(List<String> itemCodes) {
		List<InventoryItem> items = new ArrayList<InventoryItem>();
		
		items = getEntityManager()
			.createQuery("from InventoryItem where codeName in :codeNames")
			.setParameter("codeNames", itemCodes)
			.getResultList();
		
		return items;
	}

}
