package com.dhl.inventory.repository.maintenance;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.domain.maintenance.Currency;
import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.repository.AbstractNSISRepository;
import com.dhl.inventory.util.SearchType;

@Repository
class CurrencyRepositoryImpl extends AbstractNSISRepository<String, Currency> implements CurrencyRepository {

	private static final Logger LOGGER = Logger.getLogger(CurrencyRepositoryImpl.class);
	
	@Override
	public void save(Currency entity) {
		persist(entity);
	}

	@Override
	public void delete(Currency object) {
		// TODO Auto-generated method stub

	}

	@Override
	public SearchListValueObject findAll(AbstractSearchListQueryObject commandObject, SearchType mode) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Currency findByCurrencyCode(String currencyCode) {
		List<Currency> items = new ArrayList<Currency>();
		
		items = getEntityManager()
			.createQuery("from Currency where code=:currencyCode")
			.setParameter("currencyCode", currencyCode)
			.getResultList();
		
		Currency returnValue = null;
		if (items.size() > 0) {
			returnValue = items.get(0);
		}
		
		return returnValue;
	}

	@Override
	public Logger getLogger() {
		return LOGGER;
	}

}
