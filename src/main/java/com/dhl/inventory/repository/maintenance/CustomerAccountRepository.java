package com.dhl.inventory.repository.maintenance;

import com.dhl.inventory.domain.maintenance.CustomerAccount;
import com.dhl.inventory.repository.ImplementableRepository;

public interface CustomerAccountRepository extends ImplementableRepository<CustomerAccount> {
	CustomerAccount findByAccountNumber(String accountNumber);
	
	CustomerAccount findByNameAndDeliveryAddress(String name, String deliveryAddress);
	
	CustomerAccount findByName(String name);
}
