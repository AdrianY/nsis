package com.dhl.inventory.repository.maintenance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.maintenance.Route;
import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.maintenance.RouteLQO;
import com.dhl.inventory.repository.AbstractNSISRepository;
import com.dhl.inventory.util.SearchType;

@Repository
class RouteRepositoryImpl extends AbstractNSISRepository<String, Route> implements RouteRepository {

	@Override
	public void save(Route entity) {
		persist(entity);
	}

	@Override
	public void delete(Route entity) {
		remove(entity);
	}

	@Override
	public Route getByKey(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchListValueObject findAll(AbstractSearchListQueryObject commandObject, SearchType mode) {
		CriteriaBuilder cB = createEntityCriteria();
		CriteriaQuery<Route> criteriaQuery = cB.createQuery(Route.class);
		Root<Route> routeRoot = criteriaQuery.from(Route.class);
		
		RouteLQO routeLQO = (RouteLQO) commandObject;
		Predicate conditions = getRouteSearchConditions(cB,routeLQO,routeRoot, mode);
		
		Long totalCount = 0L;
		if(!SearchType.TYPE_HEAD.equals(mode)){
			totalCount = getRouteCount(cB, conditions);
		}
		List<Map<String, String>> valueObjects = 
				getRouteList(
						cB,
						routeLQO,
						criteriaQuery,
						routeRoot, 
						conditions,
						mode
				);
		
		return new SearchListValueObject(valueObjects, totalCount);
	}

	private List<Map<String, String>> getRouteList(
			CriteriaBuilder cB, RouteLQO listQueryObject,
			CriteriaQuery<Route> criteriaQuery, 
			Root<Route> root, 
			Predicate conditions, 
			SearchType mode
	) {
		CriteriaQuery<Route> selectClause = criteriaQuery.select(root);
		
		//TODO Refactor this should not be obtained from root. What?
		switch(listQueryObject.getSortOrder()){
			case AbstractSearchListQueryObject.SORT_ORDER_ASC:
				selectClause.orderBy(cB.asc(root.get(listQueryObject.getSortField())));
				break;
			case AbstractSearchListQueryObject.SORT_ORDER_DESC:
				selectClause.orderBy(cB.desc(root.get(listQueryObject.getSortField())));
				break;
		}
			
		if(null != conditions)
			selectClause.where(conditions);
		
		TypedQuery<Route> typedQuery = getEntityManager().createQuery(selectClause);
		/*typedQuery.setFirstResult(listQueryObject.getRowOffset());
		typedQuery.setMaxResults(listQueryObject.getMaxRows());*/
		List<Route> routes = typedQuery.getResultList();
		
		List<Map<String, String>> valueObjects = new ArrayList<Map<String, String>>();
		for(Route route: routes){
			Map<String, String> data = new HashMap<String, String>();
			
			data.put("routeCode", route.getRouteCode());
			
			valueObjects.add(data);
		}
		
		return valueObjects;
	}

	private Long getRouteCount(CriteriaBuilder cB, Predicate conditions) {
		CriteriaQuery<Long> countCriteriaQuery = cB.createQuery(Long.class);
		countCriteriaQuery.select(cB.count(countCriteriaQuery.from(Route.class)));
		getEntityManager().createQuery(countCriteriaQuery);
		if(null != conditions)
			countCriteriaQuery.where(conditions);
		
		return getEntityManager().createQuery(countCriteriaQuery).getSingleResult();
	}

	private Predicate getRouteSearchConditions(
			CriteriaBuilder cB, 
			RouteLQO listQueryObject, 
			Root<Route> root,
			SearchType mode
	) {
		Join<Route, Facility> facilityPath = root.join("assignedFacility");
		
		Predicate mandatoryPredicate = cB.equal(facilityPath.get("codeName"), listQueryObject.getFacilityCode());
		
		return mandatoryPredicate;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Route findByRouteCodeAndFacility(String routeCode, Facility facility) {
		List<Route> routes = new ArrayList<>();
		
		StringBuilder queryStrBldr = 
				new StringBuilder("select p from Route p ");
		queryStrBldr.append("where p.assignedFacility=:facility and p.routeCode=:routeCode");
		
		routes = getEntityManager()
			.createQuery(queryStrBldr.toString())
			.setParameter("routeCode", routeCode)
			.setParameter("facility", facility)
			.getResultList();
		
		Route returnValue = null;
		if (routes.size() > 0) {
			returnValue = routes.get(0);
		}
		
		return returnValue;
	}

	@Override
	public Logger getLogger() {
		// TODO Auto-generated method stub
		return null;
	}

}
