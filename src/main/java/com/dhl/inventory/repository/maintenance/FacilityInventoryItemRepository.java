package com.dhl.inventory.repository.maintenance;

import com.dhl.inventory.domain.maintenance.FacilityInventoryItem;
import com.dhl.inventory.repository.ImplementableRepository;

public interface FacilityInventoryItemRepository extends ImplementableRepository<FacilityInventoryItem>{
	FacilityInventoryItem findByFacilityIdAndItemCode(String facilityId, String itemCode);
}
