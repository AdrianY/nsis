package com.dhl.inventory.repository.maintenance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.maintenance.FacilityInventoryItem;
import com.dhl.inventory.domain.maintenance.InventoryItem;
import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.maintenance.FacilityItemLQO;
import com.dhl.inventory.repository.AbstractNSISRepository;
import com.dhl.inventory.util.SearchType;

@Repository
class FacilityInventoryItemRepositoryImpl extends AbstractNSISRepository<String, FacilityInventoryItem> implements FacilityInventoryItemRepository {

	private static final Logger LOGGER = Logger.getLogger(FacilityInventoryItemRepositoryImpl.class);
	
	@Override
	public void save(FacilityInventoryItem entity) {
		persist(entity);
	}

	@Override
	public void delete(FacilityInventoryItem object) {
		
	}

	@Override
	public SearchListValueObject findAll(AbstractSearchListQueryObject commandObject, SearchType mode) {
		
		CriteriaBuilder cB = createEntityCriteria();
		CriteriaQuery<FacilityInventoryItem> criteriaQuery = cB.createQuery(FacilityInventoryItem.class);
		Root<FacilityInventoryItem> facilityInventoryItemRoot = criteriaQuery.from(FacilityInventoryItem.class);
		
		FacilityItemLQO facilityItemLQO = (FacilityItemLQO) commandObject;
		Predicate conditions = getFacilityItemSearchConditions(cB,facilityItemLQO,facilityInventoryItemRoot, mode);
		
		Long totalCount = 0L;
		if(!SearchType.TYPE_HEAD.equals(mode)){
			totalCount = getFacilityItemCount(cB, conditions);
		}
		List<Map<String, String>> valueObjects = 
				getFacilityItemList(
						cB,
						facilityItemLQO,
						criteriaQuery,
						facilityInventoryItemRoot, 
						conditions,
						mode
				);
		
		return new SearchListValueObject(valueObjects, totalCount);
	}

	@SuppressWarnings("unchecked")
	@Override
	public FacilityInventoryItem findByFacilityIdAndItemCode(String facilityId, String itemCode) {
		List<FacilityInventoryItem> facilities = new ArrayList<FacilityInventoryItem>();
		
		StringBuilder queryStrBldr = 
				new StringBuilder("select f from FacilityInventoryItem f ");
		queryStrBldr.append("inner join f.owningFacility fc ");
		queryStrBldr.append("inner join f.inventoryItem i ");
		queryStrBldr.append("where fc.codeName=:facilityId and i.codeName=:codeName");
		
		facilities = getEntityManager()
			.createQuery(queryStrBldr.toString())
			.setParameter("codeName", itemCode)
			.setParameter("facilityId", facilityId)
			.getResultList();
		
		FacilityInventoryItem returnValue = null;
		if (facilities.size() > 0) {
			returnValue = facilities.get(0);
		}
		
		return returnValue;
	}

	@Override
	public Logger getLogger() {
		return LOGGER;
	}
	
	private Predicate getFacilityItemSearchConditions(
			CriteriaBuilder cB, 
			FacilityItemLQO listQueryObject, 
			Root<FacilityInventoryItem> root, 
			SearchType mode
	){
		Join<FacilityInventoryItem, Facility> facilityPath = root.join("owningFacility");
		Join<FacilityInventoryItem, InventoryItem> inventoryItemPath = root.join("inventoryItem");
		
		List<Predicate> logicallyGroupedPredicates = new ArrayList<Predicate>();
		if(isNotEmpty(listQueryObject.getType())){
			logicallyGroupedPredicates.add(cB.like(inventoryItemPath.get("type"), likeString(listQueryObject.getType())));
		}
		if(isNotEmpty(listQueryObject.getDescription())){
			logicallyGroupedPredicates.add(cB.like(inventoryItemPath.get("description"), likeString(listQueryObject.getDescription())));
		}
		if(isNotEmpty(listQueryObject.getItemCode())){
			logicallyGroupedPredicates.add(cB.like(inventoryItemPath.get("codeName"), likeString(listQueryObject.getItemCode())));
		}
		if(isNotEmpty(listQueryObject.getUnitOfMeasurement())){
			logicallyGroupedPredicates.add(cB.like(inventoryItemPath.get("unitOfMeasurement"), likeString(listQueryObject.getUnitOfMeasurement())));
		}
		if(isNotEmpty(listQueryObject.getPiecesPerUnitOfMeaurement())){
			logicallyGroupedPredicates.add(cB.greaterThanOrEqualTo(inventoryItemPath.get("piecesPerUnitOfMeasurement"), listQueryObject.getPiecesPerUnitOfMeaurement()));
		}
		
		Predicate mandatoryPredicate = cB.equal(facilityPath.get("codeName"), listQueryObject.getFacilityId());
		Predicate returnValue = mandatoryPredicate;
		if(!logicallyGroupedPredicates.isEmpty()){
			Predicate[] logicallyGroupedPredicateArr = convertToPredicateArray(logicallyGroupedPredicates);
			Predicate logicalGroupPredicate = wrappedClause(cB, mode, logicallyGroupedPredicateArr);
			returnValue = cB.and(mandatoryPredicate, logicalGroupPredicate);
		}
		
		return returnValue;
	}
	
	private List<Map<String, String>> getFacilityItemList(
			CriteriaBuilder cB, 
			FacilityItemLQO listQueryObject,
			CriteriaQuery<FacilityInventoryItem> cQ, 
			Root<FacilityInventoryItem> root, 
			Predicate conditions,
			SearchType mode
	){
		CriteriaQuery<FacilityInventoryItem> selectClause = cQ.select(root);
		
		
		//TODO Refactor this should not be obtained from root!
		if(!SearchType.TYPE_HEAD.equals(mode)){
			switch(listQueryObject.getSortOrder()){
				case AbstractSearchListQueryObject.SORT_ORDER_ASC:
					selectClause.orderBy(cB.asc(root.get(listQueryObject.getSortField())));
					break;
				case AbstractSearchListQueryObject.SORT_ORDER_DESC:
					selectClause.orderBy(cB.desc(root.get(listQueryObject.getSortField())));
					break;
			}
		}
			
		if(null != conditions)
			selectClause.where(conditions);
		
		LOGGER.info("selectClause: "+selectClause.toString());
				
		TypedQuery<FacilityInventoryItem> typedQuery = getEntityManager().createQuery(selectClause);
		typedQuery.setFirstResult(listQueryObject.getRowOffset());
		typedQuery.setMaxResults(listQueryObject.getMaxRows());
		List<FacilityInventoryItem> items = typedQuery.getResultList();
		LOGGER.info("FacilityInventoryItem size(): "+items.size());
		List<Map<String, String>> valueObjects = new ArrayList<Map<String, String>>();
		for(FacilityInventoryItem item: items){
			Map<String, String> data = new HashMap<String, String>();
			
			InventoryItem inventoryItem = item.getInventoryItem();
			data.put("itemCode", inventoryItem.getCodeName());
			data.put("type", inventoryItem.getType());
			data.put("description", inventoryItem.getDescription());
			data.put("uom", inventoryItem.getUnitOfMeasurement());
			data.put("piecesPerUom", String.valueOf(inventoryItem.getPiecesPerUnitOfMeasurement()));
			data.put("min", String.valueOf(item.getMinValue()));
			data.put("max", String.valueOf(item.getMaxValue()));
			data.put("itemCost", String.valueOf(inventoryItem.getItemCost()));
			data.put("currency", String.valueOf(inventoryItem.getCurrency().getCode()));
			
			LOGGER.info("data: "+data.toString());
			valueObjects.add(data);
		}
		
		return valueObjects;
	}

	private long getFacilityItemCount(
			CriteriaBuilder cB, 
			Predicate conditions
	){
		CriteriaQuery<Long> countCriteriaQuery = cB.createQuery(Long.class);
		countCriteriaQuery.select(cB.count(countCriteriaQuery.from(FacilityInventoryItem.class)));
		getEntityManager().createQuery(countCriteriaQuery);
		if(null != conditions)
			countCriteriaQuery.where(conditions);
		
		return getEntityManager().createQuery(countCriteriaQuery).getSingleResult();
	}
}
