package com.dhl.inventory.repository.maintenance;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.components.FacilityCache;
import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.repository.AbstractNSISRepository;

@Repository
class FacilityRepositoryImpl extends AbstractNSISRepository<String, Facility> implements FacilityRepository {

	private static final Logger LOGGER = Logger.getLogger(FacilityRepositoryImpl.class);
	
	@Autowired
	private FacilityCache cache;
	
	@SuppressWarnings("unchecked")
	@Override
	public Facility findByCodeName(String facilityCode) {
		Facility facility = cache.getFacility(facilityCode);
		if(null == facility){
			List<Facility> facilities = new ArrayList<Facility>();
			
			facilities = getEntityManager()
				.createQuery("from Facility where codeName=:codeName")
				.setParameter("codeName", facilityCode)
				.getResultList();
			
			if (facilities.size() > 0) {
				facility = facilities.get(0);
			}
		}else{
			LOGGER.info("facility got from cache "+facilityCode);
		}
		
		return facility;
	}
	
	@Override
	public void save(Facility object) {
		persist(object);
	}

	@Override
	public Logger getLogger() {
		return LOGGER;
	}

}
