package com.dhl.inventory.repository.maintenance;

import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.maintenance.WarehouseLocation;
import com.dhl.inventory.repository.ImplementableRepository;

public interface WarehouseLocationRepository extends ImplementableRepository<WarehouseLocation>{
	WarehouseLocation findByFacilityAndRackingCode(Facility facility, String rackingCode);
}
