package com.dhl.inventory.repository;

import org.apache.log4j.Logger;

import com.dhl.inventory.domain.NsisDomainEntity;
import com.dhl.inventory.util.DateUtil;
import com.dhl.inventory.util.SearchType;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;

public abstract class AbstractNSISRepository<PK extends Serializable, T extends NsisDomainEntity>{
	
	@PersistenceContext
    private EntityManager em;
	
	private final Class<T> persistentClass;
	
	@SuppressWarnings("unchecked")
	public AbstractNSISRepository(){
		this.persistentClass =(Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
	}
	
	protected EntityManager getEntityManager(){
		return em;
	}

	public abstract Logger getLogger();
	
	public void remove(T entity) {
		em.remove(entity);		
    }

	public void persist(T entity) {
		em.persist(entity);
		em.flush();
	}
	
    public T getByKey(PK key) {
        return (T) em.find(persistentClass, key);
    }

	protected CriteriaBuilder createEntityCriteria(){
		return em.getCriteriaBuilder();
	}
	
	protected static boolean isNotEmpty(String value){
		return null != value && !value.isEmpty();
	}
	
	protected static boolean isNotEmpty(int value){
		return 0 != value;
	}
	
	protected static Predicate[] convertToPredicateArray(List<Predicate> conditions){
		Predicate[] conditionsArr = new Predicate[conditions.size()];
		conditions.toArray(conditionsArr);
		return conditionsArr;
	}
	
	protected static Predicate wrappedClause(CriteriaBuilder cb, SearchType logicalOper, Predicate... predicateToBeWrapped){
		Predicate wrappedPredicate = null;
		switch(logicalOper){
			case FILTER:
				wrappedPredicate = cb.and(predicateToBeWrapped);
				break;
			case TYPE_HEAD:
				wrappedPredicate = cb.or(predicateToBeWrapped);
				break;
		}
		
		return wrappedPredicate;
	}
	
	protected static String likeString(String value){
		return "%"+value+"%";
	}
	
	protected String getTableName() {
		return EntityTableNameExtractorUtil.getTableName(em, persistentClass);
	}
	
	protected Date eobDate(final Date date){
		return DateUtil.setTime(date, 23, 59, 59);
	}
	
	protected Date sobDate(final Date date){
		return DateUtil.setTime(date, 00, 00, 00);
	}
}
