package com.dhl.inventory.repository.warehouse.inventorymaintenance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.warehouse.inventorymaintenance.OnHandByItemCodeLQO;
import com.dhl.inventory.repository.AbstractNSISQueryOnlyRepository;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;

@Repository("warehouseOnHandSuppliesRepository")
class OnHandSuppliesRepositoryImpl extends AbstractNSISQueryOnlyRepository implements OnHandSuppliesRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(OnHandSuppliesRepositoryImpl.class);
	
	@Override
	public SearchListValueObject findAllOnHandSuppliesGroupedByItemCode(OnHandByItemCodeLQO commandObject) {
		return new SearchListValueObject(
				getSearchListByItemCode(commandObject), 
				getCountByItemCode(commandObject)
		);
	}
	
	@SuppressWarnings("unchecked")
	private List<Map<String, String>> getSearchListByItemCode(OnHandByItemCodeLQO commandObject){
		String queryString = getSearchListByItemCodeQueryString(commandObject);
		Query query = getEntityManager().createNativeQuery(queryString);
		query.setParameter("facilityCode", commandObject.getFacilityCode());
		query.setParameter("maxRow", commandObject.getMaxRows());
		query.setParameter("offset", commandObject.getRowOffset());
		
		Optional.object(commandObject.getItemCode()).ifPresent(itemCode -> {
			query.setParameter("itemCode", "%"+itemCode+"%");
		});
		
		List<Object[]> onHandSuppliesByItem = query.getResultList();
		
		List<Map<String, String>> returnValue = new ArrayList<>();
		Optional.object(onHandSuppliesByItem).ifPresent(supplyList -> {
			supplyList.stream().forEach(supplyRecord -> {
				Map<String, String> record = new HashMap<>();
				record.put("itemCode", (String)supplyRecord[0]);
				record.put("description", (String)supplyRecord[1]);
				record.put("uom", (String)supplyRecord[2]);
				
				int physicalStock = (Integer)supplyRecord[3];
				record.put("physicalStock", FormatterUtil.integerFormat(physicalStock));
				record.put("totalAmountInPHP", "0.00 PHP");
				record.put("totalAmountInSGD", "0.00 SGD");
				
				returnValue.add(record);
			});
		});
		
		return returnValue;
	}
	
	private int getCountByItemCode(OnHandByItemCodeLQO commandObject){
		String queryString = getCountByItemCodeQueryString(commandObject);
		Query query = getEntityManager().createNativeQuery(queryString);
		query.setParameter("facilityCode", commandObject.getFacilityCode());
		
		Optional.object(commandObject.getItemCode()).ifPresent(itemCode -> {
			query.setParameter("itemCode", "%"+itemCode+"%");
		});
		
		Integer count = (Integer) query.getSingleResult();
		
		return count;
	}
	
	private String getSearchListByItemCodeQueryString(OnHandByItemCodeLQO commandObject){
		StringBuilder queryStringBldr = new StringBuilder();
		queryStringBldr.append("SELECT TOP (:maxRow) xx_.code_name, xx_.description, xx_.uom, xx_.physical_count ");
		queryStringBldr.append("FROM ( ");
		queryStringBldr.append("SELECT mii_.code_name AS code_name, "+
				"mii_.description AS description, "+
				"mii_.uom AS uom, "+
				"SUM(ISNULL(CASE WHEN wsrg_.status = 'FOR DELIVERY' THEN 0 " + 
				"WHEN wsrg_.id is NULL THEN 0 "+
				"ELSE wsrr_.quantity "+
				"END,0)) "+ 
				"- "+
				"SUM(ISNULL(CASE WHEN wsri_.status = 'FOR PROCESSING' THEN 0 "+
				"WHEN wsri_.id is NULL THEN 0 "+
				"ELSE wsii_.qty_iss "+
				"END,0)) as 'physical_count', "+
				"ROW_NUMBER() OVER (ORDER BY mii_.code_name) AS row_num "+
				"FROM mn_fac_inv_item mfii_ "+
				"LEFT OUTER JOIN mn_fac mf_ "+
				"ON mfii_.facility_id = mf_.id "+
				"LEFT OUTER JOIN mn_inv_item mii_ "+
				"ON mfii_.item_id = mii_.id "+
				"LEFT OUTER JOIN wh_order_req_item wori_ "+
				"ON wori_.item_id = mfii_.id "+
				"LEFT OUTER JOIN wh_order_req wor_ "+
				"ON wor_.id = wori_.order_id AND wor_.facility_id = mf_.id "+
				"LEFT OUTER JOIN wh_stock_repl_grp wsrg_ "+
				"ON wor_.id = wsrg_.order_req_id "+
				"LEFT OUTER JOIN wh_stock_repl_rec wsrr_ "+
				"ON wsrr_.ori_id = wori_.id AND wsrr_.group_id = wsrg_.id "+
				"LEFT OUTER JOIN wh_stock_iss_item wsii_ "+
				"ON wsii_.supl_batch_id = wsrr_.id "+
				"LEFT OUTER JOIN wh_stock_iss_itemgrp wsiig_ "+
				"ON wsiig_.id = wsii_.grp_id "+
				"LEFT OUTER JOIN wh_stock_req_iss wsri_ "+
				"ON wsri_.id = wsiig_.iss_id "+
				"WHERE ( "+
				"wsiig_.item_code = mii_.code_name "+
				"OR wsiig_.item_code IS NULL "+
				") "+
				constructWhereClause(commandObject, false)+
				"GROUP BY mii_.code_name, "+
				"mii_.description, "+
				"mii_.uom ");
		queryStringBldr.append(") xx_ ");
		queryStringBldr.append("WHERE xx_.row_num >= :offset");
		
		return queryStringBldr.toString();
	}

	private String getCountByItemCodeQueryString(OnHandByItemCodeLQO commandObject){
		StringBuilder queryStringBldr = new StringBuilder();
		queryStringBldr.append("SELECT COUNT(mfii_.id) "+
				"FROM mn_fac_inv_item mfii_ "+
				"LEFT OUTER JOIN mn_fac mf_ "+
					"ON mfii_.facility_id = mf_.id "+
				"LEFT OUTER JOIN mn_inv_item mii_ "+
					"ON mfii_.item_id = mii_.id ");
		queryStringBldr.append(constructWhereClause(commandObject, true));
		return queryStringBldr.toString();
	}
	
	private String constructWhereClause(OnHandByItemCodeLQO commandObject, boolean isLead){
		StringBuilder whereClause = 
				new StringBuilder((!isLead? "AND " :"WHERE ")+"mf_.code_name=:facilityCode ");
		
		Optional.object(commandObject.getItemCode()).ifPresent(itemCode -> {
			whereClause.append("AND mii_.code_name LIKE :itemCode ");
		});
		
		return whereClause.toString();
	}

	/**
	 * Gets all active delivered item batches against there consumption and disposal records
	 * 
	 */
	@Override
	public SearchListValueObject findAllOnHandSuppliesGroupedByDeliveredItemBatch(
			OnHandByItemCodeLQO commandObject) {
		int countByBatch = 0;
		try{
			countByBatch = getCountByBatch(commandObject);
		}catch(NoResultException e){
			LOGGER.error("cannot count by batch defaulting to zero");
		}
		return new SearchListValueObject(
				getSearchListByBatch(commandObject), 
				countByBatch
		);
	}

	private int getCountByBatch(OnHandByItemCodeLQO commandObject) {
		String queryString = getCountByBatchQueryString(commandObject);
		Query query = getEntityManager().createNativeQuery(queryString);
		query.setParameter("facilityCode", commandObject.getFacilityCode());
		
		Optional.object(commandObject.getItemCode()).ifPresent(itemCode -> {
			query.setParameter("itemCode", "%"+itemCode+"%");
		});
		
		Integer count = (Integer) query.getSingleResult();
		
		return count;
	}

	@SuppressWarnings("unchecked")
	private List<Map<String, String>> getSearchListByBatch(OnHandByItemCodeLQO commandObject) {
		String queryString = getSearchListByBatchQueryString(commandObject);
		Query query = getEntityManager().createNativeQuery(queryString);
		query.setParameter("facilityCode", commandObject.getFacilityCode());
		query.setParameter("maxRow", commandObject.getMaxRows());
		query.setParameter("offset", commandObject.getRowOffset());
		
		Optional.object(commandObject.getItemCode()).ifPresent(itemCode -> {
			query.setParameter("itemCode", "%"+itemCode+"%");
		});
		
		List<Object[]> onHandSuppliesByItem = query.getResultList();
		
		List<Map<String, String>> returnValue = new ArrayList<>();
		Optional.object(onHandSuppliesByItem).ifPresent(supplyList -> {
			supplyList.stream().forEach(supplyRecord -> {
				Map<String, String> record = new HashMap<>();
				record.put("rackCode", (String)supplyRecord[0]);
				record.put("dateStored", (String)supplyRecord[1]);
				record.put("itemCode", (String)supplyRecord[2]);
				record.put("description", (String)supplyRecord[3]);
				record.put("uom", (String)supplyRecord[4]);
				int physicalStock = (Integer)supplyRecord[5];
				record.put("physicalStock", FormatterUtil.integerFormat(physicalStock));
				record.put("totalAmountInPHP", "0.00 PHP");
				record.put("totalAmountInSGD", "0.00 SGD");
				
				returnValue.add(record);
			});
		});
		
		return returnValue;
	}
	
	private String getCountByBatchQueryString(OnHandByItemCodeLQO commandObject) {
		StringBuilder queryStringBldr = new StringBuilder();
		queryStringBldr.append("SELECT COUNT(DISTINCT ( "+ 
				"CONCAT(xx_.code_name,xx_.rack_code,replace(convert(NVARCHAR, "+
		            "xx_.date_stored, "+
		            "101), "+
		            "' ', "+
		            "'/')))) "+
				"FROM (");
		queryStringBldr.append("SELECT "+
			        "mii_.code_name, mwl_.rack_code, wsrr_.date_stored "+ 
			    "FROM "+
			        "wh_stock_repl_rec wsrr_ "+
			    "LEFT OUTER JOIN "+
			        "wh_stock_repl_grp wsrg_  "+
			            "ON wsrr_.group_id = wsrg_.id "+ 
			    "LEFT OUTER JOIN "+
			        "wh_order_req_item wori_  "+
			            "ON wsrr_.ori_id = wori_.id  "+
			    "LEFT OUTER JOIN "+
			        "mn_fac_inv_item mfii_  "+
			            "ON wori_.item_id = mfii_.id  "+
			    "LEFT OUTER JOIN "+
			        "mn_inv_item mii_  "+
			            "ON mii_.id = mfii_.item_id  "+
			    "LEFT OUTER JOIN "+
			        "mn_fac mf_  "+
			            "ON mf_.id = mfii_.facility_id  "+
			    "LEFT OUTER JOIN "+
			        "wh_stock_iss_item wsii_  "+
			            "ON wsii_.supl_batch_id = wsrr_.id  "+
			    "LEFT OUTER JOIN "+
			        "wh_stock_iss_itemgrp wsiig_  "+
			            "ON wsiig_.id = wsii_.grp_id  "+
			    "LEFT OUTER JOIN "+
			        "wh_stock_req_iss wsri_  "+
			            "ON wsri_.id = wsiig_.iss_id "+
				"LEFT OUTER JOIN "+
			            "mn_wh_loc mwl_  "+
			                "ON wsrr_.rack_loc_id = mwl_.id  "+ 
			    "WHERE "+
			        "wsrg_.status <> 'FOR DELIVERY' ");
		queryStringBldr.append(constructWhereClause(commandObject, false));
		queryStringBldr.append("GROUP BY mwl_.rack_code, "+
									"wsrr_.date_stored, "+
						            "mii_.code_name, "+
						            "mii_.description, "+
						            "mii_.uom, "+
									"wsri_.status, "+
									"wsri_.id, "+
									"wsrg_.status, "+
									"wsrg_.id  "+
						    "HAVING ISNULL(CASE  "+
						                "WHEN wsrg_.id is NULL THEN 0  "+
						                "ELSE SUM(wsrr_.quantity)  "+
						            "END, "+
						            "0) - ISNULL(CASE  "+
						                "WHEN wsri_.status = 'FOR PROCESSING' THEN 0  "+
						                "WHEN wsri_.id is NULL THEN 0  "+
						                "ELSE SUM(wsii_.qty_iss)  "+
						            "END, "+
						            "0) > 0 ");
		queryStringBldr.append(") xx_ ");
		return queryStringBldr.toString();
	}
	
	private String getSearchListByBatchQueryString(OnHandByItemCodeLQO commandObject) {
		StringBuilder queryStringBldr = new StringBuilder();
		queryStringBldr.append("SELECT TOP (:maxRow) xx_.rack_code, "
				+ "xx_.date_stored, xx_.code_name, xx_.description, xx_.uom, xx_.physical_count ");
		queryStringBldr.append("FROM ( ");
		queryStringBldr.append("SELECT zz_.*, ROW_NUMBER() OVER (ORDER BY zz_.code_name) AS row_num FROM ( ");
		queryStringBldr.append("SELECT mwl_.rack_code AS rack_code, "+
				   "replace(convert(NVARCHAR, wsrr_.date_stored, 101), ' ', '/') AS date_stored, "+
				   "mii_.code_name, "+
				   "mii_.description, "+
				   "mii_.uom, "+
				   "ISNULL(CASE "+
					   "WHEN wsrg_.id is NULL THEN 0 "+
					   "ELSE SUM(wsrr_.quantity) "+
					   "END, "+
				   "0) "+ 
				   "- "+
				   "ISNULL(CASE "+
					   "WHEN wsri_.status = 'FOR PROCESSING' THEN 0 "+ 
					   "WHEN wsri_.id is NULL THEN 0 "+ 
					   "ELSE SUM(wsii_.qty_iss) "+ 
					   "END, "+
				   "0) as 'physical_count' "+
					"FROM wh_stock_repl_rec wsrr_ "+
					"LEFT OUTER JOIN wh_stock_repl_grp wsrg_ "+
						"ON wsrr_.group_id = wsrg_.id "+
					"LEFT OUTER JOIN wh_order_req_item wori_ "+
						"ON wsrr_.ori_id = wori_.id "+
					"LEFT OUTER JOIN mn_fac_inv_item mfii_ "+
						"ON wori_.item_id = mfii_.id "+
					"LEFT OUTER JOIN mn_inv_item mii_ "+
						"ON mii_.id = mfii_.item_id "+
					"LEFT OUTER JOIN mn_fac mf_ "+
						"ON mf_.id = mfii_.facility_id "+
					"LEFT OUTER JOIN mn_wh_loc mwl_ "+
						"ON wsrr_.rack_loc_id = mwl_.id "+
					"LEFT OUTER JOIN wh_stock_iss_item wsii_ "+
						"ON wsii_.supl_batch_id = wsrr_.id "+
					"LEFT OUTER JOIN wh_stock_iss_itemgrp wsiig_ "+
						"ON wsiig_.id = wsii_.grp_id "+
					"LEFT OUTER JOIN wh_stock_req_iss wsri_ "+
						"ON wsri_.id = wsiig_.iss_id "+
					"WHERE wsrg_.status <> 'FOR DELIVERY' ");
		queryStringBldr.append(constructWhereClause(commandObject, false));
		queryStringBldr.append("GROUP BY mwl_.rack_code, "+
						   "wsrr_.date_stored, "+
						   "mii_.code_name, "+
						   "mii_.description, "+
						   "mii_.uom, "+
						   "wsri_.status, "+
						   "wsri_.id, "+
						   "wsrg_.status, "+
						   "wsrg_.id "+  
					"HAVING ISNULL(CASE "+
		                "WHEN wsrg_.id is NULL THEN 0 "+
		                "ELSE SUM(wsrr_.quantity) "+
		            "END, "+
		            "0) - ISNULL(CASE "+
		                "WHEN wsri_.status = 'FOR PROCESSING' THEN 0 "+
		                "WHEN wsri_.id is NULL THEN 0 "+
		                "ELSE SUM(wsii_.qty_iss) "+
		            "END, "+
		            "0) > 0");
		queryStringBldr.append(") zz_ "+
				"GROUP BY zz_.rack_code, "+
				"zz_.date_stored, "+
				"zz_.code_name, "+
				"zz_.description, "+
				"zz_.uom, "+
				"zz_.physical_count");
		queryStringBldr.append(") xx_ ");
		queryStringBldr.append("WHERE xx_.row_num >= :offset");
		return queryStringBldr.toString();
	}

}
