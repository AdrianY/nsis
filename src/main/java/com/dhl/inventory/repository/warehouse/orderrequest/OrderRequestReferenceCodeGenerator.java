package com.dhl.inventory.repository.warehouse.orderrequest;

import com.dhl.inventory.repository.ReferenceCodeGenerator;

public interface OrderRequestReferenceCodeGenerator extends ReferenceCodeGenerator {

}
