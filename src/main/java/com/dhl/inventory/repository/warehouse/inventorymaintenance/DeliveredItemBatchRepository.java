package com.dhl.inventory.repository.warehouse.inventorymaintenance;

import java.util.List;

import com.dhl.inventory.domain.warehouse.inventorymaintenance.DeliveredItemBatch;
import com.dhl.inventory.domain.warehouse.inventorymaintenance.DeliveryReceipt;
import com.dhl.inventory.domain.warehouse.orderrequest.OrderRequestItem;
import com.dhl.inventory.repository.ImplementableRepository;

public interface DeliveredItemBatchRepository extends ImplementableRepository<DeliveredItemBatch> {
	List<DeliveredItemBatch> findAllDeliveredItemBatchByOrderItemAndGroup(OrderRequestItem orderRequestItem, DeliveryReceipt deliveryReceipt);
	
	DeliveredItemBatch findByBatchNumber(String batchNumber);
	
	DeliveredItemBatch findByBatchNumberAndFacilityCode(String batchNumber, String facilityCode);
	
	List<DeliveredItemBatch> findAllInFIFOFMannerByFacilityCodeAndItemCode(String facilityCodeName, String itemCode);
}
