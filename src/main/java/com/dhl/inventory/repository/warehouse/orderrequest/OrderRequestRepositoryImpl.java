package com.dhl.inventory.repository.warehouse.orderrequest;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.domain.maintenance.FacilityInventoryItem;
import com.dhl.inventory.domain.maintenance.InventoryItem;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.warehouse.orderrequest.OrderRequest;
import com.dhl.inventory.domain.warehouse.orderrequest.OrderRequestItem;
import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.warehouse.orderrequest.OrderRequestLQO;
import com.dhl.inventory.repository.AbstractNSISRepository;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.util.DateUtil;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.util.SearchType;
import com.dhl.inventory.util.Using;

@Repository
class OrderRequestRepositoryImpl extends AbstractNSISRepository<String, OrderRequest>
		implements OrderRequestRepository {
	
	private static final Logger LOGGER = Logger.getLogger(OrderRequestRepositoryImpl.class);
	
	@Autowired
	private UserRepository userRepo;

	@SuppressWarnings("unchecked")
	@Override
	public OrderRequest findByOrderNumber(String orderNumber) {
		List<OrderRequest> orderRequests = new ArrayList<>();
		
		orderRequests = getEntityManager()
			.createQuery("from OrderRequest where orderNumber=:orderNumber")
			.setParameter("orderNumber", orderNumber)
			.getResultList();
		
		OrderRequest returnValue = null;
		if (orderRequests.size() > 0) {
			returnValue = orderRequests.get(0);
		}
		
		return returnValue;
	}

	@Override
	public Logger getLogger() {
		return LOGGER;
	}

	@Override
	public void save(OrderRequest entity) {
		persist(entity);
	}
	
	public void delete(OrderRequest entity) {
		remove(entity);
	}

	@Override
	public SearchListValueObject findAll(AbstractSearchListQueryObject commandObject, SearchType mode) {
		CriteriaBuilder cB = createEntityCriteria();
		CriteriaQuery<Tuple> criteriaQuery = cB.createTupleQuery();
		Root<OrderRequest> orderRequestRoot = criteriaQuery.from(OrderRequest.class);
		
		OrderRequestLQO orderRequestLQO = (OrderRequestLQO) commandObject;
		Predicate conditions = getOrderRequestSearchConditions(cB,orderRequestLQO,orderRequestRoot, mode);
		
		Long totalCount = 0L;
		if(!SearchType.TYPE_HEAD.equals(mode)){
			totalCount = getOrderRequestCount(cB, conditions);
		}
		List<Map<String, String>> valueObjects = 
				getOrderRequestList(
						cB,
						orderRequestLQO,
						criteriaQuery,
						orderRequestRoot, 
						conditions,
						mode
				);
		
		return new SearchListValueObject(valueObjects, totalCount);
	}

	private List<Map<String, String>> getOrderRequestList(
			CriteriaBuilder cB, 
			OrderRequestLQO listQueryObject,
			CriteriaQuery<Tuple> multiSelectClause, 
			Root<OrderRequest> root, 
			Predicate conditions,
			SearchType mode
	) {
		Join<OrderRequest, OrderRequestItem> ori = root.join("orderItems",JoinType.LEFT);
		Join<OrderRequestItem, FacilityInventoryItem> fii = ori.join("item",JoinType.LEFT);
		Join<FacilityInventoryItem, InventoryItem> ii = fii.join("inventoryItem",JoinType.LEFT);
		ori.alias("orderRequestItems_");
		fii.alias("facilityInventoryItems_");
		ii.alias("inventoryItems_");
		multiSelectClause.select(
			cB.tuple(
				root.get("orderNumber").alias("orderNumber"),
				root.get("status").alias("status"),
				root.get("orderDate").alias("orderDate"),
				root.get("orderType").alias("orderType"),
				root.get("orderById").alias("orderById"),
				root.get("orderCycle").alias("orderCycle"),
				root.get("currency").alias("currency"),
				cB.sum(cB.prod(ori.get("quantityByUOM"), ii.get("itemCost"))).alias("totalRequestCost")
			)
		);
		
		multiSelectClause.groupBy(
				root.get("orderNumber"),
				root.get("status"),
				root.get("orderDate"),
				root.get("orderType"),
				root.get("orderById"),
				root.get("orderCycle"),
				root.get("currency"),
				root.get(listQueryObject.getSortField())
		);
		
		if(!SearchType.TYPE_HEAD.equals(mode)){
			switch(listQueryObject.getSortOrder()){
				case AbstractSearchListQueryObject.SORT_ORDER_ASC:
					multiSelectClause.orderBy(cB.asc(root.get(listQueryObject.getSortField())));
					break;
				case AbstractSearchListQueryObject.SORT_ORDER_DESC:
					multiSelectClause.orderBy(cB.desc(root.get(listQueryObject.getSortField())));
					break;
			}
		}
			
		if(null != conditions)
			multiSelectClause.where(conditions);
		
		TypedQuery<Tuple> typedQuery = getEntityManager().createQuery(multiSelectClause);
		typedQuery.setFirstResult(listQueryObject.getRowOffset());
		typedQuery.setMaxResults(listQueryObject.getMaxRows());
		List<Tuple> orderRequestsAggregate = typedQuery.getResultList();
		
		LOGGER.info("OrderRequest size(): "+orderRequestsAggregate.size());
		List<Map<String, String>> valueObjects = new ArrayList<Map<String, String>>();
		for(Tuple or: orderRequestsAggregate){
			Map<String, String> data = new HashMap<String, String>();
			
			data.put("orderType", (String)or.get("orderType"));
			data.put("orderNumber", (String)or.get("orderNumber"));
			data.put("orderCycle", (String)or.get("orderCycle"));
			data.put("status", (String)or.get("status"));
			
			Optional.object((Date)or.get("orderDate")).ifPresent(od -> {
				String orderDate = FormatterUtil.fullDateFormat(od);
				data.put("orderDate", orderDate);
			});
			
			Optional.object((String) or.get("orderById")).ifPresent(val -> {
				NsisUser orderBy = userRepo.getByKey(val);
				data.put("orderBy", orderBy.getFullName());
			});
			
			String totalRequestCost = FormatterUtil.currencyFormat((BigDecimal)or.get("totalRequestCost"));
			String totalRequestCostStr =  totalRequestCost + " " + (String)or.get("currency");
			data.put("totalRequestCost", totalRequestCostStr);
			
			LOGGER.info("data: "+data.toString());
			valueObjects.add(data);
		}
		
		return valueObjects;
	}

	private Long getOrderRequestCount(CriteriaBuilder cB, Predicate conditions) {
		CriteriaQuery<Long> countCriteriaQuery = cB.createQuery(Long.class);
		countCriteriaQuery.select(cB.count(countCriteriaQuery.from(OrderRequest.class)));
		getEntityManager().createQuery(countCriteriaQuery);
		if(null != conditions)
			countCriteriaQuery.where(conditions);
		
		return getEntityManager().createQuery(countCriteriaQuery).getSingleResult();
	}

	private Predicate getOrderRequestSearchConditions(
			CriteriaBuilder cB, OrderRequestLQO orderRequestLQO,
			Root<OrderRequest> orderRequestRoot, SearchType mode
	) {

		List<Predicate> predicates = new ArrayList<>();
		
		Optional.object(orderRequestLQO.getOrderNumber()).ifPresent(val -> {
			predicates.add(cB.like(orderRequestRoot.get("orderNumber"), likeString(val)));
		});
		
		Optional.object(orderRequestLQO.getStatus()).ifPresent(val -> {
			predicates.add(cB.equal(orderRequestRoot.get("status"), val));
		});
		
		try {
			
			Optional<Date> toDate = DateUtil.parsePossibleShortDate(orderRequestLQO.getToDate());
			Optional<Date> fromDate = DateUtil.parsePossibleShortDate(orderRequestLQO.getFromDate());
			
			if(toDate.hasContent() && fromDate.hasContent()){
				predicates.add(cB.between(
						orderRequestRoot.get("orderDate"), 
						sobDate(fromDate.getValue()), 
						eobDate(toDate.getValue())
				));	
			}else if(toDate.hasContent()){
				predicates.add(cB.lessThanOrEqualTo(
						orderRequestRoot.get("orderDate"), eobDate(toDate.getValue())));
			}else if(fromDate.hasContent()){
				predicates.add(cB.greaterThanOrEqualTo(
						orderRequestRoot.get("orderDate"), sobDate(fromDate.getValue())));
			}
			
		} catch (ParseException e) {
			LOGGER.error(e);
		}
		
		Predicate logicalGroupPredicate = 
				Using
				.thisObject(predicates)
				.deriveIfPresent(p -> {
					Predicate[] logicallyGroupedPredicateArr = convertToPredicateArray(p);
					return wrappedClause(cB, mode, logicallyGroupedPredicateArr);
				});
		
		Predicate mandatoryPredicate = 
				Using.thisObject(orderRequestLQO.getStatuses())
				.deriveIfPresent(statuses -> {
					Expression<String> status = orderRequestRoot.get("status");
					return status.in(statuses);
				});
		
		Predicate returnValue = null;
		if(null!=mandatoryPredicate && null != logicalGroupPredicate){
			returnValue = cB.and(mandatoryPredicate, logicalGroupPredicate);
		}else if(null!=mandatoryPredicate){
			returnValue = mandatoryPredicate;
		}else if(null!=logicalGroupPredicate){
			returnValue = logicalGroupPredicate;
		}
		
		return returnValue;
	}

	@Override
	public int deleteAllByOrderRequestItems(OrderRequest or) {
		Query query = getEntityManager().createQuery("DELETE FROM OrderRequestItem WHERE order=:orderRequest");
		return query.setParameter("orderRequest", or).executeUpdate();
	}


}
