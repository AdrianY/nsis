package com.dhl.inventory.repository.warehouse.inventorymaintenance;

import com.dhl.inventory.domain.warehouse.inventorymaintenance.DeliveryReceipt;
import com.dhl.inventory.repository.ImplementableRepository;

public interface DeliveryReceiptRepository extends ImplementableRepository<DeliveryReceipt> {
	DeliveryReceipt findByOrderNumber(String orderNumber);
	
	int deleteDeliveredItemBatches(DeliveryReceipt dr);
}
