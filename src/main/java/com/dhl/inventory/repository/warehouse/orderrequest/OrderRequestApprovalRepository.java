package com.dhl.inventory.repository.warehouse.orderrequest;

import com.dhl.inventory.domain.warehouse.orderrequest.OrderRequest;
import com.dhl.inventory.domain.warehouse.orderrequest.OrderRequestApproval;
import com.dhl.inventory.repository.ImplementableRepository;

public interface OrderRequestApprovalRepository extends ImplementableRepository<OrderRequestApproval> {
	OrderRequestApproval findByOrderRequest(OrderRequest orderRequest);
}
