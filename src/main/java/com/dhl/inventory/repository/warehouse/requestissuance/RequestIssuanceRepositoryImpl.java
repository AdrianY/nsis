package com.dhl.inventory.repository.warehouse.requestissuance;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;
import com.dhl.inventory.domain.warehouse.requestissuance.RequestIssuance;
import com.dhl.inventory.domain.warehouse.requestissuance.SuppliedItemsGroup;
import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.warehouse.requestissuance.RequestIssuanceLQO;
import com.dhl.inventory.repository.AbstractNSISRepository;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.util.SearchType;
import com.dhl.inventory.util.Using;

@Repository("warehouseRequestIssuanceRepository")
class RequestIssuanceRepositoryImpl extends AbstractNSISRepository<String, RequestIssuance> implements RequestIssuanceRepository {

	private static final Logger LOGGER = Logger.getLogger(RequestIssuanceRepositoryImpl.class);
	
	@Autowired
	private UserRepository userRepo;
	
	@Override
	public void save(RequestIssuance entity) {
		persist(entity);
	}

	@Override
	public void delete(RequestIssuance entity) {
		remove(entity);
	}

	@Override
	public RequestIssuance getByKey(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchListValueObject findAll(AbstractSearchListQueryObject commandObject, SearchType mode) {
		CriteriaBuilder cB = createEntityCriteria();
		CriteriaQuery<Tuple> criteriaQuery = cB.createTupleQuery();
		Root<RequestIssuance> supplyRequestRoot = criteriaQuery.from(RequestIssuance.class);
		
		RequestIssuanceLQO supplyRequestLQO = (RequestIssuanceLQO) commandObject;
		Predicate conditions = getRequestIssuanceSearchConditions(cB,supplyRequestLQO,supplyRequestRoot, mode);
		
		Long totalCount = 0L;
		if(!SearchType.TYPE_HEAD.equals(mode)){
			totalCount = getRequestIssuanceSearchConditions(supplyRequestRoot,criteriaQuery, cB, conditions);
		}
		List<Map<String, String>> valueObjects = 
				getRequestIssuanceList(
						cB,
						supplyRequestLQO,
						criteriaQuery,
						supplyRequestRoot, 
						conditions,
						mode
				);
		
		return new SearchListValueObject(valueObjects, totalCount);
	}

	private Long getRequestIssuanceSearchConditions(Root<RequestIssuance> root,
			CriteriaQuery<Tuple> multiSelectClause, CriteriaBuilder cB, Predicate conditions) {
		Join<RequestIssuance, SupplyRequest> supplyRequestPath = root.join("supplyRequest");
		Join<SupplyRequest, Facility> facilityPath = supplyRequestPath.join("facility");
		supplyRequestPath.alias("supplyRequest_");
		facilityPath.alias("facility_");
		
		multiSelectClause.select(cB.tuple(cB.count(root).alias("count")));
		if(null != conditions)
			multiSelectClause.where(conditions);
		
		Tuple singleResult = getEntityManager().createQuery(multiSelectClause).getSingleResult();
		
		return singleResult.get("count", Long.class);
	}

	private List<Map<String, String>> getRequestIssuanceList(
			CriteriaBuilder cB, RequestIssuanceLQO listQueryObject,
			CriteriaQuery<Tuple> multiSelectClause, Root<RequestIssuance> root, 
			Predicate conditions, SearchType mode
	) {
		Join<RequestIssuance, SupplyRequest> supplyRequestPath = root.join("supplyRequest");
		Join<SupplyRequest, Facility> facilityPath = supplyRequestPath.join("facility");
		supplyRequestPath.alias("supplyRequest_");
		facilityPath.alias("facility_");
		
		multiSelectClause.select(
			cB.tuple(
				supplyRequestPath.get("requestNumber").alias("requestNumber"),
				supplyRequestPath.get("requestDate").alias("requestDate"),
				facilityPath.get("codeName").alias("facilityCodeName"),
				supplyRequestPath.get("requestById").alias("requestById"),
				root.get("status").alias("status")
			)
		);
		
		if(!SearchType.TYPE_HEAD.equals(mode)){
			switch(listQueryObject.getSortOrder()){
				case AbstractSearchListQueryObject.SORT_ORDER_ASC:
					multiSelectClause.orderBy(cB.asc(root.get(listQueryObject.getSortField())));
					break;
				case AbstractSearchListQueryObject.SORT_ORDER_DESC:
					multiSelectClause.orderBy(cB.desc(root.get(listQueryObject.getSortField())));
					break;
			}
		}
			
		if(null != conditions)
			multiSelectClause.where(conditions);
		
		TypedQuery<Tuple> typedQuery = getEntityManager().createQuery(multiSelectClause);
		typedQuery.setFirstResult(listQueryObject.getRowOffset());
		typedQuery.setMaxResults(listQueryObject.getMaxRows());
		List<Tuple> requestIssuancesAggregate = typedQuery.getResultList();
		
		LOGGER.info("RequestIssuances size(): "+requestIssuancesAggregate.size());
		List<Map<String, String>> valueObjects = new ArrayList<Map<String, String>>();
		for(Tuple or: requestIssuancesAggregate){
			Map<String, String> data = new HashMap<String, String>();
			
			data.put("requestNumber", (String)or.get("requestNumber"));
			data.put("status", (String)or.get("status"));
			data.put("facilityCodeName", (String)or.get("facilityCodeName"));
			
			Optional.object((Date)or.get("requestDate")).ifPresent(od -> {
				String requestDate = FormatterUtil.fullDateFormat(od);
				data.put("requestDate", requestDate);
			});
			
			Optional.object((String) or.get("requestById")).ifPresent(val -> {
				NsisUser requestBy = userRepo.getByKey(val);
				data.put("requestBy", requestBy.getFullName());
			});
			
			valueObjects.add(data);
		}
		
		return valueObjects;
	}

	private Predicate getRequestIssuanceSearchConditions(
			CriteriaBuilder cB, RequestIssuanceLQO listQueryObject,
			Root<RequestIssuance> root, SearchType mode
	) {
		List<Predicate> predicates = new ArrayList<>();
		Join<RequestIssuance, SupplyRequest> supplyRequestPath = root.join("supplyRequest");
		Join<SupplyRequest, Facility> facilityPath = supplyRequestPath.join("facility");
		supplyRequestPath.alias("supplyRequest_");
		facilityPath.alias("facility_");
		
		Optional.object(listQueryObject.getRequestNumber()).ifPresent(val -> {
			predicates.add(cB.like(root.get("requestNumber"), likeString(val)));
		});
		
		Optional.object(listQueryObject.getStatus()).ifPresent(val -> {
			predicates.add(cB.equal(root.get("status"), val));
		});
		
		Optional.object(listQueryObject.getFacilityCodeName()).ifPresent(val -> {
			predicates.add(cB.equal(facilityPath.get("codeName"), val));
		});
		
		Predicate logicalGroupPredicate = 
				Using
				.thisObject(predicates)
				.deriveIfPresent(p -> {
					Predicate[] logicallyGroupedPredicateArr = convertToPredicateArray(p);
					return wrappedClause(cB, mode, logicallyGroupedPredicateArr);
				});
		
		return Using.thisObject(logicalGroupPredicate)
				.deriveIfPresent(val -> {return val;});
	}

	@SuppressWarnings("unchecked")
	@Override
	public RequestIssuance findByRequestNumber(String requestNumber) {
		List<RequestIssuance> requestIssuances = new ArrayList<>();
		
		StringBuilder queryStrBldr = 
				new StringBuilder("select p from WarehouseRequestIssuance p ")
				.append("inner join p.supplyRequest sr ")
				.append("where sr.requestNumber=:rn ");
		
		requestIssuances = getEntityManager()
			.createQuery(queryStrBldr.toString())
			.setParameter("rn", requestNumber)
			.getResultList();
		
		RequestIssuance returnValue = null;
		if (requestIssuances.size() > 0) {
			returnValue = requestIssuances.get(0);
		}
		
		return returnValue;
	}

	@Override
	public int deleteAllSupplyItemsByRequestIssuance(RequestIssuance requestIssuance) {
		int numberOfItemsRemoved = 0;
		if(null != requestIssuance){
			for(SuppliedItemsGroup siGrp: requestIssuance.getSuppliedItemsGroup()){
				Query query = getEntityManager().createQuery("DELETE FROM WarehouseSuppliedItems "
						+ "WHERE suppliedItemsGroup=:siGrp");
				numberOfItemsRemoved =  query.setParameter("siGrp", siGrp).executeUpdate();
			}
		}
		return numberOfItemsRemoved;
	}

	@Override
	public Logger getLogger() {
		// TODO Auto-generated method stub
		return null;
	}

}
