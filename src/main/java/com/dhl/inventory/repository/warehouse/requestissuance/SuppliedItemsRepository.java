package com.dhl.inventory.repository.warehouse.requestissuance;

import java.util.List;

import com.dhl.inventory.domain.warehouse.inventorymaintenance.DeliveredItemBatch;
import com.dhl.inventory.domain.warehouse.requestissuance.SuppliedItems;

public interface SuppliedItemsRepository {
	List<SuppliedItems> findAllSuppliedItemsUsingDeliveredItemBatch(DeliveredItemBatch deliveredItemBatch, List<SuppliedItems> excludedSuppliedItems);
	
	List<SuppliedItems> findAllSuppliedItemsUsingDeliveredItemBatchForRequest(DeliveredItemBatch deliveredItemBatch, String requestNumber);

	SuppliedItems findSuppliedItem(String requestNumber,String batchNumber);
}
