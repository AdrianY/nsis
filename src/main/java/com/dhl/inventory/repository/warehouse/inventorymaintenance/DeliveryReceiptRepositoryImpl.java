package com.dhl.inventory.repository.warehouse.inventorymaintenance;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.domain.maintenance.FacilityInventoryItem;
import com.dhl.inventory.domain.maintenance.InventoryItem;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.warehouse.inventorymaintenance.DeliveryReceipt;
import com.dhl.inventory.domain.warehouse.orderrequest.OrderRequest;
import com.dhl.inventory.domain.warehouse.orderrequest.OrderRequestItem;
import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.warehouse.inventorymaintenance.DeliveryReceiptLQO;
import com.dhl.inventory.repository.AbstractNSISRepository;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.util.SearchType;
import com.dhl.inventory.util.Using;

@Repository("warehouseDeliveryReceiptRepository")
class DeliveryReceiptRepositoryImpl extends AbstractNSISRepository<String, DeliveryReceipt> implements DeliveryReceiptRepository {

	private static final Logger LOGGER = Logger.getLogger(DeliveryReceiptRepositoryImpl.class);
	
	@Autowired
	private UserRepository userRepo;
	
	@Override
	public void save(DeliveryReceipt entity) {
		persist(entity);
	}

	@Override
	public void delete(DeliveryReceipt object) {
		// TODO Auto-generated method stub

	}

	@Override
	public DeliveryReceipt getByKey(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchListValueObject findAll(AbstractSearchListQueryObject commandObject, SearchType mode) {
		CriteriaBuilder cB = createEntityCriteria();
		CriteriaQuery<Tuple> criteriaQuery = cB.createTupleQuery();
		Root<DeliveryReceipt> orderRequestRoot = criteriaQuery.from(DeliveryReceipt.class);
		
		DeliveryReceiptLQO orderRequestLQO = (DeliveryReceiptLQO) commandObject;
		Predicate conditions = getDeliveryReceiptSearchConditions(cB,orderRequestLQO,orderRequestRoot, mode);
		
		Long totalCount = 0L;
		if(!SearchType.TYPE_HEAD.equals(mode)){
			totalCount = getDeliveryReceiptCount(cB, conditions);
		}
		List<Map<String, String>> valueObjects = 
				getDeliveryReceiptList(
						cB,
						orderRequestLQO,
						criteriaQuery,
						orderRequestRoot, 
						conditions,
						mode
				);
		
		return new SearchListValueObject(valueObjects, totalCount);
	}

	private List<Map<String, String>> getDeliveryReceiptList(
			CriteriaBuilder cB, 
			DeliveryReceiptLQO listQueryObject,
			CriteriaQuery<Tuple> multiSelectClause, 
			Root<DeliveryReceipt> root, 
			Predicate conditions,
			SearchType mode
	) {
		Join<DeliveryReceipt, OrderRequest> or = root.join("orderRequest",JoinType.LEFT);
		Join<OrderRequest, OrderRequestItem> ori = or.join("orderItems",JoinType.LEFT);
		Join<OrderRequestItem, FacilityInventoryItem> fii = ori.join("item",JoinType.LEFT);
		Join<FacilityInventoryItem, InventoryItem> ii = fii.join("inventoryItem",JoinType.LEFT);
		or.alias("orderRequest_");
		ori.alias("orderRequestItems_");
		fii.alias("facilityInventoryItems_");
		ii.alias("inventoryItems_");
		
		multiSelectClause.select(
			cB.tuple(
				or.get("orderNumber").alias("orderNumber"),
				root.get("status").alias("status"),
				or.get("orderDate").alias("orderDate"),
				or.get("orderType").alias("orderType"),
				or.get("orderById").alias("orderById"),
				or.get("orderCycle").alias("orderCycle"),
				or.get("currency").alias("currency"),
				cB.sum(cB.prod(ori.get("quantityByUOM"), ii.get("itemCost"))).alias("totalRequestCost")
			)
		);
		
		multiSelectClause.groupBy(
				or.get("orderNumber"),
				root.get("status"),
				or.get("orderDate"),
				or.get("orderType"),
				or.get("orderById"),
				or.get("orderCycle"),
				or.get("currency"),
				root.get(listQueryObject.getSortField())
		);
		
		if(!SearchType.TYPE_HEAD.equals(mode)){
			switch(listQueryObject.getSortOrder()){
				case AbstractSearchListQueryObject.SORT_ORDER_ASC:
					multiSelectClause.orderBy(cB.asc(root.get(listQueryObject.getSortField())));
					break;
				case AbstractSearchListQueryObject.SORT_ORDER_DESC:
					multiSelectClause.orderBy(cB.desc(root.get(listQueryObject.getSortField())));
					break;
			}
		}
			
		if(null != conditions)
			multiSelectClause.where(conditions);
		
		TypedQuery<Tuple> typedQuery = getEntityManager().createQuery(multiSelectClause);
		typedQuery.setFirstResult(listQueryObject.getRowOffset());
		typedQuery.setMaxResults(listQueryObject.getMaxRows());
		List<Tuple> deliveyrReceiptsAggregate = typedQuery.getResultList();
		
		LOGGER.info("OrderRequest size(): "+deliveyrReceiptsAggregate.size());
		List<Map<String, String>> valueObjects = new ArrayList<Map<String, String>>();
		for(Tuple deliveryReceipt: deliveyrReceiptsAggregate){
			Map<String, String> data = new HashMap<String, String>();
			
			data.put("orderType", (String)deliveryReceipt.get("orderType"));
			data.put("orderNumber", (String)deliveryReceipt.get("orderNumber"));
			data.put("orderCycle", (String)deliveryReceipt.get("orderCycle"));
			data.put("status", (String)deliveryReceipt.get("status"));
			
			Optional.object((Date)deliveryReceipt.get("orderDate")).ifPresent(od -> {
				String orderDate = FormatterUtil.fullDateFormat(od);
				data.put("orderDate", orderDate);
			});
			
			Optional.object((String) deliveryReceipt.get("orderById")).ifPresent(val -> {
				NsisUser orderBy = userRepo.getByKey(val);
				data.put("orderBy", orderBy.getFullName());
			});
			
			String totalRequestCost = FormatterUtil.currencyFormat((BigDecimal)deliveryReceipt.get("totalRequestCost"));
			String totalRequestCostStr =  totalRequestCost + " " + (String)deliveryReceipt.get("currency");
			data.put("totalRequestCost", totalRequestCostStr);
			
			LOGGER.info("data: "+data.toString());
			valueObjects.add(data);
		}
		
		return valueObjects;
	}

	private Long getDeliveryReceiptCount(CriteriaBuilder cB, Predicate conditions) {
		CriteriaQuery<Long> countCriteriaQuery = cB.createQuery(Long.class);
		countCriteriaQuery.select(cB.count(countCriteriaQuery.from(DeliveryReceipt.class)));
		getEntityManager().createQuery(countCriteriaQuery);
		if(null != conditions)
			countCriteriaQuery.where(conditions);
		
		return getEntityManager().createQuery(countCriteriaQuery).getSingleResult();
	}

	private Predicate getDeliveryReceiptSearchConditions(
			CriteriaBuilder cB, 
			DeliveryReceiptLQO deliveryReceiptLQO,
			Root<DeliveryReceipt> root, 
			SearchType mode
	) {
		
		List<Predicate> predicates = new ArrayList<>();
		
		Optional.object(deliveryReceiptLQO.getOrderNumber()).ifPresent(val -> {
			Join<DeliveryReceipt, OrderRequest> or = root.join("orderRequest",JoinType.LEFT);
			predicates.add(cB.like(or.get("orderNumber"), likeString(val)));
		});
		
		Optional.object(deliveryReceiptLQO.getStatus()).ifPresent(val -> {
			predicates.add(cB.equal(root.get("status"), val));
		});
		
		Predicate logicalGroupPredicate = 
				Using
				.thisObject(predicates)
				.deriveIfPresent(p -> {
					Predicate[] logicallyGroupedPredicateArr = convertToPredicateArray(p);
					return wrappedClause(cB, mode, logicallyGroupedPredicateArr);
				});
		
		return logicalGroupPredicate;
	}

	@Override
	public Logger getLogger() {
		return LOGGER;
	}

	@SuppressWarnings("unchecked")
	@Override
	public DeliveryReceipt findByOrderNumber(String orderNumber) {
		List<DeliveryReceipt> deliveryReceipts = new ArrayList<DeliveryReceipt>();
		
		StringBuilder queryStrBldr = 
				new StringBuilder("select p from DeliveryReceipt p ");
		queryStrBldr.append("inner join p.orderRequest a ");
		queryStrBldr.append("where a.orderNumber=:orderNumber");
		
		deliveryReceipts = getEntityManager()
				.createQuery(queryStrBldr.toString())
				.setParameter("orderNumber", orderNumber)
				.getResultList();
		
		DeliveryReceipt returnValue = null;
		if (deliveryReceipts.size() > 0) {
			returnValue = deliveryReceipts.get(0);
		}
		
		return returnValue;
	}

	@Override
	public int deleteDeliveredItemBatches(DeliveryReceipt dr) {
		Query query = getEntityManager().createQuery("DELETE FROM DeliveredItemBatch WHERE group=:deliveryReceipt");
		return query.setParameter("deliveryReceipt", dr).executeUpdate();
	}

}
