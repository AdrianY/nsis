package com.dhl.inventory.repository.warehouse.requestissuance;

import com.dhl.inventory.domain.warehouse.requestissuance.RequestIssuance;
import com.dhl.inventory.domain.warehouse.requestissuance.SuppliedItemsGroup;
import com.dhl.inventory.repository.ImplementableRepository;

public interface RequestIssuanceRepository extends ImplementableRepository<RequestIssuance> {
	RequestIssuance findByRequestNumber(String requestNumber);
	
	int deleteAllSupplyItemsByRequestIssuance(RequestIssuance requestIssuance);
}
