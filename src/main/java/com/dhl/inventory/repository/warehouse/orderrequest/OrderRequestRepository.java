package com.dhl.inventory.repository.warehouse.orderrequest;

import com.dhl.inventory.domain.warehouse.orderrequest.OrderRequest;
import com.dhl.inventory.repository.ImplementableRepository;

public interface OrderRequestRepository extends ImplementableRepository<OrderRequest>{
	
	OrderRequest findByOrderNumber(String orderNumber);
	
	int deleteAllByOrderRequestItems(OrderRequest or);
}
