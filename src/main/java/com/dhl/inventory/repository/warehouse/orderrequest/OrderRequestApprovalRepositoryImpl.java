package com.dhl.inventory.repository.warehouse.orderrequest;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.domain.warehouse.orderrequest.OrderRequest;
import com.dhl.inventory.domain.warehouse.orderrequest.OrderRequestApproval;
import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.repository.AbstractNSISRepository;
import com.dhl.inventory.util.SearchType;

@Repository
class OrderRequestApprovalRepositoryImpl extends AbstractNSISRepository<String, OrderRequestApproval> implements OrderRequestApprovalRepository {

	private static final Logger LOGGER = Logger.getLogger(OrderRequestApprovalRepositoryImpl.class);
	
	@Override
	public void save(OrderRequestApproval entity) {
		persist(entity);
	}

	@Override
	public void delete(OrderRequestApproval object) {
		remove(object);
	}

	@Override
	public OrderRequestApproval getByKey(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchListValueObject findAll(AbstractSearchListQueryObject commandObject, SearchType mode) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public OrderRequestApproval findByOrderRequest(OrderRequest orderRequest) {
		List<OrderRequestApproval> orderRequestsApprovals = new ArrayList<>();
		
		orderRequestsApprovals = getEntityManager()
			.createQuery("from OrderRequestApproval where orderRequest=:orderRequest")
			.setParameter("orderRequest", orderRequest)
			.getResultList();
		
		OrderRequestApproval returnValue = null;
		if (orderRequestsApprovals.size() > 0) {
			returnValue = orderRequestsApprovals.get(0);
		}
		
		return returnValue;
	}

	@Override
	public Logger getLogger() {
		return LOGGER;
	}

}
