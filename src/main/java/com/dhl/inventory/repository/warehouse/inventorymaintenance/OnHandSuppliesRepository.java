package com.dhl.inventory.repository.warehouse.inventorymaintenance;

import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.warehouse.inventorymaintenance.OnHandByItemCodeLQO;

public interface OnHandSuppliesRepository {
	SearchListValueObject findAllOnHandSuppliesGroupedByItemCode(OnHandByItemCodeLQO commandObject);

	SearchListValueObject findAllOnHandSuppliesGroupedByDeliveredItemBatch(OnHandByItemCodeLQO commandObject);
}
