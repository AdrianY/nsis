package com.dhl.inventory.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;

import com.dhl.inventory.util.SearchType;

public abstract class AbstractNSISQueryOnlyRepository {
	
	@PersistenceContext
    private EntityManager em;
	
	protected EntityManager getEntityManager(){
		return em;
	}
	
	protected CriteriaBuilder createEntityCriteria(){
		return em.getCriteriaBuilder();
	}
	
	protected static boolean isNotEmpty(String value){
		return null != value && !value.isEmpty();
	}
	
	protected static boolean isNotEmpty(int value){
		return 0 != value;
	}
	
	protected static Predicate[] convertToPredicateArray(List<Predicate> conditions){
		Predicate[] conditionsArr = new Predicate[conditions.size()];
		conditions.toArray(conditionsArr);
		return conditionsArr;
	}
	
	protected static Predicate wrappedClause(CriteriaBuilder cb, SearchType logicalOper, Predicate... predicateToBeWrapped){
		Predicate wrappedPredicate = null;
		switch(logicalOper){
			case FILTER:
				wrappedPredicate = cb.and(predicateToBeWrapped);
				break;
			case TYPE_HEAD:
				wrappedPredicate = cb.or(predicateToBeWrapped);
				break;
		}
		
		return wrappedPredicate;
	}
	
	protected static String likeString(String value){
		return "%"+value+"%";
	}
}
