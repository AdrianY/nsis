package com.dhl.inventory.repository.servicecenter.inventorymaintenance;

import static com.dhl.inventory.components.BatchNumberUtilityBean.BNC_DATE_STORED;
import static com.dhl.inventory.components.BatchNumberUtilityBean.BNC_ITEM_CODE;
import static com.dhl.inventory.components.BatchNumberUtilityBean.BNC_RACK_CODE;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.components.BatchNumberUtilityBean;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.DeliveredItemBatch;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.DeliveryReceipt;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequestItem;
import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.repository.AbstractNSISRepository;
import com.dhl.inventory.util.DateUtil;
import com.dhl.inventory.util.SearchType;

@Repository("serviceCenterDeliveredItemBatchRepository")
class DeliveredItemBatchRepositoryImpl extends AbstractNSISRepository<String, DeliveredItemBatch> implements DeliveredItemBatchRepository {

	private static final Logger LOGGER = Logger.getLogger(DeliveredItemBatchRepositoryImpl.class);
	
	@Autowired
	private BatchNumberUtilityBean batchNumberAnalyzer;
	
	@Override
	public void save(DeliveredItemBatch entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(DeliveredItemBatch object) {
		// TODO Auto-generated method stub

	}

	@Override
	public DeliveredItemBatch getByKey(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchListValueObject findAll(AbstractSearchListQueryObject commandObject, SearchType mode) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DeliveredItemBatch> findAllDeliveredItemBatchByRequestItemAndGroup(SupplyRequestItem supplyRequestItem,
			DeliveryReceipt deliveryReceipt) {
		List<DeliveredItemBatch> itemBatches = new ArrayList<DeliveredItemBatch>();
		
		StringBuilder queryStrBldr = 
				new StringBuilder("select p from ServiceCenterDeliveredItemBatch p ");
		queryStrBldr.append("where p.supplyRequestItem=:supplyRequestItem and p.group=:deliveryReceipt");
		
		itemBatches = getEntityManager()
			.createQuery(queryStrBldr.toString())
			.setParameter("supplyRequestItem", supplyRequestItem)
			.setParameter("deliveryReceipt", deliveryReceipt)
			.getResultList();
		
		return itemBatches;
	}

	@Override
	public Logger getLogger() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeliveredItemBatch findByBatchNumber(String batchNumber) {
		Map<String, String> batchNumberFormattedComponents = 
				batchNumberAnalyzer.analyzeBatchNumber(batchNumber);
		
		String itemCode = batchNumberFormattedComponents.get(BNC_ITEM_CODE);
		String rackCode = batchNumberFormattedComponents.get(BNC_RACK_CODE);
		String dateStoredString = batchNumberFormattedComponents.get(BNC_DATE_STORED);
		
		DeliveredItemBatch returnValue = null;
		try {
			Date dateStored = DateUtil.parseShortDate(dateStoredString);
			returnValue = doFindDeliveredItemBatch(itemCode, rackCode, dateStored);
		} catch (ParseException e) {
			LOGGER.error("Cant parse formatted date stored from batch number");
		}
		
		return returnValue;
	}

	@Override
	public DeliveredItemBatch findByBatchNumberAndFacilityCode(String batchNumber, String facilityCode) {
		Map<String, String> batchNumberFormattedComponents = 
				batchNumberAnalyzer.analyzeBatchNumber(batchNumber);
		
		String itemCode = batchNumberFormattedComponents.get(BNC_ITEM_CODE);
		String rackCode = batchNumberFormattedComponents.get(BNC_RACK_CODE);
		String dateStoredString = batchNumberFormattedComponents.get(BNC_DATE_STORED);
		
		DeliveredItemBatch returnValue = null;
		try {
			Date dateStored = DateUtil.parseShortDate(dateStoredString);
			returnValue = doFindDeliveredItemBatch(itemCode, rackCode, dateStored, facilityCode);
		} catch (ParseException e) {
			LOGGER.error("Cant parse formatted date stored from batch number");
		}
		
		return returnValue;
	}
	
	/**
	 * Must use facility code as a  strong identifier
	 * @param itemCode
	 * @param rackCode
	 * @param dateStored
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private DeliveredItemBatch doFindDeliveredItemBatch(String itemCode, String rackCode, Date dateStored) {
		StringBuilder queryStrBldr = 
				new StringBuilder("select ss from ServiceCenterDeliveredItemBatch ss ")
				.append("inner join ss.rackingLocation rl ")
				.append("inner join ss.supplyRequestItem ori ")
				.append("inner join ori.item fi ")
				.append("inner join fi.inventoryItem ii ")
				.append("where ss.dateStored=:dateStored ")
				.append("and rl.rackCode=:rackCode ")
				.append("and ii.codeName=:itemCode ");
		
		List<DeliveredItemBatch> suppliedItemsList = getEntityManager()
			.createQuery(queryStrBldr.toString())
			.setParameter("dateStored", dateStored)
			.setParameter("rackCode", rackCode)
			.setParameter("itemCode", itemCode)
			.getResultList();
		
		DeliveredItemBatch returnValue = null;
		if(0 < suppliedItemsList.size()) returnValue = suppliedItemsList.get(0);
		
		return returnValue;
	}
	
	@SuppressWarnings("unchecked")
	private DeliveredItemBatch doFindDeliveredItemBatch(String itemCode, String rackCode, Date dateStored, String facilityCode) {
		StringBuilder queryStrBldr = 
				new StringBuilder("select ss from ServiceCenterDeliveredItemBatch ss ")
				.append("inner join ss.rackingLocation rl ")
				.append("inner join rl.facility f ")
				.append("inner join ss.supplyRequestItem ori ")
				.append("inner join ori.item fi ")
				.append("inner join fi.inventoryItem ii ")
				.append("where ss.dateStored=:dateStored ")
				.append("and rl.rackCode=:rackCode ")
				.append("and ii.codeName=:itemCode ")
				.append("and f.codeName=:facilityCode ");
		
		List<DeliveredItemBatch> suppliedItemsList = getEntityManager()
			.createQuery(queryStrBldr.toString())
			.setParameter("dateStored", dateStored)
			.setParameter("rackCode", rackCode)
			.setParameter("itemCode", itemCode)
			.setParameter("facilityCode", facilityCode)
			.getResultList();
		
		DeliveredItemBatch returnValue = null;
		if(0 < suppliedItemsList.size()) returnValue = suppliedItemsList.get(0);
		
		return returnValue;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DeliveredItemBatch> findAllInFIFOFMannerByFacilityCodeAndItemCode(String facilityCodeName,
			String itemCode) {
		List<DeliveredItemBatch> itemBatches = new ArrayList<DeliveredItemBatch>();
		
		//TODO include PARTIALLY DELIVERED
		/**
		 * 
		 */
		LOGGER.info("findAllInFIFOFMannerByFacilityCodeAndItemCode - facility code: "+facilityCodeName+ ", itemCode: "+itemCode);
		StringBuilder queryStrBldr = 
				new StringBuilder("select p from ServiceCenterDeliveredItemBatch p ")
				.append("inner join p.rackingLocation rl ")
				.append("inner join rl.facility f ")
				.append("inner join p.supplyRequestItem ori ")
				.append("inner join ori.item fi ")
				.append("inner join fi.inventoryItem ii ")
				.append("inner join p.group g ")
				.append("inner join ori.request orir ")
				.append("where ii.codeName=:itemCode and f.codeName=:facilityCode and orir.facility=f ")
				.append("and g.status in ('DELIVERED','PARTIALLY DELIVERED') ")
				.append("order by p.dateStored asc");
		
		itemBatches = getEntityManager()
			.createQuery(queryStrBldr.toString())
			.setParameter("itemCode", itemCode)
			.setParameter("facilityCode", facilityCodeName)
			.getResultList();
		
		return itemBatches;
	}

}
