package com.dhl.inventory.repository.servicecenter.requestissuance;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.dhl.inventory.domain.servicecenter.requestissuance.SubmittedCustomerRequest;
import com.dhl.inventory.repository.AbstractNSISQueryOnlyRepository;

@Repository
class SubmittedCustomerRequestRepositoryImpl extends AbstractNSISQueryOnlyRepository
		implements SubmittedCustomerRequestRepository {

	@SuppressWarnings("unchecked")
	@Override
	public SubmittedCustomerRequest findRequestbyRequestNumber(String requestNumber) {
		List<SubmittedCustomerRequest> submittedCustomerRequests = new ArrayList<>();
		
		StringBuilder queryStrBldr = 
				new StringBuilder("select p from ServiceCenterSubmittedCustomerRequest p ")
				.append("where p.requestNumber=:requestNumber ");
		
		submittedCustomerRequests = getEntityManager()
			.createQuery(queryStrBldr.toString())
			.setParameter("requestNumber", requestNumber)
			.getResultList();
		
		SubmittedCustomerRequest returnValue = null;
		if (submittedCustomerRequests.size() > 0) {
			returnValue = submittedCustomerRequests.get(0);
		}
		
		return returnValue;
	}

	@Override
	public List<SubmittedCustomerRequest> findRequestFormForEveryRequestNumber(List<String> requestNumbers) {
		CriteriaBuilder cB = createEntityCriteria();
		CriteriaQuery<SubmittedCustomerRequest> criteriaQuery = 
				cB.createQuery(SubmittedCustomerRequest.class);
		Root<SubmittedCustomerRequest> root = 
				criteriaQuery.from(SubmittedCustomerRequest.class);
		CriteriaQuery<SubmittedCustomerRequest> selectClause = 
				criteriaQuery.select(root);
		
		Expression<String> requestNumber = root.get("requestNumber");
		Predicate condition = requestNumber.in(requestNumbers);
		selectClause.where(condition);
		
		TypedQuery<SubmittedCustomerRequest> typedQuery = 
				getEntityManager().createQuery(selectClause);
		List<SubmittedCustomerRequest> issuances = typedQuery.getResultList();
		
		return issuances;
	}

}
