package com.dhl.inventory.repository.servicecenter.inventorymaintenance;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.domain.servicecenter.inventorymaintenance.PhysicalStockCountRequest;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.PhysicalStockCountRequestApproval;
import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.repository.AbstractNSISRepository;
import com.dhl.inventory.util.SearchType;

@Repository("serviceCenterPhysicalStockCountRequestApprovalRepository")
class PhysicalStockCountRequestApprovalRepositoryImpl extends AbstractNSISRepository<String, PhysicalStockCountRequestApproval>
		implements PhysicalStockCountRequestApprovalRepository {

	@Override
	public void save(PhysicalStockCountRequestApproval entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(PhysicalStockCountRequestApproval entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public PhysicalStockCountRequestApproval getByKey(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchListValueObject findAll(AbstractSearchListQueryObject commandObject, SearchType mode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PhysicalStockCountRequestApproval findByRequest(PhysicalStockCountRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Logger getLogger() {
		// TODO Auto-generated method stub
		return null;
	}

}
