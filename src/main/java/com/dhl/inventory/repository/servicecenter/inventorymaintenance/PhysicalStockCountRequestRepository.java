package com.dhl.inventory.repository.servicecenter.inventorymaintenance;

import com.dhl.inventory.domain.servicecenter.inventorymaintenance.PhysicalStockCountRequest;
import com.dhl.inventory.repository.ImplementableRepository;

public interface PhysicalStockCountRequestRepository extends ImplementableRepository<PhysicalStockCountRequest> {
	
	PhysicalStockCountRequest findByRequestNumber(String requestNumber);
	
	int deleteAllPhysicalStockCounts(PhysicalStockCountRequest request);
}
