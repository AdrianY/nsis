package com.dhl.inventory.repository.servicecenter.supplyrequest;

import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequestApproval;
import com.dhl.inventory.repository.ImplementableRepository;

public interface SupplyRequestApprovalRepository extends ImplementableRepository<SupplyRequestApproval> {
	SupplyRequestApproval findBySupplyRequest(SupplyRequest supplyRequest);
}
