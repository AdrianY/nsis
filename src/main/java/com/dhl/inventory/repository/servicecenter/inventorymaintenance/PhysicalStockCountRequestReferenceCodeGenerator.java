package com.dhl.inventory.repository.servicecenter.inventorymaintenance;

import com.dhl.inventory.repository.ReferenceCodeGenerator;

public interface PhysicalStockCountRequestReferenceCodeGenerator extends ReferenceCodeGenerator {

}
