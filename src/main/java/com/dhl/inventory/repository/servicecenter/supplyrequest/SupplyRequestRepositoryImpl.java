package com.dhl.inventory.repository.servicecenter.supplyrequest;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.maintenance.FacilityInventoryItem;
import com.dhl.inventory.domain.maintenance.InventoryItem;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequestItem;
import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.servicecenter.supplyrequest.SupplyRequestLQO;
import com.dhl.inventory.repository.AbstractNSISRepository;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.util.DateUtil;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.util.SearchType;
import com.dhl.inventory.util.Using;

@Repository
class SupplyRequestRepositoryImpl extends AbstractNSISRepository<String, SupplyRequest> implements SupplyRequestRepository {

	private static final Logger LOGGER = Logger.getLogger(SupplyRequestRepositoryImpl.class);
	
	@Autowired
	private UserRepository userRepo;

	@SuppressWarnings("unchecked")
	@Override
	public SupplyRequest findByRequestNumber(String requestNumber) {
		List<SupplyRequest> supplyRequests = new ArrayList<>();
		
		supplyRequests = getEntityManager()
			.createQuery("from SupplyRequest where requestNumber=:requestNumber")
			.setParameter("requestNumber", requestNumber)
			.getResultList();
		
		SupplyRequest returnValue = null;
		if (supplyRequests.size() > 0) {
			returnValue = supplyRequests.get(0);
		}
		
		return returnValue;
	}

	@Override
	public Logger getLogger() {
		return LOGGER;
	}

	@Override
	public void save(SupplyRequest entity) {
		persist(entity);
	}
	
	public void delete(SupplyRequest entity) {
		remove(entity);
	}

	@Override
	public SearchListValueObject findAll(AbstractSearchListQueryObject commandObject, SearchType mode) {
		CriteriaBuilder cB = createEntityCriteria();
		CriteriaQuery<Tuple> criteriaQuery = cB.createTupleQuery();
		Root<SupplyRequest> supplyRequestRoot = criteriaQuery.from(SupplyRequest.class);
		
		SupplyRequestLQO supplyRequestLQO = (SupplyRequestLQO) commandObject;
		Predicate conditions = getSupplyRequestSearchConditions(cB,supplyRequestLQO,supplyRequestRoot, mode);
		
		Long totalCount = 0L;
		if(!SearchType.TYPE_HEAD.equals(mode)){
			totalCount = getSupplyRequestCount(supplyRequestRoot,criteriaQuery, cB, conditions);
		}
		List<Map<String, String>> valueObjects = 
				getSupplyRequestList(
						cB,
						supplyRequestLQO,
						criteriaQuery,
						supplyRequestRoot, 
						conditions,
						mode
				);
		
		return new SearchListValueObject(valueObjects, totalCount);
	}

	private List<Map<String, String>> getSupplyRequestList(
			CriteriaBuilder cB, 
			SupplyRequestLQO listQueryObject,
			CriteriaQuery<Tuple> multiSelectClause, 
			Root<SupplyRequest> root, 
			Predicate conditions,
			SearchType mode
	) {
		Join<SupplyRequest, Facility> facilityPath = root.join("facility");
		Join<SupplyRequest, SupplyRequestItem> ori = root.join("requestedItems",JoinType.LEFT);
		Join<SupplyRequestItem, FacilityInventoryItem> fii = ori.join("item",JoinType.LEFT);
		Join<FacilityInventoryItem, InventoryItem> ii = fii.join("inventoryItem",JoinType.LEFT);
		ori.alias("supplyRequestItems_");
		fii.alias("facilityInventoryItems_");
		ii.alias("inventoryItems_");
		facilityPath.alias("facility_");
		
		multiSelectClause.select(
			cB.tuple(
				root.get("requestNumber").alias("requestNumber"),
				root.get("status").alias("status"),
				facilityPath.get("codeName").alias("facilityCodeName"),
				root.get("requestDate").alias("requestDate"),
				root.get("requestById").alias("requestById"),
				root.get("currency").alias("currency"),
				cB.sum(cB.prod(ori.get("quantityByUOM"), ii.get("itemCost"))).alias("totalRequestCost")
			)
		);
		
		multiSelectClause.groupBy(
				root.get("requestNumber"),
				root.get("status"),
				facilityPath.get("codeName"),
				root.get("requestDate"),
				root.get("requestById"),
				root.get("currency"),
				root.get(listQueryObject.getSortField())
		);
		
		if(!SearchType.TYPE_HEAD.equals(mode)){
			switch(listQueryObject.getSortOrder()){
				case AbstractSearchListQueryObject.SORT_ORDER_ASC:
					multiSelectClause.orderBy(cB.asc(root.get(listQueryObject.getSortField())));
					break;
				case AbstractSearchListQueryObject.SORT_ORDER_DESC:
					multiSelectClause.orderBy(cB.desc(root.get(listQueryObject.getSortField())));
					break;
			}
		}
			
		if(null != conditions)
			multiSelectClause.where(conditions);
		
		TypedQuery<Tuple> typedQuery = getEntityManager().createQuery(multiSelectClause);
		typedQuery.setFirstResult(listQueryObject.getRowOffset());
		typedQuery.setMaxResults(listQueryObject.getMaxRows());
		List<Tuple> supplyRequestsAggregate = typedQuery.getResultList();
		
		List<Map<String, String>> valueObjects = new ArrayList<Map<String, String>>();
		for(Tuple or: supplyRequestsAggregate){
			Map<String, String> data = new HashMap<String, String>();
			
			data.put("requestNumber", (String)or.get("requestNumber"));
			data.put("status", (String)or.get("status"));
			data.put("facilityCodeName", (String)or.get("facilityCodeName"));
			
			Optional.object((Date)or.get("requestDate")).ifPresent(od -> {
				String requestDate = FormatterUtil.fullDateFormat(od);
				data.put("requestDate", requestDate);
			});
			
			Optional.object((String) or.get("requestById")).ifPresent(val -> {
				NsisUser requestBy = userRepo.getByKey(val);
				data.put("requestBy", requestBy.getFullName());
			});
			
			String totalRequestCost = FormatterUtil.currencyFormat((BigDecimal)or.get("totalRequestCost"));
			String totalRequestCostStr =  totalRequestCost + " " + (String)or.get("currency");
			data.put("totalRequestCost", totalRequestCostStr);
			
			valueObjects.add(data);
		}
		
		return valueObjects;
	}

	private Long getSupplyRequestCount(
			Root<SupplyRequest> root, CriteriaQuery<Tuple> multiSelectClause,
			CriteriaBuilder cB, Predicate conditions) {
		Join<SupplyRequest, Facility> facilityPath = root.join("facility");
		facilityPath.alias("facility_");
		multiSelectClause.select(cB.tuple(cB.count(root).alias("count")));
		if(null != conditions)
			multiSelectClause.where(conditions);
		
		Tuple singleResult = getEntityManager().createQuery(multiSelectClause).getSingleResult();
		
		return singleResult.get("count", Long.class);
	}

	private Predicate getSupplyRequestSearchConditions(
			CriteriaBuilder cB, SupplyRequestLQO supplyRequestLQO,
			Root<SupplyRequest> supplyRequestRoot, SearchType mode
	) {

		List<Predicate> predicates = new ArrayList<>();
		Join<SupplyRequest, Facility> facilityPath = supplyRequestRoot.join("facility");
		facilityPath.alias("facility_");
		
		Optional.object(supplyRequestLQO.getRequestNumber()).ifPresent(val -> {
			predicates.add(cB.like(supplyRequestRoot.get("requestNumber"), likeString(val)));
		});
		
		Optional.object(supplyRequestLQO.getStatus()).ifPresent(val -> {
			predicates.add(cB.equal(supplyRequestRoot.get("status"), val));
		});
		
		try {
			
			Optional<Date> toDate = DateUtil.parsePossibleShortDate(supplyRequestLQO.getToDate());
			Optional<Date> fromDate = DateUtil.parsePossibleShortDate(supplyRequestLQO.getFromDate());
			
			if(toDate.hasContent() && fromDate.hasContent()){
				predicates.add(cB.between(
						supplyRequestRoot.get("requestDate"), 
						sobDate(fromDate.getValue()), 
						eobDate(toDate.getValue())
				));	
			}else if(toDate.hasContent()){
				predicates.add(cB.lessThanOrEqualTo(
						supplyRequestRoot.get("requestDate"), eobDate(toDate.getValue())));
			}else if(fromDate.hasContent()){
				predicates.add(cB.greaterThanOrEqualTo(
						supplyRequestRoot.get("requestDate"), sobDate(fromDate.getValue())));
			}
			
		} catch (ParseException e) {
			LOGGER.error(e);
		}
		
		
		List<Predicate> mandatoryPredicates = new ArrayList<>();
		Optional.object(supplyRequestLQO.getStatuses()).ifPresent(statuses -> {
			Expression<String> status = supplyRequestRoot.get("status");
			mandatoryPredicates.add(status.in(statuses));
		});
		
		mandatoryPredicates.add(cB.notEqual(supplyRequestRoot.get("recordStatus"), "IN"));
		
		Optional.object(supplyRequestLQO.getFacilityCodes()).then(facilityCodes -> {
			Expression<String> facilityCodeName = facilityPath.get("codeName");
			mandatoryPredicates.add(facilityCodeName.in(facilityCodes));
		}, () -> {
			if(!supplyRequestLQO.isSubmittedList()){
				Optional.object(supplyRequestLQO.getFacilityCodeName()).ifPresent(facilityCode -> {
					mandatoryPredicates.add(cB.equal(facilityPath.get("codeName"), facilityCode));
				});
			}
		});
		
		Predicate mandatoryPredicate = 
				Using.thisObject(mandatoryPredicates)
				.deriveIfPresent(ps -> {
					Predicate[] logicallyGroupedMandatoryPredicateArr = convertToPredicateArray(ps);
					return cB.and(logicallyGroupedMandatoryPredicateArr);
				});
		
		Predicate logicalGroupPredicate = 
				Using
				.thisObject(predicates)
				.deriveIfPresent(p -> {
					Predicate[] logicallyGroupedPredicateArr = convertToPredicateArray(p);
					return wrappedClause(cB, mode, logicallyGroupedPredicateArr);
				});
		
		Predicate returnValue = null;
		if(null!=mandatoryPredicate && null != logicalGroupPredicate){
			returnValue = cB.and(mandatoryPredicate, logicalGroupPredicate);
		}else if(null!=mandatoryPredicate){
			returnValue = mandatoryPredicate;
		}else if(null!=logicalGroupPredicate){
			returnValue = logicalGroupPredicate;
		}
		
		return returnValue;
	}

	@Override
	public int deleteAllSupplyRequestItems(SupplyRequest sr) {
		Query query = getEntityManager().createQuery("DELETE FROM SupplyRequestItem WHERE request=:supplyRequest");
		return query.setParameter("supplyRequest", sr).executeUpdate();
	}

	@Override
	public SupplyRequestItem findSupplyRequestItemInSupplyRequestByItemCode(SupplyRequest supplyRequest, String itemCode) {
		// TODO Auto-generated method stub
		return null;
	}

}
