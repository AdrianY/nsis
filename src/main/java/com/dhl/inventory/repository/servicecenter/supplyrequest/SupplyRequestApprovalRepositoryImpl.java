package com.dhl.inventory.repository.servicecenter.supplyrequest;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequestApproval;
import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.repository.AbstractNSISRepository;
import com.dhl.inventory.util.SearchType;

@Repository
class SupplyRequestApprovalRepositoryImpl extends AbstractNSISRepository<String, SupplyRequestApproval>
		implements SupplyRequestApprovalRepository {

	@Override
	public void save(SupplyRequestApproval entity) {
		persist(entity);
	}

	@Override
	public void delete(SupplyRequestApproval entity) {
		remove(entity);
	}

	@Override
	public SupplyRequestApproval getByKey(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchListValueObject findAll(AbstractSearchListQueryObject commandObject, SearchType mode) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public SupplyRequestApproval findBySupplyRequest(SupplyRequest supplyRequest) {
		List<SupplyRequestApproval> supplyRequestsApprovals = new ArrayList<>();
		
		supplyRequestsApprovals = getEntityManager()
			.createQuery("from SupplyRequestApproval where supplyRequest=:supplyRequest")
			.setParameter("supplyRequest", supplyRequest)
			.getResultList();
		
		SupplyRequestApproval returnValue = null;
		if (supplyRequestsApprovals.size() > 0) {
			returnValue = supplyRequestsApprovals.get(0);
		}
		
		return returnValue;
	}

	@Override
	public Logger getLogger() {
		// TODO Auto-generated method stub
		return null;
	}

}
