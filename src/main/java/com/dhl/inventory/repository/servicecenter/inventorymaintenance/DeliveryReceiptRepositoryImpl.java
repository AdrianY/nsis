package com.dhl.inventory.repository.servicecenter.inventorymaintenance;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.maintenance.FacilityInventoryItem;
import com.dhl.inventory.domain.maintenance.InventoryItem;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.DeliveryReceipt;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequestItem;
import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.DeliveryReceiptLQO;
import com.dhl.inventory.repository.AbstractNSISRepository;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.util.DateUtil;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.util.SearchType;
import com.dhl.inventory.util.Using;

@Repository("serviceCenterDeliveryReceiptRepo")
class DeliveryReceiptRepositoryImpl extends AbstractNSISRepository<String, DeliveryReceipt> implements DeliveryReceiptRepository {

	private static final Logger LOGGER = Logger.getLogger(DeliveryReceiptRepositoryImpl.class);
	
	@Autowired
	private UserRepository userRepo;
	
	@Override
	public void save(DeliveryReceipt entity) {
		persist(entity);
	}

	@Override
	public void delete(DeliveryReceipt object) {
		// TODO Auto-generated method stub

	}

	@Override
	public DeliveryReceipt getByKey(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchListValueObject findAll(AbstractSearchListQueryObject commandObject, SearchType mode) {
		CriteriaBuilder cB = createEntityCriteria();
		CriteriaQuery<Tuple> criteriaQuery = cB.createTupleQuery();
		Root<DeliveryReceipt> supplyRequestRoot = criteriaQuery.from(DeliveryReceipt.class);
		
		DeliveryReceiptLQO supplyRequestLQO = (DeliveryReceiptLQO) commandObject;
		Predicate conditions = getDeliveryReceiptSearchConditions(cB,supplyRequestLQO,supplyRequestRoot, mode);
		
		Long totalCount = 0L;
		if(!SearchType.TYPE_HEAD.equals(mode)){
			totalCount = getDeliveryReceiptCount(supplyRequestRoot,criteriaQuery, cB, conditions);
		}
		List<Map<String, String>> valueObjects = 
				getDeliveryReceiptList(
						cB,
						supplyRequestLQO,
						criteriaQuery,
						supplyRequestRoot, 
						conditions,
						mode
				);
		
		return new SearchListValueObject(valueObjects, totalCount);
	}

	private List<Map<String, String>> getDeliveryReceiptList(
			CriteriaBuilder cB, 
			DeliveryReceiptLQO listQueryObject,
			CriteriaQuery<Tuple> multiSelectClause, 
			Root<DeliveryReceipt> root, 
			Predicate conditions,
			SearchType mode
	) {
		Join<DeliveryReceipt, SupplyRequest> or = root.join("supplyRequest",JoinType.LEFT);
		Join<SupplyRequest, SupplyRequestItem> ori = or.join("requestedItems",JoinType.LEFT);
		Join<SupplyRequestItem, FacilityInventoryItem> fii = ori.join("item",JoinType.LEFT);
		Join<FacilityInventoryItem, InventoryItem> ii = fii.join("inventoryItem",JoinType.LEFT);
		Join<SupplyRequest, Facility> facilityPath = or.join("facility",JoinType.INNER);
		or.alias("supplyPath_");
		ori.alias("supplyRequestItems_");
		fii.alias("facilityInventoryItems_");
		ii.alias("inventoryItems_");
		facilityPath.alias("facility_");
		
		multiSelectClause.select(
			cB.tuple(
				or.get("requestNumber").alias("requestNumber"),
				root.get("status").alias("status"),
				or.get("requestDate").alias("requestDate"),
				or.get("requestById").alias("requestById"),
				or.get("currency").alias("currency"),
				or.get("requestCycle").alias("requestCycle"),
				cB.sum(cB.prod(ori.get("quantityByUOM"), ii.get("itemCost"))).alias("totalRequestCost")
			)
		);
		
		multiSelectClause.groupBy(
				or.get("requestNumber"),
				root.get("status"),
				or.get("requestDate"),
				or.get("requestById"),
				or.get("currency"),
				or.get("requestCycle"),
				root.get(listQueryObject.getSortField())
		);
		
		if(!SearchType.TYPE_HEAD.equals(mode)){
			switch(listQueryObject.getSortOrder()){
				case AbstractSearchListQueryObject.SORT_ORDER_ASC:
					multiSelectClause.orderBy(cB.asc(root.get(listQueryObject.getSortField())));
					break;
				case AbstractSearchListQueryObject.SORT_ORDER_DESC:
					multiSelectClause.orderBy(cB.desc(root.get(listQueryObject.getSortField())));
					break;
			}
		}
			
		if(null != conditions)
			multiSelectClause.where(conditions);
		
		TypedQuery<Tuple> typedQuery = getEntityManager().createQuery(multiSelectClause);
		typedQuery.setFirstResult(listQueryObject.getRowOffset());
		typedQuery.setMaxResults(listQueryObject.getMaxRows());
		List<Tuple> deliveyrReceiptsAggregate = typedQuery.getResultList();
		
		List<Map<String, String>> valueObjects = new ArrayList<Map<String, String>>();
		for(Tuple deliveryReceipt: deliveyrReceiptsAggregate){
			Map<String, String> data = new HashMap<String, String>();
			
			data.put("requestNumber", (String)deliveryReceipt.get("requestNumber"));
			data.put("status", (String)deliveryReceipt.get("status"));
			data.put("requestCycle", (String)deliveryReceipt.get("requestCycle"));
			
			Optional.object((Date)deliveryReceipt.get("requestDate")).ifPresent(od -> {
				String requestDate = FormatterUtil.fullDateFormat(od);
				data.put("requestDate", requestDate);
			});
			
			Optional.object((String) deliveryReceipt.get("requestById")).ifPresent(val -> {
				NsisUser requestBy = userRepo.getByKey(val);
				data.put("requestBy", requestBy.getFullName());
			});
			
			String totalRequestCost = FormatterUtil.currencyFormat((BigDecimal)deliveryReceipt.get("totalRequestCost"));
			String totalRequestCostStr =  totalRequestCost + " " + (String)deliveryReceipt.get("currency");
			data.put("totalRequestCost", totalRequestCostStr);
			
			LOGGER.info("data: "+data.toString());
			valueObjects.add(data);
		}
		
		return valueObjects;
	}

	private Long getDeliveryReceiptCount(
			Root<DeliveryReceipt> root, CriteriaQuery<Tuple> multiSelectClause,
			CriteriaBuilder cB, Predicate conditions) {
		Join<DeliveryReceipt, SupplyRequest> supplyPath = root.join("supplyRequest",JoinType.LEFT);
		Join<SupplyRequest, Facility> facilityPath = supplyPath.join("facility",JoinType.INNER);
		supplyPath.alias("supplyPath_");
		facilityPath.alias("facility_");
		
		multiSelectClause.select(cB.tuple(cB.count(root).alias("count")));
		if(null != conditions)
			multiSelectClause.where(conditions);
		
		Tuple singleResult = getEntityManager().createQuery(multiSelectClause).getSingleResult();
		
		return singleResult.get("count", Long.class);
	}

	private Predicate getDeliveryReceiptSearchConditions(
			CriteriaBuilder cB, 
			DeliveryReceiptLQO deliveryReceiptLQO,
			Root<DeliveryReceipt> root, 
			SearchType mode
	) {
		
		List<Predicate> predicates = new ArrayList<>();
		Join<DeliveryReceipt, SupplyRequest> supplyPath = root.join("supplyRequest",JoinType.LEFT);
		Join<SupplyRequest, Facility> facilityPath = supplyPath.join("facility",JoinType.INNER);
		supplyPath.alias("supplyPath_");
		facilityPath.alias("facility_");
		
		Optional.object(deliveryReceiptLQO.getRequestNumber()).ifPresent(val -> {
			predicates.add(cB.like(supplyPath.get("requestNumber"), likeString(val)));
		});
		
		Optional.object(deliveryReceiptLQO.getStatus()).ifPresent(val -> {
			predicates.add(cB.equal(root.get("status"), val));
		});
		
		Optional.object(deliveryReceiptLQO.getCycle()).ifPresent(val -> {
			predicates.add(cB.equal(supplyPath.get("requestCycle"), val));
		});
		
		try {
			
			Optional<Date> toDate = DateUtil.parsePossibleShortDate(deliveryReceiptLQO.getToDate());
			Optional<Date> fromDate = DateUtil.parsePossibleShortDate(deliveryReceiptLQO.getFromDate());
			
			if(toDate.hasContent() && fromDate.hasContent()){
				predicates.add(cB.between(
						supplyPath.get("requestDate"), 
						sobDate(fromDate.getValue()), 
						eobDate(toDate.getValue())
				));	
			}else if(toDate.hasContent()){
				predicates.add(cB.lessThanOrEqualTo(
						supplyPath.get("requestDate"), eobDate(toDate.getValue())));
			}else if(fromDate.hasContent()){
				predicates.add(cB.greaterThanOrEqualTo(
						supplyPath.get("requestDate"), sobDate(fromDate.getValue())));
			}
			
		} catch (ParseException e) {
			LOGGER.error(e);
		}
		
		Predicate logicalGroupPredicate = 
				Using
				.thisObject(predicates)
				.deriveIfPresent(p -> {
					Predicate[] logicallyGroupedPredicateArr = convertToPredicateArray(p);
					return wrappedClause(cB, mode, logicallyGroupedPredicateArr);
				});
		
		List<Predicate> mandatoryPredicates = new ArrayList<>();
		Optional.object(deliveryReceiptLQO.getFacilityCodeName()).ifPresent(facilityCodeName -> {
			mandatoryPredicates.add(cB.equal(facilityPath.get("codeName"), facilityCodeName));
		});
		
		mandatoryPredicates.add(cB.notEqual(root.get("recordStatus"), "IN"));
		
		Predicate mandatoryPredicate = 
				Using.thisObject(mandatoryPredicates)
				.deriveIfPresent(ps -> {
					Predicate[] logicallyGroupedMandatoryPredicateArr = convertToPredicateArray(ps);
					return cB.and(logicallyGroupedMandatoryPredicateArr);
				});

		Predicate returnValue = null;
		if(null != mandatoryPredicate && null != logicalGroupPredicate){
			returnValue = cB.and(mandatoryPredicate,logicalGroupPredicate);
		}else if(null != mandatoryPredicate){
			returnValue = mandatoryPredicate;
		}else if(null != logicalGroupPredicate){
			returnValue = logicalGroupPredicate;
		}
		return returnValue;
	}

	@Override
	public Logger getLogger() {
		return LOGGER;
	}

	@SuppressWarnings("unchecked")
	@Override
	public DeliveryReceipt findByRequestNumber(String requestNumber) {
		List<DeliveryReceipt> deliveryReceipts = new ArrayList<DeliveryReceipt>();
		
		StringBuilder queryStrBldr = 
				new StringBuilder("select p from ServiceCenterDeliveryReceipt p ");
		queryStrBldr.append("inner join p.supplyRequest a ");
		queryStrBldr.append("where a.requestNumber=:requestNumber");
		
		deliveryReceipts = getEntityManager()
				.createQuery(queryStrBldr.toString())
				.setParameter("requestNumber", requestNumber)
				.getResultList();
		
		DeliveryReceipt returnValue = null;
		if (deliveryReceipts.size() > 0) {
			returnValue = deliveryReceipts.get(0);
		}
		
		return returnValue;
	}

	@Override
	public int deleteDeliveredItemBatches(DeliveryReceipt dr) {
		Query query = getEntityManager().createQuery("DELETE FROM ServiceCenterDeliveredItemBatch "
				+ "WHERE group=:deliveryReceipt");
		return query.setParameter("deliveryReceipt", dr).executeUpdate();
	}

}
