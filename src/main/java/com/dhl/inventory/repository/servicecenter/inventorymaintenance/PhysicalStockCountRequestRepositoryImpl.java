package com.dhl.inventory.repository.servicecenter.inventorymaintenance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.PhysicalStockCountRequest;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;
import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.PhysicalStockCountRequestLQO;
import com.dhl.inventory.repository.AbstractNSISRepository;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.util.SearchType;
import com.dhl.inventory.util.Using;

@Repository("serviceCenterPhysicalStockCountRequestRepository")
class PhysicalStockCountRequestRepositoryImpl extends AbstractNSISRepository<String, PhysicalStockCountRequest>
		implements PhysicalStockCountRequestRepository {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(PhysicalStockCountRequestRepositoryImpl.class);
	
	@Override
	public void save(PhysicalStockCountRequest entity) {
		persist(entity);
	}

	@Override
	public void delete(PhysicalStockCountRequest entity) {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("unchecked")
	@Override
	public PhysicalStockCountRequest getByKey(String key) {
		List<PhysicalStockCountRequest> physicalStockCountRequests = new ArrayList<>();
		
		physicalStockCountRequests = getEntityManager()
			.createQuery("from ServiceCenterPhysicalStockCountRequest where id=:key")
			.setParameter("key", key)
			.getResultList();
		
		PhysicalStockCountRequest returnValue = null;
		if (physicalStockCountRequests.size() > 0) {
			returnValue = physicalStockCountRequests.get(0);
		}
		
		return returnValue;
	}

	@SuppressWarnings("unchecked")
	@Override
	public PhysicalStockCountRequest findByRequestNumber(String requestNumber) {
		List<PhysicalStockCountRequest> physicalStockCountRequests = new ArrayList<>();
		
		physicalStockCountRequests = getEntityManager()
			.createQuery("from ServiceCenterPhysicalStockCountRequest where requestNumber=:requestNumber")
			.setParameter("requestNumber", requestNumber)
			.getResultList();
		
		PhysicalStockCountRequest returnValue = null;
		if (physicalStockCountRequests.size() > 0) {
			returnValue = physicalStockCountRequests.get(0);
		}
		
		return returnValue;
	}

	@Override
	public int deleteAllPhysicalStockCounts(PhysicalStockCountRequest request) {
		Query query = getEntityManager().createQuery("DELETE FROM ServiceCenterPhysicalStockCount WHERE request=:request");
		return query.setParameter("request", request).executeUpdate();
	}
	
	@Override
	public SearchListValueObject findAll(AbstractSearchListQueryObject commandObject, SearchType mode) {
		CriteriaBuilder cB = createEntityCriteria();
		CriteriaQuery<PhysicalStockCountRequest> criteriaQuery = cB.createQuery(PhysicalStockCountRequest.class);
		Root<PhysicalStockCountRequest> physicalStockCountRequestRoot = criteriaQuery.from(PhysicalStockCountRequest.class);
		
		PhysicalStockCountRequestLQO physicalStockCountRequestLQO = 
				(PhysicalStockCountRequestLQO) commandObject;
		Predicate conditions = 
				getPhysicalStockCountRequestSearchConditions(
						cB,physicalStockCountRequestLQO,physicalStockCountRequestRoot, mode);
		
		Long totalCount = 0L;
		if(!SearchType.TYPE_HEAD.equals(mode)){
			totalCount = getPhysicalStockCountRequestCount(physicalStockCountRequestRoot,criteriaQuery, cB, conditions);
		}
		List<Map<String, String>> valueObjects = 
				getPhysicalStockCountRequestList(
						cB,
						physicalStockCountRequestLQO,
						criteriaQuery,
						physicalStockCountRequestRoot, 
						conditions,
						mode
				);
		
		return new SearchListValueObject(valueObjects, totalCount);
	}

	private List<Map<String, String>> getPhysicalStockCountRequestList(
			final CriteriaBuilder cB,
			final PhysicalStockCountRequestLQO listQueryObject, 
			final CriteriaQuery<PhysicalStockCountRequest> criteriaQuery,
			final Root<PhysicalStockCountRequest> root, 
			final Predicate conditions, 
			final SearchType mode
	) {
		Join<PhysicalStockCountRequest, Facility> facilityPath = root.join("forFacility");
		Join<PhysicalStockCountRequest, NsisUser> requestByPath = root.join("requestBy");
		facilityPath.alias("facility_");
		requestByPath.alias("requestBy_");
		
		CriteriaQuery<PhysicalStockCountRequest> selectClause = 
				criteriaQuery.select(root);
		
		if(!SearchType.TYPE_HEAD.equals(mode)){
			switch(listQueryObject.getSortOrder()){
				case AbstractSearchListQueryObject.SORT_ORDER_ASC:
					selectClause.orderBy(cB.asc(root.get(listQueryObject.getSortField())));
					break;
				case AbstractSearchListQueryObject.SORT_ORDER_DESC:
					selectClause.orderBy(cB.desc(root.get(listQueryObject.getSortField())));
					break;
			}
		}
			
		if(null != conditions)
			selectClause.where(conditions);
		
		TypedQuery<PhysicalStockCountRequest> typedQuery = 
				getEntityManager().createQuery(selectClause);
		typedQuery.setFirstResult(listQueryObject.getRowOffset());
		typedQuery.setMaxResults(listQueryObject.getMaxRows());
		List<PhysicalStockCountRequest> resultList = typedQuery.getResultList();
		
		List<Map<String, String>> valueObjects = new ArrayList<Map<String, String>>();
		for(PhysicalStockCountRequest physicalStockCountRequest: resultList){
			Map<String, String> data = new HashMap<String, String>();
			
			data.put("requestNumber", physicalStockCountRequest.getRequestNumber());
			data.put("status", physicalStockCountRequest.getStatus());
			if(listQueryObject.isShowFacilityCode()){
				data.put("facilityCodeName", physicalStockCountRequest.getForFacility().getCodeName());
			}
			
			Optional.object(physicalStockCountRequest.getRequestDate()).ifPresent(od -> {
				String requestDate = FormatterUtil.fullDateFormat(od);
				data.put("requestDate", requestDate);
			});
			
			Optional.object(physicalStockCountRequest.getRequestBy()).ifPresent(val -> {
				data.put("requestBy", val.getFullName());
			});
			
			valueObjects.add(data);
		}
		
		return valueObjects;
	}

	private Long getPhysicalStockCountRequestCount(
			Root<PhysicalStockCountRequest> root,
			CriteriaQuery<PhysicalStockCountRequest> multiSelectClause, 
			CriteriaBuilder cB, 
			Predicate conditions
	) {
		Join<PhysicalStockCountRequest, Facility> facilityPath = root.join("forFacility");
		facilityPath.alias("facility_");
		CriteriaQuery<Long> countCriteriaQuery = cB.createQuery(Long.class);
		countCriteriaQuery.select(cB.count(countCriteriaQuery.from(PhysicalStockCountRequest.class)));
		if(null != conditions)
			countCriteriaQuery.where(conditions);
		
		Long totalCount = getEntityManager().createQuery(countCriteriaQuery).getSingleResult();
		
		return totalCount;
	}

	private Predicate getPhysicalStockCountRequestSearchConditions(
			CriteriaBuilder cB,
			PhysicalStockCountRequestLQO physicalStockCountRequestLQO,
			Root<PhysicalStockCountRequest> root, 
			SearchType mode
	) {
		List<Predicate> predicates = new ArrayList<>();
		Join<SupplyRequest, Facility> facilityPath = root.join("forFacility");
		facilityPath.alias("facility_");
		
		Optional.object(physicalStockCountRequestLQO.getRequestNumber()).ifPresent(val -> {
			predicates.add(cB.like(root.get("requestNumber"), likeString(val)));
		});
		
		Optional.object(physicalStockCountRequestLQO.getStatus()).ifPresent(val -> {
			predicates.add(cB.equal(root.get("status"), val));
		});
		
		Optional.object(physicalStockCountRequestLQO.getFacilityCode()).ifPresent(val -> {
			predicates.add(cB.equal(facilityPath.get("codeName"), val));
		});
		
		Predicate logicalGroupPredicate = 
				Using
				.thisObject(predicates)
				.deriveIfPresent(p -> {
					Predicate[] logicallyGroupedPredicateArr = convertToPredicateArray(p);
					return wrappedClause(cB, mode, logicallyGroupedPredicateArr);
				});
		
		List<Predicate> mandatoryPredicates = new ArrayList<>();
		Optional.object(physicalStockCountRequestLQO.getStatuses()).ifPresent(statuses -> {
			Expression<String> status = root.get("status");
			mandatoryPredicates.add(status.in(statuses));
		});
		
		Predicate mandatoryPredicate = 
				Using.thisObject(mandatoryPredicates)
				.deriveIfPresent(ps -> {
					Predicate[] logicallyGroupedMandatoryPredicateArr = convertToPredicateArray(ps);
					return cB.and(logicallyGroupedMandatoryPredicateArr);
				});
		
		Predicate returnValue = null;
		if(null!=mandatoryPredicate && null != logicalGroupPredicate){
			returnValue = cB.and(mandatoryPredicate, logicalGroupPredicate);
		}else if(null!=mandatoryPredicate){
			returnValue = mandatoryPredicate;
		}else if(null!=logicalGroupPredicate){
			returnValue = logicalGroupPredicate;
		}
		
		return returnValue;
	}

	@Override
	public Logger getLogger() {
		// TODO Auto-generated method stub
		return null;
	}

}
