package com.dhl.inventory.repository.servicecenter.requestissuance;

import static com.dhl.inventory.components.BatchNumberUtilityBean.BNC_DATE_STORED;
import static com.dhl.inventory.components.BatchNumberUtilityBean.BNC_ITEM_CODE;
import static com.dhl.inventory.components.BatchNumberUtilityBean.BNC_RACK_CODE;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.components.BatchNumberUtilityBean;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.DeliveredItemBatch;
import com.dhl.inventory.domain.servicecenter.requestissuance.SuppliedItems;
import com.dhl.inventory.repository.AbstractNSISQueryOnlyRepository;
import com.dhl.inventory.util.DateUtil;
import com.dhl.inventory.util.Optional;

@Repository("serviceCenterSuppliedItemsRepository")
class SuppliedItemsRepositoryImpl extends AbstractNSISQueryOnlyRepository implements SuppliedItemsRepository {

private static final Logger LOGGER = Logger.getLogger(SuppliedItemsRepositoryImpl.class);
	
	@Autowired
	private BatchNumberUtilityBean batchNumberAnalyzer;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SuppliedItems> findOtherSuppliedItemsUsingDeliveredItemBatch(
			DeliveredItemBatch deliveredItemBatch, List<SuppliedItems> excluedSupplyItem) {
		List<SuppliedItems> suppliedItems = new ArrayList<SuppliedItems>();
		
		StringBuilder queryStrBldr = 
				new StringBuilder("select p from ServiceCenterSuppliedItems p ")
				.append("inner join p.suppliedItemsGroup sg ")
				.append("inner join sg.forRequestIssuance fri ")
				.append("where p.supplySource=:deliveredItemBatch ")
				.append("and fri.recordStatus='AT' ");
		
		Optional.object(excluedSupplyItem)
		.then(excludedSupplyItems -> {
			queryStrBldr.append("and ( fri.status not in ('FOR PROCESSING') ")
				.append("or p.id in (:excludedSuppliedItemIds) ) ");
			
			suppliedItems.addAll(getEntityManager()
					.createQuery(queryStrBldr.toString())
					.setParameter("deliveredItemBatch", deliveredItemBatch)
					.setParameter("excludedSuppliedItemIds", excludedSupplyItems.stream().map(val -> {
						return val.getId();
					}).collect(Collectors.toList()))
					.getResultList());
		}, () -> {
			queryStrBldr.append("and fri.status not in ('FOR PROCESSING') ");
			suppliedItems.addAll(getEntityManager()
					.createQuery(queryStrBldr.toString())
					.setParameter("deliveredItemBatch", deliveredItemBatch)
					.getResultList());
		});
		
		return suppliedItems;
	}

	@Override
	public SuppliedItems findSuppliedItem(String requestNumber, String batchNumber) {
		Map<String, String> batchNumberFormattedComponents = 
				batchNumberAnalyzer.analyzeBatchNumber(batchNumber);
		
		String itemCode = batchNumberFormattedComponents.get(BNC_ITEM_CODE);
		String rackCode = batchNumberFormattedComponents.get(BNC_RACK_CODE);
		String dateStoredString = batchNumberFormattedComponents.get(BNC_DATE_STORED);
		
		
		SuppliedItems returnValue = null;
		try {
			Date dateStored = DateUtil.parseShortDate(dateStoredString);
			returnValue = doFindSuppliedItem(requestNumber, itemCode, rackCode, dateStored);
		} catch (ParseException e) {
			LOGGER.error("Cant parse formatted date stored from batch number");
		}
		
		return returnValue;
	}
	
	@SuppressWarnings("unchecked")
	private SuppliedItems doFindSuppliedItem(
		String requestNumber,
		String itemCode,
		String rackCode,
		Date dateStored
	){
		StringBuilder queryStrBldr = 
				new StringBuilder("select p from ServiceCenterSuppliedItems p ")
				.append("inner join p.suppliedItemsGroup sg ")
				.append("inner join sg.forRequestIssuance fri ")
				.append("inner join p.supplySource ss ")
				.append("inner join ss.rackingLocation rl ")
				.append("inner join ss.supplyRequestItem ori ")
				.append("inner join ori.item fi ")
				.append("inner join fi.inventoryItem ii ")
				.append("where fri.requestNumber=:requestNumber ")
				.append("and ss.dateStored=:dateStored ")
				.append("and rl.rackCode=:rackCode ")
				.append("and ii.codeName=:itemCode ");
		
		List<SuppliedItems> suppliedItemsList = getEntityManager()
			.createQuery(queryStrBldr.toString())
			.setParameter("requestNumber", requestNumber)
			.setParameter("dateStored", dateStored)
			.setParameter("rackCode", rackCode)
			.setParameter("itemCode", itemCode)
			.getResultList();
		
		SuppliedItems returnValue = null;
		if(0 < suppliedItemsList.size()) returnValue = suppliedItemsList.get(0);
		
		return returnValue;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SuppliedItems> findAllSuppliedItemsUsingDeliveredItemBatchForRequest(
			DeliveredItemBatch deliveredItemBatch, String requestNumber) {
		List<SuppliedItems> suppliedItems = new ArrayList<SuppliedItems>();
		
		StringBuilder queryStrBldr = 
				new StringBuilder("select p from ServiceCenterSuppliedItems p ")
				.append("inner join p.suppliedItemsGroup sg ")
				.append("inner join sg.forRequestIssuance fri ")
				.append("where p.supplySource=:deliveredItemBatch ")
				.append("and ( ")
				.append("fri.status not in ('FOR PROCESSING') ")
				.append("or ")
				.append("fri.requestNumber =:requestNumber ")
				.append(") ");
		
		suppliedItems = getEntityManager()
			.createQuery(queryStrBldr.toString())
			.setParameter("deliveredItemBatch", deliveredItemBatch)
			.setParameter("requestNumber", requestNumber)
			.getResultList();
		
		return suppliedItems;
	}

}
