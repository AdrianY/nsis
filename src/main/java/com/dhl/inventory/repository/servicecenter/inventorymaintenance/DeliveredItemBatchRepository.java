package com.dhl.inventory.repository.servicecenter.inventorymaintenance;

import java.util.List;

import com.dhl.inventory.domain.servicecenter.inventorymaintenance.DeliveredItemBatch;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.DeliveryReceipt;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequestItem;
import com.dhl.inventory.repository.ImplementableRepository;

public interface DeliveredItemBatchRepository extends ImplementableRepository<DeliveredItemBatch> {
	List<DeliveredItemBatch> findAllDeliveredItemBatchByRequestItemAndGroup(SupplyRequestItem supplyRequestItem, DeliveryReceipt deliveryReceipt);

	DeliveredItemBatch findByBatchNumber(String batchNumber);
	
	DeliveredItemBatch findByBatchNumberAndFacilityCode(String batchNumber, String facilityCode);
	
	List<DeliveredItemBatch> findAllInFIFOFMannerByFacilityCodeAndItemCode(String facilityCodeName, String itemCode);
}
