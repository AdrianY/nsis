package com.dhl.inventory.repository.servicecenter.inventorymaintenance;

import com.dhl.inventory.domain.servicecenter.inventorymaintenance.DeliveryReceipt;
import com.dhl.inventory.repository.ImplementableRepository;

public interface DeliveryReceiptRepository extends ImplementableRepository<DeliveryReceipt> {
	DeliveryReceipt findByRequestNumber(String requestNumber);
	
	int deleteDeliveredItemBatches(DeliveryReceipt dr);
}
