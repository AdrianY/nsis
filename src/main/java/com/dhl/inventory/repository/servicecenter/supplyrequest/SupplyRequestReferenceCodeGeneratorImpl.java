package com.dhl.inventory.repository.servicecenter.supplyrequest;

import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;
import com.dhl.inventory.repository.AbstractReferenceCodeGenerator;

@Repository
class SupplyRequestReferenceCodeGeneratorImpl extends AbstractReferenceCodeGenerator<SupplyRequest>
		implements SupplyRequestReferenceCodeGenerator {

	private static final Logger LOGGER = Logger.getLogger(SupplyRequestReferenceCodeGeneratorImpl.class);
	
	private static final int PREFIX = 8;
	
	private static final String PADDING = "0000000";
			
	@Override
	@SuppressWarnings( "unchecked")
	public String loadCurrentReferenceCodeFromDB() {
		StringBuilder sqlStrBuilder = new StringBuilder();
		sqlStrBuilder.append("SELECT MAX(SUBSTRING(request_number,"+String.valueOf(PREFIX)+","+PADDING.length()+")) ");
		sqlStrBuilder.append("FROM "+getTableName().toUpperCase()+" ");
		Query query = this.getEntityManager().createNativeQuery(sqlStrBuilder.toString());
		
		List<String> resultList = query.getResultList();
		String result = extractResultList(resultList);
		
		String fromDBReferenceCode = executeReferenceCodeConstruction(result);
		return fromDBReferenceCode;
	}

	@Override
	public String constructReferenceCode(String baseReferenceCode) {
		String trimmedBaseReferenceCode = baseReferenceCode.replaceFirst("REQUEST", "");
		String constructedReferenceCode = executeReferenceCodeConstruction(trimmedBaseReferenceCode);
		LOGGER.info("constructedReferenceCode: "+constructedReferenceCode);
		return constructedReferenceCode;
	}

	private String executeReferenceCodeConstruction(String baseReferenceCode){
        int initialReferenceCode = Integer.valueOf(baseReferenceCode) + 1;
        String stringRefCode = String.valueOf(initialReferenceCode);
        int remPrefix = PREFIX - stringRefCode.length();
        return "REQUEST"+PADDING.substring(0, (remPrefix - 1)) + stringRefCode;
    }
	
	private String extractResultList(List<String> resultList){
		String returnValue = "0";
		if(null != resultList && !resultList.isEmpty()){
			String innerResultList = resultList.get(0);
			returnValue = null != innerResultList && !innerResultList.isEmpty() ? (String)innerResultList : "0";
		}
		
		return returnValue;
	}

}
