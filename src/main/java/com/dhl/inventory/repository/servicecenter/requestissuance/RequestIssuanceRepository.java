package com.dhl.inventory.repository.servicecenter.requestissuance;

import com.dhl.inventory.domain.servicecenter.requestissuance.RequestIssuance;
import com.dhl.inventory.repository.ImplementableRepository;

public interface RequestIssuanceRepository extends ImplementableRepository<RequestIssuance> {
	RequestIssuance findByRequestNumber(String requestNumber);
	
	int deleteAllSupplyItemsByRequestIssuance(final RequestIssuance requestIssuance);
}
