package com.dhl.inventory.repository.servicecenter.requestissuance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.servicecenter.requestissuance.RequestIssuance;
import com.dhl.inventory.domain.servicecenter.requestissuance.SuppliedItemsGroup;
import com.dhl.inventory.dto.queryobject.AbstractSearchListQueryObject;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.RequestIssuanceLQO;
import com.dhl.inventory.repository.AbstractNSISRepository;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.util.SearchType;


@Repository("serviceCenterRequestIssuanceRepository")
class RequestIssuanceRepositoryImpl extends AbstractNSISRepository<String, RequestIssuance> implements RequestIssuanceRepository {

	private static final Logger LOGGER = Logger.getLogger(RequestIssuanceRepositoryImpl.class);
	
	@Autowired
	private UserRepository userRepo;

	@Override
	public void save(RequestIssuance entity) {
		persist(entity);
	}

	@Override
	public void delete(RequestIssuance entity) {
		remove(entity);
	}

	@Override
	public RequestIssuance getByKey(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public RequestIssuance findByRequestNumber(String requestNumber) {
		List<RequestIssuance> requestIssuances = new ArrayList<>();
		
		StringBuilder queryStrBldr = 
				new StringBuilder("select p from ServiceCenterRequestIssuance p ")
				.append("where p.requestNumber=:rn ");
		
		requestIssuances = getEntityManager()
			.createQuery(queryStrBldr.toString())
			.setParameter("rn", requestNumber)
			.getResultList();
		
		RequestIssuance returnValue = null;
		if (requestIssuances.size() > 0) {
			returnValue = requestIssuances.get(0);
		}
		
		return returnValue;
	}

	@Override
	public int deleteAllSupplyItemsByRequestIssuance(final RequestIssuance requestIssuance) {
		int numberOfItemsRemoved = 0;
		if(null != requestIssuance){
			for(SuppliedItemsGroup siGrp: requestIssuance.getSuppliedItemsGroup()){
				Query query = getEntityManager().createQuery("DELETE FROM ServiceCenterSuppliedItems "
						+ "WHERE suppliedItemsGroup=:siGrp");
				numberOfItemsRemoved =  query.setParameter("siGrp", siGrp).executeUpdate();
			}
		}
		return numberOfItemsRemoved;
	}
	
	@Override
	public SearchListValueObject findAll(AbstractSearchListQueryObject commandObject, SearchType mode) {
		RequestIssuanceLQO listQueryObject = (RequestIssuanceLQO) commandObject;
		
		List<Object[]> rawQueryResultList = getRequestIssuanceList(listQueryObject);
		List<Map<String, String>> valueObjects = finalizeQueryResultSet(rawQueryResultList);
		
		Long totalCount = 0L;
		if(!SearchType.TYPE_HEAD.equals(mode)){
			totalCount = getRequestIssuanceCount(listQueryObject);
		}
		
		return new SearchListValueObject(valueObjects, totalCount);
	}
	
	private List<Map<String, String>> finalizeQueryResultSet(final List<Object[]> rawQueryResultList){
		List<Map<String, String>> returnValue = new ArrayList<>();
		Optional.object(rawQueryResultList).ifPresent(supplyList -> {
			supplyList.stream().forEach(supplyRecord -> {
				Map<String, String> record = new HashMap<>();
				record.put("requestNumber", (String)supplyRecord[0]);
				record.put("requestDate", (String)supplyRecord[1]);
				record.put("status", (String)supplyRecord[3]);
				record.put("customerName", (String)supplyRecord[4]);
				record.put("accountNumber", (String)supplyRecord[5]);
				record.put("requestType", (String)supplyRecord[6]);
				
				NsisUser user = userRepo.getByKey((String)supplyRecord[2]);
				record.put("requestBy", user.getFullName());
				
				returnValue.add(record);
			});
		});
		
		return returnValue;
	}
	
	@SuppressWarnings("unchecked")
	private List<Object[]> getRequestIssuanceList(final RequestIssuanceLQO listQueryObject){
		String queryString = getRequestIssuanceListQueryString(listQueryObject);
		Query query = generateQuery(queryString, listQueryObject);
		query.setParameter("maxRow", listQueryObject.getMaxRows());
		query.setParameter("offset", listQueryObject.getRowOffset());
		return query.getResultList();
	}
	
	private Long getRequestIssuanceCount(final RequestIssuanceLQO listQueryObject){
		String queryString = getRequestIssuanceCountQueryString(listQueryObject);
		Query query = generateQuery(queryString, listQueryObject);
		Long count = Long.valueOf(String.valueOf(query.getSingleResult()));
		return count;
	}
	
	private Query generateQuery(final String queryString, final RequestIssuanceLQO listQueryObject){
		Query query = getEntityManager().createNativeQuery(queryString);
		query.setParameter("facilityCodeName", listQueryObject.getFacilityCodeName());
		
		if(isNotEmpty(listQueryObject.getCustomerName())){
			query.setParameter("customerName", likeString(listQueryObject.getCustomerName()));
		}
		if(isNotEmpty(listQueryObject.getAccountNumber())){
			query.setParameter("accountNumber", likeString(listQueryObject.getAccountNumber()));
		}
		if(isNotEmpty(listQueryObject.getStatus())){
			query.setParameter("status", listQueryObject.getStatus());
		}
		if(isNotEmpty(listQueryObject.getToDate())){
			query.setParameter("toDate", listQueryObject.getToDate());
		}
		if(isNotEmpty(listQueryObject.getFromDate())){
			query.setParameter("fromDate", listQueryObject.getFromDate());
		}
		if(isNotEmpty(listQueryObject.getRequestType())){
			query.setParameter("requestType", listQueryObject.getRequestType());
		}
		if(isNotEmpty(listQueryObject.getReqNumber())){
			query.setParameter("requestNumber", likeString(listQueryObject.getReqNumber()));
		}
		
		return query;
	}
	
	private String getRequestIssuanceListQueryString(final RequestIssuanceLQO listQueryObject){
		StringBuilder queryStringBldr = new StringBuilder();
		queryStringBldr.append(
				"SELECT TOP (:maxRow) aa_.request_number, "+ 
						"aa_.request_date, aa_.requestor_id, "+
						"aa_.issuance_status, aa_.customer_name, "+
						"aa_.account_number, aa_.request_type "+
				"FROM (");
		queryStringBldr.append(
				"SELECT zz_.*, "+
					   "ROW_NUMBER() OVER (ORDER BY zz_.request_number) AS row_num "+
				"FROM ( "+
					"SELECT x_.req_number AS request_number, "+
						"y_.request_by AS requestor_id, "+
						"replace(convert(NVARCHAR, y_.request_date, 101), ' ', '/') AS request_date, "+
						"y_.cust_name AS customer_name, "+
						"y_.recepient_id AS account_number, "+
						"y_.type AS request_type, "+
						"x_.status AS issuance_status "+
					"FROM svc_stock_req_iss x_ "+
					"LEFT OUTER JOIN svc_subm_cr_req y_ "+
						"ON x_.req_number = y_.request_number "+
					"LEFT OUTER JOIN mn_fac z_ "+
						"ON z_.id = x_.issuing_fac_id "+
					"WHERE z_.code_name = :facilityCodeName "+
						"AND x_.record_status = 'AT' "+
				") zz_ "
		);
		queryStringBldr.append(generateRequestIssuanceQueryWhereClause(listQueryObject));
		queryStringBldr.append(") aa_ ");
		queryStringBldr.append("WHERE aa_.row_num >= :offset ");
		queryStringBldr.append(generateOrderByClause(listQueryObject));
		return queryStringBldr.toString();
	}
	
	private String getRequestIssuanceCountQueryString(final RequestIssuanceLQO listQueryObject){
		StringBuilder queryStringBldr = new StringBuilder();
		queryStringBldr.append(
				"SELECT COUNT(*) "+
				"FROM ( "+
					"SELECT x_.req_number AS request_number, "+
						"y_.request_by AS requestor_id, "+
						"replace(convert(NVARCHAR, y_.request_date, 101), ' ', '/') AS request_date, "+
						"y_.cust_name AS customer_name, "+
						"y_.recepient_id AS account_number, "+
						"y_.type AS request_type, "+
						"x_.status AS issuance_status "+
					"FROM svc_stock_req_iss x_ "+
					"LEFT OUTER JOIN svc_subm_cr_req y_ "+
						"ON x_.req_number = y_.request_number "+
					"LEFT OUTER JOIN mn_fac z_ "+
						"ON z_.id = x_.issuing_fac_id "+
					"WHERE z_.code_name = :facilityCodeName "+
						"AND x_.record_status = 'AT' "+
				") zz_ "
			);
		queryStringBldr.append(generateRequestIssuanceQueryWhereClause(listQueryObject));
		return queryStringBldr.toString();
	}
	
	private String generateRequestIssuanceQueryWhereClause(final RequestIssuanceLQO listQueryObject){
		StringBuilder queryStringBldr = new StringBuilder();
		
		boolean isNotFirstCondition = false;
		
		if(isNotEmpty(listQueryObject.getAccountNumber())){
			queryStringBldr.append(generateSqlOperator(isNotFirstCondition));
			queryStringBldr.append("zz_.account_number LIKE :accountNumber ");
			isNotFirstCondition = true;
		}
		if(isNotEmpty(listQueryObject.getReqNumber())){
			queryStringBldr.append(generateSqlOperator(isNotFirstCondition));
			queryStringBldr.append("zz_.request_number LIKE :requestNumber ");
			isNotFirstCondition = true;
		}
		if(isNotEmpty(listQueryObject.getCustomerName())){
			queryStringBldr.append(generateSqlOperator(isNotFirstCondition));
			queryStringBldr.append("zz_.customer_name LIKE :customerName ");
			isNotFirstCondition = true;
		}
		if(isNotEmpty(listQueryObject.getStatus())){
			queryStringBldr.append(generateSqlOperator(isNotFirstCondition));
			queryStringBldr.append("zz_.issuance_status = :status ");
			isNotFirstCondition = true;
		}
		if(isNotEmpty(listQueryObject.getRequestType())){
			queryStringBldr.append(generateSqlOperator(isNotFirstCondition));
			queryStringBldr.append("zz_.request_type = :requestType ");
			isNotFirstCondition = true;
		}
		
		boolean toDateIsNotEmpty = isNotEmpty(listQueryObject.getToDate());
		boolean fromDateIsNotEmpty = isNotEmpty(listQueryObject.getFromDate());
		if(toDateIsNotEmpty && fromDateIsNotEmpty){
			queryStringBldr.append(generateSqlOperator(isNotFirstCondition));
			queryStringBldr.append("zz_.request_date BETWEEN :fromDate AND :toDate ");
			isNotFirstCondition = true;
		}else if(toDateIsNotEmpty){
			queryStringBldr.append(generateSqlOperator(isNotFirstCondition));
			queryStringBldr.append("zz_.request_date <= :toDate ");
			isNotFirstCondition = true;
		}else if(fromDateIsNotEmpty){
			queryStringBldr.append(generateSqlOperator(isNotFirstCondition));
			queryStringBldr.append("zz_.request_date >= :fromDate ");
			isNotFirstCondition = true;
		}
		return queryStringBldr.toString();
	}
	
	private String generateSqlOperator(final boolean isNotFirstCondition){
		return isNotFirstCondition ? "AND " : "WHERE ";
	}
	
	private String generateOrderByClause(final RequestIssuanceLQO listQueryObject){
		return "ORDER BY "+listQueryObject.getSortField()+" "+listQueryObject.getSortOrder()+" ";
	}

	@Override
	public Logger getLogger() {
		return LOGGER;
	}

}
