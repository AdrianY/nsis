package com.dhl.inventory.repository.servicecenter.requestissuance;

import java.util.List;

import com.dhl.inventory.domain.servicecenter.inventorymaintenance.DeliveredItemBatch;
import com.dhl.inventory.domain.servicecenter.requestissuance.SuppliedItems;

public interface SuppliedItemsRepository {
	List<SuppliedItems> findOtherSuppliedItemsUsingDeliveredItemBatch(DeliveredItemBatch deliveredItemBatch, List<SuppliedItems> excluedSupplyItem);

	List<SuppliedItems> findAllSuppliedItemsUsingDeliveredItemBatchForRequest(DeliveredItemBatch deliveredItemBatch, String requestNumber);
	
	SuppliedItems findSuppliedItem(String requestNumber,String batchNumber);
}
