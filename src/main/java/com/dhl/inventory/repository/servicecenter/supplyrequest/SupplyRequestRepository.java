package com.dhl.inventory.repository.servicecenter.supplyrequest;

import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequestItem;
import com.dhl.inventory.repository.ImplementableRepository;

public interface SupplyRequestRepository extends ImplementableRepository<SupplyRequest>{
	SupplyRequest findByRequestNumber(String requestNumber);
	
	int deleteAllSupplyRequestItems(SupplyRequest sr);
	
	SupplyRequestItem findSupplyRequestItemInSupplyRequestByItemCode(SupplyRequest supplyRequest, String itemCode);
}
