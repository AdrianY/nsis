package com.dhl.inventory.repository.servicecenter.requestissuance;

import java.util.List;

import com.dhl.inventory.domain.servicecenter.requestissuance.SubmittedCustomerRequest;

public interface SubmittedCustomerRequestRepository {
	SubmittedCustomerRequest findRequestbyRequestNumber(final String requestNumber);
	
	List<SubmittedCustomerRequest> findRequestFormForEveryRequestNumber(final List<String> requestNumber);
}
