package com.dhl.inventory.repository;

import javax.persistence.EntityManager;
import javax.persistence.Table;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;

public final class  EntityTableNameExtractorUtil {
	public static <T> String getTableName(EntityManager em, Class<T> persistentDomainClass) {
		Metamodel meta = em.getMetamodel();
	    EntityType<T> entityType = meta.entity(persistentDomainClass);
 
	    Table t = persistentDomainClass.getAnnotation(Table.class);

	    String tableName = (t == null)
	                        ? entityType.getName().toUpperCase()
	                        : t.name();
	    return tableName;
	}
}
