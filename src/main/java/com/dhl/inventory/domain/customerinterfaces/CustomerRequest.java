package com.dhl.inventory.domain.customerinterfaces;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisDomainEntity;
import com.dhl.inventory.domain.maintenance.Facility;

@Entity
@Table(name = "cr_req")
public class CustomerRequest extends NsisDomainEntity {
	
	@Column(name = "request_number", unique = true,length=32)
	private String requestNumber;
	
	@Column(name = "request_by",length=36)
	private String requestById;
	
	@Column(name = "request_date")
	private Date requestDate;
	
	@Column(name = "required_date")
	private Date requiredDate;
	
	@Column(name = "status",length=25)
	private String status;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="to_facility_id",nullable = false) 
	private Facility toFacility;
	
	@Column(name = "type",length=30)
	private String requestType;
	
	@Embedded
	private CustomerRequestRecipient recipient;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy= "fromRequest")
	private Set<RequestedItem> requestedItems;
	
	public CustomerRequest(){}
	
	public CustomerRequest(String requestNumber){this.requestNumber = requestNumber;}

	/**
	 * @return the requestNumber
	 */
	public String getRequestNumber() {
		return requestNumber;
	}

	/**
	 * @param requestNumber the requestNumber to set
	 */
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * @return the requestById
	 */
	public String getRequestById() {
		return requestById;
	}

	/**
	 * @param requestById the requestById to set
	 */
	public void setRequestById(String requestById) {
		this.requestById = requestById;
	}

	/**
	 * @return the requestDate
	 */
	public Date getRequestDate() {
		return requestDate;
	}

	/**
	 * @param requestDate the requestDate to set
	 */
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the toFacility
	 */
	public Facility getToFacility() {
		return toFacility;
	}

	/**
	 * @param toFacility the toFacility to set
	 */
	public void setToFacility(Facility toFacility) {
		this.toFacility = toFacility;
	}


	/**
	 * @return the requiredDate
	 */
	public Date getRequiredDate() {
		return requiredDate;
	}

	/**
	 * @param requiredDate the requiredDate to set
	 */
	public void setRequiredDate(Date requiredDate) {
		this.requiredDate = requiredDate;
	}

	/**
	 * @return the requestedItems
	 */
	public Set<RequestedItem> getRequestedItems() {
		return requestedItems;
	}

	/**
	 * @param requestedItems the requestedItems to set
	 */
	public void setRequestedItems(Set<RequestedItem> requestedItems) {
		this.requestedItems = requestedItems;
	}

	/**
	 * @return the recipient
	 */
	public CustomerRequestRecipient getRecipient() {
		return recipient;
	}

	/**
	 * @param recipient the recipient to set
	 */
	public void setRecipient(CustomerRequestRecipient recipient) {
		this.recipient = recipient;
	}

	/**
	 * @return the requestType
	 */
	public String getRequestType() {
		return requestType;
	}

	/**
	 * @param requestType the requestType to set
	 */
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	
	
}
