package com.dhl.inventory.domain.customerinterfaces;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisViewOnlyDomainEntity;

@Entity
@Table(name = "cr_req_iss_details")
public class CustomerRequestIssuanceDetails extends NsisViewOnlyDomainEntity {
	
	@Id
	@Column(name = "request_number", unique = true,length=32)
	private String requestNumber;
	
	@Column(name = "status",length=25)
	private String status;
	
	@Column(name = "cust_name", nullable = false,length=300)
	private String custodianName;
	
	@Column(name = "custd_remarks",length=500)
	private String custodianRemarks;
	
	@Column(name = "route_code", length=10)
	private String routeCode;
	
	public CustomerRequestIssuanceDetails(){}

	/**
	 * @return the requestNumber
	 */
	public String getRequestNumber() {
		return requestNumber;
	}

	/**
	 * @param requestNumber the requestNumber to set
	 */
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the custodianName
	 */
	public String getCustodianName() {
		return custodianName;
	}

	/**
	 * @param custodianName the custodianName to set
	 */
	public void setCustodianName(String custodianName) {
		this.custodianName = custodianName;
	}

	/**
	 * @return the custodianRemarks
	 */
	public String getCustodianRemarks() {
		return custodianRemarks;
	}

	/**
	 * @param custodianRemarks the custodianRemarks to set
	 */
	public void setCustodianRemarks(String custodianRemarks) {
		this.custodianRemarks = custodianRemarks;
	}

	/**
	 * @return the routeCode
	 */
	public String getRouteCode() {
		return routeCode;
	}

	/**
	 * @param routeCode the routeCode to set
	 */
	public void setRouteCode(String routeCode) {
		this.routeCode = routeCode;
	}
}
