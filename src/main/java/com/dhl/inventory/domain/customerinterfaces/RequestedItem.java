package com.dhl.inventory.domain.customerinterfaces;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisNonLoggedDomainEntity;

@Entity
@Table(name = "cr_req_item")
public class RequestedItem extends NsisNonLoggedDomainEntity{
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="cust_req_id", nullable=false)
	private CustomerRequest fromRequest;
	
	@Column(name = "item_code", nullable = false,length=50)
	private String itemCode;
	
	@Column(name = "qty")
	private int quantity;
	
	@Column(name = "remarks")
	private String remarks;
	
	public RequestedItem(){}

	public CustomerRequest getFromRequest() {
		return fromRequest;
	}

	public void setFromRequest(CustomerRequest fromRequest) {
		this.fromRequest = fromRequest;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}
