package com.dhl.inventory.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Recipient {
	
	@Column(name = "delivery_address",length=500)
	protected String deliveryAddress;
	
	@Column(name = "recepient_id",length=36)
	protected String recipientId;
	
	@Column(name = "contact_number",length=20)
	protected String contactNumber;
	
	@Column(name = "email_address",length=100)
	protected String emailAddress;

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getRecipientId() {
		return recipientId;
	}

	public void setRecipientId(String recipientId) {
		this.recipientId = recipientId;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	
}
