package com.dhl.inventory.domain.warehouse.requestissuance;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisDomainEntity;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;

@Entity(name = "WarehouseRequestIssuance")
@Table(name = "wh_stock_req_iss")
public class RequestIssuance extends NsisDomainEntity{
	
	@Column(name = "status",length=25)
	private String status;
	
	@Column(name = "issuance_date")
	private Date issuanceDate;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="req_id",nullable = false) 
	private SupplyRequest supplyRequest;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy= "forRequestIssuance")
	private Set<SuppliedItemsGroup> suppliedItemsGroup;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="custd_id") 
	private NsisUser handlingCustodian;
	
	@Column(name = "custd_remarks",length=500)
	private String custodianRemarks;
	
	public RequestIssuance(){}
	
	public RequestIssuance(final SupplyRequest supplyRequest){
		this.supplyRequest = supplyRequest;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public SupplyRequest getSupplyRequest() {
		return supplyRequest;
	}

	public void setSupplyRequest(SupplyRequest supplyRequest) {
		this.supplyRequest = supplyRequest;
	}

	public NsisUser getHandlingCustodian() {
		return handlingCustodian;
	}

	public void setHandlingCustodian(NsisUser handlingCustodian) {
		this.handlingCustodian = handlingCustodian;
	}

	public String getCustodianRemarks() {
		return custodianRemarks;
	}

	public void setCustodianRemarks(String custodianRemarks) {
		this.custodianRemarks = custodianRemarks;
	}

	public Date getIssuanceDate() {
		return issuanceDate;
	}

	public void setIssuanceDate(Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}

	public Set<SuppliedItemsGroup> getSuppliedItemsGroup() {
		return suppliedItemsGroup;
	}

	public void setSuppliedItemsGroup(Set<SuppliedItemsGroup> suppliedItemsGroup) {
		this.suppliedItemsGroup = suppliedItemsGroup;
	}
}
