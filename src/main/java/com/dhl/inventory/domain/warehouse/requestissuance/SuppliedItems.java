package com.dhl.inventory.domain.warehouse.requestissuance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisDomainEntity;
import com.dhl.inventory.domain.warehouse.inventorymaintenance.DeliveredItemBatch;

@Entity(name= "WarehouseSuppliedItems")
@Table(name = "wh_stock_iss_item")
public class SuppliedItems extends NsisDomainEntity{
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="grp_id", nullable=false)
	private SuppliedItemsGroup suppliedItemsGroup;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="supl_batch_id", nullable=false)
	private DeliveredItemBatch supplySource;
	
	@Column(name = "qty_iss", length=32)
	private int quantity;
	
	@Column(name = "remarks", length=200)
	private String remarks;
	
	public SuppliedItems(){}
	
	public SuppliedItems(final DeliveredItemBatch supplySource){
		this(supplySource,null);
	}
	
	public SuppliedItems(final DeliveredItemBatch supplySource, final SuppliedItemsGroup forSuppliedItemsGroup){
		this.supplySource = supplySource;
		this.suppliedItemsGroup = forSuppliedItemsGroup;
	}

	public SuppliedItemsGroup getSuppliedItemsGroup() {
		return suppliedItemsGroup;
	}

	public void setSuppliedItemsGroup(SuppliedItemsGroup suppliedItemsGroup) {
		this.suppliedItemsGroup = suppliedItemsGroup;
	}

	public DeliveredItemBatch getSupplySource() {
		return supplySource;
	}

	public void setSupplySource(DeliveredItemBatch supplySource) {
		this.supplySource = supplySource;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}
