package com.dhl.inventory.domain.warehouse.orderrequest;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisDomainEntity;
import com.dhl.inventory.domain.security.NsisUser;

@Entity
@Table(name = "wh_order_appr_dtls")
public class OrderRequestApproval extends NsisDomainEntity {
	
	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "dte_respond")
	private Date dateResponded;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="order_req_id",nullable = false) 
	private OrderRequest orderRequest;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="appr_id",nullable = false) 
	private NsisUser approver;
	
	public OrderRequestApproval(){
		
	}
	
	public OrderRequestApproval(OrderRequest orderRequest, NsisUser approver){
		this.orderRequest = orderRequest;
		this.approver = approver;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getDateResponded() {
		return dateResponded;
	}

	public void setDateResponded(Date dateResponded) {
		this.dateResponded = dateResponded;
	}

	public OrderRequest getOrderRequest() {
		return orderRequest;
	}

	public NsisUser getApprover() {
		return approver;
	}

	public void setApprover(NsisUser approver) {
		this.approver = approver;
	}
}
