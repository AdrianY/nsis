package com.dhl.inventory.domain.warehouse.requestissuance;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisNonLoggedDomainEntity;

@Entity(name= "RequestedItemToWarehouse")
@Table(name = "wh_stock_iss_itemgrp")
public class SuppliedItemsGroup extends NsisNonLoggedDomainEntity {
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="iss_id", nullable=false)
	private RequestIssuance forRequestIssuance;
	
	@Column(name = "item_code", nullable = false,length=50)
	private String forRequestedItemCode;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy= "suppliedItemsGroup")
	private Set<SuppliedItems> suppliedItems;
	
	public SuppliedItemsGroup(){}
	
	public SuppliedItemsGroup(RequestIssuance requestIssuance){
		this.forRequestIssuance = requestIssuance;
	}

	public RequestIssuance getForRequestIssuance() {
		return forRequestIssuance;
	}

	public void setForRequestIssuance(RequestIssuance forRequestIssuance) {
		this.forRequestIssuance = forRequestIssuance;
	}

	public Set<SuppliedItems> getSuppliedItems() {
		return suppliedItems;
	}

	public void setSuppliedItems(Set<SuppliedItems> suppliedItems) {
		this.suppliedItems = suppliedItems;
	}

	public String getForRequestedItemCode() {
		return forRequestedItemCode;
	}

	public void setForRequestedItemCode(String forRequestedItemCode) {
		this.forRequestedItemCode = forRequestedItemCode;
	}
}
