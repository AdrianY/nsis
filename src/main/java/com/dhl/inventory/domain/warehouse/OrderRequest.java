package com.dhl.inventory.domain.warehouse;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisDomainEntity;
import com.dhl.inventory.domain.maintenance.Facility;

@Entity
@Table(name = "wh_order_req")
public class OrderRequest extends NsisDomainEntity{
	
	@Column(name = "order_number", unique = true,length=32)
	private String orderNumber;
	
	@Column(name = "order_by",length=36)
	private String orderById;
	
	@Column(name = "order_date")
	private Date orderDate;
	
	@Column(name = "order_type",length=20)
	private String orderType;
	
	@Column(name = "category",length=150)
	private String category;
	
	@Column(name = "order_cycle",length=5)
	private String orderCycle;
	
	@Column(name = "currency",length=3)
	private String currency;
	
	@Embedded
	private Recipient recipient;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="facility_id",nullable = false) 
	private Facility facility;
	
	@Column(name = "delivery_date")
	private Date deliveryDate;
	
	@Column(name = "status",length=25)
	private String status;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy= "order")
	private Set<OrderRequestItem> orderItems;

	@Override
	public void validateThis() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inactivateThis() {
		// TODO Auto-generated method stub
		
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getOrderById() {
		return orderById;
	}

	public void setOrderById(String orderById) {
		this.orderById = orderById;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getOrderCycle() {
		return orderCycle;
	}

	public void setOrderCycle(String orderCycle) {
		this.orderCycle = orderCycle;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Recipient getRecipient() {
		return recipient;
	}

	public void setRecipient(Recipient recipient) {
		this.recipient = recipient;
	}

	public Facility getFacility() {
		return facility;
	}

	public void setFacility(Facility facility) {
		this.facility = facility;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Set<OrderRequestItem> getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(Set<OrderRequestItem> orderItems) {
		this.orderItems = orderItems;
	}
	
	
}
