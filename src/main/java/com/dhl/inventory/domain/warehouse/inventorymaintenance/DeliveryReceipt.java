package com.dhl.inventory.domain.warehouse.inventorymaintenance;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisDomainEntity;
import com.dhl.inventory.domain.warehouse.orderrequest.OrderRequest;

@Entity
@Table(name = "wh_stock_repl_grp")
public class DeliveryReceipt extends NsisDomainEntity {
	
	@Column(name = "date_delivered")
	private Date dateDelivered;
	
	@Column(name = "status",length=25)
	private String status;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="order_req_id",nullable = false) 
	private OrderRequest orderRequest;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy= "group")
	private Set<DeliveredItemBatch> itemBatches;
	
	public DeliveryReceipt(){}
	
	public DeliveryReceipt(OrderRequest orderRequest){
		this.orderRequest = orderRequest;
	}
	
	public void setOrderRequest(OrderRequest orderRequest) {
		this.orderRequest = orderRequest;
	}

	public Date getDateDelivered() {
		return dateDelivered;
	}

	public void setDateDelivered(Date dateDelivered) {
		this.dateDelivered = dateDelivered;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public OrderRequest getOrderRequest() {
		return orderRequest;
	}

	public Set<DeliveredItemBatch> getItemBatches() {
		return itemBatches;
	}

	public void setItemBatches(Set<DeliveredItemBatch> itemBatches) {
		this.itemBatches = itemBatches;
	}
}
