package com.dhl.inventory.domain.warehouse;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisNonLoggedDomainEntity;
import com.dhl.inventory.domain.maintenance.FacilityInventoryItem;

@Entity
@Table(name = "wh_order_req_item")
public class OrderRequestItem extends NsisNonLoggedDomainEntity{
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="order_id", nullable=false)
	private OrderRequest order;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="item_id",nullable = false) 
	private FacilityInventoryItem item;
	
	@Column(name = "qty_by_uom")
	private int quantityByUOM;

	@Override
	public void validateThis() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inactivateThis() {
		// TODO Auto-generated method stub
		
	}

	public OrderRequest getOrder() {
		return order;
	}

	public void setOrder(OrderRequest order) {
		this.order = order;
	}

	public FacilityInventoryItem getItem() {
		return item;
	}

	public void setItem(FacilityInventoryItem item) {
		this.item = item;
	}

	public int getQuantityByUOM() {
		return quantityByUOM;
	}

	public void setQuantityByUOM(int quantityByUOM) {
		this.quantityByUOM = quantityByUOM;
	}
	
	
}
