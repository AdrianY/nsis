package com.dhl.inventory.domain.warehouse;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Recipient {
	
	@Column(name = "delivery_address",length=500)
	private String deliveryAddress;
	
	@Column(name = "recepient_id",length=36)
	private String recipientId;
	
	@Column(name = "contact_number",length=20)
	private String contactNumber;
	
	@Column(name = "email_address",length=100)
	private String emailAddress;
}
