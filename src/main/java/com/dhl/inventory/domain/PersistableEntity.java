package com.dhl.inventory.domain;

public interface PersistableEntity {
	void validateThis();
	void inactivateThis();
}
