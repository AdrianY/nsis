package com.dhl.inventory.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.GenericGenerator;

@MappedSuperclass
public abstract class NsisNonLoggedDomainEntity{
	
	@Column(name = "record_status",nullable = true,length=2)
	protected String recordStatus;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name = "id", unique = true,length=36)
	private String id;

	@Version
	private Long version;

	@Transient
	private UUID uuid;
	
	@Transient
	public boolean isNew() {
	    return (this.id == null);
	}

	public String getId() {
		return id;
	}

	@PrePersist
	protected void prePersist() {
		syncUuidString();
		recordStatus = "AT";
	}

	protected void syncUuidString() {
		if (null == id) {
			// initial method call fills the id
			getUuid();
		}
	}

	public UUID getUuid() {
		if (id == null) {
			if (uuid == null) {
				uuid = UUID.randomUUID();
			}
			id = uuid.toString();
		}
		if (uuid == null && id != null) {
			uuid = UUID.fromString(id);
		}
		return uuid;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		NsisDomainEntity that = (NsisDomainEntity) o;

		if (getUuid() != null ? !getUuid().equals(that.getUuid()) : that.getUuid() != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return getUuid() != null ? getUuid().hashCode() : 0;
	}

	public Long getVersion() {
		return version;
	}
}
