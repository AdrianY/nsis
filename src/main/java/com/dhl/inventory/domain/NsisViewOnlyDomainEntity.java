package com.dhl.inventory.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class NsisViewOnlyDomainEntity {
	@Column(name = "record_cre_by", nullable = true,length=36)
	protected String recordCreatedBy;
	
	@Column(name = "record_upd_by",  nullable = true,length=36)
	protected String recordUpdatedBy;
	
	@Column(name = "date_created",nullable = true)
	protected Date dateCreated;
	
	@Column(name = "date_updated",nullable = true)
	protected Date dateUpdated;
	
	@Column(name = "record_status",nullable = true,length=2)
	protected String recordStatus;
	

	/**
	 * @return the recordCreatedBy
	 */
	public String getRecordCreatedBy() {
		return recordCreatedBy;
	}

	/**
	 * @param recordCreatedBy the recordCreatedBy to set
	 */
	public void setRecordCreatedBy(String recordCreatedBy) {
		this.recordCreatedBy = recordCreatedBy;
	}

	/**
	 * @return the recordUpdatedBy
	 */
	public String getRecordUpdatedBy() {
		return recordUpdatedBy;
	}

	/**
	 * @param recordUpdatedBy the recordUpdatedBy to set
	 */
	public void setRecordUpdatedBy(String recordUpdatedBy) {
		this.recordUpdatedBy = recordUpdatedBy;
	}

	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}

	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * @return the dateUpdated
	 */
	public Date getDateUpdated() {
		return dateUpdated;
	}

	/**
	 * @param dateUpdated the dateUpdated to set
	 */
	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	/**
	 * @return the recordStatus
	 */
	public String getRecordStatus() {
		return recordStatus;
	}

	/**
	 * @param recordStatus the recordStatus to set
	 */
	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

}
