package com.dhl.inventory.domain.servicecenter.requestissuance;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisViewOnlyDomainEntity;
import com.dhl.inventory.domain.maintenance.Facility;

@Entity(name= "ServiceCenterSubmittedCustomerRequest")
@Table(name = "svc_subm_cr_req")
public class SubmittedCustomerRequest extends NsisViewOnlyDomainEntity {
	
	@Id
	@Column(name = "id", unique = true,length=32)
	private String id;
	
	@Column(name = "request_number", unique = true,length=32)
	private String requestNumber;
	
	@Column(name = "request_by",length=36)
	private String requestById;
	
	@Column(name = "type",length=30)
	private String requestType;
	
	@Column(name = "request_date")
	private Date requestDate;
	
	@Column(name = "required_date")
	private Date requiredDate;
	
	@Column(name = "status",length=25)
	private String status;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="to_facility_id",nullable = false) 
	private Facility toFacility;

	@Embedded 
	private Customer recipient;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy= "fromRequest")
	private Set<SubmittedRequestedItem> requestedItems;
	
	public SubmittedCustomerRequest(){}


	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the recipient
	 */
	public Customer getRecipient() {
		return recipient;
	}

	/**
	 * @return the requestedItems
	 */
	public Set<SubmittedRequestedItem> getRequestedItems() {
		return requestedItems;
	}
	
	/**
	 * @return the requestNumber
	 */
	public String getRequestNumber() {
		return requestNumber;
	}

	/**
	 * @return the requestById
	 */
	public String getRequestById() {
		return requestById;
	}

	/**
	 * @return the requestDate
	 */
	public Date getRequestDate() {
		return requestDate;
	}

	/**
	 * @return the requiredDate
	 */
	public Date getRequiredDate() {
		return requiredDate;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @return the toFacility
	 */
	public Facility getToFacility() {
		return toFacility;
	}


	/**
	 * @return the requestType
	 */
	public String getRequestType() {
		return requestType;
	}
	
	
}
