package com.dhl.inventory.domain.servicecenter.inventorymaintenance;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.dhl.inventory.domain.NsisDomainEntity;
import com.dhl.inventory.domain.maintenance.WarehouseLocation;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequestItem;
import com.dhl.inventory.util.FormatterUtil;

@Entity(name= "ServiceCenterDeliveredItemBatch")
@Table(name = "svc_stock_repl_rec")
public class DeliveredItemBatch extends NsisDomainEntity {
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="group_id", nullable=false)
	private DeliveryReceipt group;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="ori_id", nullable=false)
	private SupplyRequestItem supplyRequestItem;
	
	@Column(name = "quantity",length=32)
	private int quantity;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="rack_loc_id",nullable = false) 
	private WarehouseLocation rackingLocation;
	
	@Column(name = "date_stored")
	private Date dateStored;
	
	public DeliveredItemBatch(){}
	
	public DeliveredItemBatch(SupplyRequestItem supplyRequestItem){
		this.supplyRequestItem = supplyRequestItem;
	}

	public DeliveryReceipt getGroup() {
		return group;
	}

	public void setGroup(DeliveryReceipt group) {
		this.group = group;
	}

	public SupplyRequestItem getSupplyRequestItem() {
		return supplyRequestItem;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public WarehouseLocation getRackingLocation() {
		return rackingLocation;
	}

	public void setRackingLocation(WarehouseLocation rackingLocation) {
		this.rackingLocation = rackingLocation;
	}

	public Date getDateStored() {
		return dateStored;
	}

	public void setDateStored(Date dateStored) {
		this.dateStored = dateStored;
	}
	
	@Transient
	public String getBatchNumber(){
		String itemCode = this.supplyRequestItem.getItem().getInventoryItem().getCodeName();
		String rackingCode = this.rackingLocation.getRackCode();
		String dateStored = FormatterUtil.noSlashShortDateFormat(this.dateStored);
		return itemCode.replace("-", "")+"-"+rackingCode.replace("-", "")+"-"+dateStored;
	}
}
