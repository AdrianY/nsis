package com.dhl.inventory.domain.servicecenter.inventorymaintenance;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisDomainEntity;
import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.security.NsisUser;

@Entity(name= "ServiceCenterPhysicalStockCountRequest")
@Table(name = "svc_phy_stk_count_req")
public class PhysicalStockCountRequest extends NsisDomainEntity{
	@Column(name = "status",length=25)
	private String status;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy= "request")
	private Set<PhysicalStockCount> physicalStockCounts;
	
	@Column(name = "request_number",nullable=false, unique = true,length=32)
	private String requestNumber;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="request_by",nullable = false) 
	private NsisUser requestBy;
	
	@Column(name = "request_date")
	private Date requestDate;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="facility_id",nullable = false) 
	private Facility forFacility;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy= "request")
	private PhysicalStockCountRequestApproval approvalDetails;

	public PhysicalStockCountRequest(String requestNumber) {
		this.requestNumber = requestNumber;
	}
	
	public PhysicalStockCountRequest() {}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the physicalStockCounts
	 */
	public Set<PhysicalStockCount> getPhysicalStockCounts() {
		return physicalStockCounts;
	}

	/**
	 * @param physicalStockCounts the physicalStockCounts to set
	 */
	public void setPhysicalStockCounts(Set<PhysicalStockCount> physicalStockCounts) {
		this.physicalStockCounts = physicalStockCounts;
	}

	/**
	 * @return the requestNumber
	 */
	public String getRequestNumber() {
		return requestNumber;
	}

	/**
	 * @param requestNumber the requestNumber to set
	 */
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * @return the requestById
	 */
	public NsisUser getRequestBy() {
		return requestBy;
	}

	/**
	 * @param requestById the requestById to set
	 */
	public void setRequestBy(NsisUser requestBy) {
		this.requestBy = requestBy;
	}

	/**
	 * @return the requestDate
	 */
	public Date getRequestDate() {
		return requestDate;
	}

	/**
	 * @param requestDate the requestDate to set
	 */
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	/**
	 * @return the approvalDetails
	 */
	public PhysicalStockCountRequestApproval getApprovalDetails() {
		return approvalDetails;
	}

	/**
	 * @param approvalDetails the approvalDetails to set
	 */
	public void setApprovalDetails(PhysicalStockCountRequestApproval approvalDetails) {
		this.approvalDetails = approvalDetails;
	}

	/**
	 * @return the forFacility
	 */
	public Facility getForFacility() {
		return forFacility;
	}

	/**
	 * @param forFacility the forFacility to set
	 */
	public void setForFacility(Facility forFacility) {
		this.forFacility = forFacility;
	}
}
