package com.dhl.inventory.domain.servicecenter.requestissuance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisViewOnlyNonLoggedDomainEntity;

@Entity(name= "ServiceCenterSubmittedRequestedItem")
@Table(name = "svc_subm_cr_req_item")
public class SubmittedRequestedItem extends NsisViewOnlyNonLoggedDomainEntity {
	
	@Column(name = "item_code", nullable = false,length=50)
	protected String itemCode;
	
	@Column(name = "qty")
	protected int quantity;
	
	@Column(name = "remarks")
	protected String remarks;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="cust_req_id", nullable=false)
	private SubmittedCustomerRequest fromRequest;
	
	public SubmittedRequestedItem(){}

	/**
	 * @return the fromRequest
	 */
	public SubmittedCustomerRequest getFromRequest() {
		return fromRequest;
	}
	
	/**
	 * @return the itemCode
	 */
	public String getItemCode() {
		return itemCode;
	}

	/**
	 * @param itemCode the itemCode to set
	 */
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}
