package com.dhl.inventory.domain.servicecenter.inventorymaintenance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisNonLoggedDomainEntity;

@Entity(name= "ServiceCenterPhysicalStockCountApproval")
@Table(name = "svc_phy_stk_cnt_app")
public class PhysicalStockCountApproval extends NsisNonLoggedDomainEntity {
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="approval_id",nullable = false)
	private PhysicalStockCountRequestApproval approval;
	
	@Column(name = "batch_number")
	private String batchNumber;
	
	public PhysicalStockCountApproval(final String batchNumber, final PhysicalStockCountRequestApproval approval){
		this.batchNumber = batchNumber;
		this.approval = approval;
	}
	
	public PhysicalStockCountApproval(){}

	/**
	 * @return the approval
	 */
	public PhysicalStockCountRequestApproval getApproval() {
		return approval;
	}

	/**
	 * @return the batchNumber
	 */
	public String getBatchNumber() {
		return batchNumber;
	}
}
