package com.dhl.inventory.domain.servicecenter.supplyrequest;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisDomainEntity;
import com.dhl.inventory.domain.Recipient;
import com.dhl.inventory.domain.maintenance.Facility;

@Entity
@Table(name = "svc_sup_req")
public class SupplyRequest extends NsisDomainEntity {

	@Column(name = "request_number",nullable=false, unique = true,length=32)
	private String requestNumber;
	
	@Column(name = "request_by",length=36)
	private String requestById;
	
	@Column(name = "request_date")
	private Date requestDate;
	
	@Column(name = "currency",length=3)
	private String currency;
	
	@Embedded
	private Recipient recipient;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="facility_id",nullable = false) 
	private Facility facility;
	
	@Column(name = "issuance_date")
	private Date issuanceDate;
	
	@Column(name = "status",length=25)
	private String status;
	
	@Column(name = "req_cycle",length=5)
	private String requestCycle;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy= "request")
	private Set<SupplyRequestItem> requestedItems;
	
	public SupplyRequest(){}
	
	public SupplyRequest(String requestNumber){
		this.requestNumber = requestNumber;
	}
	
	public String getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	public String getRequestById() {
		return requestById;
	}

	public void setRequestById(String requestById) {
		this.requestById = requestById;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Recipient getRecipient() {
		return recipient;
	}

	public void setRecipient(Recipient recipient) {
		this.recipient = recipient;
	}

	public Facility getFacility() {
		return facility;
	}

	public void setFacility(Facility facility) {
		this.facility = facility;
	}

	public Date getIssuanceDate() {
		return issuanceDate;
	}

	public void setIssuanceDate(Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Set<SupplyRequestItem> getRequestedItems() {
		return requestedItems;
	}

	public void setRequestedItems(Set<SupplyRequestItem> requestedItems) {
		this.requestedItems = requestedItems;
	}

	/**
	 * @return the requestCycle
	 */
	public String getRequestCycle() {
		return requestCycle;
	}

	/**
	 * @param requestCycle the requestCycle to set
	 */
	public void setRequestCycle(String requestCycle) {
		this.requestCycle = requestCycle;
	}
}
