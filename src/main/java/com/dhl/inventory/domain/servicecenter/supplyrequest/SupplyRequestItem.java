package com.dhl.inventory.domain.servicecenter.supplyrequest;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisNonLoggedDomainEntity;
import com.dhl.inventory.domain.maintenance.FacilityInventoryItem;

@Entity
@Table(name = "svc_sup_req_item")
public class SupplyRequestItem extends NsisNonLoggedDomainEntity {
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="order_id", nullable=false)
	private SupplyRequest request;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="item_id",nullable = false) 
	private FacilityInventoryItem item;
	
	@Column(name = "qty_by_uom")
	private int quantityByUOM;
	
	@Column(name = "remarks", length=200)
	private String remarks;

	public SupplyRequest getRequest() {
		return request;
	}

	public void setRequest(SupplyRequest request) {
		this.request = request;
	}

	public FacilityInventoryItem getItem() {
		return item;
	}

	public void setItem(FacilityInventoryItem item) {
		this.item = item;
	}

	public int getQuantityByUOM() {
		return quantityByUOM;
	}

	public void setQuantityByUOM(int quantityByUOM) {
		this.quantityByUOM = quantityByUOM;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}
