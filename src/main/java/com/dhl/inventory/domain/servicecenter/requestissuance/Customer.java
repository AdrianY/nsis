package com.dhl.inventory.domain.servicecenter.requestissuance;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Customer {
	@Column(name = "cust_name",length=200)
	private String customerName;
	
	@Column(name = "delivery_address",length=500)
	protected String deliveryAddress;
	
	@Column(name = "recepient_id",length=36)
	protected String recipientId;
	
	@Column(name = "contact_number",length=20)
	protected String contactNumber;
	
	@Column(name = "contact_name",length=100)
	protected String contactName;

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getRecipientId() {
		return recipientId;
	}

	public void setRecipientId(String recipientId) {
		this.recipientId = recipientId;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	
	/**
	 * @return the contactName
	 */
	public String getContactName() {
		return contactName;
	}

	/**
	 * @param contactName the contactName to set
	 */
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
}
