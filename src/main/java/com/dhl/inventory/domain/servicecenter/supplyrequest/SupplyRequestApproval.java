package com.dhl.inventory.domain.servicecenter.supplyrequest;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisDomainEntity;
import com.dhl.inventory.domain.security.NsisUser;

@Entity
@Table(name = "svc_sup_req_appr")
public class SupplyRequestApproval extends NsisDomainEntity {
	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "dte_respond")
	private Date dateResponded;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="order_req_id",nullable = false) 
	private SupplyRequest supplyRequest;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="appr_id",nullable = false) 
	private NsisUser approver;
	
	public SupplyRequestApproval(){}
	
	public SupplyRequestApproval(SupplyRequest supplyRequest, NsisUser approver){
		this.supplyRequest = supplyRequest;
		this.approver = approver;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getDateResponded() {
		return dateResponded;
	}

	public void setDateResponded(Date dateResponded) {
		this.dateResponded = dateResponded;
	}

	public SupplyRequest getSupplyRequest() {
		return supplyRequest;
	}

	public void setSupplyRequest(SupplyRequest supplyRequest) {
		this.supplyRequest = supplyRequest;
	}

	public NsisUser getApprover() {
		return approver;
	}

	public void setApprover(NsisUser approver) {
		this.approver = approver;
	}
}
