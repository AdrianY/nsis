package com.dhl.inventory.domain.servicecenter.requestissuance;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisDomainEntity;
import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.maintenance.Route;
import com.dhl.inventory.domain.security.NsisUser;

@Entity(name= "ServiceCenterRequestIssuance")
@Table(name = "svc_stock_req_iss")
public class RequestIssuance extends NsisDomainEntity {

	@Column(name = "status",length=25)
	private String status;
	
	@Column(name = "issuance_date")
	private Date issuanceDate;
	
	@Column(name = "custd_remarks",length=500)
	private String custodianRemarks;
	
	@Column(name = "req_number",length=32)
	private String requestNumber;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="issuing_fac_id",nullable = false) 
	private Facility issuingFacility;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy= "forRequestIssuance")
	private Set<SuppliedItemsGroup> suppliedItemsGroup;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="custd_id") 
	private NsisUser handlingCustodian;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="route_code") 
	private Route route;
	
	public RequestIssuance(){}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the issuanceDate
	 */
	public Date getIssuanceDate() {
		return issuanceDate;
	}

	/**
	 * @param issuanceDate the issuanceDate to set
	 */
	public void setIssuanceDate(Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}

	/**
	 * @return the suppliedItemsGroup
	 */
	public Set<SuppliedItemsGroup> getSuppliedItemsGroup() {
		return suppliedItemsGroup;
	}

	/**
	 * @param suppliedItemsGroup the suppliedItemsGroup to set
	 */
	public void setSuppliedItemsGroup(Set<SuppliedItemsGroup> suppliedItemsGroup) {
		this.suppliedItemsGroup = suppliedItemsGroup;
	}

	/**
	 * @return the handlingCustodian
	 */
	public NsisUser getHandlingCustodian() {
		return handlingCustodian;
	}

	/**
	 * @param handlingCustodian the handlingCustodian to set
	 */
	public void setHandlingCustodian(NsisUser handlingCustodian) {
		this.handlingCustodian = handlingCustodian;
	}

	/**
	 * @return the custodianRemarks
	 */
	public String getCustodianRemarks() {
		return custodianRemarks;
	}

	/**
	 * @param custodianRemarks the custodianRemarks to set
	 */
	public void setCustodianRemarks(String custodianRemarks) {
		this.custodianRemarks = custodianRemarks;
	}

	/**
	 * @return the requestNumber
	 */
	public String getRequestNumber() {
		return requestNumber;
	}

	/**
	 * @param requestNumber the requestNumber to set
	 */
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * @return the routeCode
	 */
	public Route getRoute() {
		return route;
	}

	/**
	 * @param route the routeCode to set
	 */
	public void setRoute(Route route) {
		this.route = route;
	}

	/**
	 * @return the issuingFacility
	 */
	public Facility getIssuingFacility() {
		return issuingFacility;
	}

	/**
	 * @param issuingFacility the issuingFacility to set
	 */
	public void setIssuingFacility(Facility issuingFacility) {
		this.issuingFacility = issuingFacility;
	}
}
