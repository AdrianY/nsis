package com.dhl.inventory.domain.servicecenter.inventorymaintenance;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisDomainEntity;
import com.dhl.inventory.domain.security.NsisUser;

@Entity(name= "ServiceCenterPhysicalStockCountRequestApproval")
@Table(name = "svc_phy_stk_cnt_req_app")
public class PhysicalStockCountRequestApproval extends NsisDomainEntity {
	
	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "dte_respond")
	private Date dateResponded;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="request_id",nullable = false)
	private PhysicalStockCountRequest request;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="appr_id",nullable = false) 
	private NsisUser approver;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy= "approval")
	private Set<PhysicalStockCountApproval> countApprovalList;

	public PhysicalStockCountRequestApproval(PhysicalStockCountRequest request, NsisUser approver) {
		this.request = request;
		this.approver = approver;
	}
	
	public PhysicalStockCountRequestApproval(){}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the dateResponded
	 */
	public Date getDateResponded() {
		return dateResponded;
	}

	/**
	 * @param dateResponded the dateResponded to set
	 */
	public void setDateResponded(Date dateResponded) {
		this.dateResponded = dateResponded;
	}

	/**
	 * @return the request
	 */
	public PhysicalStockCountRequest getRequest() {
		return request;
	}

	/**
	 * @param request the request to set
	 */
	public void setRequest(PhysicalStockCountRequest request) {
		this.request = request;
	}

	/**
	 * @return the approver
	 */
	public NsisUser getApprover() {
		return approver;
	}

	/**
	 * @param approver the approver to set
	 */
	public void setApprover(NsisUser approver) {
		this.approver = approver;
	}

	/**
	 * @return the countApprovalList
	 */
	public Set<PhysicalStockCountApproval> getCountApprovalList() {
		return countApprovalList;
	}

	/**
	 * @param countApprovalList the countApprovalList to set
	 */
	public void setCountApprovalList(Set<PhysicalStockCountApproval> countApprovalList) {
		this.countApprovalList = countApprovalList;
	}
	
	
}
