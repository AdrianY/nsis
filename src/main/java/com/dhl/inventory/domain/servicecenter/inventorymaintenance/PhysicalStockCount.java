package com.dhl.inventory.domain.servicecenter.inventorymaintenance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisNonLoggedDomainEntity;

@Entity(name= "ServiceCenterPhysicalStockCount")
@Table(name = "svc_phy_stk_count")
public class PhysicalStockCount extends NsisNonLoggedDomainEntity {
	
	@Column(name = "status",length=25)
	private String status;
	
	@Column(name = "count",length=32, nullable = false)
	private int count;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="item_batch_id",nullable = false) 
	private DeliveredItemBatch forItemBatch;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="request_id",nullable = false) 
	private PhysicalStockCountRequest request;
	
	public PhysicalStockCount(DeliveredItemBatch forItemBatch, PhysicalStockCountRequest request) {
		this.forItemBatch = forItemBatch;
		this.request = request;
	}

	public PhysicalStockCount() {}

	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}

	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * @return the forItemBatch
	 */
	public DeliveredItemBatch getForItemBatch() {
		return forItemBatch;
	}

	/**
	 * @param forItemBatch the forItemBatch to set
	 */
	public void setForItemBatch(DeliveredItemBatch forItemBatch) {
		this.forItemBatch = forItemBatch;
	}

	/**
	 * @return the request
	 */
	public PhysicalStockCountRequest getRequest() {
		return request;
	}
}
