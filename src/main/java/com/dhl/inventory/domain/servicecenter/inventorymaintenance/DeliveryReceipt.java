package com.dhl.inventory.domain.servicecenter.inventorymaintenance;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisDomainEntity;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;

@Entity(name= "ServiceCenterDeliveryReceipt")
@Table(name = "svc_stock_repl_grp")
public class DeliveryReceipt extends NsisDomainEntity{
	@Column(name = "date_delivered")
	private Date dateDelivered;
	
	@Column(name = "status",length=25)
	private String status;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="order_req_id",nullable = false) 
	private SupplyRequest supplyRequest;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy= "group")
	private Set<DeliveredItemBatch> itemBatches;
	
	public DeliveryReceipt(){}
	
	public DeliveryReceipt(SupplyRequest supplyRequest){
		this.supplyRequest = supplyRequest;
	}
	
	public void setSupplyRequest(SupplyRequest supplyRequest) {
		this.supplyRequest = supplyRequest;
	}

	public Date getDateDelivered() {
		return dateDelivered;
	}

	public void setDateDelivered(Date dateDelivered) {
		this.dateDelivered = dateDelivered;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public SupplyRequest getSupplyRequest() {
		return supplyRequest;
	}

	public Set<DeliveredItemBatch> getItemBatches() {
		return itemBatches;
	}

	public void setItemBatches(Set<DeliveredItemBatch> itemBatches) {
		this.itemBatches = itemBatches;
	}
}
