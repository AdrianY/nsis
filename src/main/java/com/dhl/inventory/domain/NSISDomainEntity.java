package com.dhl.inventory.domain;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.GenericGenerator;

@MappedSuperclass
public abstract class NsisDomainEntity{
	
	@Column(name = "record_cre_by", nullable = true,length=36)
	protected String recordCreatedBy;
	
	@Column(name = "record_upd_by",  nullable = true,length=36)
	protected String recordUpdatedBy;
	
	@Column(name = "date_created",nullable = true)
	protected Date dateCreated;
	
	@Column(name = "date_updated",nullable = true)
	protected Date dateUpdated;
	
	@Column(name = "record_status",nullable = true,length=2)
	protected String recordStatus;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name = "id", unique = true,length=36)
	private String id;

	@Version
	private Long version;

	@Transient
	private UUID uuid;
	
	@Transient
	public boolean isNew() {
	    return (this.id == null);
	}

	public String getId() {
		return id;
	}

	@PrePersist
	protected void prePersist() {
		System.out.println("id: "+id);
		syncUuidString();
		dateCreated = new Date();
		if(null == recordStatus || recordStatus.isEmpty())
			recordStatus = "AT";
		if(null == recordCreatedBy || recordCreatedBy.isEmpty())
			recordCreatedBy = "SYSTEM";
	}

	@PreUpdate
	protected void onUpdate() {
		dateUpdated = new Date();
		if(null == recordUpdatedBy || recordUpdatedBy.isEmpty())
			recordUpdatedBy = "SYSTEM";
	}


	protected void syncUuidString() {
		if (null == id) {
			// initial method call fills the id
			getUuid();
		}
	}

	public UUID getUuid() {
		if (id == null) {
			if (uuid == null) {
				uuid = UUID.randomUUID();
			}
			id = uuid.toString();
		}
		if (uuid == null && id != null) {
			uuid = UUID.fromString(id);
		}
		return uuid;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		NsisDomainEntity that = (NsisDomainEntity) o;

		if (getUuid() != null ? !getUuid().equals(that.getUuid()) : that.getUuid() != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return getUuid() != null ? getUuid().hashCode() : 0;
	}

	public String getRecordCreatedBy() {
		return recordCreatedBy;
	}

	public String getRecordUpdatedBy() {
		return recordUpdatedBy;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public Long getVersion() {
		return version;
	}

	public void setRecordCreatedBy(String recordCreatedBy) {
		this.recordCreatedBy = recordCreatedBy;
	}

	public void setRecordUpdatedBy(String recordUpdatedBy) {
		this.recordUpdatedBy = recordUpdatedBy;
	}

	/**
	 * @return the recordStatus
	 */
	public String getRecordStatus() {
		return recordStatus;
	}

	/**
	 * @param recordStatus the recordStatus to set
	 */
	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}
	
	
}
