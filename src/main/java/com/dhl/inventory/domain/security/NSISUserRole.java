package com.dhl.inventory.domain.security;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisDomainEntity;

@Entity
@Table(name = "auth_user_roles")
public class NsisUserRole extends NsisDomainEntity{
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="user_id", nullable=false)
	private NsisUser user;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="role_id", nullable=false)
	private NsisRole role;

	public NsisUserRole() {
	}

	public NsisUserRole(NsisUser user, NsisRole role) {
		this.user = user;
		this.role = role;
	}

	public NsisUser getUser() {
		return user;
	}

	public NsisRole getRole() {
		return role;
	}
}
