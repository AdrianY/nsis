package com.dhl.inventory.domain.security;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisDomainEntity;

@Entity
@Table(name = "auth_role")
public class NsisRole extends NsisDomainEntity{

	@Column(name = "code_name", unique = true, nullable = false,length=150)
	private String codeName;

	@Column(name = "description", nullable = false,length=255)
	private String description;

	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy= "role")
	private Set<NsisUserRole> assignments;

	public NsisRole(){

	}

	public NsisRole(String codeName, String description){
		this.codeName = codeName;
		this.description = description;
	}

	public String getCodeName() {
		return codeName;
	}

	public String getDescription() {
		return description;
	}

	public Set<NsisUserRole> getAssignments() {
		return assignments;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setAssignments(Set<NsisUserRole> assignments) {
		this.assignments = assignments;
	}
}
