package com.dhl.inventory.domain.security;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisDomainEntity;

@Entity
@Table(name = "auth_user")
public class NsisUser extends NsisDomainEntity{
	
	@Column(name = "username", unique = true, nullable = false,length=150)
	private String username;
	
	@Column(name = "password", nullable = false)
	private String password;
	
	@Column(name = "last_name", nullable = false,length=150)
	private String lastName;
	
	@Column(name = "first_name", nullable = false,length=150)
	private String firstName;
	
	@Column(name = "email_adress", nullable = true,length=100)
	private String emailAddress;
	
	@Column(name = "contact_number", nullable = true,length=20)
	private String contactNumber;
	
	@Column(name= "enabled", nullable = false)
	private boolean enabled;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL}, mappedBy= "user")
	private Set<NsisUserRole> roles;
	
	public NsisUser(){
		
	}
	
	public NsisUser(String username, String password, boolean enabled){
		this.username = username;
		this.password = password;
		this.enabled = enabled;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}
	
	public void setName(String firstName, String lastName){
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public String getFullName() {
		return firstName + " " + lastName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	
	public Set<NsisUserRole> getUserRoles() {
		return roles;
	}

	public void setUserRoles(Set<NsisUserRole> userRoles) {
		this.roles = userRoles;
	}
}
