package com.dhl.inventory.domain.maintenance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisDomainEntity;

@Entity
@Table(name = "mc_cust_acct")
public class CustomerAccount extends NsisDomainEntity {
	
	@Column(name = "name", length=150, nullable = false)
	private String name;
	
	@Column(name = "acct_no", length=32, nullable = false, unique = true)
	private String accountNumber;
	
	@Column(name = "contact_name",length=100)
	private String contactName;
	
	@Column(name = "contact_number",length=20)
	private String contactNumber;
	
	@Column(name = "delivery_address", length=500, nullable = false)
	private String deliveryAddress;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="facility_id",nullable = false) 
	private Facility servicingFacility;
	
	public CustomerAccount(){}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	/**
	 * @return the deliveryAddress
	 */
	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	/**
	 * @param deliveryAddress the deliveryAddress to set
	 */
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	/**
	 * @return the servicingFacility
	 */
	public Facility getServicingFacility() {
		return servicingFacility;
	}

	/**
	 * @param servicingFacility the servicingFacility to set
	 */
	public void setServicingFacility(Facility servicingFacility) {
		this.servicingFacility = servicingFacility;
	}
}
