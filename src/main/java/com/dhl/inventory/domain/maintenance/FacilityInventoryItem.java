package com.dhl.inventory.domain.maintenance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisDomainEntity;

@Entity
@Table(name = "mn_fac_inv_item")
public class FacilityInventoryItem  extends NsisDomainEntity {
	
	@Column(name = "min_val", nullable = false)
	private int minValue;
	
	@Column(name = "max_val", nullable = false)
	private int maxValue;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="item_id", nullable=false)
	private InventoryItem inventoryItem;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="facility_id", nullable=false)
	private Facility owningFacility;
	
	public FacilityInventoryItem(InventoryItem item, Facility facility){
		this.inventoryItem = item;
		this.owningFacility = facility;
	}
	
	public FacilityInventoryItem(){
		
	}
	
	public int getMinValue() {
		return minValue;
	}

	public int getMaxValue() {
		return maxValue;
	}

	public InventoryItem getInventoryItem() {
		return inventoryItem;
	}

	public Facility getOwningFacility() {
		return owningFacility;
	}

	public void setMinValue(int minValue) {
		this.minValue = minValue;
	}

	public void setMaxValue(int maxValue) {
		this.maxValue = maxValue;
	}
}
