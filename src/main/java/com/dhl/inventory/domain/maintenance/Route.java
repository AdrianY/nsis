package com.dhl.inventory.domain.maintenance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.dhl.inventory.domain.NsisDomainEntity;

@Entity
@Table(
		name = "mn_courier_route",
		uniqueConstraints=@UniqueConstraint(columnNames={"facility_id", "route_code"})
)
public class Route extends NsisDomainEntity {
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="facility_id",nullable = false) 
	private Facility assignedFacility;
	
	@Column(name = "route_code",nullable = false,length=10)
	private String routeCode;
	
	public Route(){}

	/**
	 * @return the routeCode
	 */
	public String getRouteCode() {
		return routeCode;
	}

	/**
	 * @param routeCode the routeCode to set
	 */
	public void setRouteCode(String routeCode) {
		this.routeCode = routeCode;
	}

	/**
	 * @return the assignedFacility
	 */
	public Facility getAssignedFacility() {
		return assignedFacility;
	}

	/**
	 * @param assignedFacility the assignedFacility to set
	 */
	public void setAssignedFacility(Facility assignedFacility) {
		this.assignedFacility = assignedFacility;
	}
	
	
}
