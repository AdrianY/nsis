package com.dhl.inventory.domain.maintenance;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisDomainEntity;

@Entity
@Table(name = "mn_curr")
public class Currency extends NsisDomainEntity {
	
	@Column(name = "type",nullable = false,length=100)
	private String code;
	
	@Column(name = "ex_to_php",nullable = false,precision=10,scale=2)
	private BigDecimal exchangeRateToPhp;
	
	public Currency(){
	}
	
	public Currency(String code, BigDecimal exchangeRateToPhp){
		this.code = code;
		this.exchangeRateToPhp = exchangeRateToPhp;
	}
	
	public String getCode() {
		return code;
	}

	public BigDecimal getExchangeRateToPhp() {
		return exchangeRateToPhp;
	}

}
