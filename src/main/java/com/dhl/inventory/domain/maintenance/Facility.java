package com.dhl.inventory.domain.maintenance;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.dhl.inventory.domain.NsisDomainEntity;

@Entity
@Table(
		name = "mn_fac",
		uniqueConstraints=@UniqueConstraint(columnNames={"code_name", "type"})
)
public class Facility extends NsisDomainEntity {
	
	@Column(name = "address", unique = true, nullable = false,length=200)
	private String address;
	
	@Column(name = "code_name",nullable = false,length=10)
	private String codeName;
	
	@Column(name = "type", nullable = false,length=20)
	private String type;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy= "owningFacility")
	private Set<FacilityInventoryItem> allowableItemsInInventory;

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public Set<FacilityInventoryItem> getAllowableItemsInInventory() {
		return allowableItemsInInventory;
	}

	public void setAllowableItemsInInventory(Set<FacilityInventoryItem> allowableItemsInInventory) {
		this.allowableItemsInInventory = allowableItemsInInventory;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
