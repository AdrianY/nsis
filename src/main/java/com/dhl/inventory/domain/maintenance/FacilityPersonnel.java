package com.dhl.inventory.domain.maintenance;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisDomainEntity;
import com.dhl.inventory.domain.security.NsisUser;

@Entity
@Table(name = "mn_fac_per")
public class FacilityPersonnel extends NsisDomainEntity {

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="user_id",nullable = false) 
	private NsisUser userAccessDetails;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="facility_id",nullable = false) 
	private Facility assignedFacility;
	
	public FacilityPersonnel(){
		
	}
	
	public FacilityPersonnel(NsisUser userAccessDetails, Facility assignedFacility){
		this.userAccessDetails = userAccessDetails;
		this.assignedFacility = assignedFacility;
	}
	
	public NsisUser getUserAccessDetails() {
		return userAccessDetails;
	}

	public Facility getAssignedFacility() {
		return assignedFacility;
	}
}
