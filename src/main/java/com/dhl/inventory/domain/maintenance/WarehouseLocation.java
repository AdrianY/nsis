package com.dhl.inventory.domain.maintenance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.dhl.inventory.domain.NsisDomainEntity;

@Entity
@Table(
	name = "mn_wh_loc",
	uniqueConstraints=@UniqueConstraint(columnNames={"facility_id", "rack_code"})
)
public class WarehouseLocation extends NsisDomainEntity {
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="facility_id", nullable=false)
	private Facility facility;
	
	/**
	 * should be unique per facility code
	 */
	@Column(name = "rack_code", nullable = false,length=50)
	private String rackCode;
	
	public WarehouseLocation(){}
	
	public WarehouseLocation(Facility facility){
		this.facility = facility;
	}
	
	public String getRackCode() {
		return rackCode;
	}

	public void setRackCode(String rackCode) {
		this.rackCode = rackCode;
	}

	public Facility getFacility() {
		return facility;
	}
}
