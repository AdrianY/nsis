package com.dhl.inventory.domain.maintenance;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dhl.inventory.domain.NsisDomainEntity;

@Entity
@Table(name = "mn_inv_item")
public class InventoryItem extends NsisDomainEntity {

	@Column(name = "code_name", unique = true, nullable = false,length=50)
	private String codeName;
	
	@Column(name = "type",nullable = false,length=100)
	private String type;
	
	@Column(name = "pcs_uom",nullable = false)
	private int piecesPerUnitOfMeasurement;
	
	@Column(name = "uom",nullable = false,length=150)
	private String unitOfMeasurement;
	
	@Column(name = "description",nullable = false,length=255)
	private String description;
	
	@Column(name = "item_cost",nullable = false,precision=10,scale=2)
	private BigDecimal itemCost;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="currency_id",nullable = false) 
	private Currency currency;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy= "inventoryItem")
	private Set<FacilityInventoryItem> facilityUsage;
	
	public InventoryItem(){}
	
	public InventoryItem(String codeName, String type){
		this.codeName = codeName;
		this.type = type;
	}
	
	public String getCodeName() {
		return codeName;
	}

	public int getPiecesPerUnitOfMeasurement() {
		return piecesPerUnitOfMeasurement;
	}

	public String getUnitOfMeasurement() {
		return unitOfMeasurement;
	}

	public String getDescription() {
		return description;
	}

	public Set<FacilityInventoryItem> getFacilityUsage() {
		return facilityUsage;
	}

	public void setPiecesPerUnitOfMeasurement(int piecesPerUnitOfMeasurement) {
		this.piecesPerUnitOfMeasurement = piecesPerUnitOfMeasurement;
	}

	public void setUnitOfMeasurement(String unitOfMeasurement) {
		this.unitOfMeasurement = unitOfMeasurement;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setFacilityUsage(Set<FacilityInventoryItem> facilityUsage) {
		this.facilityUsage = facilityUsage;
	}

	public String getType() {
		return type;
	}
	
	public BigDecimal getItemCost() {
		return itemCost;
	}

	public void setItemCost(BigDecimal itemCost) {
		this.itemCost = itemCost;
	}
	
	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
}
