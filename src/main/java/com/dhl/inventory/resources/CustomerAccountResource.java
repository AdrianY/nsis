package com.dhl.inventory.resources;


import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.queryobject.maintenance.CustomerAccountLQO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.maintenance.CustomerAccountService;

@Component
@Path("/customeraccounts")
public class CustomerAccountResource {
	private static final Logger LOGGER = LoggerFactory.getLogger(InventoryItemResource.class);
	
	@Autowired
	@Qualifier("customerAccountService")
	private CustomerAccountService customerAccountService;
	
	@GET
	@Path("/{accountNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomerAccount(@PathParam("accountNumber") String accountNumber){
		LOGGER.info("GET - CustomerAccount: "+accountNumber);
		Response response = null;
		try {
			response = 
				ResponseWrapper.successfulResponse(customerAccountService.getCustomerAccount(accountNumber));
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		}
		
		return response;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomerAccounts(@BeanParam CustomerAccountLQO itemListTransfer){
		LOGGER.info("GET - CustomerAccounts List: "+itemListTransfer.getAccountNumber());
		return ResponseWrapper.successfulResponse(customerAccountService.findAllCustomerAccounts(itemListTransfer));
	}
	
}
