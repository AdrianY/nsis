package com.dhl.inventory.resources;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.commandobject.warehouse.inventorymaintenance.DeliveryReceiptCO;
import com.dhl.inventory.dto.commandobject.warehouse.orderrequest.OrderRequestCO;
import com.dhl.inventory.dto.queryobject.QueryResultObjectWrapper;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.warehouse.orderrequest.OrderRequestLQO;
import com.dhl.inventory.dto.queryobject.warehouse.orderrequest.OrderRequestQRO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.CannotBeRemovedException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.services.warehouse.inventorymaintenance.DeliveryReceiptService;
import com.dhl.inventory.services.warehouse.orderrequest.OrderRequestItemService;
import com.dhl.inventory.services.warehouse.orderrequest.OrderRequestService;

@Component
@Path("/orderrequests")
public class OrderRequestResource {
	private static final Logger LOGGER = LoggerFactory.getLogger(OrderRequestResource.class);
	
	@Autowired
	@Qualifier("orderRequestService")
	private OrderRequestService orderRequestService;
	
	@Autowired
	@Qualifier("orderRequestItemService")
	private OrderRequestItemService orderRequestItemService;
	 
	@Autowired
	@Qualifier("warehouseDeliveryReceiptService")
	private DeliveryReceiptService deliveryReceiptService;
	
	@GET
	@Path("/{orderNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInventoryItem(@PathParam("orderNumber") String orderNumber){
		LOGGER.info("GET - orderNumber: "+orderNumber);
		Response response = null;
		try {
			QueryResultObjectWrapper<OrderRequestQRO> orderRequestQueryResultObject = 
					orderRequestService.getOrderRequestByOrderNumber(orderNumber);
			response = ResponseWrapper.successfulResponse(orderRequestQueryResultObject);
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		}
		
		return response;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOrderRequest(@Valid @BeanParam OrderRequestLQO orderRequestListQueryObject){
		SearchList orderRequestList = orderRequestService.findAllOrderRequests(orderRequestListQueryObject);
		return ResponseWrapper.successfulResponse(orderRequestList);
	}
	
	@Path("/orderrequests")
	public AbstractHeaderResource postRequest(@HeaderParam("xAction") String action){
		AbstractHeaderResource headerBasedSubResource = null;
		
		switch(action){
			case "CALCULATE":
				headerBasedSubResource = 
					new CalculateOrdeRequestItemsRequest(orderRequestItemService);
				break;
			case "TEMPLATE":
				headerBasedSubResource = 
					new GenerateOrderRequestTemplate(orderRequestService);
				break;
			case "SUBMIT":
				headerBasedSubResource = 
					new SubmitOrderRequest(orderRequestService);
				break;
			case "APPROVE":
				headerBasedSubResource = 
					new ApproveOrderRequest(
							orderRequestService, deliveryReceiptService);
				break;
			case "REJECT":
				headerBasedSubResource = 
					new RejectOrderRequest(orderRequestService);
				break;
			default:
				headerBasedSubResource = 
					new CreateOrderRequest(orderRequestService);
		}
		
		return headerBasedSubResource;
	}
	
	@PUT
	@Path("/{orderNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateOrderRequest(OrderRequestCO orderRequestUpdateObject, @PathParam("orderNumber") String orderNumber){
		LOGGER.info("PUT - order request"+orderNumber);
		
		Response response = null;
		try {
			orderRequestService.updateOrderRequest(orderNumber, orderRequestUpdateObject);
			QueryResultObjectWrapper<OrderRequestQRO> orderRequestQueryResultObject = 
					orderRequestService.getOrderRequestByOrderNumber(orderNumber);
			response = ResponseWrapper.successfulResponse(orderRequestQueryResultObject);
		} catch (ValidationException e) {
			response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		}
		
		return response;
	}
	
	@DELETE
	@Path("/{orderNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response removeOrderRequest(@PathParam("orderNumber") String orderNumber){
		LOGGER.info("DELETE - order request"+orderNumber);
		
		Response response = null;
		try {
			orderRequestService.removeOrderRequest(orderNumber);
			response = ResponseWrapper.successfulResponse();
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		} catch (CannotBeRemovedException e) {
			response = ResponseWrapper.unprocessableEntity(e.getMessage());
		}
		
		return response;
	}
	
	
	public static abstract class AbstractHeaderResource{
	}
	
	public static class GenerateOrderRequestTemplate extends AbstractHeaderResource{
		private OrderRequestService orderRequestService;
		
		public GenerateOrderRequestTemplate(OrderRequestService orderRequestService){
			this.orderRequestService = orderRequestService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(OrderRequestCO orderRequestCommand){
			LOGGER.info("POST - TEMPLATE order request");
			
			Response response = null;
			QueryResultObjectWrapper<OrderRequestQRO> orderRequestTemplate = 
					orderRequestService.generateOrderRequestTemplate();
			response = ResponseWrapper.successfulResponse(orderRequestTemplate);
			
			return response;
		}
	}
	
	public static class CreateOrderRequest extends AbstractHeaderResource{
		
		private OrderRequestService orderRequestService;
		
		public CreateOrderRequest(OrderRequestService orderRequestService){
			this.orderRequestService = orderRequestService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(OrderRequestCO orderRequestCommand){
			LOGGER.info("POST - create order request");
			
			Response response = null;
			try {
				String orderNumber = orderRequestService.createNewOrderRequest(orderRequestCommand);
				QueryResultObjectWrapper<OrderRequestQRO> orderRequestQueryResultObject = 
						orderRequestService.getOrderRequestByOrderNumber(orderNumber);
				response = ResponseWrapper.successfulResponse(orderRequestQueryResultObject);
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			} catch (CannotBeFoundException e) {
				response = ResponseWrapper.resourceNotFound(e.getMessage());
			}
			
			return response;
		}
	}
	
	public static class CalculateOrdeRequestItemsRequest extends AbstractHeaderResource{
		
		private OrderRequestItemService orderRequestItemService;
		
		public CalculateOrdeRequestItemsRequest(OrderRequestItemService orderRequestItemService){
			this.orderRequestItemService = orderRequestItemService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(OrderRequestCO calculateCommand){
			LOGGER.info("POST - calculate order requests for OR: "+calculateCommand.getOrderNumber());
			
			Response response = null;
			try {
				List<Map<String, String>> calculatedList = orderRequestItemService.calculateDetailsOfRequestItems(calculateCommand);
				response = ResponseWrapper.successfulResponse(calculatedList);
			} catch (ValidationException e) {
				response = ResponseWrapper.badRequest(e.getAllErrors());
			}
			
			return response;
		}
	}
	
	public static class SubmitOrderRequest extends AbstractHeaderResource{
		
		private OrderRequestService orderRequestService;
		
		public SubmitOrderRequest(OrderRequestService orderRequestService){
			this.orderRequestService = orderRequestService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(OrderRequestCO orderRequestCO){
			LOGGER.info("POST - submit order request: "+orderRequestCO.getOrderNumber());
			
			Response response = null;
			try {
				orderRequestService.submitOrderRequest(orderRequestCO);
				QueryResultObjectWrapper<OrderRequestQRO> orderRequestQueryResultObject = 
						orderRequestService.getOrderRequestByOrderNumber(orderRequestCO.getOrderNumber());
				response = ResponseWrapper.successfulResponse(orderRequestQueryResultObject);
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			} catch (CannotBeFoundException e) {
				response = ResponseWrapper.resourceNotFound(e.getMessage());
			}
			
			return response;
		}
	}
	
	public static class ApproveOrderRequest extends AbstractHeaderResource{
		
		private OrderRequestService orderRequestService;
		private DeliveryReceiptService deliveryReceiptService;
		
		public ApproveOrderRequest(OrderRequestService orderRequestService, DeliveryReceiptService deliveryReceiptService){
			this.orderRequestService = orderRequestService;
			this.deliveryReceiptService = deliveryReceiptService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(OrderRequestCO orderRequestCO){
			LOGGER.info("POST - submit order request: "+orderRequestCO.getOrderNumber());
			
			Response response = null;
			try {
				orderRequestService.approveOrderRequest(orderRequestCO);
				
				deliveryReceiptService.createNewDeliveryReceipt(
						new DeliveryReceiptCO(orderRequestCO.getOrderNumber()));
				
				QueryResultObjectWrapper<OrderRequestQRO> orderRequestQueryResultObject = 
						orderRequestService.getOrderRequestByOrderNumber(orderRequestCO.getOrderNumber());
				
				response = ResponseWrapper.successfulResponse(orderRequestQueryResultObject);
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			} catch (CannotBeFoundException e) {
				response = ResponseWrapper.resourceNotFound(e.getMessage());
			}
			
			return response;
		}
	}
	
	public static class RejectOrderRequest extends AbstractHeaderResource{
		
		private OrderRequestService orderRequestService;
		
		public RejectOrderRequest(OrderRequestService orderRequestService){
			this.orderRequestService = orderRequestService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(OrderRequestCO orderRequestCO){
			LOGGER.info("POST - submit order request: "+orderRequestCO.getOrderNumber());
			
			Response response = null;
			try {
				orderRequestService.rejectOrderRequest(orderRequestCO);
				QueryResultObjectWrapper<OrderRequestQRO> orderRequestQueryResultObject = 
						orderRequestService.getOrderRequestByOrderNumber(orderRequestCO.getOrderNumber());
				response = ResponseWrapper.successfulResponse(orderRequestQueryResultObject);
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			} catch (CannotBeFoundException e) {
				response = ResponseWrapper.resourceNotFound(e.getMessage());
			}
			
			return response;
		}
	}
}
