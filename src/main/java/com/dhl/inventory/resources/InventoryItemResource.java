package com.dhl.inventory.resources;

import java.io.InputStream;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.commandobject.maintenance.InventoryItemCO;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.maintenance.InventoryItemLQO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.services.maintenance.InventoryItemService;


@Component
@Path("/items")
public class InventoryItemResource {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(InventoryItemResource.class);
	
	@Autowired
	@Qualifier("inventoryItemService")
	private InventoryItemService inventoryItemService;
	
	@GET
	@Path("/{codename}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInventoryItem(@PathParam("codename") String codename){
		LOGGER.info("GET - item: "+codename);
		Response response = null;
		try {
			response = 
				ResponseWrapper.successfulResponse(inventoryItemService.getInventoryItem(codename));
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		}
		
		return response;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInventoryItems(@BeanParam InventoryItemLQO itemListTransfer){
		SearchList searchList = null;
		LOGGER.info("itemListTransfer.getSearchType(): "+itemListTransfer.getSearchType());
		switch(itemListTransfer.getSearchType()){
			case "TYPEHEAD": 
				itemListTransfer.setRowOffset(0);
				itemListTransfer.setRows(7);
				searchList = inventoryItemService.searchInventoryItems(itemListTransfer);
				break;
			case "FILTER":
				searchList = inventoryItemService.findAllInventoryItems(itemListTransfer);
				break;
		}
		
		return ResponseWrapper.successfulResponse(searchList);
	}
	
	@POST
	@Path("/upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadMasterDataFile(@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetail){
		LOGGER.info("POST - upload master file: "+fileDetail.getFileName());
		
		/*String uploadedFileLocation = "d://uploaded/" + fileDetail.getFileName();*/
		
		/*String output = "File uploaded to : " + uploadedFileLocation;*/

		return ResponseWrapper.successfulResponse();
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response createInventoryItem(@BeanParam InventoryItemCO item){
		LOGGER.info("POST - item: "+item.getCodeName());
		
		Response response = null;
		try {
			inventoryItemService.createNewInventoryItem(item);
			InventoryItemCO updatedItem = inventoryItemService.getInventoryItem(item.getCodeName());
			response = ResponseWrapper.successfulResponse(updatedItem);
		} catch (ValidationException e) {
			response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		}
		
		return response;
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateInventoryItem(@BeanParam InventoryItemCO item){
		LOGGER.info("PUT - item: "+item.getCodeName());
		
		Response response = null;
		try {
			inventoryItemService.updateInventoryItem(item);
			InventoryItemCO updatedItem = inventoryItemService.getInventoryItem(item.getCodeName());
			response = ResponseWrapper.successfulResponse(updatedItem);
		} catch (ValidationException e) {
			response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		}
		
		return response;
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteInventoryItem(@FormParam("item") String itemCode){
		LOGGER.info("DELETE - item: "+itemCode);
		
		Response response = null;
		try {
			inventoryItemService.removeInventoryItem(itemCode);
			response = ResponseWrapper.successfulResponse();
		} catch (ValidationException e) {
			response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		}
		
		return response;
	}
}
