package com.dhl.inventory.resources;

import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.maintenance.RouteLQO;
import com.dhl.inventory.services.maintenance.RouteService;

@Component
@Path("/facilities/{facilityCodeName}/routes")
public class RouteResource {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RouteResource.class);
	
	@Autowired
	@Qualifier("routeService")
	private RouteService routeService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFacilityPersonnels(@BeanParam RouteLQO routeLQO){
		SearchList searchList = null;
		LOGGER.info("routeLQO.getFacilityCode(): "+routeLQO.getFacilityCode());
		searchList = routeService.findAllRoutes(routeLQO);
		LOGGER.info("routeLQO.getSearch size: "+searchList.getRows().size());
		return ResponseWrapper.successfulResponse(searchList);
	}
}
