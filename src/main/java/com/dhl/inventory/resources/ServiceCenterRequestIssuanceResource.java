package com.dhl.inventory.resources;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.dhl.inventory.components.FileStreamingOutputFactory;
import com.dhl.inventory.dto.commandobject.servicecenter.requestissuance.RequestIssuanceCO;
import com.dhl.inventory.dto.queryobject.QueryResultObjectWrapper;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.RequestIssuanceLQO;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.RequestIssuancePickListQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.RequestIssuanceQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.RequestIssuanceDeliveryReceiptFormQRO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.services.reports.JasperReportService;
import com.dhl.inventory.services.servicecenter.requestissuance.RequestIssuanceService;

@Component
@Path("/facilities/{facilityCodeName}/requestissuances")
public class ServiceCenterRequestIssuanceResource {
	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceCenterRequestIssuanceResource.class);
	
	@Autowired
	@Qualifier("serviceCenterRequestIssuanceService")
	private RequestIssuanceService requestIssuanceService;
	
	@Autowired
	private FileStreamingOutputFactory fileStreamingOutputFactory;
	
	@Autowired
	@Qualifier("jasperReportService")
	private JasperReportService jasperReportService;

	@GET
	@Path("/{requestNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRequestIssuance(@PathParam("requestNumber") String requestNumber){
		LOGGER.info("GET - requestNumber: "+requestNumber);
		Response response = null;
		try {
			QueryResultObjectWrapper<RequestIssuanceQRO> requestIssuanceQueryResultObject = 
					requestIssuanceService.getRequestIssuanceByRequestNumber(requestNumber);
			response = ResponseWrapper.successfulResponse(requestIssuanceQueryResultObject);
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		}

		return response;
	}
	
	@GET
	@Path("/{requestNumber}/picklistform")
	@Produces("application/pdf")
	public StreamingOutput getPicklistform(@PathParam("requestNumber") String requestNumber) {
		StreamingOutput returnValue = null;
		try {
			List<RequestIssuancePickListQRO> pickListDataList = new ArrayList<>();
			RequestIssuancePickListQRO pickListData = 
					requestIssuanceService.getRequestIssuancePickList(requestNumber);
			pickListDataList.add(pickListData);
			File pickListForm = 
					jasperReportService.generateJRPrint("SVC_REQUEST_ISSUANCE_PICK_LIST", pickListDataList);
			returnValue = fileStreamingOutputFactory.generateFileStreamingOutput(pickListForm);
		} catch (Exception e1) {
			LOGGER.error("svc ri picklistform",e1);
			if(e1 instanceof CannotBeFoundException)
				throw new WebApplicationException(e1, 404);
			else 
				throw new WebApplicationException(e1);
		}
	    return returnValue;
	}
	
	@GET
	@Path("/{requestNumber}/deliveryreceiptform")
	@Produces("application/pdf")
	public StreamingOutput getDeliveryReceiptForm(@PathParam("requestNumber") String requestNumber) {
		StreamingOutput returnValue = null;
		try {
			List<RequestIssuanceDeliveryReceiptFormQRO> deliveryReceiptFormList = new ArrayList<>();
			RequestIssuanceDeliveryReceiptFormQRO deliveryReceiptFormData = 
					requestIssuanceService.getRequestIssuanceDeliveryReceiptForm(requestNumber);
			
			deliveryReceiptFormList.add(deliveryReceiptFormData);
			File pickListForm = 
					jasperReportService.generateJRPrint("SVC_REQUEST_ISSUANCE_DELIVERY_RECEIPT", deliveryReceiptFormList);
			
			returnValue = fileStreamingOutputFactory.generateFileStreamingOutput(pickListForm);
		} catch (Exception e1) {
			if(e1 instanceof CannotBeFoundException)
				throw new WebApplicationException(e1, 404);
			else 
				throw new WebApplicationException(e1);
		}
	    return returnValue;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRequestIssuances(@Valid @BeanParam RequestIssuanceLQO requestIssuanceListQueryObject){
		SearchList requestIssuanceList = requestIssuanceService.findAllRequestIssuance(requestIssuanceListQueryObject);
		return ResponseWrapper.successfulResponse(requestIssuanceList);
	}

	@Path("/requestissuances")
	public AbstractHeaderResource postRequest(@HeaderParam("xAction") String action){
		AbstractHeaderResource headerBasedSubResource = null;

		switch(action){
		case "PROCESS":
			headerBasedSubResource = 
			new ProcessRequestIssuance(requestIssuanceService);
			break;
		case "FINALIZE":
			headerBasedSubResource = 
			new FinalizeRequestIssuance(requestIssuanceService);
			break;
		case "REJECT":
			headerBasedSubResource = 
			new RejectRequestIssuance(requestIssuanceService);
			break;
		}

		return headerBasedSubResource;
	}

	@PUT
	@Path("/{requestNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateRequestIssuance(RequestIssuanceCO requestIssuanceUpdateObject, @PathParam("requestNumber") String requestNumber){
		LOGGER.info("PUT - request issuance: "+requestNumber);

		Response response = null;
		try {
			requestIssuanceService.updateRequestIssuance(requestNumber, requestIssuanceUpdateObject);
			QueryResultObjectWrapper<RequestIssuanceQRO> requestIssuanceQueryResultObject = 
					requestIssuanceService.getRequestIssuanceByRequestNumber(requestNumber);
			response = ResponseWrapper.successfulResponse(requestIssuanceQueryResultObject);
		} catch (ValidationException e) {
			response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		}

		return response;
	}

	@DELETE
	@Path("/{requestNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response removeRequestIssuance(@PathParam("requestNumber") String requestNumber){
		LOGGER.info("DELETE - order request"+requestNumber);

		Response response = null;
		/*try {
			requestIssuanceService.removeRequestIssuance(requestNumber);
			response = ResponseWrapper.successfulResponse();
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		} catch (CannotBeRemovedException e) {
			response = ResponseWrapper.unprocessableEntity(e.getMessage());
		}
		 */
		return response;
	}


	public static abstract class AbstractHeaderResource{
	}
	

	public static class ProcessRequestIssuance extends AbstractHeaderResource{

		private RequestIssuanceService requestIssuanceService;

		public ProcessRequestIssuance(RequestIssuanceService requestIssuanceService){
			this.requestIssuanceService = requestIssuanceService;
		}

		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(RequestIssuanceCO requestIssuanceCO){
			LOGGER.info("POST - submit order request: "+requestIssuanceCO.getRequestNumber());

			Response response = null;
			try {
				requestIssuanceService.processRequestIssuance(requestIssuanceCO);
				QueryResultObjectWrapper<RequestIssuanceQRO> requestIssuanceQueryResultObject = 
						requestIssuanceService.getRequestIssuanceByRequestNumber(requestIssuanceCO.getRequestNumber());
				response = ResponseWrapper.successfulResponse(requestIssuanceQueryResultObject);
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			} catch (CannotBeFoundException e) {
				response = ResponseWrapper.resourceNotFound(e.getMessage());
			}
			

			return response;
		}
	}

	public static class FinalizeRequestIssuance extends AbstractHeaderResource{

		private RequestIssuanceService requestIssuanceService;

		public FinalizeRequestIssuance(RequestIssuanceService requestIssuanceService){
			this.requestIssuanceService = requestIssuanceService;
		}

		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(RequestIssuanceCO requestIssuanceCO){
			LOGGER.info("POST - submit order request: "+requestIssuanceCO.getRequestNumber());

			Response response = null;
			try {
				requestIssuanceService.finalizeRequestIssuance(requestIssuanceCO);

				QueryResultObjectWrapper<RequestIssuanceQRO> requestIssuanceQueryResultObject = 
						requestIssuanceService.getRequestIssuanceByRequestNumber(requestIssuanceCO.getRequestNumber());

				response = ResponseWrapper.successfulResponse(requestIssuanceQueryResultObject);
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			} catch (CannotBeFoundException e) {
				response = ResponseWrapper.resourceNotFound(e.getMessage());
			}

			return response;
		}
	}

	public static class RejectRequestIssuance extends AbstractHeaderResource{

		private RequestIssuanceService requestIssuanceService;

		public RejectRequestIssuance(RequestIssuanceService requestIssuanceService){
			this.requestIssuanceService = requestIssuanceService;
		}

		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(RequestIssuanceCO requestIssuanceCO){
			LOGGER.info("POST - submit order request: "+requestIssuanceCO.getRequestNumber());

			Response response = null;
			try {
				requestIssuanceService.rejectRequestIssuance(requestIssuanceCO);
				QueryResultObjectWrapper<RequestIssuanceQRO> requestIssuanceQueryResultObject = 
						requestIssuanceService.getRequestIssuanceByRequestNumber(requestIssuanceCO.getRequestNumber());
				response = ResponseWrapper.successfulResponse(requestIssuanceQueryResultObject);
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			} catch (CannotBeFoundException e) {
				response = ResponseWrapper.resourceNotFound(e.getMessage());
			}

			return response;
		}
	}
}
