package com.dhl.inventory.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.CalculateDeliveredItemBatchGroupResourceCO;
import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.DeliveredItemBatchGroupResourceCO;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.services.servicecenter.inventorymaintenance.DeliveredItemBatchService;

@Component
@Path("facilities/{facilityCodeName}/deliveryreceipts/{requestNumber}/itembatchgroups")
public class ServiceCenterDeliveredItemBatchResource {
private static final Logger LOGGER = LoggerFactory.getLogger(DeliveredItemBatchResource.class);
	
	@Autowired
	@Qualifier("serviceCenterDeliveredItemBatchService")
	private DeliveredItemBatchService deliveredItemBatchService;
	
	@Path("/itembatchgroups")
	public AbstractHeaderResource postRequest(@HeaderParam("xAction") String action){
		if ("VALIDATE".equals(action)) {
			return new ValidateDeliveredItemBatchGroupRequest(deliveredItemBatchService);
        } else {
        	return new RecalculateDeliveredItemBatchGroupValues(deliveredItemBatchService);
        }
	}
	
	public static abstract class AbstractHeaderResource{
	}
	
	public static class ValidateDeliveredItemBatchGroupRequest extends AbstractHeaderResource{
		
		private DeliveredItemBatchService orderRequestItemService;
		
		public ValidateDeliveredItemBatchGroupRequest(DeliveredItemBatchService orderRequestItemService){
			this.orderRequestItemService = orderRequestItemService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(
				DeliveredItemBatchGroupResourceCO dRItemBatchGroupCommand, 
				@PathParam("requestNumber") String requestNumber){
			LOGGER.info("POST - validate delivered item batch group: "+dRItemBatchGroupCommand.getItemCode());
			
			Response response = null;
			try {
				orderRequestItemService.validateDeliveredItemBatch(dRItemBatchGroupCommand, requestNumber);
				response = ResponseWrapper.successfulResponse();
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			}
			
			return response;
		}
	}
	
	public static class RecalculateDeliveredItemBatchGroupValues extends AbstractHeaderResource{
		
		private DeliveredItemBatchService orderRequestItemService;
		
		public RecalculateDeliveredItemBatchGroupValues(DeliveredItemBatchService orderRequestItemService){
			this.orderRequestItemService = orderRequestItemService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(
				CalculateDeliveredItemBatchGroupResourceCO dRItemBatchGroupCommand, @PathParam("requestNumber") String requestNumber){
			LOGGER.info("POST - calculate item batch group for itemCode: "
						+dRItemBatchGroupCommand.getItemCode());
			
			Response response = ResponseWrapper.successfulResponse(
					orderRequestItemService.calculateDetailsOfDeliveredItemBatches(
							dRItemBatchGroupCommand, requestNumber));
			
			return response;
		}
	}
}
