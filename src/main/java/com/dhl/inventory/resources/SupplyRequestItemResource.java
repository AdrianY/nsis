package com.dhl.inventory.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest.SupplyRequestItemResourceCO;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.services.servicecenter.supplyrequest.SupplyRequestItemService;

@Component
@Path("/supplyrequests/{requestNumber}/items")
public class SupplyRequestItemResource {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SupplyRequestItemResource.class);
	
	@Autowired
	@Qualifier("supplyRequestItemService")
	private SupplyRequestItemService supplyRequestItemService;
	
	@Path("/items")
	public AbstractHeaderResource postRequest(@HeaderParam("xAction") String action){
		if ("VALIDATE".equals(action)) {
			return new ValidateOrdeRequestItemRequest(supplyRequestItemService);
        } else {
        	return new ValidateOrdeRequestItemRequest(supplyRequestItemService);
        }
	}
	
	public static abstract class AbstractHeaderResource{
	}
	
	public static class ValidateOrdeRequestItemRequest extends AbstractHeaderResource{
		
		private SupplyRequestItemService supplyRequestItemService;
		
		public ValidateOrdeRequestItemRequest(SupplyRequestItemService supplyRequestItemService){
			this.supplyRequestItemService = supplyRequestItemService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(SupplyRequestItemResourceCO oRItemCommand){
			LOGGER.info("POST - validate order request item for OR Item: "+oRItemCommand.getFacilityCodeName()+"-"+oRItemCommand.getDetails().getItemCode());
			
			Response response = null;
			try {
				supplyRequestItemService.validateSupplyRequestItem(oRItemCommand);
				response = ResponseWrapper.successfulResponse();
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			}
			
			return response;
		}
	}
}
