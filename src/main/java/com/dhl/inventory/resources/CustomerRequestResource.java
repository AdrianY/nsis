package com.dhl.inventory.resources;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.commandobject.customerinterfaces.CustomerRequestCO;
import com.dhl.inventory.dto.commandobject.servicecenter.requestissuance.RequestIssuanceCO;
import com.dhl.inventory.dto.queryobject.QueryResultObjectWrapper;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.customerinterfaces.CustomerRequestLQO;
import com.dhl.inventory.dto.queryobject.customerinterfaces.CustomerRequestQRO;
import com.dhl.inventory.services.customerinterfaces.CustomerRequestService;
import com.dhl.inventory.services.customerinterfaces.CustomerRequestedItemResourceService;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.CannotBeRemovedException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.services.servicecenter.requestissuance.RequestIssuanceService;

@Component
@Path("customerrequests")
public class CustomerRequestResource {
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerRequestResource.class);
	
	@Autowired
	@Qualifier("customerRequestService")
	private CustomerRequestService customerRequestService;
	
	@Autowired
	@Qualifier("customerRequestedItemResourceService")
	private CustomerRequestedItemResourceService customerRequestItemService;
	
	@Autowired
	@Qualifier("serviceCenterRequestIssuanceService")
	private RequestIssuanceService requestIssuanceService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomerRequests(@Valid @BeanParam CustomerRequestLQO customerRequestListQueryObject){
		SearchList customerRequestList = customerRequestService.findAllCustomerRequests(customerRequestListQueryObject);
		return ResponseWrapper.successfulResponse(customerRequestList);
	}
	
	@GET
	@Path("/{requestNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomerRequest(@PathParam("requestNumber") String requestNumber){
		LOGGER.info("GET - requestNumber: "+requestNumber);
		Response response = null;
		try {
			QueryResultObjectWrapper<CustomerRequestQRO> customerRequestQueryResultObject = 
					customerRequestService.getCustomerRequestByRequestNumber(requestNumber);
			response = ResponseWrapper.successfulResponse(customerRequestQueryResultObject);
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		}
		
		return response;
	}
	
	@PUT
	@Path("/{requestNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateCustomerRequest(CustomerRequestCO customerRequestUpdateObject, @PathParam("requestNumber") String requestNumber){
		LOGGER.info("PUT - customer request: "+requestNumber);
		
		Response response = null;
		try {
			customerRequestService.updateCustomerRequest(requestNumber, customerRequestUpdateObject);
			QueryResultObjectWrapper<CustomerRequestQRO> customerRequestQueryResultObject = 
					customerRequestService.getCustomerRequestByRequestNumber(requestNumber);
			response = ResponseWrapper.successfulResponse(customerRequestQueryResultObject);
		} catch (ValidationException e) {
			response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		}
		
		return response;
	}
	
	@DELETE
	@Path("/{requestNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response removeCustomerRequest(@PathParam("requestNumber") String requestNumber){
		LOGGER.info("DELETE - order request"+requestNumber);
		
		Response response = null;
		try {
			customerRequestService.removeCustomerRequest(requestNumber);
			response = ResponseWrapper.successfulResponse();
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		} catch (CannotBeRemovedException e) {
			response = ResponseWrapper.unprocessableEntity(e.getMessage());
		}
		
		return response;
	}
	
	@Path("/customerrequests")
	public AbstractHeaderResource postRequest(@HeaderParam("xAction") String action){
		AbstractHeaderResource headerBasedSubResource = null;
		
		switch(action){
			case "TEMPLATE":
				headerBasedSubResource = 
					new GenerateCustomerRequestTemplate(customerRequestService);
				break;
			case "SUBMIT":
				headerBasedSubResource = 
					new SubmitCustomerRequest(requestIssuanceService, customerRequestService);
				break;
			case "CALCULATE":
				headerBasedSubResource = 
					new CompleteRequestedItemsDetails(customerRequestItemService);
				break;
			default:
				headerBasedSubResource = 
					new CreateCustomerRequest(customerRequestService);
		}
		
		return headerBasedSubResource;
	}
	
	public static abstract class AbstractHeaderResource{
	}
	
	public static class CompleteRequestedItemsDetails extends AbstractHeaderResource{
		
		private CustomerRequestedItemResourceService customerRequestItemService;
		
		public CompleteRequestedItemsDetails(CustomerRequestedItemResourceService customerRequestItemService){
			this.customerRequestItemService = customerRequestItemService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(CustomerRequestCO commandObject){
			LOGGER.info("POST - complete details of requested items for RN: "+commandObject.getRequestNumber());
			
			Response response = null;
			try {
				List<Map<String, String>> calculatedList = customerRequestItemService.calculateDetailsOfRequestedItems(commandObject);
				response = ResponseWrapper.successfulResponse(calculatedList);
			} catch (ValidationException e) {
				response = ResponseWrapper.badRequest(e.getAllErrors());
			}
			
			return response;
		}
	}
	
	public static class GenerateCustomerRequestTemplate extends AbstractHeaderResource{
		private CustomerRequestService customerRequestService;
		
		public GenerateCustomerRequestTemplate(CustomerRequestService customerRequestService){
			this.customerRequestService = customerRequestService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(){
			LOGGER.info("POST - TEMPLATE customer request");
			
			Response response = null;
			QueryResultObjectWrapper<CustomerRequestQRO> customerRequestTemplate = 
					customerRequestService.generateCustomerRequestTemplate();
			response = ResponseWrapper.successfulResponse(customerRequestTemplate);
			
			return response;
		}
	}
	
	public static class SubmitCustomerRequest extends AbstractHeaderResource{
		
		private CustomerRequestService customerRequestService;
		private RequestIssuanceService requestIssuanceService;
		
		public SubmitCustomerRequest(
				final RequestIssuanceService requestIssuanceService, 
				final CustomerRequestService customerRequestService
		){
			this.requestIssuanceService = requestIssuanceService;
			this.customerRequestService = customerRequestService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(CustomerRequestCO customerRequestCO){
			LOGGER.info("POST - submit customer request: "+customerRequestCO.getRequestNumber());
			
			Response response = null;
			try {
				customerRequestService.submitCustomerRequest(customerRequestCO);
				requestIssuanceService.createNewRequestIssuance(new RequestIssuanceCO(customerRequestCO.getRequestNumber()));
				QueryResultObjectWrapper<CustomerRequestQRO> customerRequestQueryResultObject = 
						customerRequestService.getCustomerRequestByRequestNumber(customerRequestCO.getRequestNumber());
				response = ResponseWrapper.successfulResponse(customerRequestQueryResultObject);
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			} catch (CannotBeFoundException e) {
				response = ResponseWrapper.resourceNotFound(e.getMessage());
			}
			
			return response;
		}
	}
	
	public static class CreateCustomerRequest extends AbstractHeaderResource{
		
		private CustomerRequestService customerRequestService;
		
		public CreateCustomerRequest(CustomerRequestService customerRequestService){
			this.customerRequestService = customerRequestService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(CustomerRequestCO customerRequestCommand){
			LOGGER.info("POST - create customer request");
			
			Response response = null;
			try {
				String requestNumber = customerRequestService.createNewCustomerRequest(customerRequestCommand);
				QueryResultObjectWrapper<CustomerRequestQRO> customerRequestQueryResultObject = 
						customerRequestService.getCustomerRequestByRequestNumber(requestNumber);
				response = ResponseWrapper.successfulResponse(customerRequestQueryResultObject);
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			} catch (CannotBeFoundException e) {
				response = ResponseWrapper.resourceNotFound(e.getMessage());
			}
			
			return response;
		}
	}
}
