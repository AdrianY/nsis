package com.dhl.inventory.resources;

import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.PhysicalStockCountRequestCO;
import com.dhl.inventory.dto.queryobject.QueryResultObjectWrapper;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.PhysicalStockCountRequestLQO;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.PhysicalStockCountRequestQRO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.services.servicecenter.inventorymaintenance.PhysicalStockCountRequestService;

@Component
@Path("facilities/{facilityCodeName}/physicalstockcountrequest")
public class PhysicalStockCountRequestResource {
	private static final Logger LOGGER = LoggerFactory.getLogger(PhysicalStockCountRequestResource.class);
	
	@Autowired
	@Qualifier("serviceCenterPhysicalStockCountRequestService")
	private PhysicalStockCountRequestService physicalStockCountRequestService;
	
	@GET
	@Path("/{requestNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPhysicalStockCountRequest(@PathParam("requestNumber") String requestNumber){
		LOGGER.info("GET - requestNumber: "+requestNumber);
		Response response = null;
		try {
			QueryResultObjectWrapper<PhysicalStockCountRequestQRO> queryResultObject = 
					physicalStockCountRequestService.getPhysicalStockCountRequest(requestNumber);
			response = ResponseWrapper.successfulResponse(queryResultObject);
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		}

		return response;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPhysicalStockCountRequest(@Valid @BeanParam PhysicalStockCountRequestLQO listQueryObject){
		SearchList requestIssuanceList = 
				physicalStockCountRequestService.findAllPhysicalStockCountRequest(listQueryObject);
		return ResponseWrapper.successfulResponse(requestIssuanceList);
	}
	
	@PUT
	@Path("/{requestNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updatePhysicalStockCountRequest(PhysicalStockCountRequestCO requestIssuanceUpdateObject, @PathParam("requestNumber") String requestNumber){
		LOGGER.info("PUT - request issuance: "+requestNumber);

		Response response = null;
		try {
			physicalStockCountRequestService.updatePhysicalStockCountRequest(requestNumber, requestIssuanceUpdateObject);
			QueryResultObjectWrapper<PhysicalStockCountRequestQRO> queryResultObject = 
					physicalStockCountRequestService.getPhysicalStockCountRequest(requestNumber);
			response = ResponseWrapper.successfulResponse(queryResultObject);
		} catch (ValidationException e) {
			response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		}

		return response;
	}

	@DELETE
	@Path("/{requestNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response removePhysicalStockCountRequest(@PathParam("requestNumber") String requestNumber){
		LOGGER.info("DELETE - order request"+requestNumber);

		Response response = null;
		/*try {
			requestIssuanceService.removePhysicalStockCountRequest(requestNumber);
			response = ResponseWrapper.successfulResponse();
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		} catch (CannotBeRemovedException e) {
			response = ResponseWrapper.unprocessableEntity(e.getMessage());
		}
		 */
		return response;
	}
	
	@Path("/physicalstockcountrequest")
	public AbstractHeaderResource postRequest(@HeaderParam("xAction") String action){
		AbstractHeaderResource headerBasedSubResource = null;
		
		switch(action){
			case "CALCULATE":
				/*headerBasedSubResource = 
					new CalculateSupplyRequestItemsRequest(physicalStockCountRequestItemService);*/
				break;
			case "TEMPLATE":
				headerBasedSubResource = 
					new GeneratePhysicalStockCountRequestTemplate(physicalStockCountRequestService);
				break;
			case "SUBMIT":
				headerBasedSubResource = 
					new SubmitPhysicalStockCountRequest(physicalStockCountRequestService);
				break;
			case "APPROVE":
				headerBasedSubResource = 
					new ApprovePhysicalStockCountRequest(physicalStockCountRequestService);
				break;
			case "REJECT":
				headerBasedSubResource = 
					new RejectPhysicalStockCountRequest(physicalStockCountRequestService);
				break;
			default:
				headerBasedSubResource = 
					new CreatePhysicalStockCountRequest(physicalStockCountRequestService);
		}
		
		return headerBasedSubResource;
	}
	
	public static abstract class AbstractHeaderResource{
	}
	
	public static class GeneratePhysicalStockCountRequestTemplate extends AbstractHeaderResource{
		private PhysicalStockCountRequestService physicalStockCountRequestService;
		
		public GeneratePhysicalStockCountRequestTemplate(PhysicalStockCountRequestService physicalStockCountRequestService){
			this.physicalStockCountRequestService = physicalStockCountRequestService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(PhysicalStockCountRequestCO physicalStockCountRequestCommand, @PathParam("facilityCodeName")String facilityCodeName){
			LOGGER.info("POST - TEMPLATE order request");
			
			Response response = null;
			QueryResultObjectWrapper<PhysicalStockCountRequestQRO> physicalStockCountRequestTemplate = 
					physicalStockCountRequestService.generatePhysicalStockCountRequestTemplate(facilityCodeName);
			response = ResponseWrapper.successfulResponse(physicalStockCountRequestTemplate);
			
			return response;
		}
	}

	public static class CreatePhysicalStockCountRequest extends AbstractHeaderResource{

		private PhysicalStockCountRequestService physicalStockCountRequestService;

		public CreatePhysicalStockCountRequest(PhysicalStockCountRequestService physicalStockCountRequestService){
			this.physicalStockCountRequestService = physicalStockCountRequestService;
		}

		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(
				PhysicalStockCountRequestCO physicalStockCountRequestCO, 
				@PathParam("facilityCodeName") String facilityCodeName
		){
			LOGGER.info("POST - create physical stock count request: "+physicalStockCountRequestCO.getRequestNumber());

			Response response = null;
			try {
				physicalStockCountRequestService.createPhysicalStockCountRequest(
						physicalStockCountRequestCO, facilityCodeName);
				QueryResultObjectWrapper<PhysicalStockCountRequestQRO> queryResultObject = 
						physicalStockCountRequestService.getPhysicalStockCountRequest(physicalStockCountRequestCO.getRequestNumber());
				response = ResponseWrapper.successfulResponse(queryResultObject);
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			} catch (CannotBeFoundException e) {
				response = ResponseWrapper.resourceNotFound(e.getMessage());
			}
			

			return response;
		}
	}
	
	public static class SubmitPhysicalStockCountRequest extends AbstractHeaderResource{

		private PhysicalStockCountRequestService physicalStockCountRequestService;

		public SubmitPhysicalStockCountRequest(PhysicalStockCountRequestService physicalStockCountRequestService){
			this.physicalStockCountRequestService = physicalStockCountRequestService;
		}

		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(PhysicalStockCountRequestCO physicalStockCountRequestCO){
			LOGGER.info("POST - submit physical stock count request: "+physicalStockCountRequestCO.getRequestNumber());

			Response response = null;
			try {
				physicalStockCountRequestService.submitPhysicalStockCountRequest(
						physicalStockCountRequestCO);

				QueryResultObjectWrapper<PhysicalStockCountRequestQRO> queryResultObject = 
						physicalStockCountRequestService.getPhysicalStockCountRequest(
								physicalStockCountRequestCO.getRequestNumber());

				response = ResponseWrapper.successfulResponse(queryResultObject);
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			} catch (CannotBeFoundException e) {
				response = ResponseWrapper.resourceNotFound(e.getMessage());
			}

			return response;
		}
	}

	public static class ApprovePhysicalStockCountRequest extends AbstractHeaderResource{

		private PhysicalStockCountRequestService physicalStockCountRequestService;

		public ApprovePhysicalStockCountRequest(PhysicalStockCountRequestService physicalStockCountRequestService){
			this.physicalStockCountRequestService = physicalStockCountRequestService;
		}

		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(PhysicalStockCountRequestCO physicalStockCountRequestCO){
			LOGGER.info("POST - approve physical stock count request: "+physicalStockCountRequestCO.getRequestNumber());

			Response response = null;
			try {
				physicalStockCountRequestService.approvePhysicalStockCountRequest(physicalStockCountRequestCO);

				QueryResultObjectWrapper<PhysicalStockCountRequestQRO> queryResultObject = 
						physicalStockCountRequestService.getPhysicalStockCountRequest(
								physicalStockCountRequestCO.getRequestNumber());

				response = ResponseWrapper.successfulResponse(queryResultObject);
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			} catch (CannotBeFoundException e) {
				response = ResponseWrapper.resourceNotFound(e.getMessage());
			}

			return response;
		}
	}

	public static class RejectPhysicalStockCountRequest extends AbstractHeaderResource{

		private PhysicalStockCountRequestService physicalStockCountRequestService;

		public RejectPhysicalStockCountRequest(PhysicalStockCountRequestService physicalStockCountRequestService){
			this.physicalStockCountRequestService = physicalStockCountRequestService;
		}

		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(String requestNumber){
			LOGGER.info("POST - create physical stock count request: "+requestNumber);

			Response response = null;
			try {
				physicalStockCountRequestService.rejectPhysicalStockCountRequest(requestNumber);
				QueryResultObjectWrapper<PhysicalStockCountRequestQRO> queryResultObject = 
						physicalStockCountRequestService.getPhysicalStockCountRequest(requestNumber);
				response = ResponseWrapper.successfulResponse(queryResultObject);
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			} catch (CannotBeFoundException e) {
				response = ResponseWrapper.resourceNotFound(e.getMessage());
			}

			return response;
		}
	}

}
