package com.dhl.inventory.resources;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.DeliveryReceiptCO;
import com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest.SupplyRequestCO;
import com.dhl.inventory.dto.commandobject.warehouse.requestissuance.RequestIssuanceCO;
import com.dhl.inventory.dto.queryobject.QueryResultObjectWrapper;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.servicecenter.supplyrequest.SupplyRequestLQO;
import com.dhl.inventory.dto.queryobject.servicecenter.supplyrequest.SupplyRequestQRO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.CannotBeRemovedException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.services.servicecenter.inventorymaintenance.DeliveryReceiptService;
import com.dhl.inventory.services.servicecenter.supplyrequest.SupplyRequestItemService;
import com.dhl.inventory.services.servicecenter.supplyrequest.SupplyRequestService;
import com.dhl.inventory.services.warehouse.requestissuance.RequestIssuanceService;

@Component
@Path("/facilities/{facilityCodeName}/supplyrequests")
public class SupplyRequestResource {
	private static final Logger LOGGER = LoggerFactory.getLogger(SupplyRequestResource.class);
	
	@Autowired
	@Qualifier("supplyRequestService")
	private SupplyRequestService supplyRequestService;
	
	@Autowired
	@Qualifier("supplyRequestItemService")
	private SupplyRequestItemService supplyRequestItemService;
	 
	@Autowired
	@Qualifier("serviceCenterDeliveryReceiptService")
	private DeliveryReceiptService deliveryReceiptService;
	
	@Autowired
	@Qualifier("issuanceForRequestToWarehouseService")
	private RequestIssuanceService requestIssuanceService;
	
	@GET
	@Path("/{requestNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInventoryItem(@PathParam("requestNumber") String requestNumber){
		LOGGER.info("GET - requestNumber: "+requestNumber);
		Response response = null;
		try {
			QueryResultObjectWrapper<SupplyRequestQRO> supplyRequestQueryResultObject = 
					supplyRequestService.getSupplyRequestByRequestNumber(requestNumber);
			response = ResponseWrapper.successfulResponse(supplyRequestQueryResultObject);
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		}
		
		return response;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSupplyRequest(@Valid @BeanParam SupplyRequestLQO supplyRequestListQueryObject){
		SearchList orderRequestList = supplyRequestService.findAllSupplyRequests(supplyRequestListQueryObject);
		return ResponseWrapper.successfulResponse(orderRequestList);
	}
	
	@Path("/supplyrequests")
	public AbstractHeaderResource postRequest(@HeaderParam("xAction") String action){
		AbstractHeaderResource headerBasedSubResource = null;
		
		switch(action){
			case "CALCULATE":
				headerBasedSubResource = 
					new CalculateSupplyRequestItemsRequest(supplyRequestItemService);
				break;
			case "TEMPLATE":
				headerBasedSubResource = 
					new GenerateSupplyRequestTemplate(supplyRequestService);
				break;
			case "SUBMIT":
				headerBasedSubResource = 
					new SubmitSupplyRequest(supplyRequestService);
				break;
			case "APPROVE":
				headerBasedSubResource = 
					new ApproveSupplyRequest(supplyRequestService, deliveryReceiptService, requestIssuanceService);
				break;
			case "REJECT":
				headerBasedSubResource = 
					new RejectSupplyRequest(supplyRequestService);
				break;
			default:
				headerBasedSubResource = 
					new CreateSupplyRequest(supplyRequestService);
		}
		
		return headerBasedSubResource;
	}
	
	@PUT
	@Path("/{requestNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateSupplyRequest(SupplyRequestCO supplyRequestUpdateObject, @PathParam("requestNumber") String requestNumber){
		LOGGER.info("PUT - order request"+requestNumber);
		
		Response response = null;
		try {
			supplyRequestService.updateSupplyRequest(requestNumber, supplyRequestUpdateObject);
			QueryResultObjectWrapper<SupplyRequestQRO> supplyRequestQueryResultObject = 
					supplyRequestService.getSupplyRequestByRequestNumber(requestNumber);
			response = ResponseWrapper.successfulResponse(supplyRequestQueryResultObject);
		} catch (ValidationException e) {
			response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		}
		
		return response;
	}
	
	@DELETE
	@Path("/{requestNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response removeSupplyRequest(@PathParam("requestNumber") String requestNumber){
		LOGGER.info("DELETE - order request"+requestNumber);
		
		Response response = null;
		try {
			supplyRequestService.removeSupplyRequest(requestNumber);
			response = ResponseWrapper.successfulResponse();
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		} catch (CannotBeRemovedException e) {
			response = ResponseWrapper.unprocessableEntity(e.getMessage());
		}
		
		return response;
	}
	
	
	public static abstract class AbstractHeaderResource{
	}
	
	public static class GenerateSupplyRequestTemplate extends AbstractHeaderResource{
		private SupplyRequestService supplyRequestService;
		
		public GenerateSupplyRequestTemplate(SupplyRequestService supplyRequestService){
			this.supplyRequestService = supplyRequestService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(SupplyRequestCO supplyRequestCommand, @PathParam("facilityCodeName")String facilityCodeName){
			LOGGER.info("POST - TEMPLATE order request");
			
			Response response = null;
			QueryResultObjectWrapper<SupplyRequestQRO> supplyRequestTemplate = 
					supplyRequestService.generateSupplyRequestTemplate(facilityCodeName);
			response = ResponseWrapper.successfulResponse(supplyRequestTemplate);
			
			return response;
		}
	}
	
	public static class CreateSupplyRequest extends AbstractHeaderResource{
		
		private SupplyRequestService supplyRequestService;
		
		public CreateSupplyRequest(SupplyRequestService supplyRequestService){
			this.supplyRequestService = supplyRequestService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(SupplyRequestCO supplyRequestCommand){
			LOGGER.info("POST - create order request");
			
			Response response = null;
			try {
				String requestNumber = supplyRequestService.createNewSupplyRequest(supplyRequestCommand);
				QueryResultObjectWrapper<SupplyRequestQRO> supplyRequestQueryResultObject = 
						supplyRequestService.getSupplyRequestByRequestNumber(requestNumber);
				response = ResponseWrapper.successfulResponse(supplyRequestQueryResultObject);
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			} catch (CannotBeFoundException e) {
				response = ResponseWrapper.resourceNotFound(e.getMessage());
			}
			
			return response;
		}
	}
	
	public static class CalculateSupplyRequestItemsRequest extends AbstractHeaderResource{

		private SupplyRequestItemService supplyRequestItemService;
		
		public CalculateSupplyRequestItemsRequest(SupplyRequestItemService supplyRequestItemService){
			this.supplyRequestItemService = supplyRequestItemService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(SupplyRequestCO calculateCommand){
			LOGGER.info("POST - calculate items requested for supply request: "+calculateCommand.getRequestNumber());
			
			Response response = null;
			try {
				List<Map<String, String>> calculatedList = supplyRequestItemService.calculateDetailsOfRequestItems(calculateCommand);
				response = ResponseWrapper.successfulResponse(calculatedList);
			} catch (ValidationException e) {
				response = ResponseWrapper.badRequest(e.getAllErrors());
			}
			
			return response;
		}
	}
	
	public static class SubmitSupplyRequest extends AbstractHeaderResource{
		
		private SupplyRequestService supplyRequestService;
		
		public SubmitSupplyRequest(SupplyRequestService supplyRequestService){
			this.supplyRequestService = supplyRequestService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(SupplyRequestCO supplyRequestCO){
			LOGGER.info("POST - submit supply request: "+supplyRequestCO.getRequestNumber());
			
			Response response = null;
			try {
				supplyRequestService.submitSupplyRequest(supplyRequestCO);
				QueryResultObjectWrapper<SupplyRequestQRO> supplyRequestQueryResultObject = 
						supplyRequestService.getSupplyRequestByRequestNumber(supplyRequestCO.getRequestNumber());
				response = ResponseWrapper.successfulResponse(supplyRequestQueryResultObject);
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			} catch (CannotBeFoundException e) {
				response = ResponseWrapper.resourceNotFound(e.getMessage());
			}
			
			return response;
		}
	}
	
	public static class ApproveSupplyRequest extends AbstractHeaderResource{
		
		private SupplyRequestService supplyRequestService;
		private DeliveryReceiptService deliveryReceiptService;
		private RequestIssuanceService requestIssuanceService;
		
		public ApproveSupplyRequest(SupplyRequestService supplyRequestService, DeliveryReceiptService deliveryReceiptService, RequestIssuanceService requestIssuanceService){
			this.supplyRequestService = supplyRequestService;
			this.deliveryReceiptService = deliveryReceiptService;
			this.requestIssuanceService = requestIssuanceService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(SupplyRequestCO supplyRequestCO){
			LOGGER.info("POST - submit order request: "+supplyRequestCO.getRequestNumber());
			
			Response response = null;
			try {
				supplyRequestService.approveSupplyRequest(supplyRequestCO);
				
				deliveryReceiptService.createNewDeliveryReceipt(
						new DeliveryReceiptCO(supplyRequestCO.getRequestNumber()));
				
				/**
				 * This should be a web service call
				 */
				requestIssuanceService.createNewRequestIssuance(
						new RequestIssuanceCO(supplyRequestCO.getRequestNumber()));
				
				QueryResultObjectWrapper<SupplyRequestQRO> supplyRequestQueryResultObject = 
						supplyRequestService.getSupplyRequestByRequestNumber(supplyRequestCO.getRequestNumber());
				
				response = ResponseWrapper.successfulResponse(supplyRequestQueryResultObject);
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			} catch (CannotBeFoundException e) {
				response = ResponseWrapper.resourceNotFound(e.getMessage());
			}
			
			return response;
		}
	}
	
	public static class RejectSupplyRequest extends AbstractHeaderResource{
		
		private SupplyRequestService supplyRequestService;
		
		public RejectSupplyRequest(SupplyRequestService supplyRequestService){
			this.supplyRequestService = supplyRequestService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(SupplyRequestCO supplyRequestCO){
			LOGGER.info("POST - submit order request: "+supplyRequestCO.getRequestNumber());
			
			Response response = null;
			try {
				supplyRequestService.rejectSupplyRequest(supplyRequestCO);
				QueryResultObjectWrapper<SupplyRequestQRO> supplyRequestQueryResultObject = 
						supplyRequestService.getSupplyRequestByRequestNumber(supplyRequestCO.getRequestNumber());
				response = ResponseWrapper.successfulResponse(supplyRequestQueryResultObject);
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			} catch (CannotBeFoundException e) {
				response = ResponseWrapper.resourceNotFound(e.getMessage());
			}
			
			return response;
		}
	}
}
