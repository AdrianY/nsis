package com.dhl.inventory.resources;

import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.ServiceCenterOnHandByItemCodeLQO;
import com.dhl.inventory.dto.queryobject.warehouse.inventorymaintenance.OnHandByItemCodeLQO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.maintenance.FacilityService;
import com.dhl.inventory.services.servicecenter.inventorymaintenance.ServiceCenterOnHandSuppliesService;
import com.dhl.inventory.services.warehouse.inventorymaintenance.OnHandSuppliesService;

@Component
@Path("/facilities/{facilityCodeName}/onhandsupplyitems")
public class OnhandSupplyItemsResource {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(OnhandSupplyItemsResource.class);
	
	@Autowired
	@Qualifier("facilityService")
	private FacilityService facilityService;
	
	@Autowired
	@Qualifier("warehouseOnHandSuppliesService")
	private OnHandSuppliesService whOnHandSuppliesService;
	
	@Autowired
	@Qualifier("serviceCenterOnHandSuppliesService")
	private ServiceCenterOnHandSuppliesService svcOnHandSuppliesService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response postRequest(@BeanParam OnHandByItemCodeLQO listQueryObject){
		LOGGER.info("GET onhandsupplyitems for facility: "+listQueryObject.getFacilityCode());
		Response response = null;
		try {
			String facilityType = facilityService.getFacilityType(listQueryObject.getFacilityCode());
			switch(facilityType){
				case "WH":
					response = 
						ResponseWrapper.successfulResponse(
								whOnHandSuppliesService.findAllOnHandSupplies(listQueryObject));
					break;
				case "SVC":
					ServiceCenterOnHandByItemCodeLQO lqo = convertToServiceCenterLQO(listQueryObject);
					response = 
						ResponseWrapper.successfulResponse(
								svcOnHandSuppliesService.findAllOnHandSupplies(lqo));
					break;
			}
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		}
		
		return response;
	}
	
	private ServiceCenterOnHandByItemCodeLQO convertToServiceCenterLQO(OnHandByItemCodeLQO defaultLQO){
		ServiceCenterOnHandByItemCodeLQO lqo = new ServiceCenterOnHandByItemCodeLQO();
		lqo.setByItemCode(defaultLQO.isByItemCode());
		lqo.setFacilityCode(defaultLQO.getFacilityCode());
		lqo.setItemCode(defaultLQO.getItemCode());
		lqo.setRowOffset(defaultLQO.getRowOffset());
		lqo.setRows(defaultLQO.getRows());
		lqo.setSord(defaultLQO.getSortOrder());
		lqo.setSidx(defaultLQO.getSidx());
		lqo.setSearchType(defaultLQO.getSearchType());
		
		return lqo;
	}
}
