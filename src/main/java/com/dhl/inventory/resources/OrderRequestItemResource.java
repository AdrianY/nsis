package com.dhl.inventory.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.commandobject.warehouse.orderrequest.OrderRequestItemResourceCO;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.services.warehouse.orderrequest.OrderRequestItemService;

@Component
@Path("/orderrequests/{orderNumber}/items")
public class OrderRequestItemResource {
	private static final Logger LOGGER = LoggerFactory.getLogger(OrderRequestItemResource.class);
	
	@Autowired
	@Qualifier("orderRequestItemService")
	private OrderRequestItemService orderRequestItemService;
	
	@Path("/items")
	public AbstractHeaderResource postRequest(@HeaderParam("xAction") String action){
		if ("VALIDATE".equals(action)) {
			return new ValidateOrdeRequestItemRequest(orderRequestItemService);
        } else {
        	return new ValidateOrdeRequestItemRequest(orderRequestItemService);
        }
	}
	
	public static abstract class AbstractHeaderResource{
	}
	
	public static class ValidateOrdeRequestItemRequest extends AbstractHeaderResource{
		
		private OrderRequestItemService orderRequestItemService;
		
		public ValidateOrdeRequestItemRequest(OrderRequestItemService orderRequestItemService){
			this.orderRequestItemService = orderRequestItemService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(OrderRequestItemResourceCO oRItemCommand){
			LOGGER.info("POST - validate order request item for OR Item: "+oRItemCommand.getFacilityId()+"-"+oRItemCommand.getDetails().getItemCode());
			
			Response response = null;
			try {
				orderRequestItemService.validateOrderRequestItem(oRItemCommand);
				response = ResponseWrapper.successfulResponse();
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			}
			
			return response;
		}
	}
}
