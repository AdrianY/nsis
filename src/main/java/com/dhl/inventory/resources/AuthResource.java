package com.dhl.inventory.resources;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import com.dhl.inventory.conf.security.TokenUtils;
import com.dhl.inventory.dto.commandobject.security.NsisUserQRO;
import com.dhl.inventory.dto.commandobject.security.TokenTransfer;
import com.dhl.inventory.services.security.NSISUserService;

@Component
@Path("auth")
public class AuthResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthResource.class);

	private NSISUserService nsisUserService;
	
	private UserDetailsService userService;

	private AuthenticationManager authManager;
	
	@Autowired
	public AuthResource(
			@Qualifier("nsisUserService") final NSISUserService nsisUserService,
			@Qualifier("userDetailsService") final UserDetailsService userService,
			@Qualifier("authenticationManager") final AuthenticationManager authManager
	){
		this.nsisUserService = nsisUserService;
		this.userService = userService;
		this.authManager = authManager;
	}


	/**
	 * Retrieves the currently logged in user.
	 * 
	 * @return A transfer containing the username and the roles.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public NsisUserQRO getUser()
	{
		LOGGER.info("GET: auth");
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		
		if (null == authentication){
			throw new WebApplicationException(401);
		}
		
		Object principal = authentication.getPrincipal();
		
		if (principal instanceof String && ((String) principal).equals("anonymousUser")) {
			throw new WebApplicationException(401);
		}
		UserDetails userDetails = (UserDetails) principal;
		
		return nsisUserService.findDTOByUsername(userDetails.getUsername());
	}


	/**
	 * Authenticates a user and creates an authentication token.
	 * 
	 * @param username
	 *            The name of the user.
	 * @param password
	 *            The password of the user.
	 * @return A transfer containing the authentication token.
	 */
	@Path("/authenticate")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response authenticate(@FormParam("username") String username, @FormParam("password") String password)
	{
		LOGGER.info("authenticate");
		LOGGER.info("user: "+username);
		LOGGER.info("POST: authenticate");

		Response response = null;
		try{
			UsernamePasswordAuthenticationToken authenticationToken =
					new UsernamePasswordAuthenticationToken(username, password);
			Authentication authentication = authManager.authenticate(authenticationToken);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			
			/*
			 * Reload user as password of authentication principal will be null after authorization and
			 * password is needed for token generation
			 */
			UserDetails userDetails = userService.loadUserByUsername(username);
			response = ResponseWrapper.successfulResponse(new TokenTransfer(TokenUtils.createToken(userDetails)));
		}catch(BadCredentialsException e){
			response = ResponseWrapper.unauthenticated();
		}
		
		return response;
	}
	


	/*private Map<String, Boolean> createRoleMap(UserDetails userDetails)
	{
		Map<String, Boolean> roles = new HashMap<String, Boolean>();
		for (GrantedAuthority authority : userDetails.getAuthorities()) {
			roles.put(authority.getAuthority(), Boolean.TRUE);
		}

		return roles;
	}*/
}
