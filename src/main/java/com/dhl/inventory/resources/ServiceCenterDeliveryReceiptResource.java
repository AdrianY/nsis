package com.dhl.inventory.resources;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.DeliveryReceiptCO;
import com.dhl.inventory.dto.queryobject.QueryResultObjectWrapper;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.DeliveryReceiptLQO;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.DeliveryReceiptQRO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.services.servicecenter.inventorymaintenance.DeliveryReceiptService;

@Component
@Path("facilities/{facilityCodeName}/deliveryreceipts")
public class ServiceCenterDeliveryReceiptResource {
	private static final Logger LOGGER = LoggerFactory.getLogger(DeliveryReceiptResource.class);

	@Autowired
	@Qualifier("serviceCenterDeliveryReceiptService")
	private DeliveryReceiptService deliveryReceiptService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDeliveryLists(@BeanParam DeliveryReceiptLQO deliveryReceiptListQueryObject){
		SearchList orderRequestList = deliveryReceiptService.findAllDeliveryReceipt(deliveryReceiptListQueryObject);
		return ResponseWrapper.successfulResponse(orderRequestList);
	}
	
	@GET
	@Path("/{requestNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDeliveryReceipt(@PathParam("requestNumber") String requestNumber){
		LOGGER.info("GET - requestNumber: "+requestNumber);
		Response response = null;
		try {
			QueryResultObjectWrapper<DeliveryReceiptQRO> deliveryReceiptQueryResultObject = 
					deliveryReceiptService.getDeliveryReceiptByRequestNumber(requestNumber);
			response = ResponseWrapper.successfulResponse(deliveryReceiptQueryResultObject);
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		}
		
		return response;
	}
	
	@PUT
	@Path("/{requestNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateOrderRequest(DeliveryReceiptCO deliveryReceiptCO, @PathParam("requestNumber") String requestNumber){
		LOGGER.info("PUT - delivery receipt for OR: "+deliveryReceiptCO.getRequestNumber());
		
		Response response = null;
		try {
			deliveryReceiptService.updateDeliveryReceipt(requestNumber, deliveryReceiptCO);
			QueryResultObjectWrapper<DeliveryReceiptQRO> deliveryReceipt = 
					deliveryReceiptService.getDeliveryReceiptByRequestNumber(
							deliveryReceiptCO.getRequestNumber());
			response = ResponseWrapper.successfulResponse(deliveryReceipt);
		} catch (ValidationException e) {
			response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		}
		
		return response;
	}
	
	@Path("/deliveryreceipts")
	public AbstractHeaderResource postRequest(@HeaderParam("xAction") String action){
		if ("FINALIZE".equals(action)) {
			return new FinalizeDeliveryReceiptRequest(deliveryReceiptService);
        } else {
        	return null;
        }
	}
	
	public static abstract class AbstractHeaderResource{
	}
	
	public static class FinalizeDeliveryReceiptRequest extends AbstractHeaderResource{
		
		private DeliveryReceiptService deliveryReceiptService;
		
		public FinalizeDeliveryReceiptRequest(DeliveryReceiptService deliveryReceiptService){
			this.deliveryReceiptService = deliveryReceiptService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(DeliveryReceiptCO deliveryReceiptCO){
			LOGGER.info("POST - finalize delivery receipt for OR: "+deliveryReceiptCO.getRequestNumber());
			
			Response response = null;
			try {
				deliveryReceiptService.finalizeDeliveryReceipt(deliveryReceiptCO);
				QueryResultObjectWrapper<DeliveryReceiptQRO> deliveryReceipt = 
						deliveryReceiptService.getDeliveryReceiptByRequestNumber(
								deliveryReceiptCO.getRequestNumber());
				response = ResponseWrapper.successfulResponse(deliveryReceipt);
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			} catch (CannotBeFoundException e) {
				response = ResponseWrapper.resourceNotFound(e.getMessage());
			}
			
			return response;
		}
	}
}
