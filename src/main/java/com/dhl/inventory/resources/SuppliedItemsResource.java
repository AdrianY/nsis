package com.dhl.inventory.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.commandobject.warehouse.requestissuance.SuppliedItemsGroupResourceCO;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.services.warehouse.requestissuance.SuppliedItemsService;

@Component
@Path("/requestissuances/{requestNumber}/supplieditemsgroup")
public class SuppliedItemsResource {
	private static final Logger LOGGER = LoggerFactory.getLogger(SuppliedItemsResource.class);
	
	@Autowired
	@Qualifier("warehouseSuppliedItemsService")
	private SuppliedItemsService suppliedItemsService;
	
	@Path("/supplieditemsgroup")
	public AbstractHeaderResource postRequest(@HeaderParam("xAction") String action){
		if ("VALIDATE".equals(action)) {
			return new ValidateSupplyItemsGroup(suppliedItemsService);
        } else if("LISTSUPPLIES".equals(action)){
        	return new ListAvailableSupplyItemsForGroup(suppliedItemsService);
        }else {
        	throw new WebApplicationException(404);
        }
	}
	
	public static abstract class AbstractHeaderResource{
	}
	
	public static class ListAvailableSupplyItemsForGroup extends AbstractHeaderResource{
		
		private SuppliedItemsService supplyRequestItemService;
		
		public ListAvailableSupplyItemsForGroup(SuppliedItemsService supplyRequestItemService){
			this.supplyRequestItemService = supplyRequestItemService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(
				SuppliedItemsGroupResourceCO oRItemCommand, 
				@PathParam("requestNumber") String requestNumber
		){
			LOGGER.info("POST - List available items supplies for requested item: "+oRItemCommand.getItemCode());
			
			return ResponseWrapper.successfulResponse(
					supplyRequestItemService.findAllSuppliedItemsGroupResource(oRItemCommand, requestNumber));
		}
	}
	
	public static class ValidateSupplyItemsGroup extends AbstractHeaderResource{
		
		private SuppliedItemsService supplyRequestItemService;
		
		public ValidateSupplyItemsGroup(SuppliedItemsService supplyRequestItemService){
			this.supplyRequestItemService = supplyRequestItemService;
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response execute(
				SuppliedItemsGroupResourceCO oRItemCommand, 
				@PathParam("requestNumber") String requestNumber
		){
			LOGGER.info("POST - validate supply item group for requested item: "+oRItemCommand.getItemCode());
			
			Response response = null;
			try {
				supplyRequestItemService.validateSuppliedItemsGroup(oRItemCommand, requestNumber);
				response = ResponseWrapper.successfulResponse();
			} catch (ValidationException e) {
				response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
			}
			
			return response;
		}
	}
}
