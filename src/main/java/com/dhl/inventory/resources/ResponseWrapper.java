package com.dhl.inventory.resources;

import java.util.Map;

import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

final class ResponseWrapper {
	
	private static final int HTTP_CODE_OK = 200;
	
	private static final int HTTP_CODE_OK_NO_CONTENT = 204;
	
	private static final int UNPROCESSABLE_ENTITY = 422;
	
	private static final int UNAUTHENTICATED = 401;
	
	private static final int NOT_FOUND = 404;
	
	private static final int BAD_REQUEST = 400;
	
	private static final ObjectMapper mapper = new ObjectMapper();

	public static Response successfulResponse(Object entity){
		Response response = null;
		try {
			response = Response.status(HTTP_CODE_OK).entity(mapper.writeValueAsString(entity)).build();
		} catch (JsonProcessingException e) {
			response = successfulResponse();
		}
		
		return response;
	}

	public static Response successfulResponse(){
		return Response.status(HTTP_CODE_OK_NO_CONTENT).build();
	}
	
	public static Response unprocessableEntity(String errorMessage){
		
		Response response = null;
		try {
			response = Response.status(UNPROCESSABLE_ENTITY).entity(mapper.writeValueAsString(errorMessage)).build();
		} catch (JsonProcessingException e) {
			response = Response.status(UNPROCESSABLE_ENTITY).build();
		}
		
		return response;
	}
	
	public static Response unprocessableEntity(Map<String, String> errorMessages){
		
		Response response = null;
		try {
			response = Response.status(UNPROCESSABLE_ENTITY).entity(mapper.writeValueAsString(errorMessages)).build();
		} catch (JsonProcessingException e) {
			response = Response.status(UNPROCESSABLE_ENTITY).build();
		}
		
		return response;
	}
	
	public static Response unauthenticated(){
		Response response = null;
		try {
			response = Response.status(UNAUTHENTICATED).entity(mapper.writeValueAsString("Unauthenticated")).build();
		} catch (JsonProcessingException e) {
			response = Response.status(UNAUTHENTICATED).build();
		}
		
		return response;
	}
	
	public static Response resourceNotFound(String msg){
		Response response = null;
		try {
			response = Response.status(NOT_FOUND).entity(mapper.writeValueAsString(msg)).build();
		} catch (JsonProcessingException e) {
			response = Response.status(NOT_FOUND).build();
		}
		
		return response;
	}
	
	public static Response badRequest(Map<String, String> errorMessages){
		Response response = null;
		try {
			response = Response.status(BAD_REQUEST).entity(mapper.writeValueAsString(errorMessages)).build();
		} catch (JsonProcessingException e) {
			response = Response.status(BAD_REQUEST).build();
		}
		
		return response;
	}
}
