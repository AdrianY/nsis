package com.dhl.inventory.resources;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.commandobject.customerinterfaces.CustomerRequestedItemResourceCO;
import com.dhl.inventory.services.customerinterfaces.CustomerRequestedItemResourceService;
import com.dhl.inventory.services.exceptions.ValidationException;

@Component
@Path("facilities/{facilityCodeName}/customerrequests/{requestNumber}/requestitems")
public class CustomerRequestItemResource {
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerRequestItemResource.class);
	
	@Autowired
	@Qualifier("customerRequestedItemResourceService")
	private CustomerRequestedItemResourceService customerRequestItemService;
	
	@POST
	@Path("/items")
	public Response postRequest(
			@PathParam("facilityCodeName") String facilityCode, 
			@PathParam("requestNumber") String requestNumber,
			CustomerRequestedItemResourceCO commandObject){
		LOGGER.info("POST - validate customer requested item "+commandObject
		+" for customer request: "+requestNumber);
		
		Response response = null;
		try {
			customerRequestItemService.validateRequestedItem(commandObject);
			response = ResponseWrapper.successfulResponse();
		} catch (ValidationException e) {
			response = ResponseWrapper.unprocessableEntity(e.getAllErrors());
		}
		
		return response;
	}
}
