package com.dhl.inventory.resources;

import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.maintenance.FacilityItemLQO;
import com.dhl.inventory.dto.queryobject.maintenance.FacilityItemQRO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.maintenance.FacilityInventoryItemService;

@Component
@Path("/facilities/{facilityId}/items")
public class FacilityItemResource {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FacilityItemResource.class);

	@Autowired
	@Qualifier("facilityInventoryItemService")
	private FacilityInventoryItemService facilityItemService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFacilityItems(@BeanParam FacilityItemLQO facilityItemsLQO){
		SearchList searchList = null;
		LOGGER.info("facilityItemsLQO.getSearchType(): "+facilityItemsLQO.getSearchType());
		switch(facilityItemsLQO.getSearchType()){
			case "TYPEHEAD": 
				facilityItemsLQO.setRowOffset(0);
				facilityItemsLQO.setRows(7);
				searchList = facilityItemService.searchFacilityItems(facilityItemsLQO);
				break;
			case "FILTER":
				searchList = facilityItemService.findAllFacilityItems(facilityItemsLQO);
				break;
		}
		
		return ResponseWrapper.successfulResponse(searchList);
	}
	
	@GET
	@Path("/{codename}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFacilityItem(@PathParam("facilityId") String facilityId, @PathParam("codename") String codeName){
		Response response = null;
		try {
			FacilityItemQRO facilityItem = facilityItemService.getFacilityItemByCodeAndFacility(codeName, facilityId);
			response =  ResponseWrapper.successfulResponse(facilityItem);
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		}
		
		return response;
	}
}
