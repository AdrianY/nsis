package com.dhl.inventory.resources;

import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.maintenance.FacilityPersonnelLQO;
import com.dhl.inventory.dto.queryobject.maintenance.FacilityPersonnelQRO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.maintenance.FacilityPersonnelService;

@Component
@Path("/facilities/{facilityId}/personnels")
public class FacilityPersonnelResource {
	private static final Logger LOGGER = LoggerFactory.getLogger(FacilityPersonnelResource.class);
	
	@Autowired
	@Qualifier("facilityPersonnelService")
	private FacilityPersonnelService facilityPersonnelService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFacilityPersonnels(@BeanParam FacilityPersonnelLQO facilityPersonnelsLQO){
		SearchList searchList = null;
		LOGGER.info("facilityItemsLQO.getSearchType(): "+facilityPersonnelsLQO.getSearchType());
		switch(facilityPersonnelsLQO.getSearchType()){
			case "TYPEHEAD": 
				facilityPersonnelsLQO.setRowOffset(0);
				facilityPersonnelsLQO.setRows(7);
				searchList = facilityPersonnelService.searchFacilityPersonnels(facilityPersonnelsLQO);
				break;
			case "FILTER":
				searchList = facilityPersonnelService.findAllFacilityPersonnels(facilityPersonnelsLQO);
				break;
		}
		
		return ResponseWrapper.successfulResponse(searchList);
	}
	
	@GET
	@Path("/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFacilityPersonnel(@PathParam("facilityId") String facilityId, @PathParam("username") String userName){
		Response response = null;
		try {
			FacilityPersonnelQRO facilityItem = facilityPersonnelService.getFacilityPersonnelByUsernameAndFacilityCode(userName, facilityId);
			response =  ResponseWrapper.successfulResponse(facilityItem);
		} catch (CannotBeFoundException e) {
			response = ResponseWrapper.resourceNotFound(e.getMessage());
		}
		
		return response;
	}
}