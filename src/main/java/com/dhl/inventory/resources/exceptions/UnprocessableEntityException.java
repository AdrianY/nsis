package com.dhl.inventory.resources.exceptions;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class UnprocessableEntityException extends WebApplicationException {
	private static final long serialVersionUID = 1L;
    private List<String> errors;
 
    public UnprocessableEntityException(String... errors)
    {
        this(Arrays.asList(errors));       
    }
 
    public UnprocessableEntityException(List<String> errors)
    {
        super(Response.status(422).type(MediaType.APPLICATION_JSON)
                .entity(new GenericEntity<List<String>>(errors)
                {}).build());
        this.errors = errors;
    }
 
    public List<String> getErrors()
    {
        return errors;
    }
}
