package com.dhl.inventory.resources.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class UnauthorizedException extends WebApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 97771520483542690L;
	
	private static final String ERROR_MESSAGE = "Unauthenticated";
	
 
    public UnauthorizedException()
    {
        super(Response.status(401).type(MediaType.APPLICATION_JSON)
                .entity(ERROR_MESSAGE).build());
    }
 
    public String getError()
    {
        return ERROR_MESSAGE;
    }

}
