package com.dhl.inventory.resources;

import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.maintenance.WarehouseLocationLQO;
import com.dhl.inventory.services.maintenance.WarehouseLocationService;

@Component
@Path("/facilities/{facilityCode}/warehouselocations")
public class WarehouseLocationResource {
	private static final Logger LOGGER = LoggerFactory.getLogger(WarehouseLocationResource.class);
	
	@Autowired
	@Qualifier("warehouseLocationService")
	private WarehouseLocationService warehouseLocationService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFacilityPersonnels(@BeanParam WarehouseLocationLQO warehouseLocationLQO){
		SearchList searchList = null;
		LOGGER.info("warehouseLocationLQO.getSearchType(): "+warehouseLocationLQO.getSearchType());
		switch(warehouseLocationLQO.getSearchType()){
			case "TYPEHEAD": 
				warehouseLocationLQO.setRowOffset(0);
				warehouseLocationLQO.setRows(7);
				searchList = warehouseLocationService.searchWarehouseLocations(warehouseLocationLQO);
				break;
			case "FILTER":
				searchList = warehouseLocationService.findAllWarehouseLocations(warehouseLocationLQO);
				break;
		}
		
		return ResponseWrapper.successfulResponse(searchList);
	}
	
}
