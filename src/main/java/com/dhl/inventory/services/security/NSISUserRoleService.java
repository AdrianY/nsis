package com.dhl.inventory.services.security;

public interface NSISUserRoleService {
	public static final String ROLE_PREFIX = "ROLE_";
	
	void addNewRole(String codeName, String description);
}
