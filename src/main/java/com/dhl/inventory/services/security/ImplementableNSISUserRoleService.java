package com.dhl.inventory.services.security;

public interface ImplementableNSISUserRoleService {
	public static final String ROLE_PREFIX = "ROLE_";
	
	void addNewRole(String codeName, String description);
}
