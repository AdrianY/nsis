package com.dhl.inventory.services.security;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dhl.inventory.domain.security.NsisRole;
import com.dhl.inventory.repository.security.RolesRepository;

@Service("nsisUserRoleService")
public class NSISUserRoleServiceImpl implements NSISUserRoleService {
	
	private static final Logger LOGGER = Logger.getLogger(NSISUserRoleServiceImpl.class);

	@Autowired 
	private RolesRepository userRoleRepo;

	@Override
	public void addNewRole(String codeName, String description) {
		LOGGER.info("raw codeName: "+codeName);
		if(null != codeName && 0 != codeName.indexOf(ROLE_PREFIX)){
			codeName = ROLE_PREFIX + codeName;
		}
		LOGGER.info("codeName: "+codeName);
		NsisRole role = new NsisRole(codeName, description);
		userRoleRepo.saveRole(role);
	}

}
