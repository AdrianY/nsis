package com.dhl.inventory.services.security;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.security.NsisRole;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.security.NsisUserRole;
import com.dhl.inventory.dto.commandobject.security.NsisUserQRO;
import com.dhl.inventory.dto.commandobject.security.NsisUserRoleQRO;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.repository.maintenance.FacilityPersonnelRepository;
import com.dhl.inventory.repository.security.RolesRepository;

import org.springframework.security.crypto.password.PasswordEncoder;

@Service("nsisUserService")
class NSISUserServiceImpl implements NSISUserService{
	
	private static final Logger LOGGER = Logger.getLogger(NSISUserServiceImpl.class);
	
	private PasswordEncoder encoder;
	
	private UserRepository userRepo;
	
	private RolesRepository userRoleRepo;
	
	private FacilityPersonnelRepository facilityPersonnelRepo;
	
	@Autowired
	public NSISUserServiceImpl(
			final PasswordEncoder encoder,
			@Qualifier("userRepository") final UserRepository userRepo,
			@Qualifier("rolesRepository") final RolesRepository userRoleRepo,
			@Qualifier("facilityPersonnelRepository") final FacilityPersonnelRepository facilityPersonnelRepo
	){
		this.encoder = encoder;
		this.userRepo = userRepo;
		this.userRoleRepo = userRoleRepo;
		this.facilityPersonnelRepo = facilityPersonnelRepo;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public NsisUser addNewUser(String username, String plainTextPassword, String firstName, String lastName, String emailAddress, String contactNumber, String[] roleIds) {

		LOGGER.info("executing addNewUser for username: " + username);
		
		NsisUser user = new NsisUser();
		user.setUsername(username);
		user.setPassword(encoder.encode(plainTextPassword));
		user.setName(firstName, lastName);
		user.setEmailAddress(emailAddress);
		user.setContactNumber(contactNumber);
		user.setEnabled(true);		
		
		if(null != roleIds){
			Set<NsisUserRole> userRoles = new HashSet<>();
			
			for(String roleId: roleIds){
				NsisRole userRole = userRoleRepo.findByCodeName(roleId);
				LOGGER.info("roleId: " + roleId + " userRole is not null " + (null!= userRole));
				userRoles.add(new NsisUserRole(user, userRole));
			}
			
			user.setUserRoles(userRoles);
		}		
		
		userRepo.save(user);
		return user;
	}

	@Transactional(readOnly=true)
	@Override
	public NsisUser findByUsername(String username) {
		LOGGER.info("start userRepo.findByUserName: "+new Date());
		NsisUser nsisUser = userRepo.findByUserName(username); 
		LOGGER.info("end userRepo.findByUserName "+new Date());
		return nsisUser;
	}

	@Transactional(readOnly=true)
	@Override
	public NsisUserQRO findDTOByUsername(String username) {
		// TODO Auto-generated method stub
		NsisUser user = findByUsername(username);
		List<NsisUserRoleQRO> userRoles = getUserRoles(user);
		return new NsisUserQRO()
				.setFirstName(user.getFirstName())
				.setLastName(user.getLastName())
				.setUsername(user.getUsername())
				.setRoles(userRoles)
				.setFacilityAssignments(retrievePerFacilityAssignment(username));
	}
	
	private List<NsisUserRoleQRO> getUserRoles(NsisUser user){
		List<NsisUserRoleQRO> userRoles = new ArrayList<>();
		
		Optional.object(user.getUserRoles()).ifPresent(joinList -> {
			joinList.stream().forEach(userRoleJoinData -> {
				String codeName = userRoleJoinData.getRole().getCodeName().replace("ROLE_", "");
				userRoles.add(new NsisUserRoleQRO().setCodeName(codeName));
			});
		});
		
		return userRoles;
	}

	@Transactional(readOnly=true)
	@Override
	public List<Map<String, String>> retrievePerFacilityAssignment(String username){
		List<Map<String, String>> returnValue = new ArrayList<>();
		Optional.object(facilityPersonnelRepo.findAllPersonnelPerFacilityAssignment(username))
		.ifPresent(facilityAssignments -> {
			facilityAssignments.stream().forEach(facilityAssignment -> {
				Map<String, String> facilityAssignmentDetails = new HashMap<>();
				Facility facility = facilityAssignment.getAssignedFacility();
				facilityAssignmentDetails.put("facilityCodeName", facility.getCodeName());
				facilityAssignmentDetails.put("type", facility.getType());
				
				returnValue.add(facilityAssignmentDetails);
			});
		});
		return returnValue;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateUser(String username, String[] roleIds) {
		NsisUser user = userRepo.findByUserName(username); 
		
		if(null != roleIds){
			Set<NsisUserRole> userRoles = new HashSet<>();
			
			Set<NsisRole> existingRoles = user.getUserRoles().stream().map(userRole -> {
				userRoles.add(userRole);
				return userRole.getRole();
			}).collect(Collectors.toSet());
			
			for(String roleId: roleIds){
				NsisRole userRole = userRoleRepo.findByCodeName(roleId);
				
				if(!existingRoles.contains(userRole)){
					LOGGER.info("roleId: " + roleId + " is a new role ");
					userRoles.add(new NsisUserRole(user, userRole));
				}
			}
			
			user.setUserRoles(userRoles);
			userRepo.save(user);
		}
	}
}
