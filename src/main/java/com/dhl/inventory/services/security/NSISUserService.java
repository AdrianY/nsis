package com.dhl.inventory.services.security;

import java.util.List;
import java.util.Map;

import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.dto.commandobject.security.NsisUserQRO;

public interface NSISUserService {
	NsisUser addNewUser(String username, String password,String firstName, String lastName, String emailAddress, String contactNumber, String[] roleIds);
	
	NsisUser findByUsername(String username);
	
	NsisUserQRO findDTOByUsername(String username);
	
	void updateUser(String username, String[] roleIds);
	
	List<Map<String, String>> retrievePerFacilityAssignment(String username);
}
