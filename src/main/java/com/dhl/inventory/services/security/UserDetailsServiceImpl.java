package com.dhl.inventory.services.security;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.security.NsisUserRole;
import com.dhl.inventory.repository.security.UserRepository;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService{
	
		private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

		private UserRepository userRepo;
		
		@Autowired
		public UserDetailsServiceImpl(@Qualifier("userRepository") final UserRepository userRepo){
			this.userRepo = userRepo;
		}
		
		@Transactional(readOnly=true)
		@Override
		public UserDetails loadUserByUsername(final String username) 
			throws UsernameNotFoundException {
			NsisUser user = userRepo.findByUserName(username);
			
			if(null == user)
				throw new UsernameNotFoundException("User not found");
			
			List<GrantedAuthority> authorities = 
	                                      buildUserAuthority(user.getUserRoles());

			return buildUserForAuthentication(user, authorities);
			
		}
		
		private User buildUserForAuthentication(NsisUser user, 
			List<GrantedAuthority> authorities) {
			return new User(user.getUsername(), user.getPassword(), 
				user.isEnabled(), true, true, true, authorities);
		}

		private List<GrantedAuthority> buildUserAuthority(Set<NsisUserRole> userRoles) {

			Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

			// Build user's authorities
			for (NsisUserRole userRole : userRoles) {
				LOGGER.info("userRole.getRole().getCodeName(): "+userRole.getRole().getCodeName());
				setAuths.add(new SimpleGrantedAuthority(userRole.getRole().getCodeName()));
			}

			List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);

			return Result;
		}

}
