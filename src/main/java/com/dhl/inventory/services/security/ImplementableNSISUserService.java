package com.dhl.inventory.services.security;

import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.dto.security.NsisUserTransfer;

public interface ImplementableNSISUserService {
	NsisUser addNewUser(String username, String password,String firstName, String lastName, String[] roleIds);
	
	NsisUser findByUsername(String username);
	
	NsisUserTransfer findDTOByUsername(String username);
}
