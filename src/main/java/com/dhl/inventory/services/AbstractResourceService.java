package com.dhl.inventory.services;

import org.springframework.beans.factory.annotation.Autowired;

import com.dhl.inventory.components.AuthenticationBean;
import com.dhl.inventory.domain.NsisDomainEntity;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.dto.queryobject.PersonnelQRO;
import com.dhl.inventory.dto.queryobject.QROEditableFieldsMeta;

public abstract class AbstractResourceService<T extends NsisDomainEntity> {
	@Autowired
	protected AuthenticationBean authenticationHolder;
	
	protected void updateLog(T entity){
		entity.setRecordUpdatedBy(authenticationHolder.getAuthenticatedUserId());
	}
	
	protected void createNewLog(T entity){
		entity.setRecordCreatedBy(authenticationHolder.getAuthenticatedUserId());
	}
	
	protected abstract QROEditableFieldsMeta generateEditMeta(T nsisDomainEntity);
	
	protected abstract QROEditableFieldsMeta generateEditMetaForTemplate();
	
	protected PersonnelQRO wrapPersonnelQRO(NsisUser currentUserDetails){
		PersonnelQRO wrappedPersonnelQRO = new PersonnelQRO();
		
		if(null != currentUserDetails){
			wrappedPersonnelQRO
				.setUserName(currentUserDetails.getUsername())
				.setFullName(currentUserDetails.getFullName())
				.setEmailAddress(currentUserDetails.getEmailAddress())
				.setContactNumber(currentUserDetails.getContactNumber());
		}
		
		return wrappedPersonnelQRO;
	}
}
