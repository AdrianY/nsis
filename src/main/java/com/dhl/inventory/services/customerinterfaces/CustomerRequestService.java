package com.dhl.inventory.services.customerinterfaces;

import com.dhl.inventory.dto.commandobject.customerinterfaces.CustomerRequestCO;
import com.dhl.inventory.dto.queryobject.QueryResultObjectWrapper;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.customerinterfaces.CustomerRequestLQO;
import com.dhl.inventory.dto.queryobject.customerinterfaces.CustomerRequestQRO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.CannotBeRemovedException;
import com.dhl.inventory.services.exceptions.ValidationException;

public interface CustomerRequestService {
	
	String createNewCustomerRequest(CustomerRequestCO customerRequestCO) throws ValidationException;
	
	void removeCustomerRequest(String requestNumber) throws CannotBeRemovedException, CannotBeFoundException;
	
	void updateCustomerRequest(String requestNumber, CustomerRequestCO customerRequestCO) throws ValidationException, CannotBeFoundException;
	
	void submitCustomerRequest(CustomerRequestCO customerRequestCO) throws ValidationException;
	
	QueryResultObjectWrapper<CustomerRequestQRO> getCustomerRequestByRequestNumber(String requestNumber) throws CannotBeFoundException;
	
	CustomerRequestQRO findCustomerRequestByRequestNumber(String requestNumber) throws CannotBeFoundException;
	
	QueryResultObjectWrapper<CustomerRequestQRO> generateCustomerRequestTemplate();

	SearchList findAllCustomerRequests(CustomerRequestLQO customerRequestListQueryObject);
}
