package com.dhl.inventory.services.customerinterfaces;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.components.AuthenticationBean;
import com.dhl.inventory.converter.customerinterfaces.CustomerRequestCOConverter;
import com.dhl.inventory.converter.customerinterfaces.CustomerRequestQROConverter;
import com.dhl.inventory.domain.customerinterfaces.CustomerRequest;
import com.dhl.inventory.domain.security.NsisRole;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.security.NsisUserRole;
import com.dhl.inventory.dto.commandobject.customerinterfaces.CustomerRequestCO;
import com.dhl.inventory.dto.queryobject.PersonnelQRO;
import com.dhl.inventory.dto.queryobject.QROEditableFieldsMeta;
import com.dhl.inventory.dto.queryobject.QueryResultObjectWrapper;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.customerinterfaces.CustomerRequestLQO;
import com.dhl.inventory.dto.queryobject.customerinterfaces.CustomerRequestQRO;
import com.dhl.inventory.dto.queryobject.customerinterfaces.RequestedItemQRO;
import com.dhl.inventory.repository.customerinterfaces.CustomerRequestIssuanceDetailsRepository;
import com.dhl.inventory.repository.customerinterfaces.CustomerRequestNumberGenerator;
import com.dhl.inventory.repository.customerinterfaces.CustomerRequestRepository;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.services.AbstractResourceService;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.CannotBeRemovedException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.util.CustomerRequestStatus;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.util.SearchType;
import com.dhl.inventory.util.SupplyRequestStatus;
import com.dhl.inventory.util.UsingChainable;
import com.dhl.inventory.validator.customerinterfaces.CustomerRequestCommandValidator;

import static com.dhl.inventory.util.ActionType.*;

@Service("customerRequestService")
class CustomerRequestServiceImpl extends AbstractResourceService<CustomerRequest> implements CustomerRequestService {

	@Autowired
	private CustomerRequestNumberGenerator referenceCodeGenerator;
	
	@Autowired 
	private UserRepository userRepo;
	
	@Autowired
	private AuthenticationBean authBean;
	
	@Autowired
	private CustomerRequestCOConverter detailsConverter;
	
	@Autowired
	private CustomerRequestQROConverter readWrapper;
	
	@Autowired
	private CustomerRequestCommandValidator commandValidator;
	
	@Autowired
	private CustomerRequestRepository customerRequestRepo;
	
	@Autowired
	private CustomerRequestIssuanceDetailsRepository issuanceDetailsRepo;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public String createNewCustomerRequest(CustomerRequestCO customerRequestCO) throws ValidationException {
		commandValidator.validate(CREATE_COMMAND, customerRequestCO);
		CustomerRequest customerRequest = 
				detailsConverter.convert(CREATE_COMMAND, customerRequestCO, new CustomerRequest());
		customerRequest.setRequestNumber(referenceCodeGenerator.generateReferenceCode());
		customerRequest.setStatus(CustomerRequestStatus.DRAFT.toString());
		customerRequest.setRequestType(determineRequestType());
		customerRequestRepo.save(customerRequest);
		
		return customerRequest.getRequestNumber();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void removeCustomerRequest(String requestNumber) throws CannotBeRemovedException, CannotBeFoundException {
		CustomerRequest customerRequest = customerRequestRepo.findByRequestNumber(requestNumber);
		validateRemovalOfCustomerRequest(customerRequest);
		customerRequestRepo.delete(customerRequest);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateCustomerRequest(String requestNumber, CustomerRequestCO customerRequestCO)
			throws ValidationException, CannotBeFoundException {
		
		CustomerRequest toBeUpdatedCustomerRequest = 
				customerRequestRepo.findByRequestNumber(requestNumber);
		
		if(null == toBeUpdatedCustomerRequest)
			throw new CannotBeFoundException("Cannot update customer request: "+requestNumber+". It does not exist");
		
		commandValidator.validate(UPDATE_COMMAND, customerRequestCO);
		
		clearAllExistingRequestedItems(toBeUpdatedCustomerRequest);
		
		CustomerRequest customerRequest = 
				detailsConverter.convert(UPDATE_COMMAND, customerRequestCO, toBeUpdatedCustomerRequest);
		
		customerRequestRepo.save(customerRequest);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void submitCustomerRequest(CustomerRequestCO customerRequestCO) throws ValidationException {
		
		validateCustomerRequestSubmit(customerRequestCO);
		
		Optional.object(customerRequestRepo.findByRequestNumber(customerRequestCO.getRequestNumber()))
		.ifNotPresent(() -> {
			return new CustomerRequest(customerRequestCO.getRequestNumber());
		})
		.then(toBeUpdatedCustomerRequest -> {
			clearAllExistingRequestedItems(toBeUpdatedCustomerRequest);
			CustomerRequest customerRequest = 
					detailsConverter.convert(SUBMIT_COMMAND, customerRequestCO, toBeUpdatedCustomerRequest);
			customerRequest.setStatus(CustomerRequestStatus.SUBMITTED.toString());
			customerRequest.setRequestType(determineRequestType());
			customerRequest.setRequestDate(new Date());
			customerRequestRepo.save(customerRequest);
		}, null);
	}

	@Override
	@Transactional(readOnly = true)
	public QueryResultObjectWrapper<CustomerRequestQRO> getCustomerRequestByRequestNumber(String requestNumber)
			throws CannotBeFoundException {
		CustomerRequest customerRequest = customerRequestRepo.findByRequestNumber(requestNumber);
		if(null == customerRequest) 
			throw new CannotBeFoundException("Cannot find Customer Request: "+requestNumber);
		
		CustomerRequestQRO qro = readWrapper.generateQRO(customerRequest);
		QROEditableFieldsMeta editMeta = generateEditMeta(customerRequest);
		
		return QueryResultObjectWrapper.generateInstance(qro, editMeta);
	}

	@Override
	@Transactional(readOnly = true)
	public QueryResultObjectWrapper<CustomerRequestQRO> generateCustomerRequestTemplate() {
		CustomerRequestQRO customerRequestTemplate = new CustomerRequestQRO()
			.setStatus(CustomerRequestStatus.NEW.toString())
			.setRequestNumber(referenceCodeGenerator.generateReferenceCode());
		
		String currentUserName = authBean.getPrincipalName();
		NsisUser currentUserDetails = userRepo.findByUserName(currentUserName);
		PersonnelQRO currentUserWrappedDetails = wrapPersonnelQRO(currentUserDetails);
		customerRequestTemplate.setRequestBy(currentUserWrappedDetails);
		customerRequestTemplate.setRequestedItems(new ArrayList<RequestedItemQRO>());

		QROEditableFieldsMeta editMeta = generateEditMetaForTemplate();
		
		return QueryResultObjectWrapper.generateInstance(customerRequestTemplate, editMeta);
	}

	/**
	 * 1. Get all request numbers with request issuances
	 * 2. Segregate them from the rest of request issuances retrieve by repo
	 * 3. Change the appropriate details
	 * 4. Combine them again with the rest of RI
	 * 
	 * @param customerRequestListQueryObject
	 * @return
	 */
	@Override
	@Transactional(readOnly = true)
	public SearchList findAllCustomerRequests(CustomerRequestLQO customerRequestListQueryObject) {
		SearchListValueObject itemSearchList = customerRequestRepo.findAll(customerRequestListQueryObject, SearchType.FILTER);
		
		List<Map<String, String>> editedSearchInstances = new ArrayList<>();
		Optional.object(itemSearchList.getSearchedInstances()).ifPresent(searchInstances -> {
			List<String> requestNumbers = 
					searchInstances.stream().map(val -> val.get("requestNumber"))
					.collect(Collectors.toList());
			
			Optional.object(issuanceDetailsRepo.findIssuanceDetailsForEveryRequest(requestNumbers)).ifPresent(issuancesDetails -> {
				
				Map<String, String> issuanceStatusMap = 
						issuancesDetails.stream().collect(
								Collectors.toMap(
									val -> val.getRequestNumber(), 
									val -> val.getStatus()));
				
				List<Map<String, String>> searchInstancesWithRI = new ArrayList<>();
				List<Map<String, String>> searchInstancesWithOutRI = new ArrayList<>();
				
				
				searchInstances.stream().forEach(instance -> {
					String issuanceStatus = issuanceStatusMap.get(instance.get("requestNumber"));
					if(null != issuanceStatus){
						instance.put("status", issuanceStatus);
						searchInstancesWithRI.add(instance);
					}else{
						searchInstancesWithOutRI.add(instance);
					}
				});
				
				editedSearchInstances.addAll(searchInstancesWithRI);
				editedSearchInstances.addAll(searchInstancesWithOutRI);
			});
			
		});
		
		List<Map<String, String>> actualSearchInstances = editedSearchInstances.isEmpty() ?
				actualSearchInstances = itemSearchList.getSearchedInstances() :
					editedSearchInstances;	
		
		SearchListValueObject actualSearchValueList = 
				new SearchListValueObject(actualSearchInstances, itemSearchList.getTotalCount());
		return SearchList.generateInstance(actualSearchValueList, customerRequestListQueryObject);
	}

	@Override
	protected QROEditableFieldsMeta generateEditMeta(CustomerRequest customerRequest) {
		QROEditableFieldsMeta returnValue  = 
				(QROEditableFieldsMeta) UsingChainable
				.thisObject(authBean.getAuthenticatedUserId())
				.deriveIfPresent(userId -> {
						if(customerRequest.getStatus().equals(SupplyRequestStatus.DRAFT.toString()) && 
								userId.equals(customerRequest.getRequestById())
								){
							return generateEditMetaForTemplate();
						}else if(
								customerRequest.getStatus().equals(SupplyRequestStatus.NEW.toString())
								){
							return generateEditMetaForTemplate();
						}else {
							return QROEditableFieldsMeta.noEditableInstance();
						}
				})
				.deriveIfNotPresent(() -> {
						return QROEditableFieldsMeta.noEditableInstance();
				})
				.execute();

			return returnValue;
	}

	@Override
	protected QROEditableFieldsMeta generateEditMetaForTemplate() {
		ArrayList<String> exceptions = new ArrayList<String>();
		exceptions.add("requestBy.fullName");
		exceptions.add("requestNumber");
		exceptions.add("requestDate");
		exceptions.add("status");
		exceptions.add("routeCode");
		exceptions.add("custodian.custodianDetails.fullName");
		exceptions.add("custodian.custodianDetails.userName");
		exceptions.add("custodian.remarks");
		return QROEditableFieldsMeta
				.editableExceptCertainFieldsInstance(exceptions);
	}

	private void clearAllExistingRequestedItems(CustomerRequest customerRequest){
		if(null != customerRequest && !customerRequest.isNew())
			customerRequestRepo.clearAllRequestedItemRecords(customerRequest);
	}
	
	private void validateRemovalOfCustomerRequest(CustomerRequest customerRequest) throws CannotBeRemovedException, CannotBeFoundException{
		if(null == customerRequest) 
			throw new CannotBeFoundException("Cannot find Request.");
		
		//validate if can be deleted
		String errorMessage = "";
		if(!SupplyRequestStatus.DRAFT.toString().equals(customerRequest.getStatus())){
			errorMessage = "Cannot remove an Request that has already been submitted or processed.";
		}else if(!authBean.getAuthenticatedUserId().equals(customerRequest.getRequestById())){
			errorMessage = "Cannot remove an Request you have not created.";
		}
		
		if(!errorMessage.isEmpty())
			throw new CannotBeRemovedException(errorMessage);
	}
	
	private void validateCustomerRequestSubmit(CustomerRequestCO commandObject) throws ValidationException {
		Map<String, String> errorMessages = new HashMap<String, String>();
		try{
			commandValidator.validate(SUBMIT_COMMAND, commandObject);
		}catch(ValidationException e){
			errorMessages.putAll(e.getAllErrors());
		}finally{
			if(null == authBean.getAuthenticatedUserId())
				errorMessages.put("orderById", "Must be logged-in to submit OR. Try to login again.");
			if(!errorMessages.isEmpty())
				throw new ValidationException(errorMessages);
		}
	}
	
	private String determineRequestType(){
		String requestorId = authBean.getAuthenticatedUserId();
		NsisUser user = userRepo.getByKey(requestorId);
		Set<NsisUserRole> userRoles = user.getUserRoles();
		
		String returnValue = "";
		if(Optional.object(userRoles).hasContent()){
			NsisUserRole userRole = userRoles.iterator().next();
			NsisRole roleDetails = userRole.getRole();
			switch(roleDetails.getCodeName()){
				case "ROLE_CSPERSONNEL": returnValue = "Customer Service"; break;
				case "ROLE_SPPERSONNEL": returnValue = "Service Provider"; break;
				case "ROLE_CRPERSONNEL": returnValue = "Courier Request"; break;
			}
		}
		return returnValue;
	}

	@Override
	@Transactional(readOnly = true)
	public CustomerRequestQRO findCustomerRequestByRequestNumber(String requestNumber) throws CannotBeFoundException {
		CustomerRequest customerRequest = customerRequestRepo.findByRequestNumber(requestNumber);
		if(null == customerRequest) 
			throw new CannotBeFoundException("Cannot find Customer Request: "+requestNumber);
		
		return readWrapper.generateQRO(customerRequest);
	}
}
