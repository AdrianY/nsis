package com.dhl.inventory.services.customerinterfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.domain.maintenance.InventoryItem;
import com.dhl.inventory.dto.commandobject.customerinterfaces.CustomerRequestCO;
import com.dhl.inventory.dto.commandobject.customerinterfaces.CustomerRequestedItemResourceCO;
import com.dhl.inventory.dto.commandobject.customerinterfaces.RequestedItemCO;
import com.dhl.inventory.repository.maintenance.InventoryItemRepository;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.util.Optional;

@Service("customerRequestedItemResourceService")
class CustomerRequestedItemResourceServiceImpl implements CustomerRequestedItemResourceService {

	@Autowired
	private InventoryItemRepository inventoryItemRepo;
	
	@Override
	public void validateRequestedItem(CustomerRequestedItemResourceCO requestedItemCO) throws ValidationException {
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional(readOnly = true)
	public List<Map<String, String>> calculateDetailsOfRequestedItems(CustomerRequestCO commandObject)
			throws ValidationException {

		List<String> itemCodes = new ArrayList<>();
		Map<String, RequestedItemCO> itemCOMap = new HashMap<>();
		Optional.object(commandObject.getRequestedItems()).ifPresent(requestedItemsCO -> {
			requestedItemsCO.forEach(requestedItemCO -> {
				itemCodes.add(requestedItemCO.getItemCode());
				itemCOMap.put(requestedItemCO.getItemCode(), requestedItemCO);
			});
		});
		
		List<InventoryItem> inventoryItems = inventoryItemRepo.findAllItemDetailsInList(itemCodes);
		List<Map<String, String>> returnValue = new ArrayList<>();
		Optional.object(inventoryItems).ifPresent(inventoryItemList -> {
			inventoryItemList.forEach(inventoryItem -> {
				Map<String, String> record = new HashMap<>();
				record.put("itemCode", inventoryItem.getCodeName());
				record.put("type", inventoryItem.getType());
				record.put("description", inventoryItem.getDescription());
				
				RequestedItemCO itemCO = itemCOMap.get(inventoryItem.getCodeName());
				record.put("remarks", itemCO.getRemarks());
				record.put("quantity", String.valueOf(itemCO.getQuantity()));
				
				returnValue.add(record);
			});
		});

		return returnValue;
	}

}
