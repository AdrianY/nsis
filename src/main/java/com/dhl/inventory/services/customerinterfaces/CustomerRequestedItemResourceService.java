package com.dhl.inventory.services.customerinterfaces;

import java.util.List;
import java.util.Map;

import com.dhl.inventory.dto.commandobject.customerinterfaces.CustomerRequestCO;
import com.dhl.inventory.dto.commandobject.customerinterfaces.CustomerRequestedItemResourceCO;
import com.dhl.inventory.services.exceptions.ValidationException;

public interface CustomerRequestedItemResourceService {
	List<Map<String, String>> calculateDetailsOfRequestedItems(CustomerRequestCO commandObject) throws ValidationException;
	
	void validateRequestedItem(CustomerRequestedItemResourceCO requestedItemCO) throws ValidationException; 
}
