package com.dhl.inventory.services.servicecenter.inventorymaintenance;

import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.ServiceCenterOnHandByItemCodeLQO;

public interface ServiceCenterOnHandSuppliesService {
	SearchList findAllOnHandSupplies(ServiceCenterOnHandByItemCodeLQO onHandByItemCodeLQO);
}
