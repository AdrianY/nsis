package com.dhl.inventory.services.servicecenter.supplyrequest;

import java.util.List;
import java.util.Map;

import com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest.SupplyRequestCO;
import com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest.SupplyRequestItemResourceCO;
import com.dhl.inventory.services.exceptions.ValidationException;


public interface SupplyRequestItemService {
	List<Map<String, String>> calculateDetailsOfRequestItems(SupplyRequestCO calculateCommand) throws ValidationException;

	void validateSupplyRequestItem(SupplyRequestItemResourceCO oRItemCommand) throws ValidationException;
}
