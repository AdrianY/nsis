package com.dhl.inventory.services.servicecenter.supplyrequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.components.AuthenticationBean;
import com.dhl.inventory.converter.servicecenter.supplyrequest.SupplyRequestApprovalCOConverter;
import com.dhl.inventory.converter.servicecenter.supplyrequest.SupplyRequestCOConverter;
import com.dhl.inventory.converter.servicecenter.supplyrequest.SupplyRequestQROConverter;
import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequestApproval;
import com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest.SupplyRequestApproverCO;
import com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest.SupplyRequestCO;
import com.dhl.inventory.dto.queryobject.PersonnelQRO;
import com.dhl.inventory.dto.queryobject.QROEditableFieldsMeta;
import com.dhl.inventory.dto.queryobject.QueryResultObjectWrapper;
import com.dhl.inventory.dto.queryobject.RecipientQRO;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.servicecenter.supplyrequest.SupplyRequestLQO;
import com.dhl.inventory.dto.queryobject.servicecenter.supplyrequest.SupplyRequestQRO;
import com.dhl.inventory.repository.maintenance.FacilityRepository;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.repository.servicecenter.supplyrequest.SupplyRequestApprovalRepository;
import com.dhl.inventory.repository.servicecenter.supplyrequest.SupplyRequestReferenceCodeGenerator;
import com.dhl.inventory.repository.servicecenter.supplyrequest.SupplyRequestRepository;
import com.dhl.inventory.services.AbstractResourceService;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.CannotBeRemovedException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.util.SupplyRequestStatus;
import com.dhl.inventory.util.SearchType;
import com.dhl.inventory.util.UsingChainable;
import com.dhl.inventory.validator.servicecenter.supplyrequest.SupplyRequestCommandValidator;

import static com.dhl.inventory.util.ActionType.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Service("supplyRequestService")
class SupplyRequestServiceImpl extends AbstractResourceService<SupplyRequest> implements SupplyRequestService{
	
	private static final Logger LOGGER = Logger.getLogger(SupplyRequestServiceImpl.class);
	
	private static final String DEFAULT_SR_CURRENCY = "PHP";

	@Autowired
	private SupplyRequestCommandValidator detailsValidator;
	
	@Autowired
	private SupplyRequestApprovalCOConverter approvalConverter;
	
	@Autowired
	private SupplyRequestCOConverter detailsConverter;
	
	@Autowired
	private SupplyRequestQROConverter readWrapper;
	
	@Autowired 
	private UserRepository userRepo;
	
	@Autowired
	private AuthenticationBean authBean;
	
	@Autowired 
	private SupplyRequestRepository supplyRequestRepo;
	
	@Autowired 
	private SupplyRequestApprovalRepository supplyRequestApprovalRepo;
	
	@Autowired 
	private SupplyRequestReferenceCodeGenerator supplyRequestReferenceCodeGenerator;
	
	@Autowired 
	private FacilityRepository facilityRepo;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public String createNewSupplyRequest(SupplyRequestCO commandObject) throws ValidationException {
		detailsValidator.validate(CREATE_COMMAND, commandObject);
		SupplyRequest supplyRequest = detailsConverter.convert(CREATE_COMMAND, commandObject, new SupplyRequest());
		supplyRequest.setStatus(SupplyRequestStatus.DRAFT.toString());
		supplyRequest.setRequestById(authBean.getAuthenticatedUserId());
		supplyRequestRepo.save(supplyRequest);
		return supplyRequest.getRequestNumber();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateSupplyRequest(String orderNumber, SupplyRequestCO commandObject) throws ValidationException {
		detailsValidator.validate(UPDATE_COMMAND, commandObject);
		SupplyRequest toBeUpdatedSupplyRequest = supplyRequestRepo.findByRequestNumber(orderNumber);
		cleanSupplyRequestItems(toBeUpdatedSupplyRequest);
		SupplyRequest supplyRequest = detailsConverter.convert(UPDATE_COMMAND, commandObject, toBeUpdatedSupplyRequest);
		supplyRequest.setRequestById(authBean.getAuthenticatedUserId());
		supplyRequestRepo.save(supplyRequest);
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void removeSupplyRequest(String orderNumber) throws CannotBeRemovedException, CannotBeFoundException {
		SupplyRequest supplyRequest = supplyRequestRepo.findByRequestNumber(orderNumber);
		validateRemovalOfSupplyRequest(supplyRequest);
		supplyRequestRepo.delete(supplyRequest);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void submitSupplyRequest(SupplyRequestCO commandObject) throws ValidationException {
		
		validateSupplyRequestSubmit(commandObject);
		
		Optional.object(supplyRequestRepo.findByRequestNumber(commandObject.getRequestNumber()))
			.ifNotPresent(() -> {
				return new SupplyRequest();
			})
			.then(or -> {
				cleanSupplyRequestItems(or);
				SupplyRequest supplyRequest = 
						detailsConverter.convert(SUBMIT_COMMAND, commandObject, or);
				supplyRequest.setStatus(SupplyRequestStatus.SUBMITTED.toString());
				supplyRequest.setRequestById(authBean.getAuthenticatedUserId());
				
				supplyRequestRepo.save(supplyRequest);
			}, null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void approveSupplyRequest(SupplyRequestCO commandObject) throws ValidationException {
		
		validateSupplyRequestApproval(commandObject, APPROVE_COMMAND);
		
		SupplyRequest toBeSubmittedSupplyRequest = 
				supplyRequestRepo.findByRequestNumber(commandObject.getRequestNumber());
		cleanSupplyRequestItems(toBeSubmittedSupplyRequest);
		SupplyRequest supplyRequest = 
				detailsConverter.convert(SUBMIT_COMMAND, commandObject, toBeSubmittedSupplyRequest);
		supplyRequest.setStatus(SupplyRequestStatus.APPROVED.toString());
		
		supplyRequestRepo.save(supplyRequest);
		
		persistORApprovalDetails(supplyRequest, commandObject.getApprover());
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void rejectSupplyRequest(SupplyRequestCO commandObject) throws ValidationException {
		validateSupplyRequestApproval(commandObject, REJECT_COMMAND);
		SupplyRequest toBeRejectedSupplyRequest = 
				supplyRequestRepo.findByRequestNumber(commandObject.getRequestNumber());
		
		toBeRejectedSupplyRequest.setStatus(SupplyRequestStatus.REJECTED.toString());
		supplyRequestRepo.save(toBeRejectedSupplyRequest);
		
		persistORApprovalDetails(toBeRejectedSupplyRequest, commandObject.getApprover());
	}

	@Override
	@Transactional(readOnly = true)
	public SearchList findAllSupplyRequests(SupplyRequestLQO supplyRequestListQueryObject) {
		SearchListValueObject itemSearchList = supplyRequestRepo.findAll(supplyRequestListQueryObject, SearchType.FILTER);
		return SearchList.generateInstance(itemSearchList, supplyRequestListQueryObject);
	}

	@Override
	@Transactional(readOnly = true)
	public SupplyRequestQRO findSupplyRequestByRequestNumber(String orderNumber) throws CannotBeFoundException{
		SupplyRequest supplyRequest = supplyRequestRepo.findByRequestNumber(orderNumber);
		if(null == supplyRequest) 
			throw new CannotBeFoundException("Cannot find OR: "+orderNumber);
		
		return readWrapper.generateQRO(supplyRequest);
	}
	
	@Override
	@Transactional(readOnly = true)
	public QueryResultObjectWrapper<SupplyRequestQRO> getSupplyRequestByRequestNumber(String orderNumber)
			throws CannotBeFoundException {
		SupplyRequest supplyRequest = supplyRequestRepo.findByRequestNumber(orderNumber);
		if(null == supplyRequest) 
			throw new CannotBeFoundException("Cannot find OR: "+orderNumber);
		
		SupplyRequestQRO qro = readWrapper.generateQRO(supplyRequest);
		QROEditableFieldsMeta editMeta = generateEditMeta(supplyRequest);
		
		return QueryResultObjectWrapper.generateInstance(qro, editMeta);
	}

	/**
	 * TODO: Since supply request are per service center and there are multiple service centers
	 * the template should accept the facility code based on what is given to the authentication
	 * response.
	 */
	@Override
	@Transactional(readOnly = true)
	public QueryResultObjectWrapper<SupplyRequestQRO> generateSupplyRequestTemplate(String facilityCode) {
		SupplyRequestQRO supplyRequestTemplate = new SupplyRequestQRO();
		//generate default order number, status
		supplyRequestTemplate.setRequestNumber(supplyRequestReferenceCodeGenerator.generateReferenceCode());
		supplyRequestTemplate.setStatus(SupplyRequestStatus.NEW.toString());
		
		//generate default recipient
		String currentUserName = authBean.getPrincipalName();
		NsisUser currentUserDetails = userRepo.findByUserName(currentUserName);
		PersonnelQRO currentUserWrappedDetails = wrapPersonnelQRO(currentUserDetails);
		RecipientQRO defaultRecipient = new RecipientQRO();
		defaultRecipient.setRecipientDetails(currentUserWrappedDetails);
		
		Facility supplyRequestFacility = facilityRepo.findByCodeName(facilityCode);
		defaultRecipient.setDeliveryAddress(supplyRequestFacility.getAddress());
		supplyRequestTemplate.setRecipient(defaultRecipient);
		
		//generate default currency, facility code
		supplyRequestTemplate.setFacilityCodeName(supplyRequestFacility.getCodeName());
		supplyRequestTemplate.setCurrency(DEFAULT_SR_CURRENCY);
		
		//set default order by
		supplyRequestTemplate.setRequestBy(currentUserWrappedDetails);
		
		QROEditableFieldsMeta editMeta = generateEditMetaForTemplate();
		
		return QueryResultObjectWrapper.generateInstance(supplyRequestTemplate, editMeta);
	}
	
	private void cleanSupplyRequestItems(SupplyRequest or){
		if(!or.isNew()){
			int noOfDeletedSupplyRequestItems = supplyRequestRepo.deleteAllSupplyRequestItems(or);
			LOGGER.info("No of deleted order request items for OR: "
				+or.getRequestNumber()
				+" is: "+noOfDeletedSupplyRequestItems);
		}
	}
	
	private void validateSupplyRequestSubmit(SupplyRequestCO commandObject) throws ValidationException {
		Map<String, String> errorMessages = new HashMap<String, String>();
		try{
			detailsValidator.validate(SUBMIT_COMMAND, commandObject);
		}catch(ValidationException e){
			errorMessages.putAll(e.getAllErrors());
		}finally{
			if(null == authBean.getAuthenticatedUserId())
				errorMessages.put("orderById", "Must be logged-in to submit OR. Try to login again.");
			if(!errorMessages.isEmpty())
				throw new ValidationException(errorMessages);
		}
	}
	
	private void validateSupplyRequestApproval(SupplyRequestCO commandObject, String actionType) throws ValidationException {
		Map<String, String> errorMessages = new HashMap<String, String>();
		try{
			detailsValidator.validate(actionType, commandObject);
		}catch(ValidationException e){
			errorMessages.putAll(e.getAllErrors());
		}finally{
			if(null == authBean.getAuthenticatedUserId())
				errorMessages.put("approver.username", "Must be logged-in to "+
						actionType.toLowerCase()+" OR. Try to login again.");
			if(!errorMessages.isEmpty())
				throw new ValidationException(errorMessages);
		}
	}
	
	private void persistORApprovalDetails(SupplyRequest supplyRequest, SupplyRequestApproverCO approvalCO){
		Optional.object(supplyRequestApprovalRepo.findBySupplyRequest(supplyRequest))
			.ifNotPresent(() -> {
				NsisUser approver = userRepo.getByKey(authBean.getAuthenticatedUserId());
				return new SupplyRequestApproval(supplyRequest, approver);
			})
			.then(
				val -> {
					SupplyRequestApproval orApprovalToBePersisted = 
							approvalConverter.convert(null, approvalCO, val);
					supplyRequestApprovalRepo.save(orApprovalToBePersisted);
				}, 
				null
			);
	}
	
	@Override
	protected QROEditableFieldsMeta generateEditMetaForTemplate() {
		ArrayList<String> exceptions = new ArrayList<String>();
		exceptions.add("requestBy.fullName");
		exceptions.add("requestNumber");
		exceptions.add("facilityCodeName");
		exceptions.add("status");
		exceptions.add("approver.approverDetails.fullName");
		exceptions.add("approver.approverDetails.userName");
		exceptions.add("approver.remarks");
		return QROEditableFieldsMeta
				.editableExceptCertainFieldsInstance(exceptions);
	}


	@Override
	protected QROEditableFieldsMeta generateEditMeta(SupplyRequest supplyRequest) {
		
		//if not in session to prevent misleading edit set to non editable
		//if in session and status is new set to editable
		//if in session and status is draft but not equal to order by id set to non editable
		//if in session and status is except submitted set to non editable
		//TODO: if in session and status is submitted and has approval and reject permission. set to editable
		LOGGER.info("supplyRequest is not null: "+(null != supplyRequest));
		QROEditableFieldsMeta returnValue  = 
			(QROEditableFieldsMeta) UsingChainable
			.thisObject(authBean.getAuthenticatedUserId())
			.deriveIfPresent(userId -> {
					if(supplyRequest.getStatus().equals(SupplyRequestStatus.DRAFT.toString()) && 
							userId.equals(supplyRequest.getRequestById())
							){
						LOGGER.info("In first condition: "+supplyRequest.getRequestById());
						return generateEditMetaForTemplate();
					}else if(
							supplyRequest.getStatus().equals(SupplyRequestStatus.NEW.toString())
							){
						LOGGER.info("In second condition");
						return generateEditMetaForTemplate();
					}else if(
							supplyRequest.getStatus().equals(SupplyRequestStatus.SUBMITTED.toString()) && 
							!userId.equals(supplyRequest.getRequestById())
							){
						LOGGER.info("In third condition");
						ArrayList<String> exceptions = new ArrayList<String>();
						exceptions.add("requestBy.fullName");
						exceptions.add("requestById");
						exceptions.add("requestNumber");
						exceptions.add("requestDate");
						exceptions.add("facilityId");
						exceptions.add("status");
						exceptions.add("approver.approverDetails.fullName");
						exceptions.add("approver.approverDetails.userName");
						return QROEditableFieldsMeta.editableExceptCertainFieldsInstance(exceptions);
					}else {
						return QROEditableFieldsMeta.noEditableInstance();
					}
			})
			.deriveIfNotPresent(() -> {
					return QROEditableFieldsMeta.noEditableInstance();
			})
			.execute();

		LOGGER.info("returnValue is not null: "+(null != returnValue)+"type is: "+(null != returnValue ? returnValue.getType() : "NA"));
		return returnValue;
	}

	private void validateRemovalOfSupplyRequest(SupplyRequest supplyRequest) throws CannotBeRemovedException, CannotBeFoundException{
		if(null == supplyRequest) 
			throw new CannotBeFoundException("Cannot find Request.");
		
		//validate if can be deleted
		String errorMessage = "";
		if(!SupplyRequestStatus.DRAFT.toString().equals(supplyRequest.getStatus())){
			errorMessage = "Cannot remove an Request that has already been submitted or processed.";
		}else if(!authBean.getAuthenticatedUserId().equals(supplyRequest.getRequestById())){
			errorMessage = "Cannot remove an Request you have not created.";
		}
		
		if(!errorMessage.isEmpty())
			throw new CannotBeRemovedException(errorMessage);
	}
}
