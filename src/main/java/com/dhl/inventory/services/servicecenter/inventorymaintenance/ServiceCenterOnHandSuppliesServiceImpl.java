package com.dhl.inventory.services.servicecenter.inventorymaintenance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.components.BatchNumberUtilityBean;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.ServiceCenterOnHandByItemCodeLQO;
import com.dhl.inventory.repository.servicecenter.inventorymaintenance.OnHandSuppliesRepository;
import com.dhl.inventory.util.DateUtil;

@Service("serviceCenterOnHandSuppliesService")
class ServiceCenterOnHandSuppliesServiceImpl implements ServiceCenterOnHandSuppliesService {
	
	private static final Logger LOGGER = Logger.getLogger(ServiceCenterOnHandSuppliesServiceImpl.class);
	
	@Autowired
	private OnHandSuppliesRepository onhandSuppliesRepo;
	
	@Autowired
	private BatchNumberUtilityBean batchNumberUtil;
	
	@Override
	@Transactional(readOnly = true)
	public SearchList findAllOnHandSupplies(ServiceCenterOnHandByItemCodeLQO onHandByItemCodeLQO){
		SearchListValueObject itemSearchList = null;
		if(onHandByItemCodeLQO.isByItemCode())
			itemSearchList = onhandSuppliesRepo.findAllOnHandSuppliesGroupedByItemCode(onHandByItemCodeLQO);
		else
			itemSearchList = findAllOnHandSuppliesByBatch(onHandByItemCodeLQO);
		
		return SearchList.generateInstance(itemSearchList, onHandByItemCodeLQO);
	}
	
	private SearchListValueObject findAllOnHandSuppliesByBatch(ServiceCenterOnHandByItemCodeLQO onHandByItemCodeLQO){
		SearchListValueObject itemSearchList = onhandSuppliesRepo.findAllOnHandSuppliesGroupedByDeliveredItemBatch(onHandByItemCodeLQO);
		List<Map<String, String>> searchedInstances = new ArrayList<>();
		itemSearchList.getSearchedInstances().stream().forEach(searchInstance -> {
			Map<String, String> modifiedSearchInstance = new HashMap<>();
			try {
				String batchNumber = batchNumberUtil.constructBatchNumber(
						searchInstance.get("itemCode"), 
						searchInstance.get("rackCode"), 
						DateUtil.parseShortDate(searchInstance.get("dateStored"))
				);
				modifiedSearchInstance.put("batchNumber", batchNumber);
				modifiedSearchInstance.put("itemCode", searchInstance.get("itemCode"));
				modifiedSearchInstance.put("description", searchInstance.get("description"));
				modifiedSearchInstance.put("uom", searchInstance.get("uom"));
				modifiedSearchInstance.put("rackLocation", searchInstance.get("rackCode"));
				modifiedSearchInstance.put("physicalStock", searchInstance.get("physicalStock"));
				modifiedSearchInstance.put("totalAmountInPHP", searchInstance.get("totalAmountInPHP"));
				modifiedSearchInstance.put("totalAmountInSGD", searchInstance.get("totalAmountInSGD"));
				searchedInstances.add(modifiedSearchInstance);
			} catch (Exception e) {
				LOGGER.error("cant parsed date stored skipping to next record");
			}
		});
		
		return new SearchListValueObject(
				searchedInstances,
				itemSearchList.getTotalCount()
		);
	}

}
