package com.dhl.inventory.services.servicecenter.supplyrequest;

import com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest.SupplyRequestCO;
import com.dhl.inventory.dto.queryobject.QueryResultObjectWrapper;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.servicecenter.supplyrequest.SupplyRequestLQO;
import com.dhl.inventory.dto.queryobject.servicecenter.supplyrequest.SupplyRequestQRO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.CannotBeRemovedException;
import com.dhl.inventory.services.exceptions.ValidationException;

public interface SupplyRequestService {
	String createNewSupplyRequest(SupplyRequestCO commandObject) throws ValidationException;
	
	void updateSupplyRequest(String requestNumber, SupplyRequestCO commandObject) throws ValidationException;
	
	void removeSupplyRequest(String requestNumber) throws CannotBeRemovedException, CannotBeFoundException;
	
	void submitSupplyRequest(SupplyRequestCO commandObject) throws ValidationException;
	
	void approveSupplyRequest(SupplyRequestCO commandObject) throws ValidationException;
	
	void rejectSupplyRequest(SupplyRequestCO commandObject) throws ValidationException;
	
	SearchList findAllSupplyRequests(SupplyRequestLQO orderRequestListQueryObject);
	
	SupplyRequestQRO findSupplyRequestByRequestNumber(String requestNumber) throws CannotBeFoundException;
	
	QueryResultObjectWrapper<SupplyRequestQRO> getSupplyRequestByRequestNumber(String requestNumber) throws CannotBeFoundException;
	
	QueryResultObjectWrapper<SupplyRequestQRO> generateSupplyRequestTemplate(String facilityCode);
}
