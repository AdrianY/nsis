package com.dhl.inventory.services.servicecenter.inventorymaintenance;

import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.PhysicalStockCountRequestCO;
import com.dhl.inventory.dto.queryobject.QueryResultObjectWrapper;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.PhysicalStockCountRequestLQO;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.PhysicalStockCountRequestQRO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.CannotBeRemovedException;
import com.dhl.inventory.services.exceptions.ValidationException;

public interface PhysicalStockCountRequestService {
	
	String createPhysicalStockCountRequest(PhysicalStockCountRequestCO physicalStockCO, String facilityCodeName) throws ValidationException;
	
	void updatePhysicalStockCountRequest(String requestNumber,PhysicalStockCountRequestCO physicalStockCO) throws ValidationException;
	
	void submitPhysicalStockCountRequest(PhysicalStockCountRequestCO physicalStockCO) throws ValidationException;

	void approvePhysicalStockCountRequest(PhysicalStockCountRequestCO physicalStockCO) throws ValidationException;
	
	void rejectPhysicalStockCountRequest(String requestNumber) throws ValidationException;
	
	void removePhysicalStockCountRequest(String requestNumber) throws CannotBeRemovedException, CannotBeFoundException;
	
	SearchList findAllPhysicalStockCountRequest(PhysicalStockCountRequestLQO listQueryObject);
	
	PhysicalStockCountRequestQRO findPhysicalStockCountRequest(String requestNumber) throws CannotBeFoundException;
	
	QueryResultObjectWrapper<PhysicalStockCountRequestQRO> getPhysicalStockCountRequest(String requestNumber) throws CannotBeFoundException;
	
	QueryResultObjectWrapper<PhysicalStockCountRequestQRO> generatePhysicalStockCountRequestTemplate(String facilityCodeName);
}
