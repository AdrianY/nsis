package com.dhl.inventory.services.servicecenter.supplyrequest;

import static com.dhl.inventory.util.ActionType.SUBMIT_COMMAND;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.converter.servicecenter.supplyrequest.SupplyRequestApprovalCOConverter;
import com.dhl.inventory.converter.servicecenter.supplyrequest.SupplyRequestCOConverter;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequestApproval;
import com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest.SupplyRequestApproverCO;
import com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest.SupplyRequestCO;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.repository.servicecenter.supplyrequest.SupplyRequestApprovalRepository;
import com.dhl.inventory.repository.servicecenter.supplyrequest.SupplyRequestReferenceCodeGenerator;
import com.dhl.inventory.repository.servicecenter.supplyrequest.SupplyRequestRepository;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.util.SupplyRequestStatus;

@Service("supplyRequestLoaderService")
class SupplyRequestLoaderServiceImpl implements SupplyRequestLoaderService {

	@Autowired 
	private SupplyRequestReferenceCodeGenerator supplyRequestReferenceCodeGenerator;
	
	@Autowired
	private SupplyRequestCOConverter detailsConverter;
	
	@Autowired 
	private SupplyRequestRepository supplyRequestRepo;
	
	@Autowired 
	private SupplyRequestApprovalRepository supplyRequestApprovalRepo;
	
	@Autowired 
	private UserRepository userRepo;
	
	@Autowired
	private SupplyRequestApprovalCOConverter approvalConverter;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public String generateDummySupplyRequest(SupplyRequestCO commandObject) {
		SupplyRequest supplyRequest = new SupplyRequest();
		supplyRequest.setRecordStatus("IN");
		
		String requestNumber = supplyRequestReferenceCodeGenerator.generateReferenceCode();
		supplyRequest.setRequestNumber(requestNumber);
		supplyRequest.setStatus(SupplyRequestStatus.APPROVED.toString());
		commandObject.setRequestNumber(requestNumber);
		SupplyRequest toBeSavedRequests = detailsConverter.convert(SUBMIT_COMMAND, commandObject, supplyRequest);
		supplyRequestRepo.save(toBeSavedRequests);
		
		persistORApprovalDetails(toBeSavedRequests, commandObject.getApprover());
		
		return requestNumber;
	}
	
	private void persistORApprovalDetails(SupplyRequest supplyRequest, SupplyRequestApproverCO approvalCO){
		Optional.object(supplyRequestApprovalRepo.findBySupplyRequest(supplyRequest))
			.ifNotPresent(() -> {
				NsisUser approver = userRepo.findByUserName("cobelen");
				return new SupplyRequestApproval(supplyRequest, approver);
			})
			.then(
				val -> {
					SupplyRequestApproval orApprovalToBePersisted = 
							approvalConverter.convert(null, approvalCO, val);
					supplyRequestApprovalRepo.save(orApprovalToBePersisted);
				}, 
				null
			);
	}

}
