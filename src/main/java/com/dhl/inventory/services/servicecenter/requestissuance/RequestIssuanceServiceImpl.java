package com.dhl.inventory.services.servicecenter.requestissuance;

import static com.dhl.inventory.util.ActionType.CREATE_COMMAND;
import static com.dhl.inventory.util.ActionType.FINALIZE_COMMAND;
import static com.dhl.inventory.util.ActionType.REJECT_COMMAND;
import static com.dhl.inventory.util.ActionType.SUBMIT_COMMAND;
import static com.dhl.inventory.util.ActionType.UPDATE_COMMAND;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.components.AuthenticationBean;
import com.dhl.inventory.converter.servicecenter.requestissuance.RequestIssuanceCOConverter;
import com.dhl.inventory.converter.servicecenter.requestissuance.RequestIssuanceDeliveryReceiptFormQROConverter;
import com.dhl.inventory.converter.servicecenter.requestissuance.RequestIssuancePickListQROConverter;
import com.dhl.inventory.converter.servicecenter.requestissuance.RequestIssuanceQROConverter;
import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.servicecenter.requestissuance.RequestIssuance;
import com.dhl.inventory.dto.commandobject.servicecenter.requestissuance.RequestIssuanceCO;
import com.dhl.inventory.dto.queryobject.QROEditableFieldsMeta;
import com.dhl.inventory.dto.queryobject.QueryResultObjectWrapper;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.RequestIssuanceDeliveryReceiptFormQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.RequestIssuanceLQO;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.RequestIssuancePickListQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.RequestIssuanceQRO;
import com.dhl.inventory.repository.maintenance.FacilityRepository;
import com.dhl.inventory.repository.servicecenter.requestissuance.RequestIssuanceRepository;
import com.dhl.inventory.services.AbstractResourceService;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.services.servicecenter.requestissuance.SuppliedItemsService;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.util.RequestIssuanceStatus;
import com.dhl.inventory.util.SearchType;
import com.dhl.inventory.util.Using;
import com.dhl.inventory.util.UsingChainable;
import com.dhl.inventory.validator.servicecenter.requestissuance.RequestIssuanceCommandValidator;

@Service("serviceCenterRequestIssuanceService")
class RequestIssuanceServiceImpl extends AbstractResourceService<RequestIssuance> implements RequestIssuanceService {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(RequestIssuanceServiceImpl.class);
	
	@Autowired
	@Qualifier("serviceCenterSuppliedItemsService")
	private SuppliedItemsService suppliedItemsService;
	
	@Autowired
	@Qualifier("serviceCenterRequestIssuanceRepository")
	private RequestIssuanceRepository requestIssuanceRepo;
	
	@Autowired
	@Qualifier("serviceCenterRequestIssuanceQROConverter")
	private RequestIssuanceQROConverter readWrapper;
	
	@Autowired
	@Qualifier("serviceCenterRequestIssuanceCOConverter")
	private RequestIssuanceCOConverter detailsConverter;
	
	@Autowired
	@Qualifier("serviceCenterRequestIssuancePickListQROConverter")
	private RequestIssuancePickListQROConverter pickListFormDataConverter;
	
	@Autowired
	@Qualifier("serviceCenterRequestIssuanceDeliveryReceiptFormQROConverter")
	private RequestIssuanceDeliveryReceiptFormQROConverter deliveryReceiptFormConverter;
	
	@Autowired
	@Qualifier("serviceCenterRequestIssuanceCommandValidator")
	private RequestIssuanceCommandValidator detailsValidator;
	
	@Autowired
	private AuthenticationBean authBean;
	
	@Autowired
	private FacilityRepository facilityRepo;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public String createNewRequestIssuance(RequestIssuanceCO requestIssuanceCO) throws ValidationException {
		detailsValidator.validate(CREATE_COMMAND, requestIssuanceCO);
		RequestIssuance newRequestIssuance = detailsConverter.convert(CREATE_COMMAND, requestIssuanceCO, new RequestIssuance());
		newRequestIssuance.setStatus(RequestIssuanceStatus.FOR_PROCESSING.toString().replace("_"," "));
		requestIssuanceRepo.save(newRequestIssuance);
		return requestIssuanceCO.getRequestNumber();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateRequestIssuance(String requestNumber, RequestIssuanceCO requestIssuanceCO)
			throws ValidationException {
		validateUpdateRequestIssuance(requestIssuanceCO);
		RequestIssuance toBeUpdatedRequestIssuance = 
				requestIssuanceRepo.findByRequestNumber(requestNumber);
		
		Optional.object(toBeUpdatedRequestIssuance).ifPresent(rI -> {
			clearExistingSuppliedItems(rI);
			if(!requestIssuanceCO.getFacilityCode().equals(requestIssuanceCO.getPreviousFacilityCode())){
				rerouteRequestIssuance(rI, requestIssuanceCO.getFacilityCode(), requestIssuanceCO.getCustodianRemarks());
				requestIssuanceRepo.save(rI);
			}else{
				RequestIssuance requestIssuance = 
						detailsConverter.convert(UPDATE_COMMAND, requestIssuanceCO, rI);
				requestIssuanceRepo.save(requestIssuance);
			}
		});
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void processRequestIssuance(RequestIssuanceCO requestIssuanceCO) throws ValidationException {
		validateProcessRequestIssuance(requestIssuanceCO);
		
		RequestIssuance toBeUpdatedRequestIssuance = 
				requestIssuanceRepo.findByRequestNumber(requestIssuanceCO.getRequestNumber());
		
		Optional.object(toBeUpdatedRequestIssuance).ifPresent(rI -> {
			
			clearExistingSuppliedItems(rI);
			
			RequestIssuance requestIssuance = 
					detailsConverter.convert(SUBMIT_COMMAND, requestIssuanceCO, rI);
			requestIssuance.setStatus(RequestIssuanceStatus.PROCESSING.toString());
			requestIssuanceRepo.save(requestIssuance);
		});
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void finalizeRequestIssuance(RequestIssuanceCO requestIssuanceCO) throws ValidationException {
		
		List<String> unsatisfiedRequestedItemCodes = 
				getUnsatisfiedRequestedItemList(requestIssuanceCO);
		
		validateFinalizeRequestIssuance(requestIssuanceCO, unsatisfiedRequestedItemCodes);
		
		RequestIssuance toBeUpdatedRequestIssuance = 
				requestIssuanceRepo.findByRequestNumber(requestIssuanceCO.getRequestNumber());
		
		if(null != toBeUpdatedRequestIssuance){
			clearExistingSuppliedItems(toBeUpdatedRequestIssuance);
			
			RequestIssuance requestIssuance = 
					detailsConverter.convert(
							FINALIZE_COMMAND, requestIssuanceCO, toBeUpdatedRequestIssuance);
			
			String status = unsatisfiedRequestedItemCodes.isEmpty() ? 
					RequestIssuanceStatus.DELIVERED.toString() :
					RequestIssuanceStatus.PARTIALLY_DELIVERED.toString().replace("_"," ");
					
			requestIssuance.setStatus(status);
				
			requestIssuanceRepo.save(requestIssuance);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void rejectRequestIssuance(RequestIssuanceCO requestIssuanceCO) throws ValidationException {
		detailsValidator.validate(REJECT_COMMAND, requestIssuanceCO);
		
		RequestIssuance toBeUpdatedRequestIssuance = 
				requestIssuanceRepo.findByRequestNumber(requestIssuanceCO.getRequestNumber());
		
		Optional.object(toBeUpdatedRequestIssuance).ifPresent(rI -> {
			
			clearExistingSuppliedItems(rI);
			
			RequestIssuance requestIssuance = 
					detailsConverter.convert(REJECT_COMMAND, requestIssuanceCO, rI);
			requestIssuance.setHandlingCustodian(null);
			requestIssuance.setRoute(null);
			requestIssuance.setIssuanceDate(null);
			requestIssuance.setStatus(RequestIssuanceStatus.REJECTED.toString());
			requestIssuanceRepo.save(requestIssuance);
		});
	}

	@Override
	@Transactional(readOnly = true)
	public QueryResultObjectWrapper<RequestIssuanceQRO> getRequestIssuanceByRequestNumber(String requestNumber)
			throws CannotBeFoundException {
		RequestIssuance requestIssuance = requestIssuanceRepo.findByRequestNumber(requestNumber);
		if(null == requestIssuance) 
			throw new CannotBeFoundException("Cannot find Request Issuance for request: "+requestNumber);
		
		RequestIssuanceQRO qro = readWrapper.generateQRO(requestIssuance);
		QROEditableFieldsMeta editMeta = generateEditMeta(requestIssuance);
		
		return QueryResultObjectWrapper.generateInstance(qro, editMeta);
	}

	@Override
	@Transactional(readOnly = true)
	public SearchList findAllRequestIssuance(RequestIssuanceLQO listQueryObject) {
		SearchListValueObject requestIssuanceSearchList = 
				requestIssuanceRepo.findAll(listQueryObject, SearchType.FILTER);
		return SearchList.generateInstance(requestIssuanceSearchList, listQueryObject);
	}

	@Override
	@Transactional(readOnly = true)
	public RequestIssuancePickListQRO getRequestIssuancePickList(String requestNumber) throws CannotBeFoundException {
		RequestIssuance requestIssuance = 
				requestIssuanceRepo.findByRequestNumber(requestNumber);
		
		if(null == requestIssuance) 
			throw new CannotBeFoundException("Cannot find Request Issuance for request: "+requestNumber);
		
		RequestIssuancePickListQRO pickListFormData = pickListFormDataConverter.generateQRO(requestIssuance);
		
		return pickListFormData;
	}
	
	@Override
	@Transactional(readOnly = true)
	public RequestIssuanceDeliveryReceiptFormQRO getRequestIssuanceDeliveryReceiptForm(String requestNumber)
			throws CannotBeFoundException {
		RequestIssuance requestIssuance = 
				requestIssuanceRepo.findByRequestNumber(requestNumber);
		
		if(null == requestIssuance) 
			throw new CannotBeFoundException("Cannot find Request Issuance for request: "+requestNumber);
		
		return deliveryReceiptFormConverter.generateQRO(requestIssuance);
	}

	@Override
	protected QROEditableFieldsMeta generateEditMeta(RequestIssuance requestIssuance) {
		QROEditableFieldsMeta returnValue  = 
				(QROEditableFieldsMeta) UsingChainable
				.thisObject(authBean.getAuthenticatedUserId())
				.deriveIfPresent(userId -> {
						String custodianId = 
								Using.thisObject(requestIssuance.getHandlingCustodian())
								.deriveIfPresent(val -> {return val.getId();});
						
						if(RequestIssuanceStatus.FOR_PROCESSING.toString().replace("_", " ").equals(requestIssuance.getStatus()) &&
								(null == custodianId || userId.equals(custodianId))){
							return generateEditMetaForTemplate();
						}else if((RequestIssuanceStatus.PROCESSING.toString()).equals(requestIssuance.getStatus()) &&
								userId.equals(custodianId)){
							return generateEditMetaForTemplate();
						}else {
							return QROEditableFieldsMeta.noEditableInstance();
						}
				})
				.deriveIfNotPresent(() -> {
						return QROEditableFieldsMeta.noEditableInstance();
				})
				.execute();

			return returnValue;
	}

	@Override
	protected QROEditableFieldsMeta generateEditMetaForTemplate() {
		ArrayList<String> exceptions = new ArrayList<String>();
		exceptions.add("requestorDetails");
		exceptions.add("requestNumber");
		exceptions.add("status");
		exceptions.add("requestDate");
		exceptions.add("requiredDate");
		exceptions.add("requestType");
		exceptions.add("recipient");
		exceptions.add("customer");
		exceptions.add("handlingCustodian.custodianDetails");
		return QROEditableFieldsMeta
				.editableExceptCertainFieldsInstance(exceptions);
	}
	
	private void validateUpdateRequestIssuance(RequestIssuanceCO requestIssuanceCO) throws ValidationException{
		Map<String, String> errorMessages = new HashMap<>();
		
		try{
			detailsValidator.validate(UPDATE_COMMAND, requestIssuanceCO);
		}catch(ValidationException e){
			errorMessages.putAll(e.getAllErrors());
		}finally {
			if(!requestIssuanceCO.getPreviousFacilityCode().equals(requestIssuanceCO.getFacilityCode())
					&& !Optional.object(requestIssuanceCO.getCustodianRemarks()).hasContent()){
				String finalErrorMsg = "Provide remarks as to "
						+ "why this request is being rerouted to Service Center "
						+ requestIssuanceCO.getFacilityCode();
				errorMessages.put("custodianRemarks", finalErrorMsg);
			}
		}
		
		if(!errorMessages.isEmpty())
			throw new ValidationException(errorMessages);
	}
	
	private void validateProcessRequestIssuance(RequestIssuanceCO requestIssuanceCO) throws ValidationException{
		Map<String, String> errorMessages = new HashMap<>();
		
		try{
			detailsValidator.validate(SUBMIT_COMMAND, requestIssuanceCO);
		}catch(ValidationException e){
			errorMessages.putAll(e.getAllErrors());
		}
		
		if(!errorMessages.isEmpty())
			throw new ValidationException(errorMessages);
	}
	
	/**
	 * should have remarks if not all requested items are satisfied
	 * 
	 * @param requestIssuanceCO
	 * @throws ValidationException
	 */
	private void validateFinalizeRequestIssuance(
			final RequestIssuanceCO requestIssuanceCO, 
			final List<String> itemsNotSatisfied
	) throws ValidationException{
		Map<String, String> errorMessages = new HashMap<>();
		try{
			detailsValidator.validate(FINALIZE_COMMAND, requestIssuanceCO);
		}catch(ValidationException e){
			errorMessages.putAll(e.getAllErrors());
		}finally {
			if(!Optional.object(requestIssuanceCO.getCustodianRemarks()).hasContent() 
					&& !itemsNotSatisfied.isEmpty()){
				String stringList = String.join(", ", itemsNotSatisfied);
				
				String finalErrorMsg = "Provide remarks as to "
						+ "why the following requested items are finalized even if not "
						+ "yet supplied with the quantity requested: " + stringList.toString();
				errorMessages.put("custodianRemarks", finalErrorMsg);
			}
		}
		
		if(!errorMessages.isEmpty())
			throw new ValidationException(errorMessages);
	}
	
	private void rerouteRequestIssuance(
			final RequestIssuance issuance, 
			final String newIssuingFacilityCode, 
			final String remarks
	){
		issuance.setCustodianRemarks(remarks);
		Facility newIssuingFacility = facilityRepo.findByCodeName(newIssuingFacilityCode);
		issuance.setIssuingFacility(newIssuingFacility);
		issuance.setRoute(null);
		issuance.setIssuanceDate(null);
		issuance.setHandlingCustodian(null);
		issuance.setStatus(RequestIssuanceStatus.FOR_PROCESSING.toString().replace("_"," "));
	}
	
	private void clearExistingSuppliedItems(final RequestIssuance requestIssuance){
		if(null != requestIssuance && !requestIssuance.isNew()){
			requestIssuanceRepo.deleteAllSupplyItemsByRequestIssuance(requestIssuance);
		}
	}
	
	private List<String> getUnsatisfiedRequestedItemList(final  RequestIssuanceCO requestIssuanceCO){
		return requestIssuanceCO.getSuppliedItemsGroups().stream().map(siGrp -> {
			return siGrp.getItemCode();
		}).collect(Collectors.toList());
	}

}
