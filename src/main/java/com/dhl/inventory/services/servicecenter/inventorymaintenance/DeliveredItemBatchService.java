package com.dhl.inventory.services.servicecenter.inventorymaintenance;

import java.util.List;
import java.util.Map;

import com.dhl.inventory.domain.servicecenter.inventorymaintenance.DeliveredItemBatch;
import com.dhl.inventory.domain.servicecenter.requestissuance.SuppliedItems;
import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.CalculateDeliveredItemBatchGroupResourceCO;
import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.DeliveredItemBatchGroupResourceCO;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.SupplyBatchSearchLQO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.ValidationException;

public interface DeliveredItemBatchService {
	List<Map<String, String>> calculateDetailsOfDeliveredItemBatches(
			CalculateDeliveredItemBatchGroupResourceCO calculateCommand, String requestNumber);

	void validateDeliveredItemBatch(DeliveredItemBatchGroupResourceCO dRItemBatchCommand, String requestNumber) throws ValidationException;

	int getRemainingPhysicalStock(String batchNumber, List<SuppliedItems> excluedSupplyItem) throws CannotBeFoundException;
	
	int getRemainingPhysicalStockForRequest(String batchNumber, String requestNumber) throws CannotBeFoundException;
	
	int getRemainingPhysicalStock(DeliveredItemBatch batchNumber, List<SuppliedItems> excluedSupplyItem);
	
	int getRemainingPhysicalStockForRequest(DeliveredItemBatch supplySource, String requestNumber);
	
	SearchList searchCurrentDeliveredItems(SupplyBatchSearchLQO listQueryObject);
}
