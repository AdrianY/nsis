package com.dhl.inventory.services.servicecenter.inventorymaintenance;

import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.DeliveryReceiptCO;
import com.dhl.inventory.dto.queryobject.QueryResultObjectWrapper;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.DeliveryReceiptLQO;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.DeliveryReceiptQRO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.CannotBeRemovedException;
import com.dhl.inventory.services.exceptions.ValidationException;

public interface DeliveryReceiptService {
	String createNewDeliveryReceipt(DeliveryReceiptCO commandObject) throws ValidationException;
	
	void updateDeliveryReceipt(String orderNumber, DeliveryReceiptCO commandObject) throws ValidationException;
	
	void removeDeliveryReceipt(String orderNumber) throws CannotBeRemovedException, CannotBeFoundException;
	
	void finalizeDeliveryReceipt(DeliveryReceiptCO commandObject) throws ValidationException;
	
	SearchList findAllDeliveryReceipt(DeliveryReceiptLQO orderRequestListQueryObject);
	
	DeliveryReceiptQRO findDeliveryReceiptByRequestNumber(String requestNumber) throws CannotBeFoundException;
	
	QueryResultObjectWrapper<DeliveryReceiptQRO> getDeliveryReceiptByRequestNumber(String requestNumber) throws CannotBeFoundException;
}
