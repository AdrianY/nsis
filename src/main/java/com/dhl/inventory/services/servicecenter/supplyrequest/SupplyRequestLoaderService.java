package com.dhl.inventory.services.servicecenter.supplyrequest;

import com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest.SupplyRequestCO;

public interface SupplyRequestLoaderService {
	String generateDummySupplyRequest(SupplyRequestCO commandObject);
}
