package com.dhl.inventory.services.servicecenter.requestissuance;

import com.dhl.inventory.dto.commandobject.servicecenter.requestissuance.SuppliedItemsGroupCO;
import com.dhl.inventory.dto.commandobject.servicecenter.requestissuance.SuppliedItemsGroupResourceCO;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.AvailableSupplyItemsGroupResourceQRO;
import com.dhl.inventory.services.exceptions.ValidationException;

public interface SuppliedItemsService {

	/**
	 * In FIFO Order
	 * 
	 * @param calculateCommand
	 * @return
	 */
	AvailableSupplyItemsGroupResourceQRO findAllSuppliedItemsGroupResource(SuppliedItemsGroupResourceCO oRItemCommand, String requestNumber, String facilityCode);

	/**
	 * get batch per supplied item using batchNumber
	 * get remaining items in that batch
	 * if remaining items is less than supplied quantity mark this as an error
	 * 
	 * 
	 * @param suppliedItemsGroupCO
	 * @throws ValidationException
	 */
	void validateSuppliedItemsGroup(SuppliedItemsGroupResourceCO oRItemCommand, String requestNumber, String facilityCode) throws ValidationException;
	
	/**
	 * checks whether the provided supply group satisfies the requested item 
	 * in the specified request
	 * @param supplyGroupCO
	 * @param requestNumber
	 * @return
	 * @throws ValidationException
	 */
	boolean hasSatisfiedRequestedItem(SuppliedItemsGroupCO supplyGroupCO, String requestNumber);
}
