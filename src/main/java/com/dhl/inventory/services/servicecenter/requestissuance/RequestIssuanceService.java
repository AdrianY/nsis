package com.dhl.inventory.services.servicecenter.requestissuance;

import com.dhl.inventory.dto.commandobject.servicecenter.requestissuance.RequestIssuanceCO;
import com.dhl.inventory.dto.queryobject.QueryResultObjectWrapper;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.RequestIssuanceLQO;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.RequestIssuancePickListQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.RequestIssuanceQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.RequestIssuanceDeliveryReceiptFormQRO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.ValidationException;

public interface RequestIssuanceService {
	
	String createNewRequestIssuance(RequestIssuanceCO requestIssuanceCO) throws ValidationException; 
	
	void updateRequestIssuance(String requestNumber, RequestIssuanceCO requestIssuanceCO) throws ValidationException;
	
	void processRequestIssuance(RequestIssuanceCO requestIssuanceCO) throws ValidationException;
	
	void finalizeRequestIssuance(RequestIssuanceCO requestIssuanceCO) throws ValidationException;
	
	QueryResultObjectWrapper<RequestIssuanceQRO> getRequestIssuanceByRequestNumber(String requestNumber) throws CannotBeFoundException;

	SearchList findAllRequestIssuance(RequestIssuanceLQO listQueryObject);

	void rejectRequestIssuance(RequestIssuanceCO requestIssuanceCO) throws ValidationException;
	
	RequestIssuancePickListQRO getRequestIssuancePickList(String requestNumber) throws CannotBeFoundException ;
	
	RequestIssuanceDeliveryReceiptFormQRO getRequestIssuanceDeliveryReceiptForm(String requestNumber) throws CannotBeFoundException;
}
