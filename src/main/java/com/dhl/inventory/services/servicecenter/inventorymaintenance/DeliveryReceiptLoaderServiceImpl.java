package com.dhl.inventory.services.servicecenter.inventorymaintenance;

import static com.dhl.inventory.util.ActionType.FINALIZE_COMMAND;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.converter.servicecenter.inventorymaintenance.DeliveryReceiptCOConverter;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.DeliveryReceipt;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;
import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.DeliveryReceiptCO;
import com.dhl.inventory.repository.servicecenter.inventorymaintenance.DeliveryReceiptRepository;
import com.dhl.inventory.repository.servicecenter.supplyrequest.SupplyRequestRepository;
import com.dhl.inventory.util.DeliveryReceiptStatus;

@Service("serviceCenterDeliveryReceiptLoaderService")
class DeliveryReceiptLoaderServiceImpl implements DeliveryReceiptLoaderService {

	private static final Logger LOGGER = Logger.getLogger(DeliveryReceiptLoaderServiceImpl.class);
	
	@Autowired
	private SupplyRequestRepository supplyRequestRepo;
	
	@Autowired
	@Qualifier(value="serviceCenterDeliveryReceiptRepo")
	private DeliveryReceiptRepository deliveryReceiptRepo;
	
	@Autowired
	@Qualifier(value="serviceCenterDeliveryReceiptCOConverter")
	private DeliveryReceiptCOConverter detailsConverter;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void generateDummyDeliveryReceipt(DeliveryReceiptCO commandObject) {
		DeliveryReceipt dummyDR = new DeliveryReceipt();
		SupplyRequest supplyRequest = 
				supplyRequestRepo.findByRequestNumber(commandObject.getRequestNumber());
		LOGGER.info("supplyRequest is null: "+(null == supplyRequest));
		dummyDR.setSupplyRequest(supplyRequest);
		dummyDR.setRecordStatus("IN");
		dummyDR.setStatus(DeliveryReceiptStatus.DELIVERED.toString());
		DeliveryReceipt deliveryReceipt = 
				detailsConverter.convert(FINALIZE_COMMAND, commandObject, dummyDR);
		deliveryReceiptRepo.save(deliveryReceipt);
	}

}
