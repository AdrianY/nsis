package com.dhl.inventory.services.servicecenter.supplyrequest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.domain.maintenance.FacilityInventoryItem;
import com.dhl.inventory.domain.maintenance.InventoryItem;
import com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest.SupplyRequestCO;
import com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest.SupplyRequestItemCO;
import com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest.SupplyRequestItemResourceCO;
import com.dhl.inventory.repository.maintenance.FacilityInventoryItemRepository;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.services.maintenance.CurrencyService;
import com.dhl.inventory.util.ActionType;
import com.dhl.inventory.util.CurrencyUtil;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.validator.servicecenter.supplyrequest.SupplyRequestItemResourceCommandValidator;

@Service("supplyRequestItemService")
class SupplyRequestItemServiceImpl implements SupplyRequestItemService {

	@Autowired
	private SupplyRequestItemResourceCommandValidator validator;
	
	@Autowired
	private FacilityInventoryItemRepository facilityItemRepo;
	
	@Autowired
	private CurrencyService currencyService;

	/**
	 * auto consolidate redundant requests
	 */
	@Override
	@Transactional(readOnly=true)
	public List<Map<String, String>> calculateDetailsOfRequestItems(SupplyRequestCO calculateCommand) throws ValidationException {
		
		// TODO create command validator
		
		List<Map<String, String>> calculatedRecords = new ArrayList<>();
		List<SupplyRequestItemCO> cleanRequestItems = new ArrayList<>();
		Optional.object(calculateCommand.getItemsRequested())
		.ifPresent(coItemsRequested -> {
			cleanRequestItems.addAll(cleanDuplicateItemsRequested(coItemsRequested));
		});
		
		Optional.object(cleanRequestItems)
		.ifPresent(uniqueRequestItems -> {
			for(SupplyRequestItemCO requestItem: uniqueRequestItems){
				FacilityInventoryItem facilityItem = facilityItemRepo.findByFacilityIdAndItemCode(calculateCommand.getFacilityCodeName(), requestItem.getItemCode());
				InventoryItem inventoryItem = facilityItem.getInventoryItem();
				int pieces = inventoryItem.getPiecesPerUnitOfMeasurement() * requestItem.getQuantity();
				BigDecimal requestCost = calculateRequestItemAmount(requestItem.getQuantity(), inventoryItem, calculateCommand.getCurrency());
				
				//TODO calculate total requested items among facilities && physical stock (receive - (issued + wastage))
				int logicalStock = 0;
				int physicalStock = 0;
				
				Map<String, String> record = new HashMap<>();
				record.put("codeName", inventoryItem.getCodeName());
				record.put("type", inventoryItem.getType());
				record.put("description", inventoryItem.getDescription());
				record.put("uom", inventoryItem.getUnitOfMeasurement());
				record.put("orderdQtyByUom", FormatterUtil.integerFormat(requestItem.getQuantity()));
				record.put("orderedQtyByPcs", FormatterUtil.integerFormat(pieces));
				record.put("logicalStock", FormatterUtil.integerFormat(logicalStock));
				record.put("physicalStock", FormatterUtil.integerFormat(physicalStock));
				record.put("max", FormatterUtil.integerFormat(facilityItem.getMaxValue()));
				record.put("min", FormatterUtil.integerFormat(facilityItem.getMinValue()));
				record.put("requestedItemsCost", FormatterUtil.currencyFormat(requestCost));
				record.put("remarks", requestItem.getRemarks());
				record.put("itemCost", FormatterUtil.currencyFormat(inventoryItem.getItemCost()));
				record.put("piecesPerUOM", String.valueOf(inventoryItem.getPiecesPerUnitOfMeasurement()));
				
				calculatedRecords.add(record);
			}
		});
		
		return calculatedRecords;
	}
	
	private List<SupplyRequestItemCO> cleanDuplicateItemsRequested(List<SupplyRequestItemCO> itemsRequested){
		Map<String, SupplyRequestItemCO> requestItemsRegistry = new LinkedHashMap<>();
		
		for(SupplyRequestItemCO requestedItem: itemsRequested){
			SupplyRequestItemCO registeredRequestItem = requestItemsRegistry.get(requestedItem.getItemCode());
			if(null == registeredRequestItem){
				requestItemsRegistry.put(requestedItem.getItemCode(), requestedItem);
			}else{
				int currentQty = registeredRequestItem.getQuantity();
				registeredRequestItem.setQuantity(currentQty+requestedItem.getQuantity());
			}
		}
		
		List<SupplyRequestItemCO> returnValue = new ArrayList<>();
		if(!requestItemsRegistry.isEmpty()){
			for(SupplyRequestItemCO entry:requestItemsRegistry.values()){
				returnValue.add(entry);
			}
		}
		
		return returnValue;
	}

	private BigDecimal calculateRequestItemAmount(int requestQuantity, InventoryItem inventoryItem, String orderCurrency){
		BigDecimal effectiveRate = currencyService.convertToCurrency(inventoryItem.getCurrency().getCode(), orderCurrency, inventoryItem.getItemCost());
		BigDecimal requestItemAmount = CurrencyUtil.multiplyValuesScaleAtTwo(effectiveRate, new BigDecimal(String.valueOf(requestQuantity)));
		return requestItemAmount;
	}

	@Override
	@Transactional(readOnly=true)
	public void validateSupplyRequestItem(SupplyRequestItemResourceCO sRItemCommand)
			throws ValidationException {
		
		validator.validate(ActionType.VALIDATE_COMMAND, sRItemCommand);
		
		FacilityInventoryItem facilityItem = 
				facilityItemRepo.findByFacilityIdAndItemCode(
						sRItemCommand.getFacilityCodeName(), sRItemCommand.getDetails().getItemCode());
		
		int minQty = facilityItem.getMinValue();
		int maxQty = facilityItem.getMaxValue();
		int requestItemQty = sRItemCommand.getDetails().getQuantity();
		String remarks = sRItemCommand.getDetails().getRemarks();
		boolean remarksIsEmpty = null == remarks || remarks.isEmpty();
		
		if(remarksIsEmpty){
			Map<String, String> errorMap = new HashMap<>();
			if(requestItemQty < minQty){
				errorMap.put("details.remarks","Provide explanation as to why quantity is less than minimum");
			}else if(requestItemQty > maxQty){
				errorMap.put("details.remarks","Provide explanation as to why quantity is more than maximum");
			}
			
			if(!errorMap.isEmpty())
				throw new ValidationException(errorMap);
		}
	}
}
