package com.dhl.inventory.services.servicecenter.inventorymaintenance;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.domain.maintenance.InventoryItem;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.DeliveredItemBatch;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;
import com.dhl.inventory.domain.servicecenter.requestissuance.SuppliedItems;
import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.CalculateDeliveredItemBatchGroupResourceCO;
import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.DeliveredItemBatchCO;
import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.DeliveredItemBatchGroupResourceCO;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.SupplyBatchSearchLQO;
import com.dhl.inventory.repository.maintenance.InventoryItemRepository;
import com.dhl.inventory.repository.servicecenter.supplyrequest.SupplyRequestRepository;
import com.dhl.inventory.repository.servicecenter.requestissuance.SuppliedItemsRepository;
import com.dhl.inventory.repository.servicecenter.inventorymaintenance.DeliveredItemBatchRepository;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.services.maintenance.CurrencyService;
import com.dhl.inventory.util.ActionType;
import com.dhl.inventory.util.CurrencyUtil;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.util.Using;
import com.dhl.inventory.validator.servicecenter.inventorymaintenance.DeliveredItemBatchResourceCOValidator;

@Service("serviceCenterDeliveredItemBatchService")
class DeliveredItemBatchServiceImpl implements DeliveredItemBatchService {

	@Autowired
	@Qualifier("serviceCenterDeliveredItemBatchResourceCOValidator")
	private DeliveredItemBatchResourceCOValidator validator;
	
	@Autowired
	private SupplyRequestRepository supplyRequestRepo;
	
	@Autowired
	private InventoryItemRepository itemRepo;
	
	@Autowired
	private CurrencyService currencyService;
	
	@Autowired
	@Qualifier("serviceCenterDeliveredItemBatchRepository")
	private DeliveredItemBatchRepository deliveredItemBatchRepo;
	
	@Autowired
	@Qualifier("serviceCenterSuppliedItemsRepository")
	private SuppliedItemsRepository suppliedItemsRepo;
	
	@Override
	@Transactional(readOnly=true)
	public void validateDeliveredItemBatch(DeliveredItemBatchGroupResourceCO dRItemBatchGroupCommand, String requestNumber) throws ValidationException {
		validator.validate(ActionType.VALIDATE_COMMAND, dRItemBatchGroupCommand);
		
		//get order item then ensure quantity is not greater than receivable;
		Map<String, String> errorMap = new HashMap<>();
		SupplyRequest supplyRequest = supplyRequestRepo.findByRequestNumber(requestNumber);
		InventoryItem inventoryItem = itemRepo.findByCodeName(dRItemBatchGroupCommand.getItemCode());
		supplyRequest.getRequestedItems().stream().filter(val -> {
			return dRItemBatchGroupCommand.getItemCode().equals(
					val.getItem().getInventoryItem().getCodeName());
		}).findFirst().ifPresent(supplyRequestItem -> {
			DeliveredItemBatchCO itemBatchToBeValidated = dRItemBatchGroupCommand.getEditedItemBatch();
			Integer quantity = Using.thisObject(dRItemBatchGroupCommand.getOtherItemBatches()).deriveIfPresent(itemBatches -> {
				return itemBatches.stream().mapToInt(i -> i.getQuantity()).sum();
			});
			int rawQuantity = quantity == null ? 0 : quantity.intValue();
			int totalQuantity = rawQuantity + itemBatchToBeValidated.getQuantity();
			
			if((supplyRequestItem.getQuantityByUOM() * inventoryItem.getPiecesPerUnitOfMeasurement()) < totalQuantity){
				errorMap.put("editedItemBatch.quantity","Total quantity cannot be greater than requested quantity");
			}
		});
		
		if(!errorMap.isEmpty())
			throw new ValidationException(errorMap);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Map<String, String>> calculateDetailsOfDeliveredItemBatches(CalculateDeliveredItemBatchGroupResourceCO calculateCommand, String requestNumber){
		
		SupplyRequest supplyRequest = supplyRequestRepo.findByRequestNumber(requestNumber);
		InventoryItem inventoryItem = itemRepo.findByCodeName(calculateCommand.getItemCode());
		String orderCurrency = supplyRequest.getCurrency();
		
		List<Map<String, String>> calculatedRecords = new ArrayList<>();
		
		List<DeliveredItemBatchCO> uniqueItemBatchess = new ArrayList<>();
		Optional.object(calculateCommand.getOtherItemBatches()).ifPresent(itemBatches -> {
			uniqueItemBatchess.addAll(cleanDuplicatedItemBatches(itemBatches));
		});
		
		Optional.object(uniqueItemBatchess).ifPresent(uniqueItemBatches -> {
			uniqueItemBatches.stream().forEach(itemBatch -> {
				int receivedQty = itemBatch.getQuantity();
				int receivedQtyByUOM = receivedQty / inventoryItem.getPiecesPerUnitOfMeasurement();
				BigDecimal equivalentAmount = calculateRequestItemAmount(receivedQtyByUOM, inventoryItem, orderCurrency);
				
				Map<String, String> record = new HashMap<>();
				record.put("batchNumber", itemBatch.getBatchNumber());
				record.put("rackingCode", itemBatch.getRackingCode());
				record.put("dateStored", itemBatch.getDateStored());
				record.put("quantity", FormatterUtil.integerFormat(receivedQty));
				record.put("quantityByUOM", String.valueOf(receivedQtyByUOM));
				record.put("amount", FormatterUtil.currencyFormat(equivalentAmount));
				
				calculatedRecords.add(record);
			});
		});
		
		return calculatedRecords;
	}
	
	private List<DeliveredItemBatchCO> cleanDuplicatedItemBatches (List<DeliveredItemBatchCO> itemBatches){
		Map<String, DeliveredItemBatchCO> itemBatchRegistry = new HashMap<>();
		
		itemBatches.stream().forEach(itemBatch -> {
			DeliveredItemBatchCO registeredItemBatch = itemBatchRegistry.get(itemBatch.getBatchNumber());
			Optional.object(registeredItemBatch).then(val -> {
				int currentReceivedQty = itemBatch.getQuantity();
				registeredItemBatch.setQuantity(currentReceivedQty + val.getQuantity());
			}, () -> {
				itemBatchRegistry.put(itemBatch.getBatchNumber(), itemBatch);
			});
		});
		
		List<DeliveredItemBatchCO> returnValue = new ArrayList<>();
		Optional.object(itemBatchRegistry).ifPresent(registry -> {
			for(DeliveredItemBatchCO itemBatch: registry.values()){
				returnValue.add(itemBatch);
			}
		});
		
		return returnValue;
	}
	
	private BigDecimal calculateRequestItemAmount(int receivedQuantity, InventoryItem inventoryItem, String orderCurrency){
		BigDecimal effectiveRate = currencyService.convertToCurrency(inventoryItem.getCurrency().getCode(), orderCurrency, inventoryItem.getItemCost());
		BigDecimal requestItemAmount = CurrencyUtil.multiplyValuesScaleAtTwo(effectiveRate, new BigDecimal(String.valueOf(receivedQuantity)));
		return requestItemAmount;
	}

	/**
	 * TODO: Must also factor in disposal records for each item batch
	 * Provide aggregated version if performance issues are noticed in request issuance menu
	 */
	@Override
	@Transactional(readOnly=true)
	public int getRemainingPhysicalStock(
			String batchNumber, 
			List<SuppliedItems> excluedSupplyItem) throws CannotBeFoundException{
		DeliveredItemBatch supplySource = 
				deliveredItemBatchRepo.findByBatchNumber(batchNumber);
		
		if(null == supplySource)
			throw new CannotBeFoundException("Delivered Item Batch "+batchNumber+" cannot be found.");
		
		return getRemainingPhysicalStock(supplySource, excluedSupplyItem);
	}
	
	@Override
	@Transactional(readOnly=true)
	public int getRemainingPhysicalStock(
			DeliveredItemBatch supplySource, 
			List<SuppliedItems> excluedSupplyItem){
		List<SuppliedItems> issuedItemsUsingSupplySource = 
				suppliedItemsRepo.findOtherSuppliedItemsUsingDeliveredItemBatch(
						supplySource,excluedSupplyItem);
		int totalIssuedItems = issuedItemsUsingSupplySource
				.stream().mapToInt(val -> val.getQuantity()).sum();
		
		return supplySource.getQuantity() - totalIssuedItems;
	}

	@Override
	@Transactional(readOnly=true)
	public int getRemainingPhysicalStockForRequest(String batchNumber, String requestNumber)
			throws CannotBeFoundException {
		DeliveredItemBatch supplySource = 
				deliveredItemBatchRepo.findByBatchNumber(batchNumber);
		
		if(null == supplySource)
			throw new CannotBeFoundException("Delivered Item Batch "+batchNumber+" cannot be found.");
		
		return getRemainingPhysicalStockForRequest(supplySource, requestNumber);
	}

	@Override
	@Transactional(readOnly=true)
	public int getRemainingPhysicalStockForRequest(DeliveredItemBatch supplySource, String requestNumber){
		List<SuppliedItems> issuedItemsUsingSupplySource = 
				suppliedItemsRepo.findAllSuppliedItemsUsingDeliveredItemBatchForRequest(supplySource, requestNumber);
		int totalIssuedItems = issuedItemsUsingSupplySource.stream().mapToInt(val -> val.getQuantity()).sum();
		
		return supplySource.getQuantity() - totalIssuedItems;
	}

	@Override
	public SearchList searchCurrentDeliveredItems(SupplyBatchSearchLQO listQueryObject) {
		// TODO Auto-generated method stub
		return null;
	}
}
