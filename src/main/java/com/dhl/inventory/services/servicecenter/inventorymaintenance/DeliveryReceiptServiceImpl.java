package com.dhl.inventory.services.servicecenter.inventorymaintenance;

import static com.dhl.inventory.util.ActionType.CREATE_COMMAND;
import static com.dhl.inventory.util.ActionType.FINALIZE_COMMAND;
import static com.dhl.inventory.util.ActionType.UPDATE_COMMAND;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.components.AuthenticationBean;
import com.dhl.inventory.converter.servicecenter.inventorymaintenance.DeliveryReceiptCOConverter;
import com.dhl.inventory.converter.servicecenter.inventorymaintenance.DeliveryReceiptQROConverter;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.DeliveryReceipt;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequestItem;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequestApproval;
import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.DeliveryReceiptCO;
import com.dhl.inventory.dto.queryobject.QROEditableFieldsMeta;
import com.dhl.inventory.dto.queryobject.QueryResultObjectWrapper;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.DeliveryReceiptLQO;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.DeliveryReceiptQRO;
import com.dhl.inventory.repository.servicecenter.inventorymaintenance.DeliveryReceiptRepository;
import com.dhl.inventory.repository.servicecenter.supplyrequest.SupplyRequestApprovalRepository;
import com.dhl.inventory.services.AbstractResourceService;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.CannotBeRemovedException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.util.DeliveryReceiptStatus;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.util.SearchType;
import com.dhl.inventory.util.UsingChainable;
import com.dhl.inventory.validator.servicecenter.inventorymaintenance.DeliveryReceiptCOValidator;

@Service("serviceCenterDeliveryReceiptService")
class DeliveryReceiptServiceImpl extends AbstractResourceService<DeliveryReceipt> implements DeliveryReceiptService {

	private static final Logger LOGGER = Logger.getLogger(DeliveryReceiptServiceImpl.class);
	
	@Autowired
	@Qualifier(value="serviceCenterDeliveryReceiptRepo")
	private DeliveryReceiptRepository deliveryReceiptRepo;
	
	@Autowired
	@Qualifier(value="serviceCenterDeliveryReceiptCOValidator")
	private DeliveryReceiptCOValidator detailsValidator;
	
	@Autowired
	@Qualifier(value="serviceCenterDeliveryReceiptCOConverter")
	private DeliveryReceiptCOConverter detailsConverter;
	
	@Autowired
	private DeliveryReceiptQROConverter readWrapper;
	
	@Autowired
	private AuthenticationBean authBean;
	
	@Autowired 
	private SupplyRequestApprovalRepository supplyRequestApprovalRepo;
	

	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public String createNewDeliveryReceipt(DeliveryReceiptCO commandObject) throws ValidationException {
		//TODO create validator. Might not need to since this is called once OR is approved.
		DeliveryReceipt deliveryReceipt = 
				detailsConverter.convert(CREATE_COMMAND, commandObject, new DeliveryReceipt());
		deliveryReceipt.setStatus(DeliveryReceiptStatus.FOR_DELIVERY.toString().replace("_"," "));
		deliveryReceiptRepo.save(deliveryReceipt);
		return deliveryReceipt.getSupplyRequest().getRequestNumber();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateDeliveryReceipt(String requestNumber, DeliveryReceiptCO commandObject) throws ValidationException {
		detailsValidator.validate(UPDATE_COMMAND, commandObject);
		DeliveryReceipt deliveryReceiptToBeUpdated = 
				deliveryReceiptRepo.findByRequestNumber(commandObject.getRequestNumber());
		cleanDeliveredItemBatches(deliveryReceiptToBeUpdated);
		DeliveryReceipt deliveryReceipt = 
				detailsConverter.convert(UPDATE_COMMAND, commandObject, deliveryReceiptToBeUpdated);
		deliveryReceiptRepo.save(deliveryReceipt);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void removeDeliveryReceipt(String requestNumber) throws CannotBeRemovedException, CannotBeFoundException {
		// TODO Auto-generated method stub. No use
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void finalizeDeliveryReceipt(DeliveryReceiptCO commandObject) throws ValidationException {
		detailsValidator.validate(FINALIZE_COMMAND, commandObject);
		
		DeliveryReceipt deliveryReceiptToBeUpdated = 
				deliveryReceiptRepo.findByRequestNumber(commandObject.getRequestNumber());
		
		cleanDeliveredItemBatches(deliveryReceiptToBeUpdated);
		if(isPartialDeliveryReceipt(commandObject, deliveryReceiptToBeUpdated))
			deliveryReceiptToBeUpdated.setStatus(
					DeliveryReceiptStatus.PARTIALLY_DELIVERED.toString().replace("_"," "));
		else
			deliveryReceiptToBeUpdated.setStatus(DeliveryReceiptStatus.DELIVERED.toString());
		
		DeliveryReceipt deliveryReceipt = 
				detailsConverter.convert(FINALIZE_COMMAND, commandObject, deliveryReceiptToBeUpdated);
		
		deliveryReceiptRepo.save(deliveryReceipt);
	}

	@Override
	@Transactional(readOnly = true)
	public SearchList findAllDeliveryReceipt(DeliveryReceiptLQO deliveryReceiptListQueryObject) {
		SearchListValueObject itemSearchList = deliveryReceiptRepo.findAll(deliveryReceiptListQueryObject, SearchType.FILTER);
		return SearchList.generateInstance(itemSearchList, deliveryReceiptListQueryObject);
	}

	@Override
	@Transactional(readOnly = true)
	public DeliveryReceiptQRO findDeliveryReceiptByRequestNumber(String requestNumber) throws CannotBeFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public QueryResultObjectWrapper<DeliveryReceiptQRO> getDeliveryReceiptByRequestNumber(String requestNumber)
			throws CannotBeFoundException {
		DeliveryReceipt deliveryReceipt = deliveryReceiptRepo.findByRequestNumber(requestNumber);
		if(null == deliveryReceipt) 
			throw new CannotBeFoundException("Cannot find OR: "+requestNumber);
		
		DeliveryReceiptQRO qro = readWrapper.generateQRO(deliveryReceipt);
		QROEditableFieldsMeta editMeta = generateEditMeta(deliveryReceipt);
		
		return QueryResultObjectWrapper.generateInstance(qro, editMeta);
	}

	/**
	 * based on QRO
	 */
	@Override
	protected QROEditableFieldsMeta generateEditMeta(DeliveryReceipt deliveryReceipt) {
		LOGGER.info("deliveryReceipt is not null: "+(null != deliveryReceipt));
		
		/**
		 * 1. FOR Delivery - can only edit delivery date and delivery batch items
		 * 2. DELIVERED - No editable instance
		 * 3. Approver cannot edit instance;
		 */
		QROEditableFieldsMeta returnValue  = 
				(QROEditableFieldsMeta) UsingChainable
				.thisObject(authBean.getAuthenticatedUserId())
				.deriveIfPresent(userId -> {
						SupplyRequestApproval orApproval = 
								supplyRequestApprovalRepo.findBySupplyRequest(deliveryReceipt.getSupplyRequest());
						String status = deliveryReceipt.getStatus();
						if(status.equals(DeliveryReceiptStatus.FOR_DELIVERY.toString().replace("_"," ")) &&
								!userId.equals(orApproval.getApprover().getId())){
							ArrayList<String> specificFields = new ArrayList<String>();
							specificFields.add("dateDelivered");
							specificFields.add("itemBatches");
							return QROEditableFieldsMeta.editableSpecificFieldsInstance(specificFields);
						}else {
							return QROEditableFieldsMeta.noEditableInstance();
						}
				})
				.deriveIfNotPresent(() -> {
						return QROEditableFieldsMeta.noEditableInstance();
				})
				.execute();

		return returnValue;
	}

	@Override
	protected QROEditableFieldsMeta generateEditMetaForTemplate() {
		// TODO Auto-generated method stub
		return null;
	}
	
	private boolean isPartialDeliveryReceipt(DeliveryReceiptCO commandObject, DeliveryReceipt deliveryReceipt){

		Map<String, Boolean> booleanMap = new HashMap<>();
		booleanMap.put("isPartialDeliveryReceipt", false);
		SupplyRequest supplyRequest = deliveryReceipt.getSupplyRequest();
		
		commandObject.getItemBatchGroups().stream().forEach(itemBatchGroup -> {
			if(!booleanMap.get("isPartialDeliveryReceipt")){
				Optional.object(itemBatchGroup.getItemBatches()).ifNotPresent(() -> {
					booleanMap.put("isPartialDeliveryReceipt", true);
				});
				
				if(!booleanMap.get("isPartialDeliveryReceipt")){
					java.util.Optional<SupplyRequestItem> supplyRequestItem = supplyRequest.getRequestedItems().stream().filter(val -> {
						String itemCode = val.getItem().getInventoryItem().getCodeName();
						return itemCode.equals(itemBatchGroup.getItemCode());
					}).findFirst();
					
					if(!booleanMap.get("isPartialDeliveryReceipt")){
						supplyRequestItem.ifPresent(ori -> {
							Optional.object(itemBatchGroup.getItemBatches()).ifPresent(itemBatches -> {
								int toBeSatisfiedQty = ori.getQuantityByUOM();	
								int totalDeliveredItems = itemBatches.stream().mapToInt(i -> i.getQuantity()).sum();
								int yetToBeSatisfiedItem = toBeSatisfiedQty - totalDeliveredItems;
								if(yetToBeSatisfiedItem > 0){
									booleanMap.put("isPartialDeliveryReceipt", true);
								}
							});
						});
					}
				}
			}
			
		});
		
		return booleanMap.get("isPartialDeliveryReceipt");
	}

	/**
	 * Second level validation when finalizing delivery receipt.
	 * Ensures that all receivable items are accounted for.
	 * 
	 * @param commandObject
	 * @param deliveryReceipt
	 * @throws ValidationException
	 * @deprecated
	 */
	private void validateToBeFinalizedDeliveryReceipt(DeliveryReceiptCO commandObject, DeliveryReceipt deliveryReceipt) throws ValidationException{
		
		Map<String, String> errorMessages = new HashMap<>();
		List<String> errorMessagesForReceivables = new ArrayList<>();
		SupplyRequest supplyRequest = deliveryReceipt.getSupplyRequest();
		commandObject.getItemBatchGroups().stream().forEach(itemBatchGroup -> {
			Optional.object(itemBatchGroup.getItemBatches()).ifNotPresent(() -> {
				String errorMessage = "Ordered Item: " + itemBatchGroup.getItemCode() + " has no delivered items yet.";
				errorMessagesForReceivables.add(errorMessage);
			});
			
			java.util.Optional<SupplyRequestItem> supplyRequestItem = supplyRequest.getRequestedItems().stream().filter(val -> {
				String itemCode = val.getItem().getInventoryItem().getCodeName();
				return itemCode.equals(itemBatchGroup.getItemCode());
			}).findFirst();
			
			supplyRequestItem.ifPresent(ori -> {
				Optional.object(itemBatchGroup.getItemBatches()).ifPresent(itemBatches -> {
					int toBeSatisfiedQty = ori.getQuantityByUOM();	
					int totalDeliveredItems = itemBatches.stream().mapToInt(i -> i.getQuantity()).sum();
					int yetToBeSatisfiedItem = toBeSatisfiedQty - totalDeliveredItems;
					if(yetToBeSatisfiedItem > 0){
						String errorMessage = "Ordered Item: " + 
								itemBatchGroup.getItemCode() + 
								" still has "+
								FormatterUtil.integerFormat(yetToBeSatisfiedItem) +
								" undelivered items.";
						errorMessagesForReceivables.add(errorMessage);
					}
				});
			});
		});
		
		Optional.object(errorMessagesForReceivables).ifPresent(list -> {
			StringBuilder errorMsgBuilder = new StringBuilder("Cannot finalized due to undelivered items. ");
			list.stream().forEach(message -> {
				errorMsgBuilder.append("\n").append(message);
			});
			errorMessages.put("itemBatchGroups", errorMsgBuilder.toString());
		});
		
		if(!errorMessages.isEmpty())
			throw new ValidationException(errorMessages);
	}
	
	private void cleanDeliveredItemBatches(DeliveryReceipt dr){
		if(!dr.isNew()){
			int noOfDeletedDeliveredItemBatches = deliveryReceiptRepo.deleteDeliveredItemBatches(dr);
			LOGGER.info("No of deleted DeliveredItemBatches for Delivery Receipt for order request: "
				+dr.getSupplyRequest().getRequestNumber()
				+" is: "+noOfDeletedDeliveredItemBatches);
		}
	}

}
