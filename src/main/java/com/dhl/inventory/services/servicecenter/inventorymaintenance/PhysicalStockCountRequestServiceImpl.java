package com.dhl.inventory.services.servicecenter.inventorymaintenance;

import static com.dhl.inventory.util.ActionType.CREATE_COMMAND;
import static com.dhl.inventory.util.ActionType.UPDATE_COMMAND;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.components.AuthenticationBean;
import com.dhl.inventory.converter.servicecenter.inventorymaintenance.PhysicalStockCountRequestCOConverter;
import com.dhl.inventory.converter.servicecenter.inventorymaintenance.PhysicalStockCountRequestQROConverter;
import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.PhysicalStockCountRequest;
import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.PhysicalStockCountRequestCO;
import com.dhl.inventory.dto.queryobject.PersonnelQRO;
import com.dhl.inventory.dto.queryobject.QROEditableFieldsMeta;
import com.dhl.inventory.dto.queryobject.QueryResultObjectWrapper;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.PhysicalStockCountRequestLQO;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.PhysicalStockCountRequestQRO;
import com.dhl.inventory.repository.maintenance.FacilityRepository;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.repository.servicecenter.inventorymaintenance.PhysicalStockCountRequestReferenceCodeGenerator;
import com.dhl.inventory.repository.servicecenter.inventorymaintenance.PhysicalStockCountRequestRepository;
import com.dhl.inventory.services.AbstractResourceService;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.CannotBeRemovedException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.util.PhysicalStockCountRequestStatus;
import com.dhl.inventory.util.SearchType;
import com.dhl.inventory.validator.servicecenter.inventorymaintenance.PhysicalStockCountRequestCommandValidator;

@Service("serviceCenterPhysicalStockCountRequestService")
class PhysicalStockCountRequestServiceImpl extends AbstractResourceService<PhysicalStockCountRequest>
		implements PhysicalStockCountRequestService {
	
	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(PhysicalStockCountRequestServiceImpl.class);
	
	@Autowired
	@Qualifier("serviceCenterPhysicalStockCountRequestReferenceCodeGenerator")
	private PhysicalStockCountRequestReferenceCodeGenerator referenceCodeGenerator;
	
	@Autowired
	@Qualifier("serviceCenterPhysicalStockCountRequestCommandValidator")
	private PhysicalStockCountRequestCommandValidator detailsValidator;
	
	@Autowired
	@Qualifier("serviceCenterPhysicalStockCountRequestCOConverter")
	private PhysicalStockCountRequestCOConverter detailsConverter;
	
	@Autowired
	@Qualifier("serviceCenterPhysicalStockCountRequestQROConverter")
	private PhysicalStockCountRequestQROConverter readWrapper;
	
	@Autowired
	@Qualifier("serviceCenterPhysicalStockCountRequestRepository")
	private PhysicalStockCountRequestRepository physicalStockCountRequestRepo;
	
	@Autowired
	private FacilityRepository facilityRepo;
	
	@Autowired 
	private UserRepository userRepo;
	
	@Autowired
	private AuthenticationBean authBean;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public String createPhysicalStockCountRequest(PhysicalStockCountRequestCO physicalStockCO, String facilityCodeName)
			throws ValidationException {
		detailsValidator.validate(CREATE_COMMAND, physicalStockCO);
		PhysicalStockCountRequest physicalStockCountRequest = detailsConverter.convert(CREATE_COMMAND, physicalStockCO, new PhysicalStockCountRequest());
		physicalStockCountRequest.setStatus(PhysicalStockCountRequestStatus.DRAFT.toString());
		physicalStockCountRequest.setRequestBy(getCurrentUser());
		physicalStockCountRequest.setForFacility(
				facilityRepo.findByCodeName(facilityCodeName));
		physicalStockCountRequestRepo.save(physicalStockCountRequest);
		return physicalStockCountRequest.getRequestNumber();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void submitPhysicalStockCountRequest(PhysicalStockCountRequestCO physicalStockCO)
			throws ValidationException {
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void approvePhysicalStockCountRequest(PhysicalStockCountRequestCO physicalStockCO)
			throws ValidationException {
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void rejectPhysicalStockCountRequest(String requestNumber) throws ValidationException {
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void removePhysicalStockCountRequest(String requestNumber)
			throws CannotBeRemovedException, CannotBeFoundException {
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional(readOnly = true)
	public SearchList findAllPhysicalStockCountRequest(PhysicalStockCountRequestLQO listQueryObject) {
		SearchListValueObject itemSearchList = 
				physicalStockCountRequestRepo.findAll(listQueryObject, SearchType.FILTER);
		return SearchList.generateInstance(itemSearchList, listQueryObject);
	}

	@Override
	@Transactional(readOnly = true)
	public PhysicalStockCountRequestQRO findPhysicalStockCountRequest(String requestNumber) throws CannotBeFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public QueryResultObjectWrapper<PhysicalStockCountRequestQRO> getPhysicalStockCountRequest(String requestNumber)
			throws CannotBeFoundException {
		PhysicalStockCountRequest physicalStockCountRequest = 
				physicalStockCountRequestRepo.findByRequestNumber(requestNumber);
		if(null == physicalStockCountRequest) 
			throw new CannotBeFoundException("Cannot find Physical Stock Count Request: "+requestNumber);
		
		PhysicalStockCountRequestQRO qro = readWrapper.generateQRO(physicalStockCountRequest);
		QROEditableFieldsMeta editMeta = generateEditMeta(physicalStockCountRequest);
		
		return QueryResultObjectWrapper.generateInstance(qro, editMeta);
	}

	@Override
	protected QROEditableFieldsMeta generateEditMeta(PhysicalStockCountRequest nsisDomainEntity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected QROEditableFieldsMeta generateEditMetaForTemplate() {
		ArrayList<String> exceptions = new ArrayList<String>();
		exceptions.add("requestBy.fullName");
		exceptions.add("requestNumber");
		exceptions.add("facilityCodeName");
		exceptions.add("status");
		exceptions.add("approver.approverDetails.fullName");
		exceptions.add("approver.approverDetails.userName");
		exceptions.add("approver.remarks");
		return QROEditableFieldsMeta
				.editableExceptCertainFieldsInstance(exceptions);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updatePhysicalStockCountRequest(String requestNumber, PhysicalStockCountRequestCO physicalStockCO)
			throws ValidationException {
		detailsValidator.validate(UPDATE_COMMAND, physicalStockCO);
		PhysicalStockCountRequest toBeUpdatedPhysicalStockCountRequest = 
				physicalStockCountRequestRepo.findByRequestNumber(requestNumber);
		cleanPhysicalStockCount(toBeUpdatedPhysicalStockCountRequest);
		PhysicalStockCountRequest physicalStockCountRequest = 
				detailsConverter.convert(UPDATE_COMMAND, physicalStockCO, toBeUpdatedPhysicalStockCountRequest);
		
		physicalStockCountRequest.setRequestBy(getCurrentUser());
		physicalStockCountRequestRepo.save(physicalStockCountRequest);
	}

	@Override
	@Transactional(readOnly = true)
	public QueryResultObjectWrapper<PhysicalStockCountRequestQRO> generatePhysicalStockCountRequestTemplate(
			String facilityCodeName
	) {
		Facility facility = facilityRepo.findByCodeName(facilityCodeName);
		
		String currentUserName = authBean.getPrincipalName();
		NsisUser currentUserDetails = userRepo.findByUserName(currentUserName);
		PersonnelQRO currentUserWrappedDetails = wrapPersonnelQRO(currentUserDetails);
		
		PhysicalStockCountRequestQRO physicalStockCountRequestTemplate = new PhysicalStockCountRequestQRO()
		.setRequestNumber(referenceCodeGenerator.generateReferenceCode())
		.setStatus(PhysicalStockCountRequestStatus.NEW.toString())
		.setFacilityCodeName(facility.getCodeName())
		.setRequestBy(currentUserWrappedDetails);
		
		QROEditableFieldsMeta editMeta = generateEditMetaForTemplate();
		
		return QueryResultObjectWrapper.generateInstance(physicalStockCountRequestTemplate, editMeta);
	}

	private void cleanPhysicalStockCount(PhysicalStockCountRequest physicalStockCountRequest){
		if(!physicalStockCountRequest.isNew()){
			physicalStockCountRequestRepo.deleteAllPhysicalStockCounts(physicalStockCountRequest);
		}
	}
	
	private NsisUser getCurrentUser(){
		String currentUserName = authBean.getPrincipalName();
		return userRepo.findByUserName(currentUserName);
	}
}
