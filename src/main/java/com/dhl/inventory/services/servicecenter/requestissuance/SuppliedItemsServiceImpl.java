package com.dhl.inventory.services.servicecenter.requestissuance;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.domain.maintenance.InventoryItem;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.DeliveredItemBatch;
import com.dhl.inventory.domain.servicecenter.requestissuance.RequestIssuance;
import com.dhl.inventory.domain.servicecenter.requestissuance.SubmittedCustomerRequest;
import com.dhl.inventory.domain.servicecenter.requestissuance.SubmittedRequestedItem;
import com.dhl.inventory.domain.servicecenter.requestissuance.SuppliedItems;
import com.dhl.inventory.dto.commandobject.servicecenter.requestissuance.SuppliedItemsGroupCO;
import com.dhl.inventory.dto.commandobject.servicecenter.requestissuance.SuppliedItemsGroupResourceCO;
import com.dhl.inventory.dto.commandobject.servicecenter.requestissuance.SuppliedItemsCO;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.AvailableSupplyItemsGroupResourceQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.AvailableSupplyItemsResourceQRO;
import com.dhl.inventory.repository.maintenance.InventoryItemRepository;
import com.dhl.inventory.repository.servicecenter.inventorymaintenance.DeliveredItemBatchRepository;
import com.dhl.inventory.repository.servicecenter.requestissuance.RequestIssuanceRepository;
import com.dhl.inventory.repository.servicecenter.requestissuance.SubmittedCustomerRequestRepository;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.services.servicecenter.inventorymaintenance.DeliveredItemBatchService;
import com.dhl.inventory.util.CurrencyUtil;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.IntegerStream;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.util.Using;

@Service("serviceCenterSuppliedItemsService")
class SuppliedItemsServiceImpl implements SuppliedItemsService {

	private static final Logger LOGGER = Logger.getLogger(SuppliedItemsServiceImpl.class);
	
	@Autowired 
	private SubmittedCustomerRequestRepository submittedRequestsRepo;
	
	@Autowired
	@Qualifier("serviceCenterDeliveredItemBatchRepository")
	private DeliveredItemBatchRepository deliveredItemBatchRepo;
	
	@Autowired
	@Qualifier("serviceCenterDeliveredItemBatchService")
	private DeliveredItemBatchService deliveredItemBatchService;
	
	@Autowired
	private InventoryItemRepository itemRepo;
	
	@Autowired
	@Qualifier("serviceCenterRequestIssuanceRepository")
	private RequestIssuanceRepository requestIssuanceRepo;
	
	@Override
	@Transactional(readOnly=true)
	public AvailableSupplyItemsGroupResourceQRO findAllSuppliedItemsGroupResource(
			SuppliedItemsGroupResourceCO suppliedItemsGroupCO, String requestNumber, String facilityCode) {
		
		RequestIssuance requestIssuance = requestIssuanceRepo.findByRequestNumber(requestNumber);
		Set<SuppliedItems> suppliedItemsSet = getSuppliedItemSet(requestIssuance);
		
		List<DeliveredItemBatch> currentSupplySourceInFacility = 
				deliveredItemBatchRepo.findAllInFIFOFMannerByFacilityCodeAndItemCode(
						facilityCode, suppliedItemsGroupCO.getItemCode());
		
		LOGGER.info("facilityCode: "+ facilityCode + ", suppliedItemsList size: "+suppliedItemsSet.size());
		
		List<AvailableSupplyItemsResourceQRO> availableSupplies = new ArrayList<>();
		IntegerStream physicalStockTally = IntegerStream.getStream();
		int totalLogicalStock = 0;
		Optional.object(currentSupplySourceInFacility).ifPresent(supplySources -> {
			InventoryItem inventoryItem = itemRepo.findByCodeName(suppliedItemsGroupCO.getItemCode());
			CostCalculator costCalculator = new CostCalculator(inventoryItem);
			supplySources.stream()
				.forEach(supplySource -> {
					int remainingQuantity = 
							deliveredItemBatchService.getRemainingPhysicalStock(
									supplySource, new ArrayList<>(suppliedItemsSet));
					
					LOGGER.info("batchNumber: "+ supplySource.getBatchNumber() + 
							" - remaining quantity: " + remainingQuantity + 
							" - id: " + supplySource.getId());
					
					if(0 < remainingQuantity || batchUsedInSuppliedItemsSet(suppliedItemsSet, supplySource)){
						AvailableSupplyItemsResourceQRO supplySourceQRO = new AvailableSupplyItemsResourceQRO()
								.setBatchNumber(supplySource.getBatchNumber())
								.setDateDelivered(FormatterUtil.fullDateFormat(supplySource.getDateStored()))
								.setRackNumber(supplySource.getRackingLocation().getRackCode())
								.setPhysicalStock(String.valueOf(remainingQuantity));
							
						physicalStockTally.add(remainingQuantity);
						
						costCalculator.init(remainingQuantity);
						
						supplySourceQRO.setItemCost(FormatterUtil.currencyFormat(costCalculator.getConvertedItemCost()))
						.setTotalCost(FormatterUtil.currencyFormat(costCalculator.computeTotalSupplyCost()));

						availableSupplies.add(supplySourceQRO);
					}
			});
		});
		
		//TODO compute logical stock (demand for this facility item request
		AvailableSupplyItemsGroupResourceQRO returnValue = new AvailableSupplyItemsGroupResourceQRO()
				.setAvailableSupplies(availableSupplies)
				.setPhysicalStock(FormatterUtil.integerFormat(physicalStockTally.sum()))
				.setLogicalStock(FormatterUtil.integerFormat(totalLogicalStock));
		
		return returnValue;
	}
	
	@Override
	@Transactional(readOnly=true)
	public void validateSuppliedItemsGroup(
			SuppliedItemsGroupResourceCO suppliedItemsGroupCO, 
			String requestNumber,
			String facilityCodeName
	)
			throws ValidationException {
		
		SuppliedItemsCO toBeValidatedSupplyItem =
				suppliedItemsGroupCO.getToBeValidatedSupplyItem();
		
		int totalSuppliedItems = (Optional.object(suppliedItemsGroupCO.getSuppliedItems()).hasContent() ? 
				suppliedItemsGroupCO.getSuppliedItems().stream().mapToInt(val -> val.getSuppliedQuantity()).sum() :
				0) + toBeValidatedSupplyItem.getSuppliedQuantity();
		
		SubmittedCustomerRequest supplyRequest = 
				submittedRequestsRepo.findRequestbyRequestNumber(requestNumber);
				
		int requestedQty = getRequestedQty(suppliedItemsGroupCO.getItemCode(),supplyRequest);
		
		String remarks = validateMustNotExceedRequestedQty(
				totalSuppliedItems,
				requestedQty,
				suppliedItemsGroupCO.getItemCode());
		
		try {
			final int remainingQuantity = 
					getCurrentPhysicalStock(toBeValidatedSupplyItem.getSourceBatchNumber(), requestNumber);
			
			if(null == remarks){
				remarks = validateThereMustBeAvailableQty(
						remainingQuantity,
						toBeValidatedSupplyItem.getSuppliedQuantity(),
						toBeValidatedSupplyItem.getPreviousSuppliedQuantity(), 
						toBeValidatedSupplyItem.getSourceBatchNumber()
				);
			}
			
			if(null == remarks){
				remarks = validateMustNotExceedRemainingQuantity(
						toBeValidatedSupplyItem.getSuppliedQuantity(),
						toBeValidatedSupplyItem.getPreviousSuppliedQuantity(),
						remainingQuantity,
						toBeValidatedSupplyItem.getSourceBatchNumber());
			}
			
			/**
			 * DIFFICULT!!
			 * 
			 * Determine if to be validated supply items is not the last supply used
			 */
			if(null == remarks){
				int remainingUnsuppliedQty = requestedQty - totalSuppliedItems;
				remarks = validateMustProvideRemarksForNotUsingAllAvailableItems(
						toBeValidatedSupplyItem.getSuppliedQuantity(),
						remainingQuantity,
						remainingUnsuppliedQty,
						toBeValidatedSupplyItem.getRemarks());
			}
			
			if(toBeValidatedItemIsNotTheLastBatchUsed(suppliedItemsGroupCO, supplyRequest, facilityCodeName) 
					&& null == remarks){
				remarks = validateMustProvideRemarksForNotUsingAllAvailableItems(
						toBeValidatedSupplyItem.getSuppliedQuantity(),
						remainingQuantity,
						toBeValidatedSupplyItem.getRemarks());
			}
			
		} catch (CannotBeFoundException e) {
			remarks = e.getMessage();
		}
		
		if(remarks != null){
			Map<String, String> errorMessages = new HashMap<>();
			errorMessages.put(toBeValidatedSupplyItem.getSourceBatchNumber(), remarks);
			throw new ValidationException(errorMessages);
		}
			
	}
	
	private Set<SuppliedItems> getSuppliedItemSet(final RequestIssuance requestIssuance){
		Set<SuppliedItems> suppliedItemsSet = new HashSet<>();
		
		Optional.object(requestIssuance.getSuppliedItemsGroup()).ifPresent(suppliedItemGroups -> {
			suppliedItemGroups.stream().forEach(suppliedItemGroup -> {
				suppliedItemsSet.addAll(suppliedItemGroup.getSuppliedItems());
			});
		});
		
		return suppliedItemsSet;
	}
	
	private boolean batchUsedInSuppliedItemsSet(final Set<SuppliedItems> suppliedItemsSet, final DeliveredItemBatch deliveredItemBatch){
		boolean returnValue = false;
		if(Optional.object(suppliedItemsSet).hasContent()){
			java.util.Optional<SuppliedItems> suppliedItems =  suppliedItemsSet.stream().filter(suppliedItem -> {
				return deliveredItemBatch.getId().equals(suppliedItem.getSupplySource().getId());
			}).findFirst();
			
			returnValue = suppliedItems.isPresent();
		}
		return returnValue;
	}
	
	
	private String validateThereMustBeAvailableQty(
			int remainingQuantity, 
			int toBeIssuedItemsQty, 
			int previousIssuedItemsQty,
			String sourceBatchNumber
	){
		String errorMessage = null;
		int actualRemainingQuantity = remainingQuantity + (previousIssuedItemsQty - toBeIssuedItemsQty);
		
		if(0 > actualRemainingQuantity )
			errorMessage = new StringBuilder()
			.append("The remaining quantity ").append(remainingQuantity)
			.append(" in supply source batch ").append(sourceBatchNumber)
			.append(" is not enough for additional ").append(Math.abs((previousIssuedItemsQty - toBeIssuedItemsQty)))
			.append(" requested items").toString();
		
		return errorMessage;
	}
	
	private String validateMustNotExceedRemainingQuantity(
			int toBeIssuedItemsQty, 
			int previousIssuedItemsQty, 
			int remainingQuantity, 
			String sourceBatchNumber
	){
		
		int toBeAddedQuantity = toBeIssuedItemsQty - previousIssuedItemsQty;
		String errorMessage = null;
		
		LOGGER.info("toBeIssuedItemsQty@validateMustNotExceedRemainingQuantity: " + toBeIssuedItemsQty);
		LOGGER.info("remainingQuantity@validateMustNotExceedRemainingQuantity: " + remainingQuantity);
		LOGGER.info("previousIssuedItemsQty@validateMustNotExceedRemainingQuantity: " + previousIssuedItemsQty);
		
		if(remainingQuantity < toBeAddedQuantity)
			errorMessage = "Total allocation must not exceed available physical quantity for batch "+sourceBatchNumber;
		
		return errorMessage;
	}
	
	private String validateMustNotExceedRequestedQty(int toBeIssuedItemsQty, int requestedQty, String requestItemCode){
		String errorMessage = null;
		LOGGER.info("requestedQty@validateMustNotExceedRequestedQty: " + requestedQty);
		LOGGER.info("toBeIssuedItemsQty@validateMustNotExceedRequestedQty: " + toBeIssuedItemsQty);
		if(requestedQty < toBeIssuedItemsQty){
			errorMessage = "Total allocation must not exceed requested quantity for item "+requestItemCode;
		}
		return errorMessage;
	}
	
	private String validateMustProvideRemarksForNotUsingAllAvailableItems(
			int toBeIssuedItemsQty, 
			int currentSupplySourceQty, 
			String remarks
	){
		String errorMessage = Using.thisObject(remarks).deriveIfNotPresent(() -> {
			String msg = null;
			if(currentSupplySourceQty > toBeIssuedItemsQty)
				msg = "Provide remarks as to why not all available supplies from this batch are not used for the requested items.";
			
			return msg;
		});
		return errorMessage;
	}
	
	private String validateMustProvideRemarksForNotUsingAllAvailableItems(
			int toBeIssuedItemsQty, 
			int currentSupplySourceQty, 
			int remainingUnsuppliedRequestQty,
			String remarks
	){
		String errorMessage = Using.thisObject(remarks).deriveIfNotPresent(() -> {
			String msg = null;
			if(currentSupplySourceQty > toBeIssuedItemsQty 
					&& remainingUnsuppliedRequestQty > 0)
				msg = "Provide remarks as to why not all available supplies from this batch are not used for the requested items.";
			
			return msg;
		});
		return errorMessage;
	}
	
	private int getRequestedQty(String requestItemCode, SubmittedCustomerRequest supplyRequest){
		java.util.Optional<SubmittedRequestedItem> matchRequestItem = 
				supplyRequest.getRequestedItems().stream().filter(requestedItem -> {
					return requestItemCode.equals(requestedItem.getItemCode());
				}).findFirst();
		
		if(matchRequestItem.isPresent()){
			LOGGER.info("match requestItemCode: "+matchRequestItem.get().getItemCode());
		}
		return matchRequestItem.isPresent() ? matchRequestItem.get().getQuantity() : 0;
	}	
	
	private boolean toBeValidatedItemIsNotTheLastBatchUsed(
			SuppliedItemsGroupResourceCO suppliedItemsGroupCO, 
			SubmittedCustomerRequest supplyRequest,
			String facilityCodeName
	){
		DeliveredItemBatch deliveredBatchItem = 
				deliveredItemBatchRepo.findByBatchNumberAndFacilityCode(
						suppliedItemsGroupCO.getToBeValidatedSupplyItem()
						.getSourceBatchNumber(),facilityCodeName);
		
		List<DeliveredItemBatch> deliveredBatchItems = new ArrayList<>();
		
		if(null != deliveredBatchItem){
			deliveredBatchItems.add(deliveredBatchItem);
			
			Optional.object(suppliedItemsGroupCO.getSuppliedItems()).ifPresent(list -> {
				list.forEach(val -> {
					DeliveredItemBatch retrieveDeliveredBatchItem = 
							deliveredItemBatchRepo.findByBatchNumberAndFacilityCode(
									val.getSourceBatchNumber(),facilityCodeName);
					
					deliveredBatchItems.add(retrieveDeliveredBatchItem);
				});
			});
		}
		
		DeliveredItemBatch laterSupply = Using.thisObject(deliveredBatchItems).deriveIfPresent(list -> {
			list.sort((first, second) -> {
				int firstLevelComp = first.getDateStored().compareTo(second.getDateStored());
				
				int returnValue = 0;
				if(0 != firstLevelComp) returnValue = firstLevelComp;
				else {
					returnValue = first.getRackingLocation().getRackCode().compareTo(
							second.getRackingLocation().getRackCode());
				}
				
				return returnValue;
			});
			return list.stream().findFirst().get();
		});
		
		LOGGER.info("laterSupply.batchNumber: "+laterSupply.getBatchNumber());
		LOGGER.info("validated.batchNumber: "+deliveredBatchItem.getBatchNumber());
		return (null != laterSupply && !laterSupply.getBatchNumber().equals(deliveredBatchItem.getBatchNumber()));
	}

	@Override
	public boolean hasSatisfiedRequestedItem(SuppliedItemsGroupCO supplyGroupCO, String requestNumber){
		SubmittedCustomerRequest supplyRequest = submittedRequestsRepo.findRequestbyRequestNumber(requestNumber);
		
		boolean returnValue = false;
		if(null != supplyRequest){
			java.util.Optional<SubmittedRequestedItem> requestedItem = 
					supplyRequest.getRequestedItems().stream().filter(ri -> {
						return ri.getItemCode().equals(supplyGroupCO.getItemCode());
					}).findFirst();
			
			if(requestedItem.isPresent() && Optional.object(supplyGroupCO.getSuppliedItems()).hasContent()){
				int totalSuppliedQty = 
						supplyGroupCO.getSuppliedItems()
							.stream().mapToInt(si -> si.getSuppliedQuantity()).sum();
				
				returnValue = totalSuppliedQty == requestedItem.get().getQuantity();
			}
		}
			
		return returnValue;
	}
	
	private int getCurrentPhysicalStock(
			final String supplySourceBatchNumber,
			final String requestNumber
	) throws CannotBeFoundException{
		int physicalStockFromDB = 
				deliveredItemBatchService.getRemainingPhysicalStockForRequest(supplySourceBatchNumber, requestNumber);
		
		return physicalStockFromDB; 
	}
	
	private static class CostCalculator{
		
		private InventoryItem item;
		
		private BigDecimal convertedCost;
		
		private int remainingQuantity;
		
		CostCalculator(final InventoryItem item){
			this.item = item;
			this.convertedCost = item.getItemCost();
		}
		
		private void init(final int remainingQuantity){
			this.remainingQuantity = remainingQuantity;
		}
		
		private BigDecimal computeTotalSupplyCost(){
			int remainingQuantityInUOM = remainingQuantity / item.getPiecesPerUnitOfMeasurement();
			BigDecimal totalSupplySourceCost = 
					CurrencyUtil.multiplyValuesScaleAtTwo(
							convertedCost, new BigDecimal(String.valueOf(remainingQuantityInUOM)));
			return totalSupplySourceCost;
		}
		
		//TODO do convert to supply request currency
		private BigDecimal getConvertedItemCost(){
			return convertedCost;
		}
	}

	
}
