package com.dhl.inventory.services.servicecenter.inventorymaintenance;

import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.DeliveryReceiptCO;

public interface DeliveryReceiptLoaderService {
	void generateDummyDeliveryReceipt(DeliveryReceiptCO commandObject);
}
