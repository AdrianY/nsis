package com.dhl.inventory.services.maintenance;

import com.dhl.inventory.dto.commandobject.maintenance.FacilityPersonnelCO;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.maintenance.FacilityPersonnelLQO;
import com.dhl.inventory.dto.queryobject.maintenance.FacilityPersonnelQRO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.ValidationException;

public interface FacilityPersonnelService {
	
	String createFacilityPersonnel(FacilityPersonnelCO facilityPersonnelCO) throws ValidationException;

	void updateFacilityPersonnel(FacilityPersonnelCO facilityPersonnelCO, String userName, String facilityId) throws ValidationException;
	
	SearchList searchFacilityPersonnels(FacilityPersonnelLQO facilityPersonnelsLQO);

	SearchList findAllFacilityPersonnels(FacilityPersonnelLQO facilityPersonnelsLQO);

	FacilityPersonnelQRO getFacilityPersonnelByUsernameAndFacilityCode(String userName, String facilityId) throws CannotBeFoundException;

}
