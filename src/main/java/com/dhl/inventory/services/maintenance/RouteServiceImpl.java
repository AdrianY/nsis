package com.dhl.inventory.services.maintenance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.domain.maintenance.Route;
import com.dhl.inventory.dto.queryobject.QROEditableFieldsMeta;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.maintenance.RouteLQO;
import com.dhl.inventory.repository.maintenance.RouteRepository;
import com.dhl.inventory.services.AbstractResourceService;
import com.dhl.inventory.util.SearchType;

@Service("routeService")
class RouteServiceImpl extends AbstractResourceService<Route> implements RouteService {
	
	@Autowired
	private RouteRepository routeRepository;

	@Override
	@Transactional(readOnly = true)
	public SearchList findAllRoutes(RouteLQO queryObject) {
		SearchListValueObject itemSearchList = routeRepository.findAll(queryObject, SearchType.TYPE_HEAD);
		return SearchList.generateInstance(itemSearchList, queryObject);
	}

	@Override
	protected QROEditableFieldsMeta generateEditMeta(Route nsisDomainEntity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected QROEditableFieldsMeta generateEditMetaForTemplate() {
		// TODO Auto-generated method stub
		return null;
	}

}
