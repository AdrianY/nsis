package com.dhl.inventory.services.maintenance;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.maintenance.FacilityPersonnel;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.dto.commandobject.maintenance.FacilityPersonnelCO;
import com.dhl.inventory.dto.queryobject.PersonnelQRO;
import com.dhl.inventory.dto.queryobject.QROEditableFieldsMeta;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.maintenance.FacilityPersonnelLQO;
import com.dhl.inventory.dto.queryobject.maintenance.FacilityPersonnelQRO;
import com.dhl.inventory.repository.maintenance.FacilityPersonnelRepository;
import com.dhl.inventory.repository.maintenance.FacilityRepository;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.services.AbstractResourceService;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.util.SearchType;

@Service("facilityPersonnelService")
class FacilityPersonnelServiceImpl extends AbstractResourceService<FacilityPersonnel> implements FacilityPersonnelService {

	private static final Logger LOGGER = Logger.getLogger(FacilityPersonnelServiceImpl.class);
	
	@Autowired
	private FacilityRepository facilityRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private FacilityPersonnelRepository facilityPersonnelRepo;
	
	@Override
	@Transactional(readOnly = true)
	public SearchList searchFacilityPersonnels(FacilityPersonnelLQO facilityPersonnelsLQO) {
		LOGGER.info("TYPEHEAD Search");
		SearchListValueObject itemSearchList = facilityPersonnelRepo.findAll(facilityPersonnelsLQO, SearchType.TYPE_HEAD);
		return SearchList.generateInstance(itemSearchList, facilityPersonnelsLQO);
	}

	@Override
	@Transactional(readOnly = true)
	public SearchList findAllFacilityPersonnels(FacilityPersonnelLQO facilityPersonnelsLQO) {
		LOGGER.info("FILTER Search");
		SearchListValueObject itemSearchList = facilityPersonnelRepo.findAll(facilityPersonnelsLQO, SearchType.FILTER);
		return SearchList.generateInstance(itemSearchList, facilityPersonnelsLQO);
	}

	@Override
	@Transactional(readOnly = true)
	public FacilityPersonnelQRO getFacilityPersonnelByUsernameAndFacilityCode(String userName, String facilityId)
			throws CannotBeFoundException {
		FacilityPersonnel facilityPersonnel = facilityPersonnelRepo.findByUsernameAndFacilityCode(userName, facilityId);
		if(null == facilityPersonnel){
			throw new CannotBeFoundException();
		}
		PersonnelQRO userDetails = wrapPersonnelQRO(facilityPersonnel.getUserAccessDetails());
		FacilityPersonnelQRO facilityPersonnelQRO =  new FacilityPersonnelQRO()
			.setAssignedFacilityAddress(facilityPersonnel.getAssignedFacility().getAddress())
			.setFacilityId(facilityPersonnel.getAssignedFacility().getCodeName())
			.setPersonnel(userDetails);
		return facilityPersonnelQRO;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public String createFacilityPersonnel(FacilityPersonnelCO facilityPersonnelCO) throws ValidationException {
		Facility facility = facilityRepo.findByCodeName(facilityPersonnelCO.getFacilityId());
		NsisUser user = userRepo.findByUserName(facilityPersonnelCO.getUsername());
		FacilityPersonnel facilityPersonnelRecord = new FacilityPersonnel(user, facility);
		LOGGER.info("facility personnel username: "+facilityPersonnelCO.getUsername());
		facilityPersonnelRepo.save(facilityPersonnelRecord);
		return facilityPersonnelCO.getUsername();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateFacilityPersonnel(FacilityPersonnelCO facilityPersonnelCO, String userName, String facilityId)
			throws ValidationException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected QROEditableFieldsMeta generateEditMeta(FacilityPersonnel nsisDomainEntity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected QROEditableFieldsMeta generateEditMetaForTemplate() {
		// TODO Auto-generated method stub
		return null;
	}
}
