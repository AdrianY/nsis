package com.dhl.inventory.services.maintenance;

import java.math.BigDecimal;

import com.dhl.inventory.dto.commandobject.maintenance.CurrencyCO;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.maintenance.CurrencyLQO;
import com.dhl.inventory.dto.queryobject.maintenance.CurrencyQRO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.ValidationException;

public interface CurrencyService {
	
	void createNewCurrency(CurrencyCO currencyCommand) throws ValidationException;
	
	void updateCurrency(CurrencyCO currencyCommand) throws ValidationException;
	
	void removeCurrency(String codeName) throws ValidationException, CannotBeFoundException;
	
	CurrencyQRO getCurrency(String codeName) throws CannotBeFoundException;
	
	SearchList findAllICurrencies(CurrencyLQO queryObject);
	
	BigDecimal convertToCurrency(String fromCurrency, String toCurrency, BigDecimal fromCurrencyAmount);
}
