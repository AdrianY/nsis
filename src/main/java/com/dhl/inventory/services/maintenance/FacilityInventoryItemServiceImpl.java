package com.dhl.inventory.services.maintenance;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.maintenance.FacilityInventoryItem;
import com.dhl.inventory.domain.maintenance.InventoryItem;
import com.dhl.inventory.dto.commandobject.maintenance.FacilityItemCO;
import com.dhl.inventory.dto.queryobject.QROEditableFieldsMeta;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.maintenance.FacilityItemLQO;
import com.dhl.inventory.dto.queryobject.maintenance.FacilityItemQRO;
import com.dhl.inventory.repository.maintenance.FacilityInventoryItemRepository;
import com.dhl.inventory.repository.maintenance.FacilityRepository;
import com.dhl.inventory.repository.maintenance.InventoryItemRepository;
import com.dhl.inventory.services.AbstractResourceService;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.CannotBeRemovedException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.util.CurrencyUtil;
import com.dhl.inventory.util.InvalidDecimalNumberException;
import com.dhl.inventory.util.SearchType;

@Service("facilityInventoryItemService")
class FacilityInventoryItemServiceImpl extends AbstractResourceService<FacilityInventoryItem> implements FacilityInventoryItemService {

	private static final Logger LOGGER = Logger.getLogger(FacilityInventoryItemServiceImpl.class);
	
	@Autowired
	private FacilityRepository facilityRepo;
	
	@Autowired
	private InventoryItemRepository inventoryItemRepo;
	
	@Autowired
	private FacilityInventoryItemRepository facilityItemRepo;
	
	@Autowired
	@Qualifier("currencyService")
	private CurrencyService currencyService;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public String createFacilityItem(FacilityItemCO facilityItemCO) throws ValidationException {
		InventoryItem inventoryItem = inventoryItemRepo.findByCodeName(facilityItemCO.getItemCode());
		Facility facility = facilityRepo.findByCodeName(facilityItemCO.getFacilityId());
		FacilityInventoryItem facilityItem = new FacilityInventoryItem(inventoryItem, facility);
		facilityItem.setMaxValue(facilityItemCO.getMaximumValue());
		facilityItem.setMinValue(facilityItemCO.getMinimumValue());
		facilityItemRepo.save(facilityItem);
		return facilityItem.getInventoryItem().getCodeName();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateFacilityItem(FacilityItemCO facilityItemCO, String itemCode, String facilityId)
			throws ValidationException {
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional(readOnly = true)
	public FacilityItemQRO getFacilityItemByCodeAndFacility(String itemCode, String facilityId)
			throws CannotBeFoundException {
		FacilityInventoryItem facilityItem = facilityItemRepo.findByFacilityIdAndItemCode(facilityId, itemCode);
		if(null == facilityItem){
			throw new CannotBeFoundException(facilityId+"-"+itemCode);
		}
		
		InventoryItem item = facilityItem.getInventoryItem();
		FacilityItemQRO facilityItemQRO = 
				new FacilityItemQRO(
						facilityItem.getOwningFacility().getCodeName(),
						item.getCodeName()
				);
		facilityItemQRO.setDescription(item.getDescription());
		facilityItemQRO.setType(item.getType());
		facilityItemQRO.setUnitOfMeasurement(item.getUnitOfMeasurement());
		facilityItemQRO.setPiecesPerUnitOfMeasurement(item.getPiecesPerUnitOfMeasurement());
		facilityItemQRO.setMaximum(facilityItem.getMaxValue());
		facilityItemQRO.setMinimum(facilityItem.getMinValue());
		
		return facilityItemQRO;
	}

	@Override
	@Transactional(readOnly = true)
	public SearchList findAllFacilityItems(FacilityItemLQO facilityItemsLQO) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Transactional(readOnly = true)
	public SearchList searchFacilityItems(FacilityItemLQO facilityItemsLQO) {
		SearchListValueObject itemSearchList = facilityItemRepo.findAll(facilityItemsLQO, SearchType.TYPE_HEAD);
		List<Map<String,String>> searchListRecords = itemSearchList.getSearchedInstances();
		
		for(Map<String, String> record: searchListRecords){
			String itemCostRaw = record.get("itemCost");
			try {
				BigDecimal itemCost = CurrencyUtil.parseAsBigDecimal(itemCostRaw);
				BigDecimal convertedItemCost = 
						currencyService.convertToCurrency(record.get("currency"), facilityItemsLQO.getOrderCurrency(), itemCost);
				record.put("itemCost", String.valueOf(convertedItemCost));
			} catch (InvalidDecimalNumberException e) {
				LOGGER.error(e);
			}
		}
		
		return SearchList.generateInstance(itemSearchList, facilityItemsLQO);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void removeFacilityItem(String itemCode, String facilityId)
			throws CannotBeFoundException, CannotBeRemovedException {
		// TODO Auto-generated method stub

	}

	@Override
	protected QROEditableFieldsMeta generateEditMeta(FacilityInventoryItem nsisDomainEntity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected QROEditableFieldsMeta generateEditMetaForTemplate() {
		// TODO Auto-generated method stub
		return null;
	}

}
