package com.dhl.inventory.services.maintenance;

import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.maintenance.RouteLQO;

public interface RouteService {
	SearchList findAllRoutes(RouteLQO queryObject);
}
