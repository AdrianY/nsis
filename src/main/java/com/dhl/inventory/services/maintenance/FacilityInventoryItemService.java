package com.dhl.inventory.services.maintenance;

import com.dhl.inventory.dto.commandobject.maintenance.FacilityItemCO;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.maintenance.FacilityItemLQO;
import com.dhl.inventory.dto.queryobject.maintenance.FacilityItemQRO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.CannotBeRemovedException;
import com.dhl.inventory.services.exceptions.ValidationException;

public interface FacilityInventoryItemService {
	String createFacilityItem(FacilityItemCO facilityItemCO) throws ValidationException;
	
	void updateFacilityItem(FacilityItemCO facilityItemCO, String itemCode, String facilityId) throws ValidationException;
	
	FacilityItemQRO getFacilityItemByCodeAndFacility(String itemCode, String facilityId) throws CannotBeFoundException;
	
	SearchList findAllFacilityItems(FacilityItemLQO facilityItemsLQO);
	
	SearchList searchFacilityItems(FacilityItemLQO facilityItemsLQO);
	
	void removeFacilityItem(String itemCode, String facilityId) throws CannotBeFoundException, CannotBeRemovedException;
}
