package com.dhl.inventory.services.maintenance;

import com.dhl.inventory.dto.commandobject.maintenance.WarehouseLocationCO;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.maintenance.WarehouseLocationLQO;
import com.dhl.inventory.dto.queryobject.maintenance.WarehouseLocationQRO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.ValidationException;

public interface WarehouseLocationService {
	
	void createWarehouseLocation(WarehouseLocationCO warehouseLocationCO) throws ValidationException;
	
	void updateWarehouseLocation(WarehouseLocationCO warehouseLocationCO) throws ValidationException;
	
	void removeWarehouseLocation(String codeName) throws ValidationException, CannotBeFoundException;
	
	WarehouseLocationQRO findWarehouseLocation(String facilityId, String rackingCode) throws CannotBeFoundException;
	
	SearchList searchWarehouseLocations(WarehouseLocationLQO queryObject);
	
	SearchList findAllWarehouseLocations(WarehouseLocationLQO queryObject);
}
