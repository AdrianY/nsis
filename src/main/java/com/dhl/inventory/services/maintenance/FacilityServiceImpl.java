package com.dhl.inventory.services.maintenance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.repository.maintenance.FacilityRepository;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;

@Service("facilityService")
class FacilityServiceImpl implements FacilityService {
	
	@Autowired
	private FacilityRepository facilityRepo;

	@Override
	public String getFacilityType(String facilityCodeName) throws CannotBeFoundException{
		Facility facility = facilityRepo.findByCodeName(facilityCodeName);
		if(null == facility)
			throw new CannotBeFoundException("Facility "+facilityCodeName+" doesn't exist");
		
		return facility.getType();
	}

}
