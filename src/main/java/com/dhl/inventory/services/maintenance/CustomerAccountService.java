package com.dhl.inventory.services.maintenance;

import com.dhl.inventory.dto.commandobject.maintenance.CustomerAccountCO;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.maintenance.CustomerAccountLQO;
import com.dhl.inventory.dto.queryobject.maintenance.CustomerAccountQRO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.ValidationException;

public interface CustomerAccountService {
	void createCustomerAccount(CustomerAccountCO customerAccountCommand) throws ValidationException;
	
	void updateCustomerAccount(CustomerAccountCO customerAccountCommand) throws ValidationException;
	
	void removeCustomerAccount(String accountNumber) throws ValidationException, CannotBeFoundException;
	
	CustomerAccountQRO getCustomerAccount(String accountNumber) throws CannotBeFoundException;
	
	SearchList findAllCustomerAccounts(CustomerAccountLQO queryObject);
}
