package com.dhl.inventory.services.maintenance;

import java.io.InputStream;

import com.dhl.inventory.dto.commandobject.maintenance.InventoryItemCO;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.maintenance.InventoryItemLQO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.ValidationException;

public interface InventoryItemService {
	void uploadItemMasterFile(InputStream uploadedInputStream,String uploadedFileLocation);
	
	void createNewInventoryItem(InventoryItemCO itemDetailInput) throws ValidationException;
	
	void updateInventoryItem(InventoryItemCO itemDetailInput) throws ValidationException;
	
	void removeInventoryItem(String codeName) throws ValidationException, CannotBeFoundException;
	
	InventoryItemCO getInventoryItem(String codeName) throws CannotBeFoundException;
	
	SearchList findAllInventoryItems(InventoryItemLQO listQueryObject);
	
	SearchList searchInventoryItems(InventoryItemLQO listQueryObject);
}
