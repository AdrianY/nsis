package com.dhl.inventory.services.maintenance;

import java.io.InputStream;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.domain.maintenance.Currency;
import com.dhl.inventory.domain.maintenance.InventoryItem;
import com.dhl.inventory.dto.commandobject.maintenance.InventoryItemCO;
import com.dhl.inventory.dto.queryobject.QROEditableFieldsMeta;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.maintenance.InventoryItemLQO;
import com.dhl.inventory.repository.maintenance.CurrencyRepository;
import com.dhl.inventory.repository.maintenance.InventoryItemRepository;
import com.dhl.inventory.services.AbstractResourceService;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.util.CurrencyUtil;
import com.dhl.inventory.util.InvalidDecimalNumberException;
import com.dhl.inventory.util.SearchType;

@Service("inventoryItemService")
class InventoryItemServiceImpl extends AbstractResourceService<InventoryItem> implements InventoryItemService {
	
	private static final Logger LOGGER = Logger.getLogger(InventoryItemServiceImpl.class);

	@Autowired 
	private InventoryItemRepository itemDetailsRepo;
	
	@Autowired
	private CurrencyRepository currencyRepo;

	@Override
	public void uploadItemMasterFile(InputStream uploadedInputStream, String uploadedFileLocation) {
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createNewInventoryItem(InventoryItemCO inventoryItemForm) throws ValidationException {
		InventoryItem item = new InventoryItem(inventoryItemForm.getCodeName(),inventoryItemForm.getType());
		item.setDescription(inventoryItemForm.getDescription());
		item.setUnitOfMeasurement(inventoryItemForm.getUnitOfMeasurement());
		item.setPiecesPerUnitOfMeasurement(inventoryItemForm.getPiecesPerUnitOfMeasurement());
		
		Currency currency = currencyRepo.findByCurrencyCode(inventoryItemForm.getCurrency());
		item.setCurrency(currency);
		
		try {
			item.setItemCost(CurrencyUtil.parseAsBigDecimal(inventoryItemForm.getCost()));
		} catch (InvalidDecimalNumberException e) {
			LOGGER.error(e);
		}
		createNewLog(item);
		itemDetailsRepo.save(item);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateInventoryItem(InventoryItemCO inventoryItemForm) throws ValidationException {
		/*InventoryItem item = itemDetailsRepo.findByCodeName(inventoryItemForm.getCodeName());
		if(null == item){
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("Cannot update non-existing code: " + inventoryItemForm.getCodeName());
			throw new ValidationException(errorMessages);
		}else{
			item.setDescription(inventoryItemForm.getDescription());
			item.setUnitOfMeasurement(inventoryItemForm.getUnitOfMeasurement());
			item.setPiecesPerUnitOfMeasurement(inventoryItemForm.getPiecesPerUnitOfMeasurement());
			updateLog(item);
			itemDetailsRepo.save(item);
		}*/
	}

	@Override
	public void removeInventoryItem(String codeName) throws ValidationException {
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional(readOnly = true)
	public InventoryItemCO getInventoryItem(String codeName) {
		LOGGER.debug("getItemDetail: "+codeName);
		
		InventoryItem inventoryItem = itemDetailsRepo.findByCodeName(codeName);
		InventoryItemCO inventoryItemTransfer = 
				new InventoryItemCO(inventoryItem.getCodeName(), inventoryItem.getType())
				.setPiecesPerUom(inventoryItem.getPiecesPerUnitOfMeasurement())
				.setUnitOfMeasurement(inventoryItem.getUnitOfMeasurement())
				.setDescription(inventoryItem.getDescription());
		return inventoryItemTransfer;
	}

	@Override
	@Transactional(readOnly = true)
	public SearchList findAllInventoryItems(InventoryItemLQO listQueryObject) {
		SearchListValueObject itemSearchList = itemDetailsRepo.findAll(listQueryObject, SearchType.FILTER);
		return SearchList.generateInstance(itemSearchList, listQueryObject);
	}

	@Override
	protected QROEditableFieldsMeta generateEditMeta(InventoryItem nsisDomainEntity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected QROEditableFieldsMeta generateEditMetaForTemplate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchList searchInventoryItems(InventoryItemLQO listQueryObject) {
		SearchListValueObject itemSearchList = itemDetailsRepo.findAll(listQueryObject, SearchType.TYPE_HEAD);
		return SearchList.generateInstance(itemSearchList, listQueryObject);
	}

}
