package com.dhl.inventory.services.maintenance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.domain.maintenance.CustomerAccount;
import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.dto.commandobject.maintenance.CustomerAccountCO;
import com.dhl.inventory.dto.queryobject.QROEditableFieldsMeta;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.maintenance.CustomerAccountLQO;
import com.dhl.inventory.dto.queryobject.maintenance.CustomerAccountQRO;
import com.dhl.inventory.repository.maintenance.CustomerAccountRepository;
import com.dhl.inventory.repository.maintenance.FacilityRepository;
import com.dhl.inventory.services.AbstractResourceService;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.util.SearchType;

@Service("customerAccountService")
class CustomerAccountServiceImpl extends AbstractResourceService<CustomerAccount> implements CustomerAccountService {

	@Autowired
	private CustomerAccountRepository customerAccountRepo;
	
	@Autowired
	private FacilityRepository facilityRepo;
	
	/**
	 * TODO: put validator
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createCustomerAccount(CustomerAccountCO customerAccountCommand) throws ValidationException {
		CustomerAccount customerAccount = new CustomerAccount();
		customerAccount.setName(customerAccountCommand.getName());
		customerAccount.setContactName(customerAccountCommand.getEmailAddress());
		customerAccount.setContactNumber(customerAccountCommand.getContactNumber());
		customerAccount.setAccountNumber(customerAccountCommand.getAccountNumber());
		customerAccount.setDeliveryAddress(customerAccountCommand.getDeliveryAddress());
		
		Facility servicingFacility = 
				facilityRepo.findByCodeName(customerAccountCommand.getFacilityCodeName());
		customerAccount.setServicingFacility(servicingFacility);
		
		customerAccountRepo.save(customerAccount);
	}

	@Override
	public void updateCustomerAccount(CustomerAccountCO customerAccountCommand) throws ValidationException {
	}

	@Override
	public void removeCustomerAccount(String accountNumber) throws ValidationException, CannotBeFoundException {
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional(readOnly = true)
	public CustomerAccountQRO getCustomerAccount(String accountNumber) throws CannotBeFoundException {
		CustomerAccount customerAccount = customerAccountRepo.findByAccountNumber(accountNumber);
		if(null == customerAccount)
			throw new CannotBeFoundException("Customer details for account: "+accountNumber+" cannot be found.");
		
		//TODO read converter
		CustomerAccountQRO qro = new CustomerAccountQRO()
			.setContactNumber(customerAccount.getContactNumber())
			.setName(customerAccount.getName())
			.setEmailAddress(customerAccount.getContactName())
			.setAccountNumber(customerAccount.getAccountNumber());
		return qro;
	}

	@Override
	@Transactional(readOnly = true)
	public SearchList findAllCustomerAccounts(CustomerAccountLQO queryObject) {
		SearchListValueObject requestIssuanceSearchList = 
				customerAccountRepo.findAll(queryObject, SearchType.FILTER);
		return SearchList.generateInstance(requestIssuanceSearchList, queryObject);
	}

	@Override
	protected QROEditableFieldsMeta generateEditMeta(CustomerAccount nsisDomainEntity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected QROEditableFieldsMeta generateEditMetaForTemplate() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
