package com.dhl.inventory.services.maintenance;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.domain.maintenance.Currency;
import com.dhl.inventory.dto.commandobject.maintenance.CurrencyCO;
import com.dhl.inventory.dto.queryobject.QROEditableFieldsMeta;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.maintenance.CurrencyLQO;
import com.dhl.inventory.dto.queryobject.maintenance.CurrencyQRO;
import com.dhl.inventory.repository.maintenance.CurrencyRepository;
import com.dhl.inventory.services.AbstractResourceService;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.util.CurrencyUtil;
import com.dhl.inventory.util.InvalidDecimalNumberException;

@Service("currencyService")
class CurrencyServiceImpl extends AbstractResourceService<Currency> implements CurrencyService {

	private static final Logger LOGGER = Logger.getLogger(CurrencyServiceImpl.class);
	
	@Autowired
	private CurrencyRepository currencyRepo;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createNewCurrency(CurrencyCO currencyCommand) throws ValidationException {
		try {
			BigDecimal exchangeRate = CurrencyUtil.parseAsBigDecimal(currencyCommand.getExchangeRateToPhp());
			Currency currency = new Currency(currencyCommand.getCode(), exchangeRate);
			currencyRepo.save(currency);
		} catch (InvalidDecimalNumberException e) {
			LOGGER.error(e);
		}
		
	}

	@Override
	public void updateCurrency(CurrencyCO currencyCommand) throws ValidationException {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeCurrency(String codeName) throws ValidationException, CannotBeFoundException {
		// TODO Auto-generated method stub

	}

	@Override
	public CurrencyQRO getCurrency(String codeName) throws CannotBeFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchList findAllICurrencies(CurrencyLQO queryObject) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public BigDecimal convertToCurrency(String fromCurrency, String toCurrency, BigDecimal fromCurrencyAmount) {
		BigDecimal returnValue = fromCurrencyAmount;
		if(!fromCurrency.equals(toCurrency)){
		}
		return returnValue;
	}

	@Override
	protected QROEditableFieldsMeta generateEditMeta(Currency nsisDomainEntity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected QROEditableFieldsMeta generateEditMetaForTemplate() {
		// TODO Auto-generated method stub
		return null;
	}

}
