package com.dhl.inventory.services.maintenance;

import com.dhl.inventory.services.exceptions.CannotBeFoundException;

public interface FacilityService {
	String getFacilityType(String facilityCodeName) throws CannotBeFoundException;
}
