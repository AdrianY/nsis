package com.dhl.inventory.services.maintenance;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.maintenance.WarehouseLocation;
import com.dhl.inventory.dto.commandobject.maintenance.WarehouseLocationCO;
import com.dhl.inventory.dto.queryobject.QROEditableFieldsMeta;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.maintenance.WarehouseLocationLQO;
import com.dhl.inventory.dto.queryobject.maintenance.WarehouseLocationQRO;
import com.dhl.inventory.repository.maintenance.FacilityRepository;
import com.dhl.inventory.repository.maintenance.WarehouseLocationRepository;
import com.dhl.inventory.services.AbstractResourceService;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.util.SearchType;

@Service("warehouseLocationService")
class WarehouseLocationServiceImpl extends AbstractResourceService<WarehouseLocation> implements WarehouseLocationService {

	private static final Logger LOGGER = Logger.getLogger(WarehouseLocationServiceImpl.class);
	
	@Autowired
	private WarehouseLocationRepository whLocationRepo;
	
	@Autowired
	private FacilityRepository facilityRepo;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createWarehouseLocation(WarehouseLocationCO warehouseLocationCO) throws ValidationException {
		// TODO create command validator and command to entity converter
		Facility facility = facilityRepo.findByCodeName(warehouseLocationCO.getFacilityCode());
		WarehouseLocation whLocation = new WarehouseLocation(facility);
		whLocation.setRackCode(warehouseLocationCO.getRackingCode());
		whLocationRepo.save(whLocation);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateWarehouseLocation(WarehouseLocationCO warehouseLocationCO) throws ValidationException {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeWarehouseLocation(String codeName) throws ValidationException, CannotBeFoundException {
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional(readOnly = true)
	public WarehouseLocationQRO findWarehouseLocation(String facilityCode, String rackingCode)
			throws CannotBeFoundException {
		Facility facility = facilityRepo.findByCodeName(facilityCode);
		WarehouseLocation whLocation = whLocationRepo.findByFacilityAndRackingCode(facility, rackingCode);
		if(null == whLocation)
			throw new CannotBeFoundException("Warehouse Location: "+rackingCode+" for facility with code "+facility);
		
		//TODO read converter
		WarehouseLocationQRO qro = new WarehouseLocationQRO();
		qro.setFacilityId(facility.getCodeName()).setRackingCode(whLocation.getRackCode());
		return qro;
	}

	@Override
	@Transactional(readOnly = true)
	public SearchList findAllWarehouseLocations(WarehouseLocationLQO queryObject) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected QROEditableFieldsMeta generateEditMeta(WarehouseLocation nsisDomainEntity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected QROEditableFieldsMeta generateEditMetaForTemplate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public SearchList searchWarehouseLocations(WarehouseLocationLQO queryObject) {
		LOGGER.info("TYPEHEAD Search");
		SearchListValueObject itemSearchList = whLocationRepo.findAll(queryObject, SearchType.TYPE_HEAD);
		return SearchList.generateInstance(itemSearchList, queryObject);
	}

}
