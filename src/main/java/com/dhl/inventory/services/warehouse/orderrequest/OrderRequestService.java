package com.dhl.inventory.services.warehouse.orderrequest;

import com.dhl.inventory.dto.commandobject.warehouse.orderrequest.OrderRequestCO;
import com.dhl.inventory.dto.queryobject.QueryResultObjectWrapper;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.warehouse.orderrequest.OrderRequestLQO;
import com.dhl.inventory.dto.queryobject.warehouse.orderrequest.OrderRequestQRO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.CannotBeRemovedException;
import com.dhl.inventory.services.exceptions.ValidationException;

public interface OrderRequestService {
	String createNewOrderRequest(OrderRequestCO commandObject) throws ValidationException;
	
	void updateOrderRequest(String orderNumber, OrderRequestCO commandObject) throws ValidationException;
	
	void removeOrderRequest(String orderNumber) throws CannotBeRemovedException, CannotBeFoundException;
	
	void submitOrderRequest(OrderRequestCO commandObject) throws ValidationException;
	
	void approveOrderRequest(OrderRequestCO commandObject) throws ValidationException;
	
	void rejectOrderRequest(OrderRequestCO commandObject) throws ValidationException;
	
	SearchList findAllOrderRequests(OrderRequestLQO orderRequestListQueryObject);
	
	OrderRequestQRO findOrderRequestByOrderNumber(String orderNumber) throws CannotBeFoundException;
	
	QueryResultObjectWrapper<OrderRequestQRO> getOrderRequestByOrderNumber(String orderNumber) throws CannotBeFoundException;
	
	QueryResultObjectWrapper<OrderRequestQRO> generateOrderRequestTemplate();
}
