package com.dhl.inventory.services.warehouse.inventorymaintenance;

import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.warehouse.inventorymaintenance.OnHandByItemCodeLQO;

public interface OnHandSuppliesService {
	SearchList findAllOnHandSupplies(OnHandByItemCodeLQO onHandByItemCodeLQO);
}
