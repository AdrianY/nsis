package com.dhl.inventory.services.warehouse.orderrequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.components.AuthenticationBean;
import com.dhl.inventory.converter.warehouse.orderrequest.OrderRequestApprovalCommandObjectConverter;
import com.dhl.inventory.converter.warehouse.orderrequest.OrderRequestCommandObjectConverter;
import com.dhl.inventory.converter.warehouse.orderrequest.OrderRequestQueryObjectConverter;
import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.warehouse.orderrequest.OrderRequest;
import com.dhl.inventory.domain.warehouse.orderrequest.OrderRequestApproval;
import com.dhl.inventory.dto.commandobject.warehouse.orderrequest.OrderRequestApproverCO;
import com.dhl.inventory.dto.commandobject.warehouse.orderrequest.OrderRequestCO;
import com.dhl.inventory.dto.queryobject.PersonnelQRO;
import com.dhl.inventory.dto.queryobject.QROEditableFieldsMeta;
import com.dhl.inventory.dto.queryobject.QueryResultObjectWrapper;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.warehouse.orderrequest.OrderRequestLQO;
import com.dhl.inventory.dto.queryobject.warehouse.orderrequest.OrderRequestQRO;
import com.dhl.inventory.dto.queryobject.warehouse.orderrequest.OrderRequestRecipientQRO;
import com.dhl.inventory.repository.maintenance.FacilityRepository;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.repository.warehouse.orderrequest.OrderRequestApprovalRepository;
import com.dhl.inventory.repository.warehouse.orderrequest.OrderRequestReferenceCodeGenerator;
import com.dhl.inventory.repository.warehouse.orderrequest.OrderRequestRepository;
import com.dhl.inventory.services.AbstractResourceService;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.CannotBeRemovedException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.util.OrderRequestStatus;
import com.dhl.inventory.util.SearchType;
import com.dhl.inventory.util.UsingChainable;
import com.dhl.inventory.validator.warehouse.orderrequest.OrderRequestCommandValidator;

import static com.dhl.inventory.util.ActionType.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service("orderRequestService")
class OrderRequestServiceImpl extends AbstractResourceService<OrderRequest> implements OrderRequestService{
	
	private static final Logger LOGGER = Logger.getLogger(OrderRequestServiceImpl.class);
	
	private static final String DEFAULT_FACILITY_CODE = "GTW";
	
	private static final String DEFAULT_OR_CURRENCY = "SGD";

	@Autowired
	private OrderRequestCommandValidator detailsValidator;
	
	@Autowired
	private OrderRequestApprovalCommandObjectConverter approvalConverter;
	
	@Autowired
	private OrderRequestCommandObjectConverter detailsConverter;
	
	@Autowired
	private OrderRequestQueryObjectConverter readWrapper;
	
	@Autowired 
	private UserRepository userRepo;
	
	@Autowired
	private AuthenticationBean authBean;
	
	@Autowired 
	private OrderRequestRepository orderRequestRepo;
	
	@Autowired 
	private OrderRequestApprovalRepository orderRequestApprovalRepo;
	
	@Autowired 
	private OrderRequestReferenceCodeGenerator orderRequestReferenceCodeGenerator;
	
	@Autowired 
	private FacilityRepository facilityRepo;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public String createNewOrderRequest(OrderRequestCO commandObject) throws ValidationException {
		detailsValidator.validate(CREATE_COMMAND, commandObject);
		OrderRequest orderRequest = detailsConverter.convert(CREATE_COMMAND, commandObject, new OrderRequest());
		orderRequest.setStatus(OrderRequestStatus.DRAFT.toString());
		orderRequest.setOrderById(authBean.getAuthenticatedUserId());
		orderRequestRepo.save(orderRequest);
		return orderRequest.getOrderNumber();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateOrderRequest(String orderNumber, OrderRequestCO commandObject) throws ValidationException {
		detailsValidator.validate(UPDATE_COMMAND, commandObject);
		OrderRequest toBeUpdatedOrderRequest = orderRequestRepo.findByOrderNumber(orderNumber);
		cleanOrderRequestItems(toBeUpdatedOrderRequest);
		OrderRequest orderRequest = detailsConverter.convert(UPDATE_COMMAND, commandObject, toBeUpdatedOrderRequest);
		orderRequest.setOrderById(authBean.getAuthenticatedUserId());
		orderRequestRepo.save(orderRequest);
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void removeOrderRequest(String orderNumber) throws CannotBeRemovedException, CannotBeFoundException {
		OrderRequest orderRequest = orderRequestRepo.findByOrderNumber(orderNumber);
		validateRemovalOfOrderRequest(orderRequest);
		orderRequestRepo.delete(orderRequest);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void submitOrderRequest(OrderRequestCO commandObject) throws ValidationException {
		
		validateOrderRequestSubmit(commandObject);
		
		Optional.object(orderRequestRepo.findByOrderNumber(commandObject.getOrderNumber()))
			.ifNotPresent(() -> {
				return new OrderRequest();
			})
			.then(or -> {
				cleanOrderRequestItems(or);
				OrderRequest orderRequest = 
						detailsConverter.convert(SUBMIT_COMMAND, commandObject, or);
				orderRequest.setStatus(OrderRequestStatus.SUBMITTED.toString());
				orderRequest.setOrderDate(new Date());
				orderRequest.setOrderById(authBean.getAuthenticatedUserId());
				
				orderRequestRepo.save(orderRequest);
			}, null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void approveOrderRequest(OrderRequestCO commandObject) throws ValidationException {
		
		validateOrderRequestApproval(commandObject, APPROVE_COMMAND);
		
		OrderRequest toBeSubmittedOrderRequest = 
				orderRequestRepo.findByOrderNumber(commandObject.getOrderNumber());
		cleanOrderRequestItems(toBeSubmittedOrderRequest);
		OrderRequest orderRequest = 
				detailsConverter.convert(SUBMIT_COMMAND, commandObject, toBeSubmittedOrderRequest);
		orderRequest.setStatus(OrderRequestStatus.APPROVED.toString());
		
		orderRequestRepo.save(orderRequest);
		
		persistORApprovalDetails(orderRequest, commandObject.getApprover());
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void rejectOrderRequest(OrderRequestCO commandObject) throws ValidationException {
		validateOrderRequestApproval(commandObject, REJECT_COMMAND);
		OrderRequest toBeRejectedOrderRequest = 
				orderRequestRepo.findByOrderNumber(commandObject.getOrderNumber());
		
		toBeRejectedOrderRequest.setStatus(OrderRequestStatus.REJECTED.toString());
		orderRequestRepo.save(toBeRejectedOrderRequest);
		
		persistORApprovalDetails(toBeRejectedOrderRequest, commandObject.getApprover());
	}

	@Override
	@Transactional(readOnly = true)
	public SearchList findAllOrderRequests(OrderRequestLQO orderRequestListQueryObject) {
		SearchListValueObject itemSearchList = orderRequestRepo.findAll(orderRequestListQueryObject, SearchType.FILTER);
		return SearchList.generateInstance(itemSearchList, orderRequestListQueryObject);
	}

	@Override
	@Transactional(readOnly = true)
	public OrderRequestQRO findOrderRequestByOrderNumber(String orderNumber) throws CannotBeFoundException{
		OrderRequest orderRequest = orderRequestRepo.findByOrderNumber(orderNumber);
		if(null == orderRequest) 
			throw new CannotBeFoundException("Cannot find OR: "+orderNumber);
		
		return readWrapper.generateQRO(orderRequest);
	}
	
	@Override
	@Transactional(readOnly = true)
	public QueryResultObjectWrapper<OrderRequestQRO> getOrderRequestByOrderNumber(String orderNumber)
			throws CannotBeFoundException {
		OrderRequest orderRequest = orderRequestRepo.findByOrderNumber(orderNumber);
		if(null == orderRequest) 
			throw new CannotBeFoundException("Cannot find OR: "+orderNumber);
		
		OrderRequestQRO qro = readWrapper.generateQRO(orderRequest);
		QROEditableFieldsMeta editMeta = generateEditMeta(orderRequest);
		
		return QueryResultObjectWrapper.generateInstance(qro, editMeta);
	}

	@Override
	@Transactional(readOnly = true)
	public QueryResultObjectWrapper<OrderRequestQRO> generateOrderRequestTemplate() {
		OrderRequestQRO orderRequestTemplate = new OrderRequestQRO();
		//generate default order number, status
		orderRequestTemplate.setOrderNumber(orderRequestReferenceCodeGenerator.generateReferenceCode());
		orderRequestTemplate.setStatus(OrderRequestStatus.NEW.toString());
		
		//generate default recipient
		String currentUserName = authBean.getPrincipalName();
		NsisUser currentUserDetails = userRepo.findByUserName(currentUserName);
		PersonnelQRO currentUserWrappedDetails = wrapPersonnelQRO(currentUserDetails);
		OrderRequestRecipientQRO defaultRecipient = new OrderRequestRecipientQRO();
		defaultRecipient.setRecipientDetails(currentUserWrappedDetails);
		
		Facility orderRequestFacility = facilityRepo.findByCodeName(DEFAULT_FACILITY_CODE);
		defaultRecipient.setDeliveryAddress(orderRequestFacility.getAddress());
		orderRequestTemplate.setRecipient(defaultRecipient);
		
		//generate default currency, facility code
		orderRequestTemplate.setFacilityId(orderRequestFacility.getCodeName());
		orderRequestTemplate.setCurrency(DEFAULT_OR_CURRENCY);
		
		//set appropriate quarter
		orderRequestTemplate.setOrderCycle(defaultOrderCycle());
		
		//set default order by
		orderRequestTemplate.setOrderBy(currentUserWrappedDetails);
		
		QROEditableFieldsMeta editMeta = generateEditMetaForTemplate();
		
		return QueryResultObjectWrapper.generateInstance(orderRequestTemplate, editMeta);
	}
	
	/**
	 * TODO: Implement logic
	 * @return
	 */
	private String defaultOrderCycle(){
		String defaultOrderCycle = "Q1";
		return defaultOrderCycle;
	}
	
	private void cleanOrderRequestItems(OrderRequest or){
		if(!or.isNew()){
			int noOfDeletedOrderRequestItems = orderRequestRepo.deleteAllByOrderRequestItems(or);
			LOGGER.info("No of deleted order request items for OR: "
				+or.getOrderNumber()
				+" is: "+noOfDeletedOrderRequestItems);
		}
	}
	
	private void validateOrderRequestSubmit(OrderRequestCO commandObject) throws ValidationException {
		Map<String, String> errorMessages = new HashMap<String, String>();
		try{
			detailsValidator.validate(SUBMIT_COMMAND, commandObject);
		}catch(ValidationException e){
			errorMessages.putAll(e.getAllErrors());
		}finally{
			if(null == authBean.getAuthenticatedUserId())
				errorMessages.put("orderById", "Must be logged-in to submit OR. Try to login again.");
			if(!errorMessages.isEmpty())
				throw new ValidationException(errorMessages);
		}
	}
	
	private void validateOrderRequestApproval(OrderRequestCO commandObject, String actionType) throws ValidationException {
		Map<String, String> errorMessages = new HashMap<String, String>();
		try{
			detailsValidator.validate(actionType, commandObject);
		}catch(ValidationException e){
			errorMessages.putAll(e.getAllErrors());
		}finally{
			if(null == authBean.getAuthenticatedUserId())
				errorMessages.put("approver.username", "Must be logged-in to "+
						actionType.toLowerCase()+" OR. Try to login again.");
			if(!errorMessages.isEmpty())
				throw new ValidationException(errorMessages);
		}
	}
	
	private void persistORApprovalDetails(OrderRequest orderRequest, OrderRequestApproverCO approvalCO){
		Optional.object(orderRequestApprovalRepo.findByOrderRequest(orderRequest))
			.ifNotPresent(() -> {
				NsisUser approver = userRepo.getByKey(authBean.getAuthenticatedUserId());
				return new OrderRequestApproval(orderRequest, approver);
			})
			.then(
				val -> {
					OrderRequestApproval orApprovalToBePersisted = 
							approvalConverter.convert(null, approvalCO, val);
					orderRequestApprovalRepo.save(orApprovalToBePersisted);
				}, 
				null
			);
	}
	
	@Override
	protected QROEditableFieldsMeta generateEditMetaForTemplate() {
		ArrayList<String> exceptions = new ArrayList<String>();
		exceptions.add("orderBy.fullName");
		exceptions.add("orderNumber");
		exceptions.add("facilityId");
		exceptions.add("status");
		exceptions.add("approver.approverDetails.fullName");
		exceptions.add("approver.approverDetails.userName");
		exceptions.add("approver.remarks");
		return QROEditableFieldsMeta
				.editableExceptCertainFieldsInstance(exceptions);
	}


	@Override
	protected QROEditableFieldsMeta generateEditMeta(OrderRequest orderRequest) {
		
		//if not in session to prevent misleading edit set to non editable
		//if in session and status is new set to editable
		//if in session and status is draft but not equal to order by id set to non editable
		//if in session and status is except submitted set to non editable
		//TODO: if in session and status is submitted and has approval and reject permission. set to editable
		LOGGER.info("orderRequest is not null: "+(null != orderRequest));
		QROEditableFieldsMeta returnValue  = 
			(QROEditableFieldsMeta) UsingChainable
			.thisObject(authBean.getAuthenticatedUserId())
			.deriveIfPresent(userId -> {
					if(orderRequest.getStatus().equals(OrderRequestStatus.DRAFT.toString()) && 
							userId.equals(orderRequest.getOrderById())
							){
						LOGGER.info("In first condition: "+orderRequest.getOrderById());
						return generateEditMetaForTemplate();
					}else if(
							orderRequest.getStatus().equals(OrderRequestStatus.NEW.toString())
							){
						LOGGER.info("In second condition");
						return generateEditMetaForTemplate();
					}else if(
							orderRequest.getStatus().equals(OrderRequestStatus.SUBMITTED.toString()) && 
							!userId.equals(orderRequest.getOrderById())
							){
						LOGGER.info("In third condition");
						ArrayList<String> exceptions = new ArrayList<String>();
						exceptions.add("orderBy.fullName");
						exceptions.add("orderById");
						exceptions.add("orderNumber");
						exceptions.add("orderDate");
						exceptions.add("facilityId");
						exceptions.add("status");
						exceptions.add("approver.approverDetails.fullName");
						exceptions.add("approver.approverDetails.userName");
						return QROEditableFieldsMeta.editableExceptCertainFieldsInstance(exceptions);
					}else {
						return QROEditableFieldsMeta.noEditableInstance();
					}
			})
			.deriveIfNotPresent(() -> {
					return QROEditableFieldsMeta.noEditableInstance();
			})
			.execute();

		LOGGER.info("returnValue is not null: "+(null != returnValue)+"type is: "+(null != returnValue ? returnValue.getType() : "NA"));
		return returnValue;
	}

	private void validateRemovalOfOrderRequest(OrderRequest orderRequest) throws CannotBeRemovedException, CannotBeFoundException{
		if(null == orderRequest) 
			throw new CannotBeFoundException("Cannot find OR.");
		
		//validate if can be deleted
		String errorMessage = "";
		if(!OrderRequestStatus.DRAFT.toString().equals(orderRequest.getStatus())){
			errorMessage = "Cannot remove an OR that has already been submitted or processed.";
		}else if(!authBean.getAuthenticatedUserId().equals(orderRequest.getOrderById())){
			errorMessage = "Cannot remove an OR you have not created.";
		}
		
		if(!errorMessage.isEmpty())
			throw new CannotBeRemovedException(errorMessage);
	}
}
