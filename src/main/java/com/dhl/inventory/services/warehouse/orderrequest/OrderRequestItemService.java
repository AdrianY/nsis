package com.dhl.inventory.services.warehouse.orderrequest;

import java.util.List;
import java.util.Map;

import com.dhl.inventory.dto.commandobject.warehouse.orderrequest.OrderRequestCO;
import com.dhl.inventory.dto.commandobject.warehouse.orderrequest.OrderRequestItemResourceCO;
import com.dhl.inventory.services.exceptions.ValidationException;

public interface OrderRequestItemService {

	List<Map<String, String>> calculateDetailsOfRequestItems(OrderRequestCO calculateCommand) throws ValidationException;

	void validateOrderRequestItem(OrderRequestItemResourceCO oRItemCommand) throws ValidationException;

}
