package com.dhl.inventory.services.warehouse.inventorymaintenance;

import java.util.List;
import java.util.Map;

import com.dhl.inventory.domain.warehouse.inventorymaintenance.DeliveredItemBatch;
import com.dhl.inventory.domain.warehouse.requestissuance.SuppliedItems;
import com.dhl.inventory.dto.commandobject.warehouse.inventorymaintenance.DeliveredItemBatchGroupResourceCO;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.ValidationException;

public interface DeliveredItemBatchService {
	
	List<Map<String, String>> calculateDetailsOfDeliveredItemBatches(DeliveredItemBatchGroupResourceCO calculateCommand);

	void validateDeliveredItemBatch(DeliveredItemBatchGroupResourceCO dRItemBatchCommand) throws ValidationException;

	int getRemainingPhysicalStock(String batchNumber, List<SuppliedItems> excludedSuppliedItems) throws CannotBeFoundException;
	
	int getRemainingPhysicalStockForRequest(String batchNumber, String requestNumber) throws CannotBeFoundException;
	
	int getRemainingPhysicalStock(DeliveredItemBatch batchNumber, List<SuppliedItems> excludedSuppliedItems);
	
	int getRemainingPhysicalStockForRequest(DeliveredItemBatch supplySource, String requestNumber);
}
