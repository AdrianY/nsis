package com.dhl.inventory.services.warehouse.inventorymaintenance;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.domain.maintenance.InventoryItem;
import com.dhl.inventory.domain.warehouse.inventorymaintenance.DeliveredItemBatch;
import com.dhl.inventory.domain.warehouse.orderrequest.OrderRequest;
import com.dhl.inventory.domain.warehouse.requestissuance.SuppliedItems;
import com.dhl.inventory.dto.commandobject.warehouse.inventorymaintenance.DeliveredItemBatchCO;
import com.dhl.inventory.dto.commandobject.warehouse.inventorymaintenance.DeliveredItemBatchGroupResourceCO;
import com.dhl.inventory.repository.maintenance.InventoryItemRepository;
import com.dhl.inventory.repository.warehouse.inventorymaintenance.DeliveredItemBatchRepository;
import com.dhl.inventory.repository.warehouse.orderrequest.OrderRequestRepository;
import com.dhl.inventory.repository.warehouse.requestissuance.SuppliedItemsRepository;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.services.maintenance.CurrencyService;
import com.dhl.inventory.util.ActionType;
import com.dhl.inventory.util.CurrencyUtil;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.util.Using;
import com.dhl.inventory.validator.warehouse.inventorymaintenance.DeliveredItemBatchResourceCOValidator;

@Service("deliveredItemBatchService")
class DeliveredItemBatchServiceImpl implements DeliveredItemBatchService {
	
	@Autowired
	@Qualifier("warehouseDeliveryItemBatchResourceCOValidator")
	private DeliveredItemBatchResourceCOValidator validator;
	
	@Autowired
	private OrderRequestRepository orderRequestRepo;
	
	@Autowired
	private InventoryItemRepository itemRepo;
	
	@Autowired
	private CurrencyService currencyService;
	
	@Autowired
	@Qualifier("warehouseSuppliedItemsRepository")
	private SuppliedItemsRepository suppliedItemsRepo;
	
	@Autowired
	@Qualifier("warehouseDeliveredItemBatchRepository")
	private DeliveredItemBatchRepository deliveredItemBatchRepo;
	
	@Override
	@Transactional(readOnly=true)
	public void validateDeliveredItemBatch(DeliveredItemBatchGroupResourceCO dRItemBatchGroupCommand) throws ValidationException {
		validator.validate(ActionType.VALIDATE_COMMAND, dRItemBatchGroupCommand);
		
		//get order item then ensure quantity is not greater than receivable;
		Map<String, String> errorMap = new HashMap<>();
		OrderRequest orderRequest = orderRequestRepo.findByOrderNumber(dRItemBatchGroupCommand.getOrderNumber());
		orderRequest.getOrderItems().stream().filter(val -> {
			return dRItemBatchGroupCommand.getItemCode().equals(
					val.getItem().getInventoryItem().getCodeName());
		}).findFirst().ifPresent(orderRequestItem -> {
			DeliveredItemBatchCO itemBatchToBeValidated = dRItemBatchGroupCommand.getEditedItemBatch();
			Integer quantity = Using.thisObject(dRItemBatchGroupCommand.getOtherItemBatches()).deriveIfPresent(itemBatches -> {
				return itemBatches.stream().mapToInt(i -> i.getQuantity()).sum();
			});
			int rawQuantity = quantity == null ? 0 : quantity.intValue();
			int totalQuantity = rawQuantity + itemBatchToBeValidated.getQuantity();
			
			if(orderRequestItem.getQuantityByUOM() < totalQuantity){
				errorMap.put("editedItemBatch.quantity","Total quantity cannot be greater than requested quantity");
			}
		});
		
		if(!errorMap.isEmpty())
			throw new ValidationException(errorMap);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Map<String, String>> calculateDetailsOfDeliveredItemBatches(DeliveredItemBatchGroupResourceCO calculateCommand){
		
		OrderRequest orderRequest = orderRequestRepo.findByOrderNumber(calculateCommand.getOrderNumber());
		InventoryItem inventoryItem = itemRepo.findByCodeName(calculateCommand.getItemCode());
		String orderCurrency = orderRequest.getCurrency();
		
		List<Map<String, String>> calculatedRecords = new ArrayList<>();
		List<DeliveredItemBatchCO> cleanItemBatches = new ArrayList<>();
		
		Optional.object(calculateCommand.getOtherItemBatches()).ifPresent(itemBatches -> {
			cleanItemBatches.addAll(cleanDuplicatedItemBatches(itemBatches));
		});
		
		Optional.object(cleanItemBatches).ifPresent(uniqueItemBatches -> {
			uniqueItemBatches.stream().forEach(itemBatch -> {
				int receivedQty = itemBatch.getQuantity();
				BigDecimal equivalentAmount = calculateRequestItemAmount(receivedQty, inventoryItem, orderCurrency);
				
				Map<String, String> record = new HashMap<>();
				record.put("batchNumber", itemBatch.getBatchNumber());
				record.put("rackingCode", itemBatch.getRackingCode());
				record.put("dateStored", itemBatch.getDateStored());
				record.put("quantity", FormatterUtil.integerFormat(receivedQty));
				record.put("amount", FormatterUtil.currencyFormat(equivalentAmount));
				
				calculatedRecords.add(record);
			});
		});
		
		return calculatedRecords;
	}
	
	private List<DeliveredItemBatchCO> cleanDuplicatedItemBatches (List<DeliveredItemBatchCO> itemBatches){
		Map<String, DeliveredItemBatchCO> itemBatchRegistry = new HashMap<>();
		
		itemBatches.stream().forEach(itemBatch -> {
			DeliveredItemBatchCO registeredItemBatch = itemBatchRegistry.get(itemBatch.getBatchNumber());
			Optional.object(registeredItemBatch).then(val -> {
				int currentReceivedQty = itemBatch.getQuantity();
				registeredItemBatch.setQuantity(currentReceivedQty + val.getQuantity());
			}, () -> {
				itemBatchRegistry.put(itemBatch.getBatchNumber(), itemBatch);
			});
		});
		
		List<DeliveredItemBatchCO> returnValue = new ArrayList<>();
		Optional.object(itemBatchRegistry).ifPresent(registry -> {
			for(DeliveredItemBatchCO itemBatch: registry.values()){
				returnValue.add(itemBatch);
			}
		});
		
		return returnValue;
	}
	
	private BigDecimal calculateRequestItemAmount(int receivedQuantity, InventoryItem inventoryItem, String orderCurrency){
		BigDecimal effectiveRate = currencyService.convertToCurrency(inventoryItem.getCurrency().getCode(), orderCurrency, inventoryItem.getItemCost());
		BigDecimal requestItemAmount = CurrencyUtil.multiplyValuesScaleAtTwo(effectiveRate, new BigDecimal(String.valueOf(receivedQuantity)));
		return requestItemAmount;
	}

	/**
	 * TODO: Must also factor in disposal records for each item batch
	 * Provide aggregated version if performance issues are noticed in request issuance menu
	 */
	@Override
	@Transactional(readOnly=true)
	public int getRemainingPhysicalStock(String batchNumber, List<SuppliedItems> excludedSuppliedItems) throws CannotBeFoundException{
		DeliveredItemBatch supplySource = 
				deliveredItemBatchRepo.findByBatchNumber(batchNumber);
		
		if(null == supplySource)
			throw new CannotBeFoundException("Delivered Item Batch "+batchNumber+" cannot be found.");
		
		return getRemainingPhysicalStock(supplySource, excludedSuppliedItems);
	}
	
	@Override
	@Transactional(readOnly=true)
	public int getRemainingPhysicalStock(DeliveredItemBatch supplySource, List<SuppliedItems> excludedSuppliedItems){
		List<SuppliedItems> issuedItemsUsingSupplySource = 
				suppliedItemsRepo.findAllSuppliedItemsUsingDeliveredItemBatch(supplySource, excludedSuppliedItems);
		int totalIssuedItems = issuedItemsUsingSupplySource.stream().mapToInt(val -> val.getQuantity()).sum();
		
		return supplySource.getQuantity() - totalIssuedItems;
	}

	@Override
	@Transactional(readOnly=true)
	public int getRemainingPhysicalStockForRequest(String batchNumber, String requestNumber)
			throws CannotBeFoundException {
		DeliveredItemBatch supplySource = 
				deliveredItemBatchRepo.findByBatchNumber(batchNumber);
		
		if(null == supplySource)
			throw new CannotBeFoundException("Delivered Item Batch "+batchNumber+" cannot be found.");
		
		return getRemainingPhysicalStockForRequest(supplySource, requestNumber);
	}

	@Override
	@Transactional(readOnly=true)
	public int getRemainingPhysicalStockForRequest(DeliveredItemBatch supplySource, String requestNumber){
		List<SuppliedItems> issuedItemsUsingSupplySource = 
				suppliedItemsRepo.findAllSuppliedItemsUsingDeliveredItemBatchForRequest(supplySource, requestNumber);
		int totalIssuedItems = issuedItemsUsingSupplySource.stream().mapToInt(val -> val.getQuantity()).sum();
		
		return supplySource.getQuantity() - totalIssuedItems;
	}
}
