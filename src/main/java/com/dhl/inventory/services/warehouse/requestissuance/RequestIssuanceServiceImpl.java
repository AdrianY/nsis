package com.dhl.inventory.services.warehouse.requestissuance;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhl.inventory.components.AuthenticationBean;
import com.dhl.inventory.converter.warehouse.requestissuance.RequestIssuanceCOConverter;
import com.dhl.inventory.converter.warehouse.requestissuance.RequestIssuanceDeliveryReceiptFormQROConverter;
import com.dhl.inventory.converter.warehouse.requestissuance.RequestIssuancePickListQROConverter;
import com.dhl.inventory.converter.warehouse.requestissuance.RequestIssuanceQROConverter;
import com.dhl.inventory.domain.warehouse.requestissuance.RequestIssuance;
import com.dhl.inventory.dto.commandobject.warehouse.requestissuance.RequestIssuanceCO;
import com.dhl.inventory.dto.commandobject.warehouse.requestissuance.SuppliedItemsGroupCO;
import com.dhl.inventory.dto.commandobject.warehouse.requestissuance.SuppliedItemsGroupResourceCO;
import com.dhl.inventory.dto.queryobject.QROEditableFieldsMeta;
import com.dhl.inventory.dto.queryobject.QueryResultObjectWrapper;
import com.dhl.inventory.dto.queryobject.SearchList;
import com.dhl.inventory.dto.queryobject.SearchListValueObject;
import com.dhl.inventory.dto.queryobject.warehouse.requestissuance.RequestIssuanceDeliveryReceiptFormQRO;
import com.dhl.inventory.dto.queryobject.warehouse.requestissuance.RequestIssuanceLQO;
import com.dhl.inventory.dto.queryobject.warehouse.requestissuance.RequestIssuancePickListQRO;
import com.dhl.inventory.dto.queryobject.warehouse.requestissuance.RequestIssuanceQRO;
import com.dhl.inventory.repository.warehouse.requestissuance.RequestIssuanceRepository;
import com.dhl.inventory.services.AbstractResourceService;
import com.dhl.inventory.services.exceptions.CannotBeFoundException;
import com.dhl.inventory.services.exceptions.ValidationException;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.util.RequestIssuanceStatus;
import com.dhl.inventory.util.SearchType;
import com.dhl.inventory.util.Using;
import com.dhl.inventory.util.UsingChainable;
import com.dhl.inventory.validator.warehouse.requestissuance.RequestIssuanceCommandValidator;
import static com.dhl.inventory.util.ActionType.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("issuanceForRequestToWarehouseService")
class RequestIssuanceServiceImpl extends AbstractResourceService<RequestIssuance> implements RequestIssuanceService {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(RequestIssuanceServiceImpl.class);
	
	@Autowired
	@Qualifier("warehouseSuppliedItemsService")
	private SuppliedItemsService suppliedItemsService;
	
	@Autowired
	@Qualifier("warehouseRequestIssuanceRepository")
	private RequestIssuanceRepository requestIssuanceRepo;
	
	@Autowired
	@Qualifier("warehouseRequestIssuanceQROConverter")
	private RequestIssuanceQROConverter readWrapper;
	
	@Autowired
	@Qualifier("warehouseRequestIssuanceCOConverter")
	private RequestIssuanceCOConverter detailsConverter;
	
	@Autowired
	@Qualifier("warehouseRequestIssuancePickListQROConverter")
	private RequestIssuancePickListQROConverter pickListFormDataConverter;
	
	@Autowired
	@Qualifier("warehouseRequestIssuanceDeliveryReceiptFormQROConverter")
	private RequestIssuanceDeliveryReceiptFormQROConverter deliveryReceiptFormConverter;
	
	@Autowired
	@Qualifier("warehouseRequestIssuanceCommandValidator")
	private RequestIssuanceCommandValidator detailsValidator;
	
	@Autowired
	private AuthenticationBean authBean;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public String createNewRequestIssuance(RequestIssuanceCO requestIssuanceCO) throws ValidationException {
		detailsValidator.validate(CREATE_COMMAND, requestIssuanceCO);
		RequestIssuance newRequestIssuance = detailsConverter.convert(CREATE_COMMAND, requestIssuanceCO, new RequestIssuance());
		newRequestIssuance.setStatus(RequestIssuanceStatus.FOR_PROCESSING.toString().replace("_"," "));
		requestIssuanceRepo.save(newRequestIssuance);
		return requestIssuanceCO.getRequestNumber();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateRequestIssuance(String requestNumber, RequestIssuanceCO requestIssuanceCO)
			throws ValidationException {
		
		validateUpdateRequestIssuance(requestIssuanceCO);
		RequestIssuance toBeUpdatedRequestIssuance = 
				requestIssuanceRepo.findByRequestNumber(requestNumber);
		
		Optional.object(toBeUpdatedRequestIssuance).ifPresent(rI -> {
			clearExistingSuppliedItems(rI);
			RequestIssuance requestIssuance = 
					detailsConverter.convert(UPDATE_COMMAND, requestIssuanceCO, rI);
			requestIssuanceRepo.save(requestIssuance);
		});
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void processRequestIssuance(RequestIssuanceCO requestIssuanceCO) throws ValidationException {
		validateUpdateRequestIssuance(requestIssuanceCO);
		
		Optional.object(requestIssuanceRepo.findByRequestNumber(
				requestIssuanceCO.getRequestNumber())).ifPresent(rI -> {
					
			clearExistingSuppliedItems(rI);
			
			RequestIssuance requestIssuance = 
					detailsConverter.convert(SUBMIT_COMMAND, requestIssuanceCO, rI);
			requestIssuance.setStatus(RequestIssuanceStatus.PROCESSING.toString());
			requestIssuanceRepo.save(requestIssuance);
		});
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void finalizeRequestIssuance(RequestIssuanceCO requestIssuanceCO) throws ValidationException {
		validateFinalizeRequestIssuance(requestIssuanceCO);
		
		RequestIssuance toBeUpdatedRequestIssuance = 
				requestIssuanceRepo.findByRequestNumber(requestIssuanceCO.getRequestNumber());
		
		Optional.object(toBeUpdatedRequestIssuance).ifPresent(rI -> {
			
			clearExistingSuppliedItems(rI);
			
			RequestIssuance requestIssuance = 
					detailsConverter.convert(FINALIZE_COMMAND, requestIssuanceCO, rI);
			requestIssuance.setStatus(RequestIssuanceStatus.DELIVERED.toString().replace("_"," "));
			requestIssuanceRepo.save(requestIssuance);
		});
	}


	@Override
	@Transactional(readOnly = true)
	public SearchList findAllRequestIssuance(RequestIssuanceLQO listQueryObject) {
		SearchListValueObject requestIssuanceSearchList = requestIssuanceRepo.findAll(listQueryObject, SearchType.FILTER);
		return SearchList.generateInstance(requestIssuanceSearchList, listQueryObject);
	}

	@Override
	protected QROEditableFieldsMeta generateEditMeta(RequestIssuance requestIssuance) {
		QROEditableFieldsMeta returnValue  = 
			(QROEditableFieldsMeta) UsingChainable
			.thisObject(authBean.getAuthenticatedUserId())
			.deriveIfPresent(userId -> {
					String custodianId = 
							Using.thisObject(requestIssuance.getHandlingCustodian())
							.deriveIfPresent(val -> {return val.getId();});
					
					if(RequestIssuanceStatus.FOR_PROCESSING.toString().replace("_", " ").equals(requestIssuance.getStatus()) &&
							(null == custodianId || userId.equals(custodianId))){
						return generateEditMetaForTemplate();
					}else if((RequestIssuanceStatus.PROCESSING.toString()).equals(requestIssuance.getStatus()) &&
							userId.equals(custodianId)){
						return generateEditMetaForTemplate();
					}else {
						return QROEditableFieldsMeta.noEditableInstance();
					}
			})
			.deriveIfNotPresent(() -> {
					return QROEditableFieldsMeta.noEditableInstance();
			})
			.execute();

		return returnValue;
	}

	@Override
	protected QROEditableFieldsMeta generateEditMetaForTemplate() {
		ArrayList<String> exceptions = new ArrayList<String>();
		exceptions.add("requestorDetails");
		exceptions.add("requestNumber");
		exceptions.add("facilityCodeName");
		exceptions.add("status");
		exceptions.add("requestDate");
		exceptions.add("currency");
		exceptions.add("recipient");
		exceptions.add("handlingCustodian.custodianDetails");
		return QROEditableFieldsMeta
				.editableExceptCertainFieldsInstance(exceptions);
	}

	@Override
	@Transactional(readOnly = true)
	public QueryResultObjectWrapper<RequestIssuanceQRO> getRequestIssuanceByRequestNumber(String requestNumber)
			throws CannotBeFoundException {
		RequestIssuance requestIssuance = requestIssuanceRepo.findByRequestNumber(requestNumber);
		if(null == requestIssuance) 
			throw new CannotBeFoundException("Cannot find Request Issuance for request: "+requestNumber);
		
		RequestIssuanceQRO qro = readWrapper.generateQRO(requestIssuance);
		QROEditableFieldsMeta editMeta = generateEditMeta(requestIssuance);
		
		return QueryResultObjectWrapper.generateInstance(qro, editMeta);
	}

	@Override
	public void rejectRequestIssuance(RequestIssuanceCO requestIssuanceCO) throws ValidationException {
		detailsValidator.validate(REJECT_COMMAND, requestIssuanceCO);
		
		RequestIssuance toBeUpdatedRequestIssuance = 
				requestIssuanceRepo.findByRequestNumber(requestIssuanceCO.getRequestNumber());
		
		Optional.object(toBeUpdatedRequestIssuance).ifPresent(rI -> {
			clearExistingSuppliedItems(rI);
			RequestIssuance requestIssuance = 
					detailsConverter.convert(REJECT_COMMAND, requestIssuanceCO, rI);
			requestIssuance.setStatus(RequestIssuanceStatus.REJECTED.toString());
			requestIssuanceRepo.save(requestIssuance);
		});
	}
	
	/**
	 * should have remarks if not all requested items are satisfied
	 * 
	 * @param requestIssuanceCO
	 * @throws ValidationException
	 */
	private void validateFinalizeRequestIssuance(RequestIssuanceCO requestIssuanceCO) throws ValidationException{
		Map<String, String> errorMessages = new HashMap<>();
		try{
			detailsValidator.validate(FINALIZE_COMMAND, requestIssuanceCO);
		}catch(ValidationException e){
			errorMessages.putAll(e.getAllErrors());
		}finally {
			Optional.object(requestIssuanceCO.getCustodianRemarks()).ifNotPresent(() -> {
				List<String> itemsNotSatisfied = new ArrayList<>();
				requestIssuanceCO.getSuppliedItemsGroups().forEach(siGrps -> {
					if(!suppliedItemsService.hasSatisfiedRequestedItem(siGrps, requestIssuanceCO.getRequestNumber())){
						itemsNotSatisfied.add(siGrps.getItemCode());
					}
				});

				if(!itemsNotSatisfied.isEmpty()){
					String initialItemCode = itemsNotSatisfied.remove(0);
					StringBuilder stringList = new StringBuilder(initialItemCode);
					
					if(!itemsNotSatisfied.isEmpty()){
						itemsNotSatisfied.forEach(itemCode -> {
							if(!initialItemCode.equals(itemCode))
								stringList.append(", ").append(itemCode);
						});
					}
					
					String finalErrorMsg = "Provide remarks as to "
							+ "why the following requested items are finalized even if not "
							+ "yet supplied with the quantity requested: " + stringList.toString();
					errorMessages.put("custodianRemarks", finalErrorMsg);
				}
			});
		}
		
		if(!errorMessages.isEmpty())
			throw new ValidationException(errorMessages);
	}
	
	private void validateUpdateRequestIssuance(RequestIssuanceCO requestIssuanceCO) throws ValidationException{
		Map<String, String> errorMessages = new HashMap<>();
		
		try{
			detailsValidator.validate(UPDATE_COMMAND, requestIssuanceCO);
		}catch(ValidationException e){
			errorMessages.putAll(e.getAllErrors());
		}/*finally {
			
			String requestNumber = requestIssuanceCO.getRequestNumber();
			Optional.object(requestIssuanceCO.getSuppliedItemsGroups()).ifPresent(suppliedItemsGroups -> {
				suppliedItemsGroups.stream().forEach(suppliedItemsGroup -> {
					Optional.object(suppliedItemsGroup.getSuppliedItems()).ifPresent(val -> {
						try{
							validateSuppliedItemsGroup(requestNumber, suppliedItemsGroup);
						}catch(ValidationException e){
							errorMessages.putAll(e.getAllErrors());
						}
					});
				});
			});
		}*/
		
		if(!errorMessages.isEmpty())
			throw new ValidationException(errorMessages);
	}
	
	/**
	 * TODO incorporate this at update
	 * 
	 * @param requestNumber
	 * @param suppliedItemsGroupCO
	 * @throws ValidationException
	 */
	private void validateSuppliedItemsGroup(
			String requestNumber, SuppliedItemsGroupCO 
			suppliedItemsGroupCO
	) throws ValidationException{
		SuppliedItemsGroupResourceCO itemsGroupCO = new SuppliedItemsGroupResourceCO();
		itemsGroupCO.setItemCode(suppliedItemsGroupCO.getItemCode());
		itemsGroupCO.setSuppliedItems(suppliedItemsGroupCO.getSuppliedItems());
		suppliedItemsService.validateSuppliedItemsGroup(itemsGroupCO,requestNumber);
	}

	@Override
	@Transactional(readOnly = true)
	public RequestIssuancePickListQRO getRequestIssuancePickList(String requestNumber) throws CannotBeFoundException {
		RequestIssuance requestIssuance = 
				requestIssuanceRepo.findByRequestNumber(requestNumber);
		
		if(null == requestIssuance) 
			throw new CannotBeFoundException("Cannot find Request Issuance for request: "+requestNumber);
		
		RequestIssuancePickListQRO pickListFormData = pickListFormDataConverter.generateQRO(requestIssuance);
		
		return pickListFormData;
	}

	@Override
	@Transactional(readOnly = true)
	public RequestIssuanceDeliveryReceiptFormQRO getRequestIssuanceDeliveryReceiptForm(String requestNumber)
			throws CannotBeFoundException {
		RequestIssuance requestIssuance = 
				requestIssuanceRepo.findByRequestNumber(requestNumber);
		
		if(null == requestIssuance) 
			throw new CannotBeFoundException("Cannot find Request Issuance for request: "+requestNumber);
		
		return deliveryReceiptFormConverter.generateQRO(requestIssuance);
	}
	
	private void clearExistingSuppliedItems(final RequestIssuance requestIssuance){
		if(null != requestIssuance && !requestIssuance.isNew()){
			requestIssuanceRepo.deleteAllSupplyItemsByRequestIssuance(requestIssuance);
		}
	}
}
