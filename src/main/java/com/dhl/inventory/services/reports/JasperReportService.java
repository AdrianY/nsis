package com.dhl.inventory.services.reports;

import java.io.File;
import java.util.List;

public interface JasperReportService {
	/**
	 * Allow passing of custom action to inject additional parameters to parameters map object
	 */
	File generateJRPrint(String reportRegistryKey, List<?> dataSourceBeanCollection)  throws Exception;
}
