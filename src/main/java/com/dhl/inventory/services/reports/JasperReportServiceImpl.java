package com.dhl.inventory.services.reports;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dhl.inventory.components.JasperReportRegistry;
import com.dhl.inventory.components.JasperReportRegistry.ReportDetails;
import com.dhl.inventory.util.Optional;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

@Service("jasperReportService")
class JasperReportServiceImpl implements JasperReportService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(JasperReportServiceImpl.class);
	
	@Autowired 
	private ServletContext servletContext;
	
	@Autowired
	private JasperReportRegistry reportRegistry;

	@Override
	public File generateJRPrint(String reportRegistryKey, List<?> dataSourceBeanCollection) throws Exception {
		ReportDetails reportDetails = reportRegistry.getReportDetails(reportRegistryKey);
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		// compile sub reports
		Optional.object(reportDetails.getSubReports()).ifPresent(subReports -> {
			subReports.forEach((subReportKey, subReportDetails) -> {
				try {
					parameters.put(subReportKey, compileAndGenerateReportObject(subReportDetails.getReportName()));
				} catch (Exception e) {
					LOGGER.error("error generating report registered as: "+reportRegistryKey,e);
				}
			});
		});
		
		// generate master report
		File returnValue = 
				compileAndGeneratePdfFile(
						reportDetails.getReportName(),
						parameters,
						dataSourceBeanCollection
				);
		
		LOGGER.info("generated report for "+reportRegistryKey+" has name "+returnValue.getName());
		
		return returnValue;
	}
	
	private File compileAndGeneratePdfFile(String reportName, Map<String, Object> parameters, List<?> dataSourceBeanCollection) throws JRException{
		// compile master report
		compileReport(reportName);

		// fill the report
		JasperPrint jrPrintReportObject = JasperFillManager.fillReport(
				servletContext.getRealPath("reports/compiled/"+asJasperFile(reportName)), 
				parameters, new JRBeanCollectionDataSource(dataSourceBeanCollection));

		String pdfFileName = servletContext.getRealPath("reports/exported/"+asPdfFile(reportName));
		JasperExportManager.exportReportToPdfFile(jrPrintReportObject,pdfFileName);
		
		return new File(pdfFileName);
	}

	private JasperReport compileAndGenerateReportObject(String reportName) throws JRException{
		compileReport(reportName);
		return(JasperReport)JRLoader.loadObjectFromFile(
				servletContext.getRealPath("reports/compiled/"+asJasperFile(reportName)));
	}
	
	private void compileReport(String reportName)  throws JRException{
		JasperCompileManager.compileReportToFile(
				servletContext.getRealPath("reports/"+asJrxmlFile(reportName)), 
				servletContext.getRealPath("reports/compiled/"+asJasperFile(reportName))
		);
	}
	
	private String asJrxmlFile(String reportName){
		return reportName + ".jrxml";
	}
	
	private String asJasperFile(String reportName){
		return reportName + ".jasper";
	}
	
	private String asPdfFile(String reportName){
		return reportName + ".pdf";
	}
}
