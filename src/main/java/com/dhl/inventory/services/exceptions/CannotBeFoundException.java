package com.dhl.inventory.services.exceptions;

public class CannotBeFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3680571242707077254L;

	public CannotBeFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CannotBeFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public CannotBeFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CannotBeFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CannotBeFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
