package com.dhl.inventory.services.exceptions;

public class CannotBeRemovedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6353232444076861175L;

	public CannotBeRemovedException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CannotBeRemovedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public CannotBeRemovedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CannotBeRemovedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CannotBeRemovedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
}
