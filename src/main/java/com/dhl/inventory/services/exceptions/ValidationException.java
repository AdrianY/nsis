package com.dhl.inventory.services.exceptions;

import java.util.Map;

public class ValidationException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5217443488448392071L;
	
	private Map<String, String> allErrors;

	public ValidationException(final Map<String, String> errorMessages) {
        this(null,errorMessages);
    }

	public ValidationException(String s,final Map<String, String> errorMessages) {
        super(s);
        this.allErrors = errorMessages;
    }

    public Map<String, String> getAllErrors(){
        return allErrors;
    }
}
