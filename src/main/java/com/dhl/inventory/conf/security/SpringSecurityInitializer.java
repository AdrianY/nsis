package com.dhl.inventory.conf.security;

import javax.annotation.Priority;

import org.springframework.core.annotation.Order;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

@Priority(value=2)
@Order(2)
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer   {

}
