package com.dhl.inventory.conf;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.jolbox.bonecp.BoneCPDataSource;

@Configuration
@EnableTransactionManagement
@ComponentScan({ "com.dhl.inventory.conf","com.dhl.inventory.components" })
@PropertySource(value = { "classpath:application.properties" })
public class HibernateConfig {
	@Autowired
	private Environment environment;
	
	@Bean(name = "datasource")
    public BoneCPDataSource dataSource() {
		
		BoneCPDataSource dataSource = new BoneCPDataSource();
        dataSource.setDriverClass(environment.getRequiredProperty("jdbc.driverClassName"));
        dataSource.setJdbcUrl(environment.getRequiredProperty("jdbc.url"));
        dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
        dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
        dataSource.setIdleConnectionTestPeriodInMinutes(Long.parseLong(environment.getRequiredProperty("jdbc.cp.idle_conn_test_pd_in_min")));
        dataSource.setIdleMaxAgeInMinutes(Long.parseLong(environment.getRequiredProperty("jdbc.cp.idle_max_age_in_min")));
        dataSource.setMaxConnectionsPerPartition(Integer.parseInt(environment.getRequiredProperty("jdbc.cp.max_conn_per_partition")));
        dataSource.setMinConnectionsPerPartition(Integer.parseInt(environment.getRequiredProperty("jdbc.cp_min_conn_per_partition")));
        dataSource.setPartitionCount(Integer.parseInt(environment.getRequiredProperty("jdbc.cp.partition_count")));
        dataSource.setAcquireIncrement(Integer.parseInt(environment.getRequiredProperty("jdbc.cp.acquire_increment")));
        dataSource.setStatementsCacheSize(Integer.parseInt(environment.getRequiredProperty("jdbc.cp.statements_cache_size")));
        dataSource.setReleaseHelperThreads(Integer.parseInt(environment.getRequiredProperty("jdbc.cp.release_helper_th")));
        return dataSource;
    }

	private Properties hibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
		properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
		properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
		properties.put("hibernate.hbm2ddl.auto", environment.getRequiredProperty("hibernate.hbm2ddl.auto"));
		properties.put("hibernate.default_schema", environment.getRequiredProperty("hibernate.default_schema"));
		
		properties.put("hibernate.cache.use_second_level_cache", environment.getRequiredProperty("hibernate.cache.use_second_level_cache"));
        properties.put("hibernate.cache.region.factory_class", environment.getRequiredProperty("hibernate.cache.region.factory_class"));
        properties.put("hibernate.cache.use_query_cache", environment.getRequiredProperty("hibernate.cache.use_query_cache"));
        properties.put("hibernate.generate_statistics", environment.getRequiredProperty("hibernate.generate_statistics"));
        
		return properties;        
	}
	
	@Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(BoneCPDataSource dataSource) {

        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setPackagesToScan(new String[]{"com.dhl.inventory.domain.*"});
        entityManagerFactoryBean.setLoadTimeWeaver(new InstrumentationLoadTimeWeaver());
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());

        entityManagerFactoryBean.setJpaProperties(hibernateProperties());

        return entityManagerFactoryBean;
    }
}
