package com.dhl.inventory.conf;

import javax.annotation.Priority;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.glassfish.jersey.servlet.ServletContainer;
import org.springframework.core.annotation.Order;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import com.dhl.inventory.conf.rest.JerseyConfig;

@Priority(value = 1)
@Order(1)
public class DhlInventoryAppInitializer implements WebApplicationInitializer {

	@Override
	public void onStartup(ServletContext container) throws ServletException {
		container.setInitParameter("contextConfigLocation", "NOTNULL");
		AnnotationConfigWebApplicationContext appContext = new AnnotationConfigWebApplicationContext();
		appContext.register(AppConfig.class);
		container.addListener(new ContextLoaderListener(appContext));

		ServletRegistration.Dynamic restDisptacher = container.addServlet(
				"dispatcher", new ServletContainer(new JerseyConfig()));
		restDisptacher.setLoadOnStartup(1);
		restDisptacher.addMapping("/rest/1.0/");
		
		ServletRegistration.Dynamic dispatcher = container.addServlet("DispatcherServlet", new DispatcherServlet(appContext));
		dispatcher.addMapping("/");
	}

}
