package com.dhl.inventory.conf.rest;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.dhl.inventory.resources")
public class RestConfig {

}
