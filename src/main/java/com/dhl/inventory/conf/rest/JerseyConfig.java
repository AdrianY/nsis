package com.dhl.inventory.conf.rest;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
 
@Configuration
@ApplicationPath("rest/1.0")
public class JerseyConfig extends ResourceConfig {
	private Logger LOGGER = LoggerFactory.getLogger(JerseyConfig.class);
	
	public JerseyConfig() {
		LOGGER.info("MyApplication started!");
	    packages("com.dhl.inventory.resources");
	    register(MultiPartFeature.class);
	    register(JacksonFeature.class);
	    
	}
	
}
