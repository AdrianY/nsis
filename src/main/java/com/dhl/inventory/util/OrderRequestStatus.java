package com.dhl.inventory.util;

public enum OrderRequestStatus {
	NEW, DRAFT, SUBMITTED, APPROVED, REJECTED
}
