package com.dhl.inventory.util;

public enum SupplyRequestStatus {
	NEW, DRAFT, SUBMITTED, APPROVED, REJECTED
}
