package com.dhl.inventory.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.TimeZone;

public final class FormatterUtil {
	
	private final static String WHOLE_NUMBER_FORMAT = "###";
	private final static String INTEGER_FORMAT = "###,##0";
    private final static String CURRENCY_BASE_FORMAT = "###,##0";
    
    private final static String FULL_DATE_FORMAT = "MMMM d, yyyy";
    private final static String SHORT_DATE_FORMAT = "MM/dd/yyyy";
    private final static String NO_SLASH_SHORT_DATE_FORMAT = "MMddyyyy";
    private final static String ISO8601_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssz";
	private final static String TIMEZONE_UTC = "UTC";
	
	private FormatterUtil(){}
	
	public static String currencyFormat(BigDecimal numberToFormat){
        return currencyFormat(numberToFormat,false);
    }

    public static String currencyFormat(BigDecimal numberToFormat, boolean parenthesized) {
        return currencyFormat(numberToFormat,parenthesized,2);
    }

    public static String currencyFormat(BigDecimal numberToFormat, boolean parenthesized, int scale) {
        String returnValue = "";
        try {
        	DecimalFormat decimalFormat = new DecimalFormat();
        	decimalFormat.applyPattern(createDecimalFormat(scale));
            if(null!=numberToFormat){
                if(0 > numberToFormat.signum()&&parenthesized)
                    returnValue = '(' + decimalFormat.format(numberToFormat.abs()) + ')';
                else
                    returnValue = decimalFormat.format(numberToFormat);
            }else{
            	returnValue = "0.00";
            }
        } catch (Exception e) {
            // do nothing
        }
        return returnValue;
    }
    
    public static String wholeNumberFormat(BigDecimal numberToFormat) {
        try {
        	DecimalFormat decimalFormat = new DecimalFormat();
        	decimalFormat.applyPattern(WHOLE_NUMBER_FORMAT);
            return decimalFormat.format(numberToFormat);

        } catch (Exception e) {
            // do nothing
        }
        return String.valueOf(0);
    }

    public static String integerFormat(int numberToFormat) {
        return integerFormat(new BigDecimal(String.valueOf(numberToFormat)));
    }
    
    public static String integerFormat(BigDecimal numberToFormat) {
        try {
        	DecimalFormat decimalFormat = new DecimalFormat();
        	decimalFormat.applyPattern(INTEGER_FORMAT);
            return null!=numberToFormat?decimalFormat.format(numberToFormat):null;
        } catch (Exception e) {
            // do nothing
        }
        return String.valueOf(0);
    }
    
    private static String createDecimalFormat(int scale){
        StringBuilder numberFormatBuilder = new StringBuilder(CURRENCY_BASE_FORMAT);

        if(0 < scale) numberFormatBuilder.append(".");

        for(int i = 0; i < scale; i++){
            numberFormatBuilder.append("0");
        }
        return numberFormatBuilder.toString();
    }
    
    public static String fullDateFormat(LocalDate dateToConvert) {
    	SimpleDateFormat simpleDateFormatter = new SimpleDateFormat();
        try {
        	simpleDateFormatter.applyPattern(FULL_DATE_FORMAT);

            return simpleDateFormatter.format(dateToConvert);
        } catch(Exception e) {
            // do nothing
        }

        return String.valueOf(0);
    }
    
    public static String fullDateFormat(Date dateToConvert) {
    	SimpleDateFormat simpleDateFormatter = new SimpleDateFormat();
        try {
        	simpleDateFormatter.applyPattern(FULL_DATE_FORMAT);

            return simpleDateFormatter.format(dateToConvert);
        } catch(Exception e) {
            // do nothing
        }

        return null;
    }
    
    public static String noSlashShortDateFormat(Date dateToConvert) {
    	SimpleDateFormat simpleDateFormatter = new SimpleDateFormat();
        try {
        	simpleDateFormatter.applyPattern(NO_SLASH_SHORT_DATE_FORMAT);

            return simpleDateFormatter.format(dateToConvert);
        } catch(Exception e) {
            // do nothing
        }

        return String.valueOf(0);
    }
    
    public static String shortDateFormat(Date dateToConvert) {
    	SimpleDateFormat simpleDateFormatter = new SimpleDateFormat();
        try {
        	simpleDateFormatter.applyPattern(SHORT_DATE_FORMAT);
            return simpleDateFormatter.format(dateToConvert);
        } catch(Exception e) {
            // do nothing
        }

        return String.valueOf(0);
    }
    
    public static String iSO8601DateFormat(Date dateToConvert) {
    	SimpleDateFormat df = new SimpleDateFormat(ISO8601_DATE_FORMAT);
        
        TimeZone tz = TimeZone.getTimeZone(TIMEZONE_UTC);
        
        df.setTimeZone( tz );

        String output = df.format( dateToConvert );

        int inset0 = 9;
        int inset1 = 6;
        
        String s0 = output.substring( 0, output.length() - inset0 );
        String s1 = output.substring( output.length() - inset1, output.length() );

        String result = s0 + s1;

        result = result.replaceAll(TIMEZONE_UTC, "+00:00" );
        
        return result;
    }
}
