package com.dhl.inventory.util;

import java.util.Collection;
import java.util.Map;

public final class Using<T> {
	private T thisObject;
	
	private Using(T thisObject){this.thisObject = thisObject;}
	
	public <U> U deriveIfPresent(ActionWithReturn<T, U> action){
		U returnValue = null;
		if(isPresent(thisObject)) returnValue = action.execute(thisObject);
		return returnValue;
	}
	
	public <U> U deriveIfNotPresent(ActionForNullWithReturn<U> action){
		U returnValue = null;
		if(!isPresent(thisObject)) returnValue = action.execute();
		return returnValue;
	}
	
	public static <T extends Object> Using<T> thisObject(T thisObject){
		Using<T> thisWrapper = new Using<T>(thisObject);
		return thisWrapper;
	}
	
	private <X> boolean isPresent(X object){
		boolean objectIsPresent = true;
		if(null != object) {
			if(object instanceof String)
				objectIsPresent = !((String) object).isEmpty();
			else if(object instanceof Collection)
				objectIsPresent = !((Collection<?>) object).isEmpty();
			else if(object instanceof Map)
				objectIsPresent = !((Map<?,?>) object).isEmpty();
		}else{
			objectIsPresent = false;
		}
		
		return objectIsPresent;
	}
	
	@FunctionalInterface
	public static interface ActionWithReturn<T, U>{
		U execute(T t);
	}
	
	@FunctionalInterface
	public static interface ActionForNullWithReturn<U>{
		U execute();
	}
}
