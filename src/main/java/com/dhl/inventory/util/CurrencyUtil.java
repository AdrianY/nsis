package com.dhl.inventory.util;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

public final class CurrencyUtil {
	private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_EVEN;
	
	private static final int DEFAULT_SCALE = 2;
	
	private CurrencyUtil(){}
	
	public static BigDecimal parseAsBigDecimal(final String currencyString) throws InvalidDecimalNumberException{
        return parseAsBigDecimal(currencyString,"");
    }

    public static BigDecimal parseAsBigDecimal(
            final String currencyString, final String fieldName
    ) throws InvalidDecimalNumberException{
        return parseAsBigDecimal(currencyString, fieldName, true);
    }
	
	public static BigDecimal parseAsBigDecimal(
            final String currencyString, final String fieldName, final boolean notBlank
    ) throws InvalidDecimalNumberException {
        BigDecimal returnValue = null;
        String fieldNameActual = StringUtils.isNotBlank(fieldName) ? fieldName : "Number text";
        String currencyStringActual = StringUtils.isNotBlank(currencyString) ? currencyString.replaceAll(",| ","").trim() : "";
        if(notBlank&&StringUtils.isBlank(currencyStringActual)){
            throw new InvalidDecimalNumberException(fieldNameActual + " cannot be blank.");
        }else if(notBlank&&!NumberUtils.isNumber(currencyStringActual)){
            throw new InvalidDecimalNumberException(fieldNameActual + " is an invalid number.");
        }else if(!notBlank&&StringUtils.isBlank(currencyStringActual)){
            returnValue = null;
        }else{
            returnValue = new BigDecimal(currencyString.replaceAll(",| ","").trim());
        }

        return returnValue;
    }

    public static BigDecimal getSumOfAllValues(List<BigDecimal> values){
        BigDecimal sumHolder = BigDecimal.ZERO;
        for(BigDecimal value: values){
        	sumHolder.add(value);
        }
        return sumHolder;
    }

    public static BigDecimal getSumOfTwoValues(BigDecimal valueOne, BigDecimal valueTwo){
        return (null!=valueOne) ? valueOne.add(nonNullValue(valueTwo)) : nonNullValue(valueTwo);
    }

    public static BigDecimal getDifferenceOfTwoValues(BigDecimal minuend, BigDecimal subtrahend){
        return minuend != null ? minuend.subtract(nonNullValue(subtrahend)) : (nonNullValue(subtrahend).negate());
    }

    public static BigDecimal divideValues(BigDecimal dividend, BigDecimal divisor, int decimalScale) {
        BigDecimal quotient = new BigDecimal("0.000");
        if (dividend != null && (divisor != null  && !equalToTheRight(BigDecimal.ZERO, divisor)))
            quotient = dividend.divide(divisor, new MathContext(decimalScale, ROUNDING_MODE));
        return quotient;
    }

    public static BigDecimal[] divideWithRemainderValues(BigDecimal dividend, BigDecimal divisor, int decimalScale) {
        BigDecimal []quotient = {new BigDecimal("0.000"),new BigDecimal("0.000")};
        		
        if (dividend != null && (divisor != null && !equalToTheRight(BigDecimal.ZERO, divisor)))
            quotient = dividend.divideAndRemainder(divisor, new MathContext(decimalScale, ROUNDING_MODE));
        return quotient;
    }
    
    public static BigDecimal multiplyValuesScaleAtTwo(BigDecimal multiplicand, BigDecimal multiplier) {
        BigDecimal quotient = new BigDecimal("0.000");
        if (multiplicand != null)
            quotient = multiplicand.multiply(nonNullValue(multiplier)).setScale(DEFAULT_SCALE, ROUNDING_MODE);
        return quotient;
    }

    public static BigDecimal multiplyValues(BigDecimal multiplicand, BigDecimal multiplier, int decimalScale) {
        BigDecimal quotient = new BigDecimal("0.000");
        if (multiplicand != null)
            quotient = multiplicand.multiply(multiplier, new MathContext(decimalScale, ROUNDING_MODE));
        return quotient;
    }

    public static BigDecimal roundValue(BigDecimal value){
        return roundValue(value,2);
    }

    public static BigDecimal roundValue(BigDecimal value, int decimalPlaces){
        return nonNullValue(value).setScale(decimalPlaces, ROUNDING_MODE);
    }

    public static boolean greaterThanRight(BigDecimal bigDecimal, BigDecimal val) {
        return 0 < bigDecimal.compareTo(val);
    }

    public static boolean lessThanRight(BigDecimal bigDecimal, BigDecimal val) {
        return 0 > bigDecimal.compareTo(val);
    }

    public static boolean equalToTheRight(BigDecimal bigDecimal, BigDecimal val) {
        return 0 == bigDecimal.compareTo(val);
    }

    public static boolean greaterThanOrEqualToRight(BigDecimal bigDecimal, BigDecimal val) {
        return greaterThanRight(bigDecimal, val) || equalToTheRight(bigDecimal, val);
    }

    public static boolean lessThanOrEqualToRight(BigDecimal bigDecimal, BigDecimal val) {
        return lessThanRight(bigDecimal, val) || equalToTheRight(bigDecimal, val);
    }

    public static boolean betweenLeftAndRightValues(BigDecimal lowerValue, BigDecimal higherValue, BigDecimal val){
        return greaterThanOrEqualToRight(val, lowerValue) && lessThanOrEqualToRight(val, higherValue);
    }
    
    private static BigDecimal nonNullValue(BigDecimal value){
    	return null != value ? value : BigDecimal.ZERO;
    }
}
