package com.dhl.inventory.util;

import java.util.Collection;

public final class UsingChainable<I, O> {
	private I thisObject;
	
	private UsingChainable(I thisObject){this.thisObject = thisObject;}
	
	private ActionWithReturn<I, O> actionIfPresent;
	
	private ActionForNullWithReturn<O> actionIfNotPresent;
	
	public UsingChainable<I, O> deriveIfPresent(ActionWithReturn<I, O> action){
		actionIfPresent = action;
		return this;
	}
	
	public UsingChainable<I, O> deriveIfNotPresent(ActionForNullWithReturn<O> action){
		actionIfNotPresent = action;
		return this;
	}
	
	public O execute(){
		boolean objectIsPresent = true;
		if(null != thisObject) {
			if(thisObject instanceof String && ((String) thisObject).isEmpty())
				objectIsPresent = false;
			else if(thisObject instanceof Collection && ((Collection<?>) thisObject).isEmpty())
				objectIsPresent = false;
		}else{
			objectIsPresent = false;
		}
		return objectIsPresent ? actionIfPresent.execute(thisObject) : actionIfNotPresent.execute();
	}
	
	public <A, B> UsingChainable<A, B> executeThenWrap(Class<A> cls){
		boolean objectIsPresent = true;
		if(null != thisObject) {
			if(thisObject instanceof String && ((String) thisObject).isEmpty())
				objectIsPresent = false;
			else if(thisObject instanceof Collection && ((Collection<?>) thisObject).isEmpty())
				objectIsPresent = false;
		}else{
			objectIsPresent = false;
		}
		
		A returnValue = cls.cast(
				!objectIsPresent ? actionIfNotPresent.execute() : actionIfPresent.execute(thisObject));
		return new UsingChainable<A, B>(returnValue);
	}
	
	@SuppressWarnings("unchecked")
	public ActionWithReturn<I, O> getActionIfPresent(){
		if(null == actionIfPresent)
			actionIfPresent = val -> {return (O)val;};
			
		return actionIfPresent;
	}
	
	public static <T, V> UsingChainable<T, V> thisObject(T thisObject){
		UsingChainable<T,V> thisWrapper = new UsingChainable<T ,V>(thisObject);
		return thisWrapper;
	}
	
	@FunctionalInterface
	public static interface ActionWithReturn<I, O>{
		O execute(I t);
	}
	
	@FunctionalInterface
	public static interface ActionForNullWithReturn<O>{
		O execute();
	}
}
