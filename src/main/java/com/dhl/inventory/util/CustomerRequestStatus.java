package com.dhl.inventory.util;

public enum CustomerRequestStatus {
	NEW, DRAFT, SUBMITTED, REJECTED, PROCESSING, FOR_PARTIAL_DELIVERY, FOR_DELIVERY
}
