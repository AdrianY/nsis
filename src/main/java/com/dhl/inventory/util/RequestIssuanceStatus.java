package com.dhl.inventory.util;

public enum RequestIssuanceStatus {
	FOR_PROCESSING,
	PROCESSING,
	DELIVERED,
	PARTIALLY_DELIVERED,
	REJECTED
}
