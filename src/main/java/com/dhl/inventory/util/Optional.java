package com.dhl.inventory.util;

import java.util.Collection;
import java.util.Map;

/**
 * Utility that executes command (instance of a functional interface) 
 * depending if the object is present or not. Has fluid interface to
 * chain actions based on return value by previous action
 * @author Adrian Yago
 *
 * @param <T> - the class of object
 */
public class Optional <T>{
	private T thisObject;
	
	private Optional(T thisObject){this.thisObject = thisObject;}

	public boolean hasContent(){
		return isPresent(thisObject);
	}
	
	public T getValue(){
		return thisObject;
	}
	
	public void ifPresent(Action<T> action){
		if(isPresent(thisObject)) action.execute(thisObject);
	}

	public <U> void injectIfPresent(ActionWithInject<T, U> action, U toInject){
		if(isPresent(thisObject)) {
			toInject = action.execute(thisObject);
		}
	}
	
	public <U> Optional<T> injectIfNotPresent(ActionForNullWithReturn<U> action, U toInject){
		if(!isPresent(thisObject)) toInject = action.execute();
		return this;
	}
	
	public void then(Action<T> actionIfPresent, ActionForNull actionIfNotPresent){
		if(isPresent(thisObject)) actionIfPresent.execute(thisObject);
		else actionIfNotPresent.execute();
	}
	
	public void ifNotPresent(ActionForNull actionIfNotPresent){
		if(!isPresent(thisObject)) actionIfNotPresent.execute();
	}
	
	@SuppressWarnings("unchecked")
	public <U> Optional<U> ifNotPresent(ActionForNullWithReturn<U> action){
		boolean objectIsPresent = true;
		if(null != thisObject) {
			if(thisObject instanceof String && ((String) thisObject).isEmpty())
				objectIsPresent = false;
			else if(thisObject instanceof Collection && ((Collection<?>) thisObject).isEmpty())
				objectIsPresent = false;
		}else{
			objectIsPresent = false;
		}
		
		U returnValue = null;
		if(!objectIsPresent) returnValue = action.execute();
		else returnValue = (U) thisObject;
		return new Optional<U>(returnValue);
	}
	
	public <U> Optional<U> then(ActionWithReturn<T, U> actionIfPresent, ActionForNullWithReturn<U> actionIfNotPresent){
		boolean objectIsPresent = true;
		if(null != thisObject) {
			if(thisObject instanceof String && ((String) thisObject).isEmpty())
				objectIsPresent = false;
			else if(thisObject instanceof Collection && ((Collection<?>) thisObject).isEmpty())
				objectIsPresent = false;
		}else{
			objectIsPresent = false;
		}
		
		U returnValue = null;
		if(objectIsPresent) {
			returnValue = actionIfPresent.execute(thisObject);
		} else {
			returnValue = actionIfNotPresent.execute();
		}
		
		return new Optional<U>(returnValue);
	}
	
	public static <T extends Object> Optional<T> object(T thisObject){
		Optional<T> thisWrapper = new Optional<T>(thisObject);
		return thisWrapper;
	}
	
	private <X> boolean isPresent(X object){
		boolean objectIsPresent = true;
		if(null != object) {
			if(object instanceof String)
				objectIsPresent = !((String) object).isEmpty();
			else if(object instanceof Collection)
				objectIsPresent = !((Collection<?>) object).isEmpty();
			else if(object instanceof Map)
				objectIsPresent = !((Map<?,?>) object).isEmpty();
		}else{
			objectIsPresent = false;
		}
		
		return objectIsPresent;
	}
	
	@FunctionalInterface
	public static interface ActionForNull{
		void execute();
	}
	
	@FunctionalInterface
	public static interface Action<T>{
		void execute(T t);
	}
	
	@FunctionalInterface
	public static interface ActionWithInject<T, U>{
		U execute(T t);
	}
	
	@FunctionalInterface
	public static interface ActionWithReturn<T, U>{
		U execute(T t);
	}
	
	@FunctionalInterface
	public static interface ActionForNullWithReturn<U>{
		U execute();
	}
}
