package com.dhl.inventory.util;

public final class ActionType {
	private ActionType(){}
	
	public static final String SINGLE_QUERY = "FindOne";
	
	public static final String VALIDATE_COMMAND = "Validate";
	
	public static final String CREATE_COMMAND = "Create";
	
	public static final String UPDATE_COMMAND = "Update";
	
	public static final String DELETE_COMMAND = "Delete";
	
	public static final String SUBMIT_COMMAND = "Submit";
	
	public static final String APPROVE_COMMAND = "Approve";
	
	public static final String REJECT_COMMAND = "Reject";
	
	public static final String FINALIZE_COMMAND = "Finalize";
}
