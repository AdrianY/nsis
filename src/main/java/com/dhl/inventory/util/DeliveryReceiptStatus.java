package com.dhl.inventory.util;

public enum DeliveryReceiptStatus {
	FOR_DELIVERY, DELIVERED, PARTIALLY_DELIVERED
}
