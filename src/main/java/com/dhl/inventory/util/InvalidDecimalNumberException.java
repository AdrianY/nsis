package com.dhl.inventory.util;

public class InvalidDecimalNumberException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6958179066553105382L;

	public InvalidDecimalNumberException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidDecimalNumberException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public InvalidDecimalNumberException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidDecimalNumberException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidDecimalNumberException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
