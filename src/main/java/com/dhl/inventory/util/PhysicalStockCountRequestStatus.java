package com.dhl.inventory.util;

public enum PhysicalStockCountRequestStatus {
	NEW, DRAFT, SUBMITTED, APPROVED, REJECTED
}
