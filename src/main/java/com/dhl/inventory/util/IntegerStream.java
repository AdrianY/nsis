package com.dhl.inventory.util;

import java.util.ArrayList;
import java.util.List;

public class IntegerStream {
	
	private List<Integer> integerList;
	
	private IntegerStream(){
		integerList = new ArrayList<>();
	}
	
	public IntegerStream add(int number){
		integerList.add(number);
		return this;
	}
	
	public IntegerStream addAll(List<Integer> numbers){
		integerList.addAll(numbers);
		return this;
	}
	
	public int sum(){
		return integerList.stream().mapToInt(val -> val.intValue()).sum();
	}
	
	public static IntegerStream getStream(){
		return new IntegerStream();
	}
}
