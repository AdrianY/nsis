package com.dhl.inventory.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public final class DateUtil {
	
	private final static String ISO8601_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	private final static String SHORT_DATE_FORMAT = "MM/dd/yyyy";
	
	private DateUtil(){}
	
	public static Date parseISO8601Date(String iSO8601Date) throws ParseException{
        SimpleDateFormat df = new SimpleDateFormat(ISO8601_DATE_FORMAT);
        return df.parse( iSO8601Date );
	}
	
	public static Date parseShortDate(String shortDate) throws ParseException{
		SimpleDateFormat df = new SimpleDateFormat(SHORT_DATE_FORMAT);
        return df.parse( shortDate );
	}
	
	public static Optional<Date> parsePossibleShortDate(String shortDate) throws ParseException{
		SimpleDateFormat df = new SimpleDateFormat(SHORT_DATE_FORMAT);
        return Optional.object(dateStrIsNotEmpty(shortDate) ? df.parse( shortDate ) : null);
	}
	
	private static boolean dateStrIsNotEmpty(String dateStr){
		return null != dateStr && !dateStr.isEmpty();
	}
	
	public static Date setHour(final Date toBeSet, final int hour){
		Calendar cal = Calendar.getInstance();  
        cal.setTime(toBeSet);  
        cal.set(Calendar.HOUR_OF_DAY, hour);  
        return cal.getTime(); 
	}
	
	public static Date setTime(final Date toBeSet, final int hour, final int min, final int sec){
		Calendar cal = Calendar.getInstance();  
        cal.setTime(toBeSet);  
        cal.set(Calendar.HOUR_OF_DAY, hour);  
        cal.set(Calendar.MINUTE, min);  
        cal.set(Calendar.SECOND, sec);  
        return cal.getTime(); 
	}
}
