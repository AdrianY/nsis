package com.dhl.inventory.converter;

public interface QueryResultObjectConverter<QRO, E> {
	QRO generateQRO(String queryType, E entity);
	
	QRO generateQRO(E entity);
}
