package com.dhl.inventory.converter.customerinterfaces;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dhl.inventory.converter.QueryResultObjectConverter;
import com.dhl.inventory.domain.customerinterfaces.CustomerRequest;
import com.dhl.inventory.domain.customerinterfaces.CustomerRequestRecipient;
import com.dhl.inventory.domain.customerinterfaces.RequestedItem;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.dto.queryobject.PersonnelQRO;
import com.dhl.inventory.dto.queryobject.customerinterfaces.CustomerAccountQRO;
import com.dhl.inventory.dto.queryobject.customerinterfaces.CustomerRequestCustodianQRO;
import com.dhl.inventory.dto.queryobject.customerinterfaces.CustomerRequestQRO;
import com.dhl.inventory.dto.queryobject.customerinterfaces.RequestedItemQRO;
import com.dhl.inventory.repository.customerinterfaces.CustomerRequestIssuanceDetailsRepository;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;

@Component
public class CustomerRequestQROConverter implements QueryResultObjectConverter<CustomerRequestQRO, CustomerRequest> {

	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private CustomerRequestIssuanceDetailsRepository issuanceRepo;
	
	@Override
	public CustomerRequestQRO generateQRO(String queryType, CustomerRequest entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CustomerRequestQRO generateQRO(CustomerRequest customerRequest) {
		CustomerRequestQRO qro = new CustomerRequestQRO()
				.setRequestNumber(customerRequest.getRequestNumber())
				.setStatus(customerRequest.getStatus())
				.setRequestType(customerRequest.getRequestType());
		
		if(null != customerRequest.getToFacility()){
			qro.setFacilityCodeName(customerRequest.getToFacility().getCodeName());
		}
		
		if(null != customerRequest.getRequiredDate()){
			qro.setRequiredDate(FormatterUtil.shortDateFormat(customerRequest.getRequiredDate()));
		}
		
		if(null != customerRequest.getRequestDate()){
			qro.setRequestDate(FormatterUtil.shortDateFormat(customerRequest.getRequestDate()));
		}
		
		if(null != customerRequest.getRequestById()){
			NsisUser user = userRepo.getByKey(customerRequest.getRequestById());
			PersonnelQRO requestBy = new PersonnelQRO();
			requestBy.setUserName(user.getUsername())
			.setEmailAddress(user.getEmailAddress())
			.setContactNumber(user.getContactNumber())
			.setFullName(user.getFullName());
			
			qro.setRequestBy(requestBy);
		}
		CustomerRequestRecipient recipient = customerRequest.getRecipient();
		CustomerAccountQRO customerQRO = new CustomerAccountQRO();
		if(null != recipient){
			customerQRO = wrapCustomerAccount(recipient);
		}
		qro.setCustomer(customerQRO);
		
		List<RequestedItemQRO> requestedItems = new ArrayList<>();
		Optional.object(customerRequest.getRequestedItems()).ifPresent(requestItems -> {
			requestedItems.addAll(wrapRequestedItems(requestItems));
		});
		qro.setRequestedItems(requestedItems);
		
		Optional.object(issuanceRepo.findIssuanceDetailsByRequestNumber(
				customerRequest.getRequestNumber())).ifPresent(issuanceDetails -> {
				CustomerRequestCustodianQRO custodianQRO = new CustomerRequestCustodianQRO()
						.setName(issuanceDetails.getCustodianName())
						.setRemarks(issuanceDetails.getCustodianRemarks());
				qro.setCustodian(custodianQRO)
					.setRouteCode(issuanceDetails.getRouteCode())
					.setStatus(issuanceDetails.getStatus());
		});
		
		return qro;
	}
	
	private CustomerAccountQRO wrapCustomerAccount(final CustomerRequestRecipient recipient){
		CustomerAccountQRO qro = new CustomerAccountQRO()
				.setAccountNumber(recipient.getRecipientId())
				.setDeliveryAddress(recipient.getDeliveryAddress())
				.setContactNumber(recipient.getContactNumber())
				.setContactName(recipient.getContactName())
				.setName(recipient.getCustomerName());
				
		return qro;	
	}
	
	private List<RequestedItemQRO> wrapRequestedItems(Set<RequestedItem> requestedItems){
		List<RequestedItemQRO> returnValue = new ArrayList<>();
		requestedItems.stream().forEach(requestedItem -> {
			RequestedItemQRO qro = new RequestedItemQRO()
					.setItemCode(requestedItem.getItemCode())
					.setQuantity(requestedItem.getQuantity())
					.setRemarks(requestedItem.getRemarks());
			
			returnValue.add(qro);
		});
		return returnValue;
	}
	
}
