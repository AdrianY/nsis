package com.dhl.inventory.converter.customerinterfaces;

import static com.dhl.inventory.util.ActionType.CREATE_COMMAND;
import static com.dhl.inventory.util.ActionType.SUBMIT_COMMAND;
import static com.dhl.inventory.util.ActionType.UPDATE_COMMAND;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dhl.inventory.components.AuthenticationBean;
import com.dhl.inventory.converter.CommandObjectConverter;
import com.dhl.inventory.domain.customerinterfaces.CustomerRequest;
import com.dhl.inventory.domain.customerinterfaces.CustomerRequestRecipient;
import com.dhl.inventory.domain.customerinterfaces.RequestedItem;
import com.dhl.inventory.dto.commandobject.customerinterfaces.CustomerCO;
import com.dhl.inventory.dto.commandobject.customerinterfaces.CustomerRequestCO;
import com.dhl.inventory.dto.commandobject.customerinterfaces.RequestedItemCO;
import com.dhl.inventory.repository.maintenance.FacilityRepository;
import com.dhl.inventory.util.DateUtil;
import com.dhl.inventory.util.Optional;

@Component
public class CustomerRequestCOConverter implements CommandObjectConverter<CustomerRequestCO, CustomerRequest> {

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerRequestCOConverter.class);
	
	@Autowired
	private AuthenticationBean authBean;
	
	@Autowired
	private FacilityRepository facilityRepo;
	
	@Override
	public CustomerRequest convert(String actionType, CustomerRequestCO commandObject,
			CustomerRequest customerRequest) {
		switch(actionType){
			case CREATE_COMMAND:convertToNewCustomerRequest(commandObject,customerRequest);break;
			case UPDATE_COMMAND:convertToUpdateCustomerRequest(commandObject,customerRequest);break;
			case SUBMIT_COMMAND:convertToSubmitCustomerRequest(commandObject,customerRequest);break;
		}
	
		return customerRequest;
	}

	private void convertToSubmitCustomerRequest(CustomerRequestCO commandObject, CustomerRequest customerRequest) {
		customerRequest.setRequestById(authBean.getAuthenticatedUserId());
		customerRequest.setRecipient(wrapRecipientDetails(commandObject.getCustomer()));
		
		try {
			Date dateValue = DateUtil.parseShortDate(commandObject.getRequiredDate());
			customerRequest.setRequiredDate(dateValue);
		} catch (Exception e) {
			LOGGER.error("Cannot parse required date: "+
					commandObject.getRequiredDate()+
					", detailed message is: "+e.getMessage());
		}
		
		customerRequest.setToFacility(
				facilityRepo.findByCodeName(commandObject.getFacilityCode()));
		customerRequest.setRequestedItems(wrapRequestedItems(
				commandObject.getRequestedItems(), customerRequest));
	}

	private void convertToUpdateCustomerRequest(CustomerRequestCO commandObject, CustomerRequest customerRequest) {
		customerRequest.setRequestById(authBean.getAuthenticatedUserId());
		CustomerCO customerCO = commandObject.getCustomer();
		if(null != customerCO){
			customerRequest.setRecipient(wrapRecipientDetails(customerCO));
		}
		
		Optional.object(commandObject.getRequiredDate())
		.ifPresent((val) -> {
			try {
				Date dateValue = DateUtil.parseShortDate(val);
				customerRequest.setRequiredDate(dateValue);
			} catch (Exception e) {
				LOGGER.error("Cannot parse required date: "+val+", detailed message is: "+e.getMessage());
			}
		});
		
		Optional.object(facilityRepo.findByCodeName(commandObject.getFacilityCode())).ifPresent(val -> {
			customerRequest.setToFacility(val);
		});
		
		Optional.object(commandObject.getRequestedItems()).ifPresent(requestedItemsCO -> {
			customerRequest.setRequestedItems(wrapRequestedItems(requestedItemsCO, customerRequest));
		});
	}

	private void convertToNewCustomerRequest(CustomerRequestCO commandObject, CustomerRequest customerRequest) {
		convertToUpdateCustomerRequest(commandObject, customerRequest);
	}
	
	private Set<RequestedItem> wrapRequestedItems(ArrayList<RequestedItemCO> requestedItems, CustomerRequest customerRequest){
		Set<RequestedItem> returnValue = new HashSet<>();
		requestedItems.stream().forEach(requestedItemCO -> {
			RequestedItem requestedItem = new RequestedItem();
			requestedItem.setFromRequest(customerRequest);
			requestedItem.setItemCode(requestedItemCO.getItemCode());
			requestedItem.setQuantity(requestedItemCO.getQuantity());
			requestedItem.setRemarks(requestedItemCO.getRemarks());
			returnValue.add(requestedItem);
		});
		return returnValue;
	}

	private CustomerRequestRecipient wrapRecipientDetails(final CustomerCO customerCO){
		CustomerRequestRecipient recipient = new CustomerRequestRecipient();
		recipient.setRecipientId(customerCO.getAccountNumber());
		recipient.setDeliveryAddress(customerCO.getDeliveryAddress());
		recipient.setContactName(customerCO.getContactName());
		recipient.setContactNumber(customerCO.getContactNumber());
		recipient.setCustomerName(customerCO.getCustomerName());
		return recipient;
	}
}
