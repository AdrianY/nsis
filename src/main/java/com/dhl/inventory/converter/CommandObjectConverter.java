package com.dhl.inventory.converter;


public interface CommandObjectConverter<CO, E> {
	E convert(String actionType, CO commandObject, E persistableEntity);
}
