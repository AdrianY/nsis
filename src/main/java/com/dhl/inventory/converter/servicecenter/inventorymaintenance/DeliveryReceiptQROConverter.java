package com.dhl.inventory.converter.servicecenter.inventorymaintenance;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dhl.inventory.converter.QueryResultObjectConverter;
import com.dhl.inventory.converter.servicecenter.supplyrequest.SupplyRequestQROConverter;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.DeliveredItemBatch;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.DeliveryReceipt;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequestItem;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.DeliveredItemBatchQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.DeliveryReceiptQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.supplyrequest.SupplyRequestQRO;
import com.dhl.inventory.repository.servicecenter.inventorymaintenance.DeliveredItemBatchRepository;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;

@Component
public class DeliveryReceiptQROConverter implements QueryResultObjectConverter<DeliveryReceiptQRO, DeliveryReceipt> {

	private static final Logger LOGGER = LoggerFactory.getLogger(DeliveryReceiptQROConverter.class);
	
	@Autowired
	private SupplyRequestQROConverter supplyRequestQROConverter;
	
	@Autowired
	private DeliveredItemBatchRepository deliveredItemBatchRepo;
	
	@Override
	public DeliveryReceiptQRO generateQRO(String queryType, DeliveryReceipt entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeliveryReceiptQRO generateQRO(DeliveryReceipt deliveryReceipt) {
		DeliveryReceiptQRO qro = new DeliveryReceiptQRO();
		qro.setSupplyRequest(getSupplyRequestQRO(deliveryReceipt.getSupplyRequest()))
		.setStatus(deliveryReceipt.getStatus())
		.setItemBatches(getItemBatchesQRO(deliveryReceipt));
		
		Optional.object(deliveryReceipt.getDateDelivered()).ifPresent(dte -> {
			LOGGER.info("date delivered is not null");
			qro.setDateDelivered(FormatterUtil.shortDateFormat(dte));
		});
		
		return qro;
	}

	private SupplyRequestQRO getSupplyRequestQRO(SupplyRequest supplyRequest){
		return supplyRequestQROConverter.generateQRO(supplyRequest);
	}

	private Map<String, List<DeliveredItemBatchQRO>> getItemBatchesQRO(DeliveryReceipt deliveryReceipt){
		Set<SupplyRequestItem> orderItems = deliveryReceipt.getSupplyRequest().getRequestedItems();
		
		return orderItems.stream().collect(Collectors.toMap(oi -> {
			return oi.getItem().getInventoryItem().getCodeName();
		}, oi -> {
			List<DeliveredItemBatchQRO> returnValue = new ArrayList<>();
			List<DeliveredItemBatch> itemBatches = 
				deliveredItemBatchRepo.findAllDeliveredItemBatchByRequestItemAndGroup(oi, deliveryReceipt);
			Optional.object(itemBatches).ifPresent(iBs -> {
				iBs.forEach(iB -> {
					DeliveredItemBatchQRO iBQRO = new DeliveredItemBatchQRO();
					Optional.object(iB.getRackingLocation()).ifPresent(rl -> {
						iBQRO.setRackingCode(rl.getRackCode());
					});
					Optional.object(iB.getDateStored()).ifPresent(dte -> {
						iBQRO.setDateStored(FormatterUtil.shortDateFormat(dte));
					});
					
					iBQRO.setQuantity(iB.getQuantity())
					.setBatchNumber(iB.getBatchNumber());
					
					returnValue.add(iBQRO);
				});
			});
			return returnValue;
		}));
	}
}
