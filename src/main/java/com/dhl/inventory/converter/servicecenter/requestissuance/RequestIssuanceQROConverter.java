package com.dhl.inventory.converter.servicecenter.requestissuance;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dhl.inventory.components.AuthenticationBean;
import com.dhl.inventory.converter.QueryResultObjectConverter;
import com.dhl.inventory.converter.servicecenter.supplyrequest.SupplyRequestQROConverter;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.servicecenter.requestissuance.Customer;
import com.dhl.inventory.domain.servicecenter.requestissuance.RequestIssuance;
import com.dhl.inventory.domain.servicecenter.requestissuance.SubmittedCustomerRequest;
import com.dhl.inventory.domain.servicecenter.requestissuance.SuppliedItems;
import com.dhl.inventory.domain.servicecenter.requestissuance.SuppliedItemsGroup;
import com.dhl.inventory.dto.queryobject.PersonnelQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.RequestIssuanceQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.RequestedItemQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.SuppliedItemsQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.CustomerQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.HandlingCustodianQRO;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.repository.servicecenter.requestissuance.SubmittedCustomerRequestRepository;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;

@Component("serviceCenterRequestIssuanceQROConverter")
public class RequestIssuanceQROConverter implements QueryResultObjectConverter<RequestIssuanceQRO, RequestIssuance>{
	private static final Logger LOGGER = LoggerFactory.getLogger(SupplyRequestQROConverter.class);
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private AuthenticationBean authBean;
	
	@Autowired
	private SubmittedCustomerRequestRepository submittedRequestsRepo;

	@Override
	public RequestIssuanceQRO generateQRO(String queryType, RequestIssuance entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RequestIssuanceQRO generateQRO(RequestIssuance requestIssuance) {
		
		SubmittedCustomerRequest submittedRequest = 
				submittedRequestsRepo.findRequestbyRequestNumber(requestIssuance.getRequestNumber());
		String facilityCode = requestIssuance.getIssuingFacility().getCodeName();
		RequestIssuanceQRO qro = new RequestIssuanceQRO()
				.setFacilityCodeName(facilityCode)
				.setPreviousFacilityCodeName(facilityCode)
				.setRequestNumber(requestIssuance.getRequestNumber())
				.setStatus(requestIssuance.getStatus())
				.setRequestType(submittedRequest.getRequestType());
		
		if(null != requestIssuance.getRoute())
			qro.setRouteCode(requestIssuance.getRoute().getRouteCode());
		
		Optional.object(requestIssuance.getIssuanceDate()).ifPresent(issuanceDate -> {
			qro.setIssuanceDate(FormatterUtil.shortDateFormat(issuanceDate));
		});
		
		Optional.object(submittedRequest.getRequestDate()).ifPresent(requestDate -> {
			qro.setRequestDate(FormatterUtil.shortDateFormat(requestDate));
		});
		
		Optional.object(submittedRequest.getRequiredDate()).ifPresent(requiredDate -> {
			qro.setRequiredDate(FormatterUtil.shortDateFormat(requiredDate));
		});
		
		NsisUser requestor = userRepo.getByKey(submittedRequest.getRequestById());
		PersonnelQRO requestorDetails = wrapPersonnelDetails(requestor);
		
		qro.setRequestorDetails(requestorDetails);
		
		Customer recipient = submittedRequest.getRecipient();
		
		CustomerQRO recipientQRO = new CustomerQRO()
				.setDeliveryAddress(recipient.getDeliveryAddress())
				.setContactNumber(recipient.getContactNumber())
				.setAccountNumber(recipient.getRecipientId())
				.setContactName(recipient.getContactName())
				.setName(recipient.getCustomerName());
		
		qro.setCustomer(recipientQRO);
		
		HandlingCustodianQRO whQRO = 
				new HandlingCustodianQRO().setRemarks(requestIssuance.getCustodianRemarks());
		Optional.object(requestIssuance.getHandlingCustodian())
			.then(
				custodianProfile -> {
					PersonnelQRO warehouseCustodian = wrapPersonnelDetails(custodianProfile);
					whQRO.setCustodianDetails(warehouseCustodian);
				},
				() -> {
					LOGGER.info("Request Issuance has no handling custoidan yet. Most likely not yet processed.");
					NsisUser currentUser = userRepo.getByKey(authBean.getAuthenticatedUserId());
					PersonnelQRO warehouseCustodian = wrapPersonnelDetails(currentUser);
					whQRO.setCustodianDetails(warehouseCustodian);
				}
			);
		qro.setHandlingCustodian(whQRO);
		
		Optional.object(requestIssuance.getSuppliedItemsGroup()).ifPresent(suppliedItemGroups -> {
			ArrayList<RequestedItemQRO> requestedItems = new ArrayList<>();
			suppliedItemGroups.stream().forEach(suppliedItemGroup -> {
				requestedItems.add(wrapToRequestedItemQRO(suppliedItemGroup));
			});
			qro.setRequestedItems(requestedItems);
		});
		
		return qro;
	}
	
	private RequestedItemQRO wrapToRequestedItemQRO(SuppliedItemsGroup suppliedItemsGroup){
		RequestedItemQRO qro = new RequestedItemQRO()
				.setItemCode(suppliedItemsGroup.getForRequestedItemCode());
		
		Optional.object(suppliedItemsGroup.getSuppliedItems()).ifPresent(suppliedItems -> {
			ArrayList<SuppliedItemsQRO> suppliedItemsList = new ArrayList<>();
			suppliedItems.stream().forEach(suppliedItem -> {
				suppliedItemsList.add(wrapToSuppliedItemsQRO(suppliedItem));
			});
			qro.setSuppliedItems(suppliedItemsList);
		});
		
		return qro;
	}
	
	private SuppliedItemsQRO wrapToSuppliedItemsQRO(SuppliedItems suppliedItems){
		SuppliedItemsQRO qro = new SuppliedItemsQRO()
				.setSourceBatchNumber(suppliedItems.getSupplySource().getBatchNumber())
				.setSuppliedQuantity(suppliedItems.getQuantity())
				.setRemarks(suppliedItems.getRemarks());
		return qro;
	}
	
	private PersonnelQRO wrapPersonnelDetails(NsisUser user){
		return wrapPersonnelDetails(user, user.getEmailAddress(), user.getContactNumber());
	}
	
	private PersonnelQRO wrapPersonnelDetails(NsisUser user, String emailAddress, String contactNumber){
		PersonnelQRO recipientDetails = new PersonnelQRO()
		.setUserName(user.getUsername())
		.setFullName(user.getFullName())
		.setEmailAddress(emailAddress)
		.setContactNumber(contactNumber);
		
		return recipientDetails;
	}
}
