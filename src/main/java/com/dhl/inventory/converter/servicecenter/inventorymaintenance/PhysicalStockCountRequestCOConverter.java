package com.dhl.inventory.converter.servicecenter.inventorymaintenance;

import static com.dhl.inventory.util.ActionType.CREATE_COMMAND;
import static com.dhl.inventory.util.ActionType.SUBMIT_COMMAND;
import static com.dhl.inventory.util.ActionType.UPDATE_COMMAND;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dhl.inventory.converter.CommandObjectConverter;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.DeliveredItemBatch;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.PhysicalStockCount;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.PhysicalStockCountRequest;
import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.PhysicalStockCountCO;
import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.PhysicalStockCountRequestCO;
import com.dhl.inventory.repository.servicecenter.inventorymaintenance.DeliveredItemBatchRepository;
import com.dhl.inventory.util.Optional;

@Component("serviceCenterPhysicalStockCountRequestCOConverter")
public class PhysicalStockCountRequestCOConverter implements CommandObjectConverter<PhysicalStockCountRequestCO, PhysicalStockCountRequest> {

	@Autowired
	private DeliveredItemBatchRepository deliveredItemBatchRepo;
	
	@Override
	public PhysicalStockCountRequest convert(String actionType, PhysicalStockCountRequestCO commandObject,
			PhysicalStockCountRequest physicalStockCountRequest) {
		
		switch(actionType){
			case CREATE_COMMAND:convertToNewPhysicalStockCountRequest(commandObject,physicalStockCountRequest);break;
			case UPDATE_COMMAND:convertToUpdatePhysicalStockCountRequest(commandObject,physicalStockCountRequest);break;
			case SUBMIT_COMMAND:convertToSubmitPhysicalStockCountRequest(commandObject,physicalStockCountRequest);break;
		}
	
		return physicalStockCountRequest;
	}

	private void convertToSubmitPhysicalStockCountRequest(PhysicalStockCountRequestCO commandObject,
			PhysicalStockCountRequest physicalStockCountRequest) {
		convertToUpdatePhysicalStockCountRequest(commandObject, physicalStockCountRequest);
	}

	private void convertToUpdatePhysicalStockCountRequest(PhysicalStockCountRequestCO commandObject,
			PhysicalStockCountRequest physicalStockCountRequest) {
		
		Optional.object(commandObject.getPhysicalCounts()).ifPresent(physicalCounts -> {
			physicalStockCountRequest.setPhysicalStockCounts(
					generatePhysicalStockCountSet(physicalCounts, physicalStockCountRequest));
		});
	}

	private void convertToNewPhysicalStockCountRequest(PhysicalStockCountRequestCO commandObject,
			PhysicalStockCountRequest physicalStockCountRequest) {
		physicalStockCountRequest.setRequestNumber(commandObject.getRequestNumber());
	}

	private Set<PhysicalStockCount> generatePhysicalStockCountSet(final List<PhysicalStockCountCO> countCOList, final PhysicalStockCountRequest request){
		return countCOList.stream().map(countCO -> {
			DeliveredItemBatch itemBatch = 
					deliveredItemBatchRepo.findByBatchNumber(countCO.getBatchNumber());
			PhysicalStockCount physicalStockCount = new PhysicalStockCount(itemBatch, request);
			physicalStockCount.setCount(countCO.getCount());
			return physicalStockCount;
		}).collect(Collectors.toSet());
	}
}
