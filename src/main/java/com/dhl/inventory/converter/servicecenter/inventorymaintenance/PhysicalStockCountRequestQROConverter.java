package com.dhl.inventory.converter.servicecenter.inventorymaintenance;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.dhl.inventory.converter.QueryResultObjectConverter;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.PhysicalStockCount;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.PhysicalStockCountRequest;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.PhysicalStockCountRequestApproval;
import com.dhl.inventory.dto.queryobject.PersonnelQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.PhysicalStockCountQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.PhysicalStockCountRequestApprovalQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.inventorymaintenance.PhysicalStockCountRequestQRO;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;

@Component("serviceCenterPhysicalStockCountRequestQROConverter")
public class PhysicalStockCountRequestQROConverter implements QueryResultObjectConverter<PhysicalStockCountRequestQRO, PhysicalStockCountRequest> {

	@Override
	public PhysicalStockCountRequestQRO generateQRO(
			String queryType, PhysicalStockCountRequest physicalStockCountRequest) {
		
		PersonnelQRO currentUserWrappedDetails = 
				wrapPersonnelQRO(physicalStockCountRequest.getRequestBy());
		
		PhysicalStockCountRequestQRO qro = new PhysicalStockCountRequestQRO()
				.setFacilityCodeName(physicalStockCountRequest.getForFacility().getCodeName())
				.setRequestBy(currentUserWrappedDetails)
				.setStatus(physicalStockCountRequest.getStatus())
				.setPhysicalCountList(wrapPhysicalCounts(physicalStockCountRequest.getPhysicalStockCounts()));
		
		Optional.object(physicalStockCountRequest.getRequestDate()).ifPresent(val -> {
			qro.setDateRequested(FormatterUtil.shortDateFormat(val));
		});
		
		Optional.object(physicalStockCountRequest.getApprovalDetails()).ifPresent(approval -> {
			qro.setApprovalDetails(wrapApprovalDetails(approval));
		});
		
		return qro;
	}

	@Override
	public PhysicalStockCountRequestQRO generateQRO(
			PhysicalStockCountRequest physicalStockCountRequest) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private PhysicalStockCountRequestApprovalQRO wrapApprovalDetails(
			final PhysicalStockCountRequestApproval approval){
		PhysicalStockCountRequestApprovalQRO qro = new PhysicalStockCountRequestApprovalQRO()
				.setApproverDetails(wrapPersonnelQRO(approval.getApprover()))
				.setRemarks(approval.getRemarks());
		
		Optional.object(approval.getCountApprovalList()).ifPresent(countApprovalList -> {
			List<String> approvedBatchNumbers = 
					countApprovalList.stream().map(countApproval -> {
						return countApproval.getBatchNumber();
					}).collect(Collectors.toList());
			
			qro.setApprovedBatchNumberCounts(approvedBatchNumbers);
		});
		return qro;
	}
	
	private List<PhysicalStockCountQRO> wrapPhysicalCounts(
			final Set<PhysicalStockCount> physicalCountSet){
		return physicalCountSet.stream().map(physicalCount -> {
			return new PhysicalStockCountQRO()
					.setBatchNumber(physicalCount.getForItemBatch().getBatchNumber())
					.setCount(String.valueOf(physicalCount.getCount()));
		}).collect(Collectors.toList());
	}
	
	private PersonnelQRO wrapPersonnelQRO(final NsisUser currentUserDetails){
		PersonnelQRO wrappedPersonnelQRO = new PersonnelQRO();
		
		if(null != currentUserDetails){
			wrappedPersonnelQRO
				.setUserName(currentUserDetails.getUsername())
				.setFullName(currentUserDetails.getFullName())
				.setEmailAddress(currentUserDetails.getEmailAddress())
				.setContactNumber(currentUserDetails.getContactNumber());
		}
		
		return wrappedPersonnelQRO;
	}

}
