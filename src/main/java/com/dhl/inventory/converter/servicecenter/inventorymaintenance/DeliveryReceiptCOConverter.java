package com.dhl.inventory.converter.servicecenter.inventorymaintenance;

import static com.dhl.inventory.util.ActionType.CREATE_COMMAND;
import static com.dhl.inventory.util.ActionType.FINALIZE_COMMAND;
import static com.dhl.inventory.util.ActionType.UPDATE_COMMAND;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dhl.inventory.converter.CommandObjectConverter;
import com.dhl.inventory.converter.warehouse.inventorymaintenance.DeliveryReceiptCommandObjectConverter;
import com.dhl.inventory.domain.maintenance.Facility;
import com.dhl.inventory.domain.maintenance.WarehouseLocation;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.DeliveredItemBatch;
import com.dhl.inventory.domain.servicecenter.inventorymaintenance.DeliveryReceipt;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequestItem;
import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.DeliveredItemBatchGroupCO;
import com.dhl.inventory.dto.commandobject.servicecenter.inventorymaintenance.DeliveryReceiptCO;
import com.dhl.inventory.repository.maintenance.WarehouseLocationRepository;
import com.dhl.inventory.repository.servicecenter.supplyrequest.SupplyRequestRepository;
import com.dhl.inventory.util.DateUtil;
import com.dhl.inventory.util.Optional;

@Component("serviceCenterDeliveryReceiptCOConverter")
public class DeliveryReceiptCOConverter implements CommandObjectConverter<DeliveryReceiptCO, DeliveryReceipt> {

	private static final Logger LOGGER = LoggerFactory.getLogger(DeliveryReceiptCommandObjectConverter.class);
	
	@Autowired
	private SupplyRequestRepository supplyRequestRepo;
	
	@Autowired
	private WarehouseLocationRepository whLocationRepo;
	
	@Override
	public DeliveryReceipt convert(String actionType, DeliveryReceiptCO commandObject,
			DeliveryReceipt deliveryReceipt) {
		switch(actionType){
			case CREATE_COMMAND:convertToNewDeliveryReceipt(commandObject,deliveryReceipt);break;
			case UPDATE_COMMAND:convertToUpdateDeliveryReceipt(commandObject,deliveryReceipt);break;
			case FINALIZE_COMMAND:convertToFinalizeDeliveryReceipt(commandObject,deliveryReceipt);break;
		}
		
		return deliveryReceipt;
	}

	private void convertToFinalizeDeliveryReceipt(DeliveryReceiptCO commandObject, DeliveryReceipt deliveryReceipt) {
		convertToUpdateDeliveryReceipt(commandObject, deliveryReceipt);
	}

	private void convertToUpdateDeliveryReceipt(DeliveryReceiptCO commandObject, DeliveryReceipt deliveryReceipt) {
		Optional.object(commandObject.getDateDelivered())
		.ifPresent((val) -> {
			try {
				Date dateValue = DateUtil.parseShortDate(val);
				deliveryReceipt.setDateDelivered(dateValue);
			} catch (Exception e) {
				LOGGER.error("Cannot parse date delivered : "+val+", detailed message is: "+e.getMessage());
			}
		});
		
		Optional.object(commandObject.getItemBatchGroups()).ifPresent(itemBatches -> {
			deliveryReceipt.setItemBatches(convertToDeliveredItemBatches(itemBatches, deliveryReceipt));
		});
	}

	private void convertToNewDeliveryReceipt(DeliveryReceiptCO commandObject, DeliveryReceipt deliveryReceipt) {
		SupplyRequest orderRequest = supplyRequestRepo.findByRequestNumber(commandObject.getRequestNumber());
		deliveryReceipt.setSupplyRequest(orderRequest);
	}

	
	private Set<DeliveredItemBatch> convertToDeliveredItemBatches(List<DeliveredItemBatchGroupCO> deliveredItemGroupCOList, DeliveryReceipt deliveryReceipt){
		SupplyRequest orderRequest = deliveryReceipt.getSupplyRequest();
		Facility facility = orderRequest.getFacility();
		
		Map<String, SupplyRequestItem> supplyRequestItemCache = 
			orderRequest.getRequestedItems().stream().collect(
					Collectors.toMap(
						val -> {
							return val.getItem().getInventoryItem().getCodeName();
						}, 
						val -> {
							return val;
						}
					)
			);
		
		Map<String, WarehouseLocation> warehouseLocationCache = new HashMap<>();
		Set<DeliveredItemBatch> returnValue = new HashSet<DeliveredItemBatch>();
		
		/*List<String> itemCodes = deliveredItemGroupCOList.stream().map(
				val -> val.getItemCode()).collect(Collectors.toList());*/
		/*Map<String, InventoryItem> itemDetailsCache = 
				inventoryItemRepo.findAllItemDetailsInList(itemCodes).stream().collect(
					Collectors.toMap(
						val -> {
							return val.getCodeName();
						}, 
						val -> {
							return val;
						}
					)
				);*/
		
		deliveredItemGroupCOList.forEach(deliveredItemGroupCO -> {
			deliveredItemGroupCO.getItemBatches().forEach(deliveredItemCO -> {
				WarehouseLocation whLocation = warehouseLocationCache.get(deliveredItemCO.getRackingCode());
				if(null == whLocation){
					whLocation = whLocationRepo.findByFacilityAndRackingCode(facility, deliveredItemCO.getRackingCode());
					warehouseLocationCache.put(deliveredItemCO.getRackingCode(), whLocation);
				}
				SupplyRequestItem forSupplyRequestItem = 
						supplyRequestItemCache.get(deliveredItemGroupCO.getItemCode());
				DeliveredItemBatch deliveredItemBatch = 
						new DeliveredItemBatch(forSupplyRequestItem);
				deliveredItemBatch.setRackingLocation(whLocation);
				
				deliveredItemBatch.setQuantity(deliveredItemCO.getQuantity());
				
				deliveredItemBatch.setGroup(deliveryReceipt);
				Optional.object(deliveredItemCO.getDateStored())
				.ifPresent((val) -> {
					try {
						Date dateValue = DateUtil.parseShortDate(val);
						deliveredItemBatch.setDateStored(dateValue);
					} catch (Exception e) {
						LOGGER.error("Cannot parse date stored : "+val+", detailed message is: "+e.getMessage());
					}
				});
				returnValue.add(deliveredItemBatch);
			});
			
		});
		
		return returnValue;
	}

}
