package com.dhl.inventory.converter.servicecenter.requestissuance;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dhl.inventory.converter.QueryResultObjectConverter;
import com.dhl.inventory.domain.maintenance.InventoryItem;
import com.dhl.inventory.domain.servicecenter.requestissuance.Customer;
import com.dhl.inventory.domain.servicecenter.requestissuance.RequestIssuance;
import com.dhl.inventory.domain.servicecenter.requestissuance.SubmittedCustomerRequest;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.RequestIssuanceDeliveryReceiptFormQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.RequestIssuanceDeliveryReceiptItemQRO;
import com.dhl.inventory.repository.maintenance.InventoryItemRepository;
import com.dhl.inventory.repository.servicecenter.requestissuance.SubmittedCustomerRequestRepository;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;

@Component("serviceCenterRequestIssuanceDeliveryReceiptFormQROConverter")
public class RequestIssuanceDeliveryReceiptFormQROConverter implements QueryResultObjectConverter<RequestIssuanceDeliveryReceiptFormQRO, RequestIssuance> {
	
	@Autowired
	private SubmittedCustomerRequestRepository submittedRequestsRepo;
	
	@Autowired
	private InventoryItemRepository inventoryItemRepo;
	
	@Override
	public RequestIssuanceDeliveryReceiptFormQRO generateQRO(String queryType, RequestIssuance entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RequestIssuanceDeliveryReceiptFormQRO generateQRO(RequestIssuance requestIssuance) {
		SubmittedCustomerRequest submittedRequest = 
				submittedRequestsRepo.findRequestbyRequestNumber(requestIssuance.getRequestNumber());
		
		RequestIssuanceDeliveryReceiptFormQRO deliveryReceiptForm = 
				new RequestIssuanceDeliveryReceiptFormQRO()
				.setRequestNumber(requestIssuance.getRequestNumber())
				.setRequestedItems(generateRequestedItemsIssuedList(requestIssuance, submittedRequest));
		
		Customer customerDetails = submittedRequest.getRecipient();
		if(null != customerDetails){
			deliveryReceiptForm.setAccountNumber(customerDetails.getRecipientId())
				.setContactNumber(customerDetails.getContactNumber())
				.setDeliveryAddress(customerDetails.getDeliveryAddress())
				.setCustomerName(customerDetails.getCustomerName());
		}
		
		if(null != submittedRequest.getRequiredDate()){
			deliveryReceiptForm.setRequiredDeliveryDate(
					FormatterUtil.shortDateFormat(submittedRequest.getRequiredDate()));
		}
		
		if(null != requestIssuance.getIssuanceDate()){
			deliveryReceiptForm.setDateIssued(
					FormatterUtil.shortDateFormat(requestIssuance.getIssuanceDate()));
		}
		
		if(null != submittedRequest.getRequestDate()){
			deliveryReceiptForm.setRequestedDate(
					FormatterUtil.shortDateFormat(submittedRequest.getRequestDate()));
		}
		
		return deliveryReceiptForm;
	}
	
	private List<RequestIssuanceDeliveryReceiptItemQRO> generateRequestedItemsIssuedList(RequestIssuance requestIssuance, SubmittedCustomerRequest submittedRequest){
		List<RequestIssuanceDeliveryReceiptItemQRO> returnValue = new ArrayList<>();
		Optional.object(requestIssuance.getSuppliedItemsGroup()).ifPresent(suppliedItemGroups -> {
			
			Map<String, Integer> requestItemQuantityMap = 
				submittedRequest.getRequestedItems().stream().collect(Collectors.toMap(
							val -> {
								return val.getItemCode();
							}, 
							val -> {
								return val.getQuantity();
							}
						)
					);
			
			Map<String, String> requestItemDescriptionMap = 
					submittedRequest.getRequestedItems().stream().collect(Collectors.toMap(
							val -> {
								return val.getItemCode();
							}, 
							val -> {
								InventoryItem item = 
										inventoryItemRepo.findByCodeName(val.getItemCode());
								return item.getDescription();
							}
						)
					);
			
			suppliedItemGroups.stream().forEach(group -> {
				String itemCode = group.getForRequestedItemCode();
				RequestIssuanceDeliveryReceiptItemQRO pickListGroup = 
						new RequestIssuanceDeliveryReceiptItemQRO()
						.setItemCode(itemCode)
						.setDescription(requestItemDescriptionMap.get(itemCode))
						.setQtyRequested(FormatterUtil.integerFormat(requestItemQuantityMap.get(itemCode)));
				
				Optional.object(group.getSuppliedItems()).ifPresent(suppliedItems -> {
					int totalIssuedQty = suppliedItems.stream().mapToInt(val -> val.getQuantity()).sum();
					pickListGroup.setQtyIssued(FormatterUtil.integerFormat(totalIssuedQty));
				});
				
				returnValue.add(pickListGroup);
			});
			
		});
		return returnValue;
	}
}
