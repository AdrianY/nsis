package com.dhl.inventory.converter.servicecenter.supplyrequest;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dhl.inventory.components.AuthenticationBean;
import com.dhl.inventory.converter.CommandObjectConverter;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequestApproval;
import com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest.SupplyRequestApproverCO;
import com.dhl.inventory.repository.security.UserRepository;

@Component
public class SupplyRequestApprovalCOConverter implements CommandObjectConverter<SupplyRequestApproverCO, SupplyRequestApproval> {

	@Autowired 
	private UserRepository userRepo;
	
	@Autowired
	private AuthenticationBean authBean;
	
	@Override
	public SupplyRequestApproval convert(String actionType, SupplyRequestApproverCO commandObject,
			SupplyRequestApproval supplyRequestApproval) {
		if(!supplyRequestApproval.isNew()){
			NsisUser approver = userRepo.getByKey(authBean.getAuthenticatedUserId());
			supplyRequestApproval.setApprover(approver);
		}
		supplyRequestApproval.setDateResponded(new Date());
		supplyRequestApproval.setRemarks(commandObject.getRemarks());
		return supplyRequestApproval;
	}

}
