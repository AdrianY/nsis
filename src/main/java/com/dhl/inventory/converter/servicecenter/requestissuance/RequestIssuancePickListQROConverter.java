package com.dhl.inventory.converter.servicecenter.requestissuance;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dhl.inventory.converter.QueryResultObjectConverter;
import com.dhl.inventory.domain.maintenance.InventoryItem;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.servicecenter.requestissuance.RequestIssuance;
import com.dhl.inventory.domain.servicecenter.requestissuance.SubmittedCustomerRequest;
import com.dhl.inventory.domain.servicecenter.requestissuance.SuppliedItems;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.PickListItemGroupQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.PickListItemQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.requestissuance.RequestIssuancePickListQRO;
import com.dhl.inventory.repository.maintenance.InventoryItemRepository;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.repository.servicecenter.requestissuance.SubmittedCustomerRequestRepository;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;

@Component("serviceCenterRequestIssuancePickListQROConverter")
public class RequestIssuancePickListQROConverter implements QueryResultObjectConverter<RequestIssuancePickListQRO, RequestIssuance> {

	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private SubmittedCustomerRequestRepository submittedRequestsRepo;
	
	@Autowired
	private InventoryItemRepository inventoryItemRepo;
	
	@Override
	public RequestIssuancePickListQRO generateQRO(String queryType, RequestIssuance entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RequestIssuancePickListQRO generateQRO(RequestIssuance requestIssuance) {
		SubmittedCustomerRequest submittedRequest = 
				submittedRequestsRepo.findRequestbyRequestNumber(requestIssuance.getRequestNumber());
		NsisUser requestor = userRepo.getByKey(submittedRequest.getRequestById());
		RequestIssuancePickListQRO pickListFormData = new RequestIssuancePickListQRO()
				.setFacilityCode(requestIssuance.getIssuingFacility().getCodeName())
				.setCustomerName(submittedRequest.getRecipient().getCustomerName())
				.setRouteCode(requestIssuance.getRoute().getRouteCode())
				.setRequestNumber(submittedRequest.getRequestNumber())
				.setRequestorName(requestor.getFullName())
				.setRequestedItems(generatePickListGroups(requestIssuance, submittedRequest));
		
		if(null != submittedRequest.getRequiredDate()){
			pickListFormData.setRequiredDeliveryDate(
					FormatterUtil.shortDateFormat(submittedRequest.getRequiredDate()));
		}
		
		if(null != submittedRequest.getRequestDate()){
			pickListFormData.setRequestedDate(
					FormatterUtil.shortDateFormat(submittedRequest.getRequestDate()));
		}
		
		return pickListFormData;
	}
	
	private List<PickListItemGroupQRO> generatePickListGroups(RequestIssuance requestIssuance, SubmittedCustomerRequest submittedRequest){
		List<PickListItemGroupQRO> returnValue = new ArrayList<>();
		Optional.object(requestIssuance.getSuppliedItemsGroup()).ifPresent(suppliedItemGroups -> {
			
			Map<String, Integer> requestItemQuantityMap = 
					submittedRequest.getRequestedItems().stream().collect(Collectors.toMap(
							val -> {
								return val.getItemCode();
							}, 
							val -> {
								return val.getQuantity();
							}
						)
					);
			
			Map<String, String> requestItemDescriptionMap = 
					submittedRequest.getRequestedItems().stream().collect(Collectors.toMap(
							val -> {
								return val.getItemCode();
							}, 
							val -> {
								InventoryItem item = inventoryItemRepo.findByCodeName(val.getItemCode());
								return item.getDescription();
							}
						)
					);
			
			suppliedItemGroups.stream().forEach(group -> {
				String itemCode = group.getForRequestedItemCode();
				PickListItemGroupQRO pickListGroup = new PickListItemGroupQRO()
						.setItemCode(itemCode)
						.setDescription(requestItemDescriptionMap.get(itemCode))
						.setQtyRequested(FormatterUtil.integerFormat(requestItemQuantityMap.get(itemCode)));
				
				Optional.object(group.getSuppliedItems()).ifPresent(suppliedItems -> {
					int totalIssuedQty = suppliedItems.stream().mapToInt(val -> val.getQuantity()).sum();
					pickListGroup.setQtyIssued(FormatterUtil.integerFormat(totalIssuedQty));
					pickListGroup.setPickListItems(generatePickListItems(suppliedItems));
				});
				
				returnValue.add(pickListGroup);
			});
			
		});
		return returnValue;
	}
	
	private List<PickListItemQRO> generatePickListItems(Set<SuppliedItems> suppliedItems){
		List<PickListItemQRO> returnValue = new ArrayList<>();
		suppliedItems.stream().forEach(item -> {
			PickListItemQRO pickListItem = new PickListItemQRO()
					.setBatchNumber(item.getSupplySource().getBatchNumber())
					.setQtyIssuedPerBatch(FormatterUtil.integerFormat(item.getQuantity()));
			returnValue.add(pickListItem);
		});
		return returnValue;
	}

}
