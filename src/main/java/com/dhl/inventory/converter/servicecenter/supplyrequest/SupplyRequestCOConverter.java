package com.dhl.inventory.converter.servicecenter.supplyrequest;

import static com.dhl.inventory.util.ActionType.CREATE_COMMAND;
import static com.dhl.inventory.util.ActionType.SUBMIT_COMMAND;
import static com.dhl.inventory.util.ActionType.UPDATE_COMMAND;

import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dhl.inventory.converter.CommandObjectConverter;
import com.dhl.inventory.domain.Recipient;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequestItem;
import com.dhl.inventory.dto.commandobject.RecipientCO;
import com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest.SupplyRequestCO;
import com.dhl.inventory.dto.commandobject.servicecenter.supplyrequest.SupplyRequestItemCO;
import com.dhl.inventory.repository.maintenance.FacilityInventoryItemRepository;
import com.dhl.inventory.repository.maintenance.FacilityRepository;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.util.DateUtil;
import com.dhl.inventory.util.Optional;

@Component
public class SupplyRequestCOConverter implements CommandObjectConverter<SupplyRequestCO, SupplyRequest> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SupplyRequestCOConverter.class);
	
	@Autowired
	private FacilityInventoryItemRepository facilityItemRepo;
	
	@Autowired
	private FacilityRepository facilityRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	@Override
	public SupplyRequest convert(String actionType, SupplyRequestCO commandObject, SupplyRequest supplyRequest) {
		switch(actionType){
			case CREATE_COMMAND:convertToNewSupplyRequest(commandObject,supplyRequest);break;
			case UPDATE_COMMAND:convertToUpdateSupplyRequest(commandObject,supplyRequest);break;
			case SUBMIT_COMMAND:convertToSubmitSupplyRequest(commandObject,supplyRequest);break;
		}

		return supplyRequest;
	}

	private void convertToSubmitSupplyRequest(SupplyRequestCO commandObject, SupplyRequest supplyRequest){
		supplyRequest.setRequestNumber(commandObject.getRequestNumber());
		supplyRequest.setCurrency(commandObject.getCurrency());
		supplyRequest.setRequestCycle(commandObject.getRequestCycle());
		try {
			supplyRequest.setIssuanceDate(DateUtil.parseShortDate(commandObject.getIssuanceDate()));
			Optional.object(commandObject.getRequestDate()).then(
				requestDateStr -> {
					try {
						supplyRequest.setRequestDate(DateUtil.parseShortDate(requestDateStr));
					} catch (ParseException e) {
						LOGGER.error("Cannot parse date. Detailed message is: "+e.getMessage());
					}
				}, 
				() -> {
					supplyRequest.setRequestDate(new Date());
				}
			);
		} catch (ParseException e) {
			LOGGER.error("Cannot parse date. Detailed message is: "+e.getMessage());
		}
		supplyRequest.setFacility(facilityRepo.findByCodeName(commandObject.getFacilityCodeName()));
		supplyRequest.setRecipient(wrappedRecipient(commandObject.getRecipient()));
		
		Set<SupplyRequestItem> supplyRequestItems = 
			convertToSupplyRequestItems(
				commandObject.getItemsRequested(),
				commandObject.getFacilityCodeName()
			);
		supplyRequestItems.stream().forEach(oi -> {oi.setRequest(supplyRequest);});
		supplyRequest.setRequestedItems(supplyRequestItems);
	}
	
	private void convertToUpdateSupplyRequest(SupplyRequestCO commandObject, SupplyRequest supplyRequest){
		supplyRequest.setRequestNumber(commandObject.getRequestNumber());
		supplyRequest.setCurrency(commandObject.getCurrency());
		supplyRequest.setFacility(facilityRepo.findByCodeName(commandObject.getFacilityCodeName()));
		supplyRequest.setRequestCycle(commandObject.getRequestCycle());
		
		Optional.object(commandObject.getIssuanceDate())
		.ifPresent((val) -> {
			try {
				Date dateValue = DateUtil.parseShortDate(val);
				supplyRequest.setIssuanceDate(dateValue);
			} catch (Exception e) {
				LOGGER.error("Cannot parse issuance date: "+val+", detailed message is: "+e.getMessage());
			}
		});
		
		Optional.object(commandObject.getRequestDate()).then(
				requestDateStr -> {
					try {
						supplyRequest.setRequestDate(DateUtil.parseShortDate(requestDateStr));
					} catch (ParseException e) {
						LOGGER.error("Cannot parse date. Detailed message is: "+e.getMessage());
					}
				}, 
				() -> {
					supplyRequest.setRequestDate(new Date());
				}
			);
		
		Optional.object(commandObject.getRecipient())
		.ifPresent((val) -> {
			supplyRequest.setRecipient(optionalWrappedRecipient(val));
		});
		
		Optional.object(commandObject.getItemsRequested())
		.ifPresent(val -> {
			Set<SupplyRequestItem> supplyRequestItems = convertToSupplyRequestItems(val,commandObject.getFacilityCodeName());
			supplyRequestItems.stream().forEach(oi -> {oi.setRequest(supplyRequest);});
			supplyRequest.setRequestedItems(supplyRequestItems);
		});
	}
	
	private void convertToNewSupplyRequest(SupplyRequestCO commandObject, SupplyRequest supplyRequest){
		supplyRequest.setRequestNumber(commandObject.getRequestNumber());
		convertToUpdateSupplyRequest(commandObject, supplyRequest);
	}
	
	private Recipient optionalWrappedRecipient(RecipientCO recipientCO){
		Recipient recipient = new Recipient();
		Optional
			.object(recipientCO.getUsername())
			.ifPresent(val -> {
				Optional.object(userRepo.findByUserName(val))
					.ifPresent(user -> {recipient.setRecipientId(user.getId());});
			});
		Optional.object(recipientCO.getDeliveryAddress())
		.ifPresent(val -> {recipient.setDeliveryAddress(val);});
		Optional.object(recipientCO.getContactNumber())
		.ifPresent(val -> {recipient.setContactNumber(val);});
		Optional.object(recipientCO.getEmailAddress())
		.ifPresent(val -> {recipient.setEmailAddress(val);});
		
		return recipient;
	}

	private Recipient wrappedRecipient(RecipientCO recipientCO){
		Recipient recipient = new Recipient();
		NsisUser user = userRepo.findByUserName(recipientCO.getUsername());
		
		recipient.setRecipientId(null != user ? user.getId() : recipientCO.getUsername());
		recipient.setDeliveryAddress(recipientCO.getDeliveryAddress());
		recipient.setContactNumber(recipientCO.getContactNumber());
		recipient.setEmailAddress(recipientCO.getEmailAddress());
		return recipient;
	}
	
	private Set<SupplyRequestItem> convertToSupplyRequestItems(List<SupplyRequestItemCO> itemRequestCOList, String facilityCode){
		Set<SupplyRequestItem> returnValue = new HashSet<SupplyRequestItem>();
		for(SupplyRequestItemCO itemRequestCO: itemRequestCOList){
			SupplyRequestItem newSupplyRequestItem = new SupplyRequestItem();
			newSupplyRequestItem.setItem(facilityItemRepo.findByFacilityIdAndItemCode(facilityCode, itemRequestCO.getItemCode()));
			newSupplyRequestItem.setQuantityByUOM(itemRequestCO.getQuantity());
			newSupplyRequestItem.setRemarks(itemRequestCO.getRemarks());
			returnValue.add(newSupplyRequestItem);
		}
		return returnValue;
	}
}
