package com.dhl.inventory.converter.servicecenter.supplyrequest;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dhl.inventory.converter.QueryResultObjectConverter;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;
import com.dhl.inventory.dto.queryobject.PersonnelQRO;
import com.dhl.inventory.dto.queryobject.RecipientQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.supplyrequest.SupplyRequestApproverQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.supplyrequest.SupplyRequestItemQRO;
import com.dhl.inventory.dto.queryobject.servicecenter.supplyrequest.SupplyRequestQRO;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.repository.servicecenter.supplyrequest.SupplyRequestApprovalRepository;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;

@Component
public class SupplyRequestQROConverter implements QueryResultObjectConverter<SupplyRequestQRO, SupplyRequest> {

	private static final Logger LOGGER = LoggerFactory.getLogger(SupplyRequestQROConverter.class);
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private SupplyRequestApprovalRepository srApprovalRepository;
	
	@Override
	public SupplyRequestQRO generateQRO(String queryType, SupplyRequest entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SupplyRequestQRO generateQRO(SupplyRequest sr) {
		SupplyRequestQRO qro = new SupplyRequestQRO();
		qro.setCurrency(sr.getCurrency())
		.setRequestNumber(sr.getRequestNumber())
		.setFacilityCodeName(sr.getFacility().getCodeName())
		.setRequestCycle(sr.getRequestCycle())
		.setStatus(sr.getStatus());
		
		Optional.object(sr.getRequestDate()).ifPresent(val -> {
			qro.setRequestDate(FormatterUtil.shortDateFormat(val));
		});
		
		Optional.object(sr.getIssuanceDate()).ifPresent(val -> {
			qro.setIssuanceDate(FormatterUtil.shortDateFormat(val));
		});
		
		Optional.object(sr.getRequestById()).ifPresent(id -> {
			NsisUser user = userRepo.getByKey(id);
			PersonnelQRO requestBy = new PersonnelQRO();
			requestBy.setUserName(user.getUsername())
			.setEmailAddress(user.getEmailAddress())
			.setContactNumber(user.getContactNumber())
			.setFullName(user.getFullName());
			
			qro.setRequestBy(requestBy);
		});
		
		Optional.object(sr.getRecipient()).ifPresent(val -> {
			PersonnelQRO recipientDetails = new PersonnelQRO();
			Optional.object(val.getRecipientId()).ifPresent(recipientId -> {
				NsisUser user = userRepo.getByKey(val.getRecipientId());
				recipientDetails.setUserName(user.getUsername())
				.setFullName(user.getFullName());
			});
			recipientDetails.setEmailAddress(val.getEmailAddress())
			.setContactNumber(val.getContactNumber());
			
			RecipientQRO recipientQRO = new RecipientQRO();
			recipientQRO.setRecipientDetails(recipientDetails)
			.setDeliveryAddress(val.getDeliveryAddress());
			
			qro.setRecipient(recipientQRO);
		});
		
		Optional.object(sr.getRequestedItems()).ifPresent(items -> {
			ArrayList<SupplyRequestItemQRO> itemsQROList = new ArrayList<>();
			items.forEach(orderItem -> {
				SupplyRequestItemQRO itemQRO = new SupplyRequestItemQRO();
				orderItem.getItem().getInventoryItem().getCodeName();
				itemQRO.setItemCode(orderItem.getItem().getInventoryItem().getCodeName())
				.setQuantity(orderItem.getQuantityByUOM())
				.setRemarks(orderItem.getRemarks());
				
				itemsQROList.add(itemQRO);
			});
			qro.setItemsRequested(itemsQROList);
		});
		
		qro.setApprover(new SupplyRequestApproverQRO());
		Optional.object(srApprovalRepository.findBySupplyRequest(sr)).ifPresent(val -> {
			
			LOGGER.info("Fetched OR "+sr.getRequestNumber()+" has approval record already.");
			
			PersonnelQRO approver = new PersonnelQRO();
			NsisUser user = val.getApprover();
			approver.setUserName(user.getUsername())
			.setEmailAddress(user.getEmailAddress())
			.setContactNumber(user.getContactNumber())
			.setFullName(user.getFullName());
			
			SupplyRequestApproverQRO approvalQRO = new SupplyRequestApproverQRO();
			approvalQRO.setApproverDetails(approver)
			.setRemarks(val.getRemarks());
			
			qro.setApprover(approvalQRO);
		});
		
		return qro;
	}

}
