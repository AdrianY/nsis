package com.dhl.inventory.converter.warehouse.requestissuance;

import static com.dhl.inventory.util.ActionType.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dhl.inventory.converter.CommandObjectConverter;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;
import com.dhl.inventory.domain.warehouse.inventorymaintenance.DeliveredItemBatch;
import com.dhl.inventory.domain.warehouse.requestissuance.RequestIssuance;
import com.dhl.inventory.domain.warehouse.requestissuance.SuppliedItems;
import com.dhl.inventory.dto.commandobject.warehouse.requestissuance.RequestIssuanceCO;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.repository.servicecenter.supplyrequest.SupplyRequestRepository;
import com.dhl.inventory.repository.warehouse.inventorymaintenance.DeliveredItemBatchRepository;
import com.dhl.inventory.domain.warehouse.requestissuance.SuppliedItemsGroup;
import com.dhl.inventory.util.DateUtil;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.util.Using;

@Component("warehouseRequestIssuanceCOConverter")
public class RequestIssuanceCOConverter implements CommandObjectConverter<RequestIssuanceCO, RequestIssuance> {

	private static final Logger LOGGER = LoggerFactory.getLogger(RequestIssuanceCOConverter.class);
	
	@Autowired 
	private SupplyRequestRepository supplyRequestRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private DeliveredItemBatchRepository deliveredItemBatchRepo;
	
	@Override
	public RequestIssuance convert(String actionType, RequestIssuanceCO requestIssuancCO,
			RequestIssuance requestIssuance) {
		switch(actionType){
			case CREATE_COMMAND:convertToCreateRequestIssuance(requestIssuancCO,requestIssuance);break;
			case UPDATE_COMMAND:convertToUpdateRequestIssuance(requestIssuancCO,requestIssuance);break;
			case SUBMIT_COMMAND:convertToProcessRequestIssuance(requestIssuancCO,requestIssuance);break;
			case FINALIZE_COMMAND:convertToFinalizeRequestIssuance(requestIssuancCO,requestIssuance);break;
			case REJECT_COMMAND:convertToRejectRequestIssuance(requestIssuancCO,requestIssuance);break;
		}
	
		return requestIssuance;
	}

	private void convertToCreateRequestIssuance(RequestIssuanceCO requestIssuancCO, RequestIssuance requestIssuance) {
		SupplyRequest supplyRequest = 
				supplyRequestRepo.findByRequestNumber(requestIssuancCO.getRequestNumber());
		
		Optional.object(supplyRequest).ifPresent(sr -> {
			Set<SuppliedItemsGroup> suppliedItemsGroups = new HashSet<>();
			sr.getRequestedItems().stream().forEach(requestedItem -> {
				SuppliedItemsGroup suppliedItemsGroup = new SuppliedItemsGroup(requestIssuance);
				suppliedItemsGroup.setForRequestedItemCode(requestedItem.getItem().getInventoryItem().getCodeName());
				suppliedItemsGroups.add(suppliedItemsGroup);
			});
			
			requestIssuance.setSupplyRequest(sr);
			Optional.object(suppliedItemsGroups).ifPresent(suppliedItemsGroupSet -> {
				requestIssuance.setSuppliedItemsGroup(suppliedItemsGroupSet);
			});
		});
	}

	private void convertToRejectRequestIssuance(RequestIssuanceCO requestIssuancCO, RequestIssuance requestIssuance) {
		requestIssuance.setCustodianRemarks(requestIssuancCO.getCustodianRemarks());
		requestIssuance.setHandlingCustodian(userRepo.findByUserName(requestIssuancCO.getCustodianUsername()));
	}

	private void convertToFinalizeRequestIssuance(RequestIssuanceCO requestIssuancCO, RequestIssuance requestIssuance) {
		convertToUpdateRequestIssuance(requestIssuancCO, requestIssuance);
	}

	private void convertToProcessRequestIssuance(RequestIssuanceCO requestIssuancCO, RequestIssuance requestIssuance) {
		convertToUpdateRequestIssuance(requestIssuancCO, requestIssuance);
	}

	private void convertToUpdateRequestIssuance(RequestIssuanceCO requestIssuancCO, RequestIssuance requestIssuance) {
		Optional.object(requestIssuancCO.getIssuanceDate()).ifPresent(issuanceDate -> {
			try {
				Date dateValue = DateUtil.parseShortDate(issuanceDate);
				requestIssuance.setIssuanceDate(dateValue);
			} catch (Exception e) {
				LOGGER.error("Cannot parse issuance date: "+issuanceDate+", detailed message is: "+e.getMessage());
			}
		});
		requestIssuance.setCustodianRemarks(requestIssuancCO.getCustodianRemarks());
		
		//TODO make validation that user is always logged in to ensure custodian username is always present
		requestIssuance.setHandlingCustodian(userRepo.findByUserName(requestIssuancCO.getCustodianUsername()));
		
		Optional.object(requestIssuancCO.getSuppliedItemsGroups()).ifPresent(suppliedItemsGroupsCO -> {
			suppliedItemsGroupsCO.stream().forEach(suppliedItemsGroupCO -> {
				SuppliedItemsGroup suppliedItemsGroup = 
						findSuppliedItemsGroupByItemCode(
								suppliedItemsGroupCO.getItemCode(),requestIssuance);
				
				Optional.object(suppliedItemsGroupCO.getSuppliedItems()).ifPresent(suppliedItemsList -> {
					Set<SuppliedItems> suppliedItemsSet = new HashSet<>();
					suppliedItemsList.stream().forEach(suppliedItemsCO -> {
						LOGGER.info("passed supplies: "+suppliedItemsCO.getSourceBatchNumber());
						DeliveredItemBatch supplySource = deliveredItemBatchRepo.findByBatchNumber(suppliedItemsCO.getSourceBatchNumber());
						SuppliedItems suppliedItems = new SuppliedItems(supplySource, suppliedItemsGroup);
						suppliedItems.setQuantity(suppliedItemsCO.getSuppliedQuantity());
						suppliedItems.setRemarks(suppliedItemsCO.getRemarks());
						suppliedItemsSet.add(suppliedItems);
					});
					
					suppliedItemsGroup.setSuppliedItems(suppliedItemsSet);
				});
				
			});
		});
	}

	private SuppliedItemsGroup findSuppliedItemsGroupByItemCode(String itemCode, RequestIssuance requestIssuance){
		return Using.thisObject(requestIssuance.getSuppliedItemsGroup()).deriveIfPresent(suppliedItemsGroups -> {
			java.util.Optional<SuppliedItemsGroup> itemsGroup = suppliedItemsGroups.stream().filter(suppliedItemsGroup -> {
				return itemCode.equals(suppliedItemsGroup.getForRequestedItemCode());
			}).findFirst();
			
			return (SuppliedItemsGroup) (itemsGroup.isPresent() ? itemsGroup.get() : null);
		});
	}
}
