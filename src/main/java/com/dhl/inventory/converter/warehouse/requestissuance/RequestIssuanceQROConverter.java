package com.dhl.inventory.converter.warehouse.requestissuance;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dhl.inventory.components.AuthenticationBean;
import com.dhl.inventory.converter.QueryResultObjectConverter;
import com.dhl.inventory.converter.servicecenter.supplyrequest.SupplyRequestQROConverter;
import com.dhl.inventory.domain.Recipient;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;
import com.dhl.inventory.domain.warehouse.requestissuance.RequestIssuance;
import com.dhl.inventory.domain.warehouse.requestissuance.SuppliedItems;
import com.dhl.inventory.domain.warehouse.requestissuance.SuppliedItemsGroup;
import com.dhl.inventory.dto.queryobject.PersonnelQRO;
import com.dhl.inventory.dto.queryobject.RecipientQRO;
import com.dhl.inventory.dto.queryobject.warehouse.requestissuance.RequestIssuanceQRO;
import com.dhl.inventory.dto.queryobject.warehouse.requestissuance.RequestedItemQRO;
import com.dhl.inventory.dto.queryobject.warehouse.requestissuance.SuppliedItemsQRO;
import com.dhl.inventory.dto.queryobject.warehouse.requestissuance.HandlingCustodianQRO;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;

@Component("warehouseRequestIssuanceQROConverter")
public class RequestIssuanceQROConverter implements QueryResultObjectConverter<RequestIssuanceQRO, RequestIssuance>{
	private static final Logger LOGGER = LoggerFactory.getLogger(SupplyRequestQROConverter.class);
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private AuthenticationBean authBean;

	@Override
	public RequestIssuanceQRO generateQRO(String queryType, RequestIssuance entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RequestIssuanceQRO generateQRO(RequestIssuance requestIssuance) {
		
		SupplyRequest supplyRequest = requestIssuance.getSupplyRequest();
		
		RequestIssuanceQRO qro = new RequestIssuanceQRO()
				.setCurrency(supplyRequest.getCurrency())
				.setFacilityCodeName(supplyRequest.getFacility().getCodeName())
				.setRequestNumber(supplyRequest.getRequestNumber())
				.setStatus(requestIssuance.getStatus());
		
		Optional.object(requestIssuance.getIssuanceDate()).ifPresent(issuanceDate -> {
			qro.setIssuanceDate(FormatterUtil.shortDateFormat(issuanceDate));
		});
		
		Optional.object(supplyRequest.getRequestDate()).ifPresent(requestDate -> {
			qro.setRequestDate(FormatterUtil.shortDateFormat(requestDate));
		});
		
		NsisUser requestor = userRepo.getByKey(supplyRequest.getRequestById());
		PersonnelQRO requestorDetails = wrapPersonnelDetails(requestor);
		
		qro.setRequestorDetails(requestorDetails);
		
		Recipient recipient = supplyRequest.getRecipient();
		NsisUser recipientSystemProfile = userRepo.getByKey(recipient.getRecipientId());
		
		PersonnelQRO recipientDetails = 
				wrapPersonnelDetails(recipientSystemProfile, recipient.getEmailAddress(), recipient.getContactNumber());
		
		RecipientQRO recipientQRO = new RecipientQRO()
				.setDeliveryAddress(recipient.getDeliveryAddress())
				.setRecipientDetails(recipientDetails);
		
		qro.setRecipient(recipientQRO);
		
		HandlingCustodianQRO whQRO = 
				new HandlingCustodianQRO().setRemarks(requestIssuance.getCustodianRemarks());
		Optional.object(requestIssuance.getHandlingCustodian())
			.then(
				custodianProfile -> {
					PersonnelQRO warehouseCustodian = wrapPersonnelDetails(custodianProfile);
					whQRO.setCustodianDetails(warehouseCustodian);
				},
				() -> {
					LOGGER.info("Request Issuance has no handling custoidan yet. Most likely not yet processed.");
					NsisUser currentUser = userRepo.getByKey(authBean.getAuthenticatedUserId());
					PersonnelQRO warehouseCustodian = wrapPersonnelDetails(currentUser);
					whQRO.setCustodianDetails(warehouseCustodian);
				}
			);
		qro.setHandlingCustodian(whQRO);
		
		Optional.object(requestIssuance.getSuppliedItemsGroup()).ifPresent(suppliedItemGroups -> {
			ArrayList<RequestedItemQRO> requestedItems = new ArrayList<>();
			suppliedItemGroups.stream().forEach(suppliedItemGroup -> {
				requestedItems.add(wrapToRequestedItemQRO(suppliedItemGroup));
			});
			qro.setRequestedItems(requestedItems);
		});
		
		return qro;
	}
	
	private RequestedItemQRO wrapToRequestedItemQRO(SuppliedItemsGroup suppliedItemsGroup){
		RequestedItemQRO qro = new RequestedItemQRO()
				.setItemCode(suppliedItemsGroup.getForRequestedItemCode());
		
		Optional.object(suppliedItemsGroup.getSuppliedItems()).ifPresent(suppliedItems -> {
			ArrayList<SuppliedItemsQRO> suppliedItemsList = new ArrayList<>();
			suppliedItems.stream().forEach(suppliedItem -> {
				suppliedItemsList.add(wrapToSuppliedItemsQRO(suppliedItem));
			});
			qro.setSuppliedItems(suppliedItemsList);
		});
		
		return qro;
	}
	
	private SuppliedItemsQRO wrapToSuppliedItemsQRO(SuppliedItems suppliedItems){
		SuppliedItemsQRO qro = new SuppliedItemsQRO()
				.setSourceBatchNumber(suppliedItems.getSupplySource().getBatchNumber())
				.setSuppliedQuantity(suppliedItems.getQuantity())
				.setRemarks(suppliedItems.getRemarks());
		return qro;
	}
	
	private PersonnelQRO wrapPersonnelDetails(NsisUser user){
		return wrapPersonnelDetails(user, user.getEmailAddress(), user.getContactNumber());
	}
	
	private PersonnelQRO wrapPersonnelDetails(NsisUser user, String emailAddress, String contactNumber){
		PersonnelQRO recipientDetails = new PersonnelQRO()
		.setUserName(user.getUsername())
		.setFullName(user.getFullName())
		.setEmailAddress(emailAddress)
		.setContactNumber(contactNumber);
		
		return recipientDetails;
	}
}
