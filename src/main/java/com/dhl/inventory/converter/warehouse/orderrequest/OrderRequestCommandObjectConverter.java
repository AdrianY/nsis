package com.dhl.inventory.converter.warehouse.orderrequest;

import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dhl.inventory.converter.CommandObjectConverter;
import com.dhl.inventory.domain.Recipient;
import com.dhl.inventory.domain.warehouse.orderrequest.OrderRequest;
import com.dhl.inventory.domain.warehouse.orderrequest.OrderRequestItem;
import com.dhl.inventory.dto.commandobject.warehouse.orderrequest.OrderRequestCO;
import com.dhl.inventory.dto.commandobject.warehouse.orderrequest.OrderRequestItemCO;
import com.dhl.inventory.dto.commandobject.warehouse.orderrequest.OrderRequestRecipientCO;
import com.dhl.inventory.repository.maintenance.FacilityInventoryItemRepository;
import com.dhl.inventory.repository.maintenance.FacilityRepository;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.util.DateUtil;
import com.dhl.inventory.util.Optional;
import static com.dhl.inventory.util.ActionType.*;

@Component
public class OrderRequestCommandObjectConverter implements CommandObjectConverter<OrderRequestCO, OrderRequest> {

	private static final Logger LOGGER = LoggerFactory.getLogger(OrderRequestCommandObjectConverter.class);
	
	@Autowired
	private FacilityInventoryItemRepository facilityItemRepo;
	
	@Autowired
	private FacilityRepository facilityRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	@Override
	public OrderRequest convert(String actionType, OrderRequestCO commandObject, OrderRequest orderRequest) {
		switch(actionType){
			case CREATE_COMMAND:convertToNewOrderRequest(commandObject,orderRequest);break;
			case UPDATE_COMMAND:convertToUpdateOrderRequest(commandObject,orderRequest);break;
			case SUBMIT_COMMAND:convertToSubmitOrderRequest(commandObject,orderRequest);break;
		}

		return orderRequest;
	}

	private void convertToSubmitOrderRequest(OrderRequestCO commandObject, OrderRequest orderRequest){
		orderRequest.setOrderNumber(commandObject.getOrderNumber());
		orderRequest.setOrderType(commandObject.getOrderType());
		orderRequest.setCurrency(commandObject.getCurrency());
		orderRequest.setOrderCycle(commandObject.getOrderCycle());
		try {
			orderRequest.setDeliveryDate(DateUtil.parseShortDate(commandObject.getDeliveryDate()));
		} catch (ParseException e) {
			LOGGER.error("Cannot parse date. Detailed message is: "+e.getMessage());
		}
		orderRequest.setFacility(facilityRepo.findByCodeName(commandObject.getFacilityId()));
		orderRequest.setRecipient(wrappedRecipient(commandObject.getRecipient()));
		
		Set<OrderRequestItem> orderRequestItems = 
			convertToOrderRequestItems(
				commandObject.getItemsRequested(),
				commandObject.getFacilityId()
			);
		orderRequestItems.stream().forEach(oi -> {oi.setOrder(orderRequest);});
		orderRequest.setOrderItems(orderRequestItems);
	}
	
	private void convertToUpdateOrderRequest(OrderRequestCO commandObject, OrderRequest orderRequest){
		orderRequest.setOrderNumber(commandObject.getOrderNumber());
		orderRequest.setCurrency(commandObject.getCurrency());
		orderRequest.setFacility(facilityRepo.findByCodeName(commandObject.getFacilityId()));
		Optional.object(commandObject.getOrderCycle()).ifPresent((val) -> orderRequest.setOrderCycle(val));
		Optional.object(commandObject.getOrderType()).ifPresent((val) -> orderRequest.setOrderType(val));
		
		Optional.object(commandObject.getDeliveryDate())
		.ifPresent((val) -> {
			try {
				Date dateValue = DateUtil.parseShortDate(val);
				orderRequest.setDeliveryDate(dateValue);
			} catch (Exception e) {
				LOGGER.error("Cannot parse delivery date: "+val+", detailed message is: "+e.getMessage());
			}
		});
		
		Optional.object(commandObject.getOrderDate()).ifPresent(
				orderDateStr -> {
					try {
						orderRequest.setOrderDate(DateUtil.parseShortDate(orderDateStr));
					} catch (ParseException e) {
						LOGGER.error("Cannot parse date. Detailed message is: "+e.getMessage());
					}
				}
			);
		
		Optional.object(commandObject.getRecipient())
		.ifPresent((val) -> {
			orderRequest.setRecipient(optionalWrappedRecipient(val));
		});
		
		Optional.object(commandObject.getItemsRequested())
		.ifPresent(val -> {
			Set<OrderRequestItem> orderRequestItems = convertToOrderRequestItems(val,commandObject.getFacilityId());
			orderRequestItems.stream().forEach(oi -> {oi.setOrder(orderRequest);});
			orderRequest.setOrderItems(orderRequestItems);
		});
	}
	
	private void convertToNewOrderRequest(OrderRequestCO commandObject, OrderRequest orderRequest){
		orderRequest.setOrderNumber(commandObject.getOrderNumber());
		convertToUpdateOrderRequest(commandObject, orderRequest);
	}
	
	private Recipient optionalWrappedRecipient(OrderRequestRecipientCO recipientCO){
		Recipient recipient = new Recipient();
		Optional
			.object(recipientCO.getUsername())
			.ifPresent(val -> {
				Optional.object(userRepo.findByUserName(val))
					.ifPresent(user -> {recipient.setRecipientId(user.getId());});
			});
		Optional.object(recipientCO.getDeliveryAddress())
		.ifPresent(val -> {recipient.setDeliveryAddress(val);});
		Optional.object(recipientCO.getContactNumber())
		.ifPresent(val -> {recipient.setContactNumber(val);});
		Optional.object(recipientCO.getEmailAddress())
		.ifPresent(val -> {recipient.setEmailAddress(val);});
		
		return recipient;
	}

	private Recipient wrappedRecipient(OrderRequestRecipientCO recipientCO){
		Recipient recipient = new Recipient();
		recipient.setRecipientId(userRepo.findByUserName(recipientCO.getUsername()).getId());
		recipient.setDeliveryAddress(recipientCO.getDeliveryAddress());
		recipient.setContactNumber(recipientCO.getContactNumber());
		recipient.setEmailAddress(recipientCO.getEmailAddress());
		return recipient;
	}
	
	private Set<OrderRequestItem> convertToOrderRequestItems(List<OrderRequestItemCO> itemRequestCOList, String facilityCode){
		Set<OrderRequestItem> returnValue = new HashSet<OrderRequestItem>();
		for(OrderRequestItemCO itemRequestCO: itemRequestCOList){
			OrderRequestItem newOrderRequestItem = new OrderRequestItem();
			newOrderRequestItem.setItem(facilityItemRepo.findByFacilityIdAndItemCode(facilityCode, itemRequestCO.getItemCode()));
			newOrderRequestItem.setQuantityByUOM(itemRequestCO.getQuantity());
			newOrderRequestItem.setRemarks(itemRequestCO.getRemarks());
			returnValue.add(newOrderRequestItem);
		}
		return returnValue;
	}

}
