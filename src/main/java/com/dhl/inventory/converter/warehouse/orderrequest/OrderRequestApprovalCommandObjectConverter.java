package com.dhl.inventory.converter.warehouse.orderrequest;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dhl.inventory.components.AuthenticationBean;
import com.dhl.inventory.converter.CommandObjectConverter;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.warehouse.orderrequest.OrderRequestApproval;
import com.dhl.inventory.dto.commandobject.warehouse.orderrequest.OrderRequestApproverCO;
import com.dhl.inventory.repository.security.UserRepository;

@Component
public class OrderRequestApprovalCommandObjectConverter implements CommandObjectConverter<OrderRequestApproverCO, OrderRequestApproval> {

	@Autowired 
	private UserRepository userRepo;
	
	@Autowired
	private AuthenticationBean authBean;
	
	@Override
	public OrderRequestApproval convert(
			String actionType, OrderRequestApproverCO commandObject,
			OrderRequestApproval orderRequestAproval
	) {
		if(!orderRequestAproval.isNew()){
			NsisUser approver = userRepo.getByKey(authBean.getAuthenticatedUserId());
			orderRequestAproval.setApprover(approver);
		}
		orderRequestAproval.setDateResponded(new Date());
		orderRequestAproval.setRemarks(commandObject.getRemarks());
		return orderRequestAproval;
	}


}
