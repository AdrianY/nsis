package com.dhl.inventory.converter.warehouse.requestissuance;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dhl.inventory.converter.QueryResultObjectConverter;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;
import com.dhl.inventory.domain.warehouse.requestissuance.RequestIssuance;
import com.dhl.inventory.domain.warehouse.requestissuance.SuppliedItems;
import com.dhl.inventory.dto.queryobject.warehouse.requestissuance.PickListItemGroupQRO;
import com.dhl.inventory.dto.queryobject.warehouse.requestissuance.PickListItemQRO;
import com.dhl.inventory.dto.queryobject.warehouse.requestissuance.RequestIssuancePickListQRO;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;

@Component("warehouseRequestIssuancePickListQROConverter")
public class RequestIssuancePickListQROConverter implements QueryResultObjectConverter<RequestIssuancePickListQRO, RequestIssuance> {

	@Autowired
	private UserRepository userRepo;
	
	@Override
	public RequestIssuancePickListQRO generateQRO(String queryType, RequestIssuance entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RequestIssuancePickListQRO generateQRO(RequestIssuance requestIssuance) {
		SupplyRequest supplyRequest = requestIssuance.getSupplyRequest();
		NsisUser requestor = userRepo.getByKey(supplyRequest.getRequestById());
		RequestIssuancePickListQRO pickListFormData = new RequestIssuancePickListQRO()
				.setFacilityCode(supplyRequest.getFacility().getCodeName())
				.setRequestNumber(supplyRequest.getRequestNumber())
				.setRequestorName(requestor.getFullName())
				.setRequestedItems(generatePickListGroups(requestIssuance));
		
		if(null != supplyRequest.getIssuanceDate()){
			pickListFormData.setRequiredDeliveryDate(
					FormatterUtil.shortDateFormat(supplyRequest.getIssuanceDate()));
		}
		
		if(null != supplyRequest.getRequestDate()){
			pickListFormData.setRequestedDate(
					FormatterUtil.shortDateFormat(supplyRequest.getRequestDate()));
		}
		
		return pickListFormData;
	}
	
	private List<PickListItemGroupQRO> generatePickListGroups(RequestIssuance requestIssuance){
		List<PickListItemGroupQRO> returnValue = new ArrayList<>();
		Optional.object(requestIssuance.getSuppliedItemsGroup()).ifPresent(suppliedItemGroups -> {
			
			Map<String, Integer> requestItemQuantityMap = requestIssuance
					.getSupplyRequest().getRequestedItems().stream().collect(Collectors.toMap(
							val -> {
								return val.getItem().getInventoryItem().getCodeName();
							}, 
							val -> {
								return val.getQuantityByUOM();
							}
						)
					);
			
			Map<String, String> requestItemDescriptionMap = requestIssuance
					.getSupplyRequest().getRequestedItems().stream().collect(Collectors.toMap(
							val -> {
								return val.getItem().getInventoryItem().getCodeName();
							}, 
							val -> {
								return val.getItem().getInventoryItem().getDescription();
							}
						)
					);
			
			suppliedItemGroups.stream().forEach(group -> {
				String itemCode = group.getForRequestedItemCode();
				PickListItemGroupQRO pickListGroup = new PickListItemGroupQRO()
						.setItemCode(itemCode)
						.setDescription(requestItemDescriptionMap.get(itemCode))
						.setQtyRequested(FormatterUtil.integerFormat(requestItemQuantityMap.get(itemCode)));
				
				Optional.object(group.getSuppliedItems()).ifPresent(suppliedItems -> {
					int totalIssuedQty = suppliedItems.stream().mapToInt(val -> val.getQuantity()).sum();
					pickListGroup.setQtyIssued(FormatterUtil.integerFormat(totalIssuedQty));
					pickListGroup.setPickListItems(generatePickListItems(suppliedItems));
				});
				
				returnValue.add(pickListGroup);
			});
			
		});
		return returnValue;
	}
	
	private List<PickListItemQRO> generatePickListItems(Set<SuppliedItems> suppliedItems){
		List<PickListItemQRO> returnValue = new ArrayList<>();
		suppliedItems.stream().forEach(item -> {
			PickListItemQRO pickListItem = new PickListItemQRO()
					.setBatchNumber(item.getSupplySource().getBatchNumber())
					.setQtyIssuedPerBatch(FormatterUtil.integerFormat(item.getQuantity()));
			returnValue.add(pickListItem);
		});
		return returnValue;
	}

}
