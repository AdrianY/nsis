package com.dhl.inventory.converter.warehouse.orderrequest;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dhl.inventory.components.AuthenticationBean;
import com.dhl.inventory.converter.QueryResultObjectConverter;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.warehouse.orderrequest.OrderRequest;
import com.dhl.inventory.dto.queryobject.PersonnelQRO;
import com.dhl.inventory.dto.queryobject.warehouse.orderrequest.OrderRequestApproverQRO;
import com.dhl.inventory.dto.queryobject.warehouse.orderrequest.OrderRequestItemQRO;
import com.dhl.inventory.dto.queryobject.warehouse.orderrequest.OrderRequestQRO;
import com.dhl.inventory.dto.queryobject.warehouse.orderrequest.OrderRequestRecipientQRO;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.repository.warehouse.orderrequest.OrderRequestApprovalRepository;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;
import com.dhl.inventory.util.OrderRequestStatus;

@Component
public class OrderRequestQueryObjectConverter implements QueryResultObjectConverter<OrderRequestQRO, OrderRequest> {

	private static final Logger LOGGER = LoggerFactory.getLogger(OrderRequestQueryObjectConverter.class);
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private AuthenticationBean authBean;
	
	@Autowired
	private OrderRequestApprovalRepository orApprovalRepository;
	
	@Override
	public OrderRequestQRO generateQRO(String queryType, OrderRequest entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OrderRequestQRO generateQRO(OrderRequest or) {
		OrderRequestQRO qro = new OrderRequestQRO();
		qro.setCurrency(or.getCurrency())
		.setOrderCycle(or.getOrderCycle())
		.setOrderNumber(or.getOrderNumber())
		.setOrderType(or.getOrderType())
		.setFacilityId(or.getFacility().getCodeName())
		.setStatus(or.getStatus());
		
		Optional.object(or.getOrderDate()).ifPresent(val -> {
			qro.setOrderDate(FormatterUtil.shortDateFormat(val));
		});
		
		Optional.object(or.getDeliveryDate()).ifPresent(val -> {
			qro.setDeliveryDate(FormatterUtil.shortDateFormat(val));
		});
		
		Optional.object(or.getOrderById()).ifPresent(id -> {
			NsisUser user = userRepo.getByKey(id);
			PersonnelQRO orderBy = new PersonnelQRO();
			orderBy.setUserName(user.getUsername())
			.setEmailAddress(user.getEmailAddress())
			.setContactNumber(user.getContactNumber())
			.setFullName(user.getFullName());
			
			qro.setOrderBy(orderBy);
		});
		
		Optional.object(or.getRecipient()).ifPresent(val -> {
			PersonnelQRO recipientDetails = new PersonnelQRO();
			Optional.object(val.getRecipientId()).ifPresent(recipientId -> {
				NsisUser user = userRepo.getByKey(val.getRecipientId());
				recipientDetails.setUserName(user.getUsername())
				.setFullName(user.getFullName());
			});
			recipientDetails.setEmailAddress(val.getEmailAddress())
			.setContactNumber(val.getContactNumber());
			
			OrderRequestRecipientQRO recipientQRO = new OrderRequestRecipientQRO();
			recipientQRO.setRecipientDetails(recipientDetails)
			.setDeliveryAddress(val.getDeliveryAddress());
			
			qro.setRecipient(recipientQRO);
		});
		
		Optional.object(or.getOrderItems()).ifPresent(items -> {
			ArrayList<OrderRequestItemQRO> itemsQROList = new ArrayList<>();
			items.forEach(orderItem -> {
				OrderRequestItemQRO itemQRO = new OrderRequestItemQRO();
				orderItem.getItem().getInventoryItem().getCodeName();
				itemQRO.setItemCode(orderItem.getItem().getInventoryItem().getCodeName())
				.setQuantity(orderItem.getQuantityByUOM())
				.setRemarks(orderItem.getRemarks());
				
				itemsQROList.add(itemQRO);
			});
			qro.setItemsRequested(itemsQROList);
		});
		
		qro.setApprover(new OrderRequestApproverQRO());
		Optional.object(orApprovalRepository.findByOrderRequest(or)).then(val -> {
			
			LOGGER.info("Fetched OR "+or.getOrderNumber()+" has approval record already.");
			
			PersonnelQRO approver = new PersonnelQRO();
			NsisUser user = val.getApprover();
			approver.setUserName(user.getUsername())
			.setEmailAddress(user.getEmailAddress())
			.setContactNumber(user.getContactNumber())
			.setFullName(user.getFullName());
			
			OrderRequestApproverQRO approvalQRO = new OrderRequestApproverQRO();
			approvalQRO.setApproverDetails(approver)
			.setRemarks(val.getRemarks());
			
			qro.setApprover(approvalQRO);
		}, () -> {
			String userId = authBean.getAuthenticatedUserId();
			if(OrderRequestStatus.SUBMITTED.toString().equals(or.getStatus()) && null != userId){
				PersonnelQRO approver = new PersonnelQRO();
				NsisUser user = userRepo.getByKey(userId);
				approver.setUserName(user.getUsername())
				.setEmailAddress(user.getEmailAddress())
				.setContactNumber(user.getContactNumber())
				.setFullName(user.getFullName());
				
				OrderRequestApproverQRO approvalQRO = new OrderRequestApproverQRO();
				approvalQRO.setApproverDetails(approver);
				
				qro.setApprover(approvalQRO);
			}
		});
		
		return qro;
	}

}
