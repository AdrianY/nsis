package com.dhl.inventory.converter.warehouse.requestissuance;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dhl.inventory.converter.QueryResultObjectConverter;
import com.dhl.inventory.domain.security.NsisUser;
import com.dhl.inventory.domain.servicecenter.supplyrequest.SupplyRequest;
import com.dhl.inventory.domain.warehouse.requestissuance.RequestIssuance;
import com.dhl.inventory.dto.queryobject.warehouse.requestissuance.RequestIssuanceDeliveryReceiptFormQRO;
import com.dhl.inventory.dto.queryobject.warehouse.requestissuance.RequestIssuanceDeliveryReceiptItemQRO;
import com.dhl.inventory.repository.security.UserRepository;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;

@Component("warehouseRequestIssuanceDeliveryReceiptFormQROConverter")
public class RequestIssuanceDeliveryReceiptFormQROConverter implements QueryResultObjectConverter<RequestIssuanceDeliveryReceiptFormQRO, RequestIssuance> {

	@Autowired
	private UserRepository userRepo;
	
	@Override
	public RequestIssuanceDeliveryReceiptFormQRO generateQRO(String queryType, RequestIssuance entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RequestIssuanceDeliveryReceiptFormQRO generateQRO(RequestIssuance requestIssuance) {
		SupplyRequest supplyRequest = requestIssuance.getSupplyRequest();
		NsisUser requestor = userRepo.getByKey(supplyRequest.getRequestById());
		RequestIssuanceDeliveryReceiptFormQRO deliveryReceiptForm = new RequestIssuanceDeliveryReceiptFormQRO()
				.setFacilityCode(supplyRequest.getFacility().getCodeName())
				.setRequestNumber(supplyRequest.getRequestNumber())
				.setRequestorName(requestor.getFullName())
				.setRequestedItems(generateRequestedItemsIssuedList(requestIssuance));
		
		if(null != supplyRequest.getIssuanceDate()){
			deliveryReceiptForm.setRequiredDeliveryDate(
					FormatterUtil.shortDateFormat(supplyRequest.getIssuanceDate()));
		}
		
		if(null != requestIssuance.getIssuanceDate()){
			deliveryReceiptForm.setDateIssued(
					FormatterUtil.shortDateFormat(requestIssuance.getIssuanceDate()));
		}
		
		if(null != supplyRequest.getRequestDate()){
			deliveryReceiptForm.setRequestedDate(
					FormatterUtil.shortDateFormat(supplyRequest.getRequestDate()));
		}
		
		return deliveryReceiptForm;
	}
	
	private List<RequestIssuanceDeliveryReceiptItemQRO> generateRequestedItemsIssuedList(RequestIssuance requestIssuance){
		List<RequestIssuanceDeliveryReceiptItemQRO> returnValue = new ArrayList<>();
		Optional.object(requestIssuance.getSuppliedItemsGroup()).ifPresent(suppliedItemGroups -> {
			
			Map<String, Integer> requestItemQuantityMap = requestIssuance
					.getSupplyRequest().getRequestedItems().stream().collect(Collectors.toMap(
							val -> {
								return val.getItem().getInventoryItem().getCodeName();
							}, 
							val -> {
								return val.getQuantityByUOM();
							}
						)
					);
			
			Map<String, String> requestItemDescriptionMap = requestIssuance
					.getSupplyRequest().getRequestedItems().stream().collect(Collectors.toMap(
							val -> {
								return val.getItem().getInventoryItem().getCodeName();
							}, 
							val -> {
								return val.getItem().getInventoryItem().getDescription();
							}
						)
					);
			
			suppliedItemGroups.stream().forEach(group -> {
				String itemCode = group.getForRequestedItemCode();
				RequestIssuanceDeliveryReceiptItemQRO pickListGroup = 
						new RequestIssuanceDeliveryReceiptItemQRO()
						.setItemCode(itemCode)
						.setDescription(requestItemDescriptionMap.get(itemCode))
						.setQtyRequested(FormatterUtil.integerFormat(requestItemQuantityMap.get(itemCode)));
				
				Optional.object(group.getSuppliedItems()).ifPresent(suppliedItems -> {
					int totalIssuedQty = suppliedItems.stream().mapToInt(val -> val.getQuantity()).sum();
					pickListGroup.setQtyIssued(FormatterUtil.integerFormat(totalIssuedQty));
				});
				
				returnValue.add(pickListGroup);
			});
			
		});
		return returnValue;
	}

}
