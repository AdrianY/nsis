package com.dhl.inventory.converter.warehouse.inventorymaintenance;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.dhl.inventory.converter.QueryResultObjectConverter;
import com.dhl.inventory.converter.warehouse.orderrequest.OrderRequestQueryObjectConverter;
import com.dhl.inventory.domain.warehouse.inventorymaintenance.DeliveredItemBatch;
import com.dhl.inventory.domain.warehouse.inventorymaintenance.DeliveryReceipt;
import com.dhl.inventory.domain.warehouse.orderrequest.OrderRequest;
import com.dhl.inventory.domain.warehouse.orderrequest.OrderRequestItem;
import com.dhl.inventory.dto.queryobject.warehouse.inventorymaintenance.DeliveredItemBatchQRO;
import com.dhl.inventory.dto.queryobject.warehouse.inventorymaintenance.DeliveryReceiptQRO;
import com.dhl.inventory.dto.queryobject.warehouse.orderrequest.OrderRequestQRO;
import com.dhl.inventory.repository.warehouse.inventorymaintenance.DeliveredItemBatchRepository;
import com.dhl.inventory.util.FormatterUtil;
import com.dhl.inventory.util.Optional;

@Component
public class DeliveryReceiptQueryObjectConverter implements QueryResultObjectConverter<DeliveryReceiptQRO, DeliveryReceipt> {

	private static final Logger LOGGER = LoggerFactory.getLogger(DeliveryReceiptQueryObjectConverter.class);
	
	@Autowired
	private OrderRequestQueryObjectConverter orderRequestQROConverter;
	
	@Autowired
	@Qualifier("warehouseDeliveredItemBatchRepository")
	private DeliveredItemBatchRepository deliveredItemBatchRepo;
	
	@Override
	public DeliveryReceiptQRO generateQRO(String queryType, DeliveryReceipt entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeliveryReceiptQRO generateQRO(DeliveryReceipt deliveryReceipt) {
		DeliveryReceiptQRO qro = new DeliveryReceiptQRO();
		qro.setOrderRequest(getOrderRequestQRO(deliveryReceipt.getOrderRequest()))
		.setStatus(deliveryReceipt.getStatus())
		.setItemBatches(getItemBatchesQRO(deliveryReceipt));
		
		Optional.object(deliveryReceipt.getDateCreated()).ifPresent(dte -> {
			LOGGER.info("date delivered is not null");
			qro.setDateDelivered(FormatterUtil.shortDateFormat(dte));
		});
		
		return qro;
	}

	private OrderRequestQRO getOrderRequestQRO(OrderRequest orderRequest){
		return orderRequestQROConverter.generateQRO(orderRequest);
	}

	private Map<String, List<DeliveredItemBatchQRO>> getItemBatchesQRO(DeliveryReceipt deliveryReceipt){
		Set<OrderRequestItem> orderItems = deliveryReceipt.getOrderRequest().getOrderItems();
		
		return orderItems.stream().collect(Collectors.toMap(oi -> {
			return oi.getItem().getInventoryItem().getCodeName();
		}, oi -> {
			List<DeliveredItemBatchQRO> returnValue = new ArrayList<>();
			List<DeliveredItemBatch> itemBatches = 
				deliveredItemBatchRepo.findAllDeliveredItemBatchByOrderItemAndGroup(oi, deliveryReceipt);
			Optional.object(itemBatches).ifPresent(iBs -> {
				iBs.forEach(iB -> {
					DeliveredItemBatchQRO iBQRO = new DeliveredItemBatchQRO();
					Optional.object(iB.getRackingLocation()).ifPresent(rl -> {
						iBQRO.setRackingCode(rl.getRackCode());
					});
					Optional.object(iB.getDateStored()).ifPresent(dte -> {
						iBQRO.setDateStored(FormatterUtil.shortDateFormat(dte));
					});
					
					iBQRO.setQuantity(iB.getQuantity())
					.setBatchNumber(iB.getBatchNumber());
					
					returnValue.add(iBQRO);
				});
			});
			return returnValue;
		}));
	}
}
